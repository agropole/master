﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using LogusSrv.CORE.REST;
using LogusSrv.DAL.Operations;

namespace LogosSrv.Controllers
{
    [RoutePrefix("api/RateFuel")]
    public class RateFuelController : BaseCtrl
    {
        public RateFuelDb db = new RateFuelDb();

        [Route("Get")]
        [HttpPost]
        public Response<List<RateFuelList>> GetRate(Request<TypeTech> req)
        {
            return Perform(() => db.GetWorks(req.RequestObject));
        }

        [Route("UpdateRate")]
        [HttpPost]
        public Response<string> UpdateRate(Request<List<RateFuelList>> req)
        {
            return Perform(() => db.UpdateRate(req.RequestObject));
        }

        [Route("DeleteRate")]
        [HttpPost]
        public Response<string> UpdateRate(Request<int> req)
        {
            return Perform(() => db.DeleteRate(req.RequestObject));
        }


    }
}