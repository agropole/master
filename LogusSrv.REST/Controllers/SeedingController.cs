﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using LogusSrv.CORE.REST;
using LogusSrv.DAL.Operations;
using LogusSrv.DAL.Entities.DTO;
using LogusSrv.DAL.Entities.DTO.SeedingBridle;
using LogusSrv.DAL.Entities.Req.SeedingBridle;
using LogusSrv.DAL.Entities.Res.SeedingBridle;

namespace LogosSrv.Controllers
{
    [RoutePrefix("api/SeedingCtrl")]
    public class SeedingController : BaseCtrl
    {

        private readonly SeedingBridleDb _seedingDb = new SeedingBridleDb(); 

        [Route("GetInfoForSeedingBridle")]
        [HttpPost]
        public Response<InfoForSeedingDto> GetInfoForSeedingBridle(Request<bool> req)
        {
            return Perform(() => _seedingDb.GetInfoForSeedingBridle(req.RequestObject));
        }
        //проверка затрат перед удалением
        [Route("CheckSeedingSpeendingCult")]
        [HttpPost]
        public Response<DictionaryItemsDTO> CheckSeedingSpeendingCult(Request<int> req)
        {
            return Perform(() => _seedingDb.CheckSeedingSpeendingCult(req.RequestObject));
        }


        [Route("GetSeedingBridleList")]
        [HttpPost]
        public Response<GetSeedingListRes> GetSeedingBridleList(Request<GetSeedingListReq> req)
        {
            return Perform(() => _seedingDb.GetSeedingBridleList(req.RequestObject));
        }


        [Route("GetDetailSeedingBridleList")]
        [HttpPost]
        public Response<GetDetailSeedingBridleRes> GetDetailSeedingBridleList(Request<GetDetailSeedingListReq> req)
        {
            return Perform(() => _seedingDb.GetDetailSeedingList(req.RequestObject));
        }


        [Route("GetInfoForDetailSeeding")]
        [HttpPost]
        public Response<GetInfoDetailSeeding> GetInfoForDetailSeeding(Request<GetDetailSeedingListReq> req)
        {
            return Perform(() => _seedingDb.GetInfoForDetailSeeding(req.RequestObject));
        }
        
        [Route("UpdateDetailSeeing")]
        [HttpPost]
        public Response<GetDetailSeedingBridleRes> UpdateDetailSeeing(Request<GetDetailSeedingBridleRes> req)
        {
            return Perform(() => _seedingDb.UpdateDetailSeeing(req.RequestObject));
        }

        [Route("ApprovedSeedingBridle")]
        [HttpPost]
        public Response<string> ApprovedSeedingBridle(Request<GetDetailSeedingBridleRes> req)
        {
            return Perform(() => _seedingDb.ApprovedSeedingBridle(req.RequestObject));
        } 

        [Route("CloseSeedingBridle")]
        [HttpPost]
        public Response<string> CloseSeedingBridle(Request<CloseFieldModel> req)
        {
            return Perform(() => _seedingDb.CloseSeedingBridle(req.RequestObject));
        }
    }
}