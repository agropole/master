﻿using System;
using System.IO;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net.Http;
using System.Web.Http;
using LogusSrv.CORE.REST;
using LogusSrv.DAL.Operations;
using LogusSrv.DAL.Entities.Res;
using LogusSrv.DAL.Entities.Req;
using LogusSrv.DAL.Entities.DTO;
using LogusSrv.DAL.Entities.Req.CloseWaysList;
using System.Net;
using System.Net.Http.Headers;


namespace LogosSrv.Controllers
{
    [RoutePrefix("api/WaysListsCtrl")]
    public class WaysListsController : BaseCtrl
    {
        private WaysListsDb waysListDB = new WaysListsDb();
        private readonly LayersOperationDb _layersOperation = new LayersOperationDb();
        public ExportToExcel exp = new ExportToExcel();

        [Route("GetWaysLists")]
        [HttpPost]
        public Response<List<WaysListsDTO>> GetWaysLists(Request<GetWaysListsReq> req)
        {
            return Perform(() => waysListDB.GetWaysLists(req.RequestObject));
        }

        [Route("GetInfoForCreateWaysList")]
        [HttpPost]
        public Response<WaysListForCreateModel> GetInfoForCreateWaysList(Request<GetWaysListsReq> req)
        {
            return Perform(() => waysListDB.GetInfoForCreateWaysList(req.RequestObject));
            
        }

        [Route("CreateWaysList")]
        [HttpPost]
        public Response<int> CreateWaysList(Request<CreateWaysListReq> req)
        {
            return Perform(() => waysListDB.CreateWaysList(req.RequestObject));
        }

        [Route("GetWaysListById")]
        [HttpPost]
        public Response<CreateWaysListReq> GetWaysListById(Request<GetWaysReq> req)
        {
            return Perform(() => waysListDB.GetWaysListById(req.RequestObject));
        }

        [Route("UpdateWaysListById")]
        [HttpPost]
        public Response<bool> UpdateWaysListById(Request<CreateWaysListReq> req)
        {
            return Perform(() => waysListDB.UpdateWaysListById(req.RequestObject));
        }


        [Route("PrintWaysListById")]
        [HttpPost]
        public Response<PrintWaysListModel> PrintWaysListById(Request<GetWaysReq> req)
        {
            return Perform(() => waysListDB.PrintWaysList(req.RequestObject));
        }

        [Route("GetCloseInfoByWaysList")]
        [HttpPost]
        public Response<ClosedWayListModel> GetCloseInfoByWaysList(Request<GetWaysReq> req)
        {
            return Perform(() => waysListDB.GetCloseInfoByWaysList(req.RequestObject));
        }

        [Route("CopyTaskInWayList")]
        [HttpPost]
        public Response<int> CopyTaskInWayList(Request<int> req)
        {
            return Perform(() => waysListDB.CopyTaskInWayList(req.RequestObject));
        }
        
        [Route("GetRefuellerInfo")]
        [HttpPost]
        public Response<List<DictionaryItemsDTO>> GetRefuellerInfo(Request<GetWaysListsReq> req)
        {
            return Perform(() => waysListDB.GetRefuellerInfo(req.RequestObject));
        }

        [Route("DeleteTaskFromWaysList")]
        [HttpPost]
        public Response<string> DeleteTaskFromWaysList(Request<int> req)
        {
            return Perform(() => waysListDB.DeleteTaskFromWaysList(req.RequestObject));
        }


        [Route("GetRefuelingsByIdList")]
        [HttpPost]
        public Response<List<RefuelingDTO>> GetRefuelingsByIdList(Request<int> req)
        {
            return Perform(() => waysListDB.GetRefuelingsByIdList(req.RequestObject));
        }

        [Route("UpdateRefuelings")]
        [HttpPost]
        public Response<string> UpdateRefulings(Request<UpdateRefuelingsReq> req)
        {
            return Perform(() => waysListDB.UpdateRefuelings(req.RequestObject));
        }

        [Route("GetTaskById")]
        [HttpPost]
        public Response<TaskFullDTO> GetTaskById(Request<int> req)
        {
            return Perform(() => waysListDB.GetTaskById(req.RequestObject));
        }

        [Route("UpdateTaskInWayList")]
        [HttpPost]
        public Response<string> UpdateTaskInWayList(Request<UpdateTaskReq> req)
        {

            return Perform(() => waysListDB.CloseWayListUpdateTasks(req.RequestObject));
        }

        [Route("ClosedWaysList")]
        [HttpPost]
        public Response<string> ClosedWaysList(Request<int> req)
        {
            return Perform(() => waysListDB.ClosedWaysList(req.RequestObject));
        }

        [Route("UpdateCloseInfoByWaysList")]
        [HttpPost]
        public Response<string> UpdateCloseInfoByWaysList(Request<ClosedWayListModel> req)
        {
            return Perform(() => waysListDB.UpdateCloseInfoByWaysList(req.RequestObject));
        }

        [Route("GetInfoForCreateWaysListByShiftTasks")]
        [HttpPost]
        public Response<WaysListForCreateModel> GetInfoForCreateWaysListByShiftTasks(Request<int> req)
        {
            return Perform(() => waysListDB.GetInfoForCreateByDayTask(req.RequestObject));
        }

        [Route("GetWaysListByShiftTask")]
        [HttpPost]
        public Response<CreateWaysListReq> GetWaysListByShiftTask(Request<int> req)
        {
            return Perform(() => waysListDB.GetWaysListByShiftTask(req.RequestObject));
        }

        [Route("ApproveShiftTaskDetail")]
        [HttpPost]
        public Response<string> ApproveShiftTaskDetail(Request<int> req)
        {
            return Perform(() => waysListDB.ApproveShiftTaskDetail(req.RequestObject));
        }


        [Route("GetCloseInfoForMap")]
        [HttpGet]
        public CloseWayListMapDTO GetCloseInfoForMap(int? task_id = null)
        {
            return waysListDB.GetCloseInfoForMap((int)task_id);
        }

        [Route("GetCloseInfoForMapByField")]
        [HttpGet]
        public CloseWayListMapDTO GetCloseInfoForMapByField(int? field_id = null, int? task_id = null)
        {
            return waysListDB.GetCloseInfoForMapByField(field_id, task_id);
        }

        [Route("DeleteWayLists")]
        [HttpPost]
        public Response<string> DeleteWayLists(Request<int> req)
        {
            return Perform(() => waysListDB.DeleteWayLists(req.RequestObject));
        }

        [Route("InsertNewField")]
        [HttpPost]
        public Response<int> InsertNewField(Request<NewFieldsReq> req)
        {
            return Perform(() => waysListDB.InsertNewField(req.RequestObject));
        }

        [Route("calcThreadedArea")]
        [HttpPost]
        public Response<decimal?> calcThreadedArea(Request<int> req)
        {
            return Perform(() => _layersOperation.calcThreadedArea(req.RequestObject, DateTime.Now, true));
        }

        [Route("getConsWialon")]
        [HttpPost]
        public Response<List<double?>> getConsWialon(Request<int> req)
        {
            return Perform(() => _layersOperation.getConsWialon(req.RequestObject));
        }


        [Route("UpdateTotalPriceInAllTask")]
        [HttpPost]
        public Response<string> UpdateTotalPriceInAllTask(Request<bool> req)
        {
            return Perform(() => waysListDB.UpdateTotalPriceInAllTask());
        }

        [Route("Report")]
        [HttpGet]
        public HttpResponseMessage Report(int? id = null)
        {
            try
            {
                var templatePath = HttpContext.Current.Server.MapPath("~/ExcelReports/DayTaskTemplate.xlsx");
                var res = exp.loadExcel((int)id, templatePath);
                byte[] byteArray = res.bytes;
                var isIe = Request.Headers.UserAgent.ToString().Contains("Trident");
                HttpResponseMessage result = new HttpResponseMessage(HttpStatusCode.OK);
                if ((byteArray != null))
                {
                    var stream = new MemoryStream(byteArray);
                    var FileName = "Сменное задание " + res.date.ToString("dd-MM-yyyy HH:mm") + ".xlsx";
                    result.Content = new StreamContent(stream);
                    result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/vnd.ms-excel");

                 //   result.Content.Headers.Add("content-disposition", "attachment; filename=);
                    result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
                    {
                        FileName = isIe ? HttpUtility.UrlEncode(FileName) : (FileName)
                    };
                }
                else
                {
                    result.StatusCode = HttpStatusCode.NoContent;
                }
                return result;
            }
           catch (Exception ex)
            {
                HttpResponseMessage errorResult = new HttpResponseMessage(HttpStatusCode.BadRequest);
                errorResult.ReasonPhrase = ex.ToString();
                return errorResult;
            }
        }

        [Route("GetRate")]
        [HttpPost]
        public Response<RateResModel> GetRate(Request<RateReq> req)
        {
            return Perform(() => waysListDB.GetRate(req.RequestObject));
        }

        //расписание
        [Route("RecountBills")]
        [HttpGet]
        public string RecountBills()
        {
            _layersOperation.RecountBills();
            return "success";
        }

        [Route("GetTime")]
        [HttpGet]
        public DateTime GetTime()
        {
            return DateTime.Now;
        }

        [Route("RecountFuelForSheet")]
        [HttpGet]
        public string RecountFuelForSheet()
        {
            return waysListDB.RecountFuelForSheet();
        }
    }
}
