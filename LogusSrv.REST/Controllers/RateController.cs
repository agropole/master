﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using LogusSrv.CORE.REST;
using LogusSrv.DAL.Operations;

namespace LogosSrv.Controllers
{
    [RoutePrefix("api/Rate")]
    public class RateController : BaseCtrl
    {
        public RateDb db = new RateDb();

        [Route("GetInfo")]
        [HttpGet]
        public Response<RateInfoModel> GetRateInfo()
        {
            return Perform(() => db.GetInfo());
        }

        [Route("Get")]
        [HttpGet]
        public Response<List<WorkList>> GetRate()
        {
            return Perform(() => db.GetWorks());
        }

        [Route("UpdateRate")]
        [HttpPost]
        public Response<string> UpdateRate(Request<List<ModelUpdateRate>> req)
        {
            return Perform(() => db.UpdateRate(req.RequestObject));
        }
    }
}