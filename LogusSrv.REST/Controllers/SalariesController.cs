﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using LogusSrv.CORE.REST;
using LogusSrv.DAL.Operations;
using LogusSrv.DAL.Entities.DTO.Salaries;
using LogusSrv.DAL.Entities.Req.Salaries;
using LogusSrv.DAL.Entities.Req;
using LogusSrv.DAL.Entities.Res.Salaries;
using System.Net;
using System.Net.Http.Headers;
using System.Net.Http;



namespace LogosSrv.Controllers
{
    [RoutePrefix("api/SalariesCtrl")]
    public class SalariesController : BaseCtrl
    {
        private SalariesDb salariesDb = new SalariesDb();

        [Route("GetSalariesDocumentList")]
        [HttpPost]
        public Response<ListShortDocInfo> GetSalariesDocumentList(Request<GridDataReq> req)
        {
            return Perform(() => salariesDb.GetSalariesDocumentList(req.RequestObject));
        }

        [Route("GetSalariesDictionary")]
        [HttpPost]
        public Response<SalariesSprDTO> GetSalariesDictionary(Request<bool> req)
        {
            return Perform(() => salariesDb.GetSalariesDictionary(req.RequestObject));
        }

        [Route("GetNewInfoSalariesDoc")]
        [HttpPost]
        public Response<NewDocInfoRes> GetNewInfoSalariesDoc(Request<int> req)
        {
            return Perform(() => salariesDb.GetNewInfoSalariesDoc(req.RequestObject));
        }

        [Route("UpdateSalariesMainDocInfo")]
        [HttpPost]
        public Response<int> UpdateMainDocInfo(Request<FullDocInfoDTO> req)
        {
            return Perform(() => salariesDb.UpdateMainDocInfo(req.RequestObject));
        }

        [Route("getSalariesDocInfoById")]
        [HttpPost]
        public Response<FullDocInfoDTO> GetSalariesDocInfoById(Request<int> req)
        {
            return Perform(() => salariesDb.GetSalariesDocInfoById(req.RequestObject));
        }

        [Route("DeleteSalaryDoc")]
        [HttpPost]
        public Response<string> DeleteSalaryDoc(Request<int> req)
        {
            return Perform(() => salariesDb.DeleteSalaryDoc(req.RequestObject));
        }

        [Route("DeleteEmployeeSalaryInfo")]
        [HttpPost]
        public Response<string> DeleteEmployeeSalaryInfo(Request<int> req)
        {
            return Perform(() => salariesDb.DeleteEmployeeSalaryInfo(req.RequestObject));
        }

        [Route("LoadReport")]
        [HttpGet]
        public HttpResponseMessage Report(int? id = null)
        {
            try
            {
                var templatePath = HttpContext.Current.Server.MapPath("~/ExcelReports/TemplateZP.xlsx");
                var info = new LoadExcelInfoDTO();
                info = salariesDb.loadExcel((int)id, templatePath);
                var isIe = Request.Headers.UserAgent.ToString().Contains("Trident");
                HttpResponseMessage result = new HttpResponseMessage(HttpStatusCode.OK);
                if ((info.Content != null))
                {
                    var stream = new MemoryStream(info.Content);
                    var FileName = info.NameFile + ".xlsx";
                    result.Content = new StreamContent(stream);
                    result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/vnd.ms-excel");

                    //   result.Content.Headers.Add("content-disposition", "attachment; filename=);
                    result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
                    {
                        FileName = isIe ? HttpUtility.UrlEncode(FileName) : (FileName)
                    };
                }
                else
                {
                    result.StatusCode = HttpStatusCode.NoContent;
                }
                return result;
            }
            catch (Exception ex)
            {
                HttpResponseMessage errorResult = new HttpResponseMessage(HttpStatusCode.BadRequest);
                errorResult.ReasonPhrase = ex.ToString();
                return errorResult;
            }

        }
        
    }
}