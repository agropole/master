﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net.Http;
using System.Web.Http;
using LogusSrv.CORE.REST;
using LogusSrv.DAL.Operations;
using LogusSrv.DAL.Entities.Res;
using LogusSrv.DAL.Entities.Req;
using LogusSrv.DAL.Entities.DTO;


namespace LogosSrv.Controllers
{
    [RoutePrefix("api/DayTasksCtrl")]
    public class DayTasksController : BaseCtrl
    {
        public DayTasksDb dayTasksDb = new DayTasksDb();

        [Route("GetDayTasks")]
        [HttpPost]
        public Response<DayTasksModels> GetDayTasks(Request<GetDayTasksReq> req)
        {
            return Perform(() => dayTasksDb.GetDayTasks(req.RequestObject));
        }

        [Route("CreateDayTasksDetails")]
        [HttpPost]
        public Response<string> CreateDayTasksDetails(Request<DayTaskDetailReq> req)
        {
            return Perform(() => dayTasksDb.CreateDayTasksDetails(req.RequestObject));
        }

        [Route("UpdateDayTasksDetails")]
        [HttpPost]
        public Response<string> UpdateDayTasksDetails(Request<DayTaskDetailReq> req)
        {
            return Perform(() => dayTasksDb.UpdateDayTasksDetails(req.RequestObject));
        }

        [Route("SetBanDateWayBills")]
        [HttpPost]
        public Response<string> SetBanDateWayBills(Request<string> req)
        {
            return Perform(() => dayTasksDb.SetBanDateWayBills(req.RequestObject));
        }
        [Route("GetBanDateWayBills")]
        [HttpGet]
        public Response<DateTime> GetBanDateWayBills()
        {
            return Perform(() => dayTasksDb.GetBanDateWayBills());
        }


        [Route("GetInfoForCreateDayTasks")]
        [HttpPost]
        public Response<WaysListForCreateModel> GetInfoForCreateDayTasks(Request<UpdateDayTaskReq> req)
        {
            return Perform(() => dayTasksDb.GetInfoForCreateDayTasks(req.RequestObject));
        }

        [Route("GetDayTasksDetailsById")]
        [HttpPost]
        public Response<DayTaskDetailReq> GetDayTasksDetailsById(Request<int?> req)
        {
            return Perform(() => dayTasksDb.GetDayTasksDetailsById((int)req.RequestObject));
        }

        [Route("DeleteDayTask")]
        [HttpPost]
        public Response<string> DeleteDayTask(Request<List<int>> req)
        {
            return Perform(() => dayTasksDb.DeleteDayTask(req.RequestObject));
        }

        [Route("GetPrintShiftTasks")]
        [HttpPost]
        public Response<PrintShiftTasks> GetPrintShiftTasks(Request<int> req)
        {
            return Perform(() => dayTasksDb.GetPrintShiftTasks(req.RequestObject));
        }

        [Route("UpdateShiftTask")]
        [HttpPost]
        public Response<int> UpdateShiftTask(Request<UpdateDayTaskReq> req)
        {
           // return Perform(() => dayTasksDb.UpdateShiftTask(req.RequestObject));
            return Perform(() => dayTasksDb.UpdateSelectShiftTask(req.RequestObject));
        }

        [Route("CopyLocalShiftTask")]
        [HttpPost]
        public Response<List<int>> CopyLocalShiftTask(Request<List<int>> req)
        {
            return Perform(() => dayTasksDb.CopyLocalShiftTask(req.RequestObject));
        }

        [Route("CopyDayTasksShift")]
        [HttpPost]
        public Response<string> CopyDayTasksShift(Request<CopyShiftTaskReq> req)
        {
            return Perform(() => dayTasksDb.CopyDayTasksShift(req.RequestObject));
        }

        [Route("ApproveDayShiftTask")]
        [HttpPost]
        public Response<string> ApproveDayShiftTask(Request<CopyShiftTaskReq> req)
        {
            return Perform(() => dayTasksDb.ApproveShiftTask(req.RequestObject));
        }
        [Route("SetBanDateTC")]
        [HttpPost]
        public Response<string> SetBanDateTC(Request<string> req)
        {
            return Perform(() => dayTasksDb.SetBanDateTC(req.RequestObject));
        }
        [Route("GetBanDateTC")]
        [HttpGet]
        public Response<string> GetBanDateTC()
        {
            return Perform(() => dayTasksDb.GetBanDateTC());
        }
        [Route("IsDriverWayBillByShiftTaskId")]
        [HttpPost]
        public Response<bool?> IsDriverWayBillByShiftTaskId(Request<int?> req)
        {
            return Perform(() => dayTasksDb.IsDriverWayBillByShiftTaskId(req.RequestObject));
        }

        [Route("GetBalanceTraktorsInDayTask")]
        [HttpPost]
        public Response<BalanceDaytasksDTO> GetBalanceTraktorsInDayTask(Request<GetDayTasksReq> req)
        {
            return Perform(() => dayTasksDb.GetBalanceTraktorsInDayTask(req.RequestObject));
        }

        //===выделение зеленым
        [Route("GetStyleShiftTask")]
        [HttpPost]
        public Response<BalanceDaytasksDTO> GetStyleShiftTask(Request<GetDayTasksReq> req)
        {
            return Perform(() => dayTasksDb.GetStyleShiftTask(req.RequestObject));
        }

        [Route("UpdateCommentDayTask")]
        [HttpPost]
        public Response<string> UpdateCommentDayTask(Request<GetDayTasksReq> req)
        {
            return Perform(() => dayTasksDb.UpdateCommentDayTask(req.RequestObject));
        }
        





    }
}