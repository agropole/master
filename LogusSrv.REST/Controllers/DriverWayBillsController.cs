﻿using System.Collections.Generic;
using System.Web.Http;
using LogusSrv.CORE.REST;
using LogusSrv.DAL.Entities.DTO;
using LogusSrv.DAL.Entities.DTO.DriverWayBills;
using LogusSrv.DAL.Entities.Req;
using LogusSrv.DAL.Entities.Res;
using LogusSrv.DAL.Operations;

namespace LogosSrv.Controllers
{
    [RoutePrefix("api/DriverWayBillsCtrl")]
    public class DriverWayBillsController : BaseCtrl
    {
        private DriverWayBillsDb driverWayBillsDb = new DriverWayBillsDb();

        [Route("GetInfoForDriverWayBillItem")]
        [HttpPost]
        public Response<WaysListForCreateModel> GetInfoForDriverWayBillItem(Request<InfoForDriverWayBillReq> req)
        {
            return Perform(() => driverWayBillsDb.GetInfoForDriverWayBillItem(req.RequestObject));
        }

        [Route("GetDriverWayBillsList")]
        [HttpPost]
        public Response<List<DriverWayBillInListDTO>> GetDriverWayBillsList(Request<GetDriverWayBillsListReq> req)
        {
            return Perform(() => driverWayBillsDb.GetDriverWayBillsList(req.RequestObject));
        }

        [Route("SubmitDriverWayBillItem")]
        [HttpPost]
        public Response<int> SubmitDriverWayBillItem(Request<DriverWayBillItemDto> req)
        {
            return Perform(() => driverWayBillsDb.SubmitDriverWayBillItem(req.RequestObject));
        }

        [Route("GetDriverWayBillItemById")]
        [HttpPost]
        public Response<DriverWayBillItemDto> GetDriverWayBillItemById(Request<int> req)
        {
            return Perform(() => driverWayBillsDb.GetDriverWayBillItemById(req.RequestObject));
        }

        [Route("DeleteDriverWayBillItemById")]
        [HttpPost]
        public Response<bool> DeleteDriverWayBillItemById(Request<int> req)
        {
            return Perform(() => driverWayBillsDb.DeleteDriverWayBillItemById(req.RequestObject));
        }

        [Route("GetCloseDriverWayBillItemById")]
        [HttpPost]
        public Response<CloseDriverWayBillItemDto> GetCloseDriverWayBillItemById(Request<int> req)
        {
            return Perform(() => driverWayBillsDb.GetCloseDriverWayBillItemById(req.RequestObject));
        }

        [Route("CopyTaskInDriverWayList")]
        [HttpPost]
        public Response<int> CopyTaskInDriverWayList(Request<int> req)
        {
            return Perform(() => driverWayBillsDb.CopyTaskInDriverWayList(req.RequestObject));
        }

        [Route("GetPrintInfoForLightAvto")]
        [HttpPost]
        public Response<PrintLightAvtoDto> GetPrintInfoForLightAvto(Request<int> req)
        {
            return Perform(() => driverWayBillsDb.GetPrintInfoForLightAvto(req.RequestObject));
        }

        [Route("UpdateCloseDriverWayBillItem")]
        [HttpPost]
        public Response<bool> UpdateCloseDriverWayBillItem(Request<CloseDriverWayBillItemDto> req)
        {
            return Perform(() => driverWayBillsDb.UpdateCloseDriverWayBillItem(req.RequestObject));
        }

        [Route("DeleteDriverWayBillTaskById")]
        [HttpPost]
        public Response<bool> DeleteDriverWayBillTaskById(Request<int> req)
        {
            return Perform(() => driverWayBillsDb.DeleteDriverWayBillTaskById(req.RequestObject));
        }

        [Route("GetCloseDriverWayBillTaskById")]
        [HttpPost]
        public Response<CloseDriverWayBillTaskDto> GetCloseDriverWayBillTaskById(Request<int> req)
        {
            return Perform(() => driverWayBillsDb.GetCloseDriverWayBillTaskById(req.RequestObject));
        }

        [Route("SubmitCloseDriverWayBillTask")]
        [HttpPost]
        public Response<int> SubmitCloseDriverWayBillTask(Request<CloseDriverWayBillTaskDto> req)
        {
            return Perform(() => driverWayBillsDb.SubmitCloseDriverWayBillTask(req.RequestObject));
        }

        [Route("SetClosedStatusDriverWayBill")]
        [HttpPost]
        public Response<bool> SetClosedStatusDriverWayBill(Request<int> req)
        {
            return Perform(() => driverWayBillsDb.SetClosedStatusDriverWayBill(req.RequestObject));
        }

        [Route("GetDriverWayBillItemFromShiftTask")]
        [HttpPost]
        public Response<DriverWayBillItemDto> GetDriverWayBillItemFromShiftTask(Request<int> req)
        {
            return Perform(() => driverWayBillsDb.GetDriverWayBillItemFromShiftTask(req.RequestObject));
        }

        [Route("GetPrintDriverWayInfo")]
        [HttpPost]
        public Response<PrintDriverWayBillInfoDto> GetPrintDriverWayInfo(Request<int> req)
        {
            return Perform(() => driverWayBillsDb.GetPrintDriverWayInfo(req.RequestObject));
        }

        [Route("UpdateTotalPriceInAllTask")]
        [HttpPost]
        public Response<string> UpdateTotalPriceInAllTask(Request<bool> req)
        {
            return Perform(() => driverWayBillsDb.UpdateTotalPriceInAllTask());
        }
    }
}