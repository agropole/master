﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Http;
using LogusSrv.CORE.REST;
using LogusSrv.DAL.Entities.Res;
using LogusSrv.DAL.Operations;

namespace LogosSrv.Controllers
{
    [RoutePrefix("api/Products")]
    public class ProductController : BaseCtrl
    {
        private ProductDb _db = new ProductDb();

        [Route("GetInfoProduct")]
        [HttpGet]
        public Response<ProductInfo> GetInfoProduct()
        {
            return Perform(() => _db.GetInfoProduct());
        }

        [Route("YearCultureProduct")]
        [HttpPost]
        public Response<List<CultureProduct>> YearCultureProduct(Request<GrossReq> year)
        {
            return Perform(() => _db.YearCultureProduct(year.RequestObject));
        }

        [Route("UpdateCultureCurPrice")]
        [HttpPost]
        public Response<string> UpdateCultureCurPrice(Request<CultureProduct> req)
        {
            return Perform(() => _db.UpdateCultureCurPrice(req.RequestObject));
        }

        [Route("GetGrossInfo")]
        [HttpPost]
        public Response<GrossInfo> GetGrossInfo(Request<GrossReq> req)
        {
            return Perform(() => _db.GetGrossInfo(req.RequestObject));
        }

        [Route("GetGross")]
        [HttpPost]
        public Response<List<Gross>> GetGross(Request<GrossReq> req)
        {
            return Perform(() => _db.GetGross(req.RequestObject));
        }

        [Route("AddGross")]
        [HttpPost]
        public Response<string> AddGross(Request<List<Gross>> req)
        {
            return Perform(() => _db.AddGross(req.RequestObject));
        }

        [Route("GetSales")]
        [HttpPost]
        public Response<SaleModel> GetSales(Request<GrossReq> req)
        {
            return Perform(() => _db.GetSales(req.RequestObject));
        }

        [Route("AddSales")]
        [HttpPost]
        public Response<string> AddSales(Request<List<Sale>> req)
        {
            return Perform(() => _db.AddSales(req.RequestObject));
        }
       // удалить строку в Урожае
        [Route("DeleteGross")]
        [HttpPost]
        public Response<string> deleteGross(Request<List<DelRow>> req)
        {
            return Perform(() => _db.DeleteGross(req.RequestObject));
        }
        // обновление строки в Урожае
        [Route("UpdateGross")]
        [HttpPost]
        public Response<string> updateGross(Request<List<DelRow>> req)
        {
            return Perform(() => _db.UpdateGross(req.RequestObject));
        }
        [Route("UpdateSales")]
        [HttpPost]
        public Response<string> updateSales(Request<List<DelRow>> req)
        {
            return Perform(() => _db.UpdateSales(req.RequestObject));
        }
        //удалить строку в Продажах
        [Route("DeleteSales")]
        [HttpPost]
        public Response<string> deleteSales(Request<List<DelRow>> req)
        {
            return Perform(() => _db.DeleteSales(req.RequestObject));
        }

    }
}
