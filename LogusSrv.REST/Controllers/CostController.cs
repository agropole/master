﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Web;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using LogusSrv.CORE.REST;
using LogusSrv.DAL.Operations;
using LogusSrv.DAL.Entities.DTO.ExportExcel;
using System.Net.Http.Headers;
using LogusSrv.DAL.Entities.Res;
using LogusSrv.DAL.Entities.DTO;
using LogusSrv.CORE.Exceptions;



namespace LogosSrv.Controllers
{
    [RoutePrefix("api/CostCtrl")]
    public class CostController : BaseCtrl
    {

        private readonly ReportsModuleDb exportDb = new ReportsModuleDb();
        private readonly CostDb costDb = new CostDb();
        private readonly SparePartsDb spareDb = new SparePartsDb();
        //план-факт новое
        [Route("PlanFactAnalysis")]
        [HttpGet]
        public HttpResponseMessage PlanFactAnalysis(int year, bool isBig)
        {
            try
            {
                var templatePath = HttpContext.Current.Server.MapPath("~/ExcelReports/CostListTemplate.xlsx");
                var report = costDb.GetPlanFactExcel(templatePath, year, isBig);
                var isIe = Request.Headers.UserAgent.ToString().Contains("Trident");
                HttpResponseMessage result = new HttpResponseMessage(HttpStatusCode.OK);
                if ((report.data != null))
                {
                    var stream = new MemoryStream(report.data);
                    var FileName = report.fileName;
                    result.Content = new StreamContent(stream);
                    result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/vnd.ms-excel");
                    result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
                    {
                        FileName = isIe ? HttpUtility.UrlEncode(FileName) : (FileName)
                    };
                }
                else
                {
                    result.StatusCode = HttpStatusCode.NoContent;
                }
                return result;
            }
            catch (Exception ex)
            {
                HttpResponseMessage errorResult = new HttpResponseMessage(HttpStatusCode.BadRequest);
                errorResult.ReasonPhrase = ex.ToString();
                return errorResult;
            }
        }


        [Route("PlanFact")]
        [HttpPost]
        public Response<TCPlanFactModel> GetPlanFact(Request<ExpensesRequestModel> req)
        {
            if (req.RequestObject.year == null) req.RequestObject.year = DateTime.Now.Year;
            return Perform(() => exportDb.GetPlanFactTC((int)req.RequestObject.year, req.RequestObject.plant_id,1));
        }
        //расценки
        [Route("GetCostResExcel")]
        [HttpGet]
        public HttpResponseMessage GetCostResExcel()
        {
            try
            {
                var templatePath = HttpContext.Current.Server.MapPath("~/ExcelReports/RateTemplate.xlsx");
                var report = costDb.GetCostListResExcel(templatePath);
                var isIe = Request.Headers.UserAgent.ToString().Contains("Trident");
                HttpResponseMessage result = new HttpResponseMessage(HttpStatusCode.OK);
                if ((report.data != null))
                {
                    var stream = new MemoryStream(report.data);
                    var FileName = report.fileName;
                    result.Content = new StreamContent(stream);
                    result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/vnd.ms-excel");
                    result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
                    {
                        FileName = isIe ? HttpUtility.UrlEncode(FileName) : (FileName)
                    };
                }
                else
                {
                    result.StatusCode = HttpStatusCode.NoContent;
                }
                return result;
            }
            catch (Exception ex)
            {
                HttpResponseMessage errorResult = new HttpResponseMessage(HttpStatusCode.BadRequest);
                errorResult.ReasonPhrase = ex.ToString();
                return errorResult;
            }
        }

        [Route("GetDayRepairExcel")]
        [HttpGet]
        public HttpResponseMessage GetDayRepairExcel(int id, DateTime date)
        {
            try
            {
                var templatePath = HttpContext.Current.Server.MapPath("~/ExcelReports/DayRepairTemplate.xlsx");
                var report = costDb.GetDayListRepairExcel(templatePath, id, date);
                var isIe = Request.Headers.UserAgent.ToString().Contains("Trident");
                HttpResponseMessage result = new HttpResponseMessage(HttpStatusCode.OK);
                if ((report.data != null))
                {
                    var stream = new MemoryStream(report.data);
                    var FileName = report.fileName;
                    result.Content = new StreamContent(stream);
                    result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/vnd.ms-excel");
                    result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
                    {
                        FileName = isIe ? HttpUtility.UrlEncode(FileName) : (FileName)
                    };
                }
                else
                {
                    result.StatusCode = HttpStatusCode.NoContent;
                }
                return result;
            }
            catch (Exception ex)
            {
                HttpResponseMessage errorResult = new HttpResponseMessage(HttpStatusCode.BadRequest);
                errorResult.ReasonPhrase = ex.ToString();
                return errorResult;
            }
        }



        //НЗП отчет
        [Route("GetNZPResExcel")]
        [HttpGet]
        public HttpResponseMessage GetNZPResExcel(int year, bool? extend, bool simple)
        {
            ReportFile report;
            try
            {
                var templatePath = HttpContext.Current.Server.MapPath("~/ExcelReports/NZPTemplate.xlsx");
                if (!simple)
                {
                     report = costDb.GetNZPListResExcel(templatePath, year, extend);
                }
                else {
                    report = costDb.GetNZPSimpleExcel(templatePath, year);
                }
                var isIe = Request.Headers.UserAgent.ToString().Contains("Trident");
                HttpResponseMessage result = new HttpResponseMessage(HttpStatusCode.OK);
                if ((report.data != null))
                {
                    var stream = new MemoryStream(report.data);
                    var FileName = report.fileName;
                    result.Content = new StreamContent(stream);
                    result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/vnd.ms-excel");
                    result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
                    {
                        FileName = isIe ? HttpUtility.UrlEncode(FileName) : (FileName)
                    };
                }
                else
                {
                    result.StatusCode = HttpStatusCode.NoContent;
                }
                return result;
            }
            catch (Exception ex)
            {
                HttpResponseMessage errorResult = new HttpResponseMessage(HttpStatusCode.BadRequest);
                errorResult.ReasonPhrase = ex.ToString();
                return errorResult;
            }
        }

        //табель рабочего времени - ЕН
        [Route("GetDailyEmployeesWortTimeExpExl")]
        [HttpGet]
        public HttpResponseMessage GetDailyEmployeesWortTimeExpExl(DateTime startDate, int? position, DateTime endDate)
        {
            try
            {
                var templatePath = HttpContext.Current.Server.MapPath("~/ExcelReports/TimeSheetTemp.xlsx");
                var report = costDb.GetDailyEmployeesWorkTime(templatePath, startDate, position, endDate);
                var isIe = Request.Headers.UserAgent.ToString().Contains("Trident");
                HttpResponseMessage result = new HttpResponseMessage(HttpStatusCode.OK);
                if ((report.data != null))
                {
                    var stream = new MemoryStream(report.data);
                    var FileName = report.fileName;
                    result.Content = new StreamContent(stream);
                    result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/vnd.ms-excel");
                    result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
                    {
                        FileName = isIe ? HttpUtility.UrlEncode(FileName) : (FileName)
                    };
                }
                else
                {
                    result.StatusCode = HttpStatusCode.NoContent;
                }
                return result;
            }
            catch (Exception ex)
            {
                HttpResponseMessage errorResult = new HttpResponseMessage(HttpStatusCode.BadRequest);
                errorResult.ReasonPhrase = ex.ToString();
                return errorResult;
            }
        }

        //движение запчастей
        [Route("GetMoveSparePartsReport")]
        [HttpGet]
        public HttpResponseMessage GetMoveSparePartsReport(DateTime start, DateTime end)
        {
            try
            {
                var templatePath = HttpContext.Current.Server.MapPath("~/ExcelReports/MoveSparePartsTemplate.xlsx");
                var report = costDb.MoveSparePartsReportExcel(templatePath, start, end);
                var isIe = Request.Headers.UserAgent.ToString().Contains("Trident");
                HttpResponseMessage result = new HttpResponseMessage(HttpStatusCode.OK);
                if ((report.data != null))
                {
                    var stream = new MemoryStream(report.data);
                    var FileName = report.fileName;
                    result.Content = new StreamContent(stream);
                    result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/vnd.ms-excel");
                    result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
                    {
                        FileName = isIe ? HttpUtility.UrlEncode(FileName) : (FileName)
                    };
                }
                else
                {
                    result.StatusCode = HttpStatusCode.NoContent;
                }
                return result;
            }
            catch (Exception ex)
            {
                HttpResponseMessage errorResult = new HttpResponseMessage(HttpStatusCode.BadRequest);
                errorResult.ReasonPhrase = ex.ToString();
                return errorResult;
            }
        }
        //
        //Свод затрат
        [Route("GetRepairEquipReport")]
        [HttpGet]
        public HttpResponseMessage GetRepairEquipReport(DateTime start, DateTime end, int techId, int eqId)
        {
            try
            {
                var templatePath = HttpContext.Current.Server.MapPath("~/ExcelReports/RepairTechEqiupTemplate.xlsx");
                var report = costDb.RepairTechEqiupExcel(templatePath, start, end, techId, eqId);
                var isIe = Request.Headers.UserAgent.ToString().Contains("Trident");
                HttpResponseMessage result = new HttpResponseMessage(HttpStatusCode.OK);
                if ((report.data != null))
                {
                    var stream = new MemoryStream(report.data);
                    var FileName = report.fileName;
                    result.Content = new StreamContent(stream);
                    result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/vnd.ms-excel");
                    result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
                    {
                        FileName = isIe ? HttpUtility.UrlEncode(FileName) : (FileName)
                    };
                }
                else
                {
                    result.StatusCode = HttpStatusCode.NoContent;
                }
                return result;
            }
            catch (Exception ex)
            {
                HttpResponseMessage errorResult = new HttpResponseMessage(HttpStatusCode.BadRequest);
                errorResult.ReasonPhrase = ex.ToString();
                return errorResult;
            }
        }




        //ЕН печать
        [Route("GetDailyResExcel")]
        [HttpGet]
        public HttpResponseMessage GetDailyResExcel(int ActId)
        {
            try
            {
                var templatePath = HttpContext.Current.Server.MapPath("~/ExcelReports/DailyTemplate.xlsx");
                var report = costDb.GetDailyListResExcel(templatePath,ActId);
                var isIe = Request.Headers.UserAgent.ToString().Contains("Trident");
                HttpResponseMessage result = new HttpResponseMessage(HttpStatusCode.OK);
                if ((report.data != null))
                {
                    var stream = new MemoryStream(report.data);
                    var FileName = report.fileName;
                    result.Content = new StreamContent(stream);
                    result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/vnd.ms-excel");
                    result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
                    {
                        FileName = isIe ? HttpUtility.UrlEncode(FileName) : (FileName)
                    };
                }
                else
                {
                    result.StatusCode = HttpStatusCode.NoContent;
                }
                return result;
            }
            catch (Exception ex)
            {
                HttpResponseMessage errorResult = new HttpResponseMessage(HttpStatusCode.BadRequest);
                errorResult.ReasonPhrase = ex.ToString();
                return errorResult;
            }
        }


        //история
        [Route("GetHistoryResExcel")]
        [HttpGet]
        public HttpResponseMessage GetHistoryResExcel(int ActId)
        {
            try
            {
                var templatePath = HttpContext.Current.Server.MapPath("~/ExcelReports/HistorySparePartsTemplate.xlsx");
                var report = costDb.GetHistoryResExcel(templatePath, ActId);
                var isIe = Request.Headers.UserAgent.ToString().Contains("Trident");
                HttpResponseMessage result = new HttpResponseMessage(HttpStatusCode.OK);
                if ((report.data != null))
                {
                    var stream = new MemoryStream(report.data);
                    var FileName = report.fileName;
                    result.Content = new StreamContent(stream);
                    result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/vnd.ms-excel");
                    result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
                    {
                        FileName = isIe ? HttpUtility.UrlEncode(FileName) : (FileName)
                    };
                }
                else
                {
                    result.StatusCode = HttpStatusCode.NoContent;
                }
                return result;
            }
            catch (Exception ex)
            {
                HttpResponseMessage errorResult = new HttpResponseMessage(HttpStatusCode.BadRequest);
                errorResult.ReasonPhrase = ex.ToString();
                return errorResult;
            }
        }

        //РАСХОД ТМЦ
        [Route("GetConsumptionExcel")]
        [HttpGet]
        public HttpResponseMessage GetConsumptionExcel(DateTime startDate, DateTime endDate, Boolean check, int? matId, int? PlantId)
        {
           string templatePath=null;

           if (check) { templatePath = HttpContext.Current.Server.MapPath("~/ExcelReports/ConsumptionTemplate.xlsx"); }
           else { templatePath = HttpContext.Current.Server.MapPath("~/ExcelReports/SmallConsumptionTemplate.xlsx"); }
            ReportFile report = costDb.GetFullConsumptionExcel(templatePath, startDate, endDate, matId, PlantId, check);
           
            try
            {

                var isIe = Request.Headers.UserAgent.ToString().Contains("Trident");
                HttpResponseMessage result = new HttpResponseMessage(HttpStatusCode.OK);
                if ((report.data != null))
                {
                    var stream = new MemoryStream(report.data);
                    var FileName = report.fileName;
                    result.Content = new StreamContent(stream);
                    result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/vnd.ms-excel");
                    result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
                    {
                        FileName = isIe ? HttpUtility.UrlEncode(FileName) : (FileName)
                    };
                }
                else
                {
                    result.StatusCode = HttpStatusCode.NoContent;
                }
                return result;
            }
            catch (Exception ex)
            {
                HttpResponseMessage errorResult = new HttpResponseMessage(HttpStatusCode.BadRequest);
                errorResult.ReasonPhrase = ex.ToString();
                return errorResult;
            }
        }

        //Справочники
        [Route("GetDictionaryExcel")]
        [HttpGet]
        public HttpResponseMessage GetDictionaryExcel(int idDictionary)
        {
            try
            {
                var templatePath = HttpContext.Current.Server.MapPath("~/ExcelReports/DictionaryTemplate.xlsx");
                var report = costDb.GetDictionaryExcel(templatePath, idDictionary);
                var isIe = Request.Headers.UserAgent.ToString().Contains("Trident");
                HttpResponseMessage result = new HttpResponseMessage(HttpStatusCode.OK);
                if ((report.data != null))
                {
                    var stream = new MemoryStream(report.data);
                    var FileName = report.fileName;
                    result.Content = new StreamContent(stream);
                    result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/vnd.ms-excel");
                    result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
                    {
                        FileName = isIe ? HttpUtility.UrlEncode(FileName) : (FileName)
                    };
                }
                else
                {
                    result.StatusCode = HttpStatusCode.NoContent;
                }
                return result;
            }
            catch (Exception ex)
            {
                HttpResponseMessage errorResult = new HttpResponseMessage(HttpStatusCode.BadRequest);
                errorResult.ReasonPhrase = ex.ToString();
                return errorResult;
            }
        }
        //

        [Route("GetPlanChartCultResExcel")]
        [HttpGet]
        public HttpResponseMessage GetPlanChartCultResExcel(int? cultId, int? year)
        {
            try
            {
                var templatePath = HttpContext.Current.Server.MapPath("~/ExcelReports/PlanChartCultTemplate.xlsx");
                var report = costDb.GetPlanChartCultResExcel(templatePath, cultId, year);
                var isIe = Request.Headers.UserAgent.ToString().Contains("Trident");
                HttpResponseMessage result = new HttpResponseMessage(HttpStatusCode.OK);
                if ((report.data != null))
                {
                    var stream = new MemoryStream(report.data);
                    var FileName = report.fileName;
                    result.Content = new StreamContent(stream);
                    result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/vnd.ms-excel");
                    result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
                    {
                        FileName = isIe ? HttpUtility.UrlEncode(FileName) : (FileName)
                    };
                }
                else
                {
                    result.StatusCode = HttpStatusCode.NoContent;
                }
                return result;
            }
            catch (Exception ex)
            {
                HttpResponseMessage errorResult = new HttpResponseMessage(HttpStatusCode.BadRequest);
                errorResult.ReasonPhrase = ex.ToString();
                return errorResult;
            }
        }
        //ПЛАН-ФАКТ ПО СЕБЕСТОИМОСТИ

        [Route("GetPlanFactDeviationExcel")]
        [HttpGet]
        public HttpResponseMessage GetPlanFactDeviationExcel(DateTime startDate, DateTime endDate, int? plantId, string selDate)
        {
            try
            {
             //   var templatePath = HttpContext.Current.Server.MapPath("~/ExcelReports/PlanFactTemplate.xlsx");
                var templatePath = HttpContext.Current.Server.MapPath("~/ExcelReports/PlanFactTemp1.xlsx");
                var report = costDb.GetPlanFactDeviationExcel(startDate, endDate, plantId, templatePath, selDate);
                var isIe = Request.Headers.UserAgent.ToString().Contains("Trident");
                HttpResponseMessage result = new HttpResponseMessage(HttpStatusCode.OK);
                if ((report.data != null))
                {
                    var stream = new MemoryStream(report.data);
                    var FileName = report.fileName;
                    result.Content = new StreamContent(stream);
                    result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/vnd.ms-excel");
                    result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
                    {
                        FileName = isIe ? HttpUtility.UrlEncode(FileName) : (FileName)
                    };
                }
                else
                {
                    result.StatusCode = HttpStatusCode.NoContent;
                }
                return result;
            }
            catch (Exception ex)
            {
                HttpResponseMessage errorResult = new HttpResponseMessage(HttpStatusCode.BadRequest);
                errorResult.ReasonPhrase = ex.ToString();
                return errorResult;
            }
        }

        //ОТЧЕТЫ
        [Route("GetRepairsReportsInfo")]
        [HttpPost]
        public Response<SpareReportsInfo> GetRepairsReportsInfo()
        {
            return Perform(() => costDb.GetRepairsReportsInfo());
        }

        [Route("GetRespRepairsEmployees")]
        [HttpPost]
        public Response<List<DictionaryItemsDTO>> GetRespRepairsEmployees(Request<DateInterval> req)
        {
            return Perform(() => costDb.GetRespRepairsEmployees(req.RequestObject.start, req.RequestObject.end));
        }

        //
        [Route("GetRespSheetsEmployees")]
        [HttpPost]
        public Response<List<DictionaryItemsDTO>> GetRespSheetsEmployees(Request<DateInterval> req)
        {
            return Perform(() => costDb.GetRespSheetsEmployees(req.RequestObject.start, req.RequestObject.end));
        }



        //Остатки по запчастям
        [Route("GetSparePartsBalanceExcel")]
        [HttpGet]
        public HttpResponseMessage GetSparePartsBalanceExcel(int storage_id, Boolean storage, Boolean reserve)
        {
            try
            {
                var templatePath = "";
                if (!storage & !reserve)
                {
                    templatePath = HttpContext.Current.Server.MapPath("~/ExcelReports/SpareBalanceTemplate.xlsx");
                }
                if (storage || reserve)
                {
                    templatePath = HttpContext.Current.Server.MapPath("~/ExcelReports/SpareBalanceTemplate2.xlsx");
                }
                var report = costDb.GetSparePartsBalanceListResExcel(templatePath, storage_id, storage, reserve);
                var isIe = Request.Headers.UserAgent.ToString().Contains("Trident");
                HttpResponseMessage result = new HttpResponseMessage(HttpStatusCode.OK);
                if ((report.data != null))
                {
                    var stream = new MemoryStream(report.data);
                    var FileName = report.fileName;
                    result.Content = new StreamContent(stream);
                    result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/vnd.ms-excel");
                    result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
                    {
                        FileName = isIe ? HttpUtility.UrlEncode(FileName) : (FileName)
                    };
                }
                else
                {
                    result.StatusCode = HttpStatusCode.NoContent;
                }
                return result;
            }
            catch (Exception ex)
            {
                HttpResponseMessage errorResult = new HttpResponseMessage(HttpStatusCode.BadRequest);
                errorResult.ReasonPhrase = ex.ToString();
                return errorResult;
            }
        }

        //Расход по запчастям
        [Route("GetSparePartsIntakeExcel")]
        [HttpGet]
        public HttpResponseMessage GetSparePartsIntakeExcel(int storage_id, DateTime start, DateTime end)
        {
            try
            {
                var templatePath = HttpContext.Current.Server.MapPath("~/ExcelReports/SpareIntakeTemplate.xlsx");
                var report = costDb.GetSparePartsIntakeListResExcel(templatePath, storage_id, start, end);
                var isIe = Request.Headers.UserAgent.ToString().Contains("Trident");
                HttpResponseMessage result = new HttpResponseMessage(HttpStatusCode.OK);
                if ((report.data != null))
                {
                    var stream = new MemoryStream(report.data);
                    var FileName = report.fileName;
                    result.Content = new StreamContent(stream);
                    result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/vnd.ms-excel");
                    result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
                    {
                        FileName = isIe ? HttpUtility.UrlEncode(FileName) : (FileName)
                    };
                }
                else
                {
                    result.StatusCode = HttpStatusCode.NoContent;
                }
                return result;
            }
            catch (Exception ex)
            {
                HttpResponseMessage errorResult = new HttpResponseMessage(HttpStatusCode.BadRequest);
                errorResult.ReasonPhrase = ex.ToString();
                return errorResult;
            }
        }

        //Учет рабочего времени
        [Route("GetRepairsSpendingsExcel")]
        [HttpGet]
        public HttpResponseMessage GetRepairsSpendingsExcel(int emp_id, DateTime start, DateTime end, bool is_big, int isSheet)
        {
            try
            {
                HttpResponseMessage result = new HttpResponseMessage(HttpStatusCode.OK);
                ReportFile report;
                if (!is_big)
                {
                    var templatePath = HttpContext.Current.Server.MapPath("~/ExcelReports/RepairsSpendingsTemplate.xlsx");
                    report = costDb.GetRepairsSpendingsListResExcel(templatePath, emp_id, start, end, isSheet);
                }
                else
                {
                    var templatePath = HttpContext.Current.Server.MapPath("~/ExcelReports/RepairsSpendingsBigTemplate.xlsx");
                    report = costDb.GetRepairsSpendingsBigListResExcel(templatePath, emp_id, start, end);
                }

                var isIe = Request.Headers.UserAgent.ToString().Contains("Trident");

                if ((report.data != null))
                {
                    var stream = new MemoryStream(report.data);
                    var FileName = report.fileName;
                    result.Content = new StreamContent(stream);
                    result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/vnd.ms-excel");
                    result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
                    {
                        FileName = isIe ? HttpUtility.UrlEncode(FileName) : (FileName)
                    };
                }
                else
                {
                    result.StatusCode = HttpStatusCode.NoContent;
                }
                return result;
            }
            catch (Exception ex)
            {
                HttpResponseMessage errorResult = new HttpResponseMessage(HttpStatusCode.BadRequest);
                errorResult.ReasonPhrase = ex.ToString();
                return errorResult;
            }
        }

        //Загрузка доступной техники в dropdown rsTech и оборудования в rsEq
        [Route("GetRespRepairsTech")]
        [HttpPost]
        public Response<RepairsReportsInfo> GetRespRepairsTech(Request<DateInterval> req)
        {
            return Perform(() => costDb.GetRespRepairsTech(req.RequestObject.start, req.RequestObject.end));
        }

        //Затраты на ремонт
        [Route("GetRepairsTechSpendingsExcel")]
        [HttpGet]
        public HttpResponseMessage GetRepairsTechSpendingsExcel(int tech_id, int equi_id, DateTime start, DateTime end)
        {
            try
            {
                HttpResponseMessage result = new HttpResponseMessage(HttpStatusCode.OK);
                ReportFile report = new ReportFile();
                var templatePath = HttpContext.Current.Server.MapPath("~/ExcelReports/TechRepairsSparePartsSpendingsTemplate.xlsx");
                report = costDb.GetRepairsSparePartsSpendingsListResExcel(templatePath, tech_id, equi_id, start, end);

                var isIe = Request.Headers.UserAgent.ToString().Contains("Trident");

                if ((report.data != null))
                {
                    var stream = new MemoryStream(report.data);
                    var FileName = report.fileName;
                    result.Content = new StreamContent(stream);
                    result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/vnd.ms-excel");
                    result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
                    {
                        FileName = isIe ? HttpUtility.UrlEncode(FileName) : (FileName)
                    };
                }
                else
                {
                    result.StatusCode = HttpStatusCode.NoContent;
                }
                return result;
            }
            catch (Exception ex)
            {
                HttpResponseMessage errorResult = new HttpResponseMessage(HttpStatusCode.BadRequest);
                errorResult.ReasonPhrase = ex.ToString();
                return errorResult;
            }
        }

        // Реестр документов
        [Route("GetDocumentRegistryExcel")]
        [HttpGet]
        public HttpResponseMessage GetDocumentRegistryExcel(DateTime start, DateTime end)
        {
            try
            {
                HttpResponseMessage result = new HttpResponseMessage(HttpStatusCode.OK);
                ReportFile report;

                var templatePath = HttpContext.Current.Server.MapPath("~/ExcelReports/DocumentRegistrySpareTemplate.xlsx");
                report = costDb.GetSpareRegistryResExcel(templatePath, start, end);

                var isIe = Request.Headers.UserAgent.ToString().Contains("Trident");

                if ((report.data != null))
                {
                    var stream = new MemoryStream(report.data);
                    var FileName = report.fileName;
                    result.Content = new StreamContent(stream);
                    result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/vnd.ms-excel");
                    result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
                    {
                        FileName = isIe ? HttpUtility.UrlEncode(FileName) : (FileName)
                    };
                }
                else
                {
                    result.StatusCode = HttpStatusCode.NoContent;
                }
                return result;
            }
            catch (Exception ex)
            {
                HttpResponseMessage errorResult = new HttpResponseMessage(HttpStatusCode.BadRequest);
                errorResult.ReasonPhrase = ex.ToString();
                return errorResult;
            }
        }
        //План-график ремонтных работ
        [Route("GetPlanChartTechResExcel")]
        [HttpGet]
        public HttpResponseMessage GetPlanChartTechResExcel(int? techIdType, int? eqIdType, int? year)
        {
            try
            {
                var templatePath = HttpContext.Current.Server.MapPath("~/ExcelReports/PlanChartTechTemplate.xlsx");
                var report = costDb.GetPlanChartTechResExcel(templatePath, techIdType, eqIdType, year);
                var isIe = Request.Headers.UserAgent.ToString().Contains("Trident");
                HttpResponseMessage result = new HttpResponseMessage(HttpStatusCode.OK);
                if ((report.data != null))
                {
                    var stream = new MemoryStream(report.data);
                    var FileName = report.fileName;
                    result.Content = new StreamContent(stream);
                    result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/vnd.ms-excel");
                    result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
                    {
                        FileName = isIe ? HttpUtility.UrlEncode(FileName) : (FileName)
                    };
                }
                else
                {
                    result.StatusCode = HttpStatusCode.NoContent;
                }
                return result;
            }
            catch (Exception ex)
            {
                HttpResponseMessage errorResult = new HttpResponseMessage(HttpStatusCode.BadRequest);
                errorResult.ReasonPhrase = ex.ToString();
                return errorResult;
            }
        }
        //1С - запчасти
        [Route("Get1CSparePartsReport")]
        [HttpGet]
        public HttpResponseMessage Get1CSparePartsReport(DateTime start, DateTime end)
        {
            try
            {
                HttpResponseMessage result = new HttpResponseMessage(HttpStatusCode.OK);

                string bum = costDb.GetSparePartsXMLData((DateTime)start, (DateTime)end);

                result.Content = new StringContent(bum);
                result.Content.Headers.ContentType = new MediaTypeHeaderValue("text/xml");
                result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
                {
                    FileName = "spareparts.xml",
                };
                return result;
            }
            catch (Exception ex)
            {
                HttpResponseMessage errorResult = new HttpResponseMessage(HttpStatusCode.BadRequest);
                errorResult.ReasonPhrase = ex.ToString();
                return errorResult;
            }
        }

        //1c - Excel
        [Route("Get1CSparePartsExcel")]
        [HttpGet]
        public HttpResponseMessage Get1CSparePartsExcel(DateTime start, DateTime end)
        {
            try
            {
                var templatePath = HttpContext.Current.Server.MapPath("~/ExcelReports/WriteOffSpare1CTemplate.xlsx");
                var report = costDb.Get1CSparePartsExcel(templatePath, (DateTime)start, (DateTime)end);
                var isIe = Request.Headers.UserAgent.ToString().Contains("Trident");
                HttpResponseMessage result = new HttpResponseMessage(HttpStatusCode.OK);
                if ((report.data != null))
                {
                    var stream = new MemoryStream(report.data);
                    var FileName = report.fileName;
                    result.Content = new StreamContent(stream);
                    result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/vnd.ms-excel");
                    result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
                    {
                        FileName = isIe ? HttpUtility.UrlEncode(FileName) : (FileName)
                    };
                }
                else
                {
                    result.StatusCode = HttpStatusCode.NoContent;
                }
                return result;
            }
            catch (Exception ex)
            {
                HttpResponseMessage errorResult = new HttpResponseMessage(HttpStatusCode.BadRequest);
                errorResult.ReasonPhrase = ex.ToString();
                return errorResult;
            }
        }
        //


        // Журналs учета применения мин. уд и пестицидов
        [Route("GetJournalExportExcel")]
        [HttpGet]
        public HttpResponseMessage GetJournalExportExcel(DateTime startDate, DateTime endDate, int? employeerId = null, int? materialId = null, int? fieldId = null, int? plantId = null)
        {
            try
            {
                HttpResponseMessage result = new HttpResponseMessage(HttpStatusCode.OK);
                ReportFile report;
                var templatePath = HttpContext.Current.Server.MapPath("~/ExcelReports/JournalSZR.xlsx");
                switch (materialId)
                {
                    case 1: templatePath = HttpContext.Current.Server.MapPath("~/ExcelReports/JournalSZR.xlsx"); break;
                    case 2: templatePath = HttpContext.Current.Server.MapPath("~/ExcelReports/JournalChemicalFertilizers.xlsx"); break;
                }
                report = costDb.GetJournalExcel(templatePath, startDate, endDate, employeerId, fieldId, plantId, materialId);
                var isIe = Request.Headers.UserAgent.ToString().Contains("Trident");

                if ((report.data != null))
                {
                    var stream = new MemoryStream(report.data);
                    var FileName = report.fileName;
                    result.Content = new StreamContent(stream);
                    result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/vnd.ms-excel");
                    result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
                    {
                        FileName = isIe ? HttpUtility.UrlEncode(FileName) : (FileName)
                    };
                }
                else
                {
                    result.StatusCode = HttpStatusCode.NoContent;
                }
                return result;
            }
            catch (Exception ex)
            {
                HttpResponseMessage errorResult = new HttpResponseMessage(HttpStatusCode.BadRequest);
                errorResult.ReasonPhrase = ex.ToString();
                return errorResult;
            }
        }










    }






}
