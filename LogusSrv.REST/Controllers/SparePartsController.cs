﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using LogusSrv.CORE.REST;
using LogusSrv.DAL.Operations;
using LogusSrv.DAL.Entities.Res;
using LogusSrv.DAL.Entities.Req;
using LogusSrv.DAL.Entities.DTO;
using LogusSrv.DAL.Entities.DTO.ActsSzr;
using System.Net.Http;
using System.Net.Http.Headers;
using System.IO;
using System.Net;

namespace LogosSrv.Controllers
{

    [RoutePrefix("api/SparePartsCtrl")]
    public class SparePartsController : BaseCtrl
    {
        public SparePartsDb spareDb = new SparePartsDb();
        //подгрузка расценок на работы в ежедневном 
        [Route("GetTaskRate")]
        [HttpPost]
        public Response<RateResModel> GetTaskRate(Request<RateResModel> req)
        {
            return Perform(() => spareDb.GetTaskRateSpareParts(req.RequestObject));
        }
        [Route("GetDictionaryDailyInfo")]
        [HttpPost]
        public Response<DictionarySpareParts> GetDictionaryDailySpareParts(Request<RepairsActsInfo> req)
        {
            return Perform(() => spareDb.GetDictionaryDailyInfo(req.RequestObject.rep_id, req.RequestObject.dail_id, req.RequestObject.date_start, req.RequestObject.typeoper));
        }

        [Route("GetDayRepairs")]
        [HttpPost]
        public Response<DayTasksModels> GetDayRepairs(Request<GetDayTasksReq> req)
        {
            return Perform(() => spareDb.GetDayRepairs(req.RequestObject));
        }

        //LoadRepairs
        [Route("LoadRepairs")]
        [HttpPost]
        public Response<string> LoadRepairs(Request<GetDayTasksReq> req)
        {
            return Perform(() => spareDb.LoadRepairs(req.RequestObject));
        }



        //подгрузка цен на запчасти в ежедневном
        [Route("GetSparePartCost")]
        [HttpPost]
        public Response<SparePartsModel> GetSparePartCost(Request<SparePartsModel> req)
        {
            return Perform(() => spareDb.GetSparePartCost(req.RequestObject));
        }

        [Route("DeleteDayRep")]
        [HttpPost]
        public Response<string> DeleteDayRep(Request<List<int?>> req)
        {
            return Perform(() => spareDb.DeleteDayRep(req.RequestObject));
        }


        //загрузка номеров актов в ежедневном
        [Route("GetRepairsActs")]
        [HttpPost]
        public Response<List<DictionaryItemsDTO>> GetRepairsActs(Request<RepairsActsInfo> req)
        {
            return Perform(() => spareDb.GetRepairsActs(req.RequestObject.date_start, req.RequestObject.typeoper));
        }
       
        [Route("GetCountGenaralWorks")]
        [HttpGet]
        public Response<DictionarySpareParts> GetCountGenaralWorks()
        {
            return Perform(() => spareDb.GetCountGenaralWorks());
        }
        [Route("SetDailyBanDate")]
        [HttpPost]
        public Response<DateTime> SetDailyBanDate(Request<DateTime> req)
        {
            return Perform(() => spareDb.SetDailyBanDate(req.RequestObject));
        }
        [Route("GetDailyBanDate")]
        [HttpGet]
        public Response<DateTime> GetDailyBanDate()
        {
            return Perform(() => spareDb.GetDailyBanDate());
        }
        [Route("SetDailyBanDateAuto")]
        [HttpGet]
        public Response<DateTime> SetDailyBanDateAuto()
        {
            return Perform(() => spareDb.SetDailyBanDateAuto());
        }
        //сохранение ежедневного
        [Route("CreateUpdateDailyRepairsAct")]
        [HttpPost]
        public Response<DateTime> CreateUpdateDailyRepairsAct(Request<DailyRepairsModel> req)
        {
            return Perform(() => spareDb.CreateUpdateDailyRepairsAct(req.RequestObject));
        }
        //CreateShiftRep
        [Route("CreateShiftRep")]
        [HttpPost]
        public Response<int> CreateShiftRep(Request<UpdateDayTaskReq> req)
        {
            return Perform(() => spareDb.CreateShiftRep(req.RequestObject));
        }

        [Route("CreateShiftDaily")]
        [HttpPost]
        public Response<string> CreateShiftDaily(Request<UpdateDayTaskReq> req)
        {
            return Perform(() => spareDb.CreateShiftDaily(req.RequestObject));
        }

        [Route("UpdateCommentRepTask")]
        [HttpPost]
        public Response<string> UpdateCommentRepTask(Request<GetDayTasksReq> req)
        {
            return Perform(() => spareDb.UpdateCommentRepTask(req.RequestObject));
        }

        //удаление ежедневного
        [Route("DeleteAct")]
        [HttpPost]
        public Response<bool> DeleteAct(Request<TableDataDaily> req)
        {
            return Perform(() => spareDb.DeleteDailyRepairsAct(req.RequestObject));
        }
        //загрузка ежедневного задания в редактор
        [Route("GetDailyRepairsItem")]
        [HttpPost]
        public Response<DailyRepairsModel> GetDailyRepairsItem(Request<EmpTaskInfo> req)
        {
            return Perform(() => spareDb.GetDailyRepairsItem(req.RequestObject));
        }
        //загрузка списка ежедневных заданий в редактор
        [Route("GetDailyRepairsList")]
        [HttpPost]
        public Response<List<DailyRepairsModelList>> GetDailyRepairsList(Request<GridDataReq> req)
        {
            return Perform(() => spareDb.GetDailyRepairsList(req.RequestObject));
        }
        //закрытие ежедневного задания
        [Route("CloseDailyRepairsAct")]
        [HttpPost]
        public Response<bool> CloseDailyRepairsAct(Request<int> req)
        {
            return Perform(() => spareDb.CloseDailyRepairsAct(req.RequestObject));
        }
        ////вывод поступление - просмотр
        [Route("GetSpareParts")]
        [HttpPost]
        public Response<SparePartsListResponse> GetServiceDetails(Request<GridDataReq> req)
        {
            return Perform(() => spareDb.GetSparePartsDetails(req.RequestObject));
        }
        ////вывод резерв - просмотр
        [Route("GetReserveSpareParts")]
        [HttpPost]
        public Response<SparePartsReserveListResponse> GetReserveSpareParts(Request<GridDataReq> req)
        {
            return Perform(() => spareDb.GetReserveListSpareParts(req.RequestObject));
        }
        ////вывод списание - просмотр
        [Route("GetWriteOffSpareParts")]
        [HttpPost]
        public Response<SparePartsWriteOffListResponse> GetWriteOffSpareParts(Request<GridDataReq> req)
        {
            return Perform(() => spareDb.GetWriteOffListSpareParts(req.RequestObject));
        }
        ////удалить приход
        [Route("DeleteInvoice")]
        [HttpPost]
        public Response<string> DeleteInvoiceAct(Request<List<DelRow>> req)
        {
            return Perform(() => spareDb.DeleteInvoiceAct(req.RequestObject));
        }
        [Route("UpdateSpareParts")]
        [HttpPost]
        public Response<int> UpdateSpareParts(Request<List<SaveSpareParts>> req)
        {
            return Perform(() => spareDb.UpdateSpareParts(req.RequestObject));
        }
        [Route("GetDictionarySpareParts")]
        [HttpPost]
        public Response<DictionarySpareParts> GetDictionarySpareParts(Request<bool> req)
        {
            return Perform(() => spareDb.GetDictionarySpareParts());
        }
        [Route("GetHistorySpareParts")]
        [HttpPost]
        public Response<List<DictionaryItemsDTO>> GetHistorySpareParts(Request<string> req)
        {
            return Perform(() => spareDb.GetHistorySpareParts(req.RequestObject));
        }
        [Route("GetDictionaryRepairs")]
        [HttpPost]
        public Response<DictionaryRepairs> GetDictionaryRepairs(Request<SparePartsDetails> req)
        {
            return Perform(() => spareDb.GetDictionaryRepairs(req.RequestObject));
        }
        //отмена списания
        [Route("ChangeStatusRepair")]
        [HttpPost]
        public Response<string> ChangeStatusRepair(Request<SparePartsDetails> req)
        {
            return Perform(() => spareDb.ChangeStatusRepair(req.RequestObject));
        }

        //склады с баланса
        [Route("GetDictionarySparePartsBalance")]
        [HttpPost]
        public Response<DictionarySpareParts> GetDictionarySparePartsBalance(Request<bool> req)
        {
            return Perform(() => spareDb.GetDictionarySparePartsBalance());
        }
        //приходная накладная
        [Route("UpdateInvoiceGeneralInfo")]
        [HttpPost]
        public Response<int> UpdateInvoiceGeneralInfo(Request<SparePartsDetails> req)
        {
            return Perform(() => spareDb.UpdateInvoiceGeneralInfo(req.RequestObject));
        }
        //акт поступления
        [Route("CreateUpdateInvoiceDetail")]
        [HttpPost]
        public Response<int> CreateUpdateInvoiceDetail(Request<List<SaveRowSpareParts>> req)
        {
            return Perform(() => spareDb.CreateUpdateInvoiceDetail(req.RequestObject));
        }
        // список актов поступления
        [Route("GetInvoiceActsByDate")]
        [HttpPost]
        public Response<SparePartsInvoiceListResponse> GetInvoiceActsByDate(Request<GridDataReq> req)
        {
            return Perform(() => spareDb.GetInvoiceActsByDateDetails(req.RequestObject));
        }
        // список нарядов на ремонт
        [Route("GetRepairsActsByDate")]
        [HttpPost]
        public Response<SparePartsRepairListResponse> GetRepairsActsByDate(Request<GridDataReq> req)
        {
            return Perform(() => spareDb.GetRepairsActsByDateDetails(req.RequestObject));
        }
        // список актов c списания
        [Route("GetWriteOffActsByDate")]
        [HttpPost]
        public Response<List<SparePartsWriteOffModel>> GetWriteOffActsByDate(Request<DateTime> req)
        {
            return Perform(() => spareDb.GetWriteOffActsByDate(req.RequestObject));
        }
        //список требований накладных
        [Route("GetDemandInvoiceByDate")]
        [HttpPost]
        public Response<List<SparePartsWriteOffModel>> GetDemandInvoiceByDate(Request<SparePartsDetails> req)
        {
            return Perform(() => spareDb.GetDemandInvoiceByDate(req.RequestObject));
        }
        //по требованию-накладной данные
        [Route("GetDemandInvoiceDetails")]
        [HttpPost]
        public Response<WriteOffActsDetails> GetDemandInvoiceDetails(Request<SparePartsDetails> req)
        {
            return Perform(() => spareDb.GetDemandInvoiceDetails(req.RequestObject));
        }
        //
        //данные по акту поступления
        [Route("GetInvoiceDetails")]
        [HttpPost]
        public Response<SparePartsDetails> GetInvoiceDetails(Request<int> req)
        {
            return Perform(() => spareDb.GetActInvoiceDetails(req.RequestObject));
        }
        //заполнить таблицу запчастей
        [Route("GetInvoiceDetailsForRepair")]
        [HttpPost]
        public Response<SparePartsDetails> GetInvoiceDetailsForRepair(Request<SparePartsModel> req)
        {
            return Perform(() => spareDb.GetInvoiceDetailsForRepair(req.RequestObject));
        }

        //данные по акту списания 
        [Route("GetWriteOffDetails")]
        [HttpPost]
        public Response<WriteOffActsDetails> GetWriteOffDetails(Request<int> req)
        {
            return Perform(() => spareDb.GetWriteOffDetails(req.RequestObject, null));
        }
        [Route("UpdateInvoiceAct")]
        [HttpPost]
        public Response<string> UpdateInvoiceAct(Request<List<SaveRowSpareParts>> req)
        {
            return Perform(() => spareDb.UpdateInvoiceAct(req.RequestObject));
        }
        //удаляет транзакции из актов поступления
        [Route("DeleteInvoiceAct")]
        [HttpPost]
        public Response<string> DeleteInvoiceTransactions(Request<List<SparePartsModel>> req)
        {
            return Perform(() => spareDb.DeleteInvoiceTransactions(req.RequestObject));
        }
       // баланс
        [Route("GetBalanceSpareList")]
        [HttpPost]
        public Response<List<SparePartsModel>> GetBalanceSpareList(Request<int> req)
        {
            return Perform(() => spareDb.GetBalanceSpareList(req.RequestObject));
        }
        // история запчасти приход-расход
        [Route("GetHistorySpareList")]
        [HttpPost]
        public Response<HistorySpareParts> GetHistorySpareList(Request<int> req)
        {
            return Perform(() => spareDb.GetHistorySpareList(req.RequestObject));
        }


        // перемещение сохранение
        [Route("SaveMovementSpareParts")]
        [HttpPost]
        public Response<string> SaveMovementSpareParts(Request<List<SparePartsMovementModel>> req)
        {
            return Perform(() => spareDb.SaveMovementSpareParts(req.RequestObject));
        }
        // перемещение вывод
        [Route("GetMovementSpareParts")]
        [HttpPost]
        public Response<SparePartsMovementListResponse> GetMovementSpareParts(Request<GridDataReq> req)
        {
            return Perform(() => spareDb.GetSparePartsMovementDetails(req.RequestObject));
        }
        // перемещение обновление
        [Route("UpdateMovementSpareParts")]
        [HttpPost]
        public Response<int> UpdateMovementSpareParts(Request<List<SaveMoveSpareParts>> req)
        {
            return Perform(() => spareDb.UpdateMovementSpareParts(req.RequestObject));
        }
        //удалить перемещение
        [Route("DeleteMovementSpareParts")]
        [HttpPost]
        public Response<string> DeleteMovementSpareParts(Request<List<DelMoveRow>> req)
        {
            return Perform(() => spareDb.DeleteMovementSpareParts(req.RequestObject));
        }
        //цена с баланса
        [Route("GetPriceCountBalance")]
        [HttpPost]
        public Response<List<SparePartsModel>> GetPriceCountBalance(Request<SparePartsModel> req)
        {
            return Perform(() => spareDb.GetPriceCountBalance(req.RequestObject));
        }
        //склады с баланса
        [Route("GetListStorageSpareParts")]
        [HttpPost]
        public Response<List<DictionaryItemsDTO>> GetListStorageSpareParts(Request<SparePartsModel> req)
        {
            return Perform(() => spareDb.GetListStorageSpareParts(req.RequestObject));
        }
        //запчасти в наряды
        [Route("GetListRepairsSpareParts")]
        [HttpPost]
        public Response<List<DictionaryItemsDTO>> GetListRepairsSpareParts(Request<SparePartsRepairs> req)
        {
            return Perform(() => spareDb.GetListRepairsSpareParts(req.RequestObject));
        }
        //код для техники
        [Route("GetUniqCodeForTech")]
        [HttpPost]
        public Response<string> GetUniqCodeForTech(Request<SparePartsRepairs> req)
        {
            return Perform(() => spareDb.GetUniqCodeForTech(req.RequestObject));
        }

        //запчасти в наряды - оборудование
        [Route("GetListEquipSpareParts")]
        [HttpPost]
        public Response<List<DictionaryItemsDTO>> GetListEquipSpareParts(Request<SparePartsRepairs> req)
        {
            return Perform(() => spareDb.GetListEquipSpareParts(req.RequestObject));
        }
        //перезапись наряда на ремонт
        [Route("CreateUpdateRepairsDetail")]
        [HttpPost]
        public Response<string> CreateUpdateRepairsDetail(Request<CreateUpdateRepairs> req)
        {
            return Perform(() => spareDb.CreateUpdateRepairsDetail(req.RequestObject));
        }
        //данные по наряду на ремонт
        [Route("GetRepairsDetails")]
        [HttpPost]
        public Response<CreateUpdateRepairs> GetRepairsDetails(Request<GeneralInfo> req)
        {
            return Perform(() => spareDb.GetRepairsDetails(req.RequestObject));
        }
           //обновить дату открытия
        [Route("SaveRepairsDateOpen")]
        [HttpPost]
        public Response<String> SaveRepairsDateOpen(Request<int?> req)
        {
            return Perform(() => spareDb.SaveRepairsDateOpen(req.RequestObject));
        }
        //удалить наряд на ремонт
        [Route("DeleteRepairs")]
        [HttpPost]
        public Response<int> DeleteRepairs(Request<int> req)
        {
            return Perform(() => spareDb.DeleteRepairs(req.RequestObject));
        }
        [Route("GetPrintRepairsInfo")]
        [HttpPost]
        public Response<PrintRepairsModel> GetPrintRepairsInfo(Request<int> req)
        {
            return Perform(() => spareDb.GetPrintRepairsInfo(req.RequestObject));
        }

        //печать требование-накладная
        [Route("GetPrintDemandInfo")]
        [HttpPost]
        public Response<WriteOffActsDetails> GetPrintDemandInfo(Request<GeneralInfo> req)
        {
            return Perform(() => spareDb.GetPrintDemandInfo(req.RequestObject.ActId, req.RequestObject.RepairNumber,req.RequestObject.dateStart));
        }

        [Route("GetPrintWriteoffInfo")]
        [HttpGet]
        public HttpResponseMessage GetPrintWriteoffInfo(int req, string act_number, DateTime? date)
        {
            var checkType = act_number.Split('/')[0]; var isReq = 0;
            var templatePath = "";
            try
            {
                if (checkType.Equals("М") || date != null || act_number.Equals("MoveAct"))
                {
                    templatePath = HttpContext.Current.Server.MapPath("~/ExcelReports/WriteOffSparePartsTwoTemplate.xlsx");
                    isReq = 1;
                }
                else
                {
                     templatePath = HttpContext.Current.Server.MapPath("~/ExcelReports/WriteoffActTemplate.xlsx");
                }
                var report = spareDb.GetPrintWriteoffInfo(req, templatePath, isReq,  date,  act_number);
                var isIe = Request.Headers.UserAgent.ToString().Contains("Trident");
                HttpResponseMessage result = new HttpResponseMessage(HttpStatusCode.OK);
                if ((report.data != null))
                {
                    var stream = new MemoryStream(report.data);
                    var FileName = report.fileName;
                    result.Content = new StreamContent(stream);
                    result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/vnd.ms-excel");
                    result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
                    {
                        FileName = isIe ? HttpUtility.UrlEncode(FileName) : (FileName)
                    };
                }
                else
                {
                    result.StatusCode = HttpStatusCode.NoContent;
                }
                return result;
            }
            catch (Exception ex)
            {
                HttpResponseMessage errorResult = new HttpResponseMessage(HttpStatusCode.BadRequest);
                errorResult.ReasonPhrase = ex.ToString();
                return errorResult;
            }
        }
    }


}