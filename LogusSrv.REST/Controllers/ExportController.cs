﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using LogusSrv.CORE.REST;
using System.Net.Http.Headers;
using System.Xml;
using System.IO;
using System.Text;

using System.Data.SqlClient;
using System.Data.OleDb;
using System.Data.Odbc;
using System.Data.Common;
using System.Configuration;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;

using LogusSrv.CORE.Exceptions;
using LogusSrv.DAL.DBModel;
using LogusSrv.DAL.Entities.DTO;
using LogusSrv.DAL.Entities.DTO.DayTaskExt;
using LogusSrv.DAL.Entities.Res;
using LogusSrv.DAL.Entities.Req;
using LogusSrv.DAL.Enums;
using LogusSrv.DAL.Utils;

using LogusSrv.DAL.Entities.SQL;


namespace LogosSrv.Controllers
{
    [RoutePrefix("api/Export")]
    public class ExportController : BaseCtrl
    {
        protected readonly LogusDBEntities db = new LogusDBEntities();
        [Route("ExportXML")]
        [HttpGet]
        public HttpResponseMessage ExportXML(DateTime? startDate = null, DateTime? endDate = null, int? exportType = null)
        {
            HttpResponseMessage result = new HttpResponseMessage(HttpStatusCode.OK);

            string bum = getMechsXML((DateTime)startDate, (DateTime)endDate, (exportType == 1) ? 1 : 2);

            result.Content = new StringContent(bum);
            result.Content.Headers.ContentType = new MediaTypeHeaderValue("text/xml");
            result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
            {
                FileName = ((exportType == 1)? "mechs.xml" : "drivers.xml")
            };
            return result;
        }

        protected string getMechsXML(DateTime sDate, DateTime eDate, int exType)
        {
            var ml = InitMainArray(sDate,eDate, exType);

            MemoryStream mStream = new MemoryStream();
            XmlTextWriter writer = new XmlTextWriter(mStream, Encoding.UTF8);

            writer.Formatting = Formatting.Indented;
            writer.WriteStartDocument();
            writer.WriteStartElement("Root");

            foreach (var m in ml)
            {
                Decimal cons = 0;
                writer.WriteStartElement("DocumentObject");
                writer.WriteElementString("Id", m.shtr_id.ToString());
                writer.WriteElementString("ВидПутевогоЛиста", exType.ToString());
                writer.WriteElementString("ПометкаУдал", "false");
                writer.WriteElementString("ПометкаМод", "false");
                writer.WriteElementString("ДатаПутевогоЛиста", m.dbegin.ToString());

                if (exType == 1) {
                    writer.WriteElementString("Агрегат", getAggregate(m.trk_code, m.eq_code));
                    writer.WriteElementString("АгрегатИмя", getAggregateName(m.trk_code, m.eq_code));  //1
                    writer.WriteElementString("СхТехника", m.trk_im_code);
                    writer.WriteElementString("СхТехникаИмя", m.trk_num); //2
                    writer.WriteElementString("Механизатор", m.mech_code);
                    writer.WriteElementString("МеханизаторИмя", m.short_name_mech);
                }
                else {
                    writer.WriteElementString("Автомобиль", m.trk_im_code);
                    writer.WriteElementString("АвтомобильИмя", m.trk_num);
                    writer.WriteElementString("Прицеп1", m.os_code);
                    writer.WriteElementString("Прицеп2", "");
                    writer.WriteElementString("Агрегат", getAggregate(m.trk_code, m.eq_code));
                    writer.WriteElementString("АгрегатИмя", getAggregateName(m.trk_code, m.eq_code));
                    writer.WriteElementString("Водитель", m.mech_code);
                    writer.WriteElementString("ВодительИмя", m.short_name_mech);
                }
                
                writer.WriteElementString("ПодразделениеОрганизации", "e6322333-ee42-11e4-80c6-002590ebf8cd");
                writer.WriteElementString("ПодразделениеОрганизацииИмя", "Бригада механизаторов");

                var fuel_id = db.Sheet_tracks.Where(t => t.shtr_id == m.shtr_id).Select(t => t.id_type_fuel).FirstOrDefault();
                var code_fuel = db.Type_fuel.Where(t => t.id == fuel_id).Select(t => t.code).FirstOrDefault();
                var name_fuel = db.Type_fuel.Where(t => t.id == fuel_id).Select(t => t.name).FirstOrDefault();

                writer.WriteElementString("Горючее", code_fuel);
                writer.WriteElementString("ГорючееИмя", name_fuel);
                writer.WriteElementString("ДатаСдачиПутевогоЛиста", m.dend.ToString());

                if (exType == 2) {
                    writer.WriteElementString("ДатаВыездаИзГаража", m.dbegin.ToString());
                    writer.WriteElementString("ДатаВозвращениеВГараж", m.dbegin.ToString());
                }

                writer.WriteElementString("НомерПутевогоЛиста", m.num_sheet);
                writer.WriteElementString("Ответственный", m.otv);
                writer.WriteElementString("СкладБак", m.trk_skld);
                writer.WriteElementString("СкладБакИмя", m.trk_num);

                if (exType == 1) writer.WriteElementString("ПоказанияСчетчикаМоточасовВозвращение", m.sm.ToString());
                else writer.WriteElementString("ПоказанияОдометра", m.sm.ToString());

                var wr = InitWorkArray(m.shtr_id);
                if (wr.Count > 0) 
                {
                    writer.WriteElementString("Комментарий", getComment(m, wr));
                    writer.WriteStartElement("ВыполненныеРаботы");
                    foreach(var w in wr)
                    {
                        writer.WriteStartElement("Row");
                        writer.WriteElementString("ВидРабот", w.in_code);
                        writer.WriteElementString("ВидРаботИмя", w.task_name);
                        writer.WriteElementString("ХарактеристикаРабот","bc9d1f2e-9d53-11e4-80bf-002590ebf8cd");
                        writer.WriteElementString("Субконто1",w.subk);
                        var name_plant = db.Plants.Where(t => t.plant_id == w.plant_plant_id).Select(t => t.name).FirstOrDefault();
                        writer.WriteElementString("Субконто1Имя", name_plant);
                        if (exType == 1)
                        {
                            writer.WriteElementString("Поле", w.porg);
                            writer.WriteElementString("ПолеИмя", w.porg_name);
                        }

                        writer.WriteElementString("ПериодС", ((w.dbegin == "")? m.dbegin : w.dbegin));
                        writer.WriteElementString("ПериодПо", ((w.dend == "") ? m.dend : w.dend));
                        writer.WriteElementString("ОтработаноДней", w.workdays.ToString());
                        writer.WriteElementString("ОтработаноЧасов", w.worktime.ToString());
                        writer.WriteElementString("ОтработаноНочныеЧасы", "0");
                        if (exType == 1) writer.WriteElementString("ПродолжительностьСмены", "0");
                        writer.WriteElementString("ВыполненоРабот", ((w.volume_fact == 0) ? "1" : w.volume_fact.ToString()));
                        if (exType == 1) writer.WriteElementString("ВыполненоРаботУГА", ((w.area == 0) ? "1" : w.area.ToString()));
                        writer.WriteElementString("КоличествоЕздок", "1");

                        Decimal sal1 = 0, sal2 = 0;

                        sal1 = w.workdays * w.salary;
                        sal2 = w.total_salary - sal1;

                        /*
                        if (exType == 1)
                        {
                            switch (Decimal.ToInt32(w.type_price))
                            {
                                case 1: sal1 = w.workdays * w.salary; break;
                                case 2: sal1 = w.area * w.salary; break;
                                case 3: sal1 = w.volume_fact * w.salary; break;
                                default: sal1 = 0; break;
                            }

                            if (w.price_add != 0)
                            {
                                if ((Decimal.ToInt32(w.type_price) != 3) && (w.volume_fact != 0))
                                {
                                    sal2 = w.volume_fact * w.price_add;
                                }
                                if ((Decimal.ToInt32(w.type_price) != 2) && (w.area != 0))
                                {
                                    sal2 = w.area * w.price_add;
                                }
                            }
                        }
                        else
                        {
                            switch((int)w.type_price)  
                            {
                                case 0:  sal1 = 0;
                                         break;
                                case 1:  sal1 = w.workdays*w.salary;   
                                         break;
                                case 5:  sal1 = w.worktime*w.salary;
                                         break;
                                case 4:  sal1 = w.mileage_total*w.salary;     
                                         break;
                                default: sal1 = 0;
                                         break;
                            }
                            sal2 = w.price_add * w.volume_fact;
                        }    */

                      writer.WriteElementString("НачисленоПоРасценкам", sal1.ToString());
                      writer.WriteElementString("НачисленоДоплаты", sal2.ToString());

                      writer.WriteEndElement(); // Row
                      cons += w.cons;
                    }
                    writer.WriteEndElement(); // ВыполненныеРаботы
                }
                else
                {
                    writer.WriteElementString("Комментарий",getComment(m));
                }

                writer.WriteStartElement("ДвижениеГСМ");

                
                if (cons != 0)
                {
                    writer.WriteStartElement("Row");
                    writer.WriteElementString("ГСМ",(m.fuelcod == "")? "4acaa0d9-01c9-11df-8b06-00195b3811bd" : m.fuelcod);
                    writer.WriteElementString("ГСМИмя", name_fuel);
                    writer.WriteElementString("ВидДвиженияГСМ","ФактическийРасход");
                    writer.WriteElementString("Склад","00000000-0000-0000-0000-000000000000");
                    writer.WriteElementString("СкладИмя", m.trk_num);
                    writer.WriteElementString("Количество",cons.ToString());
                    writer.WriteEndElement(); // Row

                    var rf = InitRefArray(m.shtr_id);
                    foreach(var r in rf)
                    {
                        writer.WriteStartElement("Row");
                        writer.WriteElementString("ГСМ", (m.fuelcod == "") ? "4acaa0d9-01c9-11df-8b06-00195b3811bd" : m.fuelcod);

                        if (r.emp_emp_id == 0)
                        {
                            writer.WriteElementString("ВидДвиженияГСМ", "ЗаправкаПоЧеку");
                        }
                        else
                        {
                            writer.WriteElementString("ВидДвиженияГСМ", "ЗаправкаСоСклада");
                            string sk="";
                            switch(r.emp_emp_id)
                            {
                                case 89: sk = "695aec0b-01c9-11df-8b06-00195b3811bd";
                                    break;
                                case 90: sk = "d7b342b1-5e60-11df-ab93-000e0ce57eec";
                                    break;
                                case 91: sk = "ad5c175c-0af6-11e3-86dc-005056c00008";
                                    break;
                            }
                            var nameEmp = db.Employees.Where(t => t.emp_id == r.emp_emp_id).Select(t => t.short_name).FirstOrDefault();


                            writer.WriteElementString("Склад", sk);
                            writer.WriteElementString("СкладИмя", nameEmp);
                        }
                        writer.WriteElementString("Количество", r.volume.ToString());
                       
                        writer.WriteEndElement(); // Row
                    }
                }

                writer.WriteEndElement(); // Движение ГСМ
                writer.WriteEndElement(); //DocumentObject
            }
            writer.WriteEndElement(); //Root

            writer.Flush();
            mStream.Flush();
            mStream.Position = 0;
            StreamReader sReader = new StreamReader(mStream);
            String FormattedXML = sReader.ReadToEnd();

            return FormattedXML;
        }

        protected List<ExportResultMainSet> InitMainArray(DateTime sDate, DateTime eDate, int exType)
        {
            var checkSheet = db.Sheet_tracks.Where(t => t.shtr_type_id == exType && t.date_begin >= sDate && t.date_end <= eDate).Count();
            if (checkSheet == 0) {
                var list = new List<ExportResultMainSet>();
                return list;
            }
             string query = @"SELECT  shtr_id, 
                                      replace(replace(replace(convert(varchar,shtr.date_begin,120),':',''),'-',''),' ','') as dbegin,
                                      trk.code as trk_code,
						              trk.name as trk_name,
                                      trk.reg_num as trk_num,  
                                      trk.im_code as trk_im_code,
                                      trk.int_id as trk_skld,
                                      (select code from Equipments where equi_id = shtr.equi_equi_id) eq_code,
                                      (select os_code from Equipments where equi_id = shtr.equi_equi_id) os_code,
						              (select name from Equipments where equi_id = shtr.equi_equi_id) eq_name,
                                      emp_mech.code as mech_code,
						              emp_mech.short_name as short_name_mech,
                                      replace(replace(replace(convert(varchar,shtr.date_end,120),':',''),'-',''),' ','') as dend,                        
                                      shtr.num_sheet as num_sheet,
                                     (select short_name from Employees e, Users u where e.emp_id = u.emp_emp_id and u.user_id = shtr.user_user_id) otv,
                                     isnull(moto_end,0) - isnull(moto_start,0) sm,
                                     isnull(shtr.exported,0) as exported,
                                     (select code from Type_fuel where id = shtr.id_type_fuel) as fuelcod
                                  FROM Sheet_tracks shtr LEFT JOIN Traktors trk on shtr.trakt_trakt_id = trk.trakt_id,
                                       Organizations org,
                                       Employees emp_mech			
                                  WHERE shtr.org_org_id = org.org_id  
                                    AND emp_mech.emp_id = shtr.emp_emp_id   
				                    AND shtr.status = 4 
				                    AND shtr.shtr_type_id = " + exType.ToString() +
                                    " AND (shtr.exported in (0,2) or shtr.exported IS NULL)" +
                                    " and shtr.date_begin >= '" + sDate.ToString("yyyy.dd.MM") + "'" +
                                    " and shtr.date_end <= '" + eDate.AddDays(1).ToString("yyyy.dd.MM") + "'" +
                                 " order by shtr.shtr_id";
            var dc = new LogusDBEntities();
            return dc.Database.SqlQuery<ExportResultMainSet>(query).ToList();
        }

        protected string getAggregate(string trak, string equi)
        {
            string query = "select agg_code as value from Aggregates where trak_code = '"+trak+"' and equi_code = '"+equi+"'";
            var dc = new LogusDBEntities();
            var list = dc.Database.SqlQuery<ExportStringValue>(query).ToList();

            if (list.Count > 0) return list[0].value;
            else return "";
        }

        protected string getAggregateName(string trak, string equi)
        {
            var name = db.Aggregates.AsEnumerable().Where(t => t.trak_code != null && t.equi_code != null &&
                t.trak_code.Equals(trak) && t.equi_code.Equals(equi)).Select(t => t.name).FirstOrDefault();
            return name;
        }




        protected List<ExportResultWorkSet> InitWorkArray(int shtr_id)
        {
            // (SELECT in_code from Fields_to_plants WHERE fielplan_id = t.plant_plant_id) as subk
            string query = @"SELECT isnull((select top 1 code from Type_Tasks_to_Plants tttp where tttp.tptas_tptas_id = tt.tptas_id and tttp.plant_plant_id = t.plant_plant_id),tt.in_code) as in_code, 
	                                tt.name as task_name,
                                    (select x.code from Plants x ,Fields_to_plants f  where x.plant_id=f.plant_plant_id and f.fielplan_id=t.plant_plant_id) as subk,
                                    replace(replace(replace(convert(varchar,isnull(t.date_begin,(select date_begin from Sheet_tracks where shtr_id = t.shtr_shtr_id)),120),':',''),'-',''),' ','') as dbegin,
                                    replace(replace(replace(convert(varchar,isnull(t.date_end,(select date_end from Sheet_tracks where shtr_id = t.shtr_shtr_id)),120),':',''),'-',''),' ','') as dend,                                      
                                    isnull(t.price_days,0) price_days,
                                    isnull(t.worktime,
                                           (datediff(hh, isnull(t.date_begin,(select date_begin from Sheet_tracks where shtr_id = t.shtr_shtr_id)), 
										                 isnull(t.date_end,(select date_end from Sheet_tracks where shtr_id = t.shtr_shtr_id))))) worktime,
                                    (SELECT code from Fields where fiel_id = t.fiel_fiel_id) porg,
					                (SELECT name from Fields where fiel_id = t.fiel_fiel_id) porg_name,
                                    (SELECT plant_plant_id from Fields_to_plants where fielplan_id = t.plant_plant_id) plant_plant_id,
 					                isnull(t.consumption_fact,0) cons,
   	  	  			                isnull(t.type_price,1) type_price,
          			                isnull(t.workdays,0) workdays,
      				                isnull(t.salary,0) salary,
   					                isnull(t.area,1) area,
   					                isnull(t.volume_fact,1) volume_fact,
   					                isnull(t.price_add,0) price_add,
                                    isnull(t.mileage_total,0) mileage_total,
                                    isnull(t.total_salary,0) total_salary
                               FROM Tasks t,   
                                    Type_tasks tt
                              WHERE tt.tptas_id = t.tptas_tptas_id 
                                AND shtr_shtr_id = " + shtr_id.ToString();
            var dc = new LogusDBEntities();
            return dc.Database.SqlQuery<ExportResultWorkSet>(query).ToList();
        }

        protected List<ExportRefuellingSet> InitRefArray(int shtr_id)
        {
            string query = @"select isnull(emp_emp_id,0) emp_emp_id,
                                    order_num,
                                    isnull(volume,0) volume    
                                    from Refuelings where shtr_shtr_id = " + shtr_id.ToString() + " order by order_num";
            var dc = new LogusDBEntities();
            return dc.Database.SqlQuery<ExportRefuellingSet>(query).ToList();
        }
        protected string getComment(ExportResultMainSet MainV, List<ExportResultWorkSet> MainW = null)
        {
            string result = "";

            if (getAggregate(MainV.trk_code, MainV.eq_code)=="")
            {
                if (MainV.eq_name != "")
                {
                    result = string.Format("Нет связки в агрегатах для {0} {1} ({2}) с оборудованием {2} ({3}) ",
                                            MainV.trk_name,
                                            MainV.trk_num,
                                            MainV.trk_code,
                                            MainV.eq_name,
                                            MainV.eq_code);
                }
                else
                {
                    result = string.Format("Нет кода в агрегатах для {0} {1} ({2}) (без оборудования) ",
                                            MainV.trk_name, MainV.trk_num, MainV.trk_code);
                }
            }

            if (MainV.trk_im_code == "") result += string.Format("Нет кода в ОС для {0} {1}", MainV.trk_name, MainV.trk_num);
            if (MainV.mech_code == "") result += string.Format("Нет кода для сотрудника {0} ", MainV.short_name_mech);
                
            if (MainW != null)
                foreach(var MW in MainW)
                    {
                        if (MW.porg == "") result += string.Format("Нет кода для поля {0} ", MW.porg_name);
                        if (MW.in_code == "") result += string.Format("Нет кода для работы {0} ", MW.task_name);
                    }
            return result;
        }

    }

    


}

