﻿using System;
using System.Text;
using System.Runtime.Serialization.Formatters.Binary; 
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Data.SqlClient;
using System.Net.Http;
using System.Web.Http;
using LogusSrv.CORE.REST;
using LogusSrv.DAL.Operations;
using LogusSrv.DAL.DBModel;
using LogusSrv.DAL.Entities.Req;
using LogusSrv.DAL.Entities.DTO;
using System.Configuration;
using LogusSrv.DAL.Entities.Res;
using System.Web;
using System.IO;
//using LogusSrv.GPSDal;

namespace LogosSrv.Controllers
{
    [RoutePrefix("api/REST")]
    public class LogusController : BaseCtrl
    {

        public GPSSocketDb gpsSocker = new GPSSocketDb();
        public LayersOperationDb layer = new LayersOperationDb();
        public LoginDb loginDb = new LoginDb();
        private readonly DayTasksDb dayTaskDb = new DayTasksDb();
        private readonly GpsFromXmlDb gpsFromXml = new GpsFromXmlDb();
       // private GpsParse gpsParse = new GpsParse();

        // 1 сокет старый , вроде уже не используется от ТреКинга
        [Route("CoordinateTraktor")]
        [HttpPost]
        public List<GPS> CoordinateTraktor(GPSmodel traktor)
        {
            return gpsSocker.GetCoordinatesByTraktorId(traktor.id);
        }
        // 1 сокет старый , вроде уже не используется от ТреКинга
        [Route("start")]
        [HttpGet]
        public string start()
        {
            return gpsSocker.StartGPSTimer();
        }
        // 1 сокет старый , вроде уже не используется от ТреКинга
        [Route("stop")]
        [HttpGet]
        public string stop()
        {
            return gpsSocker.StopGPSTimer();
        }

        [Route("calcArea")]
        [HttpPost]
        public string calc(GPSmodel traktor)
        {
            layer.calcThreadedArea(traktor.id_track , traktor.date, false);
            return "calc";
        }

        [Route("Login")]
        [HttpPost]
        public Response<UserModel> Login(Request<LoginReq> req)
        {
            var saltPostfix = ConfigurationManager.AppSettings["SaltPostfix"];
            return Perform(() => loginDb.Login(req.RequestObject.Login, req.RequestObject.Password, saltPostfix));
        }

        //===
        [Route("CheckKey")]
        [HttpPost]
        public Response<int> CheckKey()
        {
            return Perform(() => loginDb.CheckKey());
        }

        [Route("ActivateSite")]
        [HttpPost]
        public Response<String> ActivateSite(Request<GetDayTasksReq> req)
        {
            return Perform(() => loginDb.ActivateSite(req.RequestObject.Comment));
        }


        [Route("GetOgranizationList")]
        [HttpGet]
        public Response<List<DictionaryItemsDTO>> Login(Request<int> req)
        {
            return Perform(() => dayTaskDb.GetOgranizationList());
        }

        [Route("GetPass")]
        [HttpPost]
        public string GetPass(LoginReq login)
        {
            string saltPostfix = ConfigurationManager.AppSettings["SaltPostfix"].ToString();
            return loginDb.GetHash(login.Password, saltPostfix);
        }

        
        [Route("ParseXml")]
        [HttpPost]
        public bool ParseXml(byte[] xml)
         {
             var stream = new MemoryStream(xml);
             var xmlPath = HttpContext.Current.Server.MapPath("~/ExcelReports/example.xml");
          // return gpsFromXml.ParseXML(stream);
             return gpsFromXml.ParseXML(xmlPath);
          
        }
        /// <summary>
        /// сервис парсинг xml (с хранение xml координат в отделной таблице на азуре (старый 2))
        /// </summary>
        /// <returns></returns>
     /*   [Route("startParseGps")]
        [HttpGet]
        public string startParseGps()
        {
            return gpsParse.StartGPSTimer();
        }

        [Route("setIntervalTimerParse")]
        [HttpGet]
        public int setIntervalTimer(int? interval = null)
        {
            return gpsParse.setIntervalTimer((int)interval);
        }

        [Route("stopParseGps")]
        [HttpGet]
        public string stopGpsSrv()
        {
            return gpsParse.StopGPSTimer();
        } */
    }
}