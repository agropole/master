﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using LogusSrv.CORE.REST;
using LogusSrv.DAL.Operations;
using LogusSrv.DAL.Entities.Res;
using LogusSrv.DAL.Entities.Req;
using LogusSrv.DAL.Entities.DTO;
using LogusSrv.DAL.Entities.DTO.ActsSzr;

namespace LogosSrv.Controllers
{
    [RoutePrefix("api/ActsSzrCtrl")]
    public class ActsSzrController : BaseCtrl
    {
        public ActsSrzDb _actSrzDb = new ActsSrzDb();
        [Route("GetActsLists")]
        [HttpPost]
        public Response<GetActsSzrListResponse> GetActsLists(Request<GridDataReq> req)
        {
            return Perform(() => _actSrzDb.GetActsLists(req.RequestObject));
        }

        [Route("GetActSzrDetails")]
        [HttpPost]
        public Response<ActSzrDetails> GetActMaterialsDetails(Request<int> req)
        {
            return Perform(() => _actSrzDb.GetActMaterialsDetails(req.RequestObject));
        }

        //save act writeOff
        [Route("CreateUpdateActSzrDetail")]
        [HttpPost]
        public Response<int> CreateUpdateActSzrDetail(Request<List<SaveRow>> req)
        {
            return Perform(() => _actSrzDb.CreateUpdateActSzrDetail(req.RequestObject));
        }
        [Route("UpdateGeneralInfo")]
        [HttpPost]
        public Response<int> UpdateGeneralInfo(Request<ActSzrDetails> req)
        {
            return Perform(() => _actSrzDb.UpdateGeneralInfo(req.RequestObject));
        }

        [Route("GetDictionaryTmc")]
        [HttpPost]
        public Response<DictionarySpareParts> GetDictionaryTmc(Request<GridDataReq> req)  
        {
            return Perform(() => _actSrzDb.GetDictionaryTmc(req.RequestObject));
        }
        //coming
        [Route("GetTmcDetails")]
        [HttpPost]
        public Response<SparePartsDetails> GetTmcDetails(Request<int> req)
        {
            return Perform(() => _actSrzDb.GetActTmcDetails(req.RequestObject));
        }

        [Route("GetTmcMoveDetails")]
        [HttpPost]
        public Response<SparePartsDetails> GetTmcMoveDetails(Request<int> req)
        {
            return Perform(() => _actSrzDb.GetActTmcMoveDetails(req.RequestObject));
        }

        [Route("UpdateTmcAct")]
        [HttpPost]
        public Response<string> UpdateTmcAct(Request<List<SaveRowSpareParts>> req)
        {
            return Perform(() => _actSrzDb.UpdateTmcAct(req.RequestObject));
        }
        //приходная накладная
        [Route("UpdateTmcGeneralInfo")]
        [HttpPost]
        public Response<int> UpdateTmcGeneralInfo(Request<SparePartsDetails> req)
        {
            return Perform(() => _actSrzDb.UpdateTmcGeneralInfo(req.RequestObject));
        }

        //акт поступления
        [Route("CreateUpdateTmcDetail")]
        [HttpPost]
        public Response<int> CreateUpdateTmcDetail(Request<List<SaveRowSpareParts>> req)
        {
            return Perform(() => _actSrzDb.CreateUpdateTmcDetail(req.RequestObject));
        }

        //перемещение
        [Route("CreateUpdateTmcMove")]
        [HttpPost]
        public Response<int> CreateUpdateTmcMove(Request<List<SaveRowSpareParts>> req)
        {
            return Perform(() => _actSrzDb.CreateUpdateTmcMove(req.RequestObject));
        }

        ////удалить приход
        [Route("DeleteTmc")]
        [HttpPost]
        public Response<string> DeleteTmcAct(Request<List<DelRow>> req)
        {
            return Perform(() => _actSrzDb.DeleteTmcAct(req.RequestObject));
        }

        //удаляет транзакции из актов поступления
        [Route("DeleteTmcAct")]
        [HttpPost]
        public Response<string> DeleteTmcTransactions(Request<List<SparePartsModel>> req)
        {
            return Perform(() => _actSrzDb.DeleteTmcTransactions(req.RequestObject));
        }

        //удаляет транзакции из актов списания
        [Route("DeleteTransaction")]
        [HttpPost]
        public Response<string> DeleteTransaction(Request<List<ModelUpdateRow>> req)
        {
            return Perform(() => _actSrzDb.DeleteTransaction(req.RequestObject));
        }
       //update rows - списание
        [Route("UpdateRow")]
        [HttpPost]
        public Response<string> UpdateRow(Request<List<UpdateRow>> req)
        {
            return Perform(() => _actSrzDb.UpdateRow(req.RequestObject));
        }

        [Route("GetDictionaryForActs")]
        [HttpPost]
        public Response<DictionatysActSzr> GetDictionaryForActs(Request<ActSzrDetails1> req)
        {
            return Perform(() => _actSrzDb.GetDictionaryForActs(req.RequestObject));
        }
        [Route("GetDictionaryForActs1")]
        [HttpPost]
        public Response<DictionatysActSzr> GetDictionaryForActs1(Request<BalanceMaterials> req)
        {
            return Perform(() => _actSrzDb.GetDictionaryForActs1(req.RequestObject));
        }

        [Route("GetPrintActInfo")]
        [HttpPost]
        public Response<PrintActInfo> GetPrintActInfo(Request<int> req)
        {
            return Perform(() => _actSrzDb.GetPrintActInfo(req.RequestObject));
        }

        [Route("GetInfoForCreateIncomingFuel")]
        [HttpGet]
        public Response<InfoIncomingFuelModel> GetInfoForCreateIncomingFuel()
        {
            return Perform(() => _actSrzDb.GetInfoForCreateIncomingFuel());
        }

        [Route("PostIncomeFuel")]
        [HttpPost]
        public Response<bool> PostIncomeFuel(Request<List<IncomeFuel>> req)
        {
            return Perform(() => _actSrzDb.PostIncomeFuel(req.RequestObject));
        }
        [Route("GetIncomeFuelByDate")]
        [HttpPost]
        public Response<List<IncomeFuel>> GetIncomeFuelByDate(Request<DateTime> req)
        {
            return Perform(() => _actSrzDb.GetIncomeFuelByDate(req.RequestObject));
        }

        //список актов поступления
        [Route("GetTmcActsByDate")]
        [HttpPost]
        public Response<SparePartsInvoiceListResponse> GetTmcActsByDate(Request<GridDataReq> req)
        {
            return Perform(() =>  _actSrzDb.GetTmcActsByDateDetails(req.RequestObject));
        }
        //
        [Route("GetTmcMoveByDate")]
        [HttpPost]
        public Response<SparePartsInvoiceListResponse> GetTmcMoveByDate(Request<GridDataReq> req)
        {
            return Perform(() => _actSrzDb.GetTmcMoveByDateDetails(req.RequestObject));
        }

        //печать передвижение
        [Route("GetPrintMoveInfo")]
        [HttpPost]
        public Response<WriteOffActsDetails> GetPrintDemandInfo(Request<GeneralInfo> req)
        {
            return Perform(() => _actSrzDb.GetPrintMoveInfo(req.RequestObject.ActId));
        }


        [Route("GetInfoBalanceMaterials")]
        [HttpPost]
        public Response<InfoBalanceMaterialsModel> GetInfoBalanceMaterials(Request<BalanceMaterials> req)
        {
            return Perform(() => _actSrzDb.GetInfoBalanceMaterials(req.RequestObject));
        }
        //баланс
        [Route("GetBalanceMaterialsList")]
        [HttpPost]
        public Response<List<BalanceMaterials>> GetBalanceMaterialsList(Request<BalanceMaterials> req)
        {
            return Perform(() => _actSrzDb.GetBalanceMaterialsList(req.RequestObject));
        }

        [Route("GetActsIncomeLists")]
        [HttpPost]
        public Response<GetActsSzrListResponse> GetActsIncomeLists(Request<GridDataReq> req)
        {
            return Perform(() => _actSrzDb.GetActsIncomeLists(req.RequestObject));
        }

        [Route("RecountTmcAct")]
        [HttpGet]
        public string RecountTmcAct()
        {
            return _actSrzDb.RecountTmcAct();
        }

    }


}