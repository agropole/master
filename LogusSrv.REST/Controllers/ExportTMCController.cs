﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using LogusSrv.CORE.REST;
using System.Net.Http.Headers;
using System.Xml;
using System.IO;
using System.Text;
using System.Xml.Linq;
using System.Web.Http.Results;

using System.Data.SqlClient;
using System.Data.OleDb;
using System.Data.Odbc;
using System.Data.Common;
using System.Configuration;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;

using LogusSrv.CORE.Exceptions;
using LogusSrv.DAL.DBModel;
using LogusSrv.DAL.Entities.DTO;
using LogusSrv.DAL.Entities.Res;
using LogusSrv.DAL.Entities.Req;
using LogusSrv.DAL.Enums;
using LogusSrv.DAL.Utils;

using LogusSrv.DAL.Entities.SQL;


namespace LogosSrv.Controllers
{
    [RoutePrefix("api/TMCExport")]
    public class ExportTMCController : BaseCtrl
    {
        [Route("Exp")]
        [HttpGet]
        public HttpResponseMessage Exp(DateTime? startDate = null, DateTime? endDate = null)
        {
            HttpResponseMessage result = new HttpResponseMessage(HttpStatusCode.OK);

            string bum = getOutData((DateTime)startDate, (DateTime)endDate);

            result.Content = new StringContent(bum);
            result.Content.Headers.ContentType = new MediaTypeHeaderValue("text/xml");
            result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
            {
                FileName = "spisanie.xml",
            };
            return result;
        }

        protected string getOutData(DateTime sDate, DateTime eDate)
        {
            var ml = InitOutArray(sDate, eDate);

            MemoryStream mStream = new MemoryStream();
            XmlTextWriter writer = new XmlTextWriter(mStream, Encoding.UTF8);

            writer.Formatting = Formatting.Indented;
            writer.WriteStartDocument();
            writer.WriteStartElement("Организация");
            writer.WriteAttributeString("Наименование", "ООО Логус-агро");
            writer.WriteElementString("Период", sDate.ToString() + " - " + eDate.ToString());
            writer.WriteStartElement("Списание");

            foreach (var m in ml)
            {
                writer.WriteStartElement("Операция");
                writer.WriteAttributeString("Дата",m.act_date.ToShortDateString());
                writer.WriteAttributeString("Время", m.act_date.ToShortTimeString());
                
                writer.WriteStartElement("Подразделение");
                  writer.WriteAttributeString("ТипДанных", "Подразделения");
                  writer.WriteAttributeString("Код", "ЛА-000054");
                  writer.WriteAttributeString("Наименование", m.fiel_name);
                  writer.WriteAttributeString("Ссылка", m.fiel_code);
                writer.WriteEndElement(); // Подразделение

                writer.WriteStartElement("Субконто1");
                  writer.WriteAttributeString("ТипДанных", "Номенклатурные группы");
                  writer.WriteAttributeString("Код", "ЛА-000594");
                  writer.WriteAttributeString("Наименование", m.plant_name);
                  writer.WriteAttributeString("Ссылка", m.fps_code);
                writer.WriteEndElement();

                writer.WriteElementString("Субконто2", "");
                writer.WriteElementString("Субконто3", "");

                writer.WriteStartElement("КорСубконто1");
                  writer.WriteAttributeString("ТипДанных", "Номенклатура");
                  writer.WriteAttributeString("Код", "ЛА-000594");
                  writer.WriteAttributeString("Наименование", m.mat_name);
                  writer.WriteAttributeString("Ссылка", m.mat_code);
                writer.WriteEndElement(); // КорСубконто1

                writer.WriteStartElement("КорСубконто2");
                writer.WriteAttributeString("ТипДанных", "Склады (места хранения)");
                writer.WriteAttributeString("Код", "00-000021");
                writer.WriteAttributeString("Наименование", m.fullname);
                writer.WriteAttributeString("Ссылка", m.emp_code);
                writer.WriteEndElement(); // КорСубконто2

                writer.WriteElementString("КорСубконто3", "");
                writer.WriteElementString("Количество", m.count.ToString());
                writer.WriteElementString("Сумма", (m.count*m.price).ToString());
                
                writer.WriteEndElement(); // операция
            }

            writer.WriteEndElement(); // Списание
            writer.WriteEndElement(); //Организация

            writer.Flush();
            mStream.Flush();
            mStream.Position = 0;
            StreamReader sReader = new StreamReader(mStream);
            String FormattedXML = sReader.ReadToEnd();

            return FormattedXML;
        }

        protected List<ExportTMCOutSet> InitOutArray(DateTime sDate, DateTime eDate)
        {
            string query = @"select tmr.act_date,
                                    fiel.name fiel_name, 
                                    fiel.code fiel_code,
	                                plant.name plant_name,
	                                fielplan.in_code fps_code,
	                                mat.name mat_name,
	                                mat.code mat_code,
	                                emp.second_name+' '+emp.first_name+' '+emp.middle_name fullname,
	                                emp.code emp_code, 
                                    tmr.count,
                                    tmr.price, 
		                            tma.act_date,
		                            tma.act_num
                           from tmc_records tmr	
                           left join tmc_acts tma on tma.tma_id = tmr.tma_tma_id
                           join fields_to_plants fielplan on tmr.fielplan_fielplan_id = fielplan.fielplan_id
                           join fields fiel on fielplan.fiel_fiel_id = fiel.fiel_id
                           join plants plant on fielplan.plant_plant_id = plant.plant_id
                           join materials mat on tmr.mat_mat_id = mat.mat_id	   
                           join employees emp on tmr.emp_from_id = emp.emp_id "+
                           "where tmr.act_date >= '" + sDate.ToString("d") + "'" +
                           " and tmr.act_date <= '" + eDate.AddDays(1).ToString("d") + "'";
            var dc = new LogusDBEntities();
            return dc.Database.SqlQuery<ExportTMCOutSet>(query).ToList(); 
        }
//------------------------------------------------------------------------------------------------------------------------------
        [Route("Receive")]
        [HttpPost]
        public string Receive()
        {
            XmlDocument doc = new XmlDocument();
            var pp = HttpContext.Current.Request.Files.Count;
            if (pp > 0)
            {
                string rescon = "";
                int success = 0;
                var dc = new LogusDBEntities();
                var upload = HttpContext.Current.Request.Files[0];
                byte[] avatar = new byte[upload.ContentLength];
                upload.InputStream.Read(avatar, 0, upload.ContentLength);

                Stream stream = new MemoryStream(avatar);
                XDocument xDoc = XDocument.Load(stream);

                var xRoot = xDoc.Descendants(XName.Get("ПОСТУПЛЕНИЕ")).First();
                rescon += "Всего записей: " + xRoot.Descendants(XName.Get("Операция")).Count().ToString() + "<br>";

                foreach (var m in xRoot.Descendants(XName.Get("Операция")).ToList())
                {
                    int mat_id, emp_id;
                    string code1 = "", code2 = "";
                    try
                    {
                        code1 = m.Descendants(XName.Get("Субконто1")).First().Attribute("Ссылка").Value;
                        string query1 = "select mat_id as xid from Materials where code = '" + code1 + "'";
                        var res = dc.Database.SqlQuery<ExportTMCCodeSet>(query1).First();
                        mat_id = res.xid;
                    }
                    catch (InvalidOperationException e)
                    {
                        rescon += "Не найден материал " + m.Descendants(XName.Get("Субконто1")).First().Attribute("Наименование").Value + " с кодом " + code1 + "<br>";
                        continue;
                    }

                    try
                    {
                        code2 = m.Descendants(XName.Get("Субконто2")).First().Attribute("Ссылка").Value;
                        string query2 = "select emp_id as xid from Employees where sklad_code = '" + code2 + "'";
                        var res2 = dc.Database.SqlQuery<ExportTMCCodeSet>(query2).First();
                        emp_id = res2.xid;
                    }
                    catch (InvalidOperationException e)
                    {
                        rescon += "Не найден Сотрудник " + m.Descendants(XName.Get("Субконто2")).First().Attribute("Наименование").Value + " с кодом " + code2 + "<br>";
                        continue;
                    }

                    // дожили и вычислили значения правильно
                    var m_data = m.Descendants(XName.Get("Дата")).First().Value;
                    var m_time = m.Descendants(XName.Get("Время")).First().Value;
                    var v_kol = m.Descendants(XName.Get("Количество")).First().Value;
                    var v_sum = m.Descendants(XName.Get("Сумма")).First().Value;

                    m_data = m_data.Substring(6, 4) + "-" + m_data.Substring(3, 2) + "-" + m_data.Substring(0, 2);

                    try 
                    {
                        var sql_check = string.Format("select tmr_id as xid from TMC_Records where emp_to_id = {0} and mat_mat_id = {1} and act_date = convert(datetime,'{2}',101) and count = {3}", emp_id, mat_id, m_data + " " + m_time, v_kol);
                        var reschk = dc.Database.SqlQuery<ExportTMCCodeSet>(sql_check).First();
                    }
                    catch (InvalidOperationException e)
                    {
                        v_kol = v_kol.Replace('.', ',');
                        v_sum = v_sum.Replace('.', ',');
                        var f_kol = Double.Parse(v_kol);
                        Double f_price;
                        try
                        {
                            f_price = (f_kol == 0) ? 0.0 : Double.Parse(v_sum) / f_kol;
                            f_price = Math.Round(f_price, 2);
                        }
                        catch (FormatException e2) 
                        { f_price = 0; }

                        var v_price = f_price.ToString();
                        v_kol = v_kol.Replace(',', '.');
                        v_price = v_price.Replace(',', '.');

                        var sql3 = string.Format("insert into TMC_Records (cont_cont_id, mat_mat_id, act_date, count, price, emp_to_id,deleted,area,emp_from_id) values({0}, {1}, convert(datetime,'{2}',101), {3}, {4}, {5}, {6}, {7},{8});",
                                                      1, mat_id, m_data + " " + m_time, v_kol, v_price, emp_id, 0,0,0);

                        dc.Database.ExecuteSqlCommand(sql3);
                        rescon += "Строка вставлена <br>";
                        success++;
                        continue;
                    }

                    rescon += "Запись в базе уже есть <br>";
                    continue;

                }
                return rescon + "Всего успешно загружено записей: "+success.ToString();
            }

            return "Файл при загрузке не найден!";
        }

            /*
            foreach (string file in Request.Files)
            {
                var upload = Request.Files[file];
                if (upload != null)
                {
                    // получаем имя файла
                    string fileName = System.IO.Path.GetFileName(upload.FileName);
                    // сохраняем файл в папку Files в проекте
                    upload.SaveAs(Server.MapPath("~/Files/" + fileName));
                }
            }  */


           // HttpPostedFileBase postedFile = Request.Files[0];


        /*

        [HttpPost]
        [HttpGet]
        public HttpResponseMessage Receive(HttpPostedFile upload)
        {
            HttpResponseMessage result = new HttpResponseMessage(HttpStatusCode.OK);

            result.Content = new StringContent("Error 1, 2, 3");
            return result;
            XmlDocument doc = new XmlDocument();
            if (upload != null)
            {
                //имя файла
                var fileName = System.IO.Path.GetFileName(upload.FileName);
            }
            byte[] avatar = new byte[upload.ContentLength];
            upload.InputStream.Read(avatar, 0, upload.ContentLength);

            Stream stream = new MemoryStream(avatar);
            XDocument xDoc = XDocument.Load(stream);
            //XDocument xDoc = XDocument.Load("D:\\Project\\prihod.xml");

            string rescon = "";
            var dc = new LogusDBEntities();

            var xRoot = xDoc.Descendants(XName.Get("ПОСТУПЛЕНИЕ")).First();

            foreach (var m in xRoot.Descendants(XName.Get("Операция")).ToList())
            {
                int mat_id, emp_id;

                try
                {
                    var code1 = m.Descendants(XName.Get("Субконто1")).First().Attribute("Ссылка").Value;
                    string query1 = "select mat_id as xid from Materials where code = '"+code1+"'";
                    var res = dc.Database.SqlQuery<ExportTMCCodeSet>(query1).First();

                    mat_id = res.xid;
                }
                catch (InvalidOperationException e)
                {
                    continue;
                }

                try
                {
                    var code2 = m.Descendants(XName.Get("Субконто2")).First().Attribute("Ссылка").Value;
                    string query2 = "select emp_id as xid from Employees where code = '" + code2 + "'";
                    var res2 = dc.Database.SqlQuery<ExportTMCCodeSet>(query2).First();

                    emp_id = res2.xid;
                }
                catch (InvalidOperationException e)
                {
                    continue;
                }
                
                // дожили и вычислили значения правильно
                var m_data = m.Descendants(XName.Get("Дата")).First().Value;
                var m_time = m.Descendants(XName.Get("Время")).First().Value;
                var v_kol = m.Descendants(XName.Get("Количество")).First().Value;
                var v_sum = m.Descendants(XName.Get("Сумма")).First().Value;

                var sql3 = string.Format("insert into TMC_Records (cont_cont_id, mat_mat_id, act_date, count, price, emp_to_id) values({0}, {1}, '{2}', {3}, {4}, {5});",
                                       1, mat_id, m_data+" "+m_time, v_kol, v_sum, emp_id);

                dc.Database.ExecuteSqlCommand(sql3);
                rescon += "Строка вставлена \n";
            } 
            result.Content = new StringContent(rescon);
            return result; */
        //}

//-----------------------------------------------------------------------------------------------------------------------------
        [Route("ReceiveSdel")]
        [HttpPost]
        public string ReceiveSdel()
        {
            XmlDocument doc = new XmlDocument();
            var pp = HttpContext.Current.Request.Files.Count;
            if (pp > 0)
            {
                string rescon = "";
                int success = 0;
                var dc = new LogusDBEntities();
                var upload = HttpContext.Current.Request.Files[0];
                byte[] avatar = new byte[upload.ContentLength];
                upload.InputStream.Read(avatar, 0, upload.ContentLength);

                Stream stream = new MemoryStream(avatar);
                XDocument xDoc = XDocument.Load(stream);

                var xRoot = xDoc.Descendants(XName.Get("ПОСТУПЛЕНИЕ")).First();
                rescon += "Всего записей: " + xRoot.Descendants(XName.Get("Операция")).Count().ToString() + "<br>";

                foreach (var m in xRoot.Descendants(XName.Get("Операция")).ToList())
                {
                    int mat_id, emp_id;
                    string code1 = "", code2 = "";
                    try
                    {
                        code1 = m.Descendants(XName.Get("Субконто1")).First().Attribute("Ссылка").Value;
                        string query1 = "select mat_id as xid from Materials where code = '" + code1 + "'";
                        var res = dc.Database.SqlQuery<ExportTMCCodeSet>(query1).First();
                        mat_id = res.xid;
                    }
                    catch (InvalidOperationException e)
                    {
                        rescon += "Не найден материал " + m.Descendants(XName.Get("Субконто1")).First().Attribute("Наименование").Value + " с кодом " + code1 + "<br>";
                        continue;
                    }

                    try
                    {
                        code2 = m.Descendants(XName.Get("Субконто2")).First().Attribute("Ссылка").Value;
                        string query2 = "select emp_id as xid from Employees where sklad_code = '" + code2 + "'";
                        var res2 = dc.Database.SqlQuery<ExportTMCCodeSet>(query2).First();
                        emp_id = res2.xid;
                    }
                    catch (InvalidOperationException e)
                    {
                        rescon += "Не найден Сотрудник " + m.Descendants(XName.Get("Субконто2")).First().Attribute("Наименование").Value + " с кодом " + code2 + "<br>";
                        continue;
                    }

                    // дожили и вычислили значения правильно
                    var m_data = m.Descendants(XName.Get("Дата")).First().Value;
                    var m_time = m.Descendants(XName.Get("Время")).First().Value;
                    var v_kol = m.Descendants(XName.Get("Количество")).First().Value;
                    var v_sum = m.Descendants(XName.Get("Сумма")).First().Value;

                    m_data = m_data.Substring(6, 4) + "-" + m_data.Substring(3, 2) + "-" + m_data.Substring(0, 2);

                    try
                    {
                        var sql_check = string.Format("select tmr_id as xid from TMC_Records where emp_to_id = {0} and mat_mat_id = {1} and act_date = '{2}' and count = {3}", emp_id, mat_id, m_data + " " + m_time, v_kol);
                        var reschk = dc.Database.SqlQuery<ExportTMCCodeSet>(sql_check).First();
                    }
                    catch (InvalidOperationException e)
                    {
                        v_kol = v_kol.Replace('.', ',');
                        v_sum = v_sum.Replace('.', ',');
                        var f_kol = Double.Parse(v_kol);
                        Double f_price;
                        try
                        {
                            f_price = (f_kol == 0) ? 0.0 : Double.Parse(v_sum) / f_kol;
                            f_price = Math.Round(f_price, 2);
                        }
                        catch (FormatException e2)
                        { f_price = 0; }

                        var v_price = f_price.ToString();
                        v_kol = v_kol.Replace(',', '.');
                        v_price = v_price.Replace(',', '.');

                        var sql3 = string.Format("insert into TMC_Records (cont_cont_id, mat_mat_id, act_date, count, price, emp_to_id) values({0}, {1}, '{2}', {3}, {4}, {5});",
                                                      1, mat_id, m_data + " " + m_time, v_kol, v_price, emp_id);

                        dc.Database.ExecuteSqlCommand(sql3);
                        rescon += "Строка вставлена <br>";
                        success++;
                        continue;
                    }

                    rescon += "Запись в базе уже есть <br>";
                    continue;

                }
                return rescon + "Всего успешно загружено записей: " + success.ToString();
            }

            return "Файл при загрузке не найден!";
        }
////////////////////////////////////////////////////////////////////////////////////////////ВЫГРУЗКА СДЕЛЬНАЯ ЗП
        [Route("ExpDocumentsList")]
        [HttpGet]
        public HttpResponseMessage ExpDocumentsList(DateTime? startDate = null, DateTime? endDate = null)
        {
            HttpResponseMessage result = new HttpResponseMessage(HttpStatusCode.OK);

            string bum = getData((DateTime)startDate, (DateTime)endDate);

            result.Content = new StringContent(bum);
            result.Content.Headers.ContentType = new MediaTypeHeaderValue("text/xml");
            result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
            {
                FileName = "salary.xml",
            };
            return result;
        }

        protected string getData(DateTime sDate, DateTime eDate)
        {
            var ml = InitArray(sDate, eDate);
            var dc = new LogusDBEntities();
            var list = dc.Database.SqlQuery<ExportDocumentsList>(
          string.Format(@"select sum(sum) as summaDoc, doc_num as doc_num from Salaries,Salary_details where sal_id=sal_sal_id and sum is not null group by doc_num ")).ToList();

            for (int j = 0; j < list.Count; j++)
            {
                for (int i = 0; i < ml.Count; i++)
                {
                    if (ml[i].doc_num == list[j].doc_num) { ml[i].sum1 = list[j].summaDoc; }
                }
            }




          MemoryStream mStream = new MemoryStream();
          XmlTextWriter writer = new XmlTextWriter(mStream, Encoding.UTF8);
          writer.Formatting = Formatting.Indented;
          writer.WriteStartDocument();
         // writer.WriteStartElement("Root");
          writer.WriteStartElement("Организация");
          writer.WriteAttributeString("Наименование", "ООО Логус-агро");
          writer.WriteElementString("Период", sDate.ToString() + " - " + eDate.ToString());
          int b = 1; 
          for (int j = 0; j < ml.Count; j++ )
          {
           
              if (j == 0)
              {
               
                  writer.WriteStartElement("Документ");
                  writer.WriteAttributeString("Ссылка", " ");
                  writer.WriteAttributeString("Дата", ml[j].doc_date.ToString());
                  writer.WriteAttributeString("Номер", ml[j].doc_num);
                  writer.WriteAttributeString("СуммаДокумента", ml[j].sum1.ToString());
              }
              else {
                  if (ml[j - 1].doc_num != ml[j].doc_num)
                  {
                       b = 1;
                      writer.WriteStartElement("Документ");
                      writer.WriteAttributeString("Ссылка", " ");
                      writer.WriteAttributeString("Дата", ml[j].doc_date.ToString());
                      writer.WriteAttributeString("Номер", ml[j].doc_num);
                      writer.WriteAttributeString("СуммаДокумента", ml[j].sum1.ToString());
                  }
                  else { b++; }

              }
                  writer.WriteStartElement("Строка");
                  writer.WriteAttributeString("№", b.ToString());
                  writer.WriteAttributeString("ДатаРаботы", ml[j].date.ToString());
                  writer.WriteAttributeString("НомерПутевогоЛиста", " ");
                  writer.WriteAttributeString("ПолевойДень", " ");
                  writer.WriteAttributeString("ОтработаноЧасов", " ");
                  writer.WriteAttributeString("Выполнено", ml[j].value.ToString());
                  writer.WriteAttributeString("Примечание", "");
                  writer.WriteAttributeString("Расценка", ml[j].price.ToString());
                  writer.WriteAttributeString("ПроцентПремии", "");
                  writer.WriteAttributeString("СуммаПремии", "");
                  writer.WriteAttributeString("Сумма", "");

                  writer.WriteStartElement("Сотрудник");
                  writer.WriteAttributeString("Ссылка", ml[j].emp_code);
                  writer.WriteAttributeString("Код", " ");
                  writer.WriteAttributeString("Наименование", ml[j].Name);
                  writer.WriteEndElement();

                  writer.WriteStartElement("Должность");
                  writer.WriteAttributeString("Ссылка", " ");
                  writer.WriteAttributeString("Код", " ");
                  writer.WriteAttributeString("Наименование", " ");
                  writer.WriteEndElement();

                  writer.WriteStartElement("Подразделение");
                  writer.WriteAttributeString("Ссылка", " ");
                  writer.WriteAttributeString("Код", " ");
                  writer.WriteAttributeString("Наименование", " ");
                  writer.WriteEndElement();

                  writer.WriteStartElement("ПодразделениеЗатрат");
                  writer.WriteAttributeString("Ссылка", ml[j].fiel_code);
                  writer.WriteAttributeString("Код", " ");
                  writer.WriteAttributeString("Наименование", ml[j].fiel_name);
                  writer.WriteEndElement();

                  writer.WriteStartElement("ВидРабот");
                  writer.WriteAttributeString("Ссылка", ml[j].task_code1);
                  writer.WriteAttributeString("Код", ml[j].task_code);
                  writer.WriteAttributeString("Наименование", ml[j].description);
                  writer.WriteEndElement();

                  writer.WriteEndElement(); // Строка
              //
                  if (j < ml.Count - 1)
                  {
                      if (ml[j].doc_num != ml[j + 1].doc_num)
                      { writer.WriteEndElement(); }
                  } 

              if (j == ml.Count - 1) { writer.WriteEndElement(); }

          }


        //  writer.WriteEndElement(); //root
          writer.Flush();
          mStream.Flush();
          mStream.Position = 0;
          StreamReader sReader = new StreamReader(mStream);
          String FormattedXML = sReader.ReadToEnd();
          return FormattedXML;
         
         
        }

        protected List<ExportDocumentsList> InitArray(DateTime sDate, DateTime eDate)
        {
            string query = @"select doc_num as doc_num, doc_data as doc_date,sum as sum,sum as sum1, date as date, price as price,
        value as value, e.in_code as emp_code, second_name+' '+first_name+' '+middle_name as name, t.code as task_code,
         t.description as description, t.in_code as task_code1, f.code as fiel_code, f.name as  fiel_name
       from Salaries s, Salary_details d,Salaries_employees e, Type_tasks t, Fields f
       where sal_id=sal_sal_id and d.salemp_salemp_id=e.salemp_id and t.tptas_id=d.tptas_tptas_id and
      f.fiel_id=d.fiel_fiel_id " +
                           "and doc_data >= '" + sDate.ToString("d") + "'" +
                           " and doc_data <= '" + eDate.AddDays(1).ToString("d") + "'";
            var dc = new LogusDBEntities();
            return dc.Database.SqlQuery<ExportDocumentsList>(query).ToList();
        }

    }
    
}