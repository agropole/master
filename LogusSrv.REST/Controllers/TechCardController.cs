﻿using System.Web.Http;
using LogusSrv.CORE.REST;
using LogusSrv.DAL.Operations;
using LogusSrv.DAL.Entities.Req;
using LogusSrv.DAL.Entities.Res;
using System.Collections.Generic;
using LogusSrv.DAL.Entities.DTO.ExportExcel;
using System;
using System.Net.Http;
using System.Web;
using System.Net;
using System.IO;
using System.Net.Http.Headers;
using LogusSrv.DAL.Entities.DTO.ActsSzr;


namespace LogosSrv.Controllers
{

    [RoutePrefix("api/TechCard")]
    public class TechCardController : BaseCtrl
    {
        private TechCardDb tcDB = new TechCardDb();

        [Route("GetCards")]
        [HttpGet]
        public Response<List<TechCardModel>> GetCards()
        {
            return Perform(() => tcDB.GetTechCards());
        }

        [Route("GetFields")]
        [HttpGet]
        public Response<List<Field>> GetFields()
        {
            return Perform(() => tcDB.GetFields());
        }

        [Route("GetTaskLists")]
        [HttpGet]
        public Response<List<TCTask>> GetWayLists()
        {
            return Perform(() => tcDB.GetWayLists());
        }

        [Route("UpdateWayListTc")]
        [HttpPost]
        public Response<string> GetWayLists(Request<TCTask> req)
        {
            return Perform(() => tcDB.SetWayListToTC(req.RequestObject));
        }

        [Route("GetInfoTCDetail")]
        [HttpGet]
        public Response<InfoForTCDetail> GetInfoTCDetail()
        {
            return Perform(() => tcDB.GetInfoForTc());
        }

        [Route("GetPlantSortFieldByYear")]
        [HttpPost]
        public Response<List<PlantSortFieldDictionary>> GetPlantSortFieldByYear(Request<int> req)
        {
            return Perform(() => tcDB.GetPlantSortFieldByYear(req.RequestObject));
        }

        [Route("CreateUpdateTC")]
        [HttpPost]
        public Response<string> CreateUpdateTC(Request<TCDetailModel> req)
        {
            return Perform(() => tcDB.CreateUpdateTC(req.RequestObject));
        }

        [Route("GetTCByYear")]
        [HttpPost]
        public Response<List<TCItem>> GetTCByYear(Request<int> req)
        {
            return Perform(() => tcDB.GetTCByYear(req.RequestObject));
        }
        //
        [Route("CopyTCReportById")]
        [HttpPost]
        public Response<string> CopyTCReportById(Request<int> req)
        {
            return Perform(() => tcDB.CopyTCReportById(req.RequestObject));
        }

        [Route("GetTCById")]
        [HttpPost]
        public Response<TCDetailModel> GetTCById(Request<int> req)
        {
            return Perform(() => tcDB.GetTCById(req.RequestObject));
        }

        [Route("DelTC")]
        [HttpPost]
        public Response<string> DelTC(Request<int> req)
        {
            return Perform(() => tcDB.DelTC(req.RequestObject));
        }

        [Route("GetNZP")]
        [HttpPost]
        public Response<List<NZPRes>> GetNZP(Request<NZPReq> req)
        {
            return Perform(() => tcDB.GetNZP(req.RequestObject));
        }
        //расценки
        [Route("GetRate")]
        [HttpPost]
        public Response<RateResModel> GetRate(Request<RateTechReq> req)
        {
            return Perform(() => tcDB.GetRate(req.RequestObject));
        }
        //
        [Route("GetTechReport")]
        [HttpPost]
        public Response<GantTech> GetTechReport(Request<ReqGantt> req)
        {
            return Perform(() => tcDB.GetTechReport(req.RequestObject));
        }
        //распределение-техники-новое
        [Route("GetTechEqiepReport")]
        [HttpPost]
        public Response<GantTMC> GetTechEqiepReport(Request<ReqGantt> req)
        {
            return Perform(() => tcDB.GetTechEqiepReport(req.RequestObject));
        }
        //=====

        //распределение ТМЦ
        [Route("GetTMCReport")]
        [HttpPost]
        public Response<GantTMC> GetTMCReport(Request<ReqTMC> req)   
        {
            return Perform(() => tcDB.GetTMCReport(req.RequestObject)); 
        }
        [Route("GetTmcMaterial")]
        [HttpPost]
        public Response<List<MaterialDto>> GetTmcMaterial(Request<ReqTMC> req)
        {
            return Perform(() => tcDB.GetTmcMaterial(req.RequestObject));
        }
        [Route("GetTechReportInfo")]
        [HttpGet]
        public Response<GanttInfo> GetTechReportInfo()
        {
            return Perform(() => tcDB.GetTechReportInfo());
        }
        [Route("GetTCReportById")]
        [HttpGet]
        public HttpResponseMessage GetTCReportById(int tcId)
        {
            try
            {
                var templatePath = HttpContext.Current.Server.MapPath("~/ExcelReports/TcTmpl.xlsx");
                var res = tcDB.GetTCById(tcId);

                var tcReportDB = new TcReportDb();
                var report = tcReportDB.DrawTcReport(templatePath, res);
                var isIe = Request.Headers.UserAgent.ToString().Contains("Trident");
                HttpResponseMessage result = new HttpResponseMessage(HttpStatusCode.OK);
                if ((report.data != null))
                {
                    var stream = new MemoryStream(report.data);
                    var FileName = report.fileName;
                    result.Content = new StreamContent(stream);
                    result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/vnd.ms-excel");
                    result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
                    {
                        FileName = isIe ? HttpUtility.UrlEncode(FileName) : (FileName)
                    };
                }
                else
                {
                    result.StatusCode = HttpStatusCode.NoContent;
                }
                return result;
            }
            catch (Exception ex)
            {
                HttpResponseMessage errorResult = new HttpResponseMessage(HttpStatusCode.BadRequest);
                errorResult.ReasonPhrase = ex.ToString();
                return errorResult;
            }

        }
        //////////////////////////////////
        [Route("GetTCReportById1")]
        [HttpGet]
        public HttpResponseMessage GetTCReportById1(int tcId)
        {
            try
            {
                var templatePath = HttpContext.Current.Server.MapPath("~/ExcelReports/TcTmpl1.xlsx");
                var res = tcDB.GetTCById(tcId);

                var tcReportDB = new TcReportDb();
                var report = tcReportDB.DrawTcReport1(templatePath, res);
                var isIe = Request.Headers.UserAgent.ToString().Contains("Trident");
                HttpResponseMessage result = new HttpResponseMessage(HttpStatusCode.OK);
                if ((report.data != null))
                {
                    var stream = new MemoryStream(report.data);
                    var FileName = report.fileName;
                    result.Content = new StreamContent(stream);
                    result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/vnd.ms-excel");
                    result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
                    {
                        FileName = isIe ? HttpUtility.UrlEncode(FileName) : (FileName)
                    };
                }
                else
                {
                    result.StatusCode = HttpStatusCode.NoContent;
                }
                return result;
            }
            catch (Exception ex)
            {
                HttpResponseMessage errorResult = new HttpResponseMessage(HttpStatusCode.BadRequest);
                errorResult.ReasonPhrase = ex.ToString();
                return errorResult;
            }

        }

        [Route("GetTCReportByYear")]
        [HttpGet]
        public HttpResponseMessage GetTCReportByYear(int year)
        {
            try
            {
                var templatePath = HttpContext.Current.Server.MapPath("~/ExcelReports/TcTotalTmpl.xlsx");

                var tcReportDB = new TcReportDb();
                byte[] byteArray = tcReportDB.DrawTcTotalReport(templatePath, year);
                var isIe = Request.Headers.UserAgent.ToString().Contains("Trident");
                HttpResponseMessage result = new HttpResponseMessage(HttpStatusCode.OK);
                if ((byteArray != null))
                {
                    var stream = new MemoryStream(byteArray);
                    var FileName = "Сводный отчет по ТК за " + year + " год.xlsx";
                    result.Content = new StreamContent(stream);
                    result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/vnd.ms-excel");
                    result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
                    {
                        FileName = isIe ? HttpUtility.UrlEncode(FileName) : (FileName)
                    };
                }
                else
                {
                    result.StatusCode = HttpStatusCode.NoContent;
                }
                return result;
            }
            catch (Exception ex)
            {
                HttpResponseMessage errorResult = new HttpResponseMessage(HttpStatusCode.BadRequest);
                errorResult.ReasonPhrase = ex.ToString();
                return errorResult;
            }
        }
    }
}