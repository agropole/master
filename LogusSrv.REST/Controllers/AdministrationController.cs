﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using LogusSrv.CORE.REST;
using LogusSrv.DAL.Operations;
using LogusSrv.DAL.Entities.Res;
using LogusSrv.DAL.Entities.Req;
using LogusSrv.DAL.Entities.DTO;
using LogusSrv.DAL.Operations.DictionaryOperations;
using System.Configuration;
using System.Net.Http;
using System.IO;
using System.Net;
using System.Net.Http.Headers;

namespace LogosSrv.Controllers
{
    [RoutePrefix("api/AdministrationCtrl")]
    public class AdministrationController : BaseCtrl
    {
        public AdministrationDb _db = new AdministrationDb();
        public AgronomArmDb agrodb = new AgronomArmDb();

        [Route("GetUsers")]
        [HttpPost]
        public Response<UserListResponse> GetUsers(Request<GridDataReq> req)
        {
            return Perform(() => _db.GetUsers(req.RequestObject));
        }

        [Route("GetRoles")]
        [HttpPost]
        public Response<RoleListResponse> GetRoles(Request<GridDataReq> req)
        {
            return Perform(() => _db.GetRoles(req.RequestObject));
        }

        [Route("GetInfoUser")]
        [HttpPost]
        public Response<InfoUser> GetInfoUser()
        {
            return Perform(() => _db.GetInfoUser());
        }
        [Route("GetInfoPlants")]
        [HttpPost]
        public Response<UserSelectedPlants> GetInfoPlants(Request<int?> req)
        {
            return Perform(() => _db.GetInfoPlants(req.RequestObject));
        }
        [Route("AddPlant")]
        [HttpPost]
        public Response<string> AddPlant(Request<PlantModel> req)
        {
            return Perform(() => _db.AddPlant(req.RequestObject));
        }

        [Route("GetPlant")]
        [HttpGet]
        public Response<List<PlantModel>> GetPlant()
        {
            return Perform(() => _db.GetPlant());
        }

        [Route("ChangeCulture")]
        [HttpGet]
        public HttpResponseMessage GetCostResExcel()
        {
            try
            {
                var templatePath = HttpContext.Current.Server.MapPath("~/ExcelReports/ChangeCultTemplate.xlsx");
                var report = _db.ChangeCultureExcel(templatePath);

                var isIe = Request.Headers.UserAgent.ToString().Contains("Trident");
                HttpResponseMessage result = new HttpResponseMessage(HttpStatusCode.OK);
                if ((report.data != null))
                {
                    var stream = new MemoryStream(report.data);
                    var FileName = report.fileName;
                    result.Content = new StreamContent(stream);
                    result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/vnd.ms-excel");
                    result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
                    {
                        FileName = isIe ? HttpUtility.UrlEncode(FileName) : (FileName)
                    };
                }
                else
                {
                    result.StatusCode = HttpStatusCode.NoContent;
                }
                return result;
            }
            catch (Exception ex)
            {
                HttpResponseMessage errorResult = new HttpResponseMessage(HttpStatusCode.BadRequest);
                errorResult.ReasonPhrase = ex.ToString();
                return errorResult;
            }
        }


        [Route("GetHarvestNZP")]
        [HttpGet]
        public Response<List<PlantModel>> GetHarvestNZP()
        {
            return Perform(() => _db.GetHarvestNZP());
        }
        [Route("AddHarvestNZP")]
        [HttpPost]
        public Response<string> AddHarvestNZP(Request<PlantModel> req)
        {
            return Perform(() => _db.AddHarvestNZP(req.RequestObject));
        }

        //срезы тех. карт 
        [Route("AddSectionTechcard")]
        [HttpGet]
        public Response<Boolean> AddHarvestNZP()
        {
            return Perform(() => _db.AddSectionTechcard());
        }
        //даты срезов тех. карт
        [Route("GetSectionTechcard")]
        [HttpGet]
        public Response<List<String>> GetSectionTechcard()
        {
            return Perform(() => _db.GetSectionTechcard());
        }
        [Route("DeleteSectionTechcard")]
        [HttpPost]
        public Response<string> DeleteSectionTechcard(Request<string> req)
        {
            return Perform(() => _db.DeleteSectionTechcard(req.RequestObject));
        }
        [Route("DeletePlant")]
        [HttpPost]
        public Response<int> DeletePlant(Request<int> req)
        {
            return Perform(() => _db.DeletePlant(req.RequestObject));
        }
        [Route("DeleteHarvestNZP")]
        [HttpPost]
        public Response<int> HarvestNZP(Request<int> req)
        {
            return Perform(() => _db.DeleteHarvestNZP(req.RequestObject));
        }

        [Route("AddDic")]
        [HttpPost]
        public Response<string> AddDic(Request<List<UserModel>> req)
        {
            var saltPostfix = ConfigurationManager.AppSettings["SaltPostfix"];
            return Perform(() => _db.AddDic(req.RequestObject, saltPostfix));
        }

        [Route("AddEditDicRole")]
        [HttpPost]
        public Response<string> AddEditDicRole(Request<List<RoleModel>> req)
        {
            return Perform(() => _db.AddEditDicRole(req.RequestObject));
        }

        [Route("UpdateDic")]
        [HttpPost]
        public Response<int> UpdateDic(Request<List<SaveUser>> req)
        {
            var saltPostfix = ConfigurationManager.AppSettings["SaltPostfix"];
            return Perform(() => _db.UpdateDic(req.RequestObject, saltPostfix));
        }

        [Route("DeleteUser")]
        [HttpPost]
        public Response<string> deleteUser(Request<List<DelRow>> req)
        {
            return Perform(() => _db.DeleteUser(req.RequestObject));
        }

        [Route("DeleteRole")]
        [HttpPost]
        public Response<string> DeleteRole(Request<List<DelRow>> req)
        {
            return Perform(() => _db.DeleteRole(req.RequestObject));
        }

        //GetSubsystemsByRole
        [Route("GetSubsystemsByRole")]
        [HttpPost]
        public Response<List<string>> GetSubsystemsByRole(Request<int> req)
        {
            return Perform(() => _db.GetSubsystemsByRole(req.RequestObject));
        }


        //photo
        [Route("DeletePhoto")]
        [HttpPost]
        public Response<string> GetUsers(Request<int> req)
        {
            return Perform(() => agrodb.DeletePhoto(req.RequestObject));
        }

        [Route("GetParamOrganization")]
        [HttpGet]
        public Response<ParamOperationModel> GetParamOrganizationDetails()
        {
            return Perform(() => _db.GetParamOrganizationDetails());
        }

        [Route("GetControlTech")]
        [HttpGet]
        public Response<ParamOperationModel> GetControlTech()
        {
            return Perform(() => _db.GetControlTechDetails());
        }

        [Route("GetInfoParamOrganization")]
        [HttpGet]
        public Response<ParamOperDistModel> GetInfoParamOrganization()
        {
            return Perform(() => _db.GetInfoParamOrganization());
        }

        [Route("SaveParamOrganization")]
        [HttpPost]
        public Response<string> SaveParamOrganization(Request<ParamOperationModel> req)
        {
            return Perform(() => _db.SaveParamOrganization(req.RequestObject));
        }

        [Route("SaveControlTech")]
        [HttpPost]
        public Response<string> SaveControlTech(Request<ParamOperationModel> req)
        {
            return Perform(() => _db.SaveControlTech(req.RequestObject));
        }


    }
}