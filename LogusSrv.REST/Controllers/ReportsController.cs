﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using LogusSrv.CORE.REST;
using LogusSrv.DAL.Operations;
using LogusSrv.DAL.Entities.DTO.ExportExcel;
using System.Net.Http.Headers;
using System.Xml;
using System.Text;

namespace LogosSrv.Controllers
{
    [RoutePrefix("api/ReportsCtrl")]
    public class ReportsController : BaseCtrl
    {

        private readonly ReportsModuleDb exportDb = new ReportsModuleDb();


        [Route("GetReportsInfo")]
        [HttpPost]
        public Response<GetReportsInfoDto> GetReportsInfo(Request<RespReq> req)
        {
            return Perform(() => exportDb.GetReportsInfo(req.RequestObject));
        }
        //получаем сдельщиков за конкретный период
        [Route("GetResp")]
        [HttpPost]
        public Response<List<Resp>> GetResp(Request<RespReq> req)
        {
            return Perform(() => exportDb.GetResp(req.RequestObject));
        }

        //GetCulture
        [Route("GetCulture")]
        [HttpPost]
        public Response<List<Cult>> GetCult(Request<CultReq> req)
        {
            return Perform(() => exportDb.GetCult(req.RequestObject));
        }
        //По себестоимости
        [Route("GetPlanFactCult")]
        [HttpPost]
        public Response<List<Cult>> GetPlanFactCult(Request<CultReq> req)
        {
            return Perform(() => exportDb.GetPlanFactCult(req.RequestObject));
        }


        [Route("GetWayListExportExcel")]
        [HttpGet]
        public HttpResponseMessage GetWayListExportExcel(int? orgId = null,DateTime? startDate = null , DateTime? endDate = null)
        {
            try
            {
                var templatePath = HttpContext.Current.Server.MapPath("~/ExcelReports/WayListTemplate.xlsx");
                byte[] byteArray = exportDb.GetWayListExportExcel(templatePath, orgId, startDate, endDate, true);
                var isIe = Request.Headers.UserAgent.ToString().Contains("Trident");
                HttpResponseMessage result = new HttpResponseMessage(HttpStatusCode.OK);
                if ((byteArray != null))
                {
                    var stream = new MemoryStream(byteArray);
                    var FileName = "Отчет по путевым листам механизаторов  " + startDate.Value.ToString("dd.MM.yyyy") + " по " + endDate.Value.ToString("dd.MM.yyyy") + ".xlsx";
                    result.Content = new StreamContent(stream);
                    result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/vnd.ms-excel");

                    //   result.Content.Headers.Add("content-disposition", "attachment; filename=);
                    result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
                    {
                        FileName = isIe ? HttpUtility.UrlEncode(FileName) : (FileName)
                    };
                }
                else
                {
                    result.StatusCode = HttpStatusCode.NoContent;
                }
                return result;
            }
            catch (Exception ex)
            {
                HttpResponseMessage errorResult = new HttpResponseMessage(HttpStatusCode.BadRequest);
                errorResult.ReasonPhrase = ex.ToString();
                return errorResult;
            } 

        }

        [Route("GetDriverWayListExportExcel")]
        [HttpGet]
        public HttpResponseMessage GetDriverWayListExportExcel(int? orgId = null, DateTime? startDate = null, DateTime? endDate = null)
        {
            try
            {
                var templatePath = HttpContext.Current.Server.MapPath("~/ExcelReports/WayDriverListTemplate.xlsx");
                byte[] byteArray = exportDb.GetWayListExportExcel(templatePath, orgId, startDate, endDate, false);
                var isIe = Request.Headers.UserAgent.ToString().Contains("Trident");
                HttpResponseMessage result = new HttpResponseMessage(HttpStatusCode.OK);
                if ((byteArray != null))
                {
                    var stream = new MemoryStream(byteArray);
                    var FileName = "Отчет по путевым листам водителей  " + startDate.Value.ToString("dd.MM.yyyy") + " по " + endDate.Value.ToString("dd.MM.yyyy") + ".xlsx";
                    result.Content = new StreamContent(stream);
                    result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/vnd.ms-excel");

                    //   result.Content.Headers.Add("content-disposition", "attachment; filename=);
                    result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
                    {
                        FileName = isIe ? HttpUtility.UrlEncode(FileName) : (FileName)
                    };
                }
                else
                {
                    result.StatusCode = HttpStatusCode.NoContent;
                }
                return result;
            }
            catch (Exception ex)
            {
                HttpResponseMessage errorResult = new HttpResponseMessage(HttpStatusCode.BadRequest);
                errorResult.ReasonPhrase = ex.ToString();
                return errorResult;
            }

        }


        [Route("GetRefuilngsExportExcel")]
        [HttpGet]
        public HttpResponseMessage GetRefuilngsExportExcel(int? TraktorId = null, DateTime? startDate = null, DateTime? endDate = null)
        {
            try
            {
                var templatePath = HttpContext.Current.Server.MapPath("~/ExcelReports/FuelTemplate.xlsx");
                byte[] byteArray = exportDb.GetRefuilngsExportExcel(templatePath, TraktorId, startDate, endDate);
                var isIe = Request.Headers.UserAgent.ToString().Contains("Trident");
                HttpResponseMessage result = new HttpResponseMessage(HttpStatusCode.OK);
                if ((byteArray != null))
                {
                    var stream = new MemoryStream(byteArray);
                    var FileName = "Отчет по топливу " + startDate.Value.ToString("dd.MM.yyyy") + " по " + endDate.Value.ToString("dd.MM.yyyy") + ".xlsx";
                    result.Content = new StreamContent(stream);
                    result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/vnd.ms-excel");

                    //   result.Content.Headers.Add("content-disposition", "attachment; filename=);
                    result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
                    {
                        FileName = isIe ? HttpUtility.UrlEncode(FileName) : (FileName)
                    };
                }
                else
                {
                    result.StatusCode = HttpStatusCode.NoContent;
                }
                return result;
            }
            catch (Exception ex)
            {
                HttpResponseMessage errorResult = new HttpResponseMessage(HttpStatusCode.BadRequest);
                errorResult.ReasonPhrase = ex.ToString();
                return errorResult;
            }

        }

        [Route("GetExpensesList")]
        [HttpPost]
        public List<ExpensesRequestDto> GetExpensesList(Request<ExpensesRequestModel> req)
        {
            if (req.RequestObject.year == null) req.RequestObject.year = DateTime.Now.Year;
            return req.RequestObject.plant_id.HasValue ? exportDb.GetCostsByCultureByField((int)req.RequestObject.year, req.RequestObject.plant_id) : exportDb.GetCostsByCulture((int)req.RequestObject.year,null);
        }

        [Route("GetExpensesExportExcel")]
        [HttpGet]
        public HttpResponseMessage GetExpensesExportExcel(int? year = null)
        {
            if (year == null) year = DateTime.Now.Year;
            try
            {
                var templatePath = HttpContext.Current.Server.MapPath("~/ExcelReports/ExpensessTemplate.xlsx");
                byte[] byteArray = exportDb.GetExpensesExportExcel(templatePath,(int) year);
                var isIe = Request.Headers.UserAgent.ToString().Contains("Trident");
                HttpResponseMessage result = new HttpResponseMessage(HttpStatusCode.OK);
                if ((byteArray != null))
                {
                    var stream = new MemoryStream(byteArray);
                    var FileName = "Отчет по затратам на c/x техникку за  " + year + " год.xlsx";
                    result.Content = new StreamContent(stream);
                    result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/vnd.ms-excel");

                    //   result.Content.Headers.Add("content-disposition", "attachment; filename=);
                    result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
                    {
                        FileName = isIe ? HttpUtility.UrlEncode(FileName) : (FileName)
                    };
                }
                else
                {
                    result.StatusCode = HttpStatusCode.NoContent;
                }
                return result;
            }
            catch (Exception ex)
            {
                HttpResponseMessage errorResult = new HttpResponseMessage(HttpStatusCode.BadRequest);
                errorResult.ReasonPhrase = ex.ToString();
                return errorResult;
            }

        }


        //[Route("GetEmployeesWortTimeExportExcel")]
        //[HttpGet]
        //public HttpResponseMessage GetExpensesExportExcel(DateTime startDate , DateTime endDate, int? position = null)
        //{
        //    endDate = endDate.AddDays(1);
        //    try
        //    {
        //        var templatePath = HttpContext.Current.Server.MapPath("~/ExcelReports/TimeSheetTemp.xlsx");
        //        byte[] byteArray = exportDb.GetEmployeesWortTimeExportExcel(templatePath, (DateTime)startDate, (DateTime)endDate, position);
        //        var isIe = Request.Headers.UserAgent.ToString().Contains("Trident");
        //        HttpResponseMessage result = new HttpResponseMessage(HttpStatusCode.OK);
        //        if ((byteArray != null))
        //        {
        //            var stream = new MemoryStream(byteArray);
        //            var FileName = "Табель учета использования рабочего времени c "   + startDate.ToString("dd.MM.yyyy") + " по " + endDate.ToString("dd.MM.yyyy") + ".xlsx";
        //            result.Content = new StreamContent(stream);
        //            result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/vnd.ms-excel");

        //            //   result.Content.Headers.Add("content-disposition", "attachment; filename=);
        //            result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
        //            {
        //                FileName = isIe ? HttpUtility.UrlEncode(FileName) : (FileName)
        //            };
        //        }
        //        else
        //        {
        //            result.StatusCode = HttpStatusCode.NoContent;
        //        }
        //        return result;
        //    }
        //    catch (Exception ex)
        //    {
        //        HttpResponseMessage errorResult = new HttpResponseMessage(HttpStatusCode.BadRequest);
        //        errorResult.ReasonPhrase = ex.ToString();
        //        return errorResult;
        //    }

        //}

        //списание тмц отчет
        [Route("GetWriteOffExportExcel")]
        [HttpGet]
        public HttpResponseMessage GetWriteOffExportExcel(DateTime startDate , DateTime endDate , int? employeerId = null, int? materialId=null, int? fieldId = null, int? plantId = null)
        {

            var templatePath = HttpContext.Current.Server.MapPath("~/ExcelReports/WriteOffTemplate.xlsx");
            byte[] byteArray = exportDb.GetWriteOffExportExcel(startDate, endDate, templatePath, employeerId, fieldId, plantId, materialId);
            try
            {
                var isIe = Request.Headers.UserAgent.ToString().Contains("Trident");
                HttpResponseMessage result = new HttpResponseMessage(HttpStatusCode.OK);

                if ((byteArray != null))
                {
                    var stream = new MemoryStream(byteArray);
                    var FileName = "Списание ТМЦ" + ".xlsx";
                    result.Content = new StreamContent(stream);
                    result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/vnd.ms-excel");

                    result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
                    {
                        FileName = isIe ? HttpUtility.UrlEncode(FileName) : (FileName)
                    };
                }
                else
                {
                    result.StatusCode = HttpStatusCode.NoContent;
                }
                return result;
            }
            catch (Exception ex)
            {
                HttpResponseMessage errorResult = new HttpResponseMessage(HttpStatusCode.BadRequest);
                errorResult.ReasonPhrase = ex.ToString();
                return errorResult;
            }

        }
        //сдельная ЗП отчет
        [Route("GetDocResExcel")]
        [HttpGet]
        public HttpResponseMessage GetDocResExcel(DateTime startDate, DateTime endDate, int? empId = null, Boolean check=false)
        {
            byte[] byteArray=null;
            if (!check)
            {

                var templatePath = HttpContext.Current.Server.MapPath("~/ExcelReports/DocTemplate.xlsx");
                byteArray = exportDb.GetDocResExcel(startDate, endDate, templatePath, empId);
            }
            else {
                var templatePath = HttpContext.Current.Server.MapPath("~/ExcelReports/FullDocTemplate.xlsx");
                byteArray = exportDb.GetFullDocResExcel(startDate, endDate, templatePath, empId);
            }

            try
            {
                var isIe = Request.Headers.UserAgent.ToString().Contains("Trident");
                HttpResponseMessage result = new HttpResponseMessage(HttpStatusCode.OK);

                if ((byteArray != null))
                {
                    var stream = new MemoryStream(byteArray);
                    var FileName = "Сдельная ЗП" + ".xlsx";
                    result.Content = new StreamContent(stream);
                    result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/vnd.ms-excel");

                    result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
                    {
                        FileName = isIe ? HttpUtility.UrlEncode(FileName) : (FileName)
                    };
                }
                else
                {
                    result.StatusCode = HttpStatusCode.NoContent;
                }
                return result;
            }
            catch (Exception ex)
            {
                HttpResponseMessage errorResult = new HttpResponseMessage(HttpStatusCode.BadRequest);
                errorResult.ReasonPhrase = ex.ToString();
                return errorResult;
            }

        }
        //наряд на сдельную работу
        [Route("GetPieceWorkResExcel")]
        [HttpGet]
        public HttpResponseMessage GetPieceWorkResExcel(DateTime startDate, int emp_id, DateTime endDate)
        {
            byte[] byteArray = null;

            var templatePath = HttpContext.Current.Server.MapPath("~/ExcelReports/RepairPieceWorkTemplate.xlsx");
            byteArray = exportDb.GetPieceWorkResExcel(startDate,endDate, emp_id, templatePath);

            try
            {
                var isIe = Request.Headers.UserAgent.ToString().Contains("Trident");
                HttpResponseMessage result = new HttpResponseMessage(HttpStatusCode.OK);

                if ((byteArray != null))
                {
                    var stream = new MemoryStream(byteArray);
                    var FileName = "Наряд на сдельную работу" + ".xlsx";
                    result.Content = new StreamContent(stream);
                    result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/vnd.ms-excel");

                    result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
                    {
                        FileName = isIe ? HttpUtility.UrlEncode(FileName) : (FileName)
                    };
                }
                else
                {
                    result.StatusCode = HttpStatusCode.NoContent;
                }
                return result;
            }
            catch (Exception ex)
            {
                HttpResponseMessage errorResult = new HttpResponseMessage(HttpStatusCode.BadRequest);
                errorResult.ReasonPhrase = ex.ToString();
                return errorResult;
            }

        }

        //Планируемые затраты
        [Route("GetCultResExcel")]
        [HttpGet]
        public HttpResponseMessage GetСultResExcel(int cultId, int year)
        {
            byte[] byteArray = null;
            var templatePath = HttpContext.Current.Server.MapPath("~/ExcelReports/PlanTemplate.xlsx");
            byteArray = exportDb.GetСultResExcel(cultId, year, templatePath); 
            try
            {
                var isIe = Request.Headers.UserAgent.ToString().Contains("Trident");
                HttpResponseMessage result = new HttpResponseMessage(HttpStatusCode.OK);

                if ((byteArray != null))
                {
                    var stream = new MemoryStream(byteArray);
                    var FileName = "Планируемые затраты" + ".xlsx";
                    result.Content = new StreamContent(stream);
                    result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/vnd.ms-excel");

                    result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
                    {
                        FileName = isIe ? HttpUtility.UrlEncode(FileName) : (FileName)
                    };
                }
                else
                {
                    result.StatusCode = HttpStatusCode.NoContent;
                }
                return result;
            }
            catch (Exception ex)
            {
                HttpResponseMessage errorResult = new HttpResponseMessage(HttpStatusCode.BadRequest);
                errorResult.ReasonPhrase = ex.ToString();
                return errorResult;
            }

        }
        // себестоимость по культурам
        [Route("GetPlanFactExcel")]
        [HttpGet]
        public HttpResponseMessage GetPlanFactExcel(DateTime? startDate, DateTime? endDate, int? plantId, int year,
        Boolean plan, Boolean fact, Boolean detal, string date)
        {
            byte[] byteArray = null;
            var templatePath = "";
            if (plan && fact)
            {
                templatePath = HttpContext.Current.Server.MapPath("~/ExcelReports/PlanFactTemplate.xlsx");
            } else
                {
                templatePath = HttpContext.Current.Server.MapPath("~/ExcelReports/PlanTemplate.xlsx");
                }

                byteArray = exportDb.GetPlanFactResExcel(startDate, endDate, plantId, templatePath,detal, plan, fact, date);
          


            try
            {
                var isIe = Request.Headers.UserAgent.ToString().Contains("Trident");
                HttpResponseMessage result = new HttpResponseMessage(HttpStatusCode.OK);

                if ((byteArray != null))
                {
                    var stream = new MemoryStream(byteArray);
                    string FileName = null;
                    if (plan && fact)
                    {
                         FileName = "План-факт отчет по культурам" + ".xlsx";
                    }
                    if (plan && !fact)
                    {
                         FileName = "Планируемые затраты культур за период c " + startDate.Value.ToString("dd.MM.yyyy") + " по " + endDate.Value.ToString("dd.MM.yyyy") + ".xlsx";
                    }
                    if (!plan && fact)
                    {
                         FileName = "Фактические затраты культур за период c " + startDate.Value.ToString("dd.MM.yyyy") + " по " + endDate.Value.ToString("dd.MM.yyyy") + ".xlsx";
                    }
                    result.Content = new StreamContent(stream);
                    result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/vnd.ms-excel");

                    result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
                    {
                        FileName = isIe ? HttpUtility.UrlEncode(FileName) : (FileName)
                    };
                }
                else
                {
                    result.StatusCode = HttpStatusCode.NoContent;
                }
                return result;
            }
            catch (Exception ex)
            {
                HttpResponseMessage errorResult = new HttpResponseMessage(HttpStatusCode.BadRequest);
                errorResult.ReasonPhrase = ex.ToString();
                return errorResult;
            }

        }
        //

        //отстатки ТМЦ

        [Route("GetResponsablesExcel")]
        [HttpGet]
        public HttpResponseMessage GetResponsables(int? employeerId)
        {
            try
            {
                var templatePath = HttpContext.Current.Server.MapPath("~/ExcelReports/RespoTemp.xlsx");
                byte[] byteArray = exportDb.GetResponsablesExcel(templatePath, employeerId);
                var isIe = Request.Headers.UserAgent.ToString().Contains("Trident");
                HttpResponseMessage result = new HttpResponseMessage(HttpStatusCode.OK);
                if ((byteArray != null))
                {
                    var stream = new MemoryStream(byteArray);
                    var FileName = "Отчет по тмц.xlsx";
                    result.Content = new StreamContent(stream);
                    result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/vnd.ms-excel");

                    //   result.Content.Headers.Add("content-disposition", "attachment; filename=);
                    result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
                    {
                        FileName = isIe ? HttpUtility.UrlEncode(FileName) : (FileName)
                    };
                }
                else
                {
                    result.StatusCode = HttpStatusCode.NoContent;
                }
                return result;
            }
            catch (Exception ex)
            {
                HttpResponseMessage errorResult = new HttpResponseMessage(HttpStatusCode.BadRequest);
                errorResult.ReasonPhrase = ex.ToString();
                return errorResult;
            }
        }  
        //
        [Route("GetReportTrucksExcel")]
        [HttpGet]
        public HttpResponseMessage GetReportTrucksExcel(DateTime startDate, DateTime endDate)
        {
            try
            {
                var templatePath = HttpContext.Current.Server.MapPath("~/ExcelReports/TruckTemp.xlsx");
                byte[] byteArray = exportDb.GetReportTrucksExcel(templatePath, startDate, endDate);
                var isIe = Request.Headers.UserAgent.ToString().Contains("Trident");
                HttpResponseMessage result = new HttpResponseMessage(HttpStatusCode.OK);
                if ((byteArray != null))
                {
                    var stream = new MemoryStream(byteArray);
                    var FileName = "Отчет по грузовой технике c " + startDate.ToString("dd.MM.yyyy") + " по " + endDate.ToString("dd.MM.yyyy") + ".xlsx";
                    result.Content = new StreamContent(stream);
                    result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/vnd.ms-excel");

                    //   result.Content.Headers.Add("content-disposition", "attachment; filename=);
                    result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
                    {
                        FileName = isIe ? HttpUtility.UrlEncode(FileName) : (FileName)
                    };
                }
                else
                {
                    result.StatusCode = HttpStatusCode.NoContent;
                }
                return result;
            }
            catch (Exception ex)
            {
                HttpResponseMessage errorResult = new HttpResponseMessage(HttpStatusCode.BadRequest);
                errorResult.ReasonPhrase = ex.ToString();
                return errorResult;
            }
        }

    }
}