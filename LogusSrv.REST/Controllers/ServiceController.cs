﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using LogusSrv.CORE.REST;
using LogusSrv.DAL.Operations;
using LogusSrv.DAL.Entities.Res;
using LogusSrv.DAL.Entities.Req;
using LogusSrv.DAL.Entities.DTO;
using LogusSrv.DAL.Entities.DTO.ActsSzr;

namespace LogosSrv.Controllers
{
   
    [RoutePrefix("api/ServiceCtrl")]
    public class ServiceController : BaseCtrl
    {
        public ServiceDb _serviceDb = new ServiceDb();

        //услуги и единицы 
        [Route("GetInfoService")]
        [HttpPost]
        public Response<InfoService> GetInfoService()
        {
            return Perform(() => _serviceDb.GetInfoService());
        }

        //сохранение
        [Route("AddService")]
        [HttpPost]
        public Response<string> AddService(Request<List<ServiceModel>> req)
        {
            return Perform(() => _serviceDb.AddService(req.RequestObject));
        }

        //вывод
        [Route("GetService")]
        [HttpPost]
        public Response<ServiceListResponse> GetServiceDetails(Request<GridDataReq> req)
        {
            return Perform(() => _serviceDb.GetServiceDetails(req.RequestObject));
        }

        //удалить запись
        [Route("DeleteService")]
        [HttpPost]
        public Response<string> deleteService(Request<List<DelRow>> req)
        {
            return Perform(() => _serviceDb.DeleteService(req.RequestObject));
        }

        [Route("UpdateService")]
        [HttpPost]
        public Response<int> UpdateService(Request<List<SaveService>> req)
        {
            return Perform(() => _serviceDb.UpdateService(req.RequestObject));
        }

    }


}