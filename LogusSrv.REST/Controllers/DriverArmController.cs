﻿using System;
using System.Collections.Generic;
using System.Web.Http;
using LogusSrv.DAL.Operations;
using LogusSrv.DAL.Entities.DTO;
using LogusSrv.DAL.Entities.Req;
using LogusSrv.DAL.Entities.Res;
using LogusSrv.DAL.Entities.DTO.DriverArm;
using LogusSrv.CORE.REST;
using LogusSrv.DAL.Entities.DTO.ActsSzr;
using LogusSrv.DAL.Entities;
using Newtonsoft.Json.Linq;

namespace LogosSrv.Controllers
{
    [RoutePrefix("api/DriverArmCtrl")]
    public class DriverArmController : ApiController
    {

        public DriverArmDb driverArmDb = new DriverArmDb();

        [Route("GetXmlFromServer")]
        [HttpGet]
        public string GetXmlFromServer()
        {
            return driverArmDb.GetXmlFromServer();
        }

        [Route("GetDataFromAutoGraph")]
        [HttpGet]
        public string GetDataFromAutoGraph()
        {
            return driverArmDb.GetDataFromAutoGraph();
        }



        [Route("GetGPSWialonData")]
        [HttpGet]
        public String GetGPSWialonData()
        {
            return driverArmDb.GetGPSWialonData();
        }

        [Route("TestWialonData")]
        [HttpGet]
        public JObject TestWialonData()
        {
            return driverArmDb.TestWialonData();
        }




        [Route("GetCoordOrg")]
        [HttpPost]
        public List<double> GetCoordOrg(Request<GrossReq> req)
        {
            return driverArmDb.GetCoordOrg(req.RequestObject.OrgId);
        }

        //DRIVER: для планшета
        [Route("GetInfoWaysTaskByIdTraktor2")]
        [HttpPost]
        public ArmWaysTaskInfoModel GetInfoWaysTaskByIdTraktor2(ArmWaysListReq req)
        {
            return driverArmDb.GetInfoWaysTaskByIdTraktor((int)req.traktor_id, DateTime.ParseExact(req.date, "yyyy-MM-dd HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture));
        }

        //DRIVER: обновить статус
        [Route("SetStatusWaysListToInWork")]
        [HttpPost]
        public string SetSatusWaysListToInWork(ArmWaysListReq req)
        {
            return driverArmDb.SetSatusWaysListToInWork(req);
        }

        //DRIVER: получить информацию о всех полях
        [Route("GetInfoByFields")]
        [HttpGet]
        public List<FieldArmDTO> GetInfoByFields()
        {
            return driverArmDb.GetInfoByFields();
        }

        //DRIVER: получить информацию о всех тракторах
        [Route("GetLastCoordByTraktors")]
        [HttpGet]
        public List<CoordByTraktorDTO> GetLastCoordByTraktors()
        {
            return driverArmDb.GetLastCoordByTraktors();
        }

        //DRIVER: вставить массив координат
        [Route("PostCoords")]
        [HttpPost]
        public string InsertCoords(List<GPSDTOfromDriver> gps_list)
        {
            return driverArmDb.InsertCoords(gps_list);
        }

        //КАРТА: карты в Web
        [Route("GetInfoWaysTaskByIdTraktor")]
        [HttpPost]
        public ArmWaysTaskInfoModel GetInfoWaysTaskByIdTraktor(Request<ArmWaysListReq> req)
        {
            return driverArmDb.GetInfoWaysTaskByIdTraktor((int)req.RequestObject.traktor_id);
        }

        //КАРТА: получить последние координаты трека
        [Route("GetTop100GPSbyTrackId")]
        [HttpPost]
        public GPSArmModel GetTop100GPSbyTrackId(Top100GPSReq req)
        {
            return driverArmDb.GetTop100GPSbyTrackId(req.track_id,req.parking);
        }

        //КАРТА WEB: слой полей  для карты ФАКТ
        [Route("GetFieldsGeoFactJsonCoordinaties")]
        [HttpGet]
        public GeoFieldsAndPlantsMapDto GetFieldsGeoFactJsonCoordinaties(int? year)
        {
            year = year.HasValue ? year.Value : DateTime.Now.Year;
            return driverArmDb.GetFieldsGeoFactJsonCoordinaties(year.Value);
        }   

        //КАРТА: получить последние 5 задач, выполненных на поле
        [Route("GetTop5WaysListbyFieldId")]
        [HttpPost]
        public List<Top5TasksInfoModel> GetTop5WaysListbyFieldId(Request<Top5TaskReq> req)
        {
            return driverArmDb.GetTop5WaysListbyFieldId(req.RequestObject.id);
        }

        //КАРТА: информация по выполненному заданию
        [Route("GetTaskInfoById")]
        [HttpPost]
        public TaskInfoDTO GetTaskInfoById(Top5TaskReq req)
        {
            return driverArmDb.GetTaskInfoById(req.id);
        }

        //КАРТА WEB: слой полей для карты
        [Route("GetFieldsGeoJsonCoordinaties")]
        [HttpGet]
        public GeoFieldsAndPlantsMapDto GetFieldsGeoJsonCoordinaties(int? year)
        {
            year = year.HasValue ? year.Value : DateTime.Now.Year;
            return driverArmDb.GetFieldsGeoJsonCoordinaties(year.Value);
        }

        //КАРТА WEB: слой техники
        [Route("GetTraktorsGeoJsonInfo")]
        [HttpGet]
        public GeoTraktorMapDto GetTraktorsGeoJsonInfo()
        {
            return driverArmDb.GetTraktorsGeoJsonInfo();
        }

        //ЗАДАЧА ПО РАСПИСАНИЮ: перенос точек в архивную таблицу старше 3 месяцев
        [Route("ArchiveDB")]
        [HttpGet]
        public string ArchiveDB()
        {
          return driverArmDb.ArchiveDB();
        }
        [Route("ArchiveDB1")]
        [HttpGet]
        public string ArchiveDB1()
        {
            return driverArmDb.ArchiveDB1();
        }


        //НЕ ИСПОЛЬЗУЮТСЯ
        [Route("GetInfoDrivers")]
        [HttpGet]
        public List<ArmDriverInfoDTO> GetInfoDrivers()
        {
            return driverArmDb.GetInfoDrivers();
        }

        [Route("InsertGPSbyTrack")]
        [HttpPost]
        public GPSDTO InsertGPSbyTrack(GPSDTO req)
        {
            return driverArmDb.InsertGPSbyTrack(req);
        }

        [Route("GetTaskList")]
        [HttpGet]
        public List<DictionaryItemsDTO>  GetTaskList()
        {
            return driverArmDb.GetTaskList();
        }

        [Route("GetEquiList")]
        [HttpGet]
        public List<DictionaryItemsTraktors> GetEquiList()
        {
            return driverArmDb.GetEquiList();
        }

        [Route("PostTask")]
        [HttpPost]
        public TaskArmDTO PostTask(TaskArmDTO task)
        {
            return driverArmDb.PostTask(task);
        }
        ///Aleksey
        [Route("GetFieldInfoById")]
        [HttpPost]

        public List<ProgressMap> GetFieldInfoById(int id, int year, int fieldId)
        {
            return driverArmDb.GetFieldInfoById(id, year, fieldId);
        }

        [Route("GetPlantOnField")]
        [HttpPost]

        public List<ProgressInfoField> GetPlantOnField(int year, int fieldId)
        {
            return driverArmDb.GetPlantOnField(fieldId, year);

        }

        [Route("GetFieldCoordinatGeoProgress")]
        [HttpGet]
        public GeoFieldsAndPlantsMapDto GetFieldCoordinatGeoProgress(int year, int fieldId)
        {
            return driverArmDb.GetFieldCoordinatGeoProgress(year, fieldId);
        }

        [Route("GetGPSbyDate")]
        [HttpPost]
        public List<Top100GPSmodel> GetGPSbyDate(Top100GPSReq req)
        {
            return driverArmDb.GetGPSbyDate(req.track_id, req.start_time, req.end_time, req.stop, req.parking);
        }

        [Route("GetTMCforMap")]
        [HttpPost]
        public List<GetTMCMao> GetTMCforMap(Request<TMCMapReq> req)
        {

            return driverArmDb.GetTMCforMap(req.RequestObject.fieldId, req.RequestObject.year);
        }

        [Route("GetPhotoByFieldYear")]
        [HttpPost]
        public List<ImageFieldDto> GetPhotoByFieldYear(Request<TMCMapReq> req)
        {

            return driverArmDb.GetPhotoByFieldYear(req.RequestObject.fieldId, req.RequestObject.year);
        }
      


    }
}