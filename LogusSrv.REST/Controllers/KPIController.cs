﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using LogusSrv.CORE.REST;
using LogusSrv.DAL.Entities.DTO;
using LogusSrv.DAL.Entities.Req;
using LogusSrv.DAL.Entities.Res;
using LogusSrv.DAL.Operations;

namespace LogosSrv.Controllers
{
    [RoutePrefix("api/KpiCtrl")]
    public class KPIController : BaseCtrl
    {
        public KPIDb kpiDB = new KPIDb();

        [Route("KPIInfo")]
        [HttpGet]
        public Response<KPIInfoRes> GetKPIInfo(int? year)
        {
            year = year.HasValue ? year.Value : DateTime.Now.Year;
            return Perform(() => kpiDB.GetKPIInfo(year.Value));
        }

        [Route("KPIWorks")]
        [HttpPost]
        public Response<KPIResModel> KPIWorks(Request<KPIReqModel> req)
        {
            return Perform(() => kpiDB.GetWorksWithKPI(req.RequestObject.year, req.RequestObject.id_tc));
        }

        [Route("GetKPI")]
        [HttpPost]
        public Response<KPIForTaskResModel> GetKPI(Request<int> tasId)
        {
            return Perform(() => kpiDB.GetKPI(tasId.RequestObject));
        }

        [Route("AddKPIValue")]
        [HttpPost]
        public Response<string> AddKPIValue(int TaskId, int TotalRate, Request<List<KPIValueResModel>> req)
        {
            return Perform(() => kpiDB.AddKPIValue(TaskId, TotalRate, req.RequestObject));
        }

        [Route("GetTasksByPlantYear")]
        [HttpPost]
        public Response<List<DictionaryItemsDTO>> GetTasksByPlantYear(Request<KPIReqModel> req)
        {
            return Perform(() => kpiDB.GetWorksListByPlant(req.RequestObject));
        }

        [Route("AddTasksToWork")]
        [HttpPost]
        public Response<KPIResModel> AddTasksToWork(Request<TasksToUpdate> req)
        {
            return Perform(() => kpiDB.AddTasksToWork(req.RequestObject));
        }

        [Route("DeleteTasksFromWork")]
        [HttpGet]
        public Response<string> DeleteTasksFromWork(int tasId)
        {
            return Perform(() => kpiDB.DeleteTasksFromWork(tasId));
        } 
    }
}