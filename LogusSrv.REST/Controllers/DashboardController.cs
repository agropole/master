﻿using System.Web.Http;
using LogusSrv.CORE.REST;
using LogusSrv.DAL.Operations;
using LogusSrv.DAL.Entities.Req;
using LogusSrv.DAL.Entities.Res;
using System.Collections.Generic;
using LogusSrv.DAL.Entities.DTO;


namespace LogosSrv.Controllers
{
    [RoutePrefix("api/Dashboard")]
    public class DashboardController : BaseCtrl
    {
        private DashboardDb dashDb = new DashboardDb();
        [Route("GetCosts")]
        [HttpPost]
        public Response<DashboardResModel> GetCosts(Request<int> req)
        {
            return Perform(() => dashDb.GetCosts(req.RequestObject));
        }

        [Route("GetPlants")]
        [HttpGet]
        public Response<List<PlantResModel>> GetPlants()
        {
            return Perform(() => dashDb.GetPlants()); 
        }

        //PRINT MAP
        [Route("GetStructureCrop")]
        [HttpPost]
        public Response<List<PlantsDTO>> GetStructureCrop(Request<int> req)
        {
            return Perform(() => dashDb.GetStructureCrop(req.RequestObject));
        }

    }
}