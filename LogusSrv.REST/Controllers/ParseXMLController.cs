﻿using System.Web.Http;
using LogusSrv.CORE.REST;
using LogusSrv.DAL.Operations;
using LogusSrv.DAL.Entities.Req;
using LogusSrv.DAL.Entities.Res;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using LogusSrv.DAL.Entities.DTO.FileArchive;
using System;


namespace LogosSrv.Controllers
{

    [RoutePrefix("api/ParseXMLController")]
    public class ParseXMLController : BaseCtrl
    {
        [Route("ParseMaterial")]
        [HttpPost]
        public string ParseMaterial()
        {
            try
            {
                string result = String.Empty;
                if (Request.Content.IsMimeMultipartContent())
                {
                    //Копирование во временный стрим и WriteLine в конце - фикс для одной из версий веб апи. Вероятно уже пофикшено, но если будет вылезать ошибка с отсутствием конца строки, то:
                    MemoryStream tempStream = new MemoryStream();

                    using (Stream reqStream = Request.Content.ReadAsStreamAsync().Result)
                    {
                        var i = reqStream.Length;
                        reqStream.CopyTo(tempStream);
                    }

                    tempStream.Seek(0, SeekOrigin.End);
                    StreamWriter writer = new StreamWriter(tempStream);
                    writer.WriteLine();
                    writer.Flush();
                    tempStream.Position = 0;

                    StreamContent streamContent = new StreamContent(tempStream);
                    foreach (var header in Request.Content.Headers)
                    {
                        streamContent.Headers.Add(header.Key, header.Value);
                    }

                    streamContent.ReadAsMultipartAsync(
                        new MultipartMemoryStreamProvider()).ContinueWith((tsk) =>
                        {
                            MultipartMemoryStreamProvider prvdr = tsk.Result;

                            foreach (HttpContent ctnt in prvdr.Contents)
                            {
                                var fileStatus = new FilesLoadStatus();
                                FileUploadDto filemodal = new FileUploadDto()
                                {
                                    contents = ctnt.ReadAsByteArrayAsync().Result,
                                    file_name = ctnt.Headers.ContentDisposition.FileName.Trim('"'),
                                };

                                fileStatus.name = filemodal.file_name;
                                try
                                {
                                    var parser = new ParseXML();
                                    result = parser.ParseMaterial(filemodal);
                                }
                                catch (Exception ex)
                                {
                                }

                            }
                        }).Wait();
                }
                return result.Equals(String.Empty) ? "success" : result;

            }
            catch (Exception ex)
            {
                return "error";
            }
        }


        [Route("ParseFuelCost")]
        [HttpPost]
        public string ParseFuelCost()
        {
            try
            {
                string result = String.Empty;
                if (Request.Content.IsMimeMultipartContent())
                {
                    //Копирование во временный стрим и WriteLine в конце - фикс для одной из версий веб апи. Вероятно уже пофикшено, но если будет вылезать ошибка с отсутствием конца строки, то:
                    MemoryStream tempStream = new MemoryStream();

                    using (Stream reqStream = Request.Content.ReadAsStreamAsync().Result)
                    {
                        var i = reqStream.Length;
                        reqStream.CopyTo(tempStream);
                    }

                    tempStream.Seek(0, SeekOrigin.End);
                    StreamWriter writer = new StreamWriter(tempStream);
                    writer.WriteLine();
                    writer.Flush();
                    tempStream.Position = 0;

                    StreamContent streamContent = new StreamContent(tempStream);
                    foreach (var header in Request.Content.Headers)
                    {
                        streamContent.Headers.Add(header.Key, header.Value);
                    }

                    streamContent.ReadAsMultipartAsync(
                        new MultipartMemoryStreamProvider()).ContinueWith((tsk) =>
                        {
                            MultipartMemoryStreamProvider prvdr = tsk.Result;

                            foreach (HttpContent ctnt in prvdr.Contents)
                            {
                                var fileStatus = new FilesLoadStatus();
                                FileUploadDto filemodal = new FileUploadDto()
                                {
                                    contents = ctnt.ReadAsByteArrayAsync().Result,
                                    file_name = ctnt.Headers.ContentDisposition.FileName.Trim('"'),
                                };

                                fileStatus.name = filemodal.file_name;
                                try
                                {
                                    var parser = new ParseXMLFuel();
                                    result = parser.ParseFuel(filemodal);
                                }
                                catch (Exception ex)
                                {
                                }

                            }
                        }).Wait();
                }
                return result.Equals(String.Empty) ? "success" : result;

            }
            catch (Exception ex)
            {
                return "error";
            }
        }

        [Route("ParseGrossHarvest")]
        [HttpPost]
        public string ParseGrossHarvest()
        {
            try
            {
                string result = String.Empty;
                if (Request.Content.IsMimeMultipartContent())
                {
                    //Копирование во временный стрим и WriteLine в конце - фикс для одной из версий веб апи. Вероятно уже пофикшено, но если будет вылезать ошибка с отсутствием конца строки, то:
                    MemoryStream tempStream = new MemoryStream();

                    using (Stream reqStream = Request.Content.ReadAsStreamAsync().Result)
                    {
                        var i = reqStream.Length;
                        reqStream.CopyTo(tempStream);
                    }

                    tempStream.Seek(0, SeekOrigin.End);
                    StreamWriter writer = new StreamWriter(tempStream);
                    writer.WriteLine();
                    writer.Flush();
                    tempStream.Position = 0;

                    StreamContent streamContent = new StreamContent(tempStream);
                    foreach (var header in Request.Content.Headers)
                    {
                        streamContent.Headers.Add(header.Key, header.Value);
                    }

                    streamContent.ReadAsMultipartAsync(
                        new MultipartMemoryStreamProvider()).ContinueWith((tsk) =>
                        {
                            MultipartMemoryStreamProvider prvdr = tsk.Result;

                            foreach (HttpContent ctnt in prvdr.Contents)
                            {
                                var fileStatus = new FilesLoadStatus();
                                FileUploadDto filemodal = new FileUploadDto()
                                {
                                    contents = ctnt.ReadAsByteArrayAsync().Result,
                                    file_name = ctnt.Headers.ContentDisposition.FileName.Trim('"'),
                                };

                                fileStatus.name = filemodal.file_name;
                                try
                                {
                                    var parser = new ParseXMLGrossHarvest();
                                    result = parser.ParseGrossHarvest(filemodal);
                                }
                                catch (Exception ex)
                                {
                                }

                            }
                        }).Wait();
                }
                return result.Equals(String.Empty) ? "success" : result;

            }
            catch (Exception ex)
            {
                return "error";
            }
        }

    }


}