﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Net.Http;
using System.Web.Caching;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Mvc;
using LogusSrv.CORE.Exceptions;
using LogusSrv.CORE.REST;
using LogusSrv.DAL.Entities.Req;
using ModelStateDictionary = System.Web.Http.ModelBinding.ModelStateDictionary;

namespace LogosSrv.Controllers
{
    public class BaseCtrl : ApiController
    {
        public Erorr GetErorrList(ModelStateDictionary model)
        {
            Erorr erorrList = new Erorr();
            erorrList.ErorrCode = 1;
            erorrList.ErorrText = new List<string>();
            foreach (var item in ModelState)
            {
                var error = item.Value.Errors.FirstOrDefault().ErrorMessage;
                erorrList.ErorrText.Add(error);
            }
            return erorrList;
        }

        public Response<TRes> Perform<TRes>(Func<TRes> func)
        {
            Response<TRes> response = null;
            try
            {
                if (!ModelState.IsValid)
                {
                    var eList = (from value in ModelState.Values
                                 from error in value.Errors
                                 select !string.IsNullOrEmpty(error.ErrorMessage) ? error.ErrorMessage : error.Exception.Message)
                                 .ToList();
                 var  errList = eList.Distinct().ToList();
                    throw new BllException(2, "Произошли следующие ошибки:", errList);
                }
                else
                {
                    var actionResult = func();
                    response = new Response<TRes>(actionResult);
                }
            }
            catch (BllException ex)
            {
                response = new Response<TRes>(default(TRes), ex.ErrorCode, ex.ErrorMessage, ex.ErrorList);
            }
            catch (Exception ex)
            {
                response = new Response<TRes>(default(TRes), 1, ex.ToString());
            }

            if (response != null)
                response.ServiceVersion = ConfigurationManager.AppSettings["ServiceVersion"];

            return response;
        }
    }
}