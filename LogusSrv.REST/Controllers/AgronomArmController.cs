﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using LogusSrv.CORE.REST;
using LogusSrv.DAL.Operations;
using LogusSrv.DAL.Entities.Res;
using LogusSrv.DAL.Entities.Req;
using LogusSrv.DAL.Entities.DTO;
using LogusSrv.DAL.Entities.DTO.ActsSzr;
using LogusSrv.DAL.Entities;
using System.Net.Http;
using System.Net.Http.Headers;
using System.IO;
using LogusSrv.DAL.Entities.DTO.ExportExcel;
using DocumentFormat.OpenXml.Wordprocessing;
using System.Net;
using LogusSrv.DAL.Operations.DictionaryOperations;
using LogusSrv.DAL.DBModel;
using System.Threading.Tasks;

namespace LogosSrv.Controllers
{
    [RoutePrefix("api/AgronomArmCtrl")]
    public class AgronomArmController : ApiController
    {
        public AgronomArmDb agrodb = new AgronomArmDb();
        public ActsSrzDb actSrzDb = new ActsSrzDb();
        public ReportsModuleDb reportDb = new ReportsModuleDb();
        public LoginDb loginDb = new LoginDb();
        public AdministrationDb adminDb = new AdministrationDb();  
        public DayTasksDb dayTasksDb = new DayTasksDb();
        public FieldsDict fieldDist = new FieldsDict();
        protected readonly LogusDBEntities db = new LogusDBEntities();
        //android
        [Route("UpdateFields")]
        [HttpPost]
        public string UpdateFields1(List<ModelUpdateFields> updateCollection)
        {
            return fieldDist.UpdateFieldsDict(updateCollection,  db);
        }
        //не используется
        //[Route("GetCostsByCulture")]
        //[HttpPost]
        //public List<ExpensesRequestDto> GetCostsByCulture(DictionaryItemsDTO list)
        //{
        //    return reportDb.GetCostsByCulture((int)list.Id, null);
        //}

        //////фактические затраты - планшет
        [Route("GetSpendingByFields")]
        [HttpPost]
        public List<ExpensesRequestDto> GetSpendingByFields(UpdateDayTaskReq req)
        {
            return agrodb.GetSpendingByFields(req.OrgId);
        }

        //планируемые затраты - планшет
        [Route("GetSpendingPlanByFields")]
        [HttpPost]
        public List<PlanRequestDto> GetSpendingPlanByFields(UpdateDayTaskReq req)
        {
            return agrodb.GetSpendingPlanByFields(req.OrgId);
        }
        //задания за год
        [Route("GetSheetTasksFromYear")]
        [HttpPost]
        public List<TaskArmDTO> GetSheetTasksFromYear(UpdateDayTaskReq req)
        {
            return agrodb.GetSheetTasksFromYear(req.OrgId);
        }

        //затраты на ремонт и топливо
        [Route("GetFuelRepairsFromYear")]
        [HttpPost]
        public MoveSpareParts GetFuelRepairsFromYear(UpdateDayTaskReq req)
        {
            return agrodb.GetFuelRepairsFromYear(req.OrgId);
        }

        //Работы, выполненные на поле
        [Route("GetWayListInfo")]
        [HttpPost]
        public List<PrintWayListDto> GetWayListInfo(PrintWayListDto list) 
        {
            return agrodb.GetWayListInfo(list.Date, list.ShiftNum, list.TracktorId); 
        }

        //баланс ТМЦ
        [Route("GetBalanceMaterialsList")]
        [HttpPost]
        public List<BalanceMaterials> GetBalanceMaterialsList(UpdateDayTaskReq req)     
        {
            return agrodb.GetBalanceMaterialsList(req.OrgId);
        }

        //2
        [Route("GetTop10ActsSzr")]
        [HttpPost]
        public List<GetTop10ActsSzr> GetTop10ActsSzr(GetTop10ActsSzr list)
        {
            return agrodb.GetTop10ActsSzr(list.Id);
        }

        //save All acts
        [Route("SaveActsSzr")]
        [HttpPost]
        public String SaveActsSzr(List<GetTop10ActsSzr> list)
        {
            return agrodb.SaveActsSzr(list);
        }

        //справочники для списания акта  
        [Route("GetDictionaryForActs")]
        [HttpPost]
        public DictionatysActSzr GetDictionaryForActs(UpdateDayTaskReq req)
        {
            return agrodb.GetDictionaryTMC(req.OrgId);
        }

        //================= данные по акту списания 
        [Route("GetActSzrDetails")]
        [HttpPost]
        public ActSzrDetails GetActMaterialsDetails(DictionaryItemsDTO list)
        {
            return actSrzDb.GetActMaterialsDetails(list.Id.Value);
        }

        //сохранение, редактирование акта
       [Route("CreateUpdateActSzrDetail")]
       [HttpPost]
        public int CreateUpdateActSzrDetail(List<SaveRow> req)
        {
            return agrodb.CreateUpdateActSzrDetail(req);
        }

        //авторизация
       [Route("Login")]
       [HttpPost]
       public UserModel Login(LoginReq req)
       {
           var saltPostfix = System.Configuration.ConfigurationManager.AppSettings["SaltPostfix"];
           return loginDb.Login(req.Login, req.Password, saltPostfix);
       }
     
        //сменное задание, все справочники
       [Route("GetInfoForCreateDayTask")]
       [HttpPost]
       public WaysListForCreateModel GetInfoForCreateDayTasks(UpdateDayTaskReq req)
       {
           return agrodb.GetInfoForCreateDayTask(req);
       }

        //сменное задание сохранение
       [Route("UpdateSelectShiftTask")]
       [HttpPost]
       public int UpdateSelectShift(UpdateDayTaskReq req)
       {
           return dayTasksDb.UpdateSelectShiftTask(req);
       }

        //поступление ТМЦ
       [Route("GetIncomeSzr")]
       [HttpPost]
       public List<ActMaterials2> GetIncomeSzr(DictionaryItemsDTO list)
       {
           return agrodb.GetIncomeSzr(list.Id.Value);
       }

       [Route("SaveIncomeSzr")]
       [HttpPost]
       public String SaveIncomeSzr(List<ActMaterials2> list)
       {
           return agrodb.SaveIncomeSzr(list);
       }

       //ПФА  
       [Route("PlanFactArm")]
       [HttpPost]
       public TCPlanFactModel GetPlanFact(ExpensesRequestModel req)
       {
           if (req.year == null) req.year = DateTime.Now.Year;
           return reportDb.GetPlanFactTC((int)req.year, req.plant_id, 1);
       }

       [Route("GetExpensesListArm")]
       [HttpPost]
       public List<ExpensesRequestDto> GetExpensesList(ExpensesRequestModel req)
       {
           if (req.year == null) req.year = DateTime.Now.Year;
           return req.plant_id.HasValue ? reportDb.GetCostsByCultureByField((int)req.year, req.plant_id) :
               reportDb.GetCostsByCulture((int)req.year, null);
       }



       [Route("SaveImageFields")]
       [HttpPost]
       public HttpResponseMessage SaveImageFields()
       {
           using (var trans = db.Database.BeginTransaction())
           {
                try
                   {
              string filePath = System.Web.Hosting.HostingEnvironment.MapPath("/") + "photo_fields\\"; ;
              try
               {
                   var req = HttpContext.Current.Request;
                   var filenames = req.Form["name"];
                   var notes = req.Form["note"];
                   var dates = req.Form["date"];
                   var fiel_ids = req.Form["id"];
                   var markers = req.Form["marker"];
                   dates = String.Join("", dates.Split('"', '\''));
                   notes = String.Join("", notes.Split('"', '\''));
                   fiel_ids = String.Join("", fiel_ids.Split('"', '\''));
                   markers = String.Join("", markers.Split('"', '\''));
                   var arrNotes = notes.Split('*');
                   var arrDates = dates.Split('*');
                   var arrFields = fiel_ids.Split('*');
                   var arrMarkers = markers.Split('*');
                   var nId = (db.Photo_fields.Max(t => (int?)t.photo_id) ?? 0);
                   string str = "";
                   for (int i = 0; i < req.Files.Count; i++)
                   {
                       HttpPostedFile file = req.Files[i];
                       file.SaveAs(filePath + file.FileName);
                       str = str + " , "+filePath + file.FileName; 
                       nId++;
                       var dic = new Photo_fields
                       {
                           photo_id = nId,
                           note = arrNotes[i],
                           path_photo = file.FileName,
                           date = DateTime.Parse(arrDates[i]),
                           fiel_fiel_id = Int32.Parse(arrFields[i]),
                           marker = arrMarkers[i].Equals("null") ? null : arrMarkers[i],
                       };
                       db.Photo_fields.Add(dic);
                   }

                   db.SaveChanges();
                   trans.Commit();
                   HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, str);
                  return  response;
               }
              catch
              {
                  return new HttpResponseMessage(HttpStatusCode.BadRequest);
              }

                   }
                catch (Exception)
                {
                    trans.Rollback();
                    throw;
                }
           }


       }

        //

        [Route("GetPdfCulture")]
        [HttpGet]
        public HttpResponseMessage GetFile(int type, string name)
        {
            var filePath = ""; var file = "";
            //детализация
            if (type == 1)
            {
                switch (name)
                {
                    case "barley":
                        filePath = HttpContext.Current.Server.MapPath("~/ExcelReports/1.pdf");
                        file = string.Format("{0}.{1}", "barley", "pdf");
                        break;
                    case "sunflower":
                     filePath = HttpContext.Current.Server.MapPath("~/ExcelReports/2.pdf");
                     file = string.Format("{0}.{1}", "sunflower", "pdf");
                        break;
                    case "winter_wheat":
                        filePath = HttpContext.Current.Server.MapPath("~/ExcelReports/3.pdf");
                        file = string.Format("{0}.{1}", "winter_wheat", "pdf");
                        break;
                    case "sugar_beet":
                        filePath = HttpContext.Current.Server.MapPath("~/ExcelReports/4.pdf");
                        file = string.Format("{0}.{1}", "sugar_beet", "pdf");
                        break;
                    case "potatoes":
                        filePath = HttpContext.Current.Server.MapPath("~/ExcelReports/5.pdf");
                        file = string.Format("{0}.{1}", "potatoes", "pdf");
                        break;
                }
            }
            //севооборот
            if (type == 2)
            {
                filePath = HttpContext.Current.Server.MapPath("~/ExcelReports/6.pdf");
                file = string.Format("{0}.{1}", "crop_rotation", "pdf");
            }
            //сменное задание
            if (type == 3)
            {
                switch (name)
                {
                    case "1":
                        filePath = HttpContext.Current.Server.MapPath("~/ExcelReports/7.pdf");
                        file = string.Format("{0}.{1}", "task1", "pdf");
                        break;
                    case "2":
                        filePath = HttpContext.Current.Server.MapPath("~/ExcelReports/8.pdf");
                        file = string.Format("{0}.{1}", "task2", "pdf");
                        break;
                    case "3":
                        filePath = HttpContext.Current.Server.MapPath("~/ExcelReports/9.pdf");
                        file = string.Format("{0}.{1}", "task3", "pdf");
                        break;
                }
            }

            return DownloadFile(filePath, file);
        }
        public HttpResponseMessage DownloadFile(string downloadFilePath, string fileName)
        {
            try
            {
                if (!System.IO.File.Exists(downloadFilePath))
                {
                    throw new HttpResponseException(HttpStatusCode.NotFound);
                }
                MemoryStream responseStream = new MemoryStream();
                Stream fileStream = System.IO.File.Open(downloadFilePath, FileMode.Open);

                fileStream.CopyTo(responseStream);
                fileStream.Close();
                responseStream.Position = 0;

                HttpResponseMessage response = new HttpResponseMessage();
                response.StatusCode = HttpStatusCode.OK;
                response.Content = new StreamContent(responseStream);
                string contentDisposition = string.Concat("attachment; filename=", fileName);
                response.Content.Headers.ContentDisposition =
                              ContentDispositionHeaderValue.Parse(contentDisposition);
                return response;
            }
            catch
            {
                throw new HttpResponseException(HttpStatusCode.InternalServerError);
            }
        }

    }
}