﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using LogusSrv.CORE.REST;
using LogusSrv.DAL.Operations;
using LogusSrv.DAL.Entities.Res;
using LogusSrv.DAL.Entities.Req;
using LogusSrv.DAL.Entities.DTO;
using LogusSrv.DAL.Operations.DictionaryOperations;
using System.Net.Http;
using System.IO;
using System.Net;
using System.Net.Http.Headers;

namespace LogosSrv.Controllers
{
    [RoutePrefix("api/DictionaryCtrl")]
    public class DictionaryController : BaseCtrl
    {
        public DictionaryDb _db = new DictionaryDb();

        [Route("GetDictionaries")]
      //  [HttpPost]
        [HttpGet]
        public Response<List<LogusDictionary>> GetDictionaries()
        {
            return Perform(() => _db.GetDictionaries());
        }
        [Route("GetDictionaryById")]
        [HttpPost]
        public Response<LogusDictionaryData> GetDictionaryById(Request<int> req)
        {
            return Perform(() => _db.GetDictionaryById(req.RequestObject));
        }


        [Route("GetType_salary_doc")]
        [HttpGet]
        public Response<List<ModelType_salary_doc>> GetType_salary_doc()
        {
               return Perform(() => _db.GetType_salary_doc());
        }
        [Route("Updateype_salary_doc")]
        [HttpPost]
        public Response<string> Updateype_salary_doc(Request<List<UpdateType_salary_doc>> req)
        {
            return Perform(() => _db.Updateype_salary_doc(req.RequestObject));
        }


        [Route("GetContractor")]
        [HttpGet]
        public Response<List<ModelContractor>> GetContractors()
        {
            return Perform(() => _db.GetContractor());
        }
        [Route("UpdateContractor")]
        [HttpPost]
        public Response<string> Updateype_salary_doc(Request<List<UpdateContractor>> req)
        {
            return Perform(() => _db.UpdateContractor(req.RequestObject));
        }


        [Route("GetVAT")]
        [HttpGet]
        public Response<List<ModelVat>> GetVAT()
        {
            return Perform(() => _db.GetVAT());
        }
        [Route("UpdateVAT")]
        [HttpPost]
        public Response<string> UpdateVAT(Request<List<UpdateVat>> req)
        {
            return Perform(() => _db.UpdateVAT(req.RequestObject));
        }


        [Route("GetType_fuel")]
        [HttpGet]
        public Response<List<ModelTypeFuel>> GetType_fuel()
        {
            return Perform(() => _db.GetType_fuel());
        }
        [Route("UpdateType_fuel")]
        [HttpPost]
        public Response<string> UpdateType_fuel(Request<List<UpdateTypeFuel>> req)
        {
            return Perform(() => _db.UpdateType_fuel(req.RequestObject));
        }
        [Route("DetailType_fuel")]
        [HttpGet]
        public Response<DetailTypeFuel> DetailType_fuel()
        {
            return Perform(() => _db.DetailType_fuel());
        }

        
        [Route("GetType_materials")]
        [HttpGet]
        public Response<List<ModelTypeMaterials>> GetType_materials()
        {
            return Perform(() => _db.GetType_materials());
        }
        [Route("UpdateType_materials")]
        [HttpPost]
        public Response<string> UpdateType_materials(Request<List<UpdateTypeMaterials>> req)
        {
            return Perform(() => _db.UpdateType_materials(req.RequestObject));
        }


        [Route("GetMaterials")]
        [HttpGet]
        public Response<List<ModelMaterials>> GetMaterials()
        {
            return Perform(() => _db.GetMaterials(null));
        }
        [Route("UpdateMaterials")]
        [HttpPost]
        public Response<string> UpdateMaterials(Request<List<ModelUpdateMaterials>> req)
        {
            return Perform(() => _db.UpdateMaterialsDict(req.RequestObject));
        }
        [Route("DetailMaterials")]
        [HttpGet]
        public Response<DetailMaterials> GetDetailMaterials()
        {
            return Perform(() => _db.DetailMaterials());
        }

        [Route("GetPlants")]
        [HttpGet]
        public Response<List<ModelPlants>> GetPlants()
        {
            return Perform(() => _db.GetPlants());
        }
        [Route("UpdatePlants")]
        [HttpPost]
        public Response<string> UpdatePlants(Request<List<ModelUpdatePlants>> req)
        {
            return Perform(() => _db.UpdatePlantsDict(req.RequestObject));
        }
        [Route("DetailPlants")]
        [HttpGet]
        public Response<DetailPlantsModel> DetailPlants()
        {
            return Perform(() => _db.DetailPlants());
        }

        [Route("GetFields")]
        [HttpGet]
        public Response<List<ModelFields>> GetFields()
        {
            return Perform(() => _db.GetFields());
        }
        [Route("UpdateFields")]
        [HttpPost]
        public Response<string> UpdateFields(Request<List<ModelUpdateFields>> req)
        {
            return Perform(() => _db.UpdateFieldsDict(req.RequestObject));
        }
        [Route("DetailFields")]
        [HttpGet]
        public Response<DetailFieldsModel> DetailFields()
        {
            return Perform(() => _db.DetailFields());
        }

        [Route("GetSZR")]
        [HttpGet]
        public Response<List<ModelMaterials>> GetSZR()
        {
            return Perform(() => _db.GetMaterials(1));
        }
        [Route("UpdateSZR")]
        [HttpPost]
        public Response<string> UpdateSZR(Request<List<ModelUpdateMaterials>> req)
        {
            var list = req.RequestObject;
            for (var i = 0; i < list.Count; i++)
            {
                if (list[i].Document.mat_type_id == null)
                {
                    list[i].Document.mat_type_id = new DictionaryItemsDTO { Id = 1};
                    list[i].Document.description = "СЗР";
                }
            }
            return Perform(() => _db.UpdateMaterialsDict(req.RequestObject));
        }
        [Route("GetSeed")]
        [HttpGet]
        public Response<List<ModelMaterials>> GetSeed()
        {
            return Perform(() => _db.GetMaterials(4));
        }
        [Route("UpdateSeed")]
        [HttpPost]
        public Response<string> UpdateSeed(Request<List<ModelUpdateMaterials>> req)
        {
            var list = req.RequestObject;
            for (var i = 0; i < list.Count; i++)
            {
                if (list[i].Document.mat_type_id == null)
                {
                    list[i].Document.mat_type_id = new DictionaryItemsDTO { Id = 4 };
                    list[i].Document.description = "Семена";
                }
            }
            return Perform(() => _db.UpdateMaterialsDict(req.RequestObject));
        }
        [Route("GetFertilizer")]
        [HttpGet]
        public Response<List<ModelMaterials>> GetFertilizer()
        {
            return Perform(() => _db.GetMaterials(2));
        }
        [Route("UpdateFertilizer")]
        [HttpPost]
        public Response<string> UpdateFertilizer(Request<List<ModelUpdateMaterials>> req)
        {
            var list = req.RequestObject;
            for (var i = 0; i < list.Count; i++)
            {
                if (list[i].Document.mat_type_id == null)
                {
                    list[i].Document.mat_type_id = new DictionaryItemsDTO { Id = 2 };
                    list[i].Document.description = "Минеральные удобрения";
                }
            }
            return Perform(() => _db.UpdateMaterialsDict(req.RequestObject));
        }
        [Route("GetModules")]
        [HttpGet]
        public Response<List<ModulesDictModel>> GetModules()
        {
            return Perform(() => _db.GetModules());
        }
        [Route("UpdateModules")]
        [HttpPost]
        public Response<string> UpdateModules(Request<List<ModelUpdateModules>> req)
        {
            return Perform(() => _db.UpdateModulesDict(req.RequestObject));
        }
        //
        [Route("GetOrganizations")]
        [HttpGet]
        public Response<List<OrganizationsDictModel>> GetOrganizations()
        {
            return Perform(() => _db.GetOrganizations());
        }
        [Route("UpdateOrganizations")]
        [HttpPost]
        public Response<string> UpdateOrganizations(Request<List<ModelUpdateOrganizations>> req)
        {
            return Perform(() => _db.UpdateOrganizationsDict(req.RequestObject));
        }
        [Route("DetailOrganizations")]
        [HttpGet]
        public Response<DetailOrganizationsModel> DetailOrganizations()
        {
            return Perform(() => _db.DetailOrganizations());
        }

        //===
        [Route("GetGarages")]
        [HttpGet]
        public Response<List<GaragesDictModel>> GetGarages()
        {
            return Perform(() => _db.GetGarages());
        }
        [Route("UpdateGarages")]
        [HttpPost]
        public Response<string> UpdateGarages(Request<List<ModelUpdateGarages>> req)
        {
            return Perform(() => _db.UpdateGaragesDict(req.RequestObject));
        }
        [Route("DetailGarages")]
        [HttpGet]
        public Response<DetailGaragesModel> DetailGarages()
        {
            return Perform(() => _db.DetailGarages());
        }

        [Route("GetGroup_traktors")]
        [HttpGet]
        public Response<List<Type_traktorsDictModel>> GetType_traktors()
        {
            return Perform(() => _db.GetGroup_traktors());
        }
        [Route("UpdateGroup_traktors")]
        [HttpPost]
        public Response<string> UpdateType_traktors(Request<List<ModelUpdateType_traktors>> req)
        {
            return Perform(() => _db.UpdateType_traktors(req.RequestObject));
        }

        //группы работ

        [Route("GetGroup_tasks")]
        [HttpGet]
        public Response<List<Group_tasksDictModel>> GetGroupOftasks()
        {
            return Perform(() => _db.GetGroup_tasks());
        }
        [Route("UpdateGroup_tasks")]
        [HttpPost]
        public Response<string> UpdateType_tasks(Request<List<ModelUpdateGroup_tasks>> req)
        {
            return Perform(() => _db.UpdateGroup_tasks(req.RequestObject));
        }
        [Route("DetailGroup_tasks")]
        [HttpGet]
        public Response<DetailGroup_tasksModel> DetailGroup_tasks()
        {
            return Perform(() => _db.DetailGroup_tasks());
        }
        //Этапы выращивания
        [Route("GetStages_cultivation")]
        [HttpGet]
        public Response<List<Stages_cultivationDictModel>> GetStagesOfcultivation()
        {
            return Perform(() => _db.GetStages_cultivation());
        }
        [Route("UpdateStages_cultivation")]
        [HttpPost]
        public Response<string> UpdateStagesOfcultivation(Request<List<ModelUpdateStages_cultivation>> req)
        {
            return Perform(() => _db.UpdateStages_cultivation(req.RequestObject));
        }
        //////Склады
        [Route("GetStorages")]
        [HttpGet]
        public Response<List<StoragesDictModel>> GetStorage()
        {
            return Perform(() => _db.GetStorages());
        }
        [Route("UpdateStorages")]
        [HttpPost]
        public Response<string> UpdateStorages(Request<List<ModelUpdateStorages>> req)
        {
            return Perform(() => _db.UpdateStorages(req.RequestObject));
        }
        //////запчасти
        [Route("GetSpare_parts")]
        [HttpGet]
        public Response<List<Spare_partsDictModel>> GetSpare_part()
        {
            return Perform(() => _db.GetSpare_parts());
        }
        [Route("UpdateSpare_parts")]
        [HttpPost]
        public Response<string> UpdateSpare_parts(Request<List<ModelUpdateSpare_parts>> req)
        {
            return Perform(() => _db.UpdateSpare_parts(req.RequestObject));
        }
        [Route("DetailSpare_parts")]
        [HttpGet]
        public Response<DetailSpare_partsModel> DetailSpare_parts()
        {
            return Perform(() => _db.DetailSpare_parts());
        }
        //
        [Route("GetTraktors")]
        [HttpGet]
        public Response<List<ModelTraktorsDict>> GetTraktors()
        {
            return Perform(() => _db.GetTraktors());
        }
        [Route("UpdateTraktors")]
        [HttpPost]
        public Response<string> UpdateTraktors(Request<List<ModelUpdateTraktors>> req)
        {
            return Perform(() => _db.UpdateTraktorsDict(req.RequestObject));
        }
        [Route("DetailTraktors")]
        [HttpGet]
        public Response<DetailTraktorsModel> DetailTraktors()
        {
            return Perform(() => _db.DetailTraktors());
        }

        [Route("GetType_equipments")]
        [HttpGet]
        public Response<List<ModelType_equipmentsDict>> GetType_equipments()
        {
            return Perform(() => _db.GetType_equipments());
        }
        [Route("UpdateType_equipments")]
        [HttpPost]
        public Response<string> UpdateType_equipments(Request<List<ModelUpdateType_equipmentsDict>> req)
        {
            return Perform(() => _db.UpdateType_equipments(req.RequestObject));
        }

        [Route("GetEquipments")]
        [HttpGet]
        public Response<List<ModelEquipmentDict>> GetEquipments()
        {
            return Perform(() => _db.GetEquipments());
        }
        [Route("UpdateEquipments")]
        [HttpPost]
        public Response<string> UpdateEquipments(Request<List<ModelUpdateEquipments>> req)
        {
            return Perform(() => _db.UpdateEquipmentsDict(req.RequestObject));
        }
        [Route("DetailEquipments")]
        [HttpGet]
        public Response<DetailEquipmentsModel> DetailEquipments()
        {
            return Perform(() => _db.DetailEquipments());
        }

        [Route("GetType_tasks")]
        [HttpGet]
        public Response<List<ModelType_tasksDict>> GetType_tasks()
        {
            return Perform(() => _db.GetType_tasks());
        }
        [Route("UpdateType_tasks")]
        [HttpPost]
        public Response<string> UpdateType_tasks(Request<List<ModelUpdateType_tasks>> req)
        {
            return Perform(() => _db.UpdateType_tasks(req.RequestObject));
        }

        [Route("GetType_sorts_plants")]
        [HttpGet]
        public Response<List<ModelType_sorts_plantsDict>> GetType_sorts_plantsDict()
        {
            return Perform(() => _db.GetType_sorts_plants());
        }
        [Route("UpdateType_sorts_plants")]
        [HttpPost]
        public Response<string> UpdateType_sorts_plantsDict(Request<List<ModelUpdateType_sorts_plants>> req)
        {
            return Perform(() => _db.UpdateType_sorts_plantsDict(req.RequestObject));
        }
        [Route("DetailType_sorts_plants")]
        [HttpGet]
        public Response<DetailType_sorts_plantsModel> GetDetailType_sorts_plants()
        {
            return Perform(() => _db.DetailType_sorts_plants());
        }

        [Route("GetEmployees")]
        [HttpGet]
        public Response<List<ModelEmployeeDict>> GetEmployees()
        {
            return Perform(() => _db.GetEmployees());
        }
        [Route("UpdateEmployees")]
        [HttpPost]
        public Response<string> UpdateEmployees(Request<List<ModelUpdateEmployeeDict>> req)
        {
            return Perform(() => _db.UpdateEmployees(req.RequestObject));
        }
        [Route("DetailEmployees")]
        [HttpGet]
        public Response<DetailEmployeeDict> GetEmployeeInfoDict()
        {
            return Perform(() => _db.DetailEmployees());
        }
        //Сдельщики
        //вывод
        [Route("GetSalaries_employees")]
        [HttpGet]
        public Response<List<ModelSalEmployeesDict>> GetSalaries_employees()
        {
            return Perform(() => _db.GetSalaries_employees());
        }
        //update
        [Route("UpdateSalaries_employees")]
        [HttpPost]
        public Response<string> UpdateSalaries_employees(Request<List<ModelUpdateSalEmployeesDict>> req)
        {
            return Perform(() => _db.UpdateSalaries_employees(req.RequestObject));
        }
        //

        //

        [Route("GetServices")]
        [HttpGet]
        public Response<List<ModelServiceDict>> GetServiceDict()
        {
            return Perform(() => _db.GetServices());
        }
        [Route("UpdateServices")]
        [HttpPost]
        public Response<string> UpdateServiceDict(Request<List<ModelUpdateServiceDict>> req)
        {
            return Perform(() => _db.UpdateServiceDict(req.RequestObject));
        }

        [Route("GetAggregates")]
        [HttpGet]
        public Response<List<ModelAggregateDict>> GetAggregatesDict()
        {
            return Perform(() => _db.GetAggregates());
        }
        [Route("UpdateAggregates")]
        [HttpPost]
        public Response<string> UpdateAggregatesDict(Request<List<ModelUpdateAggregateDict>> req)
        {
            return Perform(() => _db.UpdateAggregatesDict(req.RequestObject));
        }

        [Route("GetTech1c")]
        [HttpGet]
        public Response<List<ModelTraktors1cDict>> GetTech1c()
        {
            return Perform(() => _db.GetTech1c());
        }
        [Route("UpdateTech1c")]
        [HttpPost]
        public Response<string> UpdateTech1c(Request<List<ModelUpdateTraktors1с>> req)
        {
            return Perform(() => _db.UpdateTech1c(req.RequestObject));
        }

        [Route("GetKPI")]
        [HttpGet]
        public Response<List<ModelKPI>> GetKPI()
        {
            return Perform(() => _db.GetKPI());
        }
        [Route("DetailKPI")]
        [HttpGet]
        public Response<DetailKPIModel> DetailKPI()
        {
            return Perform(() => _db.DetailKPI());
        }
        [Route("UpdateKPI")]
        [HttpPost]
        public Response<string> UpdateKPI(Request<List<ModelUpdateKPI>> req)
        {
            return Perform(() => _db.UpdateKPI(req.RequestObject));
        }


        [Route("GetAgtoreq")]
        [HttpGet]
        public Response<List<AgroreqModel>> GetAgtoreq()
        {
            return Perform(() => _db.GetAgtoreq());
        }
        [Route("UpdateAgroreq")]
        [HttpPost]
        public Response<string> UpdateAgroreq(Request<List<ModelUpdateAgroreq>> req)
        {
            return Perform(() => _db.UpdateAgroreq(req.RequestObject));
        }
        [Route("GetTypeSpareParts")]
        [HttpGet]
        public Response<List<TypeSparePartsDictModel>> GetTypeSpareParts()
        {
            return Perform(() => _db.GetTypeSpareParts());
        }
        [Route("UpdateTypeSpareParts")]
        [HttpPost]
        public Response<string> UpdateTypeSpareParts(Request<List<ModelUpdateTypeSpareParts>> req)
        {
            return Perform(() => _db.UpdateTypeSpareParts(req.RequestObject));
        }
        [Route("GetDictionaryField")]
        [HttpPost]
        public Response<LogusDictionaryData> GetDictionaryField(Request<int> req)
        {
            return Perform(() => _db.GetDictionaryField(req.RequestObject));
        }
        [Route("GetFieldsByType")]
        [HttpGet]
        public Response<List<ModelFields>> GetFieldsByType(int? type)
        {
            return Perform(() => _db.GetFieldsByType(type));
        }
        [Route("DetailFieldsByType")]
        [HttpGet]
        public Response<DetailFieldsModel> DetailFieldsByType(int? type)
        {
            return Perform(() => _db.DetailFieldsByType(type));
        }

        //Выгрузка координат полей в файл
        [Route("GetFieldCoordTxt")]
        [HttpGet]
        public HttpResponseMessage GetFieldCoordTxt()
        {
            try
            {
                var templatePath = HttpContext.Current.Server.MapPath("~/ExcelReports/CoordField.txt");
                var report = _db.GetFieldCoordTxt(templatePath);
                var isIe = Request.Headers.UserAgent.ToString().Contains("Trident");
                HttpResponseMessage result = new HttpResponseMessage(HttpStatusCode.OK);
                if ((report.data != null))
                {
                    var stream = new MemoryStream(report.data);
                    var FileName = report.fileName;
                    result.Content = new StreamContent(stream);
                    result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/vnd.ms-excel");
                    result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
                    {
                        FileName = isIe ? HttpUtility.UrlEncode(FileName) : (FileName)
                    };
                }
                else
                {
                    result.StatusCode = HttpStatusCode.NoContent;
                }
                return result;
            }
            catch (Exception ex)
            {
                HttpResponseMessage errorResult = new HttpResponseMessage(HttpStatusCode.BadRequest);
                errorResult.ReasonPhrase = ex.ToString();
                return errorResult;
            }
        }
        //KML
        [Route("GetFieldCoordKml")]
        [HttpGet]
        public HttpResponseMessage GetFieldCoordKml()
        {
            try
            {
                HttpResponseMessage result = new HttpResponseMessage(HttpStatusCode.OK);

                string bum = _db.GetFieldCoordKmlData();

                result.Content = new StringContent(bum);
                result.Content.Headers.ContentType = new MediaTypeHeaderValue("text/xml");
                result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
                {
                    FileName = "fields.kml",
                };
                return result;
            }
            catch (Exception ex)
            {
                HttpResponseMessage errorResult = new HttpResponseMessage(HttpStatusCode.BadRequest);
                errorResult.ReasonPhrase = ex.ToString();
                return errorResult;
            }
        }
        



        [Route("GetCoordFromTXT")]
        [HttpPost]
        public string GetCoordFromTXT(int type)
        {
            var report = new List<string>();
            var upload = HttpContext.Current.Request.Files[0];
            byte[] avatar = new byte[upload.ContentLength];
            upload.InputStream.Read(avatar, 0, upload.ContentLength);
            Stream stream = new MemoryStream(avatar);
            var str = ""; var str2 = "";
            //=============KML
            var arr = upload.FileName.Split('.');
            var checkKml = arr[arr.Length-1];
            if (checkKml.Equals("kml"))
            {
             report = _db.saveFieldsFromKmlFile(stream, type);
            }
            else
            {

                var templatePath = HttpContext.Current.Server.MapPath("~/ExcelReports/CoordField.txt");
                using (var fileStream = new FileStream(templatePath, FileMode.Truncate, FileAccess.Write))
                {
                    stream.CopyTo(fileStream);  //копируем из потока данные в файл coordField.txt
                    fileStream.Close();
                }

                report = _db.GetCoordFromTXT(type, templatePath);

            }
            //=
            if (report.Count > 1)
            {
                switch (type)
                {
                    case 1: str = " поля было обновлено! <br>"; str2 = " поля было добавлено!"; break;
                    case 2: str = " объекта было обновлено! <br>"; str2 = " объекта было добавлено!"; break;
                    case 3: str = " контура было обновлено! <br>"; str2 = " контура было добавлено!"; break;
                }
                return report[0] + str + report[1] + str2;
            }
            return "Ошибка при попытке загрузки файла";
        }

    }
}