﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using LogusSrv.CORE.REST;
using LogusSrv.DAL.Operations;
using LogusSrv.DAL.Entities.Res;
using LogusSrv.DAL.Entities.DTO.SeedingBridle;
using LogusSrv.DAL.Entities.Res.SeedingBridle;
using LogusSrv.DAL.Entities.DTO;



namespace LogosSrv.Controllers
{

    [RoutePrefix("api/TechCardTemp")]
    public class TechCardTempController : BaseCtrl
    {
        public TechCardTempDb techCardTempDb = new TechCardTempDb();

        [Route("GetTCTemp")]
        [HttpGet]
        public Response<List<TCItemTemp>> GetTCByYear()
        {
            return Perform(() => techCardTempDb.GetTCTemp());
        }
        //getPlantByTwoYears
        [Route("GetPlantByTwoYears")]
        [HttpGet]
        public Response<List<int>> GetPlantByTwoYears()
        {
            return Perform(() => techCardTempDb.GetPlantByTwoYears());
        }

        ////////года
        [Route("GetYearsCult")]
        [HttpPost]
        public Response<List<DictionaryItemsDTO>> GetYearsCult(Request<int> req)
        {
            return Perform(() => techCardTempDb.GetYearsCult(req.RequestObject));
        }

        [Route("GetPlantSort")]
        [HttpGet]
        public Response<List<DictionaryItemForPlants>> GetPlantSort()
        {
            return Perform(() => techCardTempDb.GetPlantSort());
        }

        [Route("CreateUpdateTC_temp")]
        [HttpPost]
        public Response<string> CreateUpdateTC(Request<TCTempDetailModel> req)
        {
            return Perform(() => techCardTempDb.CreateUpdateTC_temp(req.RequestObject));
        }

        [Route("DelTC_temp")]
        [HttpPost]
        public Response<string> DelTC_temp(Request<int> req)
        {
            return Perform(() => techCardTempDb.DelTC_temp(req.RequestObject));
        }

        //расценки
        [Route("GetRateTemp")]
        [HttpPost]
        public Response<RateResModel> GetRateTemp(Request<RateTechReq> req)
        {
            return Perform(() => techCardTempDb.GetRateTemp(req.RequestObject));
        }

        [Route("GetTC_tempById")]
        [HttpPost]
        public Response<TCTempDetailModel> GetTC_tempById(Request<int> req)
        {
            return Perform(() => techCardTempDb.GetTC_tempById(req.RequestObject));
        }


        [Route("CheckSortPlantYear")]
        [HttpPost]
  //      public Response<string> CheckSortPlantYear(Request<GetDetailSeedingBridleRes> req) FieldEventModel
        public Response<string> CheckSortPlantYear(Request<FieldEventModel> req)
        {
            return Perform(() => techCardTempDb.CheckSortPlantYear(req.RequestObject));
        }

        //создание шаблонов из тех карт
        [Route("CreateTemplateFromTech")]
        [HttpGet]
        public Response<string> CreateTemplateFromTech()
        {
            return Perform(() => techCardTempDb.CreateTemplateFromTech());
        }  


    }


}