﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using LogusSrv.DAL.Operations;
using LogusSrv.DAL.Enums;
using System.Threading;

namespace LogosSrv.Controllers
{
    [RoutePrefix("api/GPSCtrl")]
    public class GPSController : BaseCtrl
    {

        private readonly GpsFromXmlDb _gpsDB = new GpsFromXmlDb();
        [Route("SaveGpsData")]
        [HttpPost]
        public string SaveGpsData()
        {
            var data = Request.Content.ReadAsStringAsync();
            _gpsDB.SaveGpsData(data.Result, XmlFileStatus.Load);
            return "Ok"; //data.Result
        }
       
        [Route("StartParseGps")]
        [HttpGet]
        public string startParseGps()
        {
            return _gpsDB.StartGPSTimer();
        }

        [Route("SetIntervalTimerParse")]
        [HttpGet]
        public int setIntervalTimer(int? interval = null)
        {
            return _gpsDB.setIntervalTimer((int)interval);
        }



        [Route("StopParseGps")]
        [HttpGet]
        public string stopGpsSrv()
        {
            return _gpsDB.StopGPSTimer();
        }

        [Route("ParseLoadXmlGps")]
        [HttpGet]
        public string ParseLoadXmlGps()
        {
            var t2 = new Thread(new ThreadStart(() =>
            {
                _gpsDB.ParseLoadXmlGps();
                Console.WriteLine("Save END");
            }));
            t2.Start();
            return "Parse GO!";
        }
        //
        [Route("ParseLoadXmlGps1")]
        [HttpGet]
        public string ParseLoadXmlGps1()
        {
            var t = new Thread(new ThreadStart(() =>
            {
                _gpsDB.ParseLoadXmlGps1();
                Console.WriteLine("Save END");
            }));
            t.Start();
            return "Parse GO1!";
        }
        //

        [Route("ArchiveGPS")]
        [HttpGet]
        public string ArchiveGPS()
        {
            _gpsDB.ArchiveGPS();
            return "success";
        }
   }
}