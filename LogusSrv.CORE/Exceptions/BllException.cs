﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogusSrv.CORE.Exceptions
{
    public class BllException : Exception
    {
        public int? ErrorCode { get; set; }
        public string ErrorMessage { get; set; }
        public List<string> ErrorList { get; set; }

        public BllException(int errorCode, string errorMesage, List<string> errorList = null, Exception innerException = null)
            : base(errorMesage, innerException)
        {
            ErrorCode = errorCode;
            ErrorMessage = errorMesage;
            ErrorList = errorList;
        }

        public override string Message
        {
            get { return ErrorMessage + "\n" + string.Join("\n", ErrorList); }
        }
    }
}
