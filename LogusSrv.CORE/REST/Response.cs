﻿using System.Collections.Generic;
using System.Diagnostics.Tracing;

namespace LogusSrv.CORE.REST
{
    public class Response<T>
    {
        public int? ErrorCode { get; set; }
        public string ErrorMessage { get; set; }
        public List<string> ErrorList { get; set; } 
        public T Value { get; set; }
        public string ServiceVersion { get; set; }

        public Response(T value, int? eCode, string eMessage, List<string> eList = null)
        {
            this.Value = value;
            this.ErrorCode = eCode;
            this.ErrorMessage = eMessage;
            this.ErrorList = eList;
        }

        public Response(T value) : this(value, 0, null)
        {
        }
    }
}
