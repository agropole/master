﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogusSrv.DAL.DBModel;
using System.Xml.Linq;
using System.Globalization;
using System.Data.Entity.Spatial;
using LogusSrv.DAL.Enums;
using System.Timers;
using System.Data.Entity.Validation;
using System.Diagnostics;

namespace LogusSrv.DAL.Operations
{
    public class GpsFromXmlDb
    {
        protected readonly LogusDBEntities db = new LogusDBEntities();
        private static Timer aTimer;
        private Boolean StartTimer = false;
        private int timeTimer = 600000;
        public Boolean canBegin = true;
        public bool ParseXML(string readText)
        {
            using (var trans = db.Database.BeginTransaction())
            {
                try
                {
                    int k = 0;
                    if (readText.IndexOf("\r\n") == 0)
                    {
                        readText = readText.Remove(0, 2);
                    }
                    while (k != -1)
                    {
                        k = readText.IndexOf("<!--");
                        if (k == -1) break;
                        var k1 = readText.IndexOf("-->", k);
                        var length = k1 - k;
                        readText = readText.Remove(k, length + 3);
                    }
                    List<GPS> gpsList = new List<GPS>();
                    XDocument xmlDoc = XDocument.Parse(readText);

                    var trackId = from coorData in xmlDoc.Descendants("CAR") select coorData.Element("ID_CAR").Value;
                    var provider = new CultureInfo("en-US");
                    foreach (var coorData in xmlDoc.Descendants("COORD_DATA"))
                    {
                        var speed = Decimal.Parse(coorData.Element("SPEED").Value, provider);
                        if (speed != 0)
                        {
                            GPS gps = new GPS();
                            gps.track_id = Convert.ToInt32(trackId.FirstOrDefault()); /////имитировать
                            gps.speed = speed < 1000 ? speed : 6;
                            gps.direction = Decimal.Parse(coorData.Element("DIRECTION").Value, provider);
                            var lat = Decimal.Parse(coorData.Element("COORD_LAT").Value, provider);
                            var lon = Decimal.Parse(coorData.Element("COORD_LON").Value, provider);
                            var point = string.Format(CultureInfo.InvariantCulture.NumberFormat, "POINT({0} {1})", lon, lat);
                            gps.coord = DbGeography.PointFromText(point, 4326);
                            gps.date = (DateTime.ParseExact(coorData.Element("DATE_COORD").Value, "yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture)).AddHours(3);  //добавляем 3 ч.
                            gpsList.Add(gps);
                        }
                    }

                    db.GPS.AddRange(gpsList);
#if DEBUG
                    db.Database.Log += s => Debug.Write(s); ;
#endif
                    db.SaveChanges();
                    trans.Commit();
                }
                catch (Exception ex)
                {
                    var e = ex.Message;
                }
            }
            return true;
        }

        //////////////////////////////временная копия 
        public bool ParseXML2(string readText)
        {
            using (var trans = db.Database.BeginTransaction())
            {
                try
                {
                    int k = 0;
                    if (readText.IndexOf("\r\n") == 0)
                    {
                        readText = readText.Remove(0, 2);
                    }
                    while (k != -1)
                    {
                        k = readText.IndexOf("<!--");
                        if (k == -1) break;
                        var k1 = readText.IndexOf("-->", k);
                        var length = k1 - k;
                        readText = readText.Remove(k, length + 3);
                    }
                    List<GPS> gpsList = new List<GPS>();
                    XDocument xmlDoc = XDocument.Parse(readText);

                    var trackId = from coorData in xmlDoc.Descendants("CAR") select coorData.Element("ID_CAR").Value;
                    var provider = new CultureInfo("en-US");
                    foreach (var coorData in xmlDoc.Descendants("COORD_DATA"))
                    {
                        var speed = Decimal.Parse(coorData.Element("SPEED").Value, provider);
                        if (speed != 0)
                        {
                            GPS gps = new GPS();
                            gps.track_id = Convert.ToInt32(trackId.FirstOrDefault()); /////имитировать
                            gps.speed = speed < 1000 ? speed : 6;
                            gps.direction = Decimal.Parse(coorData.Element("DIRECTION").Value, provider);
                            var lat = Decimal.Parse(coorData.Element("COORD_LAT").Value, provider);
                            var lon = Decimal.Parse(coorData.Element("COORD_LON").Value, provider);
                            var point = string.Format(CultureInfo.InvariantCulture.NumberFormat, "POINT({0} {1})", lon, lat);
                            gps.coord = DbGeography.PointFromText(point, 4326);
                            gps.date = DateTime.ParseExact(coorData.Element("DATE_COORD").Value, "yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture);
                            gpsList.Add(gps);
                        }
                    }

                    db.GPS.AddRange(gpsList);
                    db.SaveChanges();
                    trans.Commit();
                }
                catch (Exception ex)
                {
                    var e = ex.Message;
                }
            }
            return true;
        }

        //


        public string SaveGpsData(string xml, XmlFileStatus status)
        {
            using (var trans = db.Database.BeginTransaction())
            {
                try
                {
                    Xml_archive newXML = new Xml_archive();
                    newXML.file_id = Guid.NewGuid();
                    newXML.date_load = DateTime.Now;
                    newXML.status = (int)status;
                    newXML.content = xml;
                    db.Xml_archive.Add(newXML);
#if DEBUG
                    db.Database.Log += s => Debug.Write(s); ;
#endif
                    db.SaveChanges();
                    trans.Commit();
                }
                catch (DbEntityValidationException ex)
                {
                    // Retrieve the error messages as a list of strings.
                    var errorMessages = ex.EntityValidationErrors
                            .SelectMany(x => x.ValidationErrors)
                            .Select(x => x.ErrorMessage);

                    // Join the list to a single string.
                    var fullErrorMessage = string.Join("; ", errorMessages);

                    // Combine the original exception message with the new one.
                    var exceptionMessage = string.Concat(ex.Message, " The validation errors are: ", fullErrorMessage);
                }
                return "OK";
            }
        }

        public string StartGPSTimer()
        {
            if (!StartTimer)
            {
                StartTimer = true;
                aTimer = new Timer();
                aTimer.Interval = timeTimer;
                // Hook up the Elapsed event for the timer. 
                aTimer.Elapsed += OnTimedEvent;
                aTimer.Enabled = true;
            }
            return "start gps timer";
        }

        public string StopGPSTimer()
        {
            aTimer.Enabled = false;
            StartTimer = false;
            return "stop gps timer";
        }

        public int setIntervalTimer(int newInt)
        {
            timeTimer = newInt;
            StopGPSTimer();
            StartGPSTimer();
            return newInt;
        }

        private void OnTimedEvent(Object source, ElapsedEventArgs e)
        {
            if (canBegin)
            {
                canBegin = false;

                using (var trans = db.Database.BeginTransaction())
                {
                    var xmList = db.Xml_archive.Where(x => x.status == (int)XmlFileStatus.ErrorParse || x.status == (int)XmlFileStatus.Load).OrderBy(x => x.date_load).Take(50).ToList();
                    foreach (var xml in xmList)
                    {
                        try
                        {
                            ParseXML(xml.content);
                            xml.status = (int)XmlFileStatus.Parse;
                        }
                        catch (Exception ex)
                        {
                            xml.error_text = ex.Message;
                            xml.status = (int)XmlFileStatus.ErrorParse;
                        }
                    }

                    db.SaveChanges();
                    trans.Commit();
                    canBegin = true;
                }
            }
        }


        public void ParseLoadXmlGps()
        {
            var xmList = db.Xml_archive.Where(x => x.status == (int)XmlFileStatus.ErrorParse || x.status == (int)XmlFileStatus.Load).OrderBy(x => x.date_load).Take(51).ToList(); //51
            foreach (var xml in xmList)
            {
                try
                {
                    ParseXML(xml.content);
                    xml.status = (int)XmlFileStatus.Parse;
                }
                catch (Exception ex)
                {
                    xml.error_text = ex.Message;
                    xml.status = (int)XmlFileStatus.ErrorParse;
                }
            }
        }
        //копия
        public void ParseLoadXmlGps1()
        {
                var xmList = db.Xml_archive.Where(x => x.status == (int)XmlFileStatus.ErrorParse || x.status == (int)XmlFileStatus.Load).OrderBy(x => x.date_load).Take(51).ToList(); //51
                foreach (var xml in xmList)
                {
                    try
                    {
                        ParseXML2(xml.content);
                        using (var trans = db.Database.BeginTransaction())
                        {
                            xml.status = (int)XmlFileStatus.Parse;
                            db.SaveChanges();
                            trans.Commit();
                        }
                    }
                    catch (Exception ex)
                    {
                        xml.error_text = ex.Message;
                        xml.status = (int)XmlFileStatus.ErrorParse;
                    }
                }
        }
        public void ArchiveGPS()
        {
            using (var trans = db.Database.BeginTransaction())
            {
                try
                {
                    var list = (from g in db.GPS
                                where g.date > DateTime.Now.AddMonths(-3)
                                select g).ToList();

                    if (list.Count > 0)
                    {
                        var listArchvie = (from g in list
                                           select new GPS_archive
                                           {
                                               coord = g.coord,
                                               date = g.date,
                                               direction = g.direction,
                                               mileage = g.mileage,
                                               speed = g.speed,
                                               track_id = g.track_id
                                           }).ToList();

                        db.GPS_archive.AddRange(listArchvie);
                        db.GPS.RemoveRange(list);
                    }

                    var l = new GPS_archive_log
                    {
                        ctrl = "GPSController",
                        func = "ArchiveGPS",
                        date = DateTime.Now,
                        msg = list.Count.ToString() + " were archived",
                        flag = "success"
                    };
                    db.GPS_archive_log.Add(l);

                    db.SaveChanges();
                    trans.Commit();
                }
                catch (Exception e)
                {
                    trans.Rollback();

                    var l = new GPS_archive_log
                    {
                        ctrl = "GPSController",
                        func = "ArchiveGPS",
                        date = DateTime.Now,
                        msg = e.ToString(),
                        flag = "error"
                    };
                    db.GPS_archive_log.Add(l);
                    db.SaveChanges();
                    trans.Commit();
                    throw;
                }
            }
        }
    }
}


