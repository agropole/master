﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LogusSrv.DAL.DBModel;
using LogusSrv.DAL.Entities.DTO;
using LogusSrv.DAL.Entities.Res;
using LogusSrv.CORE.Exceptions;
using LogusSrv.DAL.Utils;
using LogusSrv.DAL.Enums;

namespace LogusSrv.DAL.Operations
{
    public class ProductDb
    {
        protected readonly LogusDBEntities db = new LogusDBEntities();

        public ProductInfo GetInfoProduct()
        {
          var yearList = new List<DictionaryItemsDTO>();
          for (var i = 0; i < 20; i++) 
          {
          var item = new DictionaryItemsDTO { Id = DateTime.Now.Year - i, Name = (DateTime.Now.Year - i).ToString() };
          yearList.Add(item);
          };
            var PlantsAndSorts = (from s in db.Type_sorts_plants
                                  group s by new { id = s.plant_plant_id, name = s.Plants.name } into g

                                  select new
                                  {
                                      Id = g.Key.id,
                                      Name = g.Key.name,
                                      Values = (from s in g
                                                select new DictionaryItemsDTO
                                                {
                                                    Id = s.tpsort_id,
                                                    Name = s.name,
                                                }).ToList()
                                  }).ToList();

            var organizationsList = db.Organizations.Where(p => p.deleted != true &&
             p.tporg_tporg_id == (int)TypeOrganization.Section).
                             Select(t => new DictionaryItemsDTO
                             {
                                 Id = t.org_id,
                                 Name = t.name
                             }).OrderBy(t => t.Name).ToList();
            var allOrg = new DictionaryItemsDTO { Id = -1, Name = "Все" };
            organizationsList.Insert(0, allOrg);
            var res = new ProductInfo 
            {
                YearList = yearList.OrderByDescending(i => i.Id).ToList(),
                OrganizationsList = organizationsList,
            };
            return res;
        }

        public List<CultureProduct> YearCultureProduct(GrossReq req)
        {
            var checkOrg = db.Organizations.Where(t => t.org_id == req.OrgId).Select(t => t.tporg_tporg_id).FirstOrDefault();
            var newOrgId = checkOrg == 1 ? (int?)req.OrgId : null;
            var list = (from p in db.Product

                        let ssuumm = (from s in db.Act_sale
                                      where s.plant_plant_id == p.plant_plant_id && s.date.Year == req.Year
                                     select s).Sum(act => act.cost)

                        let kol = (from s in db.Act_sale
                                   where s.plant_plant_id == p.plant_plant_id && s.date.Year == req.Year
                                      select s).Sum(act => act.count/1000)

                        select  new CultureProduct{
                            Id = p.id,
                            Plant = new DictionaryItemsDTO
                            {
                                Id = p.plant_plant_id,
                                Name = p.Plants.name
                            },
                            Sort = p.sort_sort_id.HasValue ? new DictionaryItemsDTO {
                                Id = p.sort_sort_id,
                                Name = p.Type_sorts_plants.name
                            } : null,
                            Balance = p.balance,
                            GrossHarvest = p.gross_harvest,
                            CurrentPrice = p.current_price,
                            Sales = ssuumm.HasValue ? ssuumm : 0,
                            ProfitPlan = p.current_price * p.gross_harvest, 
                            Kol = kol.HasValue ? kol : 0,
                            Profit = (ssuumm.HasValue ? ssuumm : 0) - (p.current_price * (kol.HasValue ? kol : 0)), 
                            Year = p.year,
                        }).Where(p => p.Year == req.Year).OrderBy(p => p.Plant.Name).ToList();

            for (var i = 0; i < list.Count; i++)
            {
                if (String.IsNullOrEmpty(list[i].Profit.ToString())) {list[i].Profit = 0;}
                if (String.IsNullOrEmpty(list[i].ProfitPlan.ToString())) { list[i].ProfitPlan = 0; }
                else
                {
                    list[i].ProfitPlan = Convert.ToInt64(Math.Round((Decimal)list[i].ProfitPlan, 2));
                }
            }

            return list;
        }

        public string UpdateCultureCurPrice(CultureProduct cultureProduct)
        {
            using (var trans = db.Database.BeginTransaction())
            {
                try
                {
                    var cul = (from p in db.Product
                                where p.id == cultureProduct.Id
                                   select p).FirstOrDefault();
                    cul.current_price = cultureProduct.CurrentPrice;
                    db.SaveChanges();
                    trans.Commit();
                }
                catch (Exception)
                {
                    trans.Rollback();
                    throw;
                }
            }
            return "success";
        }

        public GrossInfo GetGrossInfo(GrossReq req)
        {
            var checkOrg = db.Organizations.Where(t => t.org_id == req.OrgId).Select(t => t.tporg_tporg_id).FirstOrDefault();
            var newOrgId = checkOrg == 1 ? (int?)req.OrgId : null;
            var yearList = new List<DictionaryItemsDTO>();
            for (var i = 0; i < 20; i++)
            {
                var item = new DictionaryItemsDTO { Id = DateTime.Now.Year - i, Name = (DateTime.Now.Year - i).ToString() };
                yearList.Add(item);
            };

            var PlantsAndSorts = (
                                  from ftp in db.Fields_to_plants
                                  where ftp.date_ingathering.Year == req.Year && ftp.Fields.type == 1 &&
                                     ftp.Plants.deleted != true && ftp.Fields.deleted != true &&
                                     (newOrgId != null ? ftp.Fields.org_org_id == newOrgId : true)
                                  join sortPlan in db.Sorts_plants_data on ftp.fielplan_id equals sortPlan.fielplan_fielplan_id into spdLj
                                  from spd in spdLj.DefaultIfEmpty()

                                  join indSort in db.Ind_sorts_plants_data.Where(i => i.indsort_indsort_id == 1) on spd.sortdat_id equals indSort.sortdat_sortdat_id into indSortLj
                                  from ispd in indSortLj.DefaultIfEmpty()

                                  group new { ftp, spd, ispd } by new { ftp.plant_plant_id, ftp.Plants.name } into g
                                  select new PlantSortFieldDictionary
                                  {
                                      Id = g.Key.plant_plant_id,
                                      Name = g.Key.name,
                                      Values = (
                                                from s in g.Where(ga => ga.spd != null).Where(s => s.spd.Type_sorts_plants.deleted != true)
                                                group new FieldDictionary
                                                {
                                                    Id = s.ftp.fiel_fiel_id,
                                                    Name = s.ftp.Fields.name,
                                                    Area = s.ftp.area_plant

                                                } by new
                                                {
                                                    s.spd.tpsort_tpsort_id,
                                                    s.spd.Type_sorts_plants.name
                                                } into g2
                                                select new SortFieldDictionary
                                                {
                                                    Id = g2.Key.tpsort_tpsort_id,
                                                    Name = g2.Key.name,
                                                    Fields = g2.ToList()

                                                }).ToList(),
                                      Fields = (from k in g.Select(x => x.ftp)
                                                select new FieldDictionary
                                                {
                                                    Id = k.fiel_fiel_id,
                                                    Name = k.Fields.name,
                                                    Area = k.area_plant
                                                }).Distinct().ToList()
                                  }).OrderBy(g => g.Name).ToList();
            var organizationsList = db.Organizations.Where(p => p.deleted != true &&
              (checkOrg == (int)TypeOrganization.Section ? p.org_id == req.OrgId : p.tporg_tporg_id == (int)TypeOrganization.Section)).
                              Select(t => new DictionaryItemsDTO
                              {
                                  Id = t.org_id,
                                  Name = t.name
                              }).OrderBy(t => t.Name).ToList();

            if (checkOrg == (int)TypeOrganization.Company)
            {
                var allOrg = new DictionaryItemsDTO { Id = -1, Name = "Все" };
                organizationsList.Insert(0, allOrg);
            }
            var res = new GrossInfo 
            {
                YearList = yearList,
                Plants = PlantsAndSorts,
                OrganizationsList = organizationsList,
            };
            return res;
        }
        private Product getProduct(int plant_plant_id, int year)
        {
            var prod = (from p in db.Product
                        where p.plant_plant_id == plant_plant_id && p.year == year
                        select p).FirstOrDefault();
            if (prod != null)
            {
                if (String.IsNullOrEmpty(prod.gross_harvest.ToString())) { prod.gross_harvest = 0; }
                return prod;
            }
            else
            {
                prod = new Product
                {
                    id = CommonFunction.GetNextId(db, "Product"),
                    plant_plant_id = plant_plant_id,
                    balance = 0,
                    current_price = 0,
                    year = year,
                    gross_harvest = 0,
                };
                db.Product.Add(prod);
                db.SaveChanges();
                return prod;
            }
        }
        public List<Gross> GetGross(GrossReq req)
        {
            var checkOrg = db.Organizations.Where(t => t.org_id == req.OrgId).Select(t => t.tporg_tporg_id).FirstOrDefault();
            var newOrgId = checkOrg == 1 ? (int?)req.OrgId : null;
            var list = (from g in db.Gross_harvest
                        where g.date.Year == req.Year && (req.PlantId.HasValue ? g.Fields_to_plants.plant_plant_id == req.PlantId : true) &&
                       (newOrgId != null ? g.Fields_to_plants.Fields.org_org_id == newOrgId : true)
                        select new Gross
                        {
                            Id = g.id,
                            Date = g.date,
                            Plant = new DictionaryItemsDTO { Id= g.Fields_to_plants.plant_plant_id, Name = g.Fields_to_plants.Plants.name},
                            Field = new DictionaryItemsDTO {Id = g.Fields_to_plants.fiel_fiel_id, Name = g.Fields_to_plants.Fields.name},
                            Count = g.count/1000,
                            Price = g.price,
                            Cost = g.cost,
                        }).OrderByDescending(p => p.Id).ToList();
            return list;
        }
        //сохранение урожая
        public string AddGross(List<Gross> rows)
        {
            using (var trans = db.Database.BeginTransaction())
            {
                try
                {
                    for (var i = 0; i < rows.Count; i++) {
                        var row = rows[i];
                        var gh = new Gross_harvest
                        {
                          id = CommonFunction.GetNextId(db, "Gross_harvest"),
                          date = row.Date,
                          ftp_ftp_id = (from ftp in db.Fields_to_plants
                          where ftp.plant_plant_id == row.Plant.Id && ftp.fiel_fiel_id == row.Field.Id && ftp.date_ingathering.Year == row.Date.Year
                          select ftp.fielplan_id).FirstOrDefault(),
                          count = row.Count*1000,
                          price = row.Price,
                          cost = row.Cost
                        };
                        db.Gross_harvest.Add(gh);
                        var prod = getProduct((int)row.Plant.Id, row.Date.Year);
                        prod.balance += row.Count;
                        prod.gross_harvest += row.Count;
                    }
                    db.SaveChanges();
                    trans.Commit();
                }
                catch (Exception)
                {
                    trans.Rollback();
                    throw;
                }
            }
            return "success";
        }
        //update gross
        public string UpdateGross(List<DelRow> req)
        {
            //удаляем повторяющиеся строки
            for (int i = 0; i < req.Count; i++)
            {
                for (int j = 0; j < req.Count; j++)
                {
                    if (j != i && req[i].rows.Id == req[j].rows.Id)
                    { req.RemoveAt(j); j--; }
                }
            }
            //пересчет урожая 
            using (var trans1 = db.Database.BeginTransaction())
            {
                for (int i = 0; i < req.Count; i++)
                {
                    var n = req[i].rows.Id;
                    if (n != 0)
                    {
                        var prod = getProduct((int)req[i].rows.Plant.Id, req[i].rows.Date.Year);
                        var arr = db.Gross_harvest.Where(p => p.id == n).FirstOrDefault();
                        if (arr != null)
                        {
                            prod.balance -= (arr.count / 1000); //возвращаем остаток,т
                            prod.balance += req[i].rows.Count; //  новое
                            prod.gross_harvest -= (arr.count / 1000);  //сбор
                            prod.gross_harvest += req[i].rows.Count;  //сбор
                        }
                    }
                }
                db.SaveChanges();
                trans1.Commit();
            }
            using (var trans = db.Database.BeginTransaction())
            {
                for (int i = 0; i < req.Count; i++)
                {
                    //update
                    var obj = req[i].rows;
                    var updateDic = db.Gross_harvest.Where(o => o.id == obj.Id).FirstOrDefault();
                    if (updateDic != null)
                    {
                        updateDic.date = obj.Date;
                        updateDic.count = obj.Count * 1000;
                        updateDic.ftp_ftp_id = (from ftp in db.Fields_to_plants
                                                where ftp.plant_plant_id == obj.Plant.Id && ftp.fiel_fiel_id == obj.Field.Id && ftp.date_ingathering.Year == obj.Date.Year
                                                select ftp.fielplan_id).FirstOrDefault();
                    }
                }
                db.SaveChanges();
                trans.Commit();
            }
            return "ok";
        }

        //удаляем строку из "Урожай" и вычитаем из сбора
        public string DeleteGross(List<DelRow> req)
        {
            using (var trans = db.Database.BeginTransaction())
            {
                for (int i = 0; i < req.Count; i++)
                {
                    var row = req[i].rows;
                    var prod = getProduct((int)row.Plant.Id, row.Date.Year);
                    prod.balance -= row.Count;
                    prod.gross_harvest -= row.Count;
                    var n = req[i].rows.Id;
                    var doc = db.Gross_harvest.Where(s => s.id == n).FirstOrDefault();
                    if (doc == null)
                        throw new BllException(15, "Документ не был найден , обновите страницу");
                    db.Gross_harvest.Remove(doc);

                }
                db.SaveChanges();
                trans.Commit();
            }
            return "ok";
        }
       
        //получение продаж
        public SaleModel GetSales(GrossReq grossReq)
        {
            var result = new SaleModel();
            result.res = (from g in db.Act_sale
                        where g.date.Year == grossReq.Year && (grossReq.PlantId.HasValue ? g.plant_plant_id == grossReq.PlantId : true)
                        select new Sale
                        {
                            Id = g.id,
                            Date = g.date,
                            Plant = new DictionaryItemsDTO { Id = g.plant_plant_id, Name = g.Plants.name },
                            Sort = g.sort_sort_id.HasValue ? new DictionaryItemsDTO { Id = g.sort_sort_id, Name = g.Type_sorts_plants.name } : null,
                            Count = g.count/1000,
                            Price = g.price,
                            Cost = g.cost,
                            Balance = db.Product.Where(t => t.plant_plant_id == grossReq.PlantId && t.year == grossReq.Year).Select(t => t.balance).FirstOrDefault(),
                        }).OrderByDescending(p => p.Id).ToList();
            result.Balance = db.Product.Where(t => t.plant_plant_id == grossReq.PlantId && t.year == grossReq.Year).Select(t => t.gross_harvest).FirstOrDefault() ?? 0;
            return result;
        }
     
        //сохранение продаж
        public string AddSales(List<Sale> rows)
        { 
            using (var trans = db.Database.BeginTransaction())
            {
                try
                {
                    for (var i = 0; i < rows.Count; i++)
                    {
                        var row = rows[i];
                        var gh = new Act_sale
                        {
                            id = CommonFunction.GetNextId(db, "Act_sale"),
                            date = row.Date,
                            plant_plant_id = (int)row.Plant.Id,
                            sort_sort_id = row.Sort != null ? row.Sort.Id : null,
                            count = row.Count*1000,
                            price = row.Price,
                            cost = row.Cost
                        };
                        db.Act_sale.Add(gh);

                        var prod = getProduct((int)row.Plant.Id, row.Date.Year);
                        prod.balance -= row.Count;
                    }
                    db.SaveChanges();
                    trans.Commit();
                }
                catch (Exception)
                {
                    trans.Rollback();
                    throw;
                }
            }
            return "success";
        }
        //удаляем строку из Продаж
        public string DeleteSales(List<DelRow> req)
        {
            using (var trans = db.Database.BeginTransaction())
            {
                for (int i = 0; i < req.Count; i++)
                {
                    var row = req[i].rows;
                    var prod = getProduct((int)row.Plant.Id, row.Date.Year);
                    prod.balance += row.Count; //возвращаем остаток
                    var n = req[i].rows.Id;
                    var doc = db.Act_sale.Where(s => s.id == n).FirstOrDefault();
                    if (doc == null)
                    throw new BllException(15, "Документ не был найден , обновите страницу");
                    db.Act_sale.Remove(doc);
                }
                db.SaveChanges();
                trans.Commit();
            }
            return "ok";
        }

        public string UpdateSales(List<DelRow> req)
        {
            //удаляем повторяющиеся строки
            for (int i = 0; i < req.Count; i++)
            {
                for (int j = 0; j < req.Count; j++)
                {
                    if (j != i && req[i].rows.Id == req[j].rows.Id)
                    { req.RemoveAt(j); j--; }
                }
            }
            //пересчет урожая 
            using (var trans1 = db.Database.BeginTransaction())
            {
                for (int i = 0; i < req.Count; i++)
                {
                    var n = req[i].rows.Id;
                    if (n != 0)
                    {
                        var prod = getProduct((int)req[i].rows.Plant.Id, req[i].rows.Date.Year);
                        var arr = db.Act_sale.Where(p => p.id == n).FirstOrDefault();
                        if (arr != null)
                        {
                            prod.balance += (arr.count / 1000); //возвращаем остаток,т
                            prod.balance -= req[i].rows.Count; //  новое
                        }
                    }
                }
                db.SaveChanges();
                trans1.Commit();
            }
            using (var trans = db.Database.BeginTransaction())
            {
                for (int i = 0; i < req.Count; i++)
                {
                    //update
                    var obj = req[i].rows;
                    var updateDic = db.Act_sale.Where(o => o.id == obj.Id).FirstOrDefault();
                    if (updateDic != null)
                    {
                        updateDic.date = obj.Date;
                        updateDic.count = obj.Count * 1000;
                        updateDic.price = obj.Price;
                        updateDic.cost = obj.Price * obj.Count;
                    }
                }
                db.SaveChanges();
                trans.Commit();
            }
            return "ok";
        }




    }
}
