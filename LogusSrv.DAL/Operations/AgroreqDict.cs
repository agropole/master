﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LogusSrv.DAL.DBModel;
using LogusSrv.DAL.Entities.Res;

namespace LogusSrv.DAL.Operations
{
    public class AgroreqDict
    {
        public List<AgroreqModel> GetAgroreq(LogusDBEntities db)
        {
            return db.Agrotech_requirment.Where(p => p.deleted != true).Select(p => new AgroreqModel
            {
                id = p.id,
                name = p.name,
                deleted = p.deleted
            }).ToList();
        }

        public string UpdateAgroreq(List<ModelUpdateAgroreq> updateObject, LogusDBEntities db)
        {
            using (var trans = db.Database.BeginTransaction())
            {
                try
                {
                    var newId = (db.Agrotech_requirment.Max(t => (int?)t.id) ?? 0) + 1;
                    for (var i = 0; i < updateObject.Count; i++)
                    {
                        var obj = updateObject[i].Document;
                        switch (updateObject[i].command)
                        {
                            case "del":
                                var delDic = db.Agrotech_requirment.Where(o => o.id == obj.id).FirstOrDefault();
                                delDic.deleted = true;
                                break;
                            case "update":
                                var updateDic = db.Agrotech_requirment.Where(o => o.id == obj.id).FirstOrDefault();
                                updateDic.name = obj.name;
                                break;
                            case "insert":
                                var newDic = new Agrotech_requirment
                                {
                                    id = newId,
                                    name = obj.name,
                                };
                                newId++;
                                db.Agrotech_requirment.Add(newDic);
                                break;
                        }
                    }
                    db.SaveChanges();
                    trans.Commit();
                }
                catch (Exception)
                {
                    trans.Rollback();
                    throw;
                }
            }
            return "success";
        }
    }

    public class AgroreqModel
    {
        public int? id { get; set; }
        public string name { get; set; }
        public bool? deleted { get; set; }
    }

    public class ModelUpdateAgroreq : UpdateDictModel
    {
        public AgroreqModel Document { get; set; }
    }
}
