﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using LogusSrv.DAL.DBModel;
using LogusSrv.DAL.Entities.Res;
using LogusSrv.DAL.Enums;
using LogusSrv.DAL.Entities.DTO;
using System.Drawing;


namespace LogusSrv.DAL.Operations
{
    public class DashboardDb
    {
        protected readonly LogusDBEntities db = new LogusDBEntities();

        public DashboardResModel GetCosts(int plant_id)
        {
            var dashResult = new DashboardResModel{
                Spenttime = 0,
                LaborCosts = 0,
                Area = 0
            };
            var taskList = (from task in db.Tasks
                             where task.Fields_to_plants.plant_plant_id == plant_id

                             select new TaskDashboardModel
                             {
                               
                                 Worktime = task.worktime,
                                 TotalSalary = task.total_salary,
                             }
                             );

            var fieldList = (from ftp in db.Fields_to_plants
                          where ftp.Plants.plant_id == plant_id
                          select ftp.Fields).ToList();

            dashResult.Area = fieldList.Sum(l => l.area);
            dashResult.Spenttime = taskList.Sum(l=> l.Worktime ?? 0);
            dashResult.LaborCosts = taskList.Sum(l => l.TotalSalary ?? 0);
            return dashResult;
        }

        public List<PlantResModel> GetPlants()
        {
            var plantList = (from plant in db.Plants
                             select new PlantResModel
                             {
                                 Id = plant.plant_id,
                                 Name = plant.name
                             }).ToList();
            return plantList;
        }

        //PRINT MAP
        public List<PlantsDTO> GetStructureCrop(int year)
        {
            var dopArea = (from ftp in db.Fields_to_plants
                           where ftp.date_ingathering.Year == year && ftp.plant_plant_id != 36
                           group ftp.area_plant by new { ftp.plant_plant_id, ftp.Plants.color, ftp.Plants.short_name } into g
                           select new PlantsDTO
                           {
                               plant_id = g.Key.plant_plant_id,
                               plant_name = g.Key.short_name,
                               area = g.Sum(),
                               color = g.Key.color,
                           }).ToList();
            dopArea.Add(new PlantsDTO { plant_name = "Итого:", area = dopArea.Sum(t => t.area) });
            return dopArea;
        }



    }
}
