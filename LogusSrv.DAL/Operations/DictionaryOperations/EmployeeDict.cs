﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LogusSrv.DAL.DBModel;
using LogusSrv.DAL.Entities.DTO;
using LogusSrv.DAL.Entities.Res;

namespace LogusSrv.DAL.Operations.DictionaryOperations
{
    public class EmployeeDict
    {
        public List<ModelEmployeeDict> GetEmployeeDict(LogusDBEntities db)
        {
            var res = (from t in db.Employees.AsEnumerable()
                       where t.deleted == false
                       select new ModelEmployeeDict
                       {
                           emp_id = t.emp_id,
                           second_name = t.second_name,
                           first_name = t.first_name,
                           middle_name = t.middle_name,
                           short_name = t.short_name,
                           code = t.code,
                           driver_license = t.driver_license,
                           pst_pst_id = new DictionaryItemsDTO { Id = t.pst_pst_id, Name = t.Posts.name },
                           org_org_id = new DictionaryItemsDTO { Id = t.org_org_id, Name = t.Organizations.name },
                           category = new DictionaryItemsDTO { Id = ! String.IsNullOrEmpty(t.driver_class) ? (int?)Int32.Parse(t.driver_class) : null,
                                                               Name = !String.IsNullOrEmpty(t.driver_class) ? getNameCategory(Int32.Parse(t.driver_class)) : null
                           },
                       }).ToList();
            return res;
        }

        public string getNameCategory(int? Id) {

            if (Id == 1) { return "Основной"; }
            if (Id == 2) { return "Вспомогательный"; }
            if (Id == 3) { return "Ответственные"; }
            return null;
        }



        public string UpdateFieldsDict(List<ModelUpdateEmployeeDict> updateObject, LogusDBEntities db)
        {
            using (var trans = db.Database.BeginTransaction())
            {
                try
                {
                    for (var i = 0; i < updateObject.Count; i++)
                    {
                        var obj = updateObject[i].Document;
                        switch (updateObject[i].command)
                        {
                            case "del":
                                var delDic = db.Employees.Where(o => o.emp_id == obj.emp_id).FirstOrDefault();
                                delDic.deleted = true;
                                break;
                            case "update":
                                var updateDic = db.Employees.Where(o => o.emp_id == obj.emp_id).FirstOrDefault();
                                updateDic.second_name = obj.second_name;
                                updateDic.first_name = obj.first_name;
                                updateDic.middle_name = obj.middle_name;
                                updateDic.short_name = obj.short_name;
                                updateDic.code = obj.code;
                                updateDic.driver_license = obj.driver_license;
                                updateDic.pst_pst_id = obj.pst_pst_id.Id.Value;
                                updateDic.org_org_id = obj.org_org_id.Id.Value;
                                updateDic.driver_class = obj.category.Id.ToString();
                                break;
                            case "insert":
                                var newDic = new Employees
                                {
                                    second_name = obj.second_name,
                                    first_name = obj.first_name,
                                    middle_name = obj.middle_name,
                                    short_name = obj.short_name,
                                    code = obj.code,
                                    driver_license = obj.driver_license,
                                    pst_pst_id = obj.pst_pst_id.Id.Value,
                                    deleted = false,
                                    org_org_id = obj.org_org_id.Id.Value,
                                    driver_class = obj.category.Id.ToString(),
                                };
                                db.Employees.Add(newDic);
                                break;
                        }
                    }
                    db.SaveChanges();
                    trans.Commit();
                }
                catch (Exception)
                {
                    trans.Rollback();
                    throw;
                }
            }
            return "success";
        }

        public DetailEmployeeDict GetEmployeeInfoDict(LogusDBEntities db)
        {
            return new DetailEmployeeDict
            {
                org_org_id = (from t in db.Organizations
                              select new DictionaryItemsDTO
                              {
                                  Id = t.org_id,
                                  Name = t.name
                              }).ToList(),
                pst_pst_id = (from p in db.Posts
                              select new DictionaryItemsDTO
                              {
                                  Id = p.pst_id,
                                  Name = p.name
                              }).ToList(),

            category = new List<DictionaryItemsDTO> {new DictionaryItemsDTO 
               {Id = 1, Name ="Основной" } ,new DictionaryItemsDTO  {Id = 2, Name ="Вспомогательный" },
               new DictionaryItemsDTO  {Id = 3, Name ="Ответственные" }},
            };
        }
    }

    public class ModelEmployeeDict
    {
        public int? emp_id { get; set; }
        public string first_name { get; set; }
        [Required(ErrorMessage = "Не заполнено поле Фамилия")]
        public string second_name { get; set; }
        public string middle_name { get; set; }
        [Required(ErrorMessage = "Не заполнено поле ФИО")]
        public string short_name { get; set; }
        public string code { get; set; }
        public string driver_license { get; set; }
        [Required(ErrorMessage = "Не заполнено поле Должность")]
        public DictionaryItemsDTO pst_pst_id { get; set; }
        [Required(ErrorMessage = "Не заполнено поле Организация")]
        public DictionaryItemsDTO org_org_id { get; set; }

        public DictionaryItemsDTO category { get; set; }

    }

    public class DetailEmployeeDict
    {
        public List<DictionaryItemsDTO> pst_pst_id { get; set; }
        public List<DictionaryItemsDTO> org_org_id { get; set; }
        public List<DictionaryItemsDTO> category { get; set; }
    }

    public class ModelUpdateEmployeeDict : UpdateDictModel
    {
        public ModelEmployeeDict Document { get; set; }
    }
}
