﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LogusSrv.DAL.DBModel;
using LogusSrv.DAL.Entities.DTO;
using LogusSrv.DAL.Entities.Res;

namespace LogusSrv.DAL.Operations.DictionaryOperations
{
    public class ModulesDict
    {
        public List<ModulesDictModel> GetModules(LogusDBEntities db)
        {
            var res = (from t in db.Modules
                       where t.deleted == false
                       select new ModulesDictModel
                       {
                           mod_id = t.mod_id,
                           name = t.name,
                           description = t.description,
                       }).ToList();
            return res;
        }

        public string UpdateModulesDict(List<ModelUpdateModules> updateObject, LogusDBEntities db)
        {
            using (var trans = db.Database.BeginTransaction())
            {
                try
                {
                    var newId = (db.Modules.Max(t => (int?)t.mod_id) ?? 0) + 1;
                    for (var i = 0; i < updateObject.Count; i++)
                    {
                        var obj = updateObject[i].Document;
                        switch (updateObject[i].command)
                        {
                            case "del":
                                var delDic = db.Modules.Where(o => o.mod_id == obj.mod_id).FirstOrDefault();
                                delDic.deleted = true;
                                break;
                            case "update":
                                var updateDic = db.Modules.Where(o => o.mod_id == obj.mod_id).FirstOrDefault();
                                updateDic.name = obj.name;
                                updateDic.description = obj.description;
                                break;
                            case "insert":
                                var newDic = new Modules
                                {
                                    mod_id = newId,
                                    name = obj.name,
                                    description = obj.description,
                                    deleted = false,
                                };
                                newId++;
                                db.Modules.Add(newDic);
                                break;
                        }
                    }
                    db.SaveChanges();
                    trans.Commit();
                }
                catch (Exception)
                {
                    trans.Rollback();
                    throw;
                }
            }
            return "success";
        }

        //public DetailModulesModel GetDetailGarages(LogusDBEntities db)
        //{
        //    var res = new DetailModulesModel
        //    {
        //        org_org_id = (from t in db.Organizations
        //                      select new DictionaryItemsDTO
        //                      {
        //                          Id = t.org_id,
        //                          Name = t.name
        //                      }).ToList()
        //    };
        //    return res;
        //}
    }

    public class ModulesDictModel
    {
        public int? mod_id { get; set; }
        public string name { get; set; }
        public string description { get; set; }

    }

    public class DetailModulesModel
    {
        public List<DictionaryItemsDTO> org_org_id { get; set; }
    }

    public class ModelUpdateModules : UpdateDictModel
    {
        public ModulesDictModel Document { get; set; }
    }
}
