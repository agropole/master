﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LogusSrv.DAL.DBModel;
using LogusSrv.DAL.Entities.DTO;
using LogusSrv.DAL.Entities.Res;
using System.Data.Entity.Validation;

namespace LogusSrv.DAL.Operations.DictionaryOperations
{
    public class TraktorsDict
    {
        public List<ModelTraktorsDict> GetTraktorsDict(LogusDBEntities db)
        {
            var res = (from t in db.Traktors
                       where t.deleted == false
                       select new ModelTraktorsDict
                       {
                           trakt_id = t.trakt_id,
                           track_id = t.track_id,
                           name = t.name,
                           model = t.model,
                           reg_num = t.reg_num,
                           width = t.width,
                           code = t.code,
                           category = new DictionaryItemsDTO { Id = t.modtr_modtr_id, Name = t.modtr_modtr_id == 1 ? 
                               "Основной" : t.modtr_modtr_id == 2 ? "Вспомогательный" : null        },
                           gar_gar_id = new DictionaryItemsDTO
                           {
                               Id = t.gar_gar_id,
                               Name = t.Garages.name
                           },
                           id_grtrakt = new DictionaryItemsDTO
                           {
                               Id = t.id_grtrakt,
                               Name = t.Group_traktors.name
                           },
                           id_type_way_list = new DictionaryItemsDTO 
                           {
                               Id = t.id_type_way_list,
                               Name = t.Type_way_list.name
                           },
                           //тип топлива
                           tpf_tpf_id = new DictionaryItemsDTO
                           {
                               Id = t.tpf_tpf_id,
                               Name = t.Type_fuel.name
                           },
                           emp_emp_id = new DictionaryItemsDTO
                           {
                               Id = db.Employees.Where(r => r.emp_id == t.emp_emp_id).Select(r => r.emp_id).FirstOrDefault(),
                               Name = db.Employees.Where(r=>r.emp_id == t.emp_emp_id).Select(r=>r.short_name).FirstOrDefault(),
                             //  index = db.Employees.Where(r => r.emp_id == t.emp_emp_id).Select(r => r.pst_pst_id).FirstOrDefault(),
                           },
                           tptrak_tptrak_id = new DictionaryItemsDTO
                           {
                               Id = t.id_grtrakt,
                               Name = t.Group_traktors.name
                           },
                       }).ToList();

            var res1 = (from r in db.Traktors
                        where r.deleted == false
                        select new ModelTraktorsDict
                        {
                            emp_emp_id = new DictionaryItemsDTO
                            {
                                Id = db.Employees.Where(r1 => r1.emp_id == r.emp_emp_id).Select(r1 => r1.emp_id).FirstOrDefault(),
                                index = db.Employees.Where(r1 => r1.emp_id == r.emp_emp_id).Select(r1 => r1.pst_pst_id).FirstOrDefault(),
                            }
                        }).ToList();

            for (int i = 0; i < res.Count; i++)
            {
                var id = res[i].emp_emp_id.Id;
                res[i].emp_emp_id.index = res1.Where(t => t.emp_emp_id.Id == id).Select(t => t.emp_emp_id.index).FirstOrDefault();
            }
                return res;
        }

        public string UpdateTraktorsDict(List<ModelUpdateTraktors> updateObject, LogusDBEntities db)
        {
            using (var trans = db.Database.BeginTransaction())
            {
                try
                {
                    for (var i = 0; i < updateObject.Count; i++)
                    {
                        var obj = updateObject[i].Document;
                        switch (updateObject[i].command)
                        {
                            case "del":
                                var delDic = db.Traktors.Where(o => o.trakt_id == obj.trakt_id).FirstOrDefault();
                                delDic.deleted = true;
                                break;
                            case "update":
                                if (obj.trakt_id != null)
                                {
                                    var updateDic = db.Traktors.Where(o => o.trakt_id == obj.trakt_id).FirstOrDefault();
                                    updateDic.name = obj.name;
                                    updateDic.model = obj.model;
                                    updateDic.reg_num = obj.reg_num;
                                    updateDic.uniq_code = "";
                                    updateDic.width = obj.width;
                                    updateDic.track_id = obj.track_id;
                                    updateDic.id_grtrakt = obj.tptrak_tptrak_id.Id;
                                    updateDic.code = obj.code;
                                    updateDic.gar_gar_id = (int)obj.gar_gar_id.Id;
                                    updateDic.emp_emp_id = (int?)obj.emp_emp_id.Id;
                                    updateDic.tpf_tpf_id = (int)obj.tpf_tpf_id.Id;
                                    updateDic.id_type_way_list = obj.id_type_way_list.Id;
                                    updateDic.modtr_modtr_id = obj.category.Id;
                                }
                                break;
                            case "insert":
                                var newDic = new Traktors
                                {
                                    name = obj.name,
                                    model = obj.model,
                                    reg_num = obj.reg_num,
                                    uniq_code = "",
                                    width = obj.width,
                                    track_id = obj.track_id,
                                    id_grtrakt = obj.id_grtrakt != null ? obj.id_grtrakt.Id : null,
                                    gar_gar_id = (int)obj.gar_gar_id.Id,
                                    tpf_tpf_id = (int)obj.tpf_tpf_id.Id,
                                    emp_emp_id = (int?)obj.emp_emp_id.Id,
                                    tptrak_tptrak_id = 1,
                                    deleted = false,
                                    code = obj.code,
                                    img = "assets/img/icons/traktor.png",
                                    id_type_way_list = obj.id_type_way_list.Id,
                                    modtr_modtr_id = obj.category.Id,

                                };
                                db.Traktors.Add(newDic);
                                break;
                        }
                    }

                    //
                    try
                    {
                        db.SaveChanges();
                    }
                    catch (DbEntityValidationException e)
                    {
                        foreach (var eve in e.EntityValidationErrors)
                        {
                            Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                                eve.Entry.Entity.GetType().Name, eve.Entry.State);
                            foreach (var ve in eve.ValidationErrors)
                            {
                                Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                                    ve.PropertyName, ve.ErrorMessage);
                            }
                        }
                        throw;
                    }
                    //
                    trans.Commit();
                }
                catch (Exception)
                {
                    trans.Rollback();
                    throw;
                }
            }
            return "success";
        }

        public DetailTraktorsModel GetDetailTraktors(LogusDBEntities db)
        {
            var res = new DetailTraktorsModel
            {
                gar_gar_id = (from t in db.Garages
                              select new DictionaryItemsDTO
                              {
                                  Id = t.gar_id,
                                  Name = t.name
                              }).ToList(),
                tptrak_tptrak_id = (from t in db.Group_traktors
                                    where t.deleted == false
                                    select new DictionaryItemsDTO
                                    {
                                        Id = t.grtrak_id,
                                        Name = t.name
                                    }).ToList(),
                tpf_tpf_id = (from t in db.Type_fuel
                              where t.fuel_flag == true
                              select new DictionaryItemsDTO
                              {
                                  Id = t.id,
                                  Name = t.name
                              }).ToList(),
               category = new List<DictionaryItemsDTO> {new DictionaryItemsDTO 
               {Id = 1, Name ="Основной" } ,new DictionaryItemsDTO  {Id = 2, Name ="Вспомогательный" } },

                id_type_way_list = db.Type_way_list.Select(p => new DictionaryItemsDTO {
                    Id = p.id,
                    Name = p.name
                }).ToList(),
                emp_emp_id = (from t in db.Employees
                              where t.deleted == false &&
                              (t.pst_pst_id==1 || t.pst_pst_id==5)
                              select new DictionaryItemsDTO
                              {
                                  Id = t.emp_id,
                                  Name = t.short_name,
                                  index =t.pst_pst_id,
                              }).ToList(),
            };
            return res;
        }
    }

    public class ModelTraktorsDict
    {
        public int? trakt_id { get; set; }
        public string name { get; set; }
        public string model { get; set; }
        public DictionaryItemsDTO category { get; set; }
        public string reg_num { get; set; }
        public string code { get; set; }
        public decimal? width { get; set; }
        [Required(ErrorMessage = "Не заполнено обязательно поле Трекер. Если трек-номер не был присвоен, то присвойте номер формата 99xxxx, сверившись с максимальным значением трек-номера в справочнике")]
        public int? track_id { get; set; }
        [Required(ErrorMessage = "Не заполнено обязательно поле Гараж.")]
        public DictionaryItemsDTO gar_gar_id { get; set; }
        [Required(ErrorMessage = "Не заполнено обязательно поле Путевой лист.")]
        public DictionaryItemsDTO id_type_way_list { get; set; }
        [Required(ErrorMessage = "Не заполнено обязательно поле Тип топлива.")]
        public DictionaryItemsDTO tpf_tpf_id { get; set; }
        public DictionaryItemsDTO emp_emp_id { get; set; }
        public DictionaryItemsDTO id_grtrakt { get; set; }
        public DictionaryItemsDTO tptrak_tptrak_id { get; set; }
    }

    public class DetailTraktorsModel
    {
        public List<DictionaryItemsDTO> gar_gar_id { get; set; }
        public List<DictionaryItemsDTO> tpf_tpf_id { get; set; }
        public List<DictionaryItemsDTO> emp_emp_id { get; set; }
        public List<DictionaryItemsDTO> tptrak_tptrak_id { get; set; }
        public List<DictionaryItemsDTO> id_type_way_list { get; set; }
        public List<DictionaryItemsDTO> category { get; set; }

    }

    public class ModelUpdateTraktors : UpdateDictModel
    {
        public ModelTraktorsDict Document { get; set; }
    }
}
