﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LogusSrv.DAL.DBModel;
using LogusSrv.DAL.Entities.DTO;
using LogusSrv.DAL.Entities.Res;
using System.Data.Entity.Spatial;
using LogusSrv.CORE.Exceptions;

namespace LogusSrv.DAL.Operations.DictionaryOperations
{
    public class GaragesDict
    {
        public List<GaragesDictModel> GetGarages(LogusDBEntities db)
        {
            var res = (from t in db.Garages
                       where t.deleted == false
                       select new GaragesDictModel   
                       {
                           gar_id = t.gar_id,
                           name = t.name,
                           description = t.description,
                           org_org_id = new DictionaryItemsDTO
                           {
                               Id = t.org_org_id,
                               Name = t.Organizations.name
                           },
                              coord = new GeoCoord
                           {
                               coord = t.coord.AsText(),  
                               center = t.center.AsText()
                           }   
                       }).ToList();
            return res;
        }

        public string UpdateGaragesDict(List<ModelUpdateGarages> updateObject, LogusDBEntities db)
        {
            using (var trans = db.Database.BeginTransaction())
            {
                try
                {
                    var newId = (db.Garages.Max(t => (int?)t.gar_id) ?? 0) + 1;
                    for (var i = 0; i < updateObject.Count; i++)
                    {
                        var obj = updateObject[i].Document;
                        if (obj.coord == null) {
                            throw new BllException(14, "Необходимо задать координаты гаража.");
                        }
                        switch (updateObject[i].command)
                        {
                            case "del":
                                var delDic = db.Garages.Where(o => o.gar_id == obj.gar_id).FirstOrDefault();
                                delDic.deleted = true;
                                break;
                            case "update":
                                var updateDic = db.Garages.Where(o => o.gar_id == obj.gar_id).FirstOrDefault();
                                updateDic.name = obj.name;
                                updateDic.description = obj.description;
                                updateDic.org_org_id = (int)obj.org_org_id.Id;
                                if (obj.coord != null)
                                {
                                    updateDic.coord = DbGeography.PolygonFromText(obj.coord.coord, 4326);
                                }
                                if (obj.coord != null)
                                {
                                    updateDic.center = DbGeography.PointFromText(obj.coord.center, 4326);
                                }
                                break;
                            case "insert":
                                var newDic = new Garages
                                {
                                    gar_id = newId,
                                    name = obj.name,
                                    description = obj.description,
                                    org_org_id = (int)obj.org_org_id.Id,
                                    deleted = false,
                                    code = newId.ToString(),
                                    coord = DbGeography.PolygonFromText(obj.coord.coord, 4326),
                                    center = DbGeography.PointFromText(obj.coord.center, 4326)
                                };
                                newId++;
                                db.Garages.Add(newDic);
                                break;
                        }
                    }
                    db.SaveChanges();
                    trans.Commit();
                }
                catch (Exception)
                {
                    trans.Rollback();
                    throw;
                }
            }
            return "success";
        }

        public DetailGaragesModel GetDetailGarages(LogusDBEntities db)
        {
            var res = new DetailGaragesModel
            {
                org_org_id = (from t in db.Organizations
                                      select new DictionaryItemsDTO
                                      {
                                          Id = t.org_id,
                                          Name = t.name
                                      }).ToList()
            };
            return res;
        }
    }

    public class GaragesDictModel
    {
        public int? gar_id { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public DictionaryItemsDTO org_org_id { get; set; }
        public GeoCoord coord { get; set; }
 
    }

    public class DetailGaragesModel
    {
        public List<DictionaryItemsDTO> org_org_id { get; set; }
    }

    public class ModelUpdateGarages : UpdateDictModel
    {
        public GaragesDictModel Document { get; set; }
    }
}
