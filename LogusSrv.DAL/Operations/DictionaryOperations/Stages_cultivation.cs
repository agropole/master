﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LogusSrv.DAL.DBModel;
using LogusSrv.DAL.Entities.DTO;
using LogusSrv.DAL.Entities.Res;
using System.Data.Entity.Validation;
using System.Web;

namespace LogusSrv.DAL.Operations.DictionaryOperations
{
    public class Stages_cultivationDict
    {
        public List<Stages_cultivationDictModel> GetStages_cultivationDict(LogusDBEntities db)
        {
            var res = db.Stages_cultivation.Where(t => t.deleted != true).Select(t => new Stages_cultivationDictModel
            {
                stages_id = t.stages_id,
                name = t.name,
                deleted = t.deleted,
            }).ToList();
            return res;
        }

        public string UpdateStages_cultivationDict(List<ModelUpdateStages_cultivation> updateObject, LogusDBEntities db)
        {
            using (var trans = db.Database.BeginTransaction())
            {
                try
                {
                    var newId = (db.Stages_cultivation.Max(t => (int?)t.stages_id) ?? 0) + 1;
                    for (var i = 0; i < updateObject.Count; i++)
                    {
                        var obj = updateObject[i].Document;
                        switch (updateObject[i].command)
                        {
                            case "del":
                                var delDic = db.Stages_cultivation.Where(o => o.stages_id == obj.stages_id).FirstOrDefault();
                                delDic.deleted = true;
                                break;
                            case "update":
                                var updateDic = db.Stages_cultivation.Where(o => o.stages_id == obj.stages_id).FirstOrDefault();
                                updateDic.name = obj.name;
                                break;
                            case "insert":
                                var newDic = new Stages_cultivation
                                {
                                    stages_id = newId,
                                    name = obj.name,
                                    deleted = false
                                };
                                newId++;
                                db.Stages_cultivation.Add(newDic);
                                break;
                        }
                    }

                    db.SaveChanges();
                    trans.Commit();
                }
                catch (Exception)
                {
                    trans.Rollback();
                    throw;
                }
            }
            return "success";
        }
    }



    public class Stages_cultivationDictModel
    {
        public int? stages_id { get; set; }
        public string name { get; set; }
        public bool? deleted { get; set; }
    }
    public class ModelUpdateStages_cultivation : UpdateDictModel
    {
        public Stages_cultivationDictModel Document { get; set; }
    }
}
