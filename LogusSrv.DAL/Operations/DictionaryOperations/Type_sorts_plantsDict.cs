﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LogusSrv.DAL.DBModel;
using LogusSrv.DAL.Entities.DTO;
using LogusSrv.DAL.Entities.Res;

namespace LogusSrv.DAL.Operations.DictionaryOperations
{
    public class Type_sorts_plantsDict
    {
        public List<ModelType_sorts_plantsDict> GetType_sorts_plantsDict(LogusDBEntities db)
        {
            var res = (from t in db.Type_sorts_plants
                       where t.deleted == false
                       select new ModelType_sorts_plantsDict
                       {
                           tpsort_id = t.tpsort_id,
                           name = t.name,
                           plant_plant_id = new DictionaryItemsDTO
                           {
                               Id = (int)t.plant_plant_id,
                               Name = t.Plants.name
                           }
                       }).ToList();
            return res;
        }

        public string UpdateType_sorts_plantsDict(List<ModelUpdateType_sorts_plants> updateObject, LogusDBEntities db)
        {
            using (var trans = db.Database.BeginTransaction())
            {
                try
                {
                    var newId = (db.Type_sorts_plants.Max(t => (int?)t.tpsort_id) ?? 0) + 1;
                    for (var i = 0; i < updateObject.Count; i++)
                    {
                        var obj = updateObject[i].Document;
                        switch (updateObject[i].command)
                        {
                            case "del":
                                var delDic = db.Type_sorts_plants.Where(o => o.tpsort_id == obj.tpsort_id).FirstOrDefault();
                                delDic.deleted = true;
                                break;
                            case "update":
                                var updateDic = db.Type_sorts_plants.Where(o => o.tpsort_id == obj.tpsort_id).FirstOrDefault();
                                updateDic.name = obj.name;
                                updateDic.plant_plant_id = (int)obj.plant_plant_id.Id;
                                break;
                            case "insert":
                                var newDic = new Type_sorts_plants
                                {
                                    tpsort_id = newId,
                                    name = obj.name,
                                    deleted = false,
                                    plant_plant_id = (int)obj.plant_plant_id.Id
                                };
                                newId++;
                                db.Type_sorts_plants.Add(newDic);
                                break;
                        }
                    }
                    db.SaveChanges();
                    trans.Commit();
                }
                catch (Exception)
                {
                    trans.Rollback();
                    throw;
                }
            }
            return "success";
        }

        public DetailType_sorts_plantsModel GetDetailType_sorts_plants(LogusDBEntities db)
        {
            var res = new DetailType_sorts_plantsModel
            {
                plant_plant_id = (from t in db.Plants
                                      select new DictionaryItemsDTO
                                      {
                                          Id = t.plant_id,
                                          Name = t.name
                                      }).ToList()
            };
            return res;
        }
    }

    public class ModelType_sorts_plantsDict
    {
        public int? tpsort_id { get; set; }
        public string name { get; set; }
        public DictionaryItemsDTO plant_plant_id { get; set; }
    }

    public class ModelUpdateType_sorts_plants : UpdateDictModel
    {
        public ModelType_sorts_plantsDict Document { get; set; }
    }

    public class DetailType_sorts_plantsModel
    {
        public List<DictionaryItemsDTO> plant_plant_id { get; set; }
    }
}
