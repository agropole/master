﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LogusSrv.DAL.DBModel;
using LogusSrv.DAL.Entities.DTO;
using LogusSrv.DAL.Entities.Res;

namespace LogusSrv.DAL.Operations.DictionaryOperations
{
    public class KPIDict
    {
        public List<ModelKPI> Get(LogusDBEntities db)
        {
            var res = (from k in db.KPI
                       where k.deleted == false
                       select new ModelKPI 
                       {
                           id = k.id,
                           name = k.name,
                           tpsal_tpsal_id = new DictionaryItemsDTO {
                               Id = k.tpsal_tpsal_id,
                               Name = k.Type_salary_doc.name
                           },
                           standard_value = k.standard_value,
                           deviation_limit = k.deviation_limit,
                           rate = k.rate,
                           deviation_limit2 = k.deviation_limit2,
                           rate2 = k.rate2,
                           deviation_limit3 = k.deviation_limit3,
                           rate3 = k.rate3
                       }
                ).ToList();
            return res;
        }

        public string Update(List<ModelUpdateKPI> updateObject, LogusDBEntities db)
        {
            using (var trans = db.Database.BeginTransaction())
            {
                try
                {
                    var newId = (db.Materials.Max(t => (int?)t.mat_id) ?? 0) + 1;
                    for (var i = 0; i < updateObject.Count; i++)
                    {
                        var obj = updateObject[i].Document;
                        switch (updateObject[i].command)
                        {
                            case "del":
                                var delDic = db.KPI.Where(o => o.id == obj.id).FirstOrDefault();
                                delDic.deleted = true;
                                break;
                            case "update":
                                var updateDic = db.KPI.Where(o => o.id == obj.id).FirstOrDefault();
                                updateDic.name = obj.name;
                                updateDic.tpsal_tpsal_id = obj.tpsal_tpsal_id.Id;
                                updateDic.standard_value = obj.standard_value;
                                updateDic.deviation_limit = obj.deviation_limit;
                                updateDic.rate = obj.rate;
                                updateDic.deviation_limit2 = obj.deviation_limit2;
                                updateDic.rate2 = obj.rate2;
                                updateDic.deviation_limit3 = obj.deviation_limit3;
                                updateDic.rate3 = obj.rate3;
                                break;
                            case "insert":
                                var newDic = new KPI
                                {
                                    id = newId,
                                    name = obj.name,
                                    tpsal_tpsal_id = obj.tpsal_tpsal_id.Id,
                                    standard_value = obj.standard_value,
                                    deviation_limit = obj.deviation_limit,
                                    rate = obj.rate,
                                    deviation_limit2 = obj.deviation_limit2,
                                    rate2 = obj.rate2,
                                    deviation_limit3 = obj.deviation_limit3,
                                    deleted = false,
                                    rate3 = obj.rate3
                                };
                                newId++;
                                db.KPI.Add(newDic);
                                break;
                        }
                    }
                    db.SaveChanges();
                    trans.Commit();
                }
                catch (Exception)
                {
                    trans.Rollback();
                    throw;
                }
            }
            return "success";
        }

        public DetailKPIModel Detail(LogusDBEntities db)
        {
            return new DetailKPIModel
            {
                tpsal_tpsal_id = db.Type_salary_doc.Select(p => new DictionaryItemsDTO { Id = p.tpsal_id, Name = p.name }).ToList()
            };
        }
    }

    public class ModelKPI
    {
        public int? id { get; set; }

        public string name { get; set; }

        public float standard_value { get; set; }

        public float deviation_limit { get; set; }

        public int rate { get; set; }

        public float? deviation_limit2 { get; set; }

        public int? rate2 { get; set; }

        public float? deviation_limit3 { get; set; }

        public int? rate3 { get; set; }

        public DictionaryItemsDTO tpsal_tpsal_id { get; set; }
    }

    public class DetailKPIModel
    {
        public List<DictionaryItemsDTO> tpsal_tpsal_id { get; set; }
    }

    public class ModelUpdateKPI : UpdateDictModel
    {
        public ModelKPI Document { get; set; }
    }
}
