﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LogusSrv.DAL.DBModel;
using LogusSrv.DAL.Entities.DTO;
using LogusSrv.DAL.Entities.Res;

namespace LogusSrv.DAL.Operations.DictionaryOperations
{
    public class EquipmentsDict
    {
        public List<ModelEquipmentDict> GetEquipmentsDict(LogusDBEntities db)
        {
            var res = (from t in db.Equipments
                       where t.deleted == false
                       select new ModelEquipmentDict
                       {
                           equi_id = t.equi_id,
                           name = t.name,
                           model = t.model,
                           shot_name = t.shot_name,
                           width = t.width,
                           code = t.code,
                           reg_num = t.reg_num,
                         //  uniq_code = t.uniq_code,
                           tpequi_tpequi_id = new DictionaryItemsDTO
                           {
                               Id = t.tpequi_tpequi_id,
                               Name = t.Type_equipments.name
                           },
                           modequi_modequi_id = new DictionaryItemsDTO
                           {
                               Id = t.modequi_modequi_id,
                               Name = db.Organizations.Where(t1 => t1.org_id == t.modequi_modequi_id).Select(t1 => t1.name).FirstOrDefault(),
                           },
                       }).ToList();
            return res;
        }

        public string UpdateEquipmentsDict(List<ModelUpdateEquipments> updateObject, LogusDBEntities db)
        {
            using (var trans = db.Database.BeginTransaction())
            {
                try
                {
                    for (var i = 0; i < updateObject.Count; i++)
                    {
                        var obj = updateObject[i].Document;
                        switch (updateObject[i].command)
                        {
                            case "del":
                                var delDic = db.Equipments.Where(o => o.equi_id == obj.equi_id).FirstOrDefault();
                                delDic.deleted = true;
                                break;
                            case "update":
                                var updateDic = db.Equipments.Where(o => o.equi_id == obj.equi_id).FirstOrDefault();
                                updateDic.name = obj.name;
                                updateDic.model = obj.model;
                                updateDic.shot_name = obj.shot_name;
                                updateDic.code = obj.code;
                                updateDic.width = obj.width ?? 0;
                                updateDic.tpequi_tpequi_id = (int)obj.tpequi_tpequi_id.Id;
                                updateDic.reg_num = obj.reg_num;
                                updateDic.modequi_modequi_id = obj.modequi_modequi_id.Id;
                                break;
                            case "insert":
                                var newDic = new Equipments
                                {
                                    name = obj.name,
                                    model = obj.model != null ? obj.model : "",
                                    code = obj.code != null ? obj.code : "",
                                    shot_name = obj.shot_name,
                                    width = obj.width ?? 0,
                                    tpequi_tpequi_id = (int)obj.tpequi_tpequi_id.Id,
                                    deleted = false,
                                    reg_num = obj.reg_num,
                                    modequi_modequi_id = obj.modequi_modequi_id.Id,
                                };
                                db.Equipments.Add(newDic);
                                break;
                        }
                    }
                    db.SaveChanges();
                    trans.Commit();
                }
                catch (Exception)
                {
                    trans.Rollback();
                    throw;
                }
            }
            return "success";
        }

        public DetailEquipmentsModel GetDetailEquipments(LogusDBEntities db)
        {
            var res = new DetailEquipmentsModel
            {
                tpequi_tpequi_id = (from t in db.Type_equipments
                                      select new DictionaryItemsDTO
                                      {
                                          Id = t.tpequi_id,
                                          Name = t.name
                                      }).ToList(),
                                       modequi_modequi_id = (from t in db.Organizations
                                       where t.deleted != true
                                      select new DictionaryItemsDTO
                                      {
                                          Id = t.org_id,
                                          Name = t.name
                                      }).ToList()
            };
            return res;
        }
    }

    public class ModelEquipmentDict
    {
        public int? equi_id {get; set;}
        public string name {get; set;}
     //   public string uniq_code { get; set; }
        public string model {get; set;}
        public string code { get; set; }
        public string shot_name {get; set;}
        public string reg_num { get; set; }
        public decimal? width {get; set;}
        public DictionaryItemsDTO tpequi_tpequi_id { get; set; }
        public DictionaryItemsDTO modequi_modequi_id { get; set; }
    }

    public class DetailEquipmentsModel
    {
        public List<DictionaryItemsDTO> tpequi_tpequi_id { get; set; }
        public List<DictionaryItemsDTO> modequi_modequi_id { get; set; }
    }

    public class ModelUpdateEquipments : UpdateDictModel
    {
        public ModelEquipmentDict Document { get; set; }
    }
}
