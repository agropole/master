﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LogusSrv.DAL.DBModel;
using LogusSrv.DAL.Entities.DTO;
using LogusSrv.DAL.Entities.Res;

namespace LogusSrv.DAL.Operations.DictionaryOperations
{
    public class TraktorsDict1c
    {
        public List<ModelTraktors1cDict> GetTraktors1сDict(LogusDBEntities db)
        {
            var res = (from t in db.Traktors
                       where t.deleted == false
                       select new ModelTraktors1cDict
                       {
                           trakt_id = t.trakt_id,
                           name = t.name,
                           model = t.model,
                           reg_num = t.reg_num,
                           im_code = t.im_code,
                           int_id = t.int_id,
                           code = t.code
                       }).ToList();
            return res;
        }

        public string UpdateTraktors1сDict(List<ModelUpdateTraktors1с> updateObject, LogusDBEntities db)
        {
            using (var trans = db.Database.BeginTransaction())
            {
                try
                {
                    for (var i = 0; i < updateObject.Count; i++)
                    {
                        var obj = updateObject[i].Document;
                        switch (updateObject[i].command)
                        {
                            case "del":
                                var delDic = db.Traktors.Where(o => o.trakt_id == obj.trakt_id).FirstOrDefault();
                                delDic.deleted = true;
                                break;
                            case "update":
                                if (obj.trakt_id != null)
                                {
                                    var updateDic = db.Traktors.Where(o => o.trakt_id == obj.trakt_id).FirstOrDefault();
                                    updateDic.name = obj.name;
                                    updateDic.model = obj.model;
                                    updateDic.reg_num = obj.reg_num;
                                    updateDic.code = obj.code;
                                    updateDic.int_id = obj.int_id;
                                    updateDic.im_code = obj.im_code;
                                }
                                break;
                            case "insert":
                                var newDic = new Traktors
                                {
                                    name = obj.name,
                                    model = obj.model,
                                    reg_num = obj.reg_num,
                                    code = obj.code,
                                    int_id = obj.int_id,
                                    deleted = false,
                                    im_code = obj.im_code
                                };
                                db.Traktors.Add(newDic);
                                break;
                        }
                    }
                    db.SaveChanges();
                    trans.Commit();
                }
                catch (Exception)
                {
                    trans.Rollback();
                    throw;
                }
            }
            return "success";
        }
    }

    public class ModelTraktors1cDict
    {
        public int? trakt_id { get; set; }
        public string name { get; set; }
        public string model { get; set; }
        public string reg_num { get; set; }
        [Required(ErrorMessage = "Не заполнено обязательно поле Трекер. Если трек-номер не был присвоен, то присвойте номер формата 99xxxx, сверившись с максимальным значением трек-номера в справочнике")]
        public string code { get; set; }
        public string int_id { get; set; }
        public string im_code { get; set; }
    }

    public class ModelUpdateTraktors1с : UpdateDictModel
    {
        public ModelTraktors1cDict Document { get; set; }
    }
}
