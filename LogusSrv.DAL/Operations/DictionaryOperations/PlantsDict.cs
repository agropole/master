﻿using System;
using System.Data.Entity;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LogusSrv.CORE.Exceptions;
using LogusSrv.DAL.Entities.DTO.ActsSzr;
using LogusSrv.DAL.Entities.DTO;
using LogusSrv.DAL.Entities.Req;
using LogusSrv.DAL.DBModel;
using LogusSrv.DAL.Utils;
using LogusSrv.DAL.Enums;
using LogusSrv.DAL.Entities.Res;


namespace LogusSrv.DAL.Operations.DictionaryOperations
{
    public class PlantsDict
    {
        public List<ModelPlants> GetPlantsDict(LogusDBEntities db)
        {
            var res = (from t in db.Plants
                       where t.deleted == false
                       select new ModelPlants
                       {
                           plant_id = t.plant_id,
                           name = t.name,
                           short_name = t.short_name,
                           description = t.description,
                           code = t.code,
                           tpplant_tpplant_id =  new DictionaryItemsDTO
                           {
                               Id = (int)t.tpplant_tpplant_id,
                               Name = t.Type_plants.name
                           }
                       }).ToList();
            return res;
        }

        public string UpdatePlantsDict(List<ModelUpdatePlants> updateObject, LogusDBEntities db)
        {
            using (var trans = db.Database.BeginTransaction())
            {
                try
                {
                    var newId = (db.Plants.Max(t => (int?)t.plant_id) ?? 0) + 1;
                    for (var i = 0; i < updateObject.Count; i++)
                    {
                        var obj = updateObject[i].Document;
                        switch (updateObject[i].command)
                        {
                            case "del":
                                var delDic = db.Plants.Where(o => o.plant_id == obj.plant_id).FirstOrDefault();
                                delDic.deleted = true;
                                break;
                            case "update":
                                var updateDic = db.Plants.Where(o => o.plant_id == obj.plant_id).FirstOrDefault();
                                updateDic.name = obj.name;
                                updateDic.code = obj.code;
                                updateDic.short_name = obj.short_name;
                                updateDic.description = obj.description;
                                updateDic.tpplant_tpplant_id = (int)obj.tpplant_tpplant_id.Id;
                                break;
                            case "insert":
                                var newDic = new Plants
                                {
                                    plant_id = newId,
                                    name = obj.name,
                                    code = obj.code,
                                    short_name = obj.short_name,
                                    description = obj.description,
                                    deleted = false,
                                    tpplant_tpplant_id = (int)obj.tpplant_tpplant_id.Id
                                };
                                newId++;
                                db.Plants.Add(newDic);
                                break;
                        }
                    }
                    db.SaveChanges();
                    trans.Commit();
                }
                catch (Exception)
                {
                    trans.Rollback();
                    throw;
                }
            }
            return "success";
        }

        public DetailPlantsModel GetDetailPlants(LogusDBEntities db)
        {
            var res = new DetailPlantsModel
            {
                tpplant_tpplant_id = (from t in db.Type_plants
                               select new DictionaryItemsDTO
                               {
                                   Id = t.tpplant_id,
                                   Name = t.name
                               }).ToList()
            };
            return res;
        }
    }

    public class ModelPlants
    {
        public int? plant_id { get; set; }
        public string name { get; set; }
        public string code { get; set; }
        public string short_name { get; set; }
        public DictionaryItemsDTO tpplant_tpplant_id { get; set; }
        public string description { get; set; }
    }

    public class DetailPlantsModel
    {
        public List<DictionaryItemsDTO> tpplant_tpplant_id { get; set; }
    }

    public class ModelUpdatePlants : UpdateDictModel
    {
        public ModelPlants Document { get; set; }
    }
}
