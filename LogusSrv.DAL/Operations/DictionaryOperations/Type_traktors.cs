﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LogusSrv.DAL.DBModel;
using LogusSrv.DAL.Entities.DTO;
using LogusSrv.DAL.Entities.Res;
using System.Data.Entity.Validation;
using System.Web;

namespace LogusSrv.DAL.Operations.DictionaryOperations
{
    public class Type_traktorsDict
    {
        public List<Type_traktorsDictModel> GetType_traktorsDict(LogusDBEntities db)
        {
            var res = db.Group_traktors.Where(t => t.deleted != true).Select(t => new Type_traktorsDictModel
                       {
                           grtrak_id = t.grtrak_id,
                           name = t.name,
                           deleted = t.deleted,
                           uniq_code = t.uniq_code,
                       }).ToList();
            return res;
        }

        public string UpdateType_traktorsDict(List<ModelUpdateType_traktors> updateObject, LogusDBEntities db)
        {
            using (var trans = db.Database.BeginTransaction())
            {
                try
                {
                    var newId = (db.Group_traktors.Max(t => (int?)t.grtrak_id) ?? 0) + 1;
                    for (var i = 0; i < updateObject.Count; i++)
                    {
                        var obj = updateObject[i].Document;
                        switch (updateObject[i].command)
                        {
                            case "del":
                                var delDic = db.Group_traktors.Where(o => o.grtrak_id == obj.grtrak_id).FirstOrDefault();
                                delDic.deleted = true;
                                break;
                            case "update":
                                var updateDic = db.Group_traktors.Where(o => o.grtrak_id == obj.grtrak_id).FirstOrDefault();
                                updateDic.name = obj.name;
                                updateDic.uniq_code = obj.uniq_code;
                                break;
                            case "insert":
                                var newDic = new Group_traktors
                                {
                                    grtrak_id = newId,
                                    name = obj.name,
                                    deleted = false,
                                    uniq_code = obj.uniq_code,
                                };
                                newId++;
                                db.Group_traktors.Add(newDic);
                                break;
                        }
                    }
       
                    db.SaveChanges();
                    trans.Commit();
                }
                catch (Exception)
                {
                    trans.Rollback();
                    throw;
                }
            }
            return "success";
        }
    }

    public class Type_traktorsDictModel
    {
        public int? grtrak_id { get; set;}
        public string name { get; set; }
        public string uniq_code { get; set; }
        public string code { get; set; }
        public string description { get; set; }
        public bool? deleted { get; set;}
    }
    public class ModelUpdateType_traktors : UpdateDictModel
    {
        public Type_traktorsDictModel Document { get; set; }
    }
}
