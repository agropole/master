﻿using System;
using System.Data.Entity;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LogusSrv.CORE.Exceptions;
using LogusSrv.DAL.Entities.DTO.ActsSzr;
using LogusSrv.DAL.Entities.DTO;
using LogusSrv.DAL.Entities.Req;
using LogusSrv.DAL.DBModel;
using LogusSrv.DAL.Utils;
using LogusSrv.DAL.Enums;
using LogusSrv.DAL.Entities.Res;


namespace LogusSrv.DAL.Operations.DictionaryOperations
{
    public class Type_salary_docDict
    {
        public List<ModelType_salary_doc> GetType_salary_doc(LogusDBEntities db)
        {
            var res = (from t in db.Type_salary_doc
                       where t.deleted == false
                       select new ModelType_salary_doc { 
                           tpsal_id = t.tpsal_id,
                           name = t.name,
                           short_name = t.short_name
                       }).ToList();
            return res;
        }

        public string Updateype_salary_doc(List<UpdateType_salary_doc> updateObject, LogusDBEntities db)
        {
            using (var trans = db.Database.BeginTransaction())
            {
                try
                {
                    var newId = (db.Type_salary_doc.Max(t => (int?)t.tpsal_id) ?? 0) + 1;
                    for (var i=0; i<updateObject.Count; i++) {
                        var obj = updateObject[i].Document;
                        switch (updateObject[i].command)
                        {
                            case "del":
                                var delDic = db.Type_salary_doc.Where(o => o.tpsal_id == obj.tpsal_id).FirstOrDefault();
                                delDic.deleted = true;
                                break;
                            case "update":
                                var updateDic = db.Type_salary_doc.Where(o => o.tpsal_id == obj.tpsal_id).FirstOrDefault();
                                updateDic.short_name = obj.short_name;
                                updateDic.name = obj.name;
                                break;
                            case "insert":
                                var newDic = new Type_salary_doc
                                {
                                    tpsal_id = newId,
                                    name = obj.name,
                                    deleted = false,
                                    short_name = obj.short_name
                                };
                                db.Type_salary_doc.Add(newDic);
                                newId++;
                                break;
                        }
                    }
                    db.SaveChanges();
                    trans.Commit();
                }
                catch (Exception)
                {
                    trans.Rollback();
                    throw;
                }
            }
            return "success";
        }


    }


    public class ModelType_salary_doc
    {
        public int? tpsal_id { get; set; }
        public string name { get; set; }
        public string short_name { get; set; }
    }

    public class UpdateType_salary_doc : UpdateDictModel
    {
        public ModelType_salary_doc Document { get; set; }
    }
}
