﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LogusSrv.DAL.DBModel;
using LogusSrv.DAL.Entities.DTO;
using LogusSrv.DAL.Entities.Res;

namespace LogusSrv.DAL.Operations.DictionaryOperations
{
    public class OrganizationsDict
    {
        public List<OrganizationsDictModel> GetOrganizations(LogusDBEntities db)
        {
            var res = (from t in db.Organizations
                       where t.deleted != true
                       select new OrganizationsDictModel   
                       {
                          org_id  = t.org_id,
                          name = t.name,
                          description = t.description,
                          address = t.address,
                          tporg_tporg_id = new DictionaryItemsDTO { 
                               Id = t.tporg_tporg_id,
                               Name = t.tporg_tporg_id == 1 ? "Отделение" : "Организация"
                           }                                
                       }).ToList();
            return res;
        }

        public string UpdateOrganizationsDict(List<ModelUpdateOrganizations> updateObject, LogusDBEntities db)
        {
            using (var trans = db.Database.BeginTransaction())
            {
                try
                {
                    var newId = (db.Organizations.Max(t => (int?)t.org_id) ?? 0) + 1;
                    for (var i = 0; i < updateObject.Count; i++)
                    {
                        var obj = updateObject[i].Document;
                        switch (updateObject[i].command)
                        {
                            case "del":
                                var delDic = db.Organizations.Where(o => o.org_id == obj.org_id).FirstOrDefault();
                                delDic.deleted = true;
                                break;
                            case "update":
                                var updateDic = db.Organizations.Where(o => o.org_id == obj.org_id).FirstOrDefault();
                                updateDic.name = obj.name;
                                updateDic.description = obj.description;
                                updateDic.address = obj.address;
                                updateDic.tporg_tporg_id = (int)obj.tporg_tporg_id.Id;
                                break;
                            case "insert":
                                var newDic = new Organizations
                                {
                                    org_id = newId,
                                    name = obj.name,
                                    description = obj.description,
                                    tporg_tporg_id = (int)obj.tporg_tporg_id.Id,
                                    deleted = false,
                                    address = obj.address,
                                    org_org_id = 1,
                                };
                                newId++;
                                db.Organizations.Add(newDic);
                                break;
                        }
                    }
                    db.SaveChanges();
                    trans.Commit();
                }
                catch (Exception)
                {
                    trans.Rollback();
                    throw;
                }
            }
            return "success";
        }

        public DetailOrganizationsModel GetDetailOrganizations(LogusDBEntities db)
        {
            var res = new DetailOrganizationsModel
            {
                tporg_tporg_id = new List<DictionaryItemsDTO>() { new DictionaryItemsDTO { Id = 1, Name = "Отделение"} ,
                 new DictionaryItemsDTO { Id = 2, Name = "Организация"}},
            };
            return res;
        }
    }

    public class OrganizationsDictModel
    {
        public int? org_id { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public string address { get; set; }
        public DictionaryItemsDTO tporg_tporg_id { get; set; }
 
    }

    public class DetailOrganizationsModel
    {
        public List<DictionaryItemsDTO> tporg_tporg_id { get; set; }
    }

    public class ModelUpdateOrganizations : UpdateDictModel
    {
        public OrganizationsDictModel Document { get; set; }
    }
}
