﻿using System;
using System.Data.Entity;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LogusSrv.CORE.Exceptions;
using LogusSrv.DAL.Entities.DTO.ActsSzr;
using LogusSrv.DAL.Entities.DTO;
using LogusSrv.DAL.Entities.Req;
using LogusSrv.DAL.DBModel;
using LogusSrv.DAL.Utils;
using LogusSrv.DAL.Enums;
using LogusSrv.DAL.Entities.Res;


namespace LogusSrv.DAL.Operations.DictionaryOperations
{
    public class ContractorDict
    {
        public List<ModelContractor> GetContractors(LogusDBEntities db)
        {
            var res = (from t in db.Contractor
                       where t.deleted == false
                       select new ModelContractor
                       {
                           id = t.id,
                           name = t.name,
                           code = t.code,
                           inn = t.inn,
                           kpp = t.kpp,
                       }).ToList();
            return res;
        }

        public string UpdateContractor(List<UpdateContractor> updateObject, LogusDBEntities db)
        {
            using (var trans = db.Database.BeginTransaction())
            {
                try
                {
                    for (var i = 0; i < updateObject.Count; i++)
                    {
                        var obj = updateObject[i].Document;
                        switch (updateObject[i].command)
                        {
                            case "del":
                                var delDic = db.Contractor.Where(o => o.id == obj.id).FirstOrDefault();
                                delDic.deleted = true;
                                break;
                            case "update":
                                var updateDic = db.Contractor.Where(o => o.id == obj.id).FirstOrDefault();
                                updateDic.code = obj.code;
                                updateDic.name = obj.name;
                                updateDic.inn = obj.inn;
                                updateDic.kpp = obj.kpp;
                                break;
                            case "insert":
                                var newDic = new Contractor
                                {
                                    name = obj.name,
                                    code = obj.code,
                                    inn = obj.inn,
                                    kpp = obj.kpp,
                                    deleted = false,
                                };
                                db.Contractor.Add(newDic);
                                break;
                        }
                    }
                    db.SaveChanges();
                    trans.Commit();
                }
                catch (Exception)
                {
                    trans.Rollback();
                    throw;
                }
            }
            return "success";
        }
    }

    public class ModelContractor
    {
        public int? id { get; set; }
        public string name { get; set; }
        public string inn { get; set; }
        public string kpp { get; set; }
        public string code { get; set; }
    }

    public class UpdateContractor : UpdateDictModel
    {
        public ModelContractor Document { get; set; }
    }
}
