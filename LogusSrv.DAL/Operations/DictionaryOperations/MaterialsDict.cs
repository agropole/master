﻿using System;
using System.Data.Entity;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LogusSrv.CORE.Exceptions;
using LogusSrv.DAL.Entities.DTO.ActsSzr;
using LogusSrv.DAL.Entities.DTO;
using LogusSrv.DAL.Entities.Req;
using LogusSrv.DAL.DBModel;
using LogusSrv.DAL.Utils;
using LogusSrv.DAL.Enums;
using LogusSrv.DAL.Entities.Res;


namespace LogusSrv.DAL.Operations.DictionaryOperations
{
    public class MaterialsDict
    {
        public List<ModelMaterials> GetMaterialsDict(LogusDBEntities db, int? type)
        {
            var res = (from t in db.Materials
                       where type.HasValue ? t.mat_type_id == type : true
                       where t.deleted == false
                       select new ModelMaterials
                       {
                           mat_id = t.mat_id,
                           name = t.name,
                           code = t.code,
                           description = t.description,
                           balance = t.balance,
                           current_price = t.current_price,
                           mat_type_id = t.mat_type_id.HasValue ? new DictionaryItemsDTO
                           {
                               Id = (int)t.mat_type_id,
                               Name= t.Type_materials.name
                           } : null
                       }).ToList();
            return res;
        }

        public string UpdateMaterialsDict(List<ModelUpdateMaterials> updateObject, LogusDBEntities db)
        {
            using (var trans = db.Database.BeginTransaction())
            {
                try
                {
                    var newId = (db.Materials.Max(t => (int?)t.mat_id) ?? 0) + 1;
                    for (var i = 0; i < updateObject.Count; i++)
                    {
                        var obj = updateObject[i].Document;
                        switch (updateObject[i].command)
                        {
                            case "del":
                                var delDic = db.Materials.Where(o => o.mat_id == obj.mat_id).FirstOrDefault();
                                delDic.deleted = true;
                                break;
                            case "update":
                                var updateDic = db.Materials.Where(o => o.mat_id == obj.mat_id).FirstOrDefault();
                                updateDic.name = obj.name;
                                updateDic.code = obj.code;
                                updateDic.description = obj.description;
                                updateDic.mat_type_id = obj.mat_type_id.Id;
                                updateDic.current_price = obj.current_price;
                                updateDic.balance = obj.balance;
                                break;
                            case "insert":
                                var newDic = new Materials
                                {
                                    mat_id = newId,
                                    name = obj.name,
                                    code = obj.code,
                                    description = obj.description,
                                    mat_type_id = obj.mat_type_id.Id,
                                    current_price = obj.current_price,
                                    deleted = false,
                                    balance = obj.balance
                                };
                                newId++;
                                db.Materials.Add(newDic);
                                break;
                        }
                    }
                    db.SaveChanges();
                    trans.Commit();
                }
                catch (Exception)
                {
                    trans.Rollback();
                    throw;
                }
            }
            return "success";
        }

        public DetailMaterials GetDetailMaterials(LogusDBEntities db)
        {
            var res = new DetailMaterials
            {
                mat_type_id = (from t in db.Type_materials
                                  select new DictionaryItemsDTO
                                  {
                                      Id = t.type_id,
                                      Name = t.name
                                  }).ToList()
            };
            return res;
        }
    }

    public class ModelMaterials
    {
        public int? mat_id { get; set; }
        public string name { get; set; }
        public DictionaryItemsDTO mat_type_id { get; set; }
        public string code { get; set; }
        public string description { get; set; }
        public decimal? balance { get; set; }
        public decimal? current_price { get; set; }
    }

    public class DetailMaterials
    {
        public List<DictionaryItemsDTO> mat_type_id { get; set; }
    }

    public class ModelUpdateMaterials : UpdateDictModel
    {
        public ModelMaterials Document { get; set; }
    }
}
