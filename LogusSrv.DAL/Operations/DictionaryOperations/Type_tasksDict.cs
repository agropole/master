﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LogusSrv.DAL.DBModel;
using LogusSrv.DAL.Entities.Res;
using LogusSrv.DAL.Entities.DTO;

namespace LogusSrv.DAL.Operations.DictionaryOperations
{
    public class Type_tasksDict
    {
        public List<ModelType_tasksDict> GetType_tasksDict(LogusDBEntities db)
        {
            var res = db.Type_tasks.Select(t => new ModelType_tasksDict
                       {
                           tptas_id = t.tptas_id,
                           name = t.name,
                           description = t.description,
                           code = t.in_code,
                         auxiliary_rate = new DictionaryItemsDTO
                           {
                               Id = (int?)t.auxiliary_rate,
                               Name = db.Group_tasks.Where(p => p.grtask_id == t.auxiliary_rate).Select(p=>p.name).FirstOrDefault(),
                           },
                           type_fuel_id = new DictionaryItemsDTO
                           {
                               Id = (int?)t.type_fuel_id,
                               Name = db.Stages_cultivation.Where(p => p.stages_id == t.type_fuel_id).Select(p => p.name).FirstOrDefault(),
                           },
                           deleted = t.deleted,
                       }).Where(p => p.deleted != true).OrderBy(t => t.name).ToList();
            return res;
        }

        public string UpdateType_tasksDict(List<ModelUpdateType_tasks> updateObject, LogusDBEntities db)
        {
            using (var trans = db.Database.BeginTransaction())
            {
                try
                {
                    var newId = (db.Type_tasks.Max(t => (int?)t.tptas_id) ?? 0) + 1;
                    for (var i = 0; i < updateObject.Count; i++)
                    {
                        var obj = updateObject[i].Document;
                        switch (updateObject[i].command)
                        {
                            case "del":
                                var delDic = db.Type_tasks.Where(o => o.tptas_id == obj.tptas_id).FirstOrDefault();
                                delDic.deleted = true;
                                break;
                            case "update":
                                var updateDic = db.Type_tasks.Where(o => o.tptas_id == obj.tptas_id).FirstOrDefault();
                                updateDic.name = obj.name;
                                updateDic.in_code = obj.code;
                                updateDic.description = obj.description;
                                updateDic.auxiliary_rate = obj.auxiliary_rate.Id;
                                updateDic.type_fuel_id = obj.type_fuel_id.Id;
                                break;
                            case "insert":
                                if (obj.type_fuel_id == null)
                                {
                                    obj.type_fuel_id = new DictionaryItemsDTO { Id = null, Name = null }; 
                                }
                                var newDic = new Type_tasks
                                {
                                    code = newId.ToString(),
                                    name = obj.name,
                                    in_code = obj.code,
                                    deleted = false,
                                    description = obj.description,
                                    auxiliary_rate = obj.auxiliary_rate.Id,
                                    type_fuel_id = obj.type_fuel_id.Id,
                                };
                                newId++;
                                db.Type_tasks.Add(newDic);
                                break;
                        }
                    }
                    db.SaveChanges();
                    trans.Commit();
                }
                catch (Exception)
                {
                    trans.Rollback();
                    throw;
                }
            }
            return "success";
        }

        //
        public DetailGroup_tasksModel GetDetailGroup_tasks(LogusDBEntities db)
        {
            var res = new DetailGroup_tasksModel
            {
                auxiliary_rate = (from t in db.Group_tasks
                                    where t.deleted == false
                                    select new DictionaryItemsDTO
                                    {
                                        Id = t.grtask_id,
                                        Name = t.name
                                    }).ToList(),


                type_fuel_id = (from t in db.Stages_cultivation
                                where t.deleted == false
                                select new DictionaryItemsDTO
                                {
                                    Id = t.stages_id,
                                    Name = t.name
                                }).ToList(),
            };
            return res;
        }
        //




    }
    public class DetailGroup_tasksModel
    {
        public List<DictionaryItemsDTO> auxiliary_rate { get; set; }
        public List<DictionaryItemsDTO> type_fuel_id { get; set; }
    }
    public class ModelType_tasksDict
    {
        public int? tptas_id { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public string code { get; set; }
        public bool? deleted { get; set; }
        public DictionaryItemsDTO auxiliary_rate { get; set; }
        public DictionaryItemsDTO type_fuel_id { get; set; }
    }



    public class ModelUpdateType_tasks : UpdateDictModel
    {
        public ModelType_tasksDict Document { get; set; }
    }
}
