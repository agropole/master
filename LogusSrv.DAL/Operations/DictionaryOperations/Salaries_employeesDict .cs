﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LogusSrv.DAL.DBModel;
using LogusSrv.DAL.Entities.DTO;
using LogusSrv.DAL.Entities.Res;

namespace LogusSrv.DAL.Operations.DictionaryOperations
{
    public class Salaries_employeesDict
    {
        public List<ModelSalEmployeesDict> GetSalaries_employeesDict(LogusDBEntities db)
        {
            var res = (from t in db.Salaries_employees
                       where t.deleted == false
                       select new ModelSalEmployeesDict
                       {
                         salemp_id =t.salemp_id,
                         second_name = t.second_name,
                         first_name = t.first_name,
                         middle_name = t.middle_name,
                         short_name = t.short_name,
                         in_code = t.in_code
                       }).ToList();
            return res;
        }

        public string UpdateSalDict(List<ModelUpdateSalEmployeesDict> updateObject, LogusDBEntities db)
        {
            using (var trans = db.Database.BeginTransaction())
            {
                try
                {
                    for (var i = 0; i < updateObject.Count; i++)
                    {
                        var obj = updateObject[i].Document;
                        switch (updateObject[i].command)
                        {
                            case "del":
                                var delDic = db.Salaries_employees.Where(o => o.salemp_id == obj.salemp_id).FirstOrDefault();
                                delDic.deleted = true;
                                break;
                            case "update":
                                var updateDic = db.Salaries_employees.Where(o => o.salemp_id == obj.salemp_id).FirstOrDefault();
                                updateDic.second_name = obj.second_name;
                                updateDic.first_name = obj.first_name;
                                updateDic.middle_name = obj.middle_name;
                                updateDic.short_name = obj.short_name;
                                updateDic.in_code = obj.in_code;
                                break;
                            case "insert":
                                int Id = db.Salaries_employees.Max(u => u.salemp_id)+1;
                                var newDic = new Salaries_employees
                                {
                                    salemp_id =Id,
                                    second_name = obj.second_name,
                                    first_name = obj.first_name,
                                    middle_name = obj.middle_name,
                                    short_name = obj.short_name,
                                    in_code = obj.in_code,
                                    deleted = false,
                                };
                                db.Salaries_employees.Add(newDic);
                                break;
                        }
                    }
                    db.SaveChanges();
                    trans.Commit();
                }
                catch (Exception)
                {
                    trans.Rollback();
                    throw;
                }
            }
            return "success";
        }
    }

    public class ModelSalEmployeesDict
    {
        public int? salemp_id{ get; set; }
        public string first_name { get; set; }
        [Required(ErrorMessage = "Не заполнено поле Фамилия")]
        public string second_name { get; set; }
        public string middle_name { get; set; }
        [Required(ErrorMessage = "Не заполнено поле ФИО")]
        public string short_name { get; set; }
        public string in_code { get; set; }
    }

    public class ModelUpdateSalEmployeesDict : UpdateDictModel
    {
        public ModelSalEmployeesDict Document { get; set; }
    }
}
