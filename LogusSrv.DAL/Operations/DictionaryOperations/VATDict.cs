﻿using System;
using System.Data.Entity;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LogusSrv.CORE.Exceptions;
using LogusSrv.DAL.Entities.DTO.ActsSzr;
using LogusSrv.DAL.Entities.DTO;
using LogusSrv.DAL.Entities.Req;
using LogusSrv.DAL.DBModel;
using LogusSrv.DAL.Utils;
using LogusSrv.DAL.Enums;
using LogusSrv.DAL.Entities.Res;


namespace LogusSrv.DAL.Operations.DictionaryOperations
{
    public class VATDict
    {
        public List<ModelVat> GetVAT(LogusDBEntities db)
        {
            var res = (from t in db.VAT
                       where t.deleted == false
                       select new ModelVat
                       {
                           id = t.id,
                           vat_value = t.vat_value
                       }).ToList();
            return res;
        }

        public string UpdateVAT(List<UpdateVat> updateObject, LogusDBEntities db)
        {
            using (var trans = db.Database.BeginTransaction())
            {
                try
                {
                    for (var i = 0; i < updateObject.Count; i++)
                    {
                        var obj = updateObject[i].Document;
                        switch (updateObject[i].command)
                        {
                            case "del":
                                var delDic = db.VAT.Where(o => o.id == obj.id).FirstOrDefault();
                                delDic.deleted = true;
                                break;
                            case "update":
                                var updateDic = db.VAT.Where(o => o.id == obj.id).FirstOrDefault();
                                updateDic.vat_value = obj.vat_value;
                                break;
                            case "insert":
                                var newDic = new VAT
                                {
                                    vat_value = obj.vat_value,
                                    deleted = false
                                };
                                db.VAT.Add(newDic);
                                break;
                        }
                    }
                    db.SaveChanges();
                    trans.Commit();
                }
                catch (Exception)
                {
                    trans.Rollback();
                    throw;
                }
            }
            return "success";
        }


    }

    public class ModelVat
    {
        public int? id { get; set; }
        public decimal? vat_value { get; set; }
    }

    public class UpdateVat : UpdateDictModel
    {
        public ModelVat Document { get; set; }
    }
}
