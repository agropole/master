﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LogusSrv.DAL.DBModel;
using LogusSrv.DAL.Entities.Res;

namespace LogusSrv.DAL.Operations.DictionaryOperations
{
    class Type_materialsDict
    {
        public List<ModelTypeMaterials> GetType_materials(LogusDBEntities db)
        {
            var res = (from t in db.Type_materials
                       where t.deleted == false
                       select new ModelTypeMaterials
                       {
                           type_id = t.type_id,
                           name = t.name,
                           description = t.description,
                           code = t.code,
                       }).ToList();
            return res;
        }


        internal string UpdateType_materials(List<UpdateTypeMaterials> updateObject, LogusDBEntities db)
        {
            using (var trans = db.Database.BeginTransaction())
            {
                try
                {
                    var newId = (db.Type_materials.Max(t => (int?)t.type_id) ?? 0) + 1;
                    for (var i = 0; i < updateObject.Count; i++)
                    {
                        var obj = updateObject[i].Document;
                        switch (updateObject[i].command)
                        {
                            case "del":
                                var delDic = db.Type_materials.Where(o => o.type_id == obj.type_id).FirstOrDefault();
                                delDic.deleted = true;
                                break;
                            case "update":
                                var updateDic = db.Type_materials.Where(o => o.type_id == obj.type_id).FirstOrDefault();
                                updateDic.name = obj.name;
                                updateDic.description = obj.description;
                                updateDic.code = obj.code;
                                break;
                            case "insert":
                                var newDic = new Type_materials
                                {
                                    type_id = newId,
                                    name = obj.name,
                                    description = obj.description,
                                    deleted = false,
                                    code = obj.code
                                };
                                newId++;
                                db.Type_materials.Add(newDic);
                                break;
                        }
                    }
                    db.SaveChanges();
                    trans.Commit();
                }
                catch (Exception)
                {
                    trans.Rollback();
                    throw;
                }
            }
            return "success";
        }
    }

    public class ModelTypeMaterials
    {
        public int? type_id { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public string code { get; set; }
    }

    public class UpdateTypeMaterials : UpdateDictModel
    {
        public ModelTypeMaterials Document { get; set; }
    }
}
