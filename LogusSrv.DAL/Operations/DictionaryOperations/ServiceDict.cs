﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LogusSrv.DAL.DBModel;
using LogusSrv.DAL.Entities.DTO;
using LogusSrv.DAL.Entities.Res;

namespace LogusSrv.DAL.Operations.DictionaryOperations
{
    public class ServiceDict
    {
        public List<ModelServiceDict> GetServiceDict(LogusDBEntities db)
        {
            var res = (from t in db.Service
                       where t.deleted == false
                       select new ModelServiceDict
                       {
                           id = t.id,
                           name = t.name,
                       }).ToList();
            return res;
        }

        public string UpdateFieldsDict(List<ModelUpdateServiceDict> updateObject, LogusDBEntities db)
        {
            using (var trans = db.Database.BeginTransaction())
            {
                try
                {
                    var newId = (db.Service.Max(t => (int?)t.id) ?? 0) + 1;
                    for (var i = 0; i < updateObject.Count; i++)
                    {
                        var obj = updateObject[i].Document;
                        switch (updateObject[i].command)
                        {
                            case "del":
                                var delDic = db.Service.Where(o => o.id == obj.id).FirstOrDefault();
                                delDic.deleted = true;
                                break;
                            case "update":
                                var updateDic = db.Service.Where(o => o.id == obj.id).FirstOrDefault();
                                updateDic.name = obj.name;
                                break;
                            case "insert":
                                var newDic = new Service
                                {
                                    id = newId,
                                    name = obj.name,
                                    deleted = false,
                                };
                                newId++;
                                db.Service.Add(newDic);
                                break;
                        }
                    }
                    db.SaveChanges();
                    trans.Commit();
                }
                catch (Exception)
                {
                    trans.Rollback();
                    throw;
                }
            }
            return "success";
        }

        
    }

    public class ModelServiceDict
    {
        public int? id { get; set; }
        [Required(ErrorMessage = "Не заполнено поле Название")]
        public string name { get; set; }
    }

    public class ModelUpdateServiceDict : UpdateDictModel
    {
        public ModelServiceDict Document { get; set; }
    }
}

    

