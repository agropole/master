﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LogusSrv.DAL.DBModel;
using LogusSrv.DAL.Entities.Res;
using System.Data.Entity.Validation;

namespace LogusSrv.DAL.Operations.DictionaryOperations
{
    public class Type_equipmentsDict
    {
        public List<ModelType_equipmentsDict> GetEquiDict(LogusDBEntities db)
        {
            var res = (from t in db.Type_equipments
                       where t.deleted == false
                       select new ModelType_equipmentsDict
                       {
                           tpequi_id = t.tpequi_id,
                           name = t.name,
                           description = t.description,
                       }).ToList();
            return res;
        }

        public string UpdateEquiDict(List<ModelUpdateType_equipmentsDict> updateObject, LogusDBEntities db)
        {
            using (var trans = db.Database.BeginTransaction())
            {
                try
                {
                    var newId = (db.Type_equipments.Max(t => (int?)t.tpequi_id) ?? 0) + 1;
                    for (var i = 0; i < updateObject.Count; i++)
                    {
                        var obj = updateObject[i].Document;
                        switch (updateObject[i].command)
                        {
                            case "del":
                                var delDic = db.Type_equipments.Where(o => o.tpequi_id == obj.tpequi_id).FirstOrDefault();
                                delDic.deleted = true;
                                break;
                            case "update":
                                var updateDic = db.Type_equipments.Where(o => o.tpequi_id == obj.tpequi_id).FirstOrDefault();
                                updateDic.name = obj.name;
                                updateDic.description = obj.description;
                                break;
                            case "insert":
                                var newDic = new Type_equipments
                                {
                                    tpequi_id = newId,
                                    name = obj.name,
                                    deleted = false,
                                    description = obj.description,
                                    code ="",
                                };
                                newId++;
                                db.Type_equipments.Add(newDic);
                                break;
                        }
                    }
                    //
                     try
                {
                    db.SaveChanges();
                          }
                catch (DbEntityValidationException ex)
                {
                    foreach (DbEntityValidationResult validationError in ex.EntityValidationErrors)
                    {

                        foreach (DbValidationError err in validationError.ValidationErrors)
                        {
                        }
                    }
                }




                    trans.Commit();
                }
                catch (Exception)
                {
                    trans.Rollback();
                    throw;
                }
            }
            return "success";
        }
    }

    public class ModelType_equipmentsDict
    {
        public int? tpequi_id {get; set;}
        public string name {get; set;}
        public string description {get; set;}
        public string code { get; set; }
    }

    public class ModelUpdateType_equipmentsDict : UpdateDictModel
    {
        public ModelType_equipmentsDict Document { get; set; }
    }
}
