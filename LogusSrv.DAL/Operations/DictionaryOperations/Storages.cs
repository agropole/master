﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LogusSrv.DAL.DBModel;
using LogusSrv.DAL.Entities.DTO;
using LogusSrv.DAL.Entities.Res;
using System.Data.Entity.Validation;
using System.Web;

namespace LogusSrv.DAL.Operations.DictionaryOperations
{
    public class StoragesDict
    {
        public List<StoragesDictModel> GetStoragesDict(LogusDBEntities db)
        {
            var res = db.Storages.Where(t => t.deleted != true).Select(t => new StoragesDictModel
            {
                store_id = t.store_id,
                name = t.name,
                deleted = t.deleted,
            }).ToList();
            return res;
        }

        public string UpdateStoragesDict(List<ModelUpdateStorages> updateObject, LogusDBEntities db)
        {
            using (var trans = db.Database.BeginTransaction())
            {
                try
                {
                    var newId = (db.Storages.Max(t => (int?)t.store_id) ?? 0) + 1;
                    for (var i = 0; i < updateObject.Count; i++)
                    {
                        var obj = updateObject[i].Document;
                        switch (updateObject[i].command)
                        {
                            case "del":
                                var delDic = db.Storages.Where(o => o.store_id == obj.store_id).FirstOrDefault();
                                delDic.deleted = true;
                                break;
                            case "update":
                                var updateDic = db.Storages.Where(o => o.store_id == obj.store_id).FirstOrDefault();
                                updateDic.name = obj.name;
                                break;
                            case "insert":
                                var newDic = new Storages
                                {
                                    store_id = newId,
                                    name = obj.name,
                                    deleted = false
                                };
                                newId++;
                                db.Storages.Add(newDic);
                                break;
                        }
                    }

                    db.SaveChanges();
                    trans.Commit();
                }
                catch (Exception)
                {
                    trans.Rollback();
                    throw;
                }
            }
            return "success";
        }
    }



    public class StoragesDictModel
    {
        public int? store_id { get; set; }
        public string name { get; set; }
        public bool? deleted { get; set; }
    }
    public class ModelUpdateStorages : UpdateDictModel
    {
        public StoragesDictModel Document { get; set; }
    }
}
