﻿using System;
using System.Data.Entity;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LogusSrv.CORE.Exceptions;
using LogusSrv.DAL.Entities.DTO.ActsSzr;
using LogusSrv.DAL.Entities.DTO;
using LogusSrv.DAL.Entities.Req;
using LogusSrv.DAL.DBModel;
using LogusSrv.DAL.Utils;
using LogusSrv.DAL.Enums;
using LogusSrv.DAL.Entities.Res;


namespace LogusSrv.DAL.Operations.DictionaryOperations
{
    public class Type_fuelDict
    {
        public List<ModelTypeFuel> GetType_fuel(LogusDBEntities db)
        {
            var res = (from t in db.Type_fuel
                       where t.deleted == false
                       select new ModelTypeFuel
                       {
                           id = t.id,
                           name = t.name,
                           balance = t.balance,
                           current_price = t.current_price,
                           code = t.code,
                           tpsal_tpsal_id = t.tpsal_tpsal_id.HasValue ? new DictionaryItemsDTO {
                               Id= (int)t.tpsal_tpsal_id,
                               Name= t.Type_salary_doc.short_name
                           } : null
                       }).ToList();
            return res;
        }

        public string UpdateType_fuel(List<UpdateTypeFuel> updateObject, LogusDBEntities db)
        {
            using (var trans = db.Database.BeginTransaction())
            {
                try
                {
                    var newId = (db.Type_fuel.Max(t => (int?)t.id) ?? 0) + 1;
                    for (var i = 0; i < updateObject.Count; i++)
                    {
                        var obj = updateObject[i].Document;
                        switch (updateObject[i].command)
                        {
                            case "del":
                                var delDic = db.Type_fuel.Where(o => o.id == obj.id).FirstOrDefault();
                                delDic.deleted = true;
                                break;
                            case "update":
                                var updateDic = db.Type_fuel.Where(o => o.id == obj.id).FirstOrDefault();
                                updateDic.name = obj.name;
                                updateDic.code = obj.code;
                                updateDic.tpsal_tpsal_id = obj.tpsal_tpsal_id.Id;
                                updateDic.current_price = obj.current_price;
                                updateDic.balance = obj.balance;
                                break;
                            case "insert":
                                var newDic = new Type_fuel
                                {
                                    id = newId,
                                    name = obj.name,
                                    code = obj.code,
                                    tpsal_tpsal_id = obj.tpsal_tpsal_id.Id,
                                    deleted = false,
                                    current_price = obj.current_price,
                                    balance = obj.balance
                                };
                                newId++;
                                db.Type_fuel.Add(newDic);
                                break;
                        }
                    }
                    db.SaveChanges();
                    trans.Commit();
                }
                catch (Exception)
                {
                    trans.Rollback();
                    throw;
                }
            }
            return "success";
        }

        public DetailTypeFuel DetailType_fuel(LogusDBEntities db)
        {
            var res = new DetailTypeFuel
            {
                tpsal_tpsal_id = (from t in db.Type_salary_doc
                                  select new DictionaryItemsDTO
                                  {
                                      Id = t.tpsal_id,
                                      Name = t.short_name
                                  }).ToList()
            };
            return res;
        }
    }

    public class ModelTypeFuel
    {
        public int? id { get; set; }
        public string name { get; set; }
        public string code { get; set; }
        public float? balance { get; set; }
        public float? current_price { get; set; }
        public DictionaryItemsDTO tpsal_tpsal_id { get; set; }
    }

    public class DetailTypeFuel
    {
        public List<DictionaryItemsDTO> tpsal_tpsal_id { get; set; }
    }

    public class UpdateTypeFuel : UpdateDictModel
    {
        public ModelTypeFuel Document { get; set; }
    }
}
