﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LogusSrv.DAL.DBModel;
using LogusSrv.DAL.Entities.DTO;
using LogusSrv.DAL.Entities.Res;
using System.Data.Entity.Validation;
using System.Web;
using LogusSrv.CORE.Exceptions;

namespace LogusSrv.DAL.Operations.DictionaryOperations
{
    public class Spare_partsDict
    {
        public List<Spare_partsDictModel> GetSpare_partsDict(LogusDBEntities db)
        {
            var res = db.Spare_parts.Where(t => t.deleted != true).Select(t => new Spare_partsDictModel
            {
                spare_id = t.spare_id,
                name = t.name,
                code = t.code,
                vendor_code = t.vendor_code,
                manufacturer = t.manufacturer,
                deleted = t.deleted,
                mod_mod_id = new DictionaryItemsDTO
                            {
                                Id = t.mod_mod_id,
                                Name = db.Modules.Where(r1 => r1.mod_id == t.mod_mod_id).Select(r1 => r1.name).FirstOrDefault(),
                            },
               tptrak_tptrak_id = new DictionaryItemsDTO
                {
                    Id = t.tptrak_tptrak_id,
                    Name = db.Type_spare_parts.Where(r1 => r1.tsp_id == t.tptrak_tptrak_id).Select(r1 => r1.name).FirstOrDefault(),
                },

            }).ToList();
            return res;
        }

        public string UpdateSpare_partsDict(List<ModelUpdateSpare_parts> updateObject, LogusDBEntities db)
        {
            using (var trans = db.Database.BeginTransaction())
            {
                try
                {
                    var newId = (db.Spare_parts.Max(t => (int?)t.spare_id) ?? 0) + 1;
                    for (var i = 0; i < updateObject.Count; i++)
                    {
                        var obj = updateObject[i].Document;
                        if (obj != null)
                        {
                            switch (updateObject[i].command)
                            {
                                case "del":
                                    if (obj.spare_id != null)
                                    {
                                        var delDic = db.Spare_parts.Where(o => o.spare_id == obj.spare_id).FirstOrDefault();
                                        delDic.deleted = true;
                                    }
                                    break;
                                case "update":
                                    if (obj.spare_id != null)
                                    {
                                        var updateDic = db.Spare_parts.Where(o => o.spare_id == obj.spare_id).FirstOrDefault();
                                        updateDic.name = obj.name;
                                        updateDic.code = obj.code;
                                        updateDic.vendor_code = obj.vendor_code;
                                        updateDic.manufacturer = obj.manufacturer;
                                        updateDic.tptrak_tptrak_id = obj.tptrak_tptrak_id.Id;
                                        updateDic.mod_mod_id = obj.mod_mod_id.Id;
                                    }
                                    break;
                            }
                        }
                    }
                    //вставка запчасти
                    var ins = updateObject.Where(t => t.command.Equals("insert")).FirstOrDefault();
                    if (ins != null) {
                        if (ins.Document.code == null)
                        {
                            var month = DateTime.Now.Month.ToString().Length < 2 ? "0" + DateTime.Now.Month.ToString() : DateTime.Now.Month.ToString();
                            var day = DateTime.Now.Day.ToString().Length < 2 ? "0" + DateTime.Now.Day.ToString() : DateTime.Now.Day.ToString();
                            int number1 = db.Spare_parts.Count() + 1;
                            string number = number1.ToString();
                            var nNum = number.Length == 1 ? "000" + number : number.Length == 2 ? "00" + number : number.Length == 3 ? "0" + number : number;
                            ins.Document.code = "000." + month + day + "." + nNum;
                        }

                        if (ins.Document.tptrak_tptrak_id == null) { ins.Document.tptrak_tptrak_id = new DictionaryItemsDTO { Id = null, Name = null }; }
                        if (ins.Document.mod_mod_id == null) { ins.Document.mod_mod_id = new DictionaryItemsDTO { Id = null, Name = null }; }

                        //проверка на код
                        var checkCode = db.Spare_parts.Where(t => t.code.Equals(ins.Document.code)).FirstOrDefault();
                        if (checkCode != null) {
                            throw new BllException(14, "Запись с таким кодом уже существует.");
                        }
                        //проверка на одинакову запись
                        var checkRow = db.Spare_parts.Where(t => t.vendor_code.Equals(ins.Document.vendor_code) && t.name.Equals(ins.Document.name) &&
                        t.manufacturer.Equals(ins.Document.manufacturer) && t.tptrak_tptrak_id == ins.Document.tptrak_tptrak_id.Id &&
                        t.mod_mod_id == ins.Document.mod_mod_id.Id).FirstOrDefault();
                        if (checkRow != null)
                        {
                            throw new BllException(14, "Запись уже существует.");
                        }

                        var newDic = new Spare_parts
                        {
                            spare_id = newId,
                            name = ins.Document.name,
                            code = ins.Document.code,
                            vendor_code = ins.Document.vendor_code,
                            manufacturer = ins.Document.manufacturer,
                            mod_mod_id = ins.Document.mod_mod_id.Id,
                            tptrak_tptrak_id = ins.Document.tptrak_tptrak_id.Id,
                            deleted = false,
                        };
                        newId++;
                        db.Spare_parts.Add(newDic);
                    }
                    //
                    db.SaveChanges();
                    trans.Commit();
                }
                catch (Exception)
                {
                    trans.Rollback();
                    throw;
                }
            }
            return "success";
        }

        public DetailSpare_partsModel GetDetailSpare_parts(LogusDBEntities db)
        {
            var res = new DetailSpare_partsModel
            {
                mod_mod_id = (from t in db.Modules
                              where t.deleted == false
                              select new DictionaryItemsDTO
                              {
                                  Id = t.mod_id,
                                  Name = t.name
                              }).ToList(),
                tptrak_tptrak_id = (from t in db.Type_spare_parts
                                    where t.deleted == false
                                    select new DictionaryItemsDTO
                                    {
                                        Id = t.tsp_id,
                                        Name = t.name
                                    }).ToList(), 
            };
            return res;
        }

    }
     
    public class DetailSpare_partsModel
    {
        public List<DictionaryItemsDTO> tptrak_tptrak_id { get; set; }
        public List<DictionaryItemsDTO> mod_mod_id { get; set; }
    }


    public class Spare_partsDictModel
    {
        public int? spare_id { get; set; }
        public DictionaryItemsDTO mod_mod_id { get; set; }
        public DictionaryItemsDTO tptrak_tptrak_id { get; set; }
        public string name { get; set; }
        public string code { get; set; }
        public string vendor_code { get; set; }
        public string manufacturer { get; set; }
        public bool? deleted { get; set; }
    }
    public class ModelUpdateSpare_parts : UpdateDictModel
    {
        public Spare_partsDictModel Document { get; set; }
    }
}
