﻿using System;
using System.Data.Entity;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LogusSrv.CORE.Exceptions;
using LogusSrv.DAL.Entities.DTO.ActsSzr;
using LogusSrv.DAL.Entities.DTO;
using LogusSrv.DAL.Entities.Req;
using LogusSrv.DAL.DBModel;
using LogusSrv.DAL.Utils;
using LogusSrv.DAL.Enums;
using LogusSrv.DAL.Entities.Res;
using System.Data.Entity.Spatial;
using System.Text.RegularExpressions;
using System.IO;
using System.Globalization;


namespace LogusSrv.DAL.Operations.DictionaryOperations
{
    public class FieldsDict
    {
        public List<ModelFields> GetFieldsDict(LogusDBEntities db)
        {
            var res = (from t in db.Fields
                       where t.deleted == false
                       select new ModelFields
                       {
                           fiel_id = t.fiel_id,
                           name = t.name,
                           org_org_id = new DictionaryItemsDTO { Id = t.org_org_id, Name = t.Organizations.name},
                           type = t.type,
                           code = t.code,
                           coord = new GeoCoord
                           {
                               coord = t.coord.AsText(),
                               area = t.area,
                               center = t.center.AsText()
                           } 
                       }).ToList();
            return res;
        }

        public string UpdateFieldsDict(List<ModelUpdateFields> updateObject, LogusDBEntities db)
        {
            using (var trans = db.Database.BeginTransaction())
            {
                try
                {
                    var newId = (db.Fields.Max(t => (int?)t.fiel_id) ?? 0) + 1;
                    for (var i = 0; i < updateObject.Count; i++)
                    {
                        var obj = updateObject[i].Document;
                        switch (updateObject[i].command)
                        {
                            case "del":
                                var delDic = db.Fields.Where(o => o.fiel_id == obj.fiel_id).FirstOrDefault();
                                delDic.deleted = true;
                                break;
                            case "update":
                                var updateDic = db.Fields.Where(o => o.fiel_id == obj.fiel_id).FirstOrDefault();
                                updateDic.name = obj.name;
                                updateDic.org_org_id = obj.org_org_id.Id.Value;
                                updateDic.type = obj.type;
                                updateDic.code = obj.code;
                                if (obj.coord.coord != null)
                                {
                                    updateDic.coord = DbGeography.PolygonFromText(obj.coord.coord, 4326);
                                }
                                if (obj.coord.coord2 != null)
                                {
                                    updateDic.coord = checkPolygon(updateDic.coord, obj.coord.coord2);
                                }
                                updateDic.area = obj.coord.area;
                                if (obj.coord.center != null)
                                {
                                    updateDic.center = DbGeography.PointFromText(obj.coord.center, 4326);
                                }
                                break;
                            case "insert":
                                var newDic = new Fields
                                {
                                    fiel_id = newId,
                                    name = obj.name,
                                    org_org_id = obj.org_org_id.Id.Value,
                                    type = obj.type,
                                    code = obj.code,
                                    deleted = false,
                                    coord = DbGeography.PolygonFromText(obj.coord.coord, 4326),
                                    area = obj.coord.area,
                                    center = DbGeography.PointFromText(obj.coord.center, 4326)
                                };
                                if (obj.coord.coord2 != null)
                                {
                                    newDic.coord = checkPolygon(newDic.coord, obj.coord.coord2);
                                }
                                newId++;
                                db.Fields.Add(newDic);
                                break;
                        }
                    }
                    db.SaveChanges();
                    trans.Commit();
                }
                catch (Exception)
                {
                    trans.Rollback();
                    throw;
                }
            }
            return "success";
        }



        public static DbGeography checkPolygon(DbGeography pol, string coord2)
        {
            if (pol.Area > 10000000000)
            {
                return DbGeography.PolygonFromText(coord2, 4326);
            }
            else
            {
                return pol;
            }
        }

        public DetailFieldsModel GetDetailFields(LogusDBEntities db)
        {
            var res = new DetailFieldsModel
            {
                org_org_id = (from t in db.Organizations
                               select new DictionaryItemsDTO
                               {
                                   Id = t.org_id,
                                   Name = t.name
                               }).ToList()
            };
            return res;
        }

        public List<ModelFields> GetFieldsDictByType(int? type, LogusDBEntities db)
        {
            var res = (from t in db.Fields
                       where t.deleted == false && t.type == type
                       select new ModelFields
                       {
                           fiel_id = t.fiel_id,
                           name = t.name,
                           org_org_id = new DictionaryItemsDTO { Id = t.org_org_id, Name = t.Organizations.name },
                           type = t.type,
                           code = t.code,
                           coord = new GeoCoord
                           {
                               coord = t.coord.AsText(),
                               area = t.area,
                               center = t.center.AsText()
                           }
                       }).ToList();
            return res;
        }
        ///Получение данных по полям из файла
        public List<String> GetCoordFromTXT(int type, string templatePath, LogusDBEntities db)
        {
            StreamReader reader = new StreamReader(templatePath);
            int records = 0;
            int insert = 0;
            List<String> result = new List<String>();
            string line;
            var coord = @"\/ (P)([^\^]+)\/";
            var name = @"([^\^]+)\/\/";
            var Parselist = new List<ArmFieldsForGeo>(); //Лист с кординатами из файла

            while ((line = reader.ReadLine()) != null)
            {
                MatchCollection matches1 = Regex.Matches(line, coord);
                MatchCollection matches2 = Regex.Matches(line, name);
                foreach (Match CoordMatch in matches1)
                {
                    foreach (Match NameMatch in matches2)
                    {

                        Parselist.Add(new ArmFieldsForGeo
                        {
                            coord = CoordMatch.Value.Replace("/ ", string.Empty).Replace(" /", string.Empty),
                            name = NameMatch.Value.Replace(" //", string.Empty),
                        });
                    }
                }
            }
            reader.Close();
            using (var trans = db.Database.BeginTransaction())
            {
                try
                {
                    var newId = (db.Fields.Max(t => (int?)t.fiel_id) ?? 0) + 1;
                    for (int i = 0; i < Parselist.Count; i++)
                    {
                        var Name = Parselist[i].name;
                        var Coord = DbGeography.FromText(Parselist[i].coord, 4326);
                        double? Area = Coord.Area.Value / 10000 * 10;
                        Area = (Math.Round((double)Area) / 10) - 0.1;
                        var Center = GetCenter(Coord);
                        var updateList = db.Fields.Where(f => f.type == type && f.name == Name && f.deleted != true).FirstOrDefault();
                        if (updateList != null)
                        {
                            updateList.coord = Coord;
                            updateList.area = Area != null ? Convert.ToDecimal(Area) : (decimal)1.0;
                            updateList.center = Center;
                            records++;
                        }
                        else
                        {
                            var newDic = new Fields
                            {
                                fiel_id = newId,
                                name = Name,
                                org_org_id = 1, //Пока по умолчанию Логус
                                type = type,
                                //code = 
                                deleted = false,
                                coord = Coord,
                                area = Area != null ? Convert.ToDecimal(Area) : (decimal)0,
                                center = Center,
                            };
                            newId++;
                            insert++;
                            db.Fields.Add(newDic);
                        }

                    }
                    db.SaveChanges();
                    trans.Commit();
                    result.Add(Convert.ToString(records));
                    result.Add(Convert.ToString(insert));
                    return result;

                }
                catch (Exception ex)
                {
                    trans.Rollback();
                    result.Add(ex.Message);
                    return result;
                    throw;

                }
            }

        }

        public DbGeography GetCenter(DbGeography coord)
        {
            var maX1 = 0.0; var maX2 = 0.0; var maY1 = 0.0; var maY2 = 0.0;
            var miX1 = 2000.0; var miX2 = 2000.0; var miY1 = 2000.0; var miY2 = 2000.0;
            double Xcenter; double Ycenter;
            //aCoor is array of coordinate, format = {x1,y1,x2,y2,x3,y3,x4,y4,...}
            for (int i = 1; i < coord.PointCount; i++)
            {

                if (maX2 < coord.PointAt(i).Longitude)
                {
                    maX2 = (double)coord.PointAt(i).Longitude;
                    //this to get first max x
                    if (maX1 < maX2) { maX1 += maX2; maX2 = maX1 - maX2; maX1 -= maX2; }
                }

                //this to get second min X
                if (miX2 > coord.PointAt(i).Longitude)
                {
                    miX2 = (double)coord.PointAt(i).Longitude;
                    //this to get first min x
                    if (miX1 > miX2) { miX1 += miX2; miX2 = miX1 - miX2; miX1 -= miX2; }
                }

                //this to get second max Y
                if (maY2 < coord.PointAt(i).Latitude)
                {
                    maY2 = (double)coord.PointAt(i).Latitude;
                    //this to get first max x
                    if (maY1 < maY2) { maY1 += maY2; maY2 = maY1 - maY2; maY1 -= maY2; }
                }

                //this to get second min Y
                if (miY2 > coord.PointAt(i).Latitude)
                {
                    miY2 = (double)coord.PointAt(i).Latitude;
                    //this to get first min x
                    if (miY1 > miY2) { miY1 += miY2; miY2 = miY1 - miY2; miY1 -= miY2; }
                }
            }

            if (coord.PointCount < 5)
            {
                Xcenter = (maX1 + miX1) / 2;
                Ycenter = (maY1 + miY1) / 2;
            }
            else
            {
                Xcenter = (maX2 + miX2) / 2;
                Ycenter = (maY2 + miY2) / 2;
            }
            //Create received point
            string center = string.Format(CultureInfo.InvariantCulture.NumberFormat, "POINT({0} {1})", Xcenter, Ycenter);
            var Center = DbGeography.PointFromText(center, 4326);
            return Center;
        }
       
        public DetailFieldsModel GetDetailFieldsByType(int? type, LogusDBEntities db)
        {
            var res = new DetailFieldsModel
            {
                org_org_id = (from t in db.Fields
                              where t.type == type
                              select new DictionaryItemsDTO
                              {
                                  Id = t.Organizations.org_id,
                                  Name = t.Organizations.name
                              }).ToList()
            };
            return res;
        }
    }

    public class ModelFields
    {
        
        public string name { get; set; }
        public int? fiel_id { get; set; }
        public DictionaryItemsDTO org_org_id { get; set; }
        public string code { get; set; }
        public decimal? type { get; set; }
        public GeoCoord coord { get; set; }
    }

    public class DetailFieldsModel
    {
        public List<DictionaryItemsDTO> org_org_id { get; set; }
    }

    public class ModelUpdateFields : UpdateDictModel
    {
        public ModelFields Document { get; set; }
    }

    public class GeoCoord
    {
        public string coord { get; set; }
        public string coord2 { get; set; }
        public decimal area { get; set; }

        public string center { get; set; }
    }


   

    
}
