﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LogusSrv.DAL.DBModel;
using LogusSrv.DAL.Entities.DTO;
using LogusSrv.DAL.Entities.Res;
using System.Data.Entity.Validation;
using System.Web;

namespace LogusSrv.DAL.Operations.DictionaryOperations
{
    public class Group_tasksDict
    {
        public List<Group_tasksDictModel> GetGroup_tasksDict(LogusDBEntities db)
        {
            var res = db.Group_tasks.Where(t => t.deleted != true).Select(t => new Group_tasksDictModel
            {
                grtask_id = t.grtask_id,
                name = t.name,
                deleted = t.deleted,
            }).ToList();
            return res;
        }

        public string UpdateGroup_tasksDict(List<ModelUpdateGroup_tasks> updateObject, LogusDBEntities db)
        {
            using (var trans = db.Database.BeginTransaction())
            {
                try
                {
                    var newId = (db.Group_tasks.Max(t => (int?)t.grtask_id) ?? 0) + 1;
                    for (var i = 0; i < updateObject.Count; i++)
                    {
                        var obj = updateObject[i].Document;
                        switch (updateObject[i].command)
                        {
                            case "del":
                                var delDic = db.Group_tasks.Where(o => o.grtask_id == obj.grtask_id).FirstOrDefault();
                                delDic.deleted = true;
                                break;
                            case "update":
                                var updateDic = db.Group_tasks.Where(o => o.grtask_id == obj.grtask_id).FirstOrDefault();
                                updateDic.name = obj.name;
                                break;
                            case "insert":
                                var newDic = new Group_tasks
                                {
                                    grtask_id = newId,
                                    name = obj.name,
                                    deleted = false
                                };
                                newId++;
                                db.Group_tasks.Add(newDic);
                                break;
                        }
                    }

                    db.SaveChanges();
                    trans.Commit();
                }
                catch (Exception)
                {
                    trans.Rollback();
                    throw;
                }
            }
            return "success";
        }
    }

  

    public class Group_tasksDictModel
    {
        public int? grtask_id { get; set; }
        public string name { get; set; }
        public bool? deleted { get; set; }
    }
    public class ModelUpdateGroup_tasks : UpdateDictModel
    {
        public Group_tasksDictModel Document { get; set; }
    }
}
