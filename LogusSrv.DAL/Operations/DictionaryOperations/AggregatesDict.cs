﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LogusSrv.DAL.DBModel;
using LogusSrv.DAL.Entities.DTO;
using LogusSrv.DAL.Entities.Res;
using LogusSrv.CORE.Exceptions;

namespace LogusSrv.DAL.Operations.DictionaryOperations
{
    public class AggregateDict
    {
        public List<ModelAggregateDict> GetAggregateDict(LogusDBEntities db)
        {
            var res = (from t in db.Aggregates
                       where t.deleted == false
                       select new ModelAggregateDict
                       {
                           agg_id = t.agg_id,
                           name = t.name,
                           agg_code = t.agg_code,
                           trak_code = t.trak_code,
                           equi_code = t.equi_code,
                       }).ToList();
            return res;
        }

        public string UpdateFieldsDict(List<ModelUpdateAggregateDict> updateObject, LogusDBEntities db)
        {
            using (var trans = db.Database.BeginTransaction())
            {
                try
                {
                    var newId = (db.Aggregates.Max(t => (int?)t.agg_id) ?? 0) + 1;
                    for (var i = 0; i < updateObject.Count; i++)
                    {
                        var obj = updateObject[i].Document;
                    //    if (obj != null)
                    //    {
                            switch (updateObject[i].command)
                            {
                                case "del":
                                    if (obj.agg_id != null)
                                    {
                                        var delDic = db.Aggregates.Where(o => o.agg_id == obj.agg_id).FirstOrDefault();
                                        delDic.deleted = true;
                                    }
                                    break;
                                case "update":
                                    var updateDic = db.Aggregates.Where(o => o.agg_id == obj.agg_id).FirstOrDefault();
                                    updateDic.name = obj.name;
                                    updateDic.agg_code = obj.agg_code;
                                    updateDic.trak_code = obj.trak_code;
                                    updateDic.equi_code = obj.equi_code; ;
                                    break;
                                case "insert":
                                    var newDic = new Aggregates
                                    {
                                        agg_id = newId,
                                        name = obj.name,
                                        agg_code = obj.agg_code,
                                        trak_code = obj.trak_code,
                                        deleted = false,
                                        equi_code = obj.equi_code,
                                    };
                                    db.Aggregates.Add(newDic);

                                    break;
                            }
                    //    }
                    }
                    db.SaveChanges();
                    trans.Commit();
                }
                catch (Exception)
                {
                    trans.Rollback();
                    throw;
                }
            }
            return "success";
        }
    }

    public class ModelAggregateDict
    {
        public int? agg_id { get; set; }
        public string name { get; set; }
        public string agg_code { get; set; }
        public string trak_code { get; set; }
        public string equi_code { get; set; }
    }

    public class ModelUpdateAggregateDict : UpdateDictModel
    {
        public ModelAggregateDict Document { get; set; }
    }
}