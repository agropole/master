﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogusSrv.DAL.DBModel;
using LogusSrv.DAL.Entities.DTO;
using LogusSrv.DAL.Entities.DTO.DriverArm;
using LogusSrv.DAL.Entities.Res;
using LogusSrv.DAL.Entities.Req;
using LogusSrv.DAL.Enums;
using LogusSrv.DAL.Utils;
using System.Data.Entity.Spatial;
using System.Globalization;
using System.Threading.Tasks;
using System.Net;
using System.IO;
using System.Text;
using System.Net.Sockets;
using System.Collections.Specialized;
using System.Data.Entity.Validation;
using LogusSrv.DAL.Entities.DTO.ActsSzr;
using LogusSrv.DAL.Entities;
using LogusSrv.DAL.Operations.DictionaryOperations;
using System.Data;
using Z.BulkOperations;
using System.Data.SqlClient;
using System.Configuration;
using DocumentFormat.OpenXml.Drawing;
using Newtonsoft.Json.Linq;
using System.Web;

namespace LogusSrv.DAL.Operations
{
    public class DriverArmDb
    {
        protected readonly LogusDBEntities db = new LogusDBEntities();
        protected readonly LayersOperationDb loyersOperation = new LayersOperationDb();
        private GpsFromXmlDb _gpsParse = new GpsFromXmlDb();
        public String x, x1, y, y1, st;
        public int k1, k2, count = 0, count1 = 0, count3 = 0, con1 = 0, con2 = 0;
        List<Double?> busyX = new List<Double?>();
        List<Double?> busyY = new List<Double?>();
        List<int> ly = new List<int>();
        List<int> lx = new List<int>();

        //20171212_000000&EndDate=20171212_235959&IdCar=370039&Username=logusagro&Password=test
        //получаем xml-координаты
        public string GetXmlFromServer()
        {
           var date2 = DateTime.Now.AddHours(-3); 
           var date = DateTime.Now.AddHours(-3).AddMinutes(-10);

            //загрузить за опр. дату
        //   date = new DateTime(2018, 08, 16,12,00,00);
            //

           var responseString = "";
           var start_date = "";
        //   var start_date = date.ToString("yyyyMMdd_HHmmss");
          var end_date = date2.ToString("yyyyMMdd_HHmmss");

          //загрузить за опр. дату2
        //  end_date = new DateTime(2018, 08, 17, 07, 00, 00).ToString("yyyyMMdd_HHmmss"); //test

           var trackIdList = db.Traktors.Where(t => t.deleted != true).Select(t => t.track_id).ToList();
            //все последние даты
           var lastDates = (from f in db.GPS
                            group f by new { f.track_id } into g
                            select new GPSmodel
                                {
                                    id_track = (int)g.Key.track_id,
                                    date = (DateTime)g.Max(t => t.date)
                                }).ToList();

            using (var client = new WebClient())
          {
           for (int i = 0; i < trackIdList.Count; i++)
           {
               DateTime? check_date = null;
               var curTrackId = trackIdList[i];
               var check_track = lastDates.Where(t => t.id_track == curTrackId).FirstOrDefault();
               if (check_track != null)
               {
                   check_date = check_track.date.AddSeconds(1);
               }

              //загрузить за опр. дату2
            //  check_date = null;
               //
              if (check_date != null) {
              check_date = check_date.Value.AddHours(-3);
              start_date = check_date.Value.ToString("yyyyMMdd_HHmmss");
              }
              if (check_date == null) { start_date = date.ToString("yyyyMMdd_HHmmss"); }

               responseString = client.DownloadString("https://ons-systems.ru/mon/getxml?StartDate=" + start_date + "&EndDate=" + end_date + "&IdCar=" + trackIdList[i] + "&Username=logusagro&Password=test");

                if (responseString.Contains("COORD_DATA"))    {
                    using (var trans = db.Database.BeginTransaction())
                    {
                        try
                        {
                            Xml_archive newXML = new Xml_archive();
                            newXML.file_id = Guid.NewGuid();
                            newXML.date_load = DateTime.Now;
                            newXML.status = 1;
                            newXML.content = responseString;
                            db.Xml_archive.Add(newXML);
                            db.SaveChanges();
                            trans.Commit();
                        }
                        catch (DbEntityValidationException ex)
                        {
                            var errorMessages = ex.EntityValidationErrors
                                    .SelectMany(x => x.ValidationErrors)
                                    .Select(x => x.ErrorMessage);
                            var fullErrorMessage = string.Join("; ", errorMessages);
                            var exceptionMessage = string.Concat(ex.Message, " The validation errors are: ", fullErrorMessage);
                        }
                    }
               }
            }
          }
            return responseString;
        }

        public static DateTime UnixTimeStampToDateTime(double unixTimeStamp)
        {
            // Unix timestamp is seconds past epoch
            System.DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
            dtDateTime = dtDateTime.AddSeconds(unixTimeStamp).ToLocalTime();
            return dtDateTime;
        }


        //TestWialonData
        public JObject TestWialonData()
        {
            //dd3ac355e33615a76b5d4380158297727C589A245FE43244FF6E0FCE617885E6C316C453
            var end_date = new DateTime(2019, 07, 18, 19,00,00);
            var start_date = new DateTime(2019, 07, 12, 07, 00, 00);
            Int32 unixStartDate = (int)((start_date.ToUniversalTime().Ticks - 621355968000000000) / 10000000);
            Int32 unixEndDate = (int)((end_date.ToUniversalTime().Ticks - 621355968000000000) / 10000000);

            var json = new WebClient().DownloadString("https://hst-api.wialon.com/wialon/ajax.html?svc=token/login&params={" +
           " \"token\" : \"56743f56d2aaf835f9186f7128de783343E7AA58529C13BE3B53E53B3D0D4F6CF6082CE2\"   }");
            dynamic data = JObject.Parse(json);
            string sid = data.eid;  //id session
            //все id техник
            var WebClient = new WebClient();
            WebClient.Encoding = Encoding.UTF8;
            //avl_unit
            var techs = WebClient.DownloadString("https://hst-api.wialon.com/wialon/ajax.html?svc=core/search_items&params={" +
             "\"spec\" :{" +
             "\"itemsType\": \"\" , " +
             "\"propName\": \"reporttemplates\"," +                    //sys_name
             "\"propValueMask\" : \"*\", " +
             "\"sortType\" : \"sys_name\"" +
             "}," +
                 "\"force\": 1, " +
                 "\"flags\" : 1, " +
                 "\"from\" : 0, " +
                 "\"to\" : 0 " +
              "}&sid=" + sid);
            dynamic techList = JObject.Parse(techs);

            //loyersOperation.AddRefuelingsInSheetTracks();
            var logout = new WebClient().
             DownloadString("https://hst-api.wialon.com/wialon/ajax.html?svc=core/logout&params={}&sid=" + sid);

           // return JObject.Parse(fuel);
            return techList;
        }


        public string GetDataFromAutoGraph()
        {
            var end_date = DateTime.Now.AddHours(-3).ToString("yyyyMMdd-HHmm");
            string start_date = DateTime.Now.AddHours(-3).AddMinutes(-10).ToString("yyyyMMdd-HHmm");

            //end_date = "20200802-1603";
            //date = "20200801-0700";
            //start_date = "20200801-0700";
            var session = new WebClient().DownloadString("https://navi.gps36.ru/ServiceJSON/Login?UserName=logusagro&Password=logusagro");
            var schema = new WebClient().DownloadString("https://navi.gps36.ru/ServiceJSON/EnumSchemas?session=" + session);

            JArray schemaArr = JArray.Parse(schema);
            string schemaId = schemaArr.FirstOrDefault()?["ID"]?.Value<String>();

            //ids техник
            var listTech = new List<DictionaryItemsDTO>();
            var json2 = new WebClient().DownloadString("https://navi.gps36.ru/ServiceJSON/EnumDevices?"+"session=" + session + " &schemaID="+ schemaId);
            dynamic techs = JObject.Parse(json2);

            foreach (var obj in techs.Items)
            {
                listTech.Add(new DictionaryItemsDTO { 
                Id = (int)obj.Serial,
                Description = (string)obj.ID,
                });
            }

            //последние даты
            var lastDates = (from f in db.GPS
                             group f by new { f.track_id } into g
                             select new GPSmodel
                             {
                                 id_track = (int)g.Key.track_id,
                                 date = (DateTime)g.Max(t => t.date)
                             }).ToList();

            DateTime? check_date = null;
        //    var curTrackId = 2115310;
            var listGPS = new List<GPS>();
            try
            {
          int gpsId = CommonFunction.GetNextId(db, "GPS");
          for (int i = 0; i < listTech.Count; i++)   {

            var curTrackId = listTech[i].Id;
            var curTrackString = listTech[i].Description;
            var check_track = lastDates.Where(t => t.id_track == curTrackId).FirstOrDefault();
            if (check_track != null)
            {
                check_date = check_track.date.AddSeconds(1);
            }

            if (check_date != null)
            {
                start_date = check_date.Value.AddHours(-3).ToString("yyyyMMdd-HHmm"); 
            }

                    if (curTrackId == 9029823) {
                        var df = 0;
                    }



            var json = new WebClient().DownloadString("https://navi.gps36.ru/ServiceJSON/GetTrack?" + "session=" + session + " &schemaID=" + schemaId +
             "&IDs=" + curTrackId +"&" +
             "SD=" + start_date + "&ED=" + end_date + "&tripSplitterIndex=-1");
             dynamic data = JObject.Parse(json);

                    if ((data[curTrackString]).Count == 0) 
                    {
                        continue;
                    }
                var datesArr = data[curTrackString][0].DT;
                var speedArr = data[curTrackString][0].Speed;
                var LatArr = data[curTrackString][0].Lat;
                var LngArr = data[curTrackString][0].Lng;

                for (int j = 0; j < datesArr.Count; j++)
                {
                    var point = string.Format(CultureInfo.InvariantCulture.NumberFormat, "POINT({1} {0})", LatArr[j], LngArr[j]);
                    var coord = DbGeography.PointFromText(point, 4326);
                    listGPS.Add(new GPS
                    {
                        date = datesArr[j],
                        speed = speedArr[j],
                        track_id = curTrackId,
                        coord = coord,
                        gps_id = gpsId,
                    });
                    gpsId++;
                }
                }


                for (int i = 0; i < listGPS.Count; i++)
                {
                    listGPS[i].date = listGPS[i].date.Value.AddHours(3);
                }

                using (var scope = db.Database.BeginTransaction())
                {
                    LogusDBEntities context = null;
                    try
                    {
                        context = new LogusDBEntities();
                        context.Configuration.AutoDetectChangesEnabled = false;

                        int count = 0;
                        foreach (var entityToInsert in listGPS)
                        {
                            ++count;
                            context = AddToContext(context, entityToInsert, count, 1000, true);
                        }

                        context.SaveChanges();
                    }
                    finally
                    {
                        if (context != null)
                            context.Dispose();
                    }

                }
            }
            catch { Exception e; }

            return listGPS.Count.ToString();
        }


        public string GetGPSWialonData()
        {
            var end_date = DateTime.Now;
            var date = DateTime.Now.AddMinutes(-10);
            DateTime? start_date = new DateTime();

            var json = new WebClient().DownloadString("https://hst-api.wialon.com/wialon/ajax.html?svc=token/login&params={" +
            " \"token\" : \"56743f56d2aaf835f9186f7128de783343E7AA58529C13BE3B53E53B3D0D4F6CF6082CE2\"   }");
            dynamic data = JObject.Parse(json);

            string sid = data.eid;  //id session

            //все id техник
            var techs = new WebClient().DownloadString("https://hst-api.wialon.com/wialon/ajax.html?svc=core/search_items&params={" +
               "\"spec\" :{" +
               "\"itemsType\": \"avl_unit\" , " +
               "\"propName\": \"sys_name\"," +
               "\"propValueMask\" : \"*\", " +
               "\"sortType\" : \"sys_name\"" +
               "}," +
                   "\"force\": 1, " +
                   "\"flags\" : 1, " +
                   "\"from\" : 0, " +
                   "\"to\" : 0 " +
                "}&sid=" + sid);
            dynamic techList = JObject.Parse(techs);
            List<int> ids = new List<int>();
            foreach (var obj in techList.items)
            {
                ids.Add((int)obj.id);
            }
             //последние даты
            var lastDates = (from f in db.GPS
                             group f by new { f.track_id } into g
                             select new GPSmodel
                             {
                                 id_track = (int)g.Key.track_id,
                                 date = (DateTime)g.Max(t => t.date)
                             }).ToList();

            var report = "";
            var listGPS = new List<GPS>(); 
            int gpsId = CommonFunction.GetNextId(db, "GPS");
            for (int i = 0; i < ids.Count; i++)
            {

                DateTime? check_date = null;
                var curTrackId = ids[i];
                var check_track = lastDates.Where(t => t.id_track == curTrackId).FirstOrDefault();
                if (check_track != null)
                {
                    check_date = check_track.date.AddSeconds(1);
                }

                //загрузить за опр. дату2
                //  check_date = null;
                //
                if (check_date != null)
                {
                    start_date = check_date;
                }
                if (check_date == null) { start_date = date; }

                //test
              //  start_date = new DateTime(2019, 07, 19, 17, 12, 00);
              //  end_date = new DateTime(2019, 07, 24, 10, 37, 00);
                //==test

                Int32 unixStartDate = (int)((start_date.Value.ToUniversalTime().Ticks - 621355968000000000) / 10000000);
                Int32 unixEndDate = (int)((end_date.ToUniversalTime().Ticks - 621355968000000000) / 10000000);

                report = new WebClient().DownloadString("https://hst-api.wialon.com/wialon/ajax.html?svc=messages/load_interval&params={" +
                    "\"itemId\" :" + ids[i] + "," +
                    "\"timeFrom\" : " + unixStartDate + "," +
                    "\"timeTo\" : " + unixEndDate + "," +
                    "\"flags\" : 1," +
                    "\"flagsMask\" : 65281," +
                    "\"loadCount\" : \"0xffffffff\"" +
                    "}&sid=" + sid);
                 dynamic gpsList = JObject.Parse(report);

                 foreach (var obj in gpsList.messages)
                 {

                     var point = string.Format(CultureInfo.InvariantCulture.NumberFormat, "POINT({1} {0})", obj.pos.y, obj.pos.x);
                     var coord = DbGeography.PointFromText(point, 4326);
                     var ndate = UnixTimeStampToDateTime((double)obj.t);

                     var item = new GPS
                     {
                         gps_id = gpsId,
                         coord = coord,
                         date = ndate,
                         track_id = ids[i],
                   //    mileage = Math.Round((decimal)obj.p.mileage, 2) ,
                         speed = (int)obj.pos.s,
                     };
                     listGPS.Add(item);
                     gpsId++;
                 }

            }

            using (var scope = db.Database.BeginTransaction())
            {
                LogusDBEntities context = null;
                try
                {
                    context = new LogusDBEntities();
                    context.Configuration.AutoDetectChangesEnabled = false;

                    int count = 0;
                    foreach (var entityToInsert in listGPS)
                    {
                        ++count;
                        context = AddToContext(context, entityToInsert, count, 1000, true);
                    }

                    context.SaveChanges();
                }
                finally
                {
                    if (context != null)
                        context.Dispose();
                }

            }

            var logout = new WebClient().
              DownloadString("https://hst-api.wialon.com/wialon/ajax.html?svc=core/logout&params={}&sid=" + sid);

           // var js = JObject.Parse(report);
            return listGPS.Count.ToString();
        }

        private LogusDBEntities AddToContext(LogusDBEntities context, GPS entity, int count, int commitCount, bool recreateContext)
        {
            context.Set<GPS>().Add(entity);

            if (count % commitCount == 0)
            {
                context.SaveChanges();
                if (recreateContext)
                {
                    context.Dispose();
                    context = new LogusDBEntities();
                    context.Configuration.AutoDetectChangesEnabled = false;
                }
            }

            return context;
        }

        public List<double> GetCoordOrg(int? OrgId) {
         var lst = new List<double>();
         var checkOrg = db.Organizations.Where(t => t.org_id == OrgId).Select(t => t.tporg_tporg_id).FirstOrDefault();
         var newOrgId = checkOrg == 1 ? (int?)OrgId : null;

         var orgList = db.Organizations.Where(t => t.deleted != true && t.tporg_tporg_id == 1 && ((newOrgId != null ? t.org_id == newOrgId : true))).Select(t => t.org_id).ToList();
         var garList = db.Garages.Where(t => t.deleted != true && t.center != null && orgList.Contains(t.org_org_id)).Select(t => t.center).ToList();

         double? totalX = 0, totalY = 0;
         foreach (DbGeography p in garList)
         {
           totalX += p.Longitude;
           totalY +=  p.Latitude;
         }
         double centerX = (double) (totalX / garList.Count);
         double centerY = (double) (totalY / garList.Count);
         lst.Add(centerX); lst.Add(centerY);
         return lst;
        }

        //все координаты гаражей 
        public List<int> WriteCoordXY(int dx, int dy)
        {
            List<int> arr = new List<int>();
            arr.Clear();
            for (int l = dx; l < dy; l++)
            {
                arr.Add(l);
                l = l + 50;   //расстояние между техникой 
            }
            return arr;
        }
        public void ListOfBusyCoord(String x2, String y2, List<CoordByTraktorDTO> list)
        {
            for (int j = 0; j < list.Count; j++)
            {
                if (list[j].coord.Contains(x2) & list[j].coord.Contains(y2))
                {
                    busyX.Add(DbGeography.FromText(list[j].coord).Longitude); //x - координаты 
                    busyY.Add(DbGeography.FromText(list[j].coord).Latitude); //y-координаты 

                }
                ;
            }
        }
        public void SearchCoordXY(String x1, String y1)
        {

            for (int l = 0; l < lx.Count; l++)
            {
                if (count == 1) { break; }
                x = x1 + lx[l].ToString();
                for (int l1 = 0; l1 < ly.Count; l1++)
                {
                    if (count == 1) { break; }
                    y = y1 + ly[l1].ToString();
                    for (k2 = 0; k2 < busyY.Count; k2++)
                    {
                        if (busyY[k2] == Double.Parse(y, CultureInfo.InvariantCulture) &&
                          busyX[k2] == Double.Parse(x, CultureInfo.InvariantCulture)) { break; }
                        if (k2 == busyY.Count - 1) { count = 1; }
                    }

                }

            }

            count = 0;
            busyX.Add(Convert.ToDouble(x, CultureInfo.InvariantCulture));
            busyY.Add(Convert.ToDouble(y, CultureInfo.InvariantCulture));
        }
        public void AddNewRowInGPS(CoordByTraktorDTO item)
        {
            var trans = db.Database.BeginTransaction();
            var newDic = new GPS
            {
                coord = DbGeography.FromText("POINT(" + x + " " + y + ")"),
                date = DateTime.Now,
                track_id = item.track_id,
                direction = 00,
                speed = 00,
                mileage = null,
            };
            db.GPS.Add(newDic);
            db.SaveChanges();
            trans.Commit();
        }
        public void CheckTractorsInGarage(String x2, String y2, int dx1, int dx2, int dy1, int dy2, CoordByTraktorDTO item)
        {
            //проверяем стоит ли техника в гараже 
            con1 = 0;
            for (int l = dx1; l < dx2; l++) //ищем координату х в гараже 
            {
                if (DbGeography.FromText(item.coord).Longitude == Double.Parse(x2 + l.ToString(), CultureInfo.InvariantCulture))
                { con1 = 1; }
            }
            con2 = 0;
            for (int l = dy1; l < dy2; l++) //ищем координату y в гараже 
            {
                if (DbGeography.FromText(item.coord).Latitude == Double.Parse(y2 + l.ToString(), CultureInfo.InvariantCulture))
                { con2 = 1; }
            }
        }
        public List<ArmDriverInfoDTO> GetInfoDrivers()
        {
            var list = (from emp in db.Employees
                        join post in db.Posts on emp.pst_pst_id equals post.pst_id
                        join org in db.Organizations on emp.org_org_id equals org.org_id
                        where emp.pst_pst_id == 1
                        select new ArmDriverInfoDTO
                        {
                            driver_id = emp.emp_id,
                            first_name = emp.first_name,
                            middle_name = emp.middle_name,
                            second_name = emp.second_name,
                            post = post.name,
                            Organization = org.name,
                        }).ToList();
            return list;
        }
        public ArmWaysTaskInfoModel GetInfoWaysTaskByIdTraktor(decimal idTraktor, DateTime? dateNow = null)
        {
            if (dateNow == null)
            {
                dateNow = DateTime.Now;
            }
            int idSheet;
            var listSheet = db.Sheet_tracks.Where(s => s.trakt_trakt_id == idTraktor).ToList();

            idSheet = listSheet.Where(s => s.status == (int)WayListStatus.Issued && s.date_begin <= dateNow && s.date_end >= dateNow).Select(s => s.shtr_id).FirstOrDefault();
            if (idSheet == 0)
            {
                idSheet = listSheet.Where(s => s.status == (int)WayListStatus.Issued && s.date_begin > dateNow).Select(s => s.shtr_id).FirstOrDefault();
                if (idSheet == 0)
                {
                    idSheet = listSheet.Where(s => s.status == (int)WayListStatus.Issued && s.date_end < dateNow).Select(s => s.shtr_id).FirstOrDefault();
                    if (idSheet == 0) 
                    {
                        int min = -1;
                        for (var i=0; i<listSheet.Count; i++) {
                            var s = listSheet[i];
                            TimeSpan ts = s.date_begin - dateNow.Value;
                            var delta = Math.Abs(ts.TotalMilliseconds);
                            if (min < 0 || min > delta || delta == 0) {
                                idSheet = s.shtr_id;
                                min = (int)delta;
                            }
                        }
                    }
                }
            }
            
            /*
            var dateEnd = dateNow.AddDays(1);
            idSheet = listSheet.Where(s => s.date_begin >= dateNow && s.date_begin <= dateEnd && s.status == (int)WayListStatus.Issued).OrderBy(s => s.date_begin).Select(s => s.shtr_id).FirstOrDefault();
            if (idSheet == 0)
            {
                idSheet = listSheet.Where(s => s.date_begin >= dateNow && s.status == (int)WayListStatus.Issued).OrderBy(s => s.date_begin).Select(s => s.shtr_id).FirstOrDefault();
                if (idSheet == 0)
                {
                    idSheet = listSheet.Where(s => s.date_begin <= dateEnd && s.status == (int)WayListStatus.Issued).OrderByDescending(s => s.date_begin).Select(s => s.shtr_id).FirstOrDefault();
                    if (idSheet == 0)
                    {
                        idSheet = listSheet.Where(s => s.status == (int)WayListStatus.Closed).OrderByDescending(s => s.date_begin).Select(s => s.shtr_id).FirstOrDefault();
                    }
                }
            } */
            //оптимизировать две верхнии 
            //select TOP 1 * from Sheet_tracks where trakt_trakt_id = idTraktor order by cast(floor(cast(date_begin as float)) as datetime)  DESC , status ASC
            var listTask = (from t in db.Tasks
                            where t.shtr_shtr_id == idSheet
                            select new TaskArmDTO
                            {
                                field_id = t.fiel_fiel_id,
                                field_name = t.Fields.name,
                                plant_name = t.Fields_to_plants.Plants.name,
                                plant_icon_path = t.Fields_to_plants.Plants.Type_plants.icon_path, // Иконка культуры для инфопанели на карте
                                task_name = t.Type_tasks.name,
                                type_task_id = t.tptas_tptas_id,
                                task_id = t.tas_id,
                                status = (int)t.status,
                                date_begin = t.date_begin.Value,
                                date_end = t.date_end,
                                width = t.equi_equi_id != null ? (int)t.Equipments.width : (int)t.Sheet_tracks.Traktors.width,
                                equipment_name = t.Equipments.name,
                                equipment_id = t.equi_equi_id,
                            }).Take(5).ToList();


            var info = db.Sheet_tracks.Where(s => s.shtr_id == idSheet).Select(s => new ArmWaysTaskInfoModel
            {
                driver_id = s.Employees.emp_id,
                driver_name = s.Employees.short_name,
                traktor_id = (int)idTraktor,
                traktor_brand = s.Traktors.name,
                traktor_number = s.Traktors.reg_num,
                date_begin = s.date_begin,
                equipment_name = s.Equipments.name,
                status = (int)s.status,
                ways_list_id = s.shtr_id,
            }).FirstOrDefault();
            if (info != null) info.tasks_info = listTask;
            return info;
        }


        public string CreateTaskForWayList(CreateTask taskInfo)
        {
            using (var trans = db.Database.BeginTransaction())
            {
                var task_num = db.Tasks.Where(t => t.shtr_shtr_id == taskInfo.way_list_id).Select(t => t.task_num).ToList().Max(t => t);

                var newTask = new Tasks
                {
                    tas_id = CommonFunction.GetNextId(db, "Tasks"),
                    shtr_shtr_id = taskInfo.way_list_id,
                    tptas_tptas_id = taskInfo.task_type_id,
                    fiel_fiel_id = taskInfo.field_id,
                    status = 1,
                    task_num = task_num++
                };

                db.Tasks.Add(newTask);
                db.SaveChanges();
                trans.Commit();
                return "ok";
            }
        }

        public GPSDTO InsertGPSbyTrack(GPSDTO elemt)
        {

            DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
            using (var trans = db.Database.BeginTransaction())
            {
                int id = CommonFunction.GetNextId(db, "GPS");

                GPS gps = new GPS();
                gps.gps_id = id;
                gps.mileage = elemt.mileage;
                gps.speed = elemt.speed;
                gps.direction = elemt.direction;
                gps.date = dtDateTime.AddMilliseconds((double)elemt.time).ToLocalTime();
                var point = string.Format(CultureInfo.InvariantCulture.NumberFormat, "POINT({1} {0})", elemt.lat, elemt.lon);
                gps.coord = DbGeography.PointFromText(point, 4326);
                gps.track_id = elemt.track_id;
                id++;
                db.GPS.Add(gps);

                db.SaveChanges();
                trans.Commit();
            }
            return elemt;
        }

        public List<FieldArmDTO> GetInfoByFields()
        {
            var FieldsAndPlants = (from f in db.Fields_to_plants
                                   where f.status == (int)StatusFtp.Approved && f.date_ingathering.Year == DateTime.Now.Year
                                   group new PlantsDTO
                                   {
                                       plant_id = f.Plants.plant_id,
                                       plant_name = f.Plants.name,
                                       color = f.Plants.Type_plants.color,
                                       icon_path = f.Plants.Type_plants.icon_path,
                                       description = f.Plants.description,
                                       area = f.area_plant,
                                       date_ingathering = f.date_ingathering,
                                       date_sowing = f.date_sowing
                                   } by new
                                   {
                                       id = f.fiel_fiel_id,
                                       coord = f.Fields.coord.AsText(),
                                       center = f.Fields.center.AsText(),
                                       name = f.Fields.name,
                                       area = f.Fields.area,
                                   } into g
                                   select new FieldArmDTO
                                   {
                                       id = g.Key.id,
                                       coord = g.Key.coord,
                                       center = g.Key.center,
                                       name = g.Key.name,
                                       area = g.Key.area,
                                       plants = g.ToList()
                                   }).OrderBy(p => p.name).ToList();
            return FieldsAndPlants;
        }
     
        public GeoFieldsAndPlantsMapDto GetFieldsGeoJsonCoordinaties(int year)
        {
            GeoFieldsAndPlantsMapDto result = new GeoFieldsAndPlantsMapDto();
            result.features = new List<GeoFieldAndPlantsFeaturesDto>();
            result.type = "FeatureCollection";
            var FieldsAndPlants = (from f in db.Fields_to_plants
                                   where (f.status == (int)StatusFtp.Approved || f.status == (int)StatusFtp.Closed)
                                        && f.Fields.type == 1
                                        && f.date_ingathering.Year == year
                                        && f.plant_plant_id != 36
                                   group new PlantsDTO
                                   {
                                       plant_id = f.Plants.plant_id,
                                       plant_name = f.Plants.name,
                                       color = f.Plants.color,    //для определения цвета
                                       icon_path = f.Plants.Type_plants.icon_path,
                                       description = f.Plants.description,
                                       area = f.area_plant,
                                       date_ingathering = f.date_ingathering,
                                       date_sowing = f.date_sowing
                                   } by new
                                   {
                                       id = f.fiel_fiel_id,
                                       coord = f.Fields.coord.AsText(),
                                       name = f.Fields.name,
                                       area = f.Fields.area,
                                       status = f.status
                                   } into g
                                   select new ArmFieldsForGeo
                                   {
                                       id = g.Key.id,
                                       coord = g.Key.coord,
                                       name = g.Key.name,
                                       area = g.Key.area,
                                       status = (int)g.Key.status,
                                       plants = g.ToList()
                                   }).ToList();

            for (int i = 0; i < FieldsAndPlants.Count; i++)
            {
                if (String.IsNullOrEmpty(FieldsAndPlants[i].coord)) { FieldsAndPlants.RemoveAt(i); i--; }
            }


            for (int i = 0; i < FieldsAndPlants.Count; i++)
            {
                for (int j = 0; j < FieldsAndPlants[i].plants.Count; j++)
                {
                    if (FieldsAndPlants[i].status == 3)
                    {
                        FieldsAndPlants[i].plants.RemoveAt(j); j--;
                    }
                }
            }


            result.features = new List<GeoFieldAndPlantsFeaturesDto>();
            foreach (var field in FieldsAndPlants)
            {

                GeoFieldAndPlantsFeaturesDto info = new GeoFieldAndPlantsFeaturesDto();
                var foo = new CoordGeoJsonDTO
                {
                    Kk = DbGeography.FromText(field.coord.ToString())
                };
                var firstColor = field.plants.Select(p => p.color).FirstOrDefault();
                var jsonText = CommonFunction.ParseCoord(foo.Kk);
                info.type = "Feature";
                info.properties = new GeoFieldsAndPlantsPropDto();
                info.geometry = new CloseGeometryInfoDTO();
                info.geometry.coordinates = jsonText.ToObject<List<List<decimal[]>>>();
                info.geometry.type = "Polygon";
                if (field.plants.Count() != 1 && field.status != (int)StatusFtp.Closed)
                {
                    decimal? max = -1;
                    for (int i = 0; i < field.plants.Count; i++)
                    {

                        if (field.plants[i].area > max)
                        {
                            max = field.plants[i].area;
                            info.properties.color = field.plants[i].color != null ? field.plants[i].color : "0x2a728c";
                        }
                    }
                }
                else
                {
                    if (field.plants.Count() == 1 && field.status != (int)StatusFtp.Closed)
                    {
                        info.properties.color = field.plants.Select(p => p.color).FirstOrDefault();
                        info.properties.color = info.properties.color != null ? info.properties.color : "0x2a728c";
                    }
                    else { info.properties.color = ""; }
                }
                info.properties.plant = string.Join(", ", field.plants.Select(p => p.plant_name));
                info.properties.name = field.name;
                info.properties.area = field.area;
                info.properties.id = field.id;
                info.properties.plants = new List<PlantsDTO>();
                info.properties.plants = field.plants;
                result.features.Add(info);
            }
            return result;
        }
        public GeoFieldsAndPlantsMapDto GetFieldsGeoFactJsonCoordinaties(int year)
        {
            GeoFieldsAndPlantsMapDto result = new GeoFieldsAndPlantsMapDto();
            result.features = new List<GeoFieldAndPlantsFeaturesDto>();
            result.type = "FeatureCollection";
            var FieldsAndPlants = (from f in db.Fields
                                   where f.type == 3 && f.deleted != true
                                   group new PlantsDTO
                                   {
                                   } by new
                                   {
                                       id = f.fiel_id,
                                       coord = f.coord.AsText(),
                                       name = f.name,
                                       area = f.area,

                                   } into g
                                   select new ArmFieldsForGeo
                                   {
                                       id = g.Key.id,
                                       coord = g.Key.coord,
                                       name = g.Key.name,
                                       area = g.Key.area,
                                   }).ToList();




            result.features = new List<GeoFieldAndPlantsFeaturesDto>();
            foreach (var field in FieldsAndPlants)
            {

                GeoFieldAndPlantsFeaturesDto info = new GeoFieldAndPlantsFeaturesDto();
                var foo = new CoordGeoJsonDTO
                {
                    Kk = DbGeography.FromText(field.coord.ToString())
                };

                var jsonText = CommonFunction.ParseCoord(foo.Kk);
                info.type = "Feature";
                info.properties = new GeoFieldsAndPlantsPropDto();
                info.geometry = new CloseGeometryInfoDTO();
                info.geometry.coordinates = jsonText.ToObject<List<List<decimal[]>>>();
                info.geometry.type = "Polygon";
                info.properties.color = "0xffffff";
                info.properties.area = field.area;
                info.properties.id = field.id;
                result.features.Add(info);
            }
            return result;
        }
        public GeoTraktorMapDto GetTraktorsGeoJsonInfo()
        {

            GeoTraktorMapDto result = new GeoTraktorMapDto();
            result.features = new List<GeoTraktorFeaturesDto>();
            result.type = "FeatureCollection";
           // var df = db.Database.SqlQuery.;

          var  list = (from g in db.GPS
                    group new
                    {
                        track_id = g.track_id,
                        date = g.date,
                        gps_id = g.gps_id,
                    } by new
                    {
                        track_id = g.track_id,
                    } into g
                    select new CoordByTraktorDTO
                    {
                        track_id = g.Key.track_id,
                        date = g.Max(p => p.date),
                        gps_id = g.Max(p => p.gps_id),
                    }).ToList();
            var traktors = db.Traktors.ToList();
            var type_traktors = db.Type_traktors.ToList();
            for (int i = 0; i < list.Count; i++)
            {
                var id = list[i].gps_id;
                var track_id = list[i].track_id;

                var checkId = db.Traktors.Where(t => t.track_id == track_id).Select(t => t.trakt_id).FirstOrDefault();
                if (checkId != 0)
                {
                   var grtrack_id = db.Traktors.Where(t => t.trakt_id == checkId).Select(t => t.id_grtrakt).FirstOrDefault();
                    if (grtrack_id != null) {
                    var grtDic = db.Group_traktors.Where(t => t.grtrak_id == grtrack_id).FirstOrDefault();

                    list[i].name = grtDic.uniq_code;
                    list[i].grtrak_id = grtDic.grtrak_id;
                    }

                    list[i].id = checkId;
                    list[i].coord = db.GPS.Where(t => t.gps_id == id).Select(t => t.coord.AsText()).FirstOrDefault();
                    list[i].model = traktors.Where(t => t.track_id == track_id).Select(t => t.model).FirstOrDefault();
                    list[i].reg_num = traktors.Where(t => t.track_id == track_id).Select(t => t.reg_num).FirstOrDefault();
                    list[i].gar_gar_id = traktors.Where(t => t.track_id == track_id).Select(t => t.gar_gar_id).FirstOrDefault();
                    list[i].type_id = traktors.Where(t => t.track_id == track_id).Select(t => t.tptrak_tptrak_id).FirstOrDefault();
                    var type_id = list[i].type_id;
                    list[i].type_name = type_traktors.Where(t => t.tptrak_id == type_id).Select(t => t.name).FirstOrDefault();
                    list[i].icon_path = db.Traktors.Where(t => t.track_id == track_id).Select(t => t.Group_traktors.img).FirstOrDefault();
                    list[i].icon_path = list[i].icon_path != null ? list[i].icon_path : "assets/img/icons/groups_technic/dd.png";

                }
            }
           var list2 = list.Where(t => t.id != 0).ToList();
           list.Clear(); list.AddRange(list2);
            ///////new
           var garages = (from t in db.Garages
                         where t.deleted == false && 
                         t.center != null
                         select new GaragesDictModel   
                        {
                           gar_id = t.gar_id,
                           coord = new GeoCoord
                           {
                               coord = t.coord.AsText(),  
                               center = t.center.AsText()
                           }   
                       }).ToList();

           for (int i = 0; i < list.Count; i++)
           {
               if (DateTime.Now.Date.CompareTo(list[i].date) > 0)
               {

                   var garage = garages.Where(t => t.gar_id == list[i].gar_gar_id).FirstOrDefault();
                   var longitudeF =  DbGeography.FromText(garage.coord.center).Longitude.ToString().Replace(',','.'); 
                   var latitudeF = DbGeography.FromText(garage.coord.center).Latitude.ToString().Replace(',','.'); 

                   var longitude = longitudeF.Substring(0, 5);
                   var latitude = latitudeF.Substring(0, 5);
                   var dx1 =  Int32.Parse(longitudeF.Substring(5, 4));
                   var dx2 = dx1+3000;
                   var dy1 = Int32.Parse(latitudeF.Substring(5, 4));
                   var dy2 = dy1 + 3000;
                   CheckTractorsInGarage(longitude, latitude, dx1, dx2, dy1, dy2, list[i]);

                   if (con1 == 0 | con2 == 0)
                   {
                       lx = WriteCoordXY(dx1, dx2);
                       ly = WriteCoordXY(dy1, dy2);
                       ListOfBusyCoord(longitude, latitude, list); //записываем занятые координаты из list
                       SearchCoordXY(longitude, latitude);
                       AddNewRowInGPS(list[i]);
                       list[i].coord = "POINT (" + x + " " + y + ")";
                   }
               }
           }
 
            foreach (var traktor in list)
            {
                GeoTraktorFeaturesDto info = new GeoTraktorFeaturesDto();
                var foo = new CoordGeoJsonDTO
                {
                    Kk = DbGeography.FromText(
                        traktor.coord)
                };

                var jsonText = CommonFunction.ParseCoord(foo.Kk);
                info.type = "Feature";
                info.properties = new GeoTraktorPropDto();
                info.geometry = new CloseGeometryInfoDTO();
                info.geometry.coordinates = jsonText.ToObject<decimal[]>();
                info.geometry.type = "Point";
                info.properties.id = traktor.id;
                info.properties.track_id = traktor.track_id;
                info.properties.model = traktor.model;
                info.properties.reg_num = traktor.reg_num;
                info.properties.type_name = traktor.type_name;
                info.properties.type_id = traktor.type_id;
                info.properties.icon_path = traktor.icon_path;
                info.properties.date = traktor.date;
                info.properties.speed = traktor.speed;
                info.properties.mileage = traktor.mileage;
                info.properties.direction = traktor.direction;
                info.properties.name = traktor.name;
                info.properties.grtrak_id = traktor.grtrak_id;
                result.features.Add(info);
            }
            return result;

        }

        public List<CoordByTraktorDTO> GetLastCoordByTraktors()
        {
            var list = db.Database.SqlQuery<CoordByTraktorDTO>(
                string.Format(@"select  
                                ggr.coord.STAsText() AS  coord,
                                ggr.date AS date,
                                t.track_id,
                                ggr.speed,
                                ggr.direction,
                                ggr.mileage,
                                t.trakt_id AS id,
                                t.name, t.model,
                                t.reg_num,
                                t.tptrak_tptrak_id AS type_id,
                                t.emp_emp_id,
                                (select name from Type_traktors where tptrak_id =  t.tptrak_tptrak_id) AS type_name,
                                (select img from Type_traktors where tptrak_id =  t.tptrak_tptrak_id) AS icon_path
                                        
                                from Traktors t 
                                left join
                                    (select g.coord , g.date, g.track_id, g.speed, g.mileage, g.direction FROM 
                                    (select track_id, MAX(date) as date from GPS g group by g.track_id) gr
                                    join GPS g on gr.track_id = g.track_id and gr.date = g.date )
                                ggr on ggr.track_id = t.track_id order by id")).ToList();
            return list;
        }

        public GPSArmModel GetTop100GPSbyTrackId(int trackId, bool? tablet)
        {
           DateTime sDate = new DateTime();
           DateTime endDate = new DateTime();
           DateTime nowDate = DateTime.Now;
           PrintShiftDTO sheet = new PrintShiftDTO();
           GPSArmModel model = new GPSArmModel();
         
            var trakt_id = db.Traktors.Where(t => t.track_id == trackId).Select(t => t.trakt_id).FirstOrDefault();
            var Sheet = db.Sheet_tracks.Where(t => t.trakt_trakt_id == trakt_id && nowDate >= t.date_begin && nowDate <= t.date_end).FirstOrDefault();

            if (Sheet != null)
            {
                var task = db.Tasks.Where(t => t.shtr_shtr_id == Sheet.shtr_id && t.task_num == 1).FirstOrDefault();

                sDate = Sheet.date_begin;
                endDate = Sheet.date_end;
                sheet.Employees = Sheet.Employees.short_name;
                sheet.TraktorName =  Sheet.Traktors.name;
                sheet.TraktorNum =  Sheet.Traktors.reg_num;
                sheet.EquipmentName = Sheet.Equipments != null ? Sheet.Equipments.name : null;
                sheet.PlantName =  task.Fields_to_plants != null ? task.Fields_to_plants.Plants.short_name : null;
                sheet.FieldName = task.Fields_to_plants != null ? task.Fields_to_plants.Fields.name : null ;
                sheet.TaskPlan =  task.Type_tasks.name;
            }
                model.Sheet = sheet;
                model.Coord = db.GPS.Where(g => g.track_id == trackId && g.date >= sDate && g.date < endDate).
                OrderBy(g => g.date).
                Select(g => new Top100GPSmodel
                {
                    coord = g.coord.AsText(),
                    date = g.date,
                    direction = g.direction,
                    speed = g.speed,
                    mileage = g.mileage,
                }).ToList();
                if (tablet == true && model.Coord.Count != 0)
                {
                    model.Coord = getParkingStopData(model.Coord, true, true);
                }


            return model;
        }

        public string SetSatusWaysListToInWork(ArmWaysListReq req)
        {
            if ((req.ways_list_id != null && req.task_id != null) || (req.ways_list_id == null && req.task_id == null))
            {
                return "не надо так или ways_list_id или task_id";
            }


            if (req.ways_list_id != null)
            {
                using (var trans = db.Database.BeginTransaction())
                {
                    var item = db.Sheet_tracks.Where(e => e.shtr_id == req.ways_list_id).FirstOrDefault();
                    item.status = (decimal)req.status;

                    /*  if(req.status == 2) {
                          item.date_begin = dtDateTime.AddMilliseconds((double)req.time).ToLocalTime();
                          }
                       if(req.status == 3) {
                           item.date_end = dtDateTime.AddMilliseconds((double)req.time).ToLocalTime();
                          } */

                    db.SaveChanges();
                    trans.Commit();
                }
            }

            if (req.task_id != null)
            {

                if (req.status == 3)
                {
                    if (!string.IsNullOrEmpty(req.xml)) {
                        var gpsParse = new GpsFromXmlDb();
                        gpsParse.SaveGpsData(req.xml, XmlFileStatus.Parse);
                        CountArea(gpsParse, req);
                    }
                    else
                    {
                        if (req.task_id != null)
                        {
             //   using (var trans = db.Database.BeginTransaction())
             //   {
                            var task = db.Tasks.Where(e => e.tas_id == req.task_id).FirstOrDefault();
                            task.date_begin = DateTime.ParseExact(req.time_begin_str, "yyyy-MM-dd HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture);
                            task.date_end = DateTime.ParseExact(req.time_end_str, "yyyy-MM-dd HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture);
                            var sheetTrack = db.Sheet_tracks.Where(s => s.shtr_id == task.shtr_shtr_id).FirstOrDefault();
                            sheetTrack.status = (int)WayListStatus.Completed;
                            loyersOperation.calcThreadedArea(req.task_id.Value, task.date_end.Value, false);
                //            db.SaveChanges();
                //            trans.Commit();
               // }
                        }
                    }
                    
                }
            }

            var str = "Изменен статус на " + req.status + " у ";
            Console.WriteLine("RETURN");
            return req.ways_list_id != null ? str + " путевого листа id =  " + req.ways_list_id : str + " задания id =  " + req.task_id;
        }

        private async Task CountArea(GpsFromXmlDb gpsParse, ArmWaysListReq req)
        {
            await Task.Run(() =>
            {
                /*DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
                dtDateTime = dtDateTime.AddHours(3);*/
                var task = db.Tasks.Where(e => e.tas_id == req.task_id).FirstOrDefault();
                gpsParse.ParseXML(req.xml);

                using (var trans = db.Database.BeginTransaction())
                {

                    task.status = req.status;

                    if (req.status == 1)
                    {
                        task.date_begin = null;
                        task.date_end = null;
                    }
                    if (req.status == 2)
                    {
                        task.date_begin = DateTime.ParseExact(req.time_begin_str, "yyyy-MM-dd HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture);
                        task.date_end = null;
                    }
                    if (req.status == 3)
                    {
                        task.date_begin = DateTime.ParseExact(req.time_begin_str, "yyyy-MM-dd HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture);
                        task.date_end = DateTime.ParseExact(req.time_end_str, "yyyy-MM-dd HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture);

                        var tasks = db.Tasks.Where(t => t.shtr_shtr_id == task.shtr_shtr_id).ToList();
                        bool allTasksClosed = true;

                        foreach (var t in tasks)
                        {
                            if (t.tas_id != task.tas_id && t.status != 3)
                            {
                                allTasksClosed = false;
                            }
                        }

                        if (allTasksClosed == true)
                        {
                            var sheetTrack = db.Sheet_tracks.Where(s => s.shtr_id == task.shtr_shtr_id).FirstOrDefault();
                            sheetTrack.status = (int)WayListStatus.Completed;
                        }
                        loyersOperation.calcThreadedArea(req.task_id.Value, task.date_end.Value, false);
                    }
                    db.SaveChanges();
                    trans.Commit();
                }

                
                return "ok";
            });
        }

        public List<Top5TasksInfoModel> GetTop5WaysListbyFieldId(int fieldId)
        {

            var listResult = db.Tasks.Where(t => t.fiel_fiel_id == fieldId).OrderByDescending(t => t.Sheet_tracks.date_begin).Take(5)
                .Select(t => new Top5TasksInfoModel
                {
                    driver_id = t.Sheet_tracks.Employees.emp_id,
                    driver_name = t.Sheet_tracks.Employees.short_name,
                    traktor_brand = t.Sheet_tracks.Traktors.name,
                    traktor_id = t.Sheet_tracks.Traktors.trakt_id,
                    traktor_number = t.Sheet_tracks.Traktors.reg_num,
                    equipment_name = t.Sheet_tracks.equi_equi_id != null ? t.Sheet_tracks.Equipments.name : "Нет данных",
                    task_id = t.tas_id,
                    task_name = t.Type_tasks.name,
                    field_name = t.Fields.name,
                    date_begin = t.Sheet_tracks.date_begin,
                    date_end = t.Sheet_tracks.date_end,
                    status = (int)t.Sheet_tracks.status,
                    plant_name = t.Fields_to_plants.Plants.name,
                    area = t.area,
                }).ToList();

            return listResult;

        }

        public TaskInfoDTO GetTaskInfoById(int taskId)
        {
            var taskList = db.Tasks.Where(t => t.tas_id == taskId).Select(t => new TaskInfoDTO
            {
                task_id = t.tas_id,
                field_name = t.Fields.name,
                plant_name = t.Fields_to_plants.Plants.name,
                task_name = t.Type_tasks.name,
                equip_name = t.Equipments.name,
                work_time = t.worktime,
                work_day = t.workdays,
                price_days = t.price_days,
                moto_hours = t.moto_hours,
                volume_fact = t.volume_fact,
                area = t.area,
                salary = t.salary,
                consumption_rate = t.consumption_rate,
                consumption = t.consumption_rate != null && t.area != null ? t.area * t.consumption_rate : 0,
                consumption_fact = t.consumption_fact,
                available_name = t.Employees.short_name,
                task_num = t.task_num,
                driver_name = t.Sheet_tracks.Employees.short_name,
                traktor_brand = t.Sheet_tracks.Traktors.name,
                traktor_number = t.Sheet_tracks.Traktors.reg_num,
                list_num = t.Sheet_tracks.num_sheet,
                date_begin = t.Sheet_tracks.date_begin,
                equipment_name = t.Sheet_tracks.Equipments.name
            }).FirstOrDefault();
            return taskList;
        }

        public string ArchiveDB1()
        {
            using (var trans = db.Database.BeginTransaction())
            {
                var nId = (db.GPS_archive_log.Max(t => (int?)t.id) ?? 0) + 1;
                var g = new GPS_archive_log
                {
                    id = nId,
                    date = DateTime.Now,
                    ctrl = "Driver",
                    func = "Test",
                    msg = "Test",
                    flag = "Test",
                };
                db.GPS_archive_log.Add(g);
                db.SaveChanges();
                trans.Commit();
            }
            return "success";
        }


        public string ArchiveDB()
        {
          archive();
          return "success";
        }

        //архивация данных
        private async Task archive()
        {
            await Task.Run(() =>
            {
                db.Database.CommandTimeout = 600;

                var nId = (db.GPS_archive_log.Max(t => (int?)t.id) ?? 0) + 1;
                var d = DateTime.Now.AddMonths(-3);

                var con = System.Configuration.ConfigurationManager.ConnectionStrings["LogusDBEntities"].ConnectionString;
                System.Data.Entity.Core.EntityClient.EntityConnectionStringBuilder efBuilder =
                    new System.Data.Entity.Core.EntityClient.EntityConnectionStringBuilder(con);
                con = efBuilder.ProviderConnectionString;
                string insertGPS_archive = "  set IDENTITY_INSERT GPS_archive ON " +
                "INSERT INTO GPS_archive ([gps_id],[coord],[date],[track_id],[direction],[speed],[mileage]) "+
                "SELECT [gps_id],[coord],[date],[track_id],[direction],[speed],[mileage] FROM GPS WHERE date < DATEADD(month, -3, GETDATE()) "+
                "set IDENTITY_INSERT GPS_archive OFF";

                string deleteGPS = "delete FROM GPS WHERE date < DATEADD(month, -3, GETDATE())";

                string delXml = " delete  FROM Xml_archive WHERE date_load < DATEADD(month, -3, GETDATE())";

                using (var connection = new SqlConnection(con))
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand(insertGPS_archive, connection);
                    command.CommandTimeout = 18000;  
                    command.ExecuteNonQuery();

                    SqlCommand command2 = new SqlCommand(deleteGPS, connection);
                    command2.CommandTimeout = 18000;  
                    command2.ExecuteNonQuery();
                    SqlCommand command3 = new SqlCommand(delXml, connection);
                    command3.CommandTimeout = 18000;  
                    command3.ExecuteNonQuery();

                    connection.Close();
                };

                using (var trans = db.Database.BeginTransaction())
                {

                    var g = new GPS_archive_log
                    {
                        id = nId,
                        date = DateTime.Now,
                        ctrl = "Driver",
                        func = "ArchiveDB",
                        flag = d.ToString(),
                    };
                    db.GPS_archive_log.Add(g);

                    db.SaveChanges();
                    trans.Commit();
                }


            });
        }

        public string InsertCoords(List<GPSDTOfromDriver> gps_list)
        {
            using (var trans = db.Database.BeginTransaction())
            {
                for (int i = 0; i < gps_list.Count; i++)
                {
                    var point = string.Format(CultureInfo.InvariantCulture.NumberFormat, "POINT({1} {0})", gps_list[i].lat, gps_list[i].lon);
                    var g = new GPS
                    {
                        coord = DbGeography.PointFromText(point, 4326),
                        date = DateTime.ParseExact(gps_list[i].time, "yyyy-MM-dd HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture),
                        direction = gps_list[i].direction,
                        mileage = gps_list[i].mileage,
                        speed = gps_list[i].speed,
                        track_id = gps_list[i].track_id
                    };
                    db.GPS.Add(g);
                }


                db.SaveChanges();
                trans.Commit();
            }
            return "success";
        }

        public List<DictionaryItemsDTO> GetTaskList()
        {
            return db.Type_tasks.Where(p => p.deleted != true).Select(p => new DictionaryItemsDTO { Id = p.tptas_id, Name = p.name }).OrderBy(p=>p.Name).ToList();
        }

        public List<DictionaryItemsTraktors> GetEquiList()
        {
            return db.Equipments.Where(p => p.deleted != true).Select(p => new DictionaryItemsTraktors { Id = p.equi_id, Name = p.name, Width = p.width }).OrderBy(p => p.Name).ToList();
        }

        public TaskArmDTO PostTask(TaskArmDTO task)
        {
            using (var trans = db.Database.BeginTransaction())
            {
                var task_num = db.Tasks.Where(t => t.shtr_shtr_id == task.way_list_id).Select(t => t.task_num).ToList().Max(t => t);
                var newid =  CommonFunction.GetNextId(db, "Tasks");
                task.task_id = newid;
                var equi = db.Equipments.Where(p => p.equi_id == task.equipment_id).First();
                
                var newTask = new Tasks
                {
                    tas_id = newid,
                    shtr_shtr_id = task.way_list_id.Value,
                    tptas_tptas_id = task.type_task_id,
                    fiel_fiel_id = task.field_id,
                    equi_equi_id = task.equipment_id,
                    width_equip = equi.width + 3,
                    status = 1,
                    task_num = task_num++
                };

                var shtr = db.Sheet_tracks.Where(t => t.shtr_id == task.way_list_id).First();
                shtr.status = 1;

                db.Tasks.Add(newTask);
                db.SaveChanges();
                trans.Commit();
                task.way_list_id = null;
                return task;
            }
        }
   
        public List<ProgressMap> GetFieldInfoById(int plantId, int year, int fieldId)
        {
            List<ProgressMap> taskList = new List<ProgressMap>();
            List<ProgressMap> taskListChacked = new List<ProgressMap>();
            bool back = false, equal = false, forvard = false;
            List<ChackTimeStage> grouptask = new List<ChackTimeStage>();
            if (db.TC_operation.Where(t => t.id_tc == t.TC.id && t.TC.plant_plant_id == plantId && t.TC.year == year && t.TC.fiel_fiel_id != fieldId).Select(t => t.TC.fiel_fiel_id).FirstOrDefault() != null)
            {
                taskList = (from t in db.TC_operation
                            where t.id_tc == t.TC.id && t.TC.plant_plant_id == plantId
                            && t.TC.year == year && t.TC.fiel_fiel_id == fieldId
                            select new ProgressMap
                            {
                                TaskId = t.tptas_tptas_id,
                                TaskName = t.Type_tasks.name,
                                groupId = t.Type_tasks.type_fuel_id != null ? t.Type_tasks.type_fuel_id : 5, // группа работ
                                DateT = t.date_start.Value,
                                DateT1 = t.date_finish.Value,

                            }
                              ).OrderBy(s => s.DateT).ToList();
            }
            else
            {
                taskList = (from t in db.TC_operation
                            where t.id_tc == t.TC.id && t.TC.plant_plant_id == plantId
                            && t.TC.year == year && t.TC.fiel_fiel_id == null
                            select new ProgressMap
                            {
                                TaskId = t.tptas_tptas_id,
                                TaskName = t.Type_tasks.name,
                                groupId = t.Type_tasks.type_fuel_id != null ? t.Type_tasks.type_fuel_id : 5, // группа работ
                                DateT = t.date_start.Value,
                                DateT1 = t.date_finish.Value,

                            }
                              ).OrderBy(s => s.DateT).ToList();
            }

            for (int i = 0; i < taskList.Count; i++)
            {
                int breaking = 0;
                List<ProgressMap> datelist = new List<ProgressMap>();
                var typetask = taskList[i].TaskId;
                var date_start = taskList[i].DateT;
                var date_end = taskList[i].DateT1;
                var chackDate = grouptask.Where(g => g.taskID == typetask).Select(g => g.DataChack).FirstOrDefault();

                taskList[i].datePlan = taskList[i].DateT.ToString("dd.MM.yyyy") + "<br>" + taskList[i].DateT1.ToString("dd.MM.yyyy");

                if (grouptask.Where(g => g.taskID == typetask).Select(g => g.taskID).FirstOrDefault() == null)
                {
                    grouptask.Add(new ChackTimeStage
                    {
                        taskID = (int)typetask,
                        back = false,
                        equal = false,
                        forvard = false,


                    });
                }
                else
                {
                    back = grouptask.Where(g => g.taskID == typetask).Select(g => g.back).FirstOrDefault();
                    equal = grouptask.Where(g => g.taskID == typetask).Select(g => g.equal).FirstOrDefault();
                    forvard = grouptask.Where(g => g.taskID == typetask).Select(g => g.forvard).FirstOrDefault();

                }

                if (date_start <= DateTime.Now)
                {
                    datelist = db.Tasks.Where(s => s.tptas_tptas_id == typetask && s.Fields_to_plants.Plants.plant_id == plantId
                        && s.Sheet_tracks.date_begin.Year == year && s.Fields_to_plants.fiel_fiel_id == fieldId).Select(s => new ProgressMap
                        {
                            Date = s.Sheet_tracks.date_begin,
                            DateT = s.Sheet_tracks.date_end,
                        }).OrderBy(s => s.Date).ToList();
                    if (datelist.Count > 0)
                    {
                        if (equal == false)
                        {

                            taskListChacked = datelist.Where(d => d.Date >= date_start && d.DateT <= date_end
                            ).Select(d => new ProgressMap
                            {
                                Date = d.Date,
                            }).OrderBy(s => s.Date).ToList();
                            if (taskListChacked.Count == 0)
                            {
                                equal = true;
                                back = false;
                                forvard = false;
                            }
                            else
                            {
                                equal = false;
                                back = true;
                                forvard = true;

                            }
                        }
                        if (back == false)
                        {
                            taskListChacked = datelist.Where(d => d.Date <= date_start
                            ).Select(d => new ProgressMap
                            {
                                Date = d.Date,
                            }).OrderBy(s => s.Date).ToList();
                            if (taskListChacked.Count == 0)
                            {

                                equal = true;
                                back = true;
                                forvard = false;
                            }
                            else
                            {
                                equal = true;
                                back = false;
                                forvard = true;
                            }
                        }
                        if (forvard == false)
                        {
                            taskListChacked = datelist.Where(d => d.DateT >= date_end
                            ).Select(d => new ProgressMap
                            {
                                Date = d.Date,
                            }).OrderBy(s => s.Date).ToList();
                            if (taskListChacked.Count == 0)
                            {

                                equal = false;
                                back = true;
                                forvard = true;
                                if (breaking == 1) { break; }
                                breaking++;
                            }
                            else
                            {
                                equal = true;
                                back = true;
                                forvard = false;
                            }
                        }
                        grouptask.Where(g => g.taskID == typetask).Select(g => g.equal = equal).ToList();
                        grouptask.Where(g => g.taskID == typetask).Select(g => g.back = back).ToList();
                        grouptask.Where(g => g.taskID == typetask).Select(g => g.forvard = forvard).ToList();


                        int min = 400;
                        int max = -1;
                        for (int j = 0; j < taskListChacked.Count; j++)
                        {
                            if (taskListChacked[j].Date > chackDate.AddDays(1))
                            {
                                int delta = taskListChacked[j].Date.DayOfYear - taskList[i].DateT1.DayOfYear;
                                if (min > delta && delta > 0)
                                {
                                    min = delta;
                                    taskList[i].dateFact = taskListChacked[j].Date.ToString("dd.MM.yyyy");
                                    taskList[i].Date = taskListChacked[j].Date;
                                    grouptask.Where(g => g.taskID == typetask).Select(g => g.DataChack = taskListChacked[j].Date).ToList();
                                }
                                else if (delta <= 0)
                                {
                                    if (max < Math.Abs(delta))
                                    {
                                        max = Math.Abs(delta);
                                        taskList[i].dateFact = taskListChacked[j].Date.ToString("dd.MM.yyyy");
                                        taskList[i].Date = taskListChacked[j].Date;
                                        grouptask.Where(g => g.taskID == typetask).Select(g => g.DataChack = taskListChacked[j].Date).ToList();
                                    }

                                }
                            }
                        }


                    }
                }

                switch (taskList[i].groupId)
                {
                    case 1: taskList[i].colorgroup = "#66ccff"; break;
                    case 2: taskList[i].colorgroup = "#33cc00"; break;
                    case 3: taskList[i].colorgroup = "#ffff1a"; break;
                    case 4: taskList[i].colorgroup = "#b38600"; break;
                    case 5: taskList[i].colorgroup = "#c2c2a3"; break;
                }
                taskList[i].dateFact = taskList[i].dateFact != null ? taskList[i].dateFact : " - ";
                taskList[i].DateT = taskList[i].DateT.Date;
                taskList[i].DateT1 = taskList[i].DateT1.Date;
                taskList[i].Date = taskList[i].Date.Year == 0001 ? taskList[i].DateT.Date : taskList[i].Date.Date;
            }

            return taskList;
        }
        public List<ProgressInfoField> GetPlantOnField(int fieldId, int year)
        {
            var plantsList = db.Fields_to_plants.Where(p => p.fiel_fiel_id == fieldId && p.date_sowing.Value.Year == year && p.plant_plant_id != 36).Select(p => new ProgressInfoField
            {
                PlantId = p.plant_plant_id,
                PlantName = p.Plants.name,
                FieldName = p.Fields.name,
            }).ToList();
            return plantsList;
        }

        public GeoFieldsAndPlantsMapDto GetFieldCoordinatGeoProgress(int year, int fieldId)
        {
            GeoFieldsAndPlantsMapDto result = new GeoFieldsAndPlantsMapDto();
            result.features = new List<GeoFieldAndPlantsFeaturesDto>();
            result.type = "FeatureCollection";
            var FieldsAndPlants = (from f in db.Fields_to_plants
                                   where (f.status == (int)StatusFtp.Approved || f.status == (int)StatusFtp.Closed)
                                        && f.Fields.fiel_id == fieldId
                                        && f.date_ingathering.Year == year
                                        && f.plant_plant_id != 36
                                   group new PlantsDTO
                                   {
                                       plant_id = f.Plants.plant_id,
                                       plant_name = f.Plants.name,
                                       color = f.Plants.color,//для определения цвета
                                       icon_path = f.Plants.Type_plants.icon_path,
                                       description = f.Plants.description,
                                       area = f.area_plant,
                                       date_ingathering = f.date_ingathering,
                                       date_sowing = f.date_sowing
                                   } by new
                                   {
                                       id = f.fiel_fiel_id,
                                       coord = f.Fields.coord.AsText(),
                                       name = f.Fields.name,
                                       area = f.Fields.area,
                                       status = f.status
                                   } into g
                                   select new ArmFieldsForGeo
                                   {
                                       id = g.Key.id,
                                       coord = g.Key.coord,
                                       name = g.Key.name,
                                       area = g.Key.area,
                                       status = (int)g.Key.status,
                                       plants = g.ToList()
                                   }).ToList();

            for (int i = 0; i < FieldsAndPlants.Count; i++)
            {
                if (String.IsNullOrEmpty(FieldsAndPlants[i].coord)) { FieldsAndPlants.RemoveAt(i); i--; }
            }


            for (int i = 0; i < FieldsAndPlants.Count; i++)
            {
                for (int j = 0; j < FieldsAndPlants[i].plants.Count; j++)
                {
                    if (FieldsAndPlants[i].status == 3)
                    {
                        FieldsAndPlants[i].plants.RemoveAt(j); j--;
                    }
                }
            }


            result.features = new List<GeoFieldAndPlantsFeaturesDto>();
            foreach (var field in FieldsAndPlants)
            {

                GeoFieldAndPlantsFeaturesDto info = new GeoFieldAndPlantsFeaturesDto();
                var foo = new CoordGeoJsonDTO
                {
                    Kk = DbGeography.FromText(field.coord.ToString())
                };

                var maxArea = field.plants.Select(p => p.area).Max();
                var jsonText = CommonFunction.ParseCoord(foo.Kk);
                info.type = "Feature";
                info.properties = new GeoFieldsAndPlantsPropDto();
                info.geometry = new CloseGeometryInfoDTO();
                info.geometry.coordinates = jsonText.ToObject<List<List<decimal[]>>>();
                info.geometry.type = "Polygon";
                if (field.plants.Count() != 1 && field.status != (int)StatusFtp.Closed)
                {
                    decimal? max = -1;
                    for (int i = 0; i < field.plants.Count; i++)
                    {

                        if (field.plants[i].area > max)
                        {
                            max = field.plants[i].area;
                            info.properties.color = field.plants[i].color != null ? field.plants[i].color : "0x2a728c";
                        }
                    }
                }
                else
                {
                    if (field.plants.Count() == 1 && field.status != (int)StatusFtp.Closed)
                    {
                        info.properties.color = field.plants.Select(p => p.color).FirstOrDefault();
                        info.properties.color = info.properties.color != null ? info.properties.color : "0x2a728c";
                    }
                    else { info.properties.color = ""; }
                }
                info.properties.plant = string.Join(", ", field.plants.Select(p => p.plant_name));
                info.properties.name = field.name;
                info.properties.area = field.area;
                info.properties.id = field.id;
                info.properties.plants = new List<PlantsDTO>();
                info.properties.plants = field.plants;
                result.features.Add(info);
            }
            return result;
        }

        public List<Top100GPSmodel> GetGPSbyDate(int traktId, string start_time, string end_time, bool? stop, bool? parking)
        {
            TimeSpan ts = new TimeSpan(01, 15, 0);
            var checkDate = (DateTime.Now.Date + ts).AddMonths(-3);

            var st_date = start_time.Split('-')[0].ToString();
            if (st_date.Length == 1) { start_time = "0" + start_time; }

            var en_date = end_time.Split('-')[0].ToString();
            if (en_date.Length == 1) { end_time = "0" + end_time; }

            DateTime start = DateTime.ParseExact(start_time, "dd-MM-yyyy HH:mm",
                                       System.Globalization.CultureInfo.InvariantCulture);
            DateTime end = DateTime.ParseExact(end_time, "dd-MM-yyyy HH:mm",
                                       System.Globalization.CultureInfo.InvariantCulture);
            var track_id = db.Traktors.Where(t => t.trakt_id == traktId).Select(t => t.track_id).FirstOrDefault();
            var listResult = new List<Top100GPSmodel>();
            if (start <= checkDate)
            {
                listResult = db.GPS_archive.Where(g => g.track_id == track_id && g.date >= start && g.date < end).
                    Select(g => new Top100GPSmodel
                    {
                        coord = g.coord.AsText(),
                        date = g.date,
                        direction = g.direction,
                        speed = g.speed,
                        mileage = g.mileage,
                    }).ToList();
            }
            if (start >= checkDate)
            {
             //   track_id = 370029;  //test   370029
                listResult.AddRange(db.GPS.Where(g => g.track_id == track_id && g.date >= start && g.date < end).
                OrderByDescending(g => g.date).
                Select(g => new Top100GPSmodel
                {
                    id = g.gps_id,
                    coord = g.coord.AsText(),
                    date = g.date,
                    direction = g.direction,
                    speed = g.speed,
                    mileage = g.mileage,
                }).ToList());
            }
            listResult = listResult.OrderBy(g => g.date).ToList();

            if (parking == true || stop == true)
            {
                listResult = getParkingStopData(listResult, parking, stop);
            }
                return listResult;
        }

        public List<Top100GPSmodel> getParkingStopData(List<Top100GPSmodel> listResult, bool? parking, bool? stop) {
            int? startStop = null; int countCoord = 0;
            TimeSpan parkingVal = new TimeSpan(0, Int32.Parse(db.Parameters.Where(t => t.name.Equals("Parking")).Select(t => t.value).FirstOrDefault()), 0);
            //в мин.
            TimeSpan stopVal = new TimeSpan(0, Int32.Parse(db.Parameters.Where(t => t.name.Equals("Stop")).Select(t => t.value).FirstOrDefault()), 0);
            var speedVal = Decimal.Parse(db.Parameters.AsEnumerable().Where(t => t.name.Equals("Speed")).Select(t => t.value.Replace('.',',')).FirstOrDefault());
            TimeSpan maxParking = new TimeSpan(0, Int32.Parse(db.Parameters.Where(t => t.name.Equals("MaxParking")).Select(t => t.value).FirstOrDefault()), 0);

            for (int i = 0; i < listResult.Count; i++)
            {
                if (listResult[i].speed <= speedVal && startStop == null)
                {
                    startStop = i;
                }
                 //поехал
                if ((startStop != null && listResult[i].speed > speedVal) || (startStop != null && listResult.Count - 1 == i))
                {
                    countCoord++;
                    TimeSpan? curDate = new TimeSpan(0);
                    if (listResult.Count - 1 == i)
                    {
                        curDate = DateTime.Now - listResult[startStop.Value].date.Value;
                    }
                    else {
                        if (countCoord > 3)
                        {
                            curDate = listResult[i-3].date.Value - listResult[startStop.Value].date.Value;
                        }
                    }
                    var days = curDate.Value.Days != 0 ? curDate.Value.Days.ToString() + " д." : "";
                    var hour = curDate.Value.Hours != 0 ? curDate.Value.Hours.ToString() + " ч." : "";
                    var minutes = curDate.Value.Minutes != 0 ? curDate.Value.Minutes.ToString() + " мин." : "";
                    var seconds = curDate.Value.Seconds != 0 ? curDate.Value.Seconds.ToString() + " сек." : "";

                    if (curDate > stopVal && curDate < parkingVal && countCoord > 3)
                    {
                        if (stop == true)
                        {
                            listResult[i].stop = true;
                            listResult[i].massage = "Остановка: " + listResult[startStop.Value].date.Value.ToString("dd.MM.yyyy HH:mm:ss") +
                               ". Длительность: " + days + hour + " " + minutes + " " + seconds;
                        }
                        countCoord = 0;
                        startStop = null;
                    }
                    if (curDate > parkingVal && maxParking > curDate && countCoord > 3)
                    {
                        if (parking == true)
                        {
                            listResult[i].parking = true;
                            listResult[i].massage = "Парковка: " + listResult[startStop.Value].date.Value.ToString("dd.MM.yyyy HH:mm:ss") +
                               ". Длительность: " + days + hour + " " + minutes + " " + seconds;
                        }

                        countCoord = 0;
                        startStop = null;
                    }
                    if (countCoord > 3)
                    {
                        startStop = null;
                        countCoord = 0;
                    }
                }
                if (startStop != null && listResult.Count - 1 == i) {
                    var curDate = DateTime.Now - listResult[startStop.Value].date;

                }

            }
            return listResult;
        }



        public List<GetTMCMao> GetTMCforMap(int? field, int year)
        {
            var fielplan = db.Fields_to_plants.Where(f => f.fiel_fiel_id == field && f.date_sowing.Value.Year == year).Select(f => f.fielplan_id).FirstOrDefault();
            var TMCList = (from t in db.TMC_Records
                           where t.fielplan_fielplan_id == fielplan && t.TMC_Acts.year == year
                           select new GetTMCMao
                           {
                               Id = t.mat_mat_id,
                               Material = t.Materials.name,
                               Count = t.count,
                               Cost = t.price,
                               MaterialType = t.Materials.mat_type_id

                           }).ToList();
            var list = TMCList.GroupBy(t1 => new { t1.Id, t1.Material })
                         .Select(x => new GetTMCMao
                         {
                             Id = x.First().Id,
                             Material = x.First().Material,
                             Count = Math.Round(x.Sum(t1 => t1.Count), 2),
                             Cost = x.First().Cost,
                             MaterialType = x.First().MaterialType
                         }).ToList();

            return list;
        }
        public List<ImageFieldDto> GetPhotoByFieldYear(int? field, int year)
        {
            var res = db.Photo_fields.Where(t => t.fiel_fiel_id == field && t.date.Value.Year == year).AsEnumerable()
               .Select(t => new ImageFieldDto { 
               fiel_fiel_id = t.fiel_fiel_id,
               id = t.photo_id,
               note = t.note,
               name_photo = t.path_photo,
               date2 = t.date.Value.ToString("dd.MM.yyyy"),
               marker = t.marker,
               }).ToList();
            return res;
        }


    }
}