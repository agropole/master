﻿using System;
using System.Data.Entity;
using System.Collections.Generic;
using System.Linq;
using LogusSrv.CORE.Exceptions;
using LogusSrv.DAL.Entities.DTO.ActsSzr;
using LogusSrv.DAL.Entities.DTO;
using LogusSrv.DAL.Entities.Req;
using LogusSrv.DAL.DBModel;
using LogusSrv.DAL.Utils;
using LogusSrv.DAL.Enums;
using LogusSrv.DAL.Entities.Res;
using System.Data.Entity.Validation;


namespace LogusSrv.DAL.Operations
{
    public class ActsSrzDb
    {
        protected readonly LogusDBEntities db = new LogusDBEntities();
        public readonly WayListBase wayList = new WayListBase();
        public ActSzrDetails1 result1;
        public int? key; public int t = 0;
        public int tma;
        public ActSzrDetails1 resultActSzr1;
        List<BalanceMaterials> balanceList = new List<BalanceMaterials>();
        public GetActsSzrListResponse GetActsLists(GridDataReq req)
        {
            return GetActs(req, false);
        }

        public GetActsSzrListResponse GetActsIncomeLists(GridDataReq req)
        {
            return GetActs(req, true);
        }

        public SparePartsInvoiceListResponse GetTmcActsByDateDetails(GridDataReq req)
        {
            return GetTmcActsByDate(req);
        }
        //GetTmcMoveByDateDetails
        public SparePartsInvoiceListResponse GetTmcMoveByDateDetails(GridDataReq req)
        {
            return GetTmcMoveByDate(req);
        }


        //список актов поступления
        public SparePartsInvoiceListResponse GetTmcActsByDate(GridDataReq req)
        {
            var checkOrg = db.Organizations.Where(t => t.org_id == req.OrgId).Select(t => t.tporg_tporg_id).FirstOrDefault();
            var newOrgId = checkOrg == 1 ? (int?)req.OrgId : null;
            if (req.DateStart != null)
            {
                req.DateStart = req.DateStart.Value.AddDays(1);
            }
            var res = new List<SparePartsItem>();
            var resultList = new SparePartsInvoiceListResponse();
            res = (from t in db.TMC_Acts
                   where (req.DateStart != null ? DbFunctions.TruncateTime(t.act_date) >= DbFunctions.TruncateTime(req.DateStart) : true) &&
                   (req.DateEnd != null ? DbFunctions.TruncateTime(t.act_date) <= DbFunctions.TruncateTime(req.DateEnd) : true)
                   && ((req.ContractorsId != null && req.ContractorsId != -1) ? t.contr_id == req.ContractorsId : true) && t.type_act == 1 &&
                   (newOrgId != null ? t.fielplan_fielplan_id == newOrgId : true)
                   select new SparePartsItem
                   {
                       Id = t.tma_id,
                       DateBegin1 = t.act_date.Value,
                       Number = t.act_num,
                       Contractor = new DictionaryItemsDTO { Id = t.contr_id, Name = db.Contractor.Where(v => v.id == t.contr_id).Select(v => v.name).FirstOrDefault() },
                       Storage = new DictionaryItemsDTO { Id = t.emp_emp_id, Name = db.Employees.Where(v => v.emp_id == t.emp_emp_id).Select(v => v.short_name).FirstOrDefault() },
                       Torg12 = t.torg_id,
                   }).OrderByDescending(t => t.DateBegin1).ToList();
            for (int i = 0; i < res.Count; i++)
            {
                res[i].DateBegin = res[i].DateBegin1.ToString("dd.MM.yyyy");
            }

            if (req.CheckSearch != null && req.CheckSearch != "")
            {
                res = res.Where(t => t.Number != null && t.Number.Contains(req.CheckSearch)).ToList();
            }
            var CountItems = res.Count();
            var TotalPage = (double)CountItems / (double)req.PageSize;
            double CountPages = TotalPage < 0 ? 1 : Math.Ceiling(TotalPage);
            var gridData = GridHelpers<SparePartsItem>.GetGridData(res.AsQueryable(), req);
            resultList.Values = gridData.OrderByDescending(m => m.DateBegin1).ToList();
            resultList.CountPages = (int)CountPages;
            resultList.CountItems = CountItems;
            return resultList;
        }
        //move acts
        public SparePartsInvoiceListResponse GetTmcMoveByDate(GridDataReq req)
        {
            var checkOrg = db.Organizations.Where(t => t.org_id == req.OrgId).Select(t => t.tporg_tporg_id).FirstOrDefault();
            var newOrgId = checkOrg == 1 ? (int?)req.OrgId : null;
            if (req.DateStart != null)
            {
                req.DateStart = req.DateStart.Value.AddDays(1);
            }
            var res = new List<SparePartsItem>();
            var resultList = new SparePartsInvoiceListResponse();
            res = (from t in db.TMC_Acts
                   where (req.DateStart != null ? DbFunctions.TruncateTime(t.act_date) >= DbFunctions.TruncateTime(req.DateStart) : true) &&
                   (req.DateEnd != null ? DbFunctions.TruncateTime(t.act_date) <= DbFunctions.TruncateTime(req.DateEnd) : true)
                   && ((req.ContractorsId != null && req.ContractorsId != -1) ? t.contr_id == req.ContractorsId : true) && t.type_act == 3 &&
                   (newOrgId != null ? t.fielplan_fielplan_id == newOrgId : true)
                   select new SparePartsItem
                   {
                       Id = t.tma_id,
                       DateBegin1 = t.act_date.Value,
                       Number = t.act_num,
                       Contractor = new DictionaryItemsDTO { Id = t.emp_utv_id, Name = db.Employees.Where(v => v.emp_id == t.emp_utv_id).Select(v => v.short_name).FirstOrDefault() },
                       Storage = new DictionaryItemsDTO { Id = t.emp_emp_id, Name = db.Employees.Where(v => v.emp_id == t.emp_emp_id).Select(v => v.short_name).FirstOrDefault() },
                       Cost  = db.TMC_Records.Where(b => b.tma_tma_id == t.tma_id).Sum(b => (float?)(b.price * b.count) ?? 0 ) ,
                       Comment = t.comment,
                   }).OrderByDescending(t => t.DateBegin1).ToList();
            for (int i = 0; i < res.Count; i++)
            {
                res[i].DateBegin = res[i].DateBegin1.ToString("dd.MM.yyyy");
                res[i].Cost = (float) Math.Round((decimal)(res[i].Cost ?? 0), 2);
            }

            if (req.CheckSearch != null && req.CheckSearch != "")
            {
                res = res.Where(t => t.Number != null && t.Number.Contains(req.CheckSearch)).ToList();
            }
            var CountItems = res.Count();
            var TotalPage = (double)CountItems / (double)req.PageSize;
            double CountPages = TotalPage < 0 ? 1 : Math.Ceiling(TotalPage);
            var gridData = GridHelpers<SparePartsItem>.GetGridData(res.AsQueryable(), req);
            resultList.Values = gridData.OrderByDescending(m => m.DateBegin1).ToList();
            resultList.CountPages = (int)CountPages;
            resultList.CountItems = CountItems;
            return resultList;
        }

        public DictionarySpareParts GetDictionaryTmc(GridDataReq req)
        {
            var checkOrg = db.Organizations.Where(t => t.org_id == req.OrgId).Select(t => t.tporg_tporg_id).FirstOrDefault();
            var newOrgId = checkOrg == 1 ? (int?)req.OrgId : null;
            var result = new DictionarySpareParts();
            result.StorageList = db.Employees.Where(p => p.deleted != true && (newOrgId != null ? p.org_org_id == newOrgId : true) && p.Posts.pst_id == 7).
            Select(p => new DictionaryItemsDTO { Id = p.emp_id, Name = p.short_name }).ToList();
            result.ContractorsList = db.Contractor.Where(p => p.deleted == false).Select(p => new DictionaryItemsDTO { Id = p.id, Name = p.name }).ToList();
            result.SparePartsList = db.Materials.Where(p => p.deleted == false).Select(p => new DictionaryItemsDTO
            {
                Id = p.mat_id,
                Name = p.name,
             //   Current_price = 
            }).ToList();
            result.OrganzationsList = db.Organizations.Where(p => p.deleted != true &&
                (checkOrg == (int)TypeOrganization.Section ? p.org_id == req.OrgId : p.tporg_tporg_id == (int)TypeOrganization.Section)).
                                Select(t => new DictionaryItemsDTO
                                {
                                    Id = t.org_id,
                                    Name = t.name
                                }).OrderBy(t => t.Name).ToList();
            if (checkOrg == (int)TypeOrganization.Company || req.OrgId == -1)
            {
                result.OrganzationsList.Insert(0, new DictionaryItemsDTO { Id = -1, Name = "Все" });
            }
            return result;
        }

        //данные по акту поступления
        public SparePartsDetails GetActTmcDetails(int ActId)
        {
            var result = db.TMC_Acts.Where(p => p.tma_id == ActId).Select(g => new SparePartsDetails
            {
                Id = g.tma_id,
                ActId = g.tma_id,
                Date = g.act_date,
                Number = g.act_num,
                ContractorsId = g.contr_id,
                StorageId = g.emp_emp_id,
                Torg12 = (g.torg_id == null || g.torg_id == 0) ? false : true,
                SparePartsList = db.TMC_Records.Where(p => p.tma_tma_id == ActId &&
                    p.emp_from_id == 0 && p.emp_to_id != 0).Select(m => new SparePartsModel
                {
                    SpareParts = new DictionaryItemsDTO
                    {
                        Id = (int)m.mat_mat_id,
                        Name = m.Materials.name,
                    },
                    Storage = new DictionaryItemsDTO
                    {
                        Id = (int)m.emp_to_id,
                        Name = db.Employees.Where(v => v.emp_id == m.emp_to_id).Select(v => v.short_name).FirstOrDefault()
                    },
                    Count = m.count,
                    Price = m.price,
                    Cost1 = Math.Round(m.price * m.count, 2),
                    Cost2 = Math.Round(m.nds ?? 0, 2),
                    Cost3 = Math.Round(m.price * m.count + (float)((m.price * m.count) * (m.nds / 100)), 2),
                    Id = m.tmr_id
                }).ToList(),
            }).First();

            for (int i = 0; i < result.SparePartsList.Count; i++)
            {
                result.SparePartsList[i].Number = i + 1;
            }

            return result;
        }
        //movement
        public SparePartsDetails GetActTmcMoveDetails(int ActId)
        {
            var result = db.TMC_Acts.Where(p => p.tma_id == ActId).Select(g => new SparePartsDetails
            {
                Id = g.tma_id,
                ActId = g.tma_id,
                Date = g.act_date,
                Number = g.act_num,
                ContractorsId = g.emp_utv_id,
                StorageId = g.emp_emp_id,
                Name = g.comment,
                SparePartsList = db.TMC_Records.Where(p => p.tma_tma_id == ActId &&
                    p.emp_from_id != 0 && p.emp_to_id != 0).Select(m => new SparePartsModel
                    {
                        SpareParts = new DictionaryItemsDTO
                        {
                            Id = (int)m.mat_mat_id,
                            Name = m.Materials.name,
                        },
                        Count = m.count,
                        Price = m.price,
                        Cost1 = Math.Round(m.price * m.count, 2),
                        Id = m.tmr_id
                    }).ToList(),
            }).First();

            for (int i = 0; i < result.SparePartsList.Count; i++)
            {
                result.SparePartsList[i].Number = i + 1;
            }

            return result;
        }

        public WriteOffActsDetails GetPrintMoveInfo(int? ActId)
        {
            var data = new SparePartsDetails();
            var param = new WriteOffActsDetails();
            data = GetActTmcMoveDetails(ActId.Value);
            param.Act_number = data.Number;
            param.Date1 = data.Date.Value.ToString("dd.MM.yyyy");
            param.ShortName = db.Employees.Where(t => t.emp_id == data.ContractorsId).Select(t => t.short_name).FirstOrDefault(); 
            param.OtvName = db.Employees.Where(t => t.emp_id == data.StorageId).Select(t => t.short_name).FirstOrDefault();
            param.SparePartsList = data.SparePartsList;
            param.SumCost = data.SparePartsList.Sum(t => t.Cost1);
            param.SumCount = data.SparePartsList.Sum(t => t.Count);
            param.SumCost = (double)Math.Round((decimal)param.SumCost, 2);
            param.SumCount = (double)Math.Round((decimal)param.SumCount, 2);
            var idOrg = Int32.Parse(db.Parameters.Where(t => t.name.Equals("NameOrg")).Select(t => t.value).FirstOrDefault());
            param.OrgName = db.Organizations.Where(t => t.org_id == idOrg).Select(t => t.name).FirstOrDefault();
            return param;
        }

        //обновление акта прихода и перемещения
        public string UpdateTmcAct(List<SaveRowSpareParts> req)
        {
            //movement
            if (req[0].TypeAct == 3)
            {
                if (req[0].StorageId == req[0].ContractorsId) { throw new BllException(14, "Отправитель не может быть получателем."); }
                ////проверка на количество
                var check = 0; string message = "Недостаточно материалов на балансе. Перемещение невозможно. Материалы: ";
               var arr = db.TMC_Records.AsEnumerable().Where(t => t.tma_tma_id == req[0].Id &&
                   t.emp_from_id != 0 && t.emp_to_id != 0).ToList();

                for (int i = 0; i < req.Count; i++)
                {
                    var cur = db.TMC_Current.AsEnumerable().Where(t => t.mat_mat_id == req[i].Document.SpareParts.Id &&
                        t.emp_emp_id == req[0].StorageId).FirstOrDefault();

                   var oldDataFrom = arr.Where(t => t.emp_from_id == req[i].StorageId && t.mat_mat_id == req[i].Document.SpareParts.Id).FirstOrDefault();
                   var oldDataTo = arr.Where(t => t.emp_to_id == req[i].StorageId && t.mat_mat_id == req[i].Document.SpareParts.Id).FirstOrDefault();
                   var count = 0.0; var countBalFrom = 0.0; var countBalTo = 0.0;

                   if (oldDataFrom != null) { countBalFrom = oldDataFrom.count; }

                   if (oldDataTo != null) { countBalTo = oldDataTo.count; }

                   if (cur != null) { count = cur.count.Value; }
                   if ((cur == null) || (cur != null && (count + countBalFrom - countBalTo) < req[i].Document.Count))
                    {
                        var id = req[i].Document.SpareParts.Id;
                        check = 1; message = message + db.Materials.Where(t => t.mat_id == id).Select(t => t.name).FirstOrDefault() + ", ";
                    }
                }
                //==
                if (check == 1)
                {
                    throw new BllException(14, message);
                }
            }


            using (var trans = db.Database.BeginTransaction())
            {
                try
                {
                    for (var i = 0; i < req.Count; i++)
                    {
                        var obj = req[i].Document;
                        var updateDic = db.TMC_Records.Where(o => o.tmr_id == obj.Id).FirstOrDefault();

                        if (!String.IsNullOrEmpty(req[i].Document.SpareParts.Id.ToString()) && req[i].Document.Id != 0)
                        {
                            updateDic.mat_mat_id = obj.SpareParts.Id.Value;
                        }
                        if (req[i].Document.Count == null) { req[i].Document.Count = 0; }

                        if (!String.IsNullOrEmpty(req[i].Document.Count.ToString()) && req[i].Document.Id != 0)
                        {
                            updateDic.count = obj.Count.Value;
                        }
                        if (req[i].Document.Price == null) { req[i].Document.Price = 0; }
                        if (!String.IsNullOrEmpty(req[i].Document.Price.ToString()) && req[i].Document.Id != 0)
                        {
                            updateDic.price = obj.Price.Value;
                        }
                        //поступление
                        if (req[0].TypeAct != 3)
                        {
                            if (!String.IsNullOrEmpty(req[i].Document.Storage.Id.ToString()) && req[i].Document.Storage.Id != 0)
                            {
                                updateDic.emp_to_id = obj.Storage.Id.Value;
                            }
                            if (req[i].Document.Cost2 == null) { req[i].Document.Cost2 = 0; }
                            if (!String.IsNullOrEmpty(req[i].Document.Cost2.ToString()) && req[i].Document.Id != 0)
                            {
                                updateDic.nds = (float?)obj.Cost2;
                            }
                        }
                        //перемещение
                        if (req[0].TypeAct == 3)
                        {
                            if (req[0].ContractorsId != null && req[0].ContractorsId != 0)
                            {
                                updateDic.emp_to_id = req[0].ContractorsId.Value;
                            }
                            if (req[0].StorageId != null && req[0].StorageId != 0)
                            {
                                updateDic.emp_from_id = req[0].StorageId.Value;
                            }
                        }

                    }
                    db.SaveChanges();
                    trans.Commit();
                }
                catch (Exception)
                {
                    trans.Rollback();
                    throw;
                }
            }
            return "success";
        }

        //приходная накладная
        public int UpdateTmcGeneralInfo(SparePartsDetails req)
        {
            using (var trans = db.Database.BeginTransaction())
            {
                var updateDic = db.TMC_Acts.Where(o => o.tma_id == req.Id).FirstOrDefault();
                if (!String.IsNullOrEmpty(req.Date.ToString()) && !String.IsNullOrEmpty(req.Id.ToString()))
                {
                    updateDic.act_date = req.Date.Value; //дата
                }
                if (!String.IsNullOrEmpty(req.StorageId.ToString()) && !String.IsNullOrEmpty(req.Id.ToString()))
                {
                    updateDic.emp_emp_id = req.StorageId;  //отправитель
                }
                if (req.Number != null && !String.IsNullOrEmpty(req.Id.ToString()) && !String.IsNullOrEmpty(req.Number.ToString()))
                {
                    updateDic.act_num = req.Number; //номер акта
                }

                if (req.TypeAct != 3)
                {
                    if (!String.IsNullOrEmpty(req.ContractorsId.ToString()) && !String.IsNullOrEmpty(req.Id.ToString()))
                    {
                        updateDic.contr_id = req.ContractorsId;
                    }

                    if (!String.IsNullOrEmpty(req.Torg12.ToString()) && !String.IsNullOrEmpty(req.Id.ToString()))
                    {
                        updateDic.torg_id = req.Torg12 == true ? 1 : 0;
                    }
                }
                //перемещение
                if (req.TypeAct == 3)
                {
                    if (req.ContractorsId != null && req.ContractorsId != 0)
                    {
                        updateDic.emp_utv_id = req.ContractorsId.Value; // получатель
                    }
                    updateDic.comment = req.Name;
                }



                db.SaveChanges();
                trans.Commit();
            }
            return 0;
        }

        //удаление приходной накладной
        public string DeleteTmcAct(List<DelRow> req)
        {
            var check = 0; string message = "Материалы были списаны ранее. Удаление невозможно. Материалы: ";
            var arr = db.TMC_Records.AsEnumerable().Where(t => t.tma_tma_id == req[0].rows.Id &&
                t.emp_from_id == 0 && t.emp_to_id != 0).ToList();

            for (int i = 0; i < arr.Count; i++)
            {
                var cur = db.TMC_Current.AsEnumerable().Where(t => t.mat_mat_id == arr[i].mat_mat_id && t.emp_emp_id == arr[i].emp_to_id).FirstOrDefault();
                if (cur == null && arr[i].count != 0 && arr[i].price != 0)
                {
                    check = 1;
                }
                else
                {
                    if (cur != null && cur.count < arr[i].count)
                    {
                        var id = arr[i].mat_mat_id;
                        check = 1; message = message + db.Materials.Where(t => t.mat_id == id).Select(t => t.name).FirstOrDefault() + ", ";
                    }
                }
            }

            if (check == 0)
            {
                using (var trans = db.Database.BeginTransaction())
                {
                    var n = req[0].rows.Id;
                    var doc = db.TMC_Acts.Where(s => s.tma_id == n).FirstOrDefault();
                    if (doc == null)
                        throw new BllException(15, "Документ не был найден , обновите страницу");
                    db.TMC_Acts.Remove(doc);

                    var doc2 = db.TMC_Records.Where(s => s.tma_tma_id == n).ToList();
                    if (doc2.Count != 0)
                    {
                        db.TMC_Records.RemoveRange(doc2);
                    }
                    db.SaveChanges();
                    trans.Commit();
                }
                return "success";
            }
            else { return message; }
        }

        public int CreateUpdateTmcMove(List<SaveRowSpareParts> req)
        {

            if (req[0].StorageId == req[0].ContractorsId) { throw new BllException(14, "Отправитель не может быть получателем."); }
            ////проверка на количество
            var check = 0; string message = "Недостаточно материалов на балансе. Перемещение невозможно. Материалы: ";
            //var arr = db.TMC_Records.AsEnumerable().Where(t => t.tma_tma_id == req[0].rows.Id &&
            //    t.emp_from_id == 0 && t.emp_to_id != 0).ToList();

            for (int i = 0; i < req.Count; i++)
            {
                var cur = db.TMC_Current.AsEnumerable().Where(t => t.mat_mat_id == req[i].Document.SpareParts.Id &&
                    t.emp_emp_id == req[0].StorageId).FirstOrDefault();
                    if ((cur == null ) || (cur != null && cur.count < req[i].Document.Count))
                    {
                        var id = req[i].Document.SpareParts.Id;
                        check = 1; message = message + db.Materials.Where(t => t.mat_id == id).Select(t => t.name).FirstOrDefault() + ", ";
                    }
            }
            //==
            //новый акт
            if (check == 1) {
                throw new BllException(14, message);
            }

            using (var trans = db.Database.BeginTransaction())
            {
                if (!String.IsNullOrEmpty(req[0].Id.ToString()))
                {
                    var id = req[0].Id;
                    var updateDic = db.TMC_Acts.Where(o => o.tma_id == id).FirstOrDefault();
                    updateDic.act_num = req[0].Number; //номер акта
                }
                if (String.IsNullOrEmpty(req[0].Id.ToString()))
                {
                    var newDic = new TMC_Acts
                    {
                        tma_id = CommonFunction.GetNextId(db, "TMC_Acts"),
                        act_date = req[0].Date.Value,
                        act_num = req[0].Number,
                        emp_emp_id = req[0].StorageId, //отправитель
                        type_act = 3,
                        fielplan_fielplan_id = req[0].OrganizationId,
                        soil_soil_id = 0,
                        emp_utv_id = req[0].ContractorsId, //получатель
                        comment = req[0].Name,
                    };
                    db.TMC_Acts.Add(newDic);
                }
                db.SaveChanges();
                trans.Commit();
            }
            using (var trans = db.Database.BeginTransaction())
            {
                if (String.IsNullOrEmpty(req[0].Pk.ToString()) || req[0].Pk == 0)
                {
                    var list = db.Database.SqlQuery<BalanceMaterials>(
                    string.Format(@"select max(tma_id) as Id from TMC_Acts")).ToList();
                    key = list[0].Id;
                }
                if (!String.IsNullOrEmpty(req[0].Pk.ToString()) && req[0].Pk != 0)
                {
                    key = req[0].Pk;
                }

                for (int i = 0; i < req.Count; i++)
                {
                    if (req[i].Document != null)
                    {
                        var newDic1 = new TMC_Records
                        {
                            tmr_id = CommonFunction.GetNextId(db, "TMC_Records"),
                            mat_mat_id = req[i].Document.SpareParts.Id.Value,
                            act_date = (DateTime)req[i].Date,
                            count = (float)req[i].Document.Count,
                            price = (float)req[i].Document.Price,
                            emp_from_id = req[i].StorageId,
                            emp_to_id = req[i].ContractorsId.Value,
                            tma_tma_id = key,
                            cont_cont_id = 0 ,
                            deleted = true,
                            area = 0,
                        };
                        db.TMC_Records.Add(newDic1);
                    }
                }

                try
                {
                    db.SaveChanges();
                }
                catch (DbEntityValidationException e)
              {
                 foreach (var eve in e.EntityValidationErrors)
                 {
                     Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                     eve.Entry.Entity.GetType().Name, eve.Entry.State);
                 foreach (var ve in eve.ValidationErrors)
                    {
                     Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",  ve.PropertyName, ve.ErrorMessage);
                  }
                   }
                   throw;
                   }
                trans.Commit();
            }

            return (int)key;
        }
        //==



        //приходная накладная создание
        public int CreateUpdateTmcDetail(List<SaveRowSpareParts> req)
        {
            //новый акт
            using (var trans = db.Database.BeginTransaction())
            {
                if (!String.IsNullOrEmpty(req[0].Id.ToString()))
                {
                    var id = req[0].Id;
                    var updateDic = db.TMC_Acts.Where(o => o.tma_id == id).FirstOrDefault();
                    updateDic.act_num = req[0].Number; //номер акта
                }
                if (String.IsNullOrEmpty(req[0].Id.ToString()))
                {
                    var newDic = new TMC_Acts
                    {
                        tma_id = CommonFunction.GetNextId(db, "TMC_Acts"),
                        act_date = req[0].Date.Value,
                        act_num = req[0].Number,
                        contr_id = req[0].ContractorsId,
                        emp_emp_id = req[0].StorageId,
                        torg_id = req[0].Torg12 == true ? 1 : 0,
                        type_act = 1,
                        fielplan_fielplan_id = req[0].OrganizationId,
                        soil_soil_id = 0,
                        emp_utv_id = 0,
                    };
                    db.TMC_Acts.Add(newDic);
                }
                db.SaveChanges();
                trans.Commit();
            }
            using (var trans = db.Database.BeginTransaction())
            {
                if (String.IsNullOrEmpty(req[0].Pk.ToString()) || req[0].Pk == 0)
                {
                    var list = db.Database.SqlQuery<BalanceMaterials>(
                    string.Format(@"select max(tma_id) as Id from TMC_Acts")).ToList();
                    key = list[0].Id;
                }
                if (!String.IsNullOrEmpty(req[0].Pk.ToString()) && req[0].Pk != 0)
                {
                    key = req[0].Pk;
                }

                for (int i = 0; i < req.Count; i++)
                {
                    if (req[i].Document != null)
                    {
                        var newDic1 = new TMC_Records
                        {
                            tmr_id = CommonFunction.GetNextId(db, "TMC_Records"),
                            mat_mat_id = req[i].Document.SpareParts.Id.Value,
                            nds = (float?)req[i].Document.Cost2.Value,
                            act_date = (DateTime)req[i].Date,
                            count = (float)req[i].Document.Count,
                            price = (float)req[i].Document.Price,
                            emp_from_id = 0,
                            emp_to_id = req[i].Document.Storage.Id ?? 0,
                            tma_tma_id = key
                        };
                        db.TMC_Records.Add(newDic1);
                    }
                }
                db.SaveChanges();
                trans.Commit();
            }
            return (int)key;
        }

        //удаление транзакций из актов поступления
        public string DeleteTmcTransactions(List<SparePartsModel> req)
        {
            var check = 0; string massage = "Материалы были списаны ранее. Удаление невозможно. Материалы: ";

            if (req[0].Pk != 3)
            {
                for (int i = 0; i < req.Count; i++)
                {
                    if (!String.IsNullOrEmpty(req[i].Id.ToString()))
                    {
                        var cur = db.TMC_Current.AsEnumerable().Where(t => t.mat_mat_id == req[i].SpareParts.Id &&
                            t.emp_emp_id == req[i].Storage.Id).FirstOrDefault();
                        if (cur == null && req[i].Count != 0 && req[i].Price != 0)
                        {
                            check = 1;
                        }
                        else
                        {
                            if (cur != null && cur.count < req[i].Count)
                            {
                                check = 1; massage = massage + req[i].SpareParts.Name + ", ";
                            }
                        }
                    }
                }
            }

            if (check == 0)
            {
                using (var trans = db.Database.BeginTransaction())
                {
                    for (int i = 0; i < req.Count; i++)
                    {
                        if (!String.IsNullOrEmpty(req[i].Id.ToString()))
                        {
                            var n = req[i].Id;
                            var doc = db.TMC_Records.Where(s => s.tmr_id == n).FirstOrDefault();
                            if (doc == null)
                                throw new BllException(15, "Документ не был найден , обновите страницу");
                            db.TMC_Records.Remove(doc);
                        }
                    }
                    db.SaveChanges();
                    trans.Commit();
                }
                return "success";
            }
            else { return massage; }
        }

        //получение актов списания
        private GetActsSzrListResponse GetActs(GridDataReq req, bool income)
        {
            var checkOrg = db.Organizations.Where(t => t.org_id == req.OrgId).Select(t => t.tporg_tporg_id).FirstOrDefault();
            var newOrgId = checkOrg == 1 ? (int?)req.OrgId : null;
            var resultList = new GetActsSzrListResponse();
            var list = db.Database.SqlQuery<GetActsSzr>(
             string.Format(@"select tma_id as Id, Organizations.name as Organization ,Employees.short_name as RecipientName, act_num as Number,
             act_date as DateT, Type_soil.name as SoilTypeName, year as Year, Organizations.org_id as OrgId, type_act as TypeAct
             from Organizations, Employees, TMC_Acts,Type_soil where Organizations.org_id=Employees.org_org_id and  type_act = 2
             and TMC_Acts.emp_utv_id=Employees.emp_id and Type_soil.soil_id=TMC_Acts.soil_soil_id order by  CONVERT(INT, TMC_Acts.act_num)  DESC,
             act_date Desc")).ToList();

            for (int i = 0; i < list.Count; i++)
            {
                list[i].DateBegin = list[i].DateT.ToString("dd.MM.yyyy");
                var idAct = list[i].Id;
                if (db.TMC_Records.Where(t => t.tma_tma_id == idAct).Count() != 0)
                {
                    list[i].Cost1 = db.TMC_Records.Where(t => t.tma_tma_id == idAct).Select(t => t.count * t.price).Sum();
                }
                list[i].Cost1 = (float) Math.Round((double?)list[i].Cost1 ?? 0 , 2);

            }
            list = list.Where(t => (newOrgId != null ? t.OrgId == newOrgId : true) && t.TypeAct == 2 ).ToList();
            if (req.CheckSearch != null && req.CheckSearch != "")
            {
                list = list.Where(t => t.Number != null && t.Number.Contains(req.CheckSearch)).ToList();
            }

            var CountItems = list.Count();
            var TotalPage = (double)CountItems / (double)req.PageSize;
            double CountPages = TotalPage < 0 ? 1 : Math.Ceiling(TotalPage);

            var gridData = GridHelpers<GetActsSzr>.GetGridData(list.AsQueryable(), req);
            resultList.Values = gridData.ToList();
            resultList.CountPages = (int)CountPages;
            resultList.CountItems = CountItems;
            return resultList;

        }

        //данные по акту списания
        public ActSzrDetails GetActMaterialsDetails(int ActId)
        { 
                var result = db.TMC_Acts.Where(p => p.tma_id == ActId).Select(g => new ActSzrDetails
                  {
                      Id = g.tma_id,
                      Date = g.act_date,
                      Year = g.year,
                      RecipientId = g.emp_emp_id,
                      ResponsibleId = g.emp_utv_id,
                      Number = g.act_num,
                      OrganizationId = g.fielplan_fielplan_id,
                      SoilTypeId = g.soil_soil_id,
                      MaterialsList = g.TMC_Records.Where(p => p.tma_tma_id == ActId).Select(m => new ActMaterials
                               {
                                   Material = new MaterialDto
                                   {
                                       Id = (int)m.mat_mat_id,
                                       Name = m.Materials.name,
                                       Price = m.Materials.current_price,
                                       Balance = m.Materials.balance,
                                   },
                                   Plant = new PlantDto { Id = m.cont_cont_id != -1 ? m.Fields_to_plants.plant_plant_id : -1, 
                                                        Name = m.cont_cont_id != -1 ? m.Fields_to_plants.Plants.name  : "Культура" },
                                   Field = new FieldDto { Id = m.cont_cont_id != -1 ? m.Fields_to_plants.fielplan_id : m.fielplan_fielplan_id, 
                                   Name = m.cont_cont_id != -1 ?  m.Fields_to_plants.Fields.name : db.Fields.Where(t => t.fiel_id == m.fielplan_fielplan_id ).Select(t => t.name ).FirstOrDefault()   },
                                   Area = m.area,
                                   Count = m.count,
                                   Cost = m.price * m.count,
                                   ConsumptionRate = m.area!=0  ? (float)m.count / m.area : 0 , //норма расхода 
                                   Price = m.price,
                                   Id = m.tmr_id, 
                               }).OrderBy(t => t.Id).ToList(),
                  }).FirstOrDefault();
                for (int i = 0; i < result.MaterialsList.Count; i++)
                {
                    if (result.MaterialsList[i].Cost != 0) { result.MaterialsList[i].Cost = Single.Parse(result.MaterialsList[i].Cost.ToString("#.##")); }
                    if (result.MaterialsList[i].ConsumptionRate < 0.01) { result.MaterialsList[i].ConsumptionRate = 0; }
                    else
                    {
                        result.MaterialsList[i].ConsumptionRate = Single.Parse(result.MaterialsList[i].ConsumptionRate.ToString("#.##"));
                    }
                }
            //checkRes
                //var employees = new List<int?>();
                //var wLDb = new WaysListsDb();
                //employees.Add(result.RecipientId);
                //employees.Add(result.ResponsibleId);
                //var lst = new CreateWaysListReq();
                //lst.OrgId = result.OrganizationId;
                //result.AllResource = wLDb.checkAllResource(lst, false, employees, false, false, false);
             //
              return result;
        }


        public int UpdateGeneralInfo(ActSzrDetails req)
        {
            using (var trans = db.Database.BeginTransaction())
            {
                if (!String.IsNullOrEmpty(req.Date.ToString()) && !String.IsNullOrEmpty(req.Id.ToString()))
                {
                    var updateDic = db.TMC_Acts.Where(o => o.tma_id == req.Id).FirstOrDefault();
                    updateDic.act_date = req.Date; //дата
                }
                if (!String.IsNullOrEmpty(req.Year.ToString()) && !String.IsNullOrEmpty(req.Id.ToString()))
                {
                    var updateDic = db.TMC_Acts.Where(o => o.tma_id == req.Id).FirstOrDefault();
                    updateDic.year = req.Year; //год урожая 
                }
                if (!String.IsNullOrEmpty(req.SoilTypeId.ToString()) && !String.IsNullOrEmpty(req.Id.ToString()))
                {
                    var updateDic = db.TMC_Acts.Where(o => o.tma_id == req.Id).FirstOrDefault();
                    updateDic.soil_soil_id = req.SoilTypeId; //вид земель
                }
                if (!String.IsNullOrEmpty(req.RecipientId.ToString()) && !String.IsNullOrEmpty(req.Id.ToString()))
                {
                    var updateDic = db.TMC_Acts.Where(o => o.tma_id == req.Id).FirstOrDefault();
                    updateDic.emp_emp_id = req.RecipientId; //получатель
                }
                if (!String.IsNullOrEmpty(req.ResponsibleId.ToString()) && !String.IsNullOrEmpty(req.Id.ToString()))
                {
                    var updateDic = db.TMC_Acts.Where(o => o.tma_id == req.Id).FirstOrDefault();
                    updateDic.emp_utv_id = req.ResponsibleId; //утвердитель
                }
                if (!String.IsNullOrEmpty(req.Number.ToString()) && !String.IsNullOrEmpty(req.Id.ToString()))
                {
                    //проверка номера акта
                    var list2 = db.Database.SqlQuery<ActSzrDetails>(string.Format(@"select act_num as Name from TMC_Acts where type_act=2 and
                    tma_id <>" + req.Id + "")).ToList();
                    for (int i = 0; i < list2.Count; i++)
                    {
                        if (list2[i].Name.Equals(req.Number)) { throw new BllException(14, "Акт списания с таким номером уже существует"); }
                    }

                    var updateDic = db.TMC_Acts.Where(o => o.tma_id == req.Id).FirstOrDefault();
                    updateDic.act_num = req.Number; //номер акта
                }
                db.SaveChanges();
                trans.Commit();
            }
        return 0;
            }

        public int CreateUpdateActSzrDetail(List<SaveRow> req)
        {

            for (int i = 0; i < req.Count; i++)
            {
                var balance = db.TMC_Current.AsEnumerable().Where(t => t.mat_mat_id == req[i].Document.Material.Id &&
                                t.emp_emp_id == req[i].ResponsibleId).Select(t => t.count).FirstOrDefault();
                if (balance == null) { balance = 0; }
                if (balance < req[i].Document.Count) { throw new BllException(14, "Недостаточно материалов на балансе."); }
            }


            //новый акт
            using (var trans = db.Database.BeginTransaction())
            {
                if (!String.IsNullOrEmpty(req[0].Number.ToString()) && !String.IsNullOrEmpty(req[0].Id.ToString()))
                {
                  //проверка номера акта
                    var list2 = db.Database.SqlQuery<ActSzrDetails>(string.Format(@"select act_num as Name from TMC_Acts where type_act=1 and  
                       tma_id <>" + req[0].Id)).ToList();
                    for (int i = 0; i < list2.Count; i++)
                    {
                        if (list2[i].Name.Equals(req[0].Number)) { throw new BllException(14, "Акт списания с таким номером уже существует"); }
                    }
                    var updateDic = db.TMC_Acts.Where(o => o.tma_id == req[0].Id).FirstOrDefault();
                    updateDic.act_num = req[0].Number; //номер акта
                }
                if (String.IsNullOrEmpty(req[0].Id.ToString()) && req[0].Document.Pk != -1)
                {
                    var Pk2 = req[0].Pk;
                    var list2 = db.TMC_Acts.Where(t => t.tma_id != Pk2 && t.type_act == 2).Select(t => new ActSzrDetails { Name = t.act_num }).ToList();
                    for (int i = 0; i < list2.Count; i++)
                    {
                        if (list2[i].Name.Equals(req[0].Number)) { throw new BllException(14, "Акт списания с таким номером уже существует"); }
                    }
                    if (Pk2 == null)
                    {
                        var newDic = new TMC_Acts
                        {
                            tma_id = CommonFunction.GetNextId(db, "TMC_Acts"),
                            act_date = req[0].Date,
                            act_num = req[0].Number,
                            year = req[0].Year,
                            soil_soil_id = req[0].SoilTypeId,
                            emp_emp_id = req[0].RecipientId, //получатель
                            emp_utv_id = req[0].ResponsibleId, //утвердитель
                            fielplan_fielplan_id = req[0].OrganizationId,
                            type_act = 2,
                        };
                        db.TMC_Acts.Add(newDic);
                    }
                }
                db.SaveChanges();
                trans.Commit();
            ;}
            using (var trans = db.Database.BeginTransaction())
            {
                if (String.IsNullOrEmpty(req[0].Pk.ToString()) || req[0].Pk==0)
                {
                    var list = db.Database.SqlQuery<BalanceMaterials>(
              string.Format(@"select max(tma_id) as Id from [TMC_Acts]")).ToList();
                    key = list[0].Id;
                    ;
                }
                if (!String.IsNullOrEmpty(req[0].Pk.ToString()) && req[0].Pk!=0)
                {
                    key = req[0].Pk;
                }
           
                     //
                for (int i = 0; i < req.Count; i++)
                    {
                         var newDic1 = new TMC_Records
                         {
                             tmr_id = CommonFunction.GetNextId(db, "TMC_Records"),
                             mat_mat_id = req[i].Document.Material.Id,
                             cont_cont_id = req[i].Document.Plant.Id.Value,
                             fielplan_fielplan_id = req[i].Document.Field.Id, //поле, культура, год урожая
                             act_date = (DateTime)req[i].Date,
                             count = (float)req[i].Document.Count,
                             price = (float)req[i].Document.Price,
                             area = (float)req[i].Document.Area,
                             emp_from_id = (int)req[i].ResponsibleId,
                             emp_to_id = 0,
                             tma_tma_id = key
                         };
                         db.TMC_Records.Add(newDic1);
                        }
                     db.SaveChanges();
                     trans.Commit();
                 }
            return (int)key;
        }
        
        public string DeleteTransaction(List<ModelUpdateRow> req)
        {
            using (var trans = db.Database.BeginTransaction())
            {

                for (int i = 0; i < req.Count; i++)
                {
                    if (!String.IsNullOrEmpty(req[i].id.ToString()))
                    {
                        var n = req[i].Id;
                        var doc = db.TMC_Records.Where(s => s.tmr_id == n).FirstOrDefault();
                        if (doc == null)
                            throw new BllException(15, "Документ не был найден , обновите страницу");
                        db.TMC_Records.Remove(doc);
                    }
                }
                db.SaveChanges();
                trans.Commit();
            }
            return "success";
        }

        //update rows списание тмц
        public string UpdateRow(List<UpdateRow> req)
        {
            for (var i = 0; i < req.Count; i++)
            {

               var balance = db.TMC_Current.AsEnumerable().Where(t => t.mat_mat_id == req[i].Document.Material.Id && 
                    t.emp_emp_id == req[i].ResponsibleId).Select(t => t.count).FirstOrDefault();
               var oldCount = db.TMC_Records.AsEnumerable().Where(t => t.tmr_id == req[i].Document.Id && t.mat_mat_id == req[i].Document.Material.Id &&
                   t.emp_from_id == req[i].ResponsibleId).Select(t => t.count).FirstOrDefault();

               if (balance == null) { balance = 0;}
               if (oldCount == null) { oldCount = 0; }
               if ((balance + oldCount) < req[i].Document.count) { throw new BllException(14, "Недостаточно материалов на балансе."); }
            }

            using (var trans = db.Database.BeginTransaction())
            {
                try
                {
                    for (var i = 0; i < req.Count; i++)
                    {
                         var obj = req[i].Document;
                        var updateDic = db.TMC_Records.Where(o => o.tmr_id == obj.Id).FirstOrDefault();
                        if (!String.IsNullOrEmpty(req[i].Document.Material.Id.ToString()) && req[i].Document.Id != 0)
                        {
                      if (updateDic != null)
                      {
                          updateDic.mat_mat_id = obj.Material.Id; //материал
                      }
                     }
                        if (!String.IsNullOrEmpty(req[i].Document.area.ToString()) && req[i].Document.Id != 0)
                        {
                            if (updateDic != null)
                            {
                                updateDic.area = obj.area; //площадь
                            }
                        }

                        if (req[i].ResponsibleId != null) {
                            updateDic.emp_from_id = req[i].ResponsibleId;
                        }

                        if (!String.IsNullOrEmpty(req[i].Document.count.ToString()) && req[i].Document.Id != 0)
                        {
                            if (updateDic != null)
                            {
                                updateDic.count = obj.count; //количество
                            }
                        }
                        if (!String.IsNullOrEmpty(req[i].Document.price.ToString()) && req[i].Document.Id != 0)
                        {
                            if (updateDic != null)
                            {
                                updateDic.price = obj.price; //цена
                            }
                        }
                        if (!String.IsNullOrEmpty(req[i].Document.Field.Id.ToString()) && req[i].Document.Id != 0 && 
                            (!String.IsNullOrEmpty(req[i].Document.Plant.Id.ToString())))
                        {
                            if (updateDic != null)
                            {
                                updateDic.fielplan_fielplan_id = req[i].Document.Field.Id; //поле и культура
                                updateDic.cont_cont_id = req[i].Document.Plant.Id.Value;
                            }
                        }


                }
                    db.SaveChanges();
                    trans.Commit();
                }
                catch (Exception)
                {
                    trans.Rollback();
                    throw;
                }
            }
            return "success2";
        } 
       

        //
        public DictionatysActSzr GetDictionaryForActs(ActSzrDetails1 req)
        {
            var checkOrg = db.Organizations.Where(t => t.org_id == req.OrgId).Select(t => t.tporg_tporg_id).FirstOrDefault();
            var newOrgId = (checkOrg == 1 && req.OrgId != - 1)  ? (int?)req.OrgId : null;
            //int? checkRes = null; var wLDb = new WaysListsDb();
            //if (req.ActId != null)
            //{
            //    var act = GetActMaterialsDetails((int)req.ActId);
            //    var employees = new List<int?>();
            //    employees.Add(act.RecipientId); 
            //    employees.Add(act.ResponsibleId); 
            //    var lst = new CreateWaysListReq();
            //    lst.OrgId = act.OrganizationId;
            //    checkRes = (wLDb.checkAllResource(lst, false, employees, false, false, false) == true) ? 1 : 0;
            //}
            var result = new DictionatysActSzr();
            result.TypeMaterials = db.Type_materials.Where(p => p.deleted != true).Select(p => new DictionaryItemsTypeMaterials
                           {
                               Id = p.type_id,
                               Name = p.name,
                               Materials = p.Materials.Where(m => m.deleted != true).Select(m => new DictionaryItemsDTO
                               { Id = m.mat_id, Name = m.name, Current_price = m.current_price }).ToList()
                           }).ToList();
            result.EmployeesList1 = db.Employees.Where(p => p.deleted != true).Where(p => p.Posts.pst_id == 7 && 
                ((newOrgId != null) ? p.org_org_id == newOrgId : true) )
                .Select(p => new DictionaryItemsDTO { Id = p.emp_id, Name = p.short_name }).ToList();

            result.EmployeesList2 = db.Employees.Where(p => p.deleted != true).Where(p => p.Posts.pst_id == 9).
                Select(p => new DictionaryItemsDTO { Id = p.emp_id, Name = p.short_name }).ToList();
            result.EmployeesList2.AddRange(result.EmployeesList1);

            result.TypeSoilsList = db.Type_soil.Select(p => new DictionaryItemsDTO { Id = p.soil_id, Name = p.name }).ToList();
            result.MaterialsList = db.TMC_Current.Select(p => new DictionaryItemsDTO { Id = p.mat_mat_id, Name = p.Materials.name}).ToList();
            result.AllFields = db.Fields.Where(p => p.deleted != true && ((newOrgId != null) ? p.org_org_id == newOrgId : true)).
                Select(p => new DictionaryItemsDTO { Id = p.fiel_id, Name = p.name, Area_plant = p.area }).ToList();

            var values = wayList.GetSelectedPlants(); 
            result.FieldsAndPlants = (from f in db.Fields_to_plants
                                      where f.status == (int)StatusFtp.Approved && ((f.date_ingathering.Year == DateTime.Now.Year && f.plant_plant_id != 36) ||
                                          values.Contains(f.fielplan_id)) && ((newOrgId != null) ? f.Fields.org_org_id == newOrgId : true)
                                      group new DictionaryPlantItemsDTO
                                      {
                                          Id = f.fielplan_id,
                                          Name = f.Fields.name,
                                          Area_plant = f.area_plant
                                      } by new
                                      {
                                          f.plant_plant_id,
                                          f.Plants.name,
                                          f.date_ingathering,
                                      } into g
                                      select new DictionaryItemForFields
                                      {
                                          Id = g.Key.plant_plant_id,
                                          Name = g.Key.name + " (" + g.Key.date_ingathering.Year + ")",
                                          Values = g.ToList()
                                      }).OrderBy(t => t.Name).ToList();

            //культура
            var cult = new DictionaryItemForFields
            {
                Id = -1,
                Name = "Культура",
                Values = db.Fields.Where(f => f.type == 1 && f.deleted != true && ((newOrgId != null) ? f.org_org_id == newOrgId : true)).
                             Select(t => new DictionaryPlantItemsDTO
                             {
                                 Name = t.name,
                                 Id = t.fiel_id,
                             }).OrderBy(t => t.Name).ToList(),
            };
            result.FieldsAndPlants.Add(cult);

            result.OrganzationsList = db.Organizations.Where(p => p.deleted != true &&
                (checkOrg == (int)TypeOrganization.Section ? p.org_id == req.OrgId : p.tporg_tporg_id == (int)TypeOrganization.Section)).
                                Select(t => new DictionaryItemsDTO
                                {
                                    Id = t.org_id,
                                    Name = t.name
                                }).OrderBy(t => t.Name).ToList();
            if (checkOrg == (int)TypeOrganization.Company || req.OrgId == -1)
            {
                result.OrganzationsList.Insert(0, new DictionaryItemsDTO { Id = -1, Name = "Все" });
            }
            var curYear = DateTime.Now.Year;
            result.YearList = new List<DictionaryItemsDTO>();
            for (var i = curYear - 5; i < curYear + 5; i++)
            {
                result.YearList.Add(new DictionaryItemsDTO { Id = i, Name = i.ToString() });
            }
            return result;
        }
        //
        public DictionatysActSzr GetDictionaryForActs1(BalanceMaterials req)
        {
            var checkOrg = db.Organizations.Where(t => t.org_id == req.OrgId).Select(t => t.tporg_tporg_id).FirstOrDefault();
            var newOrgId = checkOrg == 1 ? (int?)req.OrgId : null;
            var result = new DictionatysActSzr();

            result.EmployeesList = db.Employees.Where(p => p.deleted != true && (newOrgId != null ? p.org_org_id == newOrgId : true) && p.Posts.pst_id == 7).
            Select(p => new DictionaryItemsDTO { Id = p.emp_id, Name = p.short_name }).ToList();

            result.MaterialsList = db.Materials.Where(p => p.deleted != true).
                Select(p => new DictionaryItemsDTO { Id = p.mat_id, Name = p.name }).ToList();
            return result;
        }
        //
        public PrintActInfo GetPrintActInfo(int actId)
        {
            var materialsInfo = new List<PrintActMaterials>();
            var result = db.TMC_Acts.Where(a => a.tma_id == actId).
                Select(a => new PrintActInfo
                {
                    DateD = a.act_date.Value.Day,
                    DateT = a.act_date.Value,
                    DateY = a.act_date.Value.Year,
                    Year = a.year,
                    Number = a.act_num
                }).
                FirstOrDefault();

            var list = db.Database.SqlQuery<ActSzrDetails>(
          string.Format(@"select  Organizations.name as Name , Type_soil.name as Number
             from Organizations, Employees, TMC_Acts,Type_soil where Organizations.org_id=Employees.org_org_id 
             and TMC_Acts.emp_utv_id=Employees.emp_id and Type_soil.soil_id=TMC_Acts.soil_soil_id and  tma_id=" + actId + "")).ToList();
            result.SoilTypeName = list[0].Number;
            result.Organization = list[0].Name;
             list = db.Database.SqlQuery<ActSzrDetails>(
         string.Format(@"select  Employees.short_name as Name from Employees,TMC_Acts where TMC_Acts.emp_emp_id=Employees.emp_id and  tma_id=" + actId + "")).ToList();
             result.ResponsibleName = list[0].Name;
             list = db.Database.SqlQuery<ActSzrDetails>(
      string.Format(@"select  Employees.short_name as Name from Employees,TMC_Acts where TMC_Acts.emp_utv_id=Employees.emp_id and  tma_id=" + actId + "")).ToList();
             result.RecipientName = list[0].Name;
            result.DateM = result.DateT.ToString("MM");

            if (result == null)
             throw new BllException(15, "Акт списания СРЗ не был найден");
            //////////////все данные акта
            var alldata = (from m in db.TMC_Records
                           where m.tma_tma_id == actId

                           select new PrintActMaterialsElement
                           {
                               tmr_id = m.tmr_id,
                               fielplan_id = m.fielplan_fielplan_id,
                               Area = m.area,
                               Count = m.count,
                               Summ = m.count*m.price,
                               mat_mat_id = m.mat_mat_id,
                           }).ToList();
            // fielplan_id
            var pl = (from l in db.TMC_Records
                      where l.tma_tma_id == actId
                      select new
                      {
                          Id = l.Fields_to_plants.fielplan_id,
                          Tmr_id = l.tmr_id,
                          Name = l.Fields_to_plants.Plants.name + " " + l.Fields_to_plants.Fields.name,
                          NameFields = l.Fields_to_plants.Fields.name,
                          mat_mat_id = l.mat_mat_id,
                      }).OrderBy(t => t.Id).ToList();

            for (int i = 1; i < pl.Count; i++)
            {
                if (pl[i].Id == pl[i - 1].Id) { pl.RemoveAt(i); i--; }
            }

            result.PlantAndFields = pl.Select(l => l.Name).ToList();

            var ids2 = pl.Select(l => l.Id).ToList();
            var con = 0;
            result.Materials = GetInfoForPrintList(ids2, actId, alldata);
            //удаляем выведенное
            for (int i = 0; i < ids2.Count; i++)
            {
                if (con == 4) { break; }
                ids2.RemoveAt(i); i--;
                con++;
            }
            result.Materials2 = GetInfoForPrintList(ids2, actId, alldata);
          
            return result;
        }


        public List<PrintActMaterials> GetInfoForPrintList(List<int> ids2,int actId, List<PrintActMaterialsElement> alldata) {
            var mater = new List<int?>();
          var  materialsInfo = new List<PrintActMaterials>();
            //материалы 
            for (int i = 0; i < ids2.Count; i++)
            {
                if (i == 4) { break; }
                var id3 = ids2[i];
                var ls = db.TMC_Records.Where(t => t.tma_tma_id == actId && t.fielplan_fielplan_id == id3).Select(t => t.mat_mat_id).ToList();
                if (ls.Count != 0)
                {
                    mater.AddRange(ls);
                }
            }
            mater = mater.Distinct().ToList();
            //записываем 
            for (int i = 0; i < mater.Count; i++)
            {
                var material = new PrintActMaterials();
                var plantsValues = new List<PrintActMaterialsElement>();
                var idname = mater[i];
                material.Name = db.Materials.Where(m => m.mat_id == idname).Select(m => m.name).FirstOrDefault();
                for (int j = 0; j < ids2.Count; j++)
                {
                    if (j == 4) { break; }
                    var mat = new PrintActMaterialsElement();
                    var idfiel = ids2[j]; //id связки
                    var inf = alldata.Where(t => t.mat_mat_id == idname && t.fielplan_id == idfiel).FirstOrDefault();
                    if (inf != null)
                    {
                        mat.Area = inf.Area;
                        mat.Count = inf.Count;
                        mat.Summ = inf.Summ;
                    }
                    plantsValues.Add(mat);
                }
                material.PlantsValue = plantsValues;
                materialsInfo.Add(material);
            }
         return materialsInfo;
        }

        public InfoIncomingFuelModel GetInfoForCreateIncomingFuel()
        {

            InfoIncomingFuelModel result = new InfoIncomingFuelModel();
            result.Fuels = (from f in db.Type_fuel
                            select new FuelDictionary
                            {
                                Id = f.id,
                                Name = f.name,
                                type_salary_doc = new DictionaryItemsDTO
                                {
                                    Id = f.tpsal_tpsal_id,
                                    Name = f.Type_salary_doc.short_name
                                }
                            }).ToList();

            result.Contractors = (from c in db.Contractor
                                  select new DictionaryItemsDTO
                                  {
                                      Id = c.id,
                                      Name = c.name
                                  }).ToList();
            result.VATs = (from v in db.VAT
                           select new DictionaryItemsDTO
                           {
                               Id = v.id,
                               Name = v.vat_value.ToString()
                           }).ToList();
            return result;
        }

        public bool PostIncomeFuel(List<IncomeFuel> incomes)
        {
            using (var trans = db.Database.BeginTransaction())
            {
                try
                {
                    foreach (var a in incomes)
                    {
                        var act = new Acts_fuel_income
                        {
                            id_type_fuel = (int)a.Fuel.Id,
                            price = a.Price,
                            count = a.Count,
                            cost = a.Cost,
                            comment = a.Comment,
                            date = a.Date,
                            id_contractor = a.Contractor != null ? a.Contractor.Id : null,
                            id_vat = a.VAT != null ? a.VAT.Id : null,
                            id_tpsal = a.Type_salary_doc != null ? a.Type_salary_doc.Id : null
                        };

                        var recountMaterial = (from m in db.Type_fuel
                                               where m.id == act.id_type_fuel
                                               select m).FirstOrDefault();
                        var cost = (float?)recountMaterial.balance * recountMaterial.current_price;
                        recountMaterial.balance += act.count;
                        var newCost = cost + (float)act.cost;
                        var newCurPrice = newCost / (float)recountMaterial.balance.Value;
                        recountMaterial.current_price = newCurPrice;

                        db.Acts_fuel_income.Add(act);
                    }

                    db.SaveChanges();
                    trans.Commit();
                    return true;
                }
                catch (Exception)
                {
                    trans.Rollback();
                    throw;
                }
                                    }
        }

        public List<IncomeFuel> GetIncomeFuelByDate(DateTime date)
        {
            var d = date.Date;
            List<IncomeFuel> res = (from income in db.Acts_fuel_income
                                    where DbFunctions.TruncateTime(income.date) == d
                                    select new IncomeFuel
                                    {
                                        Id = income.id,
                                        Fuel = new DictionaryItemsDTO
                                        {
                                            Id = income.id_type_fuel,
                                            Name = income.Type_fuel.name
                                        },
                                        Price = income.price,
                                        Count = income.count,
                                        Cost = income.cost,
                                        Comment = income.comment,
                                        Date = income.date,
                                        Contractor = new DictionaryItemsDTO
                                        {
                                            Id = income.id_contractor,
                                            Name = income.Contractor.name
                                        },
                                        VAT = new DictionaryItemsDTO
                                        {
                                            Id = income.id_vat,
                                            Name = income.VAT.vat_value.ToString()
                                        },
                                        Type_salary_doc = new DictionaryItemsDTO
                                        {
                                            Id = income.id_tpsal,
                                            Name = income.Type_salary_doc.short_name
                                        }
                                    }).ToList();
            return res;
        }

        public InfoBalanceMaterialsModel GetInfoBalanceMaterials(BalanceMaterials req)
        {
            var result = new InfoBalanceMaterialsModel();
            var checkType = db.Organizations.Where(t => t.org_id == req.OrgId).Select(t => t.tporg_tporg_id).FirstOrDefault();
            var newOrgId = checkType == 1 ? (int?)req.OrgId : null;

            result.ContractorsList = db.Contractor.Where(p => p.deleted == false).Select(p => new DictionaryItemsDTO { Id = p.id, Name = p.name }).ToList();
            result.TypeMaterials = db.Type_materials.Where(t => t.deleted != true).
                                Select(o => new DictionaryItemsDTO
                                {
                                    Id = o.type_id,
                                    Name = o.name
                                }).ToList();
            result.OrganizationsList = db.Organizations.Where(p => p.deleted != true &&
                (checkType == (int)TypeOrganization.Section ? p.org_id == req.OrgId : p.tporg_tporg_id == (int)TypeOrganization.Section)).
                                Select(t => new DictionaryItemsDTO
                                {
                                    Id = t.org_id,
                                    Name = t.name
                                }).OrderBy(t => t.Name).ToList();

            result.EmpList = db.Employees.Where(p => p.deleted != true && (newOrgId != null ? p.org_org_id == newOrgId : true) && p.Posts.pst_id == 7).
            Select(p => new DictionaryItemsDTO { Id = p.emp_id, Name = p.short_name }).ToList();


            var allType = new DictionaryItemsDTO { Id = null, Name = "Все" };
            var allOrg = new DictionaryItemsDTO { Id = -1, Name = "Все" };
            var allEmp = new DictionaryItemsDTO { Id = null, Name = "Все" };
            result.TypeMaterials.Insert(0, allType);
            result.EmpList.Insert(0, allEmp);
            if (checkType == (int)TypeOrganization.Company)
            {
                result.OrganizationsList.Insert(0, allOrg);
            }
            return result;
        }
        public List<BalanceMaterials> GetBalanceMaterialsList(BalanceMaterials req)
        {

            var checkOrg = db.Organizations.Where(t => t.org_id == req.OrgId).Select(t => t.tporg_tporg_id).FirstOrDefault();
            var newOrgId = checkOrg == 1 ? (int?)req.OrgId : null;

            var balanceList = (from c in db.TMC_Current.AsEnumerable()
                               select new BalanceMaterials
                               {
                                   Name = db.Materials.Where(t => t.mat_id == c.mat_mat_id).Select(t => t.name).FirstOrDefault(),
                                   Employees = db.Employees.Where(t => t.emp_id == c.emp_emp_id).Select(t => t.short_name).FirstOrDefault(),
                                   Description = db.Materials.Where(t => t.mat_id == c.mat_mat_id).Select(t => t.Type_materials.name).FirstOrDefault(),
                                   Balance = (float)Math.Round((float)c.count, 2),
                                   MatId = db.Materials.Where(t => t.mat_id == c.mat_mat_id).Select(t => t.Type_materials.type_id).FirstOrDefault(),
                                   Current_price = Convert.ToSingle(Math.Round(c.price, 2)),
                                   IdEmployees = c.emp_emp_id,
                                   IdMaterial = c.mat_mat_id,
                                   Summa = (float)Math.Round(c.price * (float)c.count,2),
                                   Id1 = db.Employees.Where(t => t.emp_id == c.emp_emp_id).Select(t => t.org_org_id).FirstOrDefault(),
                               }).ToList();
                 balanceList = balanceList.Where(t => t.Name != null && t.Employees != null &&
                (req.MatId != null ? t.MatId == req.MatId : true) && (newOrgId != null ? t.Id1 == newOrgId : true)
                && (req.IdMaterial != null ? t.IdMaterial == req.IdMaterial : true)
                && (req.IdEmployees != null ? t.IdEmployees == req.IdEmployees : true)).ToList();
            return balanceList;
        }



        //пересчет цены
        public string RecountTmcAct()
        {
            var actsId = db.TMC_Acts.Where(t => t.act_date.Value.Year == 2018).Select(t =>t.tma_id).ToList();
            var records = db.TMC_Records.Where(t =>t.tma_tma_id != null && actsId.Contains((int)t.tma_tma_id)).ToList();
            using (var trans = db.Database.BeginTransaction())
            {
                for (int i = 0; i < records.Count; i++)
                {
                    if (records[i].Materials != null)
                    {
                        records[i].price = (float)(records[i].Materials.current_price ?? 0);
                    }
                    if (i % 100 == 0)
                    {
                        db.SaveChanges();
                    }
                }
                db.SaveChanges();
                trans.Commit();
            }


            return "df";
        }




    }

}
