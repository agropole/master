﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LogusSrv.DAL.DBModel;
using LogusSrv.DAL.Entities.DTO;
using LogusSrv.DAL.Entities.Res;
using System.Data.SqlClient;
using LogusSrv.CORE.Exceptions;
using System.Data.Entity.Validation;
using System.Data.Entity.Infrastructure;

namespace LogusSrv.DAL.Operations
{
    public class RateDb
    {
        protected readonly LogusDBEntities db = new LogusDBEntities();

        public List<WorkList> GetWorks()
        {
            return db.Type_tasks.Where(p => p.deleted != true).Select(p => new WorkList
            {
                Id = p.tptas_id,
                Name = p.name,
                Values = p.Task_rate.Select(t => new RateModel
                {
                    Id = t.id,
                    IdTrak = new DictionaryItemsDTO {Id = t.id_grtrakt,Name = t.Group_traktors.name},
                    IdEqui = new DictionaryItemsDTO { Id = t.id_equi, Name = t.Equipments.name },
                    IdPlant = new DictionaryItemsDTO {Id = t.id_plant,Name = t.Plants.name},
                    IdAgroReq = new DictionaryItemsDTO { Id = t.id_agroreq, Name = t.Agrotech_requirment.name },
                    IdTptas = t.id_tptas,
                    ShiftOutput = t.shift_output,
                    RatePiecework = t.rate_piecework,
                    RateShift = t.rate_shift,
                    FuelConsumption = t.fuel_consumption
                }).ToList()
            }).OrderBy(p => p.Name).ToList();
        }

        public RateInfoModel GetInfo()
        {
            var info = new RateInfoModel
            {
                IdTrak = db.Group_traktors.Where(p => p.deleted != true).Select(p => new DictionaryItemsDTO { Id = p.grtrak_id, Name = p.name }).ToList(),
                IdEqui = db.Equipments.Where(p => p.deleted != true).Select(p => new DictionaryItemsDTO { Id = p.equi_id, Name = p.name + (p.reg_num != null ? " (" + p.reg_num + ")" : "") }).ToList(),
                IdPlant = db.Plants.Where(p => p.deleted != true).Select(p => new DictionaryItemsDTO { Id = p.plant_id, Name = p.name }).ToList(),
                IdAgroReq = db.Agrotech_requirment.Where(p => p.deleted != true).Select(p => new DictionaryItemsDTO { Id = p.id, Name = p.name }).ToList()
            };
            return info;
        }

        public string UpdateRate(List<ModelUpdateRate> req)
        {
            using (var trans = db.Database.BeginTransaction())
            {
                try
                {
                    var newId = (db.Task_rate.Max(t => (int?)t.id) ?? 0) + 1;
                    for (var i = 0; i < req.Count; i++)
                    {
                        var obj = req[i].Document;
                        switch (req[i].command)
                        {
                            case "del":
                                var checkTask = db.Tasks.Where(t => t.id_rate == obj.Id).FirstOrDefault();
                                if (checkTask != null) {
                                    throw new BllException(15, "Расценка используется.Удаление невозможно.");
                                }
                                var delDic = db.Task_rate.Where(o => o.id == obj.Id).FirstOrDefault();
                                db.Task_rate.Remove(delDic);
                                break;
                            case "update":
                                var updateDic = db.Task_rate.Where(o => o.id == obj.Id).FirstOrDefault();
                                updateDic.id_grtrakt = obj.IdTrak != null ? obj.IdTrak.Id : null;
                                updateDic.id_equi = obj.IdEqui != null ? obj.IdEqui.Id : null;
                                updateDic.id_agroreq = obj.IdAgroReq != null ? obj.IdAgroReq.Id : null;
                                updateDic.id_plant = obj.IdPlant != null ? obj.IdPlant.Id : null;
                                updateDic.id_tptas = obj.IdTptas;
                                updateDic.shift_output = obj.ShiftOutput;
                                updateDic.rate_shift = obj.RateShift;
                                updateDic.rate_piecework = obj.RatePiecework;
                                updateDic.fuel_consumption = obj.FuelConsumption;
                                break;
                            case "insert":
                                var newDic = new Task_rate
                                {
                                    id = newId,
                                    id_grtrakt = obj.IdTrak != null ? obj.IdTrak.Id : null,
                                    id_equi = obj.IdEqui != null ? obj.IdEqui.Id : null,
                                    id_agroreq = obj.IdAgroReq != null ? obj.IdAgroReq.Id : null,
                                    id_plant = obj.IdPlant != null ? obj.IdPlant.Id : null,
                                    id_tptas = obj.IdTptas,
                                    shift_output = obj.ShiftOutput,
                                    rate_shift = obj.RateShift,
                                    rate_piecework = obj.RatePiecework,
                                    fuel_consumption = obj.FuelConsumption
                                };
                                newId++;
                                db.Task_rate.Add(newDic);
                                break;
                        }
                    }
                    try
                    {
                        db.SaveChanges();
                    }
                    catch (DbUpdateException dbu)
                    {
                        throw new BllException(15, "Расценка используется.Удаление невозможно.");
                    }

                    trans.Commit();
                }
                catch (Exception)
                {
                    trans.Rollback();
                    throw;
                }
            }
            return "success";
        }
    }

    public class RateModel
    {
        public int? Id { get; set; }
        public DictionaryItemsDTO IdTrak { get; set; }
        public DictionaryItemsDTO IdEqui { get; set; }
        public DictionaryItemsDTO IdAgroReq { get; set; }
        public DictionaryItemsDTO IdPlant { get; set; }
        public int? IdTptas { get; set; }
        public decimal? ShiftOutput { get; set; }
        public decimal? RateShift { get; set; }
        public decimal? RatePiecework { get; set; }
        public decimal? FuelConsumption { get; set; }
    }

    public class WorkList : DictionaryItemsDTO
    {
        public List<RateModel> Values { get; set; }
    }

    public class RateInfoModel
    {
        public List<DictionaryItemsDTO> IdTrak { get; set; }
        public List<DictionaryItemsDTO> IdEqui { get; set; }
        public List<DictionaryItemsDTO> IdPlant { get; set; }
        public List<DictionaryItemsDTO> IdAgroReq { get; set; }
    }

    public class ModelUpdateRate : UpdateDictModel
    {
        public RateModel Document { get; set; }
    }
}
