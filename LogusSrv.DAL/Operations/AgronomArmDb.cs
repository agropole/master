﻿using System;
using System.Data.Entity;
using System.Collections.Generic;
using System.Linq;
using LogusSrv.CORE.Exceptions;
using LogusSrv.DAL.Entities.DTO.ActsSzr;
using LogusSrv.DAL.Entities.DTO;
using LogusSrv.DAL.Entities.Req;
using LogusSrv.DAL.DBModel;
using LogusSrv.DAL.Utils;
using LogusSrv.DAL.Enums;
using LogusSrv.DAL.Entities.Res;
using OfficeOpenXml.Style;
using OfficeOpenXml;
using System.Drawing;
using System.IO;
using System.Globalization;
using System.Data.Entity.Spatial;
using LogusSrv.DAL.Entities.DTO.ExportExcel;
using System.Net;
using LogusSrv.DAL.Entities;
using System.Web;
using System.Drawing.Imaging;
namespace LogusSrv.DAL.Operations
{
    public class AgronomArmDb
    {
        protected readonly LogusDBEntities db = new LogusDBEntities();
        protected readonly TechCardDb techcardDb = new TechCardDb();

     
        /// 2 top10 GetTop10ActsSzr
        public List<GetTop10ActsSzr> GetTop10ActsSzr(int? Id)
        {
            var list = (from f in db.TMC_Acts
                        where f.emp_utv_id == (int)Id && f.type_act == 2
                        select new GetTop10ActsSzr
                        {
                            Id = f.tma_id,
                            DateT = f.act_date.Value,
                            RecipientId = f.emp_utv_id,
                            ResponsibleId = f.emp_emp_id,
                            SoilTypeId = f.soil_soil_id,
                            Number = f.act_num,
                            Year = f.year,
                            tableData = (
                                  from v in db.TMC_Records
                                  where v.tma_tma_id == f.tma_id &&
                                  v.emp_from_id != 0 && v.emp_to_id == 0
                                  select new ActMaterials
                                  {
                                      Id = v.tmr_id,
                                      Count = v.count,
                                      Price = v.price,
                                      Material = new MaterialDto { Id = v.mat_mat_id },
                                      Field = new FieldDto { Id = v.fielplan_fielplan_id },
                                      Area = v.area,
                                  }).ToList()
                        }).AsEnumerable().OrderByDescending(t => Int32.Parse(t.Number)).Take(10).ToList();
            for (int i = 0; i < list.Count; i++)
            {
                list[i].DateBegin = list[i].DateT.ToString("dd.MM.yyyy");
            }
            return list;
        }

        //save
        public string SaveActsSzr(List<GetTop10ActsSzr> list)
        {
            using (var trans = db.Database.BeginTransaction())
            {
                try
                {
                    var AddActs = new List<TMC_Acts>();
                    var AddRec = new List<TMC_Records>();
                    var idRecords = CommonFunction.GetNextId(db, "TMC_Records");
                    var idActs = CommonFunction.GetNextId(db, "TMC_Acts");
                    var nextNumber = 1;
                    var con = db.TMC_Acts.Count();
                    if (con != 0) {
                    nextNumber = db.TMC_Acts.AsEnumerable().Max(t => Int32.Parse(t.act_num))+1;
                    }

                    for (int i = 0; i < list.Count; i++)
                    {
                        var tma_id = list[i].Id;
                        //удаляем акт
                        if (tma_id != null && tma_id != 0)
                        {
                            var delActs = db.TMC_Acts.Where(t => t.tma_id == tma_id).FirstOrDefault();
                            var delRecords = db.TMC_Records.Where(t => t.tma_tma_id != null && t.tma_tma_id == tma_id).ToList();
                            db.TMC_Acts.Remove(delActs);
                            db.TMC_Records.RemoveRange(delRecords);
                        }
                        //записываем новый 
                        var dic = new TMC_Acts {
                            tma_id = idActs,
                            soil_soil_id = list[i].SoilTypeId,
                            act_date = DateTime.Parse(list[i].DateBegin),
                            act_num = nextNumber.ToString(),
                            emp_emp_id = list[i].RecipientId,
                            emp_utv_id = list[i].ResponsibleId,
                            year = list[i].Year,
                            fielplan_fielplan_id = list[i].OrgId ?? 0,
                            type_act = 2,
                        };
                        AddActs.Add(dic);

                        //table data
                        for (int j = 0; j < list[i].tableData.Count; j++)
                        {
                            var dic2 = new TMC_Records
                            {
                                tmr_id = idRecords,
                                mat_mat_id = list[i].tableData[j].Material.Id,
                                cont_cont_id = 0,
                                fielplan_fielplan_id = list[i].tableData[j].Field.Id,
                                tma_tma_id = idActs,
                                count = (float)list[i].tableData[j].Count,
                                price = (float)list[i].tableData[j].Price,
                                area = (float)list[i].tableData[j].Area,
                                emp_from_id = list[i].ResponsibleId,
                                emp_to_id = 0,
                                act_date = DateTime.Now,
                            };
                            AddRec.Add(dic2);
                            idRecords++;
                        }
                        idActs++; nextNumber++;  
                    }
                    db.TMC_Acts.AddRange(AddActs);
                    db.TMC_Records.AddRange(AddRec);

                    db.SaveChanges();
                    trans.Commit();
                }
                catch (Exception)
                {
                    trans.Rollback();
                    throw;
                }


                return "success";
            }
        }
        
        public DictionatysActSzr GetDictionaryForActs()
        {
            var result = new DictionatysActSzr();
            result.TypeMaterials = db.Type_materials.Where(p => p.deleted != true).Select(p => new DictionaryItemsTypeMaterials
            {
                Id = p.type_id,
                Name = p.name,
                Materials = p.Materials.Where(m => m.deleted != true).Select(m => new DictionaryItemsDTO { Id = m.mat_id, Name = m.name, Current_price = m.current_price }).ToList()
            }).ToList();
            result.Contractors = db.Contractor.Where(p => p.deleted != true).Select(p => new DictionaryItemsDTO { Id = p.id, Name = p.name }).ToList();
            result.VATs = db.VAT.Where(p => p.deleted != true).Select(p => new DictionaryItemsDTO { Id = p.id, Name = p.vat_value.ToString() + "%" }).ToList();
            result.Type_salary_docs = db.Type_salary_doc.Where(p => p.deleted != true).Select(p => new DictionaryItemsDTO { Id = p.tpsal_id, Name = p.name }).ToList();
            result.OrganzationsList = db.Organizations.Select(p => new DictionaryItemsDTO { Id = p.org_id, Name = p.name }).ToList();
            result.EmployeesList = db.Employees.Where(p => p.deleted != true).Select(p => new DictionaryItemsDTO { Id = p.emp_id, Name = p.short_name }).ToList();
            result.EmployeesList1 = db.Employees.Where(p => p.deleted != true).Where(p => p.Posts.pst_id == 7).Select(p => new DictionaryItemsDTO { Id = p.emp_id, Name = p.short_name }).ToList();
            result.EmployeesList2 = db.Employees.Where(p => p.deleted != true).Where(p => p.Posts.pst_id == 7 || p.Posts.pst_id == 9).Select(p => new DictionaryItemsDTO { Id = p.emp_id, Name = p.short_name }).ToList();
            result.TypeSoilsList = db.Type_soil.Select(p => new DictionaryItemsDTO { Id = p.soil_id, Name = p.name }).ToList();
            result.MaterialsList = db.Materials.Where(p => p.deleted != true).Select(p => new DictionaryItemsDTO { Id = p.mat_id, Name = p.name, Current_price = p.current_price }).ToList();
            result.AllFields = db.Fields.Where(p => p.deleted != true).Select(p => new DictionaryItemsDTO { Id = p.fiel_id, Name = p.name, Area_plant = p.area }).ToList();
            result.FieldsAndPlants = (from f in db.Fields_to_plants
                                      where f.status == (int)StatusFtp.Approved && f.date_ingathering.Year >= DateTime.Now.Year
                                      group new DictionaryPlantItemsDTO
                                      {
                                          Id = f.fielplan_id,
                                          Name = f.Fields.name,
                                          Area_plant = f.area_plant
                                      } by new
                                      {
                                          f.plant_plant_id,
                                          f.Plants.name,
                                          f.date_ingathering,
                                      } into g
                                      select new DictionaryItemForFields
                                      {
                                          Id = g.Key.plant_plant_id,
                                          Name = g.Key.name + " (" + g.Key.date_ingathering.Year + ")",
                                          Values = g.ToList()
                                      }).OrderBy(t => t.Name).ToList();

            var curYear = DateTime.Now.Year;
            result.YearList = new List<DictionaryItemsDTO>();
            for (var i = curYear - 5; i < curYear + 5; i++)
            {
                result.YearList.Add(new DictionaryItemsDTO { Id = i, Name = i.ToString() });
            }

            return result;
        }

        ///сохранение и обновление акта - планшет
        public int CreateUpdateActSzrDetail(List<SaveRow> req)
        {
            using (var trans = db.Database.BeginTransaction())
            {
                    var nextNumber = 1;
                    var con = db.TMC_Acts.Count();
                    if (con != 0) {
                    nextNumber = db.TMC_Acts.AsEnumerable().Max(t => Int32.Parse(t.act_num)) + 1;
                    }
                //редактирование акта
                if (req[0].Number.Length != 0 && req[0].Id != null)
                {
                    //проверка номера акта
                    var list2 = db.Database.SqlQuery<ActSzrDetails>(string.Format(@"select act_num as Name from TMC_Acts where type_act=2 and  tma_id <>" + req[0].Id)).ToList();
                    for (int i = 0; i < list2.Count; i++)
                    {
                        if (list2[i].Name.Equals(req[0].Number)) {
                            req[0].Number = nextNumber.ToString();
                          //  return -1;
                        
                        }
                    }
                    var id = req[0].Id;
                    var updateDic = db.TMC_Acts.Where(o => o.tma_id == id).FirstOrDefault();
                    updateDic.act_num = req[0].Number; //номер акта
                    if (!String.IsNullOrEmpty(req[0].Date.ToString()))
                    {
                        updateDic.act_date = req[0].Date; //дата
                    }
                    if (!String.IsNullOrEmpty(req[0].Year.ToString()))
                    {
                        updateDic.year = req[0].Year; //год урожая 
                    }
                    if (!String.IsNullOrEmpty(req[0].SoilTypeId.ToString()))
                    {
                        updateDic.soil_soil_id = req[0].SoilTypeId; //вид земель
                    }
                    if (!String.IsNullOrEmpty(req[0].RecipientId.ToString()))
                    {
                        updateDic.emp_emp_id = req[0].RecipientId; //получатель
                    }
                    if (!String.IsNullOrEmpty(req[0].ResponsibleId.ToString()))
                    {
                        updateDic.emp_utv_id = req[0].ResponsibleId; //утвердитель
                    }
                    var tableData = db.TMC_Records.Where(t => t.tma_tma_id == id).ToList();
                    db.TMC_Records.RemoveRange(tableData);

                }
                  
                    //новый акт
                if (req[0].Id == null)
                    {
                        //проверка номера акта
                        var list2 = db.Database.SqlQuery<ActSzrDetails>(string.Format(@"select act_num as Name from TMC_Acts where type_act=2")).ToList();
                        for (int i = 0; i < list2.Count; i++)
                        {
                            if (list2[i].Name.Equals(req[0].Number)) {
                                req[0].Number = nextNumber.ToString();  
                            //    return -1;
                            }
                        }
                        var newDic = new TMC_Acts
                        {
                            tma_id = CommonFunction.GetNextId(db, "TMC_Acts"),
                            act_date = req[0].Date,
                            act_num = req[0].Number,
                            year = req[0].Year,
                            soil_soil_id = req[0].SoilTypeId,
                            emp_emp_id = req[0].RecipientId, //получатель
                            emp_utv_id = req[0].ResponsibleId, //утвердитель
                            fielplan_fielplan_id = 0,
                            type_act = 2,
                        };
                        db.TMC_Acts.Add(newDic);
                    }

                db.SaveChanges();
                trans.Commit();
            }
            int? key = 0;
            using (var trans = db.Database.BeginTransaction())
            {
                if (String.IsNullOrEmpty(req[0].Id.ToString()) || req[0].Id == 0)
                {
                    var list = db.Database.SqlQuery<BalanceMaterials>(
              string.Format(@"select max(tma_id) as Id from [TMC_Acts]")).ToList();
                    key = list[0].Id;
                }
                if (req[0].Id != null)
                {
                    key = req[0].Id;
                }

                //строчки в таблице
                for (int i = 0; i < req.Count; i++)
                {
                    var newDic1 = new TMC_Records
                    {
                        tmr_id = CommonFunction.GetNextId(db, "TMC_Records"),
                        mat_mat_id = req[i].Document.Material.Id,
                        cont_cont_id = 0,
                        fielplan_fielplan_id = req[i].Document.Field.Id, //поле, культура, год урожая
                        act_date = (DateTime)req[i].Date,
                        count = (float)req[i].Document.Count,
                        price = (float)req[i].Document.Price,
                        area = (float)req[i].Document.Area,
                        emp_from_id = (int)req[i].ResponsibleId,
                        emp_to_id = 0,
                        tma_tma_id = key
                    };
                    db.TMC_Records.Add(newDic1);
                }
                db.SaveChanges();
                trans.Commit();
            }
            return (int)key;
        }
        //справочники для ТМЦ
        public DictionatysActSzr GetDictionaryTMC(int? OrgId)
        {
            var checkOrg = db.Organizations.Where(t => t.org_id == OrgId).Select(t => t.tporg_tporg_id).FirstOrDefault();
            var newOrgId = checkOrg == 1 ? (int?)OrgId : null;
            var result = new DictionatysActSzr();
            result.TypeSoilsList = db.Type_soil.Select(p => new DictionaryItemsDTO { Id = p.soil_id, Name = p.name }).ToList();
            result.MaterialsList = db.Materials.Where(p => p.deleted != true).Select(p => new DictionaryItemsDTO
            { Id = p.mat_id, Name = p.name, Current_price = p.current_price, Description = p.Type_materials.name }).ToList();
            var wayList = new WayListBase();
            var values = wayList.GetSelectedPlants();
            result.FieldsAndPlants = (from f in db.Fields_to_plants
                                      where f.status == (int)StatusFtp.Approved && ((f.date_ingathering.Year == DateTime.Now.Year && f.plant_plant_id != 36) ||
                                      values.Contains(f.fielplan_id)) && (newOrgId != null ? f.Fields.org_org_id == newOrgId : true)
                                      group new DictionaryPlantItemsDTO
                                      {
                                          Id = f.fielplan_id,
                                          Name = f.Fields.name,
                                          Area_plant = f.area_plant
                                      } by new
                                      {
                                          f.plant_plant_id,
                                          f.Plants.name,
                                          f.date_ingathering,
                                      } into g
                                      select new DictionaryItemForFields
                                      {
                                          Id = g.Key.plant_plant_id,
                                          Name = g.Key.name + " (" + g.Key.date_ingathering.Year + ")",
                                          Values = g.ToList()
                                      }).OrderBy(t => t.Name).ToList();
            return result;
        }



        //сменное задание
        public WaysListForCreateModel GetInfoForCreateDayTask(UpdateDayTaskReq req)
        {
            var checkOrg = db.Organizations.Where(t => t.org_id == req.OrgId).Select(t => t.tporg_tporg_id).FirstOrDefault();
            var newOrgId = checkOrg == 1 ? (int?)req.OrgId : null;

            WaysListForCreateModel result = new WaysListForCreateModel();
            if (req.Traktors == true)
            {
                result.TraktorsList = db.Traktors.Where(p => p.deleted != true &&
                           (newOrgId != null ? p.Garages.org_org_id == newOrgId : true)).
                                        Select(t => new DictionaryItemsTraktors
                                        {
                                            Name = t.name,
                                            RegNum = t.reg_num,
                                            TypeTraktor = t.Type_traktors.name,
                                            TypeId = t.Type_traktors.tptrak_id,
                                            TrackId = t.track_id,
                                            Model = t.model,
                                            Id = t.trakt_id,
                                            emp_emp_id = t.emp_emp_id,
                                            Description = t.Employees.short_name,
                                        }).OrderBy(t => t.Name).ToList();
            }
            if (req.Employees == true)
            {
                result.EmployeesList = db.Employees.
                                        Where(e => (e.pst_pst_id == 1 || e.pst_pst_id == 5 || e.pst_pst_id == 2 ||
                                            e.pst_pst_id == 7) && e.deleted != true &&
                                            (newOrgId != null ? e.org_org_id == newOrgId : true)).
                                        Select(e => new DictionaryItemsDTO
                                        {
                                            Name = e.short_name,
                                            Id = e.emp_id,
                                            index = e.pst_pst_id,
                                            Bold = e.Posts.pst_id == 7 ? true : false,
                                        }).OrderBy(t => t.Name).ToList();
                result.ChiefsList = db.Employees.
                                        Where(e => e.pst_pst_id == 9 && e.deleted != true).
                                        Select(e => new DictionaryItemsDTO
                                        {
                                            Name = e.short_name,
                                            Id = e.emp_id,
                                            index = e.pst_pst_id,
                                        }).OrderBy(t => t.Name).ToList();
            }
            if (req.Equipments == true)
            {
                result.EquipmnetsList = db.Equipments.Where(p => p.deleted != true &&
                    (newOrgId != null ? p.modequi_modequi_id == newOrgId : true)).
                                        Select(e => new DictionaryItemsTraktors
                                        {
                                            Name = e.name,
                                            Id = e.equi_id,
                                            Width = e.width,
                                        }).OrderBy(t => t.Name).ToList();
            }
            if (req.Type_tasks == true)
            {
                result.TasksList = db.Type_tasks.Where(p => p.deleted != true).
                                    Select(t => new DictionaryItemsDTO
                                    {
                                        Name = t.name,
                                        Id = t.tptas_id,
                                    }).OrderBy(t => t.Name).ToList();
            }
            if (req.Fields_to_plants == true)
            {
                var nextYear = DateTime.Now.Year + 1;
                var firstYear = DateTime.Now.Year - 5;
                result.FieldsAndPlants = db.Fields_to_plants.Where(t => t.date_ingathering.Year >= firstYear && t.date_ingathering.Year <= nextYear
                    && (newOrgId != null ? t.Fields.org_org_id == newOrgId : true)).
                    Select(t => new DictionaryItemForFields
                    {
                        Id = t.fielplan_id,
                        Area = t.area_plant,
                        Name = t.Fields.name,
                        Description = t.Plants.name,
                        plantId = t.Plants.plant_id,
                        index = t.date_ingathering.Year,
                        fieldId = t.fiel_fiel_id,
                    }).ToList();
            }
            if (req.Fields == true)
            {
                result.FieldsList = db.Fields.Where(p => p.deleted == false &&
                    (newOrgId != null ? p.org_org_id == newOrgId : true)).Select(p => new FieldDictionary
                {
                    Id = p.fiel_id,
                    Name = p.name,
                    Area = p.area,
                    Coord = p.coord.AsText(),
                    Center = p.center.AsText(),
                    typeField = p.type,
                }).ToList();
            }
            if (req.Plants == true)
            {
                result.PlantList = db.Plants.Where(p => p.deleted == false).Select(p => new DictionaryItemsDTO
                {
                    Id = p.plant_id,
                    Name = p.name,
                }).ToList();
            }
            if (req.Agrotech_requirment == true)
            {
                result.AgroreqList = db.Agrotech_requirment.Where(p => p.deleted != true).
                 Select(p => new DictionaryItemsDTO { Id = p.id, Name = p.name }).OrderBy(p => p.Name).ToList();
            }
            return result;
        }

        public List<ActMaterials2> GetIncomeSzr(int empId)
        {
            var materialsList = new List<ActMaterials2>();
            materialsList = db.TMC_Records.AsEnumerable().Where(m => m.emp_from_id == 0 && m.emp_to_id != 0 && m.fielplan_fielplan_id == null
                && m.emp_to_id == empId).Select(m => new ActMaterials2
                      {
                          Material = new MaterialDto
                          {
                              Id = (int)m.mat_mat_id,
                              Name = m.Materials.name
                          },
                          //ответственный
                          Responsible = new MaterialDto
                          {
                              Id = m.emp_to_id,
                              Name = m.Employees.short_name
                          },
                          Price = m.price,
                          Kol = m.count,
                          DateBegin = m.act_date.ToString("dd.MM.yyyy"),
                          Pk = m.tmr_id,
                          Cost = m.price * m.count,
                      }).OrderByDescending(m => DateTime.Parse(m.DateBegin)).Take(10).ToList();
              return materialsList;
        }

        public string DeletePhoto(int id)
        {
            var name = db.Photo_fields.Where(t => t.photo_id == id).Select(t => t.path_photo).FirstOrDefault();
            var pathFile = System.Web.Hosting.HostingEnvironment.MapPath("/") + "photo_fields\\" + name; 
            using (var trans = db.Database.BeginTransaction())
            {
                try
                {
                    if (File.Exists(pathFile))
                    {
                        File.Delete(pathFile);
                    }
                    var del = db.Photo_fields.Where(t => t.photo_id == id).FirstOrDefault();
                    db.Photo_fields.Remove(del);

                    db.SaveChanges();
                    trans.Commit();
                }
                catch (Exception)
                {
                    trans.Rollback();
                    throw;
                }
                return "success";
            }
        }

        public String SaveIncomeSzr(List<ActMaterials2> list)   {
            using (var trans = db.Database.BeginTransaction())
            {
                try
                {
                    var AddRec = new List<TMC_Records>();
                    var idRecords = CommonFunction.GetNextId(db, "TMC_Records");
                    for (int i = 0; i < list.Count; i++)
                    {
                        var tmr_id = list[i].Pk;
                        //удаляем записи
                        if (tmr_id != 0)
                        {
                            var delRecords = db.TMC_Records.Where(t => t.tmr_id == tmr_id).FirstOrDefault();
                            db.TMC_Records.Remove(delRecords);
                        }
                        //записываем новый 
                            var dic2 = new TMC_Records
                            {
                                tmr_id = idRecords,
                                mat_mat_id = list[i].Material.Id,
                                cont_cont_id = 0,
                                act_date = DateTime.Parse(list[i].DateBegin),
                                count = (float)list[i].Kol ,
                                price = (float)list[i].Price,
                                emp_from_id = 0,
                                emp_to_id = (int)list[i].Responsible.Id
                            };
                            AddRec.Add(dic2);
                            idRecords++;

                    }
                    db.TMC_Records.AddRange(AddRec);
                    db.SaveChanges();
                    trans.Commit();
                }
                catch (Exception)
                {
                    trans.Rollback();
                    throw;
                }
                return "success";
            }
        }

        //===========================
        public List<ExpensesRequestDto> GetSpendingByFields(int orgId)
        {
            var year = DateTime.Now.Year;
            var checkOrg = db.Organizations.Where(t => t.org_id == orgId).Select(t => t.tporg_tporg_id).FirstOrDefault();
            var newOrgId = checkOrg == 1 ? (int?)orgId : null;
            //топливо и ЗП из путевых листов
            var list = (from ftp in db.Fields_to_plants
                        where ftp.date_ingathering.Year == year && ftp.Fields.type == 1 
                        join task1 in db.Tasks on ftp.fielplan_id equals task1.plant_plant_id into leftTask1
                        from t1 in leftTask1.DefaultIfEmpty()
                        where (newOrgId != null ? t1.Sheet_tracks.org_org_id == newOrgId : true)
                        group new
                        {
                            consumption = t1.fuel_cost ?? 0,
                            total_salary = t1.total_salary ?? 0,
                        }
                            by new
                            {
                                name = ftp.Fields.name,
                                id = ftp.fiel_fiel_id,
                            } into g
                        select new
                        {
                            id = g.Key.id,
                            name = g.Key.name,
                            consumption = (float)g.Sum(p => p.consumption),
                            total_salary = (float?)g.Sum(p => p.total_salary)
                        });
            //сдельная ЗП
            var dopSalary = (from ftp in db.Fields_to_plants
                             where ftp.date_ingathering.Year == year && ftp.Fields.type == 1

                             join sal_detail in db.Salary_details on ftp.fielplan_id equals sal_detail.plant_plant_id into leftDetail
                             from t2 in leftDetail.DefaultIfEmpty()
                             group new
                             {
                                 sum = t2.sum ?? 0
                             }
                                 by new
                                 {
                                     name = ftp.Fields.name,
                                     plant_plant_id = ftp.fiel_fiel_id,
                                 } into g
                             select new
                             {
                                 plant_plant_id = g.Key.plant_plant_id,
                                 dopSalary = g.Sum(p => p.sum)
                             });

            //сзр, удобрения, семена, прочие материалы
            var acts = (from ftp in db.Fields_to_plants
                        where ftp.date_ingathering.Year == year && ftp.Fields.type == 1
                        join act in db.TMC_Records on ftp.fielplan_id equals act.fielplan_fielplan_id into leftAct1
                        from a in leftAct1.DefaultIfEmpty()
                        where a.emp_from_id != 0 && a.emp_to_id == 0 &&
                        (newOrgId != null ? a.Employees.org_org_id == newOrgId : true)
                        group new { ftp, a } 
                        by new { ftp.fiel_fiel_id, ftp.Fields.name } into g
                        select new
                        {
                            name = g.Key.name,
                            id = g.Key.fiel_fiel_id,
                            Values = (
                                from m in g.Where(ga => ga.a != null)
                                group new
                                {
                                    cost = (m.a.count * m.a.price),
                                } by new
                                {
                                    m.a.Materials.mat_type_id
                                } into g2
                                select new
                                {
                                    type_id = g2.Key.mat_type_id,
                                    cost = g2.Sum(mat => mat.cost)
                                }).ToList()}
                                );
            var check = acts.Where(t => t.id == 265).ToList();
            //валовый сбор
            var dopGross = (from gh in db.Gross_harvest
                            where gh.Fields_to_plants.date_ingathering.Year == year
                            group gh by new { gh.Fields_to_plants.fiel_fiel_id } into g
                            select new
                            {
                                plant_plant_id = g.Key.fiel_fiel_id,
                                gross_harvest = g.Sum(g1 => g1.count / 1000)
                            });

            //площадь
            var dopArea = (from ftp in db.Fields_to_plants
                           where ftp.date_ingathering.Year == year
                           group ftp.area_plant by new { ftp.fiel_fiel_id } into g
                           select new
                           {
                               plant_plant_id = g.Key.fiel_fiel_id,
                               area = (double)g.Sum()
                           });

            var result = (from l in list
                          join act in acts on l.id equals act.id
                            into actLj
                          from a in actLj.DefaultIfEmpty()
                          join dG in dopGross on l.id equals dG.plant_plant_id
                              into jGross
                          from gross in jGross.DefaultIfEmpty()
                          join dA in dopArea on l.id equals dA.plant_plant_id
                              into jArea
                          from area in jArea.DefaultIfEmpty()
                          join dS in dopSalary on l.id equals dS.plant_plant_id
                          into jSalary
                          from salary in jSalary.DefaultIfEmpty()

                          let a_type1 = a.Values.Where(t => t.type_id == 1).FirstOrDefault()
                          let a_type2 = a.Values.Where(t => t.type_id == 2).FirstOrDefault()
                          let a_type4 = a.Values.Where(t => t.type_id == 4).FirstOrDefault()
                          let a_type5 = a.Values.Where(t => t.type_id == 5).FirstOrDefault()

                          let a_total_fact = ((l.total_salary == null ? 0 : l.total_salary) + l.consumption + (a_type1 == null ? 0 : a_type1.cost) + (a_type2 == null ? 0 : a_type2.cost) +
                          (a_type4 == null ? 0 : a_type4.cost) + (salary.dopSalary == null ? 0 : salary.dopSalary)  + (a_type5 == null ? 0 : a_type5.cost)) 
                          let a_cost_ga_fact = area.area != 0 ? Math.Round((double)a_total_fact / area.area, 2) : 0
                          let a_cost_t_fact = gross.gross_harvest.HasValue && gross.gross_harvest != 0 ? (float?)a_total_fact / (float?)gross.gross_harvest : 0

                          let total_salary = (float?)db.Tasks.Where(t => t.fiel_fiel_id == l.id
                             && t.Fields_to_plants.date_ingathering.Year == year &&  t.Fields_to_plants.plant_plant_id == 36).Select(t => t.total_salary).Sum()

                          let consumption = (float?)db.Tasks.Where(t => t.fiel_fiel_id == l.id
                           && t.Fields_to_plants.date_ingathering.Year == year && t.Fields_to_plants.plant_plant_id == 36).Select(t => t.fuel_cost).Sum()

                          let dop_salary = (float?)db.Salary_details.Where(t => t.Fields_to_plants.fiel_fiel_id == l.id
                          && t.Fields_to_plants.date_ingathering.Year == year && t.Fields_to_plants.plant_plant_id == 36).Select(t => t.sum).Sum()

                          let deduction = ((dop_salary ?? 0) + (total_salary ?? 0)) * 0.321

                          let tmc = (float?)db.TMC_Records.Where(t =>  t.emp_to_id == 0 && t.emp_from_id != 0  && t.Fields_to_plants.date_ingathering.Year == year 
                          && t.Fields_to_plants.fiel_fiel_id == l.id && t.Fields_to_plants.plant_plant_id == 36).Select(t => t.count * t.price).Sum()
                          select new ExpensesRequestDto
                          {
                              fiel_fiel_id = l.id, 
                              plant_name = l.name,
                              salary_total = (l.total_salary == null ? 0 : l.total_salary) + (salary.dopSalary == null ? 0 : salary.dopSalary),
                              razx_top = l.consumption,
                              szr_fact = a_type1 == null ? 0 : a_type1.cost,
                              minud_fact = a_type2 == null ? 0 : a_type2.cost,
                              posevmat_fact = a_type4 == null ? 0 : a_type4.cost,
                              dop_material_fact = a_type5 == null ? 0 : a_type5.cost,
                              total_fact = a_total_fact,
                              nzp = (total_salary ?? 0) + (consumption ?? 0) + (dop_salary ?? 0) + deduction  + (tmc ?? 0),
                              area = area.area,
                              gross_harvest = gross.gross_harvest.HasValue ? (float?)gross.gross_harvest : 0,
                              cost_ga_fact = a_cost_ga_fact,
                              cost_t_fact = a_cost_t_fact.HasValue ? Math.Round(a_cost_t_fact.Value, 2) : 0,
                          }).OrderBy(p => p.plant_name).ToList();

            //  считаем для га и т для всех записей
            for (int i = 0; i < result.Count; i++)
            {
                result[i].total_fact = result[i].total_fact + (result[i].nzp ?? 0);
                result[i].total_delta = result[i].total_plan - (float)result[i].total_fact;
                if (result[i].gross_harvest != 0) { result[i].cost_ga_plan = result[i].total_plan / (float)result[i].gross_harvest; }
                else { result[i].cost_ga_plan = 0; }//План (т)
                if (result[i].gross_harvest != 0) { result[i].cost_ga_fact = result[i].total_fact / result[i].gross_harvest; }
                else { result[i].cost_ga_fact = 0; } // Факт (т)
                result[i].cost_ga_delta = result[i].cost_ga_plan - (double)result[i].cost_ga_fact; //отклонение (т)

                if (result[i].area != 0) { result[i].cost_t_plan = result[i].total_plan / (float)result[i].area; }
                else { result[i].cost_t_plan = 0; } //План (га)
                if (result[i].area != 0) { result[i].cost_t_fact = (float)result[i].total_fact / (float)result[i].area; }
                else { result[i].cost_t_fact = 0; }//Факт (га)
                result[i].cost_t_delta = result[i].cost_t_plan - (float)result[i].cost_t_fact; // отклонение  га                 
            }

            return result;

        }

        public List<TaskArmDTO> GetSheetTasksFromYear(int OrgId)
        {
            var checkOrg = db.Organizations.Where(t => t.org_id == OrgId).Select(t => t.tporg_tporg_id).FirstOrDefault();
            var newOrgId = checkOrg == 1 ? (int?)OrgId : null;
            var res = new List<TaskArmDTO>();
            var listTask = (from t in db.Tasks
                            where t.Sheet_tracks.date_begin.Year == DateTime.Now.Year &&
                            (newOrgId != null ? t.Sheet_tracks.org_org_id == newOrgId : true)
                            select new TaskArmDTO
                            {
                                field_id = t.fiel_fiel_id,
                                field_name = t.Fields.name,
                                plant_name = t.Fields_to_plants.Plants.name,
                                task_name = t.Type_tasks.name,
                                type_task_id = t.tptas_tptas_id,
                                task_id = t.tas_id,
                                status = (int)t.Sheet_tracks.status,
                                date_begin = t.Sheet_tracks.date_begin,
                                date_end = t.Sheet_tracks.date_end,
                                equipment_id = t.Sheet_tracks.trakt_trakt_id,
                                emp_emp_id = t.Sheet_tracks.emp_emp_id,
                                empName = t.Sheet_tracks.Employees.short_name,
                                shiftNum = t.Sheet_tracks.date_begin.Hour < t.Sheet_tracks.date_end.Hour ? 1 : 2,
                            }).OrderByDescending(t => t.date_begin).ToList();
            var ids = listTask.Select(t => t.field_id).Distinct().ToList();
            for (int i = 0; i < ids.Count; i++)
            {
                var id = ids[i];
                var list = listTask.Where(t => t.field_id == id).Take(5).ToList();
                res.AddRange(list);
            }
            return res;
        }

        private List<SparePartsModel> GetFuelByDate(DateTime start, DateTime end)
        {
            var data = (from t in db.Repairs_acts
                        where t.date_open >= start && t.date_open <= end
                            && t.trakt_trakt_id != null && t.status == 3
                         let sum1 = db.Spare_parts_Records.Where(r => r.store_from_id != 0 && r.store_to_id == 0 && r.spss_spss_id == t.rep_id)
                         .Sum(r => r.price * r.count)
                         let sum2 = (from s in db.Daily_acts
                                     where s.rep_id == t.rep_id
                                     join z in db.Repairs_Tasks_Spendings on s.dai_id equals z.dai_dai_id into leftTask
                                     from t1 in leftTask.DefaultIfEmpty()
                                     select (t1.rate_piecework ?? 0) * (t1.hours ?? 0) + (t1.rate_shift ?? 0)).Sum()
                         select new SparePartsModel
                         {
                             Id = t.rep_id,
                             Pk = t.trakt_trakt_id,
                             DateBegin = t.date_open,
                             Cost1 = sum1 + (float)(sum2 != null ? sum2 : 0),
                             Number = 1,
                         }).Where(t => t.Cost1 != 0).OrderByDescending(t => t.DateBegin).ToList();
            return data;
        }


        public MoveSpareParts GetFuelRepairsFromYear(int OrgId)
        {
            var checkOrg = db.Organizations.Where(t => t.org_id == OrgId).Select(t => t.tporg_tporg_id).FirstOrDefault();
            var newOrgId = checkOrg == 1 ? (int?)OrgId : null;
            var res = new MoveSpareParts();
            res.BalanceList = new List<SparePartsModel>();
            res.InvoiceList = new List<SparePartsModel>();
            var date = DateTime.Now.Date;
            var date10 = date.AddDays(-10);
            var spare = GetFuelByDate(date10, date);
            var ids = spare.Select(t => t.Pk).Distinct().ToList();
            for (int i = 0; i < ids.Count; i++)
            {
                var id = ids[i];
                var list = spare.Where(t => t.Pk == id).Take(5).ToList();
                res.BalanceList.AddRange(list);
            }
            var fuel = (from t in db.Sheet_tracks
                        where t.date_begin >= date10 && t.date_begin <= date &&
                        (newOrgId != null ? t.org_org_id == newOrgId : true)
                        select new SparePartsModel
                        {
                            Id = t.shtr_id,
                            Pk = t.trakt_trakt_id,
                            DateBegin = t.date_begin,
                            Cost1 = (double)db.Tasks.Where(v => v.shtr_shtr_id == t.shtr_id).Sum(v => (v.fuel_cost ?? 0)),
                            Number = 2,
                        }).OrderByDescending(t => t.DateBegin).ToList();
            ids = fuel.Select(t => t.Pk).Distinct().ToList();
            for (int i = 0; i < ids.Count; i++)
            {
                var id = ids[i];
                var list = fuel.Where(t => t.Pk == id).Take(5).ToList();
                res.BalanceList.AddRange(list);
            }

            //===
            var dayFirst = new DateTime(date.Year, 1, 1);
            var dayLast = new DateTime(date.Year, 12, 31);
            var spareYear = GetFuelByDate(dayFirst, dayLast);

            var fuelYear = (from t in db.Sheet_tracks
                            where t.date_begin.Year == DateTime.Now.Year &&
                             (newOrgId != null ? t.org_org_id == newOrgId : true)
                            select new SparePartsModel
                            {
                                Id = t.shtr_id,
                                Pk = t.trakt_trakt_id,
                                DateBegin = t.date_begin,
                                Cost1 = (double)db.Tasks.Where(v => v.shtr_shtr_id == t.shtr_id).Sum(v => (v.fuel_cost ?? 0)),
                                Number = 2,
                            }).ToList();
            ids = db.Traktors.Select(t => (int?)t.trakt_id).ToList();

            for (int i = 0; i < ids.Count; i++)
            {
                var id = ids[i];
                var item = new SparePartsModel
                {
                    Pk = id,
                    Cost1 = spareYear.Where(t => t.Pk == id).Sum(t => t.Cost1),
                    Cost2 = fuelYear.Where(t => t.Pk == id).Sum(t => t.Cost1),
                };
                if (item.Cost1 == 0 && item.Cost2 == 0) { continue; }
                res.InvoiceList.Add(item);
            }
            return res;
        }

        //планшет-план
        public List<PlanRequestDto> GetSpendingPlanByFields(int OrgId)
        {
            var year = DateTime.Now.Year;
            var totalplan = (from tc in db.TC_param_value
                             where tc.TC.year == year &&
                             (tc.id_tc_param == 1 || tc.id_tc_param == 2 || tc.id_tc_param == 3 || tc.id_tc_param == 7 || tc.id_tc_param == 53
                             || tc.id_tc_param == 54 || tc.id_tc_param == 55 || tc.id_tc_param == 56 || tc.id_tc_param == 57
                             || tc.id_tc_param == 60 || tc.id_tc_param == 61 || tc.id_tc_param == 35 || tc.id_tc_param == 38
                             || tc.id_tc_param == 39 || tc.id_tc_param == 59 || tc.id_tc_param == 19 || tc.id_tc_param == 25
                             || tc.id_tc_param == 22 || tc.id_tc_param == 33)
                             group new { }
                                 by new
                                 {
                                     id_tc = tc.id_tc,
                                     id_tc_param = tc.id_tc_param,
                                     value = tc.value,
                                     id = tc.id,
                                     plant_id = tc.TC.Plants.plant_id,
                                     tpsort_id = tc.TC.tpsort_tpsort_id,
                                     field_id = tc.TC.fiel_fiel_id,
                                     grtask_id = tc.TC_operation.Type_tasks.auxiliary_rate,
                                 } into g
                             select new PlanRequestDto
                             {
                                 plant_id = g.Key.plant_id,
                                 field_id = g.Key.field_id,
                                 tpsort_id = g.Key.tpsort_id,
                                 id_tc = g.Key.id_tc,
                                 id_tc_param1 = g.Key.id_tc_param,
                                 value = (float?)g.Key.value,
                                 id = g.Key.id,
                                 grtask_id = g.Key.grtask_id,
                             }).ToList();

            int j3 = 0;
            var totplan = totalplan.Select(s => s.id_tc).Distinct().ToList();
            var totalplan1 = new List<PlanRequestDto>();
            for (j3 = 0; j3 < totplan.Count; j3++)
            {
                int id = totplan[j3];
                var area = totalplan.Where(s => s.id_tc == totplan[j3] && s.id_tc_param1 == 1).Select(s => s.value).FirstOrDefault();
                var gross_harvest = totalplan.Where(s => s.id_tc == totplan[j3] && s.id_tc_param1 == 3).Select(s => s.value).FirstOrDefault();
                var dop_material = totalplan.Where(s => s.id_tc == totplan[j3] && s.id_tc_param1 == 61).Select(s => s.value).FirstOrDefault();
                var dop_material_ga = area == 0 ? 0 : (dop_material == null ? 0 : dop_material) / area;
                var salary = totalplan.Where(s => s.id_tc == totplan[j3] && s.id_tc_param1 == 35 && s.grtask_id == 1).Select(s => s.value).Sum();
                var dopsalary = totalplan.Where(s => s.id_tc == totplan[j3] && s.id_tc_param1 == 38 && s.grtask_id == 1).Select(s => s.value).Sum();
                var deduction = (salary == null ? 0 : salary * 0.321) + (dopsalary == null ? 0 : dopsalary * 0.321);
                //орошение
                var salary_watering = totalplan.Where(s => s.id_tc == totplan[j3] && s.id_tc_param1 == 35 && s.grtask_id == 2).Select(s => s.value).Sum();
                var dopsalary_watering = totalplan.Where(s => s.id_tc == totplan[j3] && s.id_tc_param1 == 38 && s.grtask_id == 2).Select(s => s.value).Sum();
                var deduction_watering = (salary_watering == null ? 0 : salary_watering * 0.321) + (dopsalary_watering == null ? 0 : dopsalary_watering * 0.321);
                //
                var total_salary = (salary == null ? 0 : salary) + (dopsalary == null ? 0 : dopsalary) + (deduction == null ? 0 : deduction) +
                   (salary_watering == null ? 0 : salary_watering) + (dopsalary_watering == null ? 0 : dopsalary_watering)
                   + (deduction_watering == null ? 0 : deduction_watering);
                var workload = totalplan.Where(s => s.id_tc == totplan[j3] && s.id_tc_param1 == 33 && s.grtask_id == 1).Select(s => s.value).ToList();
                var workload_watering = totalplan.Where(s => s.id_tc == totplan[j3] && s.id_tc_param1 == 33 && s.grtask_id == 2).Select(s => s.value).ToList();
                var fuelConsumption = totalplan.Where(s => s.id_tc == totplan[j3] && s.id_tc_param1 == 59 && s.grtask_id == 1).Select(s => s.value).ToList();
                float? consumption = 0;
                for (int b = 0; b < fuelConsumption.Count; b++)
                {
                    consumption = consumption + fuelConsumption[b] * workload[b];
                }
                var fuel_cost = totalplan.Where(s => s.id_tc == totplan[j3] && s.id_tc_param1 == 39 && s.grtask_id == 1).Select(s => s.value).Sum();
                var fuel_cost_ga = area == 0 ? 0 : (consumption == null ? 0 : consumption) / area;

                //топливо орошение
                var fuelConsumption_watering = totalplan.Where(s => s.id_tc == totplan[j3] && s.id_tc_param1 == 59 && s.grtask_id == 2).Select(s => s.value).ToList();
                float? consumption_watering = 0;
                for (int b = 0; b < fuelConsumption_watering.Count; b++)
                {
                    consumption_watering = consumption_watering + fuelConsumption_watering[b] * workload_watering[b];
                }
                var fuel_cost_watering = totalplan.Where(s => s.id_tc == totplan[j3] && s.id_tc_param1 == 39 && s.grtask_id == 2).Select(s => s.value).Sum();
                var fuel_cost_ga_watering = area == 0 ? 0 : (consumption_watering == null ? 0 : consumption_watering) / area;
                //
                var total_consumption = consumption + consumption_watering;
                var chemicalFertilizers = totalplan.Where(s => s.id_tc == totplan[j3] && s.id_tc_param1 == 22).Select(s => s.value).FirstOrDefault();
                var szr = totalplan.Where(s => s.id_tc == totplan[j3] && s.id_tc_param1 == 25).Select(s => s.value).FirstOrDefault();
                var seed = totalplan.Where(s => s.id_tc == totplan[j3] && s.id_tc_param1 == 19).Select(s => s.value).FirstOrDefault();
                var total_cost = (fuel_cost ?? 0) + (fuel_cost_watering ?? 0);  //итого по топливу

                var nzp = totalplan.Where(s => s.id_tc == totplan[j3] && s.id_tc_param1 == 7).Select(s => s.value).FirstOrDefault();
                var combine = db.TC_to_service.Where(t => t.TC.year == year && t.id_service == 1 && t.id_tc == id).Select(v => (float?)v.cost).Sum();
                var loader = db.TC_to_service.Where(t => t.TC.year == year && t.id_service == 2 && t.id_tc == id).Select(v => (float?)v.cost).Sum();
                var autotransport = db.TC_to_service.Where(t => t.TC.year == year && t.id_service == 3 && t.id_tc == id).Select(v => (float?)v.cost).Sum();
                var desiccation = db.TC_to_service.Where(t => t.TC.year == year && t.id_service == 4 && t.id_tc == id).Select(v => (float?)v.cost).Sum();
                var analyzes = db.TC_to_service.Where(t => t.TC.year == year && t.id_service == 5 && t.id_tc == id).Select(v => (float?)v.cost).Sum();
                var total_service = db.TC_to_service.Where(t => t.TC.year == year && t.id_tc == id).Select(v => (float?)v.cost).Sum();
                var total = (nzp == null ? 0 : nzp) + (total_salary == null ? 0 : total_salary) + total_cost
                 + (seed == null ? 0 : seed) + (szr == null ? 0 : szr) + (chemicalFertilizers == null ? 0 : chemicalFertilizers)
                 + (total_service == null ? 0 : total_service) + (dop_material == null ? 0 : dop_material);

                totalplan1.Add(new PlanRequestDto
                {
                    id_tc = id,
                    //     plant_name = totalplan.Where(s => s.id_tc == totplan[j3]).Select(s => s.plant_name).FirstOrDefault(),
                    plant_id = totalplan.Where(s => s.id_tc == totplan[j3]).Select(s => s.plant_id).FirstOrDefault(),
                    field_id = totalplan.Where(s => s.id_tc == totplan[j3]).Select(s => s.field_id).FirstOrDefault(),
                    tpsort_id = totalplan.Where(s => s.id_tc == totplan[j3]).Select(s => s.tpsort_id).FirstOrDefault(),
                    area = (decimal?)area,
                    productivity = totalplan.Where(s => s.id_tc == totplan[j3] && s.id_tc_param1 == 2).Select(s => s.value).FirstOrDefault(),
                    gross_harvest = gross_harvest,
                    nzp = (decimal?)nzp,
                    nzp_zp = totalplan.Where(s => s.id_tc == totplan[j3] && s.id_tc_param1 == 53).Select(s => s.value).FirstOrDefault(),
                    nzp_fuel = totalplan.Where(s => s.id_tc == totplan[j3] && s.id_tc_param1 == 54).Select(s => s.value).FirstOrDefault(),
                    nzp_szr = totalplan.Where(s => s.id_tc == totplan[j3] && s.id_tc_param1 == 55).Select(s => s.value).FirstOrDefault(),
                    nzp_chemicalFertilizers = totalplan.Where(s => s.id_tc == totplan[j3] && s.id_tc_param1 == 56).Select(s => s.value).FirstOrDefault(),
                    nzp_seed = totalplan.Where(s => s.id_tc == totplan[j3] && s.id_tc_param1 == 57).Select(s => s.value).FirstOrDefault(),
                    nzp_dop_material = totalplan.Where(s => s.id_tc == totplan[j3] && s.id_tc_param1 == 60).Select(s => s.value).FirstOrDefault(),
                    dop_material = (decimal?)dop_material,
                    dop_material_ga = dop_material_ga,
                    salary = (decimal?)salary,
                    dopsalary = (decimal?)dopsalary,
                    salary_watering = (decimal?)salary_watering,
                    dopsalary_watering = (decimal?)dopsalary_watering,
                    deduction_watering = (decimal?)deduction_watering,
                    fuel_cost = (decimal?)fuel_cost,
                    fuel_cost_watering = (decimal?)fuel_cost_watering,
                    deduction = (decimal?)deduction,
                    total_salary = (decimal?)total_salary,
                    consumption = (decimal?)consumption,
                    consumption_watering = (decimal?)consumption_watering,
                    fuel_cost_ga = fuel_cost_ga,
                    fuel_cost_ga_watering = fuel_cost_ga_watering,
                    total_consumption = (decimal?)total_consumption,
                    total_cost = (decimal?)total_cost,  //итого по топливу
                    seed = (decimal?)seed,
                    szr = (decimal?)szr,
                    szr_ga = area == 0 ? 0 : (szr == null ? 0 : szr) / area,
                    chemicalFertilizers = (decimal)(chemicalFertilizers == null ? 0 : chemicalFertilizers),
                    chemicalFertilizers_ga = area == 0 ? 0 : (chemicalFertilizers == null ? 0 : chemicalFertilizers) / area,
                    combine = (decimal?)combine,
                    loader = (decimal?)loader,
                    autotransport = (decimal?)autotransport,
                    desiccation = (decimal?)desiccation,
                    analyzes = (decimal?)analyzes,
                    total_service = (decimal?)total_service,
                    total = (decimal?)total,
                    total_ga = area == 0 ? 0 : total / area,
                    total_t = gross_harvest == 0 ? 0 : total / gross_harvest,
                });
            }


            var sortFieldData = techcardDb.GetPlantSortFieldByYear(DateTime.Now.Year);
            var allFields = db.Fields_to_plants.Where(t => t.date_ingathering.Year == year).Select(t => t.fiel_fiel_id).Distinct().ToList();
            var totalplan2 = new List<PlanRequestDto>();
            for (int i = 0; i < totalplan1.Count; i++)
            {
                //if (totalplan1[i].plant_id == 3)
                //{
                //    var df = 0;

                //}
                if (totalplan1[i].field_id == null)
                {
                    var plant_id = totalplan1[i].plant_id;
             //       var tpsort_id = totalplan1[i].tpsort_id;

                    var dfff = sortFieldData.Where(t => t.Id == plant_id).Select(t => t.Fields).FirstOrDefault();

               //     var curData = sortFieldData.Where(t => t.Id == plant_id).Select(t => t.Values.Select(l => l.Fields).FirstOrDefault()).FirstOrDefault();
                    var curData = sortFieldData.Where(t => t.Id == plant_id).Select(t => t.Fields).FirstOrDefault();
                    if (curData != null)
                    {
                        for (int j = 0; j < curData.Count; j++)
                        {
                            var coef = curData.Sum(t => t.Area) != 0 ? (curData[j].Area / (curData.Sum(t => t.Area))) : 1;
                            totalplan2.Add(new PlanRequestDto
                           {
                               field_id = curData[j].Id,
                               area = curData[j].Area,
                      //        tpsort_id = null,
                               plant_id = totalplan1[i].plant_id,
                               nzp = totalplan1[i].nzp * coef,                //нзп
                               total_salary = totalplan1[i].total_salary * coef,   // зп
                               total_cost = totalplan1[i].total_cost * coef,  //топливо
                               seed = totalplan1[i].seed * coef,  //семена
                               szr = totalplan1[i].szr * coef,  //сзр
                               chemicalFertilizers = totalplan1[i].chemicalFertilizers * coef,
                               dop_material = totalplan1[i].dop_material * coef,  //доп. матер
                               total_service = totalplan1[i].total_service * coef,
                               gross_harvest = totalplan1[i].gross_harvest * (float)coef,

                           });
                        }
                    }
                }
                else
                {
              //     totalplan1[i].area1 = -1;
                    totalplan2.Add(totalplan1[i]);
                }
                }

            var df11 = totalplan2.Where(t => t.field_id == 2).ToList();


            for (int i = 0; i < totalplan2.Count; i++)
            {
                for (int j = 0; j < totalplan2.Count; j++)
                {
                    if (i != j && totalplan2[j].area1 != -1  && totalplan2[i].field_id == totalplan2[j].field_id) {
                  //      totalplan2[j].area1 = -1;
                        totalplan2[i].area = (totalplan2[i].area ?? 0) + (totalplan2[j].area ?? 0);
                        totalplan2[i].nzp = (totalplan2[i].nzp ?? 0) + (totalplan2[j].nzp ?? 0);
                        totalplan2[i].total_salary = (totalplan2[i].total_salary ?? 0) + (totalplan2[j].total_salary ?? 0);
                        totalplan2[i].dop_material = (totalplan2[i].dop_material ?? 0) + (totalplan2[j].dop_material ?? 0);
                        totalplan2[i].seed = (totalplan2[i].seed ?? 0) + (totalplan2[j].seed ?? 0);
                        totalplan2[i].szr = (totalplan2[i].szr ?? 0) + (totalplan2[j].szr ?? 0);
                        totalplan2[i].total_cost = totalplan2[i].total_cost + totalplan2[j].total_cost;
                        totalplan2[i].chemicalFertilizers = (totalplan2[i].chemicalFertilizers ?? 0) + (totalplan2[j].chemicalFertilizers ?? 0);
                        totalplan2[i].total_service = totalplan2[i].total_service + totalplan2[j].total_service;
                        totalplan2.RemoveAt(j); j--;
                    }
                }
            }
            for (int i = 0; i < totalplan2.Count; i++)
            {
                totalplan2[i].total = (totalplan2[i].nzp ?? 0) + (totalplan2[i].total_salary ?? 0) + (totalplan2[i].dop_material ?? 0)
                    + (totalplan2[i].seed ?? 0) + (totalplan2[i].szr ?? 0) + (totalplan2[i].total_cost)+
                (totalplan2[i].chemicalFertilizers ?? 0) + (totalplan2[i].total_service ?? 0);
            }
                return totalplan2;       
        }

        //отчет - выполненные работы на поле
        public List<PrintWayListDto> GetWayListInfo(DateTime date, int mechId, int? traktorId)
        {
            var list = (from shtr in db.Sheet_tracks
                        where DbFunctions.TruncateTime(shtr.date_begin) == DbFunctions.TruncateTime(date)
                        && shtr.emp_emp_id == mechId && shtr.status == 1 &&
                        (traktorId != null ? shtr.trakt_trakt_id == traktorId : true)

                        join task in db.Tasks on shtr.shtr_id equals task.shtr_shtr_id into leftTask
                        from t in leftTask.DefaultIfEmpty()

                        select new PrintWayListDto
                        {
                            ShtrId = shtr.shtr_id,
                            TaskId = t.tas_id,
                            ConsumptionFact = t.consumption_fact,
                            Date = shtr.date_begin,
                            TypeTask = t.Type_tasks.name,
                            NumLifts = t.Type_tasks.tptas_id,
                            FieldName = t.Fields.name,
                            FieldId = t.Fields.fiel_id,
                            PlantName = t.Fields_to_plants.Plants.name + " (" + t.Fields_to_plants.date_ingathering.Year + ")",
                            ShiftNum = shtr.date_begin.Hour < shtr.date_end.Hour ? 1 : 2,
                            TypePrice = t.type_price,
                            Worktime = t.worktime,
                            Area = t.area,
                            Volumefact = t.volume_fact,
                            WorkDay = t.workdays,
                            TracktorId = shtr.trakt_trakt_id,
                            EquipmentId = shtr.equi_equi_id,
                        }).OrderBy(l => l.Date).ToList();
            return list;
        }

        public List<BalanceMaterials> GetBalanceMaterialsList(int OrgId)
        {
            var checkOrg = db.Organizations.Where(t => t.org_id == OrgId).Select(t => t.tporg_tporg_id).FirstOrDefault();
            var newOrgId = checkOrg == 1 ? (int?)OrgId : null;

            var balanceList = (from c in db.TMC_Current.AsEnumerable()
                               select new BalanceMaterials
                               {
                                   Name = db.Materials.Where(t => t.mat_id == c.mat_mat_id).Select(t => t.name).FirstOrDefault(),
                                   Employees =  db.Employees.Where(t => t.emp_id == c.emp_emp_id).Select(t => t.short_name).FirstOrDefault(),
                                   Description = db.Materials.Where(t => t.mat_id == c.mat_mat_id).Select(t => t.Type_materials.name).FirstOrDefault(),
                                   Balance = (float)c.count,
                                   Current_price =  Convert.ToSingle(Math.Round(c.price,2)),
                                   IdEmployees = c.emp_emp_id,
                                   IdMaterial = c.mat_mat_id,
                                   Summa = c.price * (float)c.count,
                                   Id1 = db.Employees.Where(t => t.emp_id == c.emp_emp_id).Select(t => t.org_org_id).FirstOrDefault(),
                               }).ToList();
            balanceList = balanceList.Where(t => t.Name != null && t.Employees != null).ToList();
            if (newOrgId != null) {
                balanceList = balanceList.Where(t => t.Id1 == newOrgId).ToList();
            }
            return balanceList;
        }




        private DbGeography checkPolygon(DbGeography pol)
        {
            try
            {

                if (pol.Area > 10000000000)
                {
                    var coord2 = pol.AsText();
                    coord2 = coord2.Replace("MULTIPOLYGON (((", "").Replace(")))", "").Replace("POLYGON ((", "").Replace("))", "").Replace(", ", ",");
                    var ars = coord2.Split(',');
                    var coordNew = "POLYGON ((";
                    for (var i = ars.Length - 1; i >= 0; i--)
                    {
                        coordNew += ars[i];
                        if (i != 0)
                        {
                            coordNew += ", ";
                        }
                        else
                        {
                            coordNew += "))";
                        }
                    }
                    var reorientedPol = DbGeography.PolygonFromText(coordNew, 4326);
                    if (reorientedPol.Area != 0)
                    {
                        return reorientedPol;
                    }
                    else
                    {
                        return null;
                    }
                }
                else
                {
                    if (pol.Area != 0)
                    {
                        return pol;
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}
