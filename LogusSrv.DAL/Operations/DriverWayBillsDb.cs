﻿using System; 
using System.Collections.Generic;
using System.Linq;
using LogusSrv.CORE.Exceptions;
using LogusSrv.DAL.DBModel;
using LogusSrv.DAL.Enums;
using LogusSrv.DAL.Entities.DTO;
using LogusSrv.DAL.Entities.DTO.DriverWayBills;
using LogusSrv.DAL.Entities.DTO.DriverWayBillTaskExt;
using LogusSrv.DAL.Entities.Req;
using LogusSrv.DAL.Entities.Res;
using LogusSrv.DAL.Utils;
using LogusSrv.DAL.Entities.DTO.WaysList;
using System.Data.Entity;

namespace LogusSrv.DAL.Operations
{
    public class DriverWayBillsDb : WayListBase
    {
        public WaysListForCreateModel GetInfoForDriverWayBillItem(InfoForDriverWayBillReq req)
        {
            if (req.ShiftTaskId.HasValue) {
                return GetInfoForCreateWaysListByShiftTasks(req.ShiftTaskId.Value, 2);
            } else {
                return GetInfoForCreateWaysList(2, req.OrgId, req.WaysListId, req.TaskId, req.DayTaskId);
            }
        }

        public List<DriverWayBillInListDTO> GetDriverWayBillsList(GetDriverWayBillsListReq req)
        {
            var orgId = req.TporgId == 1 ? req.OrgId : null;

            var list = (from s in db.Sheet_tracks
                        where s.shtr_type_id == 2  
                          && (orgId != null ? s.org_org_id == orgId : true) &&
                     (!String.IsNullOrEmpty(req.RegNum) ? (s.Traktors.reg_num.Contains(req.RegNum)) : true) &&
                   //   (String.IsNullOrEmpty(req.RegNum) ? ((DbFunctions.TruncateTime(s.date_begin) >= (DbFunctions.TruncateTime(req.DateFilter)))
                 //     && (DbFunctions.TruncateTime(s.date_begin) <= DbFunctions.TruncateTime(req.DateFilterEnd))) : true) 
                 ((DbFunctions.TruncateTime(s.date_begin) >= (DbFunctions.TruncateTime(req.DateFilter)))
                  && (DbFunctions.TruncateTime(s.date_begin) <= DbFunctions.TruncateTime(req.DateFilterEnd)))
                        select new DriverWayBillInListDTO
                        {
                            WayBillId = s.shtr_id,
                            WaysListNum = s.num_sheet,
                            Date = s.date_begin,
                            FioDriver = s.Employees.short_name,
                            CarsNum = s.Traktors.reg_num,
                            CarsName = s.Traktors.name,
                            Status = s.status,
                            ShiftNum = s.date_begin.Hour < s.date_end.Hour ? 1 : 2,
                            TypeTraktor = s.Traktors.tptrak_tptrak_id,
                            IsRepair = s.is_repair == null ? false : s.is_repair,
                            GroupTraktor = s.Traktors.id_grtrakt,
                        }).AsEnumerable().OrderBy(l => l.ShiftNum).ThenByDescending(l => l.Date).ToList();

            foreach (var elm in list)
            {
                elm.DateBegin = elm.Date.ToString("dd.MM.yyyy");
            }

            return list;
        }

        public PrintLightAvtoDto GetPrintInfoForLightAvto(int WayBillId)
        {
            var info = (from s in db.Sheet_tracks
                       where s.shtr_type_id == 2 // Путевой лист водителя
                       && s.shtr_id == WayBillId

                       join tasks in db.Tasks.Where(t=> t.task_num == 0) on s.shtr_id equals tasks.shtr_shtr_id into tasksLj
                       from t in tasksLj.DefaultIfEmpty()
                       select new PrintLightAvtoDto
                       {
                           FiledId = t.fiel_fiel_id,
                           NumWayBill = s.num_sheet,
                           Date = s.date_begin,
                           TraktorBrand = s.Traktors.name,
                           TraktorNum = s.Traktors.reg_num,
                           DriverName = s.Employees.short_name,
                           DriverLicense = s.Employees.driver_license,
                           Description = t.description,
                           FieldName = t.Fields.name,
                           Available = t.Employees.short_name,
                       }).FirstOrDefault();
            var idOrg = Int32.Parse(db.Parameters.Where(t => t.name.Equals("NameOrg")).Select(t => t.value).FirstOrDefault());
            var Ogranization = db.Organizations.Where(t => t.org_id == idOrg).Select(t => t.name).FirstOrDefault();
            info.OGRN = db.Parameters.Where(t => t.name.Equals("OGRN")).Select(t => t.value).FirstOrDefault(); 
            info.Day = info.Date.ToString("dd");
            info.Month = getMonth(info.Date.ToString("MM"));
            info.Year = info.Date.ToString("yy");
            info.StartTime = info.Date.ToString("HH-mm");
            info.OrgName = Ogranization;
            info.Customer = getObject(info.FieldName, info.Description, info.FiledId, info.Available);

            return info;
        }

        public int SubmitDriverWayBillItem(DriverWayBillItemDto req)
        {
            ValidatePlantRequiredOnFields(req.TableTasksData);
            var isUpdating = req.WayBillId.HasValue;
            if (!isUpdating)
              req.WayListNum = checkWayListNum(req.WayListNum, req.IsRepair, req.WayBillId, 2);
            using (var trans = db.Database.BeginTransaction())
            {
                try
                {
                    Sheet_tracks sheetTrack;
                    if (isUpdating)
                    {
                        
                        var sheetTrackFromDb = (from st in db.Sheet_tracks
                            where st.shtr_id == req.WayBillId.Value
                            && st.shtr_type_id == 2 // Путевой лист водителя
                            select st).FirstOrDefault();
                        if (sheetTrackFromDb != null)
                        {
                            sheetTrack = sheetTrackFromDb;
                        }
                        else
                        {
                            throw new BllException(15, "Не найден путевой лист.");
                        }
                    }
                    else
                    {
                        sheetTrack = new Sheet_tracks
                        {
                            shtr_id = CommonFunction.GetNextId(db, "Sheet_tracks"),
                            status = (int)WayListStatus.Issued,
                            shtr_type_id = 2 // Путевой лист водителя
                        };
                    }

                    MapSheetTrackFromDto(sheetTrack, req);

                    if (!isUpdating)
                        db.Sheet_tracks.Add(sheetTrack);

                    var reqTaskIds = from t in req.TableTasksData
                                     where t.TaskId != null
                                     select t.TaskId;

                    var tasksToUpdate = (from t in db.Tasks
                                        where t.shtr_shtr_id == sheetTrack.shtr_id
                                        && reqTaskIds.Contains(t.tas_id)
                                        select t).ToList();

                    var tasksToDelete = (from t in db.Tasks
                                        where t.shtr_shtr_id == sheetTrack.shtr_id
                                        && !reqTaskIds.Contains(t.tas_id)
                                        select t).ToList();

                    // Create tasks
                    foreach (var taskReq in req.TableTasksData.Where(t => t.TaskId == null))
                    {
                        var task = new Tasks
                        {
                            tas_id = CommonFunction.GetNextId(db, "Tasks"),
                            shtr_shtr_id = sheetTrack.shtr_id,
                            task_num = 0
                        };
                        MapTaskFromDto(task, taskReq, sheetTrack.trakt_trakt_id.Value, req.IsRepair, "create");
                   

                        db.Tasks.Add(task);
                    }

                    // Update tasks
                    foreach (var t in tasksToUpdate)
                    {
                        var reqTask = req.TableTasksData.FirstOrDefault(ttd => ttd.TaskId == t.tas_id);
                        if (reqTask != null)
                        {
                            MapTaskFromDto(t, reqTask, sheetTrack.trakt_trakt_id.Value, req.IsRepair, "update");
                        }
                    }

                    // Delete tasks
                    db.Tasks.RemoveRange(tasksToDelete);


                    db.SaveChanges();
                    trans.Commit();

                    return sheetTrack.shtr_id;
                }
                catch (Exception)
                {
                    trans.Rollback();
                    throw;
                }
            }
        }

        private void ValidatePlantRequiredOnFields(IEnumerable<DriverWayBillItemTaskDto> req)
        {
            var fieldsWithNoPlants = req
                                        .Where(t => t.FieldInfo != null && t.FieldInfo.Id.HasValue && (t.PlantInfo == null || !t.PlantInfo.Id.HasValue))
                                        .Select(t => t.FieldInfo.Id).ToList();

            if (fieldsWithNoPlants.Any())
            {
                var fieldNames = (from f in db.Fields
                                  where f.type == 1
                                  && fieldsWithNoPlants.Contains(f.fiel_id)

                                  select f.name).ToList();

                if (fieldNames.Any())
                {
                    throw new BllException(14, "Необходимо выбрать культуру для следующих полей:", fieldNames);
                }
            }
        }

        private void ValidatePlantRequiredOnField(int? fId, int? pId)
        {
            if (!fId.HasValue)
            {
                throw new BllException(20, "Необходимо выбрать поле");
            }

            if (pId.HasValue)
                return;

            var fieldName = (from f in db.Fields
                              where f.type == 1
                              && f.fiel_id == fId

                              select f.name).FirstOrDefault();

            if (fieldName != null)
            {
                throw new BllException(14, "Необходимо выбрать культуру для поля: " + fieldName);
            }
        }

        public void MapSheetTrackFromDto(Sheet_tracks sheetTrack, DriverWayBillItemDto dto)
        {
            sheetTrack.num_sheet = dto.WayListNum;
            sheetTrack.danger_cargo = dto.DangerCargo;
            if (dto.TypeFuel != null)
            {
                sheetTrack.id_type_fuel = dto.TypeFuel;
                sheetTrack.fuel_price = (decimal)db.Type_fuel.Where(p => p.id == dto.TypeFuel).Select(p => p.current_price).FirstOrDefault();
            }
           
            if (dto.IsRepair == false && dto.TraktorId == null)
                throw new BllException(25, "Не выбрано значение в поле \"Техника\"");
            sheetTrack.is_repair = dto.IsRepair;
            if (dto.TraktorId != null) sheetTrack.trakt_trakt_id = dto.TraktorId.Value;
            if (dto.DriverId != null) sheetTrack.emp_emp_id = dto.DriverId.Value;
            if (dto.UserId != null) sheetTrack.user_user_id = dto.UserId.Value;
            if (dto.OrgId != null) sheetTrack.org_org_id = dto.OrgId.Value;
            var timeStart = new TimeSpan(Int32.Parse(dto.timeStart.Substring(0, 2)), Int32.Parse(dto.timeStart.Substring(3, 2)), 0);
            var timeEnd = new TimeSpan(Int32.Parse(dto.timeEnd.Substring(0, 2)), Int32.Parse(dto.timeEnd.Substring(3, 2)), 0);
            if (dto.DateBegin.HasValue)
            {
                sheetTrack.date_begin = dto.DateBegin.Value.Date + timeStart;
                if (timeStart > timeEnd) sheetTrack.date_end = dto.DateBegin.Value.Date.AddDays(1) + timeEnd;
                else sheetTrack.date_end = dto.DateBegin.Value.Date + timeEnd;

                sheetTrack.date_end = dto.DateBegin.Value.Date + timeEnd;
                if (timeStart > timeEnd)
                    sheetTrack.date_end = sheetTrack.date_end.AddDays(1);

                sheetTrack.date_begin_fact = sheetTrack.date_begin;
                sheetTrack.date_end_fact = sheetTrack.date_end;
            }
            if (dto.ShiftTaskId != null) sheetTrack.dtd_dtd_id = dto.ShiftTaskId;

            //save org_id
            var employees = new List<int?>();
            employees.Add(sheetTrack.emp_emp_id);
            var lst = new CreateWaysListReq();
            var waysLst = new WaysListsDb();
            lst.OrgId = dto.OrgId;  lst.TraktorId = dto.TraktorId;
            sheetTrack.exported = (waysLst.checkAllResource(lst, true, employees, false, false, false) == true) ? 1 : 0;
        }

        public void MapTaskFromDto(Tasks task, DriverWayBillItemTaskDto dto, int trakt_id, Boolean? isRepair, string oper)
        {
            if (isRepair == true)
            {
                task.num_runs = 0;
                task.mileage_on_track = 0;
                task.mileage_cargo = 0;
                task.consumption_fact = 0;
                task.type_price = 5;
            }

            if (dto.FieldInfo != null && dto.FieldInfo.Id != null) task.fiel_fiel_id = dto.FieldInfo.Id.Value;
            if (dto.PlantInfo != null && dto.PlantInfo.Id != null)
            {
                task.plant_plant_id = dto.PlantInfo.Id.Value;
                if (oper.Equals("create") && task.fiel_fiel_id != 0 && task.plant_plant_id != null && task.plant_plant_id < 300)
                {
                    var fielplan_id = db.Fields_to_plants.Where(t => t.fiel_fiel_id == task.fiel_fiel_id
                    && t.plant_plant_id == task.plant_plant_id).Select(t => t.fielplan_id).FirstOrDefault();
                    task.plant_plant_id = fielplan_id;
                }

            }
            if (dto.TypeTaskInfo != null && dto.TypeTaskInfo.Id != null) task.tptas_tptas_id = dto.TypeTaskInfo.Id.Value;
            if (dto.EquipInfo != null && dto.EquipInfo.Id != null) task.equi_equi_id = dto.EquipInfo.Id.Value;
            if (dto.AvailableInfo != null ) task.emp_emp_id = dto.AvailableInfo.Id;
            task.description = dto.Comment;
            var rate = GetRate(task.tptas_tptas_id, null, trakt_id, task.equi_equi_id, task.plant_plant_id);
            if (rate != null)
            {
                task.id_rate = rate.Id;
            }
            task.status = 1;
        }

        public int CopyTaskInDriverWayList(int TaskId)
        {
            using (var trans = db.Database.BeginTransaction())
            {
                var newId = CommonFunction.GetNextId(db, "Tasks");
                var copyTask = db.Tasks.Where(d => d.tas_id == TaskId).FirstOrDefault();
                var newTask = new Tasks();
                newTask = copyTask;
                newTask.status = (int)WayListStatus.Issued;
                db.Tasks.Add(newTask);
                db.SaveChanges();
                trans.Commit();
                if (copyTask.Sheet_tracks.is_repair == false || copyTask.Sheet_tracks.is_repair == null) CalculationMotoEndInWayList(copyTask.shtr_shtr_id);
                return newId;
            }
        }
        private void CalculationMotoEndInWayList(int WayListId)
        {
            using (var trans = db.Database.BeginTransaction())
            {
                var list = db.Sheet_tracks.Where(s => s.shtr_id == WayListId && s.shtr_type_id == 2).FirstOrDefault();
              
                var taskList = db.Tasks.Where(t => t.shtr_shtr_id == WayListId).ToList();
                var sumMileageTotal = new decimal?();
                var sumConsumptionFact = new decimal?();
                if (taskList.Any())
                {
                    sumMileageTotal = taskList.Sum(t => t.mileage_total ?? 0);
                    sumConsumptionFact = taskList.Sum(t => t.consumption_fact ?? 0);
                }

                var refuelings = db.Refuelings.Where(r => r.shtr_shtr_id == WayListId).ToList();
                list.remain_fuel = list.left_fuel + (refuelings.Any() ? refuelings.Sum(r => r.volume ?? 0) : 0) - sumConsumptionFact;
                list.moto_end = (list.moto_start ?? 0) + sumMileageTotal;


                var olderList = (from s in db.Sheet_tracks
                                 where s.trakt_trakt_id == list.trakt_trakt_id &&
                                       s.date_begin > list.date_begin &&
                                       (s.is_repair == false || s.is_repair == null) && s.shtr_type_id == 2
                                 let reful = db.Refuelings.Where(r => r.shtr_shtr_id == s.shtr_id).ToList()
                                 let tasks = db.Tasks.Where(t => t.shtr_shtr_id == s.shtr_id).ToList()
                                 orderby s.date_begin
                                 select new WayListCalcInfoDto
                                 {
                                     s = s,
                                     reful = reful.Any() ? reful.Sum(r => r.volume ?? 0) : 0,
                                     ConsumptionFact = tasks.Sum(t => t.consumption_fact ?? 0),
                                     MileageTotal = tasks.Sum(t => t.mileage_total ?? 0)
                                 }).ToList();
                var MotoEnd = list.moto_end;
                var RemainFuel = list.remain_fuel;

                foreach (var e in olderList)
                {
                    e.s.moto_start = MotoEnd;
                    e.s.left_fuel = RemainFuel;
                    MotoEnd = e.s.moto_end = e.s.moto_start + e.MileageTotal;
                    RemainFuel = e.s.remain_fuel = e.s.left_fuel + e.reful - e.ConsumptionFact;
                }

                db.SaveChanges();
                trans.Commit();
            }
        }

        public PrintDriverWayBillInfoDto GetPrintDriverWayInfo(int wbId)
        {
            var result = (from st in db.Sheet_tracks
                          where st.shtr_type_id == 2 // Путевой лист водителя
                          && st.shtr_id == wbId

                          join tasks in db.Tasks on st.shtr_id equals tasks.shtr_shtr_id into tasksLj
                          from t in tasksLj.DefaultIfEmpty()
                          group new PrintDriverWayBillTaskListDto
                          {

                              TaskId = t.tas_id,
                              TypeTask = t.Type_tasks.name,
                              TaskAndPlant = t.Type_tasks.name + " " + t.Fields_to_plants.Plants.name,
                              Available = t.Employees.short_name,
                              Equip = t.Equipments.model,
                              EquipNum = t.Equipments.reg_num,
                              TypeField = (decimal)t.Fields.type,
                              Description = t.description,
                              FieldName = t.Fields.name,
                              FiledId = t.Fields.fiel_id,
                              Volume = t.volume_fact,
                              Salary = t.salary,
                              NumRuns = t.num_runs,
                              MileageTotal = t.mileage_total,
                              NumRefuel = t.num_refuel,
                              ConsumptionRate = t.consumption_rate,
                              ConsumptionFact = t.consumption_fact,
                              MileageCargo = t.mileage_cargo,
                              TotalSalary = t.total_salary,
                              MileageNoCargo = t.mileage_total - t.mileage_cargo,
                              MileageOnTrack = t.mileage_on_track,
                              MileageOnGround = t.mileage_on_ground,
                              WorkTime = t.worktime,
                              NumLifts = t.num_lifts
                              //  Task = t,
                          } by st into g
                          select new PrintDriverWayBillInfoDto
                          {
                              WayBillNum = g.Key.num_sheet,
                              OrgName = "ООО \"Логус-агро\"",
                              Model = g.Key.Traktors.model,
                              ModelNum = g.Key.Traktors.reg_num,
                              Driver = g.Key.Employees.short_name,
                              Certificate = g.Key.Employees.driver_license,
                              StartDate = g.Key.date_begin,
                              EndDate = g.Key.date_end,
                              StartFactDate = g.Key.date_begin_fact,
                              EndFactDate = g.Key.date_end_fact,
                              IssuedFuel = g.Key.issued_fuel,
                              RemainFuel = g.Key.remain_fuel,
                              leftFuel = g.Key.left_fuel,
                              MotoStart = g.Key.moto_start,
                              MotoEnd = g.Key.moto_end,
                              TaskList = g.ToList(),
                          }).FirstOrDefault();

            var refuelings = db.Refuelings.Where(r => r.shtr_shtr_id == wbId).ToList();

            if (result.TaskList.Any())
            {
                result.MileageCargoTotal = result.TaskList.Sum(t => t.MileageCargo);
                result.MileageNoCargoTotal = result.TaskList.Sum(t => t.MileageNoCargo);
                result.MileageOnGroundTotal = result.TaskList.Sum(t => t.MileageOnGround);
                result.MileageOnTrackTotal = result.TaskList.Sum(t => t.MileageOnTrack);
                result.MileageTotal = result.TaskList.Sum(t => t.MileageTotal);
                result.NumRunsTotal = result.TaskList.Sum(t => t.NumRuns);
                result.NumLiftsTotal = result.TaskList.Sum(t => t.NumLifts);
                result.SalaryTotal = result.TaskList.Sum(t => t.Salary);
                result.ConsumptionFact = result.TaskList.Sum(t => t.ConsumptionFact);
                result.WorkTime = result.TaskList.Sum(t => t.WorkTime);
                result.VolumeTotal = result.TaskList.Sum(t => t.Volume);
                result.DoToVolumeTotal = result.MileageCargoTotal * result.VolumeTotal;
                result.TotalSalary = result.TaskList.Sum(t => t.TotalSalary);
                result.MileageTotalStr = "Общий пробег - " + result.MileageTotal + " км";
                result.MileageCargoTotalStr = "Пробег с грузом - " + result.MileageCargoTotal + " км";
                result.MileageNoCargoTotalStr = "Пробег без груза - " + result.MileageNoCargoTotal + " км";
                result.MileageOnTrackTotalStr = "Пробег по трассе - " + result.MileageOnTrackTotal + " км";
                result.MileageOnGroundTotalStr = "Пробег по грунту - " + result.MileageOnGroundTotal + " км";
                result.NumLiftsTotalStr = "Кол-во подъемов - " + result.NumLiftsTotal;
            }

            result.IssuedFuel = refuelings.Any() ? refuelings.Sum(r => r.volume) : 0;
            result.StartYear = result.StartDate.ToString("yy");
            result.StartDD = result.StartDate.ToString("dd");
            result.StartMM = result.StartDate.ToString("MM");
            result.Starthh = result.StartDate.ToString("HH");
            result.Startmm = result.StartDate.ToString("mm");
            result.EndDD = result.EndDate.ToString("dd");
            result.EndMM = result.EndDate.ToString("MM");
            result.Endhh = result.EndDate.ToString("HH");
            result.Endmm = result.EndDate.ToString("mm");
            result.StartFactTime = result.StartFactDate.Value.ToString("dd.MM HH:mm");
            result.EndFactTime = result.EndFactDate.Value.ToString("dd.MM HH:mm");
            result.StartMonth = getMonth(result.StartMM);

            foreach (var item in result.TaskList)
            {
                item.Customer = getObject(item.FieldName, item.Description, item.FiledId, item.Available);
            }
                 var idOrg = Int32.Parse(db.Parameters.Where(t => t.name.Equals("NameOrg")).Select(t => t.value).FirstOrDefault());
                 result.OrgName = db.Organizations.Where(t => t.org_id == idOrg).Select(t => t.name).FirstOrDefault();
                 result.OGRN = db.Parameters.Where(t => t.name.Equals("OGRN")).Select(t => t.value).FirstOrDefault();
          
            return result;
        }

        private string getObject(string name, string description, int fielId, string Available ) 
        {
         
            if (fielId == 115)
            {
                return description;
            }
            else
            {
                return name + "/" + Available;
            }

      
        }
        private string getMonth(string m)
        {
            switch (m)
            {
                case "01":
                    return "Январь";
                case "02":
                    return "Февраль";
                case "03":
                    return "Март";
                case "04":
                    return "Апрель";
                case "05":
                    return "Май";
                case "06":
                    return "Июнь";
                case "07":
                    return "Июль";
                case "08":
                    return "Август";
                case "09":
                    return "Сентябрь";
                case "10":
                    return "Октябрь";
                case "11":
                    return "Ноябрь";
                case "12":
                    return "Декабрь";
                default :
                    return "";
            }
        }

        public DriverWayBillItemDto GetDriverWayBillItemById(int wbId)
        {
            var result = (from w in db.Sheet_tracks
                          where w.shtr_id == wbId
                          select new DriverWayBillItemDto
                          {
                              WayBillId = w.shtr_id,
                              WayListNum = w.num_sheet,
                              IsRepair = w.is_repair,
                              DangerCargo = w.danger_cargo,
                              TraktorId = w.trakt_trakt_id,
                              DriverId = w.emp_emp_id,
                              OrgId = w.org_org_id,
                              UserId = w.user_user_id,
                              DateBegin = w.date_begin,
                              DateEnd = w.date_end,
                              TypeFuel = w.id_type_fuel,
                              AllResource = w.exported == 1 ? true : false,
                              TableTasksData = w.Tasks.Select(t => new DriverWayBillItemTaskDto
                              {
                                  TaskId = t.tas_id,
                                  AvailableInfo = new DrvWbTaskAvailableInfoDto { Id = t.emp_emp_id, Name = t.Employees.short_name },
                                  EquipInfo = new DrvWbTaskEquipmentInfoDto { Id = t.equi_equi_id, Name = t.Equipments.name },
                                  FieldInfo = new DrvWbTaskFieldInfoDto { Id = t.fiel_fiel_id, Name = t.Fields.name },
                                  PlantInfo = new DrvWbTaskPlantInfoDto { Id = t.plant_plant_id, Name = t.plant_plant_id != -1 ? t.Fields_to_plants.Plants.name : "Культура" },
                                  TypeTaskInfo = new DrvWbTaskTypeInfoDto { Id = t.tptas_tptas_id, Name = t.Type_tasks.name },
                                  Comment = !string.IsNullOrEmpty(t.description) ? t.description : null 
                              }).ToList()
                          }).FirstOrDefault();

            if (result != null)
            {
                if (result.DateBegin.HasValue)
                    result.timeStart = result.DateBegin.Value.ToString("HH:mm");
                if (result.DateEnd.HasValue)
                    result.timeEnd = result.DateEnd.Value.ToString("HH:mm");
            }

            return result;
        }

        public bool DeleteDriverWayBillItemById(int wbId)
        {
            using (var trans = db.Database.BeginTransaction())
            {
                try
                {
                    var sheetTrackToDelete = (from st in db.Sheet_tracks
                                              where st.shtr_id == wbId
                                              select st).FirstOrDefault();

                  //  if (sheetTrackToDelete != null)
                  //  {
                  ////      if (sheetTrackToDelete.status == (int) WayListStatus.Closed)
                  ////          throw new BllException(23, "Путевой лист не может быть удален со статусом \"Закрыт\"");
                  //  }
                  //  else
                  //  {
                  //      throw new BllException(24, "Путевой лист не найден");
                  //  }

                    var tasksToDelete = (from t in db.Tasks
                        where t.shtr_shtr_id == wbId
                        select t).ToList();

                    if (tasksToDelete.Any())
                    {
                        db.Tasks.RemoveRange(tasksToDelete);
                    }

                    var refuel = db.Refuelings.Where(r => r.shtr_shtr_id == wbId).ToList();

                    if (refuel.Any())
                    {
                        db.Refuelings.RemoveRange(refuel);
                    }

                    if (sheetTrackToDelete != null)
                    {

                        var detail = db.Day_task_detail.Where(d => d.dtd_id == sheetTrackToDelete.dtd_dtd_id).FirstOrDefault();
                        if (detail != null) detail.status = (int)DayTaskStatus.Assigned;
                        db.Sheet_tracks.Remove(sheetTrackToDelete);
                    }

                    db.SaveChanges();
                    trans.Commit();

                    return true;
                }
                catch (Exception)
                {
                    trans.Rollback();
                    throw;
                }
            }
        }

        public CloseDriverWayBillItemDto GetCloseDriverWayBillItemById(int wbId)
        {
            var result = db.Sheet_tracks
                        .Where(s => s.shtr_id == wbId && s.shtr_type_id == 2 /* Путевой лист водителя */)
                        .Select(s => new CloseDriverWayBillItemDto
                        {
                            WayBillId = s.shtr_id,
                            DriverName = s.Employees.short_name,
                            TraktorName = !string.IsNullOrEmpty(s.Traktors.reg_num) ? s.Traktors.name + " (" + s.Traktors.reg_num + ")" : s.Traktors.name,
                            TraktorId = s.trakt_trakt_id,
                            MotoStart = s.moto_start,
                            MotoEnd = s.moto_end,
                            IssuedFuel = s.issued_fuel,
                            RemainFuel = s.remain_fuel,
                            leftFuel = s.left_fuel,
                            WaysListNum = s.num_sheet,
                            IsRepair = s.is_repair,
                            DateBegin = s.date_begin,
                            DateEnd = s.date_end,
                            OrgId = s.org_org_id,
                        }).FirstOrDefault();

            if (result != null)
            {

                if ( result.TraktorId != null && (result.IsRepair == false || result.IsRepair == null))
                {
                    var lastClosedList = db.Sheet_tracks.
                                    Where(s =>
                                                s.trakt_trakt_id == result.TraktorId &&
                                                s.date_begin < result.DateBegin &&
                                                (s.is_repair == false || s.is_repair == null) &&
                                                s.shtr_type_id == 2
                                            ).
                                    OrderByDescending(s => s.date_begin).
                                    Take(1).FirstOrDefault();
                    if((result.MotoStart == null || result.MotoStart == 0) && lastClosedList !=null) result.MotoStart = lastClosedList.moto_end;
                    if ((result.leftFuel == null || result.leftFuel == 0) && lastClosedList != null) result.leftFuel = lastClosedList.remain_fuel;
                }

                var refuelings = db.Refuelings.Where(r => r.shtr_shtr_id == wbId).ToList();

                var taskList = db.Tasks.Where(t => t.shtr_shtr_id == wbId).Select(t => new CloseDriverWayBillTaskInListDto()
                {
                    TaskId = t.tas_id,
                    FieldName = t.Fields.name,
                    PlantName = t.plant_plant_id != -1 ? t.Fields_to_plants.Plants.name : "Культура",
                    TaskName = t.Type_tasks.name,
                    EquipmentName = t.equi_equi_id.HasValue ? t.Equipments.shot_name : null,
                    Volumefact = t.volume_fact, 
                    RateTrailer = t.rate_trailer,
                    TrailerFact = t.trailer_fact,
                    Salary = t.salary,
                    AvailableName = t.emp_emp_id.HasValue ? t.Employees.short_name : null,
                    TaskNum = t.task_num,
                    NumRuns = t.num_runs,
                    MileageTotal = t.mileage_total,
                    MileageCargo = t.mileage_cargo,
                    MileageNoCargo = t.mileage_total - t.mileage_cargo,
                    MileageOnTrack = t.mileage_on_track,
                    MileageOnGround = t.mileage_on_ground,
                    NumRefuel = t.num_refuel,
                    ConsumptionRate = t.consumption_rate,
                    ConsumptionFact = t.consumption_fact,
                    NumLifts = t.num_lifts,
                    PriceAdd = t.price_add,
                    WorkTime = t.worktime,
                    WorkDay = t.workdays,
                    TypePrice = t.type_price,
                    PriceTotal = t.total_salary,
                    Area = t.area
                }).ToList();

                foreach (var t in taskList)
                {
                    if (t.TypePrice.HasValue)
                    {
                        decimal? k = 0;
                        switch ((TypePrice)((int)t.TypePrice))
                        {
                            case TypePrice.Ga:
                                k = t.Area * t.PriceAdd;
                                break;

                            case TypePrice.Hours:
                                k = t.WorkTime * t.PriceAdd;
                                break;
                            case TypePrice.Kilometers:
                                k = t.MileageTotal * t.PriceAdd;
                                break;
                            case TypePrice.Tonn:
                           k = (t.Volumefact ?? 0) * t.PriceAdd;
                                break;
                            default:
                                k = 0;
                                break;
                        }
                        t.PriceAdd = k;
                    }
                }
                result.Tasks = taskList;

                result.IssuedFuel = refuelings.Any() ? refuelings.Sum(r => r.volume) : 0;
                var TotalConsuptionFact = new decimal?();
                if(taskList.Any()){
                    TotalConsuptionFact = taskList.Sum(t=> t.ConsumptionFact ?? 0);
                }
                result.RemainFuel = (result.leftFuel ?? 0) + (result.IssuedFuel ?? 0) - (TotalConsuptionFact ?? 0);
                if (result.DateBegin.HasValue)
                    result.timeStartFact = result.DateBegin.Value.ToString("HH:mm");
                if (result.DateEnd.HasValue)
                    result.timeEndFact = result.DateEnd.Value.ToString("HH:mm");
            }
            //check org_id
            CreateWaysListReq lst = new CreateWaysListReq();  WaysListsDb  waysLst = new WaysListsDb();
            lst.OrgId = result.OrgId;
            var employees = db.Refuelings.Where(r => r.shtr_shtr_id == wbId).Select(t => t.emp_emp_id).ToList();
            result.AllResource = waysLst.checkAllResource(lst, false, employees, false, false, false);
            return result;
        }

        public decimal? UpdateTotalSalryDriverWayBill(Tasks task)
        {
            decimal? t = task.salary * task.workdays;
            decimal? k = 0;
            if (task.type_price != null)
            {
                switch ((TypePrice)((int)task.type_price))
                {
                    case TypePrice.Ga:
                        if (!task.area.HasValue)
                        {
                            throw new BllException(15, "Не введено значение в поле \"В пер.на усл.га\"");
                        }
                        k = task.area * task.price_add;
                        break;

                    case TypePrice.Hours:
                        if (!task.worktime.HasValue)
                        {
                            throw new BllException(15, "Не введено значение в поле \"Отработано, ч\"");
                        }
                        k = task.worktime * task.price_add;
                        break;
                    case TypePrice.Kilometers:
                        if (!task.mileage_total.HasValue)
                        {
                            throw new BllException(15, "Не введено значение в поле \"Пробег общий, км\"");
                        }
                        k = task.mileage_total * task.price_add;
                        break;
                    case TypePrice.Tonn:
                        if (!task.volume_fact.HasValue)
                        {
                            throw new BllException(15, "Не введено значение в поле \"Факт. объем,т\"");
                        }

                        k = (task.volume_fact ?? 0) * task.price_add;

                        break;
                    default:
                        k = 0;
                        break;
                }
            }
            task.total_salary = t + k.Value;
            return k;
        }

        public bool UpdateCloseDriverWayBillItem(CloseDriverWayBillItemDto req)
        {
            using (var trans = db.Database.BeginTransaction())
            {
                try
                {
                    var bill = db.Sheet_tracks.FirstOrDefault(s => s.shtr_id == req.WayBillId && s.shtr_type_id == 2 /* Путевой лист водителя */);

                    if (bill != null)
                    {
                        bill.moto_start = req.MotoStart;
                        bill.moto_end = req.MotoEnd;
                        bill.issued_fuel = req.IssuedFuel;
                        bill.remain_fuel = req.RemainFuel;
                        bill.left_fuel = req.leftFuel;


                        var timeStart = new TimeSpan(Int32.Parse(req.timeStartFact.Substring(0, 2)), Int32.Parse(req.timeStartFact.Substring(3, 2)), 0);
                        var timeEnd = new TimeSpan(Int32.Parse(req.timeEndFact.Substring(0, 2)), Int32.Parse(req.timeEndFact.Substring(3, 2)), 0);
                        var date = bill.date_begin.Date;

                        bill.date_begin = date + timeStart;
                        bill.date_end = date + timeEnd;

                        if (timeStart > timeEnd)
                            bill.date_end_fact = bill.date_end_fact.Value.AddDays(1);
                    }
                    else
                    {
                        throw new BllException(17, "Не найден путевой лист.");
                    }

                    db.SaveChanges();
                    trans.Commit();
                    if (bill.is_repair == false || bill.is_repair == null) CalculationMotoEndInWayList(bill.shtr_id);
                    return true;
                }
                catch (Exception)
                {
                    trans.Rollback();
                    throw;
                }
            }
        }

        public bool DeleteDriverWayBillTaskById(int taskId)
        {
            using (var trans = db.Database.BeginTransaction())
            {
                try
                {
                    var task = (from t in db.Tasks
                                 where t.tas_id == taskId
                                 select t).FirstOrDefault();

                    if (task != null)
                    {
                        db.Tasks.Remove(task);
                    }
                    var isRepair = db.Sheet_tracks.Where(s => s.shtr_id == task.shtr_shtr_id).Select(s => s.is_repair).FirstOrDefault();
                    db.SaveChanges();
                    trans.Commit();
                    if (isRepair == false || isRepair == null) CalculationMotoEndInWayList(task.shtr_shtr_id);
                    return true;
                }
                catch (Exception)
                {
                    trans.Rollback();
                    throw;
                }
            }
        }

        public CloseDriverWayBillTaskDto GetCloseDriverWayBillTaskById(int taskId)
        {
            var rateFuelList = db.Rate_fuel;
         //   var startSummer  = new DateTime(DateTime.Now.Year, 03, 30);
      //      var endSummer  = new DateTime(DateTime.Now.Year, 11, 1);
            var startSummer = db.Parameters.Where(t => t.name == "summerDate").AsEnumerable().
                Select(t => DateTime.Parse(t.value)).FirstOrDefault();
            var endSummer = db.Parameters.Where(t => t.name == "winterDate").AsEnumerable().
                Select(t => DateTime.Parse(t.value)).FirstOrDefault();
            var task = db.Tasks.AsEnumerable().Where(t => t.tas_id == taskId).Select(t => new CloseDriverWayBillTaskDto
            {
                TaskId = t.tas_id,
                Area = t.area,
                FieldId = t.fiel_fiel_id,
                PlantId = t.plant_plant_id,
                TaskTypeId = t.tptas_tptas_id,
                EquipmentId = t.equi_equi_id,
                Volumefact = t.volume_fact,
                RateShift = t.salary.HasValue ? t.salary.Value : (t.id_rate.HasValue ? t.Task_rate.rate_shift : null),
                RatePiecework = t.price_add.HasValue ? t.price_add.Value : (t.id_rate.HasValue ? t.Task_rate.rate_piecework : null),
                AvailableId = t.emp_emp_id,
                TaskNum = t.task_num,
                NumRuns = t.num_runs,
                IdRate = t.id_rate,
                MileageTotal = t.mileage_total,
                MileageCargo = t.mileage_cargo,
                OrgId = t.Sheet_tracks.org_org_id,
                TypePrice = t.type_price != null ? (int?)t.type_price : null,   
                //Engine_heating = t.num_runs.HasValue ? t.mileage_no_cargo   :
                //((decimal?)rateFuelList.Where(v => v.trakt_trakt_id == t.Sheet_tracks.trakt_trakt_id).Select(v => v.engine_heating).FirstOrDefault()),
                Engine_heating = ((decimal?)rateFuelList.Where(v => v.trakt_trakt_id == t.Sheet_tracks.trakt_trakt_id).Select(v => v.engine_heating).FirstOrDefault()),
                MileageCity = t.mileage,          // по городу
                Icebox = ((decimal?)rateFuelList.Where(v => v.trakt_trakt_id == t.Sheet_tracks.trakt_trakt_id).Select(v => v.icebox).FirstOrDefault()),
                Icebox_per = (decimal?)rateFuelList.Where(v => v.trakt_trakt_id == t.Sheet_tracks.trakt_trakt_id).Select(v =>  v.icebox_per).FirstOrDefault(),
                Coef = ((decimal?)rateFuelList.Where(v => v.trakt_trakt_id == t.Sheet_tracks.trakt_trakt_id).Select(v => v.coef).FirstOrDefault()),
                Heating = ((decimal?)rateFuelList.Where(v => v.trakt_trakt_id == t.Sheet_tracks.trakt_trakt_id).Select(v => v.heating).FirstOrDefault()),
                CoefTrailer = ((decimal?)rateFuelList.Where(v => v.trakt_trakt_id == t.Sheet_tracks.trakt_trakt_id).Select(v => v.trailer).FirstOrDefault()),
                CoefFuel = ((decimal?)rateFuelList.Where(v => v.trakt_trakt_id == t.Sheet_tracks.trakt_trakt_id).Select(v => v.refill).FirstOrDefault()),
                CoefSink = ((decimal?)rateFuelList.Where(v => v.trakt_trakt_id == t.Sheet_tracks.trakt_trakt_id).Select(v => v.sink).FirstOrDefault()),
                MileageOnTrack = t.mileage_on_track,
                MileageOnGround = t.mileage_on_ground,
                NumRefuel = t.num_refuel,
                ConsumptionRate = t.consumption_rate,
                ConsumptionFact = t.consumption_fact,
                NumLifts =  t.num_lifts ,
                Body_lifting = ((decimal?)rateFuelList.Where(v => v.trakt_trakt_id == 
                    t.Sheet_tracks.trakt_trakt_id).Select(v => v.body_lifting).FirstOrDefault()),
                WorkTime = t.worktime,
                Engine_heating_hour = t.engine_hour,
                Icebox_hour = t.icebox_hour,
                Heating_hour = t.heating_hour,
                WorkDay = t.workdays,
                Trailerfact = t.trailer_fact,
                NumFuel = t.num_fuel,
                NumSink = t.num_sink,
                Setting = t.setting,
                CoefSetting = t.coef_setting,
                RateTrailer = t.rate_trailer,
                MileageTrailer = t.mileage_trailer,
                Comment = !string.IsNullOrEmpty(t.description) ? t.description : null,
                FuelCost = t.fuel_cost,
                TraktorId = t.Sheet_tracks.trakt_trakt_id,
                TypeFuel = new DictionaryItemsDTO 
                { 
                    Id = t.Sheet_tracks.id_type_fuel,
                    Name = t.Sheet_tracks.Type_fuel.name,
                    Price = t.Sheet_tracks.Type_fuel.current_price
                },
                CoefCity = (decimal?)rateFuelList.Where(v => v.trakt_trakt_id == t.Sheet_tracks.trakt_trakt_id)
                .Select(v => (t.Sheet_tracks.date_begin.Date >= startSummer && t.Sheet_tracks.date_begin.Date < endSummer) 
                    ? v.summer_city : v.winter_city   ).FirstOrDefault(),
                CoefField = (decimal?)rateFuelList.Where(v => v.trakt_trakt_id == t.Sheet_tracks.trakt_trakt_id)
                              .Select(v => (t.Sheet_tracks.date_begin.Date >= startSummer && t.Sheet_tracks.date_begin.Date < endSummer)
                                  ? v.summer_field : v.winter_field).FirstOrDefault(),
                CoefTrack = (decimal?)rateFuelList.Where(v => v.trakt_trakt_id == t.Sheet_tracks.trakt_trakt_id)
               .Select(v => (t.Sheet_tracks.date_begin.Date >= startSummer && t.Sheet_tracks.date_begin.Date < endSummer)
                ? v.summer_track : v.winter_track).FirstOrDefault(),
                CheckNeedChange = t.num_runs,
                DemIceBox = t.num_runs.HasValue ? t.dim_icebox : null,
            }).FirstOrDefault();
            //
            //check org_id
            CreateWaysListReq lst = new CreateWaysListReq(); lst.OrgId = task.OrgId;
            var employees = new List<int?>(); employees.Add(task.AvailableId); lst.TrailerId = task.EquipmentId;
            var waysLDb = new WaysListsDb();
            task.AllResource = waysLDb.checkAllResource(lst, false, employees, true, false, false);
            //


            return task;
        }

        public int SubmitCloseDriverWayBillTask(CloseDriverWayBillTaskDto req)
        {
            ValidatePlantRequiredOnField(req.FieldId, req.PlantId);

            using (var trans = db.Database.BeginTransaction())
            {
                try
                {
                    var isRepairWayList = db.Sheet_tracks.Where(s => s.shtr_id == req.WayBillId).Select(s => s.is_repair).FirstOrDefault();
                    var isUpdating = req.TaskId.HasValue;

                    Tasks task;
                    if (isUpdating)
                    {
                        var taskFromDb = (from t in db.Tasks
                                         where t.tas_id == req.TaskId.Value
                                         select t).FirstOrDefault();
                        if (taskFromDb != null)
                        {
                            task = taskFromDb;
                        }
                        else
                        {
                            throw new BllException(15, "Не найден путевой лист.");
                        }
                    }
                    else
                    {
                        task = new Tasks
                        {
                            tas_id = CommonFunction.GetNextId(db, "Tasks"),
                            shtr_shtr_id = req.WayBillId,
                            task_num = 0
                        };
                    }

                    if (req.FieldId.HasValue)
                    {
                        task.fiel_fiel_id = req.FieldId.Value;
                    }
                    task.plant_plant_id = req.PlantId;
                    if (req.TaskTypeId.HasValue)
                    {
                        task.tptas_tptas_id = req.TaskTypeId.Value;
                    }
                    task.equi_equi_id = req.EquipmentId;
                    task.volume_fact = req.Volumefact;
                    task.area = req.Area;
                    task.salary = req.RateShift;
                    task.emp_emp_id = req.AvailableId;
                    task.mileage = req.MileageCity;     //пробег по городу
                    task.mileage_no_cargo = req.Engine_heating;    //обогрев двигателя
                    task.coef = req.Coef;
                    task.icebox = req.Icebox;
                    task.setting = req.Setting;
                    task.coef_setting = req.CoefSetting;
                    task.heating = req.Heating;
                    task.heating_hour = req.Heating_hour;
                    task.engine_hour = req.Engine_heating_hour;
                    task.icebox_hour = req.Icebox_hour;
                    task.dim_icebox = req.DemIceBox;
                    task.num_runs = req.NumRuns;
                    task.consumption_fact = req.ConsumptionFact;
                    task.num_refuel = req.NumRefuel;
                    task.mileage_total = req.MileageTotal;
                    task.mileage_cargo = req.MileageCargo;
                    task.description = !string.IsNullOrEmpty(req.Comment) ? req.Comment : null;
                    task.price_add = req.RatePiecework;
                    task.id_rate = req.IdRate;
                    task.worktime = req.WorkTime;
                    task.workdays = req.WorkDay;
                    task.type_price = req.TypePrice;
                    task.mileage_on_track = req.MileageOnTrack;
                    task.mileage_on_ground = req.MileageOnGround;
                    task.num_lifts = req.NumLifts;
                    task.fuel_cost = req.FuelCost;
                    task.mileage_trailer = req.MileageTrailer;
                    task.num_fuel = req.NumFuel;
                    task.num_sink = req.NumSink;
                    task.rate_trailer = req.RateTrailer;
                    task.trailer_fact = req.Trailerfact;
                    UpdateTotalSalryDriverWayBill(task);

                    if (!isUpdating)
                    {
                        db.Tasks.Add(task);
                    }

                    db.SaveChanges();
                    trans.Commit();
                    if (isRepairWayList == false || isRepairWayList == null) CalculationMotoEndInWayList(req.WayBillId);
                    return task.tas_id;
                }
                catch (Exception)
                {
                    trans.Rollback();
                    throw;
                }
            }
        }

        public string UpdateTotalPriceInAllTask()
        {
            using (var trans = db.Database.BeginTransaction())
            {
                try
                {
                    var tasks = db.Tasks.Where(t => t.Sheet_tracks.shtr_type_id == 2).ToList();
                    foreach (var t in tasks)
                    {
                        UpdateTotalSalryDriverWayBill(t);
                    }

                    db.SaveChanges();
                    trans.Commit();
                    return "ok";
                }
                catch (Exception)
                {
                    trans.Rollback();
                    throw;
                }
            }
        }

        public bool SetClosedStatusDriverWayBill(int wbId)
        {
            using (var trans = db.Database.BeginTransaction())
            {
                try
                {
                    var bill = db.Sheet_tracks.FirstOrDefault(s => s.shtr_id == wbId && s.shtr_type_id == 2);
                    if (bill == null)
                    {
                        throw new BllException(21, "Не найден путевой лист");
                    }
                    if ((bill.is_repair == false) && (bill.left_fuel == null || bill.remain_fuel == null || bill.moto_start == null))
                    {
                        throw new BllException(21, "Путевой лист не может быть закрыт если у него не заполены значения полей \"Спидометр (начало)\"  , \"Было горючего\", \"Отстаток горючего\"");
                    }
                    bill.status = (int) WayListStatus.Closed;

                    db.SaveChanges();
                    trans.Commit();
                    return true;
                }
                catch (Exception)
                {
                    trans.Rollback();
                    throw;
                }
            }
        }

        public DriverWayBillItemDto GetDriverWayBillItemFromShiftTask(int stId)
        {
            var dayTaskDetail = (from dtd in db.Day_task_detail
                                 where dtd.dtd_id == stId
                                 select new
                                 {
                                     dtd.trak_trak_id,
                                     dtd.equi_equi_id,
                                     dtd.Equipments.name,
                                     dtd.emp1_emp_id,
                                     dtd.emp2_emp_id,
                                     dtd.shift_num,
                                     dtd.Day_task.dt_date,
                                     dtd.id_agroreq
                                 }).FirstOrDefault();

            var result = (from dtd in db.Day_task_detail
                         where dtd.dtd_id == stId
                         select new DriverWayBillItemDto
                         {
                             TableTasksData = new List<DriverWayBillItemTaskDto>
                             {
                                 new DriverWayBillItemTaskDto
                                 {
                                     FieldInfo = new DrvWbTaskFieldInfoDto
                                     {
                                         Id = dtd.Fields.fiel_id,
                                         Name = dtd.Fields.name,
                                     },
                                     PlantInfo = new DrvWbTaskPlantInfoDto
                                     {
                                         Id = dtd.Fields != null ? db.Fields_to_plants.Where(t => t.plant_plant_id == dtd.Plants.plant_id && t.fiel_fiel_id == dtd.Fields.fiel_id
                                         && (dtd.Plants.plant_id != 36 ? t.date_ingathering.Year == dayTaskDetail.dt_date.Year : t.date_ingathering.Year == dayTaskDetail.dt_date.Year + 1))
                                         .Select(t => t.fielplan_id).FirstOrDefault()
                                         : dtd.Plants.plant_id, 
                                         Name = dtd.plant_plant_id != -1 ? dtd.Plants.short_name : "Культура",
                                         RepairStatus = dtd.plant_plant_id == -1 ? "color: CornflowerBlue;" : null,
                                     },
                                     TypeTaskInfo = new DrvWbTaskTypeInfoDto
                                     {
                                         Id = dtd.Type_tasks.tptas_id,
                                         Name = dtd.Type_tasks.name
                                     },
                                     EquipInfo = new DrvWbTaskEquipmentInfoDto()
                                     {
                                         Id = dayTaskDetail.equi_equi_id,
                                         Name = dayTaskDetail.name
                                     },
                                     AvailableInfo = new DrvWbTaskAvailableInfoDto
                                     {
                                         Id = dtd.Employees2.emp_id,
                                         Name = dtd.Employees2.short_name
                                     }
                                 }
                             },
                             OrgId = dtd.Day_task.org_org_id,
                         }).FirstOrDefault();

            if (result != null)
            {
                if (dayTaskDetail != null)
                {
                    result.AgroreqId = dayTaskDetail.id_agroreq;
                    result.DateBegin = dayTaskDetail.shift_num == 1 ? dayTaskDetail.dt_date.AddDays(1) : dayTaskDetail.dt_date;
                    result.timeStart = dayTaskDetail.shift_num == 1 ? "0700" : "1900";
                    result.timeEnd = dayTaskDetail.shift_num == 1 ? "1900" : "0700";

                    result.TraktorId = dayTaskDetail.trak_trak_id;

                    var drvList = new List<int>();
                    if (dayTaskDetail.emp1_emp_id.HasValue)
                        drvList.Add(dayTaskDetail.emp1_emp_id.Value);
                    if (dayTaskDetail.emp2_emp_id.HasValue)
                        drvList.Add(dayTaskDetail.emp2_emp_id.Value);

                    result.DriverId = db.Employees.
                                    Where(e => drvList.Contains(e.emp_id) && (new[] { 1, 5 }).Contains(e.pst_pst_id)).
                                    Select(e => new DictionaryItemsDTO
                                    {
                                        Name = e.short_name,
                                        Id = e.emp_id
                                    })
                                    .OrderBy(g => g.Name)
                                    .Select(e => e.Id)
                                    .FirstOrDefault();
                    //check Res
                    var employees = new List<int?>();
                    employees.Add(result.DriverId);
                    var lst = new CreateWaysListReq();
                    lst.OrgId = result.OrgId;
                    lst.TraktorId = result.TraktorId;
                    var wLDb = new WaysListsDb();
                    result.AllResource = wLDb.checkAllResource(lst, true, employees, false, false, true);
                    //
                }
            }
            return result;
        }
    }
}
