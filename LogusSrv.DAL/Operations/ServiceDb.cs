﻿using System;
using System.Data.Entity;
using System.Collections.Generic;
using System.Linq;
using LogusSrv.CORE.Exceptions;
using LogusSrv.DAL.Entities.DTO.ActsSzr;
using LogusSrv.DAL.Entities.DTO;
using LogusSrv.DAL.Entities.Req;
using LogusSrv.DAL.DBModel;
using LogusSrv.DAL.Utils;
using LogusSrv.DAL.Enums;
using LogusSrv.DAL.Entities.Res;


namespace LogusSrv.DAL.Operations
{
    
    public class ServiceDb
    {
        protected readonly LogusDBEntities db = new LogusDBEntities();
        List< ServiceModel> checkNum = new List<ServiceModel>();
        public ServiceListResponse GetServiceDetails(GridDataReq req)
        {
            return GetService(req);
        }
        public InfoService GetInfoService()
        {
            InfoService result = new InfoService();
           //услуги
            result.ServiceList = (from s in db.Service
                                  where s.deleted == false
                                  select new DictionaryItemsDTO
                                  {
                                      Id = s.id,
                                      Name = s.name
                                  }).ToList();
            //единицы
            result.TypeDocList = db.Type_salary_doc.Where(t => t.deleted != true). 
                                  Select(t => new DictionaryItemsDTO
                                  {
                                      Id = t.tpsal_id,
                                      Name = t.name
                                  }).OrderBy(e => e.Name).ToList();
            result.AllPlants = db.Plants
                              .Where(f => f.deleted == false).
                              Select(t => new DictionaryItemsDTO
                              {
                                  Name = t.name,
                                  Id = t.plant_id,
                              }).OrderBy(t => t.Name).ToList();
            return result;
        }
        //сохранение в бд
        public string AddService(List<ServiceModel> rows)
        {
          List<string> lst = new List<string>();
            //
              for (int j = 0; j < rows.Count; j++)
            {
                lst.Add(rows[j].Act_number);
            }

       if (lst.Count != lst.Distinct().ToList().Count) { throw new BllException(14, "Акт с таким номером уже существует"); }
            //
            var list = db.Database.SqlQuery<ServiceModel>(
       string.Format(@"select act_num as Act_number from Service_records")).ToList();
            for (int j = 0; j < rows.Count; j++)
            {
                for (int i = 0; i < list.Count; i++)
                {
                    if (list[i].Act_number.Equals(rows[j].Act_number)) { throw new BllException(14, "Акт с таким номером уже существует"); }
                }
            }
            //

            using (var trans = db.Database.BeginTransaction())
            {
                try
                {
                    for (var i = 0; i < rows.Count; i++)
                    {
                        var row = rows[i];
                        var gh = new Service_records
                        {
                           date = row.Date,
                           act_num = row.Act_number,
                           name_service = row.NameWork, 
                           service_id = row.NameService.Id,
                           tpsal_tpsal_id = row.TypeSalary.Id,
                           plant_plant_id =row.NamePlant.Id,
                           price = row.Price,
                           cost = row.Cost,
                           count = row.Count,
                        };
                        db.Service_records.Add(gh);

                     
                    }

                    db.SaveChanges();
                    trans.Commit();
                }
                catch (Exception)
                {
                    trans.Rollback();
                    throw;
                }
            }
            return "success";
        }
        //вывод
        public ServiceListResponse GetService(GridDataReq req)
        {
          List<ServiceModel> list = new List<ServiceModel>();
           var resultList = new  ServiceListResponse();
            list = (from s in db.Service_records
                    join k1 in db.Service on s.service_id equals k1.id into left1
                    from t1 in left1.DefaultIfEmpty()
                    join k2 in db.Type_salary_doc on s.tpsal_tpsal_id equals k2.tpsal_id into left2
                    from t2 in left2.DefaultIfEmpty()
                    join k3 in db.Plants on s.plant_plant_id equals k3.plant_id into left3
                    from t3 in left3.DefaultIfEmpty()
                    group new
                    {
                        Act_number = s.act_num
                    } 
                        by new
                        {
                            Id = s.serv_id,
                            Date = s.date,
                            Act_number = s.act_num,
                            NameWork = s.name_service,
                            Price = s.price,
                            Count = s.count,
                            Cost = s.cost,
                            NameService = new DictionaryItemsDTO
                               {
                                   Id = t1.id,
                                   Name = t1.name,
                               },
                            TypeSalary = new DictionaryItemsDTO
                            {
                                Id = t2.tpsal_id,
                                Name = t2.name,
                            },
                            NamePlant = new DictionaryItemsDTO
                            {
                                Id = t3.plant_id,
                                Name = t3.name,
                            },
                        } into g
                    select new ServiceModel
                       {
                           Id = g.Key.Id,
                           Id1 = g.Key.Id,
                           Date = g.Key.Date,
                           Act_number = g.Key.Act_number,
                           NameWork = g.Key.NameWork,
                           Price = g.Key.Price,
                           Count = g.Key.Count,
                           Cost = g.Key.Cost,
                           NameService = new DictionaryItemsDTO
                               {
                                   Id = g.Key.NameService.Id,
                                   Name = g.Key.NameService.Name
                               },
                           TypeSalary = new DictionaryItemsDTO
                           {
                               Id = g.Key.TypeSalary.Id,
                               Name = g.Key.TypeSalary.Name
                           },
                           NamePlant = new DictionaryItemsDTO
                           {
                               Id = g.Key.NamePlant.Id,
                               Name = g.Key.NamePlant.Name
                           },
                       }).OrderByDescending(m=>m.Date).ToList();

            var CountItems = list.Count();
            var TotalPage = (double)CountItems / (double)req.PageSize;
            double CountPages = TotalPage < 0 ? 1 : Math.Ceiling(TotalPage);

            var gridData = GridHelpers<ServiceModel>.GetGridData(list.AsQueryable(), req);
            resultList.Values = gridData.OrderByDescending(m => Int32.Parse(m.Act_number)).ToList();
            resultList.CountPages = (int)CountPages;
            resultList.CountItems = CountItems;
            return resultList;

        }

        //удаление
        public string DeleteService(List<DelRow> req)
        {
            using (var trans = db.Database.BeginTransaction())
            {

                for (int i = 0; i < req.Count; i++)
                {
                    var n = req[i].rows.Id;
                    var doc = db.Service_records.Where(s => s.serv_id == n).FirstOrDefault();
                    if (doc == null)
                        throw new BllException(15, "Документ не был найден , обновите страницу");
                    db.Service_records.Remove(doc);
                }

                db.SaveChanges();
                trans.Commit();
            }
            return "ok";
        }
        //обновление
        public int UpdateService(List<SaveService> req)
         {
          //удаляем повторяющиеся строки
            for (int i = 0; i < req.Count; i++)
            {
                for (int j = 0; j < req.Count; j++)
                {
                    if (j != i && req[i].Document.Date == req[j].Document.Date &&
                    req[i].Document.Act_number == req[j].Document.Act_number && req[i].Document.NameService.Id == req[j].Document.NameService.Id &&
                    req[i].Document.TypeSalary.Id == req[j].Document.TypeSalary.Id && req[i].Document.NameWork==req[j].Document.NameWork && 
                    req[i].Document.NamePlant.Id==req[j].Document.NamePlant.Id && req[i].Document.Price==req[j].Document.Price &&
                    req[i].Document.Cost == req[j].Document.Cost && req[i].Document.Count == req[j].Document.Count)
                    { req.RemoveAt(j); j--; }
                }
            }
            //проверка  повторяющийся номеров при обнорвлении 
            List<string> lst = new List<string>();
            //
            for (int j = 0; j < req.Count; j++)
            {
                lst.Add(req[j].Document.Act_number);
            }

            if (lst.Count != lst.Distinct().ToList().Count) { throw new BllException(14, "Акт с таким номером уже существует"); }
      


            using (var trans = db.Database.BeginTransaction())
            {
                try
                {
                    for (var i = 0; i < req.Count; i++)
                    {
                       //проверка изменился ли номер
                   var list1 = db.Database.SqlQuery<ServiceModel>(
                    string.Format(@"select act_num as Act_number from Service_records where serv_id=" + req[i].Document.Id)).ToList();

                   if (list1[0].Act_number.Equals(req[i].Document.Act_number))
                   {
                       checkNum = db.Database.SqlQuery<ServiceModel>(
                   string.Format(@"select act_num as Act_number from Service_records where act_num<>"+req[i].Document.Act_number)).ToList();
                   }
                   else {
                       checkNum = db.Database.SqlQuery<ServiceModel>(
                     string.Format(@"select act_num as Act_number from Service_records")).ToList();
                   }

                                for (int i1 = 0; i1 < checkNum.Count; i1++)
                                {
                                    if (checkNum[i1].Act_number.Equals(req[i].Document.Act_number)) { throw new BllException(14, "Акт с таким номером уже существует"); }
                                }
                       
                            var obj = req[i].Document;
                            var updateDic = db.Service_records.Where(o => o.serv_id == obj.Id).FirstOrDefault();
                            updateDic.date = obj.Date;

                             obj = req[i].Document;
                             updateDic = db.Service_records.Where(o => o.serv_id == obj.Id).FirstOrDefault();
                            updateDic.act_num = obj.Act_number;


                            obj = req[i].Document;
                            updateDic = db.Service_records.Where(o => o.serv_id == obj.Id).FirstOrDefault();
                            updateDic.service_id = obj.NameService.Id;

                            obj = req[i].Document;
                            updateDic = db.Service_records.Where(o => o.serv_id == obj.Id).FirstOrDefault();
                            updateDic.tpsal_tpsal_id = obj.TypeSalary.Id;

                            obj = req[i].Document;
                            updateDic = db.Service_records.Where(o => o.serv_id == obj.Id).FirstOrDefault();
                            updateDic.name_service = obj.NameWork;

                            obj = req[i].Document;
                            updateDic = db.Service_records.Where(o => o.serv_id == obj.Id).FirstOrDefault();
                            updateDic.plant_plant_id = obj.NamePlant.Id;

                            obj = req[i].Document;
                            updateDic = db.Service_records.Where(o => o.serv_id == obj.Id).FirstOrDefault();
                            updateDic.price = obj.Price;

                            obj = req[i].Document;
                            updateDic = db.Service_records.Where(o => o.serv_id == obj.Id).FirstOrDefault();
                            updateDic.cost = obj.Cost;

                            obj = req[i].Document;
                            updateDic = db.Service_records.Where(o => o.serv_id == obj.Id).FirstOrDefault();
                            updateDic.count = obj.Count;

                    }
                    db.SaveChanges();
                    trans.Commit();
                }
                catch (Exception)
                {
                    trans.Rollback();
                    throw;
                }
            }
            return req.Count;
        }



    }

}
