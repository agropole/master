﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LogusSrv.DAL.DBModel;
using LogusSrv.DAL.Entities.DTO;
using LogusSrv.DAL.Entities.DTO.SeedingBridle;
using LogusSrv.DAL.Entities.Req.SeedingBridle;
using LogusSrv.DAL.Entities.Res.SeedingBridle;
using LogusSrv.DAL.Enums;
using LogusSrv.DAL.Utils;
using LogusSrv.DAL.Entities.DTO.DayTaskExt;
using LogusSrv.CORE.Exceptions;



namespace LogusSrv.DAL.Operations
{
    public class SeedingBridleDb
    {
        protected readonly LogusDBEntities db = new LogusDBEntities();

        public InfoForSeedingDto GetInfoForSeedingBridle(bool req)
        {

            var result = new InfoForSeedingDto();
            result.OrgList = db.Organizations.
                                Where(o => o.tporg_tporg_id == (int)TypeOrganization.Section && o.deleted != true).
                                Select(o=> new DictionaryItemsDTO
                                {
                                    Id = o.org_id,
                                    Name = o.name
                                }).OrderBy(o=> o.Name).ToList();

            var allOrg = new DictionaryItemsDTO { Id = -1, Name = "Все" };
            result.OrgList.Insert(0, allOrg);
            result.PlantsList = db.Plants.Where(o => o.deleted == false).
                                Select(o => new DictionaryItemsDTO
                                {
                                    Id = o.plant_id,
                                    Name = o.name
                                }).OrderBy(o => o.Name).ToList();
            result.FieldList = db.Fields.Where(o => o.deleted == false && o.type == 1).
                // && o.Organizations.tporg_tporg_id == (int)TypeOrganization.Section).
                              Select(o => new DictionaryItemsDTO
                              {
                                  Id = o.fiel_id,
                                  Name = o.name
                              }).OrderBy(o => o.Name).ToList();
            var allPlants = new DictionaryItemsDTO { Id = -1, Name = "Все" };
            result.PlantsList.Insert(0, allPlants);
            result.FieldList.Insert(0, allPlants);
            result.IndicatorsList = db.Indicators_sorts_plants.
                                    Select(i => new DictionaryItemsDTO
                                    {
                                        Id = i.indsort_id,
                                        Name = i.name,
                                    }).OrderBy(g => g.Name).ToList();

            var yearList = new List<DictionaryItemsDTO>();
            var endYear = DateTime.Now.Year + 30;
            for (var i = 2000; i <= endYear; i++)
            {
                var item = new DictionaryItemsDTO { Id = i, Name = i.ToString() };
                yearList.Add(item);
            }
            result.yearList = yearList.OrderBy(i => i.Id).ToList();

            return result;
        }

        public DictionaryItemsDTO CheckSeedingSpeendingCult(int fielplanId)
        {
            var data = new DictionaryItemsDTO();
            //проверка на удаление
            var tasks = db.Tasks.Where(t => t.plant_plant_id == fielplanId).FirstOrDefault(); //путевые листы
            var salary = db.Salary_details.Where(t => t.plant_plant_id == fielplanId).FirstOrDefault(); // сдельная
            var tmc = db.TMC_Records.Where(t => t.fielplan_fielplan_id == fielplanId).FirstOrDefault(); // тмц
            if (tasks != null || salary != null || tmc != null)
            {
                data.Id = 0;
            }
            else { data.Id = 1; }
            return data;
        }



        public GetSeedingListRes GetSeedingBridleList(GetSeedingListReq req)
        {
            var resultList = new List<GetSeedingListDto>();
            var res = new GetSeedingListRes();
            var orgList = new List<int>();
            var plantList = new List<int>();
            if (req.FilterOrgId == -1)
            {
                orgList = db.Organizations.
                      //    Where(o => o.tporg_tporg_id == (int)TypeOrganization.Section).
                          Select(o => o.org_id).ToList();
            }
            else orgList.Add(req.FilterOrgId);
            if (req.FilterPlantId == -1)
            {
                plantList = db.Plants.          
                          Select(o => o.plant_id).ToList();
            }
            else plantList.Add(req.FilterPlantId);
            var fieldsArr = new List<int>();
            if (req.FilterFieldId == -1)
            {
               fieldsArr = db.Fields_to_plants.Where(f => plantList.Contains(f.plant_plant_id)).Select(f => f.fiel_fiel_id).Distinct().ToList();
            }
            else {
                fieldsArr.Add(req.FilterFieldId);
            }
            var getParams = (from ftp in db.Fields_to_plants
                           where ftp.date_ingathering.Year >= req.FilterYearId - 2
                            && ftp.date_ingathering.Year <= req.FilterYearId + 2
                            && orgList.Contains(ftp.Fields.org_org_id)
                            && fieldsArr.Contains(ftp.fiel_fiel_id)

                             join sort in db.Sorts_plants_data on ftp.fielplan_id equals sort.fielplan_fielplan_id
                             join ind in db.Ind_sorts_plants_data.Where(i => i.indsort_indsort_id == req.FilterIndicatorId) on sort.sortdat_id equals ind.sortdat_sortdat_id
                             group new { ftp, sort, ind } by new { ftp.fielplan_id } into ftpg
                             select new
                             {
                                 fiel_fiel_id = ftpg.Key.fielplan_id,
                                 value_plan = ftpg.Sum(_ => _.ind.value_plan),
                                 value_fact = ftpg.Sum(_ => _.ind.value_fact),
                             }
                        );

            //учет нзп
            var nzpYear = db.Selected_Plants.Where(t => t.type == 2).Select(t => t.year).ToList();
            var list = (
                        from ftp in db.Fields_to_plants
                        where ftp.date_ingathering.Year >= req.FilterYearId - 2
                        && ftp.date_ingathering.Year <= req.FilterYearId + 2
                        && orgList.Contains(ftp.Fields.org_org_id)
                        && fieldsArr.Contains(ftp.fiel_fiel_id)
                        && (ftp.plant_plant_id == 36 ? nzpYear.Contains(ftp.date_ingathering.Year) : true)

                        join ftpVal in getParams on ftp.fielplan_id equals ftpVal.fiel_fiel_id into ftpFLeft
                        from ftpV in ftpFLeft.DefaultIfEmpty()

                        group new { ftp, ftpV.value_fact, ftpV.value_plan }
                        by new { ftp.fiel_fiel_id, ftp.Fields.name } into g1
                        from g2 in
                            (
                                from ftp1 in g1
                                group ftp1 by ftp1.ftp.date_ingathering
                            )
                        group g2 by g1.Key into g3
                        select g3);

            var empItem = new GetSeedingItemListDto();

            var df = list.Where(t => t.Key.name.Equals("кеке")).ToList();

            foreach (var groupField in list)
            {
                var elm = new GetSeedingListDto();
                elm.Field = new FieldInfoDTO
                {
                    Id = groupField.Key.fiel_fiel_id,
                    Name = groupField.Key.name
                };
                elm.FieldName = groupField.Key.name;
                var groupFieldL = groupField.ToList();

                foreach (var gpYear in groupFieldL)
                {
                  /*  if (gpYear.Key.Year == req.FilterYearId - 3)
                    {
                        elm.year1 = new List<GetSeedingItemListDto>();
                        foreach (var gpPlant in gpYear)
                        {
                            var itemDto = new GetSeedingItemListDto { PlantName = gpPlant.ftp.Plants.short_name , Fact = gpPlant.value_fact , Plan = gpPlant.value_plan };
                            elm.year1.Add(itemDto);
                        }
                    } */

                    if (gpYear.Key.Year == req.FilterYearId - 2)
                    {
                        elm.year1 = new List<GetSeedingItemListDto>();
                        foreach (var gpPlant in gpYear)
                        {
                            var itemDto = new GetSeedingItemListDto 
                            {
                                PlantName = gpPlant.ftp.Plants.short_name, 
                                Fact = gpPlant.value_fact,
                                Plan = gpPlant.value_plan,
                                Status = gpPlant.ftp.status
                            };
                            elm.year1.Add(itemDto);
                        }
                    }

                    if (gpYear.Key.Year == req.FilterYearId - 1)
                    {
                        elm.year2 = new List<GetSeedingItemListDto>();
                        foreach (var gpPlant in gpYear)
                        {
                            var itemDto = new GetSeedingItemListDto
                            {
                                PlantName = gpPlant.ftp.Plants.short_name,
                                Fact = gpPlant.value_fact,
                                Plan = gpPlant.value_plan,
                                Status = gpPlant.ftp.status
                            };
                            elm.year2.Add(itemDto);
                        }
                    }

                    if (gpYear.Key.Year == req.FilterYearId)
                    {
                        elm.year3 = new List<GetSeedingItemListDto>();
                        foreach (var gpPlant in gpYear)
                        {
                            var itemDto = new GetSeedingItemListDto
                            {
                                PlantName = gpPlant.ftp.Plants.short_name,
                                Fact = gpPlant.value_fact,
                                Plan = gpPlant.value_plan,
                                Status = gpPlant.ftp.status
                            };
                            elm.year3.Add(itemDto);
                        }
                    }

                    if (gpYear.Key.Year == req.FilterYearId + 1)
                    {
                        elm.year4 = new List<GetSeedingItemListDto>();
                        foreach (var gpPlant in gpYear)
                        {
                            var itemDto = new GetSeedingItemListDto
                            {
                                PlantName = gpPlant.ftp.Plants.short_name,
                                Fact = gpPlant.value_fact,
                                Plan = gpPlant.value_plan,
                                Status = gpPlant.ftp.status
                            };
                            elm.year4.Add(itemDto);
                        }
                    }

                    if (gpYear.Key.Year == req.FilterYearId + 2)
                    {
                        elm.year5 = new List<GetSeedingItemListDto>();
                        foreach (var gpPlant in gpYear)
                        {
                            var itemDto = new GetSeedingItemListDto
                            {
                                PlantName = gpPlant.ftp.Plants.short_name,
                                Fact = gpPlant.value_fact,
                                Plan = gpPlant.value_plan,
                                Status = gpPlant.ftp.status
                            };
                            elm.year5.Add(itemDto);
                        }
                    }

                    /*if (gpYear.Key.Year == req.FilterYearId + 3)
                    {
                        elm.year7 = new List<GetSeedingItemListDto>();
                        foreach (var gpPlant in gpYear)
                        {
                            var itemDto = new GetSeedingItemListDto { PlantName = gpPlant.ftp.Plants.short_name, Fact = gpPlant.value_fact, Plan = gpPlant.value_plan };
                            elm.year7.Add(itemDto);
                        }
                    }*/
                                      
                }
                resultList.Add(elm);
            }
            if (req.FilterPlantId == -1 && req.FilterOrgId == -1 && req.FilterFieldId == -1)
            {
                var emptyFields = db.Fields.Where(t => !fieldsArr.Contains(t.fiel_id) && t.deleted == false && t.type == 1).ToList();
          //         && t.Organizations.tporg_tporg_id == (int)TypeOrganization.Section).ToList();
                foreach (var f in emptyFields)
                {
                    var item = new GetSeedingListDto
                    {
                        FieldName = f.name,
                        Field = new FieldInfoDTO { Id = f.fiel_id, Name = f.name },
                    };
                    resultList.Add(item);
                }
            }
            var CountItems = resultList.Count();
            var TotalPage = (double)CountItems / (double)req.PageSize;
            double CountPages = TotalPage < 0 ? 1 : Math.Ceiling(TotalPage);
            var gridData = GridHelpers<GetSeedingListDto>.GetGridData(resultList.AsQueryable(), req);
            res.Values = gridData.ToList();
            res.CountPages = (int)CountPages;
            res.CountItems = CountItems;

            return res;                            
                                     
        }

        public GetInfoDetailSeeding GetInfoForDetailSeeding(GetDetailSeedingListReq req)
        {
            var result = new GetInfoDetailSeeding();
            result.FieldsList = db.Fields.Where(f => f.type == (int)TypeFields.Fields && f.deleted == false).Select(f => new DictionaryItemFields { Id = f.fiel_id, Name = f.name, Area = f.area }).ToList();

            result.ReproductionList = db.Type_reproduction.Select(r => new DictionaryItemsDTO { Id = r.tprep_id, Name = r.name }).ToList();

            var yearList = new List<DictionaryItemsDTO>();
            var endYear = DateTime.Now.Year + 30;
            for (var i = 2000; i <= endYear; i++)
            {
                var item = new DictionaryItemsDTO { Id = i, Name = i.ToString() };
                yearList.Add(item);
            }
            result.YearsList = yearList.OrderBy(i => i.Id).ToList();
            //урожай
            var checkNzp = db.Fields_to_plants.Where(t => t.plant_plant_id == 36 &&
                t.date_ingathering.Year == req.YearId && t.fiel_fiel_id == req.FieldId).Select(t => t.fielplan_id).FirstOrDefault();

            var nzpYear = db.Selected_Plants.Where(t => t.year == req.YearId).Select(t => t.year).FirstOrDefault();

            result.PlantsAndSorts = db.Plants.Where(p => p.deleted != true && (nzpYear == null ? p.plant_id != 36 : (checkNzp != 0 ? p.plant_id != 36 : true))).Select(p => new DictionaryItemForPlants
                                      {
                                          Id = p.plant_id,
                                          Name = p.name,
                                          Values = p.Type_sorts_plants.Select(sort => new DictionaryItemsDTO { Id = sort.tpsort_id, Name = sort.name }).ToList()
                                      }).OrderBy(g => g.Name).ToList();
            return result;

        }
        public GetDetailSeedingBridleRes GetDetailSeedingList(GetDetailSeedingListReq req)
        {
            var result = new GetDetailSeedingBridleRes();
            var area = db.Fields.Where(f => f.fiel_id == req.FieldId).Select(f=>f.area).FirstOrDefault();
            //учет нзп
            var nzpYear = db.Selected_Plants.Where(t => t.type == 2).Select(t => t.year).ToList();
            var list = (from ftp in db.Fields_to_plants
                        where ftp.fiel_fiel_id == req.FieldId
                        && ftp.date_ingathering.Year  == req.YearId
                        && (ftp.plant_plant_id == 36 ? nzpYear.Contains(ftp.date_ingathering.Year) : true)

                        join jSort in db.Sorts_plants_data on ftp.fielplan_id equals jSort.fielplan_fielplan_id into leftSort
                        from sort in leftSort.DefaultIfEmpty()
                       
                        join ind1 in db.Ind_sorts_plants_data.Where(i => i.indsort_indsort_id == 1) on  sort.sortdat_id equals ind1.sortdat_sortdat_id into leftind1
                        from indArea in leftind1.DefaultIfEmpty()

                        join ind2 in db.Ind_sorts_plants_data.Where(i => i.indsort_indsort_id == 2) on  sort.sortdat_id equals ind2.sortdat_sortdat_id into leftind2
                        from indHarvest in leftind2.DefaultIfEmpty()

                        join ind3 in db.Ind_sorts_plants_data.Where(i => i.indsort_indsort_id == 3) on  sort.sortdat_id equals ind3.sortdat_sortdat_id into leftind3
                        from indCosts1 in leftind3.DefaultIfEmpty()

                        join ind4 in db.Ind_sorts_plants_data.Where(i => i.indsort_indsort_id == 4) on  sort.sortdat_id equals ind4.sortdat_sortdat_id into leftind4
                        from indCosts2 in leftind4.DefaultIfEmpty()
                        
                        group new GetDetailSeedingListDto
                        {
                            Sort = new SeedingSortInfoDto
                            {
                                Name = sort.Type_sorts_plants.name,
                                Id = sort.tpsort_tpsort_id,
                            },
                            Reproduction = new SeedingReproductionInfoDto
                            {
                                Name = sort.Type_reproduction.name,
                                Id = sort.tprep_tprep_id,
                            },
                            Area = new SeedingIndicatorInfoDto
                            {
                                Fact = indArea.value_fact,
                                Plan = indArea.value_plan,
                            },
                            Harvest = new SeedingIndicatorInfoDto
                            {
                                Fact = indHarvest.value_fact,
                                Plan = indHarvest.value_plan,
                            },
                            CostGA = new SeedingIndicatorInfoDto
                            {
                                Fact = indCosts1.value_fact,
                                Plan = indCosts1.value_plan,
                            },
                            CostTonn = new SeedingIndicatorInfoDto
                            {
                                Fact = indCosts2.value_fact,
                                Plan = indCosts2.value_plan,
                            }
                        } by  new {ftp.plant_plant_id, ftp.Plants.name, ftp.fielplan_id, ftp.status} into g
                        select new GetDetailPlantSeeding
                        {
                            Plant = new SeedingPlantInfoDto {
                                
                                Id = g.Key.plant_plant_id,
                                Name = g.Key.name
                            },
                            FieldPlantId = g.Key.fielplan_id,
                            Status = g.Key.status,
                            Sorts = g.Where(i =>i.Sort.Id != null).OrderBy(i=> i.Sort.Name).ToList()
                        }).OrderBy(i=>i.Plant.Name).ToList();
            result.area = area;
            result.Values = list;
            return result;
        }

            public GetDetailSeedingBridleRes UpdateDetailSeeing(GetDetailSeedingBridleRes req )
            {
                using (var trans = db.Database.BeginTransaction())
                {
                    try
                    {
                        var error = from val in req.Values
                                group val
                                by new {val.Plant.Id , val.Plant.Name}  into g
                                where g.Count() > 1
                                select new { id = g.Key.Id, Name = g.Key.Name, Values = g.ToList()};

                        if (error.Any())
                        {
                            var err = string.Join(", ", error.Select(p => p.Name));
                            throw new BllException(14, "Добавлены более одной и той же культуры :" + err);
                        }
                        //if(c.)
                        var updateItemList = req.Values.Where(i => i.FieldPlantId != null).ToList();
                        var crateItemList = req.Values.Where(i => i.FieldPlantId == null).ToList();
                        var wasItemList = db.Fields_to_plants.Where(f => f.fiel_fiel_id == req.FieldId && f.date_ingathering.Year == req.YearId).Select(f => f.fielplan_id).ToList();
                        var deleteItem = wasItemList.Where(r => !updateItemList.Select(u => u.FieldPlantId).Contains(r)).ToList();
                        //

                        //todo autoincriment key 
                        var sortId = CommonFunction.GetNextId(db,"Sorts_plants_data"); 
                        var IndId = CommonFunction.GetNextId(db,"Ind_sorts_plants_data"); 
                        if (updateItemList.Any())
                        {
                            foreach (var item in updateItemList)
                            {
                                var areaFact = item.Sorts.Sum(p => p.Area.Fact);
                                var areaPlan = item.Sorts.Sum(p => p.Area.Plan);
                                var oldItem = db.Fields_to_plants.Where(o => o.fielplan_id == item.FieldPlantId).FirstOrDefault();
                                oldItem.plant_plant_id = (int)item.Plant.Id;
                                oldItem.fiel_fiel_id = (int)req.FieldId;
                                oldItem.date_ingathering = new DateTime((int)req.YearId, 1, 1);
                                oldItem.area_plant = (areaFact != null && areaFact!=0) ? areaFact.Value : (areaPlan ?? 0); //do

                                var oldSorts = db.Sorts_plants_data.Where(s => s.fielplan_fielplan_id == item.FieldPlantId).ToList();
                                var itemOld = oldSorts.Select(o => o.sortdat_id).ToList();
                                var oldInd = db.Ind_sorts_plants_data.Where(ind => itemOld.Contains((int)ind.sortdat_sortdat_id)).ToList();

                                db.Ind_sorts_plants_data.RemoveRange(oldInd);
                                db.Sorts_plants_data.RemoveRange(oldSorts);

                                UpdateSortAndInd(item, ref sortId, ref IndId);
                            }
                        }

                        if (crateItemList.Any())
                        {
                            var fielplanId = CommonFunction.GetNextId(db,"Fields_to_plants");
                            foreach (var item in crateItemList)
                            {
                                var areaFact = item.Sorts.Sum(p => p.Area.Fact);
                                var areaPlan = item.Sorts.Sum(p => p.Area.Plan);
                                var newItem = new Fields_to_plants();
                                newItem.plant_plant_id = (int)item.Plant.Id;
                                newItem.fiel_fiel_id = (int)req.FieldId;
                                newItem.date_ingathering = new DateTime((int)req.YearId, 1, 1);
                                newItem.date_sowing = new DateTime((int)req.YearId, 1, 1);
                                newItem.status = (int)StatusFtp.Approved;
                                newItem.area_plant = (areaFact != null && areaFact != 0) ? areaFact.Value : (areaPlan ?? 0); //do
                                item.FieldPlantId = fielplanId;
                                db.Fields_to_plants.Add(newItem);
                                UpdateSortAndInd(item, ref sortId, ref IndId);
                                fielplanId++;
                            }
                        }
                        //удаление севооборота
                        if (deleteItem.Any())
                        {
                                foreach (var item in deleteItem)
                                {
                                    var oldItem = db.Fields_to_plants.Where(o => o.fielplan_id == item).FirstOrDefault();
                                    var oldSorts = db.Sorts_plants_data.Where(s => s.fielplan_fielplan_id == item).ToList();

                                    var itemOld = oldSorts.Select(o => o.sortdat_id).ToList();
                                    var oldInd = db.Ind_sorts_plants_data.Where(ind => itemOld.Contains((int)ind.sortdat_sortdat_id)).ToList();

                                    db.Ind_sorts_plants_data.RemoveRange(oldInd);
                                    db.Sorts_plants_data.RemoveRange(oldSorts);
                                    db.Fields_to_plants.Remove(oldItem);
                                }
                        }
                        db.SaveChanges();
                        trans.Commit();

                        return GetDetailSeedingList(new GetDetailSeedingListReq { FieldId = (int)req.FieldId, YearId = (int)req.YearId });
                    }
                    catch (Exception)
                    {
                        trans.Rollback();
                        throw;
                    }
                }
            }

            public string ApprovedSeedingBridle(GetDetailSeedingBridleRes req)
            {
                using (var trans = db.Database.BeginTransaction())
                {
                    try
                    {
                        foreach (var item in req.Values)
                        {
                            var feildAndPlants = db.Fields_to_plants.Where(f => f.fielplan_id == item.FieldPlantId).FirstOrDefault();
                            feildAndPlants.status = (int)StatusFtp.Approved;
                        }
                        db.SaveChanges();
                        trans.Commit();
                        return "approved";
                    }
                    catch (Exception)
                    {
                        trans.Rollback();
                        throw;
                    }
                }
            }

            private void UpdateSortAndInd(GetDetailPlantSeeding sorts, ref int sortId, ref int IndId)
            {

                foreach (var item in sorts.Sorts)
                {
                    var newSort = new Sorts_plants_data();
                   // newSort.sortdat_id = sortId;

                    if (item.Sort != null)
                    {
                        newSort.tpsort_tpsort_id = item.Sort.Id;
                        newSort.fielplan_fielplan_id = sorts.FieldPlantId;
                        newSort.tprep_tprep_id = item.Reproduction.Id;
                    }
          
                    var indList = new List<Ind_sorts_plants_data>();
                    //показатели
                    if (item.Area != null)
                    {
                        var newArea = new Ind_sorts_plants_data();
                        newArea.sortdat_sortdat_id = sortId;
                        newArea.indsort_indsort_id = 1;
                     //   newArea.inddata_id = IndId;
                        newArea.value_plan = item.Area.Plan;
                        newArea.value_fact = item.Area.Fact;
                        indList.Add(newArea);
                     //   IndId++;
                    }


                    if (item.Harvest != null)
                    {
                        var newHarvest = new Ind_sorts_plants_data();
                        newHarvest.sortdat_sortdat_id = sortId;
                        newHarvest.indsort_indsort_id = 2;
                     //   newHarvest.inddata_id = IndId;
                        newHarvest.value_plan = item.Harvest.Plan;
                        newHarvest.value_fact = item.Harvest.Fact;
                        indList.Add(newHarvest);
                    //    IndId++;
                    }
        
                    if (item.CostGA != null)
                    {
                        var newCostGA = new Ind_sorts_plants_data();
                        newCostGA.sortdat_sortdat_id = sortId;
                        newCostGA.indsort_indsort_id = 3;
                   //     newCostGA.inddata_id = IndId;
                        newCostGA.value_plan = item.CostGA.Plan;
                        newCostGA.value_fact = item.CostGA.Fact;
                        indList.Add(newCostGA);
                    //    IndId++;
                    }

                    if (item.CostTonn != null)
                    {
                        var newCostTonn = new Ind_sorts_plants_data();
                        newCostTonn.sortdat_sortdat_id = sortId;
                        newCostTonn.indsort_indsort_id = 4;
                        //   newCostTonn.inddata_id = IndId;
                        newCostTonn.value_plan = item.CostTonn.Plan;
                        newCostTonn.value_fact = item.CostTonn.Fact;
                        indList.Add(newCostTonn);
                        //    IndId++;
                    }

                    db.Sorts_plants_data.Add(newSort);
                    db.SaveChanges();
                    db.Ind_sorts_plants_data.AddRange(indList);
                    sortId++;
                }
            }


            public string CloseSeedingBridle(CloseFieldModel req)
            {
                using (var trans = db.Database.BeginTransaction())
                {
                    try
                    {
                        var itemToClose = db.Fields_to_plants
                            .Where(f => f.fiel_fiel_id == req.FieldId &&
                                f.date_ingathering.Year == req.YearId)
                                .ToList();
                        foreach (var item in itemToClose)
                        {
                            item.status = (int)StatusFtp.Closed;
                            item.date_close = req.DateClose;
                        }
                        db.SaveChanges();
                        trans.Commit();
                        return "success";
                    }
                    catch (Exception)
                    {
                        trans.Rollback();
                        throw;
                    }
                }
            }
    }
}
