﻿using System;
using System.Data.Entity;
using System.Collections.Generic;
using System.Linq;
using LogusSrv.CORE.Exceptions;
using LogusSrv.DAL.Entities.DTO.ActsSzr;
using LogusSrv.DAL.Entities.DTO;
using LogusSrv.DAL.Entities.Req;
using LogusSrv.DAL.DBModel;
using LogusSrv.DAL.Utils;
using LogusSrv.DAL.Enums;
using LogusSrv.DAL.Entities.Res;
using LogusSrv.DAL.Entities.Res.SeedingBridle;
using LogusSrv.DAL.Entities.DTO.SeedingBridle;


namespace LogusSrv.DAL.Operations
{

    public class TechCardTempDb
    {
        protected readonly LogusDBEntities db = new LogusDBEntities();
        public TechCardDb techcard = new TechCardDb();
        public List<TCItemTemp> GetTCTemp()
        {
            var res = new List<TCItemTemp>();
            res = (from t in db.TC_temp
                   where t.type_module == (int)TypeModule.Template
                   select new TCItemTemp
                   {
                       Id = t.id,
                       Plant = db.Plants.Where(q => q.plant_id == (int)t.plant_plant_id).Select(q=>q.name).FirstOrDefault(), //не трогать
                       Sort = t.Type_sorts_plants.name,
                       PlantId = (int)t.plant_plant_id,
                       SortId = t.tpsort_tpsort_id,
                   }).ToList();
                 for (int i = 0; i < res.Count; i++)
                 res[i].Number = i + 1;
                 return res;
        }

        public List<int> GetPlantByTwoYears()
        {
            var res = new List<int>();

            var yearCur = DateTime.Now.Year;
            res = db.Fields_to_plants.Where(t => t.date_ingathering.Year == yearCur ||
                t.date_ingathering.Year == (yearCur + 1)).Select(t => t.plant_plant_id).Distinct().ToList();
            return res;
        }

        //список годов по культуре
        public List<DictionaryItemsDTO> GetYearsCult(int plantId)
        {
            var curYear = DateTime.Now.Year;
            var yearList = new List<DictionaryItemsDTO>();
            var res = db.Fields_to_plants.Where(t => t.plant_plant_id == plantId && t.date_ingathering.Year >= curYear).
                Select(t => new DictionaryItemsDTO { Id = t.date_ingathering.Year })
                .Distinct().OrderByDescending(t => t.Id).ToList();
            for (int i = 0; i < res.Count; i++) {
                yearList.Add(new DictionaryItemsDTO { Id = res[i].Id, Name = res[i].Id.ToString() });
            }
            return yearList;
        }

        //Культура-сорт
        public List<DictionaryItemForPlants> GetPlantSort()
        {
            var result = new GetInfoDetailSeeding();
            result.PlantsAndSorts = db.Plants.Where(p => p.deleted != true).Select(p => new DictionaryItemForPlants
            {
                Id = p.plant_id,
                Name = p.name,
                Values = p.Type_sorts_plants.Select(sort => new DictionaryItemsDTO { Id = sort.tpsort_id, Name = sort.name }).ToList()
            }).OrderBy(g => g.Name).ToList();

            return result.PlantsAndSorts;
        }

        /////////
        public string CreateUpdateTC_temp(TCTempDetailModel tc)
        {
            for (int i = 0; i < tc.Works.Count; i++)
            {
                //utc время
                tc.Works[i].DateStart = tc.Works[i].DateStart.Value.AddHours(3);
                tc.Works[i].DateEnd = tc.Works[i].DateEnd.Value.AddHours(3);

                var dis = tc.Works.Where(v => DateTime.Compare((DateTime)tc.Works[i].DateStart, (DateTime)v.DateStart) == 0 && v.Name.Name == tc.Works[i].Name.Name
                && v.Id != tc.Works[i].Id && DateTime.Compare((DateTime)tc.Works[i].DateEnd, (DateTime)v.DateEnd) == 0 &&
                v.UnicFlag == tc.Works[i].UnicFlag && v.Count == tc.Works[i].Count && v.Factor == tc.Works[i].Factor &&
                v.Workload == tc.Works[i].Workload).Select(v => v.index).Sum();
                if (dis > 0) { throw new BllException(14, "Невозможно сохранить две одинаковые записи в назначенных работах"); }
                if (String.IsNullOrEmpty(tc.Works[i].Name.Name)) { throw new BllException(14, "Не выбрано название работы"); }
            }

            for (int i = 0; i < tc.SupportStuff.Count; i++)
            {
                if (String.IsNullOrEmpty(tc.SupportStuff[i].NameSelected.Name)) { throw new BllException(14, "Не заполнено поле Наименование работы для Вспомогательного персонала"); }

            }
            for (int i = 0; i < tc.DataServices.Count; i++)
            {
                if (String.IsNullOrEmpty(tc.DataServices[i].NameService.Name)) { throw new BllException(14, "Не выбран тип услуг"); }
            }
            for (int i = 0; i < tc.DataChemicalFertilizers.Count; i++)
            {
                if (tc.DataChemicalFertilizers[i].NameFertilizer == null || tc.DataChemicalFertilizers[i].NameFertilizer.Name == null) 
                { throw new BllException(14, "Не заполнено поле Материал"); }
                if (String.IsNullOrEmpty(tc.DataChemicalFertilizers[i].NameSelectedWorkForMaterial.Name)) { throw new BllException(14, "Не заполнено поле Наименование работы для материалов"); }
            }

            for (int i = 0; i < tc.DataSeed.Count; i++)
            {
                if (tc.DataSeed[i].NameSeed == null || tc.DataSeed[i].NameSeed.Name == null) { throw new BllException(14, "Не заполнено поле Материал"); }
                if (String.IsNullOrEmpty(tc.DataSeed[i].NameSelectedWorkForMaterial.Name)) { throw new BllException(14, "Не заполнено поле Наименование работы для материалов"); }
            }
            for (int i = 0; i < tc.DataSZR.Count; i++)
            {
                if (tc.DataSZR[i].NameSZR == null || tc.DataSZR[i].NameSZR.Name == null) { throw new BllException(14, "Не заполнено поле Материал"); }
                if (String.IsNullOrEmpty(tc.DataSZR[i].NameSelectedWorkForMaterial.Name)) { throw new BllException(14, "Не заполнено поле Наименование работы для материалов"); }
            }
            for (int i = 0; i < tc.DataDopMaterial.Count; i++)
            {
                if (tc.DataDopMaterial[i].NameDopMaterial == null || tc.DataDopMaterial[i].NameDopMaterial.Name == null)
                { throw new BllException(14, "Не заполнено поле Материал"); }
                if (String.IsNullOrEmpty(tc.DataDopMaterial[i].NameSelectedWorkForMaterial.Name)) { throw new BllException(14, "Не заполнено поле Наименование работы для материалов"); }
            }
            using (var trans = db.Database.BeginTransaction())
            {
                try
                {
                    var curTC = tc.TC;
                    if (!curTC.Id.HasValue)
                    {
                        //insert TC
                        var newId = (db.TC_temp.Max(t => (int?)t.id) ?? 0) + 1;
                        curTC.Id = newId;
                        db.TC_temp.Add(new TC_temp
                        {
                            id = (int)curTC.Id,
                            plant_plant_id = curTC.PlantId,
                            tpsort_tpsort_id = curTC.SortId,
                            type_module = curTC.TypeModuleId,
                       
                        });
                    }
                    else
                    {
                        //update TC_temp
                        var updatedTC = (from t in db.TC_temp where t.id == curTC.Id select t).FirstOrDefault();
                        updatedTC.plant_plant_id = curTC.PlantId;
                        updatedTC.tpsort_tpsort_id = curTC.SortId;
                        var oldP = db.TC_param_value_temp.Where(o => o.id_temp == curTC.Id).ToList();
                        var oldTech = db.TC_operation_to_Tech_temp.Where(o => o.TC_operation_temp.id_temp == curTC.Id).ToList();
                        db.TC_operation_to_Tech_temp.RemoveRange(oldTech);
                        db.TC_param_value_temp.RemoveRange(oldP);
                        //TODO
                    }

                    var oldOp = db.TC_operation_temp.Where(o => o.id_temp == curTC.Id).ToList();

                

                    ////insert параметры свода затрат
                    //var total = tc.DataTotal;
                   var li = 0;
                
                    //insert услуги
                    if (tc.DataServices != null)
                    {

                        for (int i = 0; i < tc.DataServices.Count; i++)
                        {
                            tc.DataServices[i].Id = null;
                        }

                        var doc = db.TC_to_service_temp.Where(s => s.id_temp == curTC.Id.Value).ToList();
                        if (doc != null)
                        {
                            db.TC_to_service_temp.RemoveRange(doc);
                        }

                        db.SaveChanges();

                        //

                        insertServices(tc.DataServices, curTC.Id.Value);
                    }

                    //insert задачи
                    var works = tc.Works;
                    li = works.Count;
                    var workList = new List<TC_operation_temp>();
                    var pIDs = new[] {33, 51};
                    var pNames = new[] { "Workload", "UnicFlag" };
                    var newOperId = db.TC_operation_temp.Max(t => (int?)t.id) ?? 0;
                    newOperId++;

                    for (var i = 0; i < li; i++)
                    {
                        var w = works[i];
                        var wType = w.GetType();

                        int operId;
                        TC_operation_temp oper;
                        if (w.Id != null)
                        {
                            operId = w.Id.Value;
                            oper = (from o in db.TC_operation_temp
                                    where o.id == operId
                                    select o).FirstOrDefault();
                            oper.id_temp = curTC.Id.Value;
                            oper.tptas_tptas_id = w.Name.Id.Value;
                            oper.multiplicity = w.Count;
                            oper.date_start = w.DateStart;
                            oper.date_finish = w.DateEnd;
                            oper.factor = w.Factor;

                            oldOp.Remove(oldOp.Where(p => p.id == w.Id.Value).FirstOrDefault());
                        }
                        else
                        {
                            oper = new TC_operation_temp
                            {
                                id = newOperId,
                                id_temp = (int)curTC.Id,
                                tptas_tptas_id = (int)w.Name.Id,
                                multiplicity = w.Count,
                                date_start = w.DateStart,
                                date_finish = w.DateEnd,
                                factor = w.Factor
                            };
                            db.TC_operation_temp.Add(oper);
                            operId = newOperId;
                            newOperId++;
                        }

                        workList.Add(oper);
                        for (var j = 0; j < pIDs.Length; j++)
                        {
                            var newParamValue = new TC_param_value_temp
                            {
                                id_temp = (int)curTC.Id,
                                plan = 1,
                                id_tc_operation_temp = operId,
                                id_tc_param_temp = pIDs[j],
                                value = (float?)wType.GetProperty(pNames[j]).GetValue(w),
                            };
                            db.TC_param_value_temp.Add(newParamValue);
                        }

                        var equip = w.Equipment;
                        if (equip != null)
                        {
                            for (var j = 0; j < equip.Count; j++)
                            {
                                db.TC_operation_to_Tech_temp.Add(new TC_operation_to_Tech_temp
                                {
                                    it_tc_operation_temp = operId,
                                    equi_equi_id = equip[j].Id
                                });
                            }
                        }

                        var traks = w.Tech;
                        if (traks != null)
                        {
                            for (var j = 0; j < traks.Count; j++)
                            {
                                db.TC_operation_to_Tech_temp.Add(new TC_operation_to_Tech_temp
                                {
                                    it_tc_operation_temp = operId,
                                    trakt_trakt_id = traks[j].Id
                                });
                            }
                        }
                    }
                    if (oldOp.Count >= 0)
                    {
                        db.TC_operation_temp.RemoveRange(oldOp);
                    }
                    db.SaveChanges();

                    //insert вспомогательный персонал
                    var support = tc.SupportStuff;
                    for (var i = 0; i < support.Count; i++)
                    {
                        var s = support[i];
                        var staffCount = new TC_param_value_temp
                        {
                            id_temp = (int)curTC.Id,
                            id_tc_param_temp = 36,
                            id_tc_operation_temp = (int)GetOperationId(workList, (int)s.NameSelected.Id),
                            value = s.Count,
                            plan = 1
                        };
                        //расценка р/ПД
                        var staffPrice = new TC_param_value_temp
                        {
                            id_temp = (int)curTC.Id,
                            id_tc_param_temp = 65,
                            id_tc_operation_temp = (int)GetOperationId(workList, (int)s.NameSelected.Id),
                            value = s.Price,
                            plan = 1
                        };
                        //наименование работ
                        var staffNameWork = new TC_param_value_temp
                        {
                            id_temp = (int)curTC.Id,
                            id_tc_param_temp = 64,
                            id_tc_operation_temp = (int)GetOperationId(workList, (int)s.NameSelected.Id),
                            value = s.NameSelected.Id,
                            plan = 1
                        };
                        //id из TC_Operation
                        var staffIndex = new TC_param_value_temp
                        {
                            id_temp = (int)curTC.Id,
                            id_tc_param_temp = 66,
                            id_tc_operation_temp = (int)GetOperationId(workList, (int)s.NameSelected.Id),
                            value = s.id_tc_oper,
                            plan = 1
                        };

                        db.TC_param_value_temp.Add(staffCount);
                        db.TC_param_value_temp.Add(staffNameWork);
                        db.TC_param_value_temp.Add(staffPrice);
                        db.TC_param_value_temp.Add(staffIndex);
                    }
                    db.SaveChanges();

                    var szr = tc.DataSZR;
                    for (var i = 0; i < szr.Count; i++)
                    {
                        var s = szr[i];
                        saveMaterials(curTC.Id, GetOperationId(workList, (int)s.NameSelectedWorkForMaterial.Id), s.NameSZR.Id,
                             (float)s.ConsumptionRate, s.id_tc_oper);
                    }

                    var fertilizers = tc.DataChemicalFertilizers;
                    for (var i = 0; i < fertilizers.Count; i++)
                    {
                        var s = fertilizers[i];
                        saveMaterials(curTC.Id, GetOperationId(workList, (int)s.NameSelectedWorkForMaterial.Id), s.NameFertilizer.Id,
                            (float)s.ConsumptionRate, s.id_tc_oper);
                    }

                    var seeds = tc.DataSeed;
                    for (var i = 0; i < seeds.Count; i++)
                    {
                        var s = seeds[i];
                        saveMaterials(curTC.Id, GetOperationId(workList, (int)s.NameSelectedWorkForMaterial.Id), s.NameSeed.Id,
                             (float)s.ConsumptionRate, s.id_tc_oper);
                    }
                    var dopMaterials = tc.DataDopMaterial;
                    for (var i = 0; i < dopMaterials.Count; i++)
                    {
                        var s = dopMaterials[i];
                        saveMaterials(curTC.Id, GetOperationId(workList, (int)s.NameSelectedWorkForMaterial.Id), s.NameDopMaterial.Id,
                             (float)s.ConsumptionRate, s.id_tc_oper);
                    }
                    db.SaveChanges();
                    trans.Commit();
                }
                catch (Exception)
                {
                    trans.Rollback();
                    throw;
                }
            }
            return "success";
        }
        //////////
        private void addParam(int id_temp, int id_tc_param_temp, float? value)
        {
            db.TC_param_value_temp.Add(new TC_param_value_temp
            {
                id_temp = id_temp,
                id_tc_param_temp = id_tc_param_temp,
                value = value,
                plan = 1
            });
        }

        private void saveMaterials(int? tc_id, int? operId, int? matId, float rate, int? id_tc_oper)
        {
            db.TC_param_value_temp.Add(CreateMaterialParamValue(tc_id, 48, operId, matId, rate));
            db.TC_param_value_temp.Add(CreateMaterialParamValue(tc_id, 67, operId, matId, (float)id_tc_oper));
        }
        private void insertServices(List<DataServiceTemp> list, int id_temp)
        {
            var ss = new List<TC_to_service_temp>();
            for (var i = 0; i < list.Count; i++)
            {
                var l = list[i];
                TC_to_service_temp s;
                if (l.Id.HasValue)
                {
                    s = db.TC_to_service_temp.Where(p => p.id == l.Id.Value).First();
                    s.id_service = l.NameService.Id.Value;
                    s.cost = (float)l.Cost;
                }
                else
                {
                    s = new TC_to_service_temp
                    {
                        id_temp = id_temp,
                        id_service = l.NameService.Id.Value,
                        cost = (float)l.Cost
                    };
                    ss.Add(s);
                }
            }
            if (ss.Count > 0)
            {
                db.TC_to_service_temp.AddRange(ss);
            }
            //



            db.SaveChanges();
        }
        private int? GetOperationId(List<TC_operation_temp> opers, int id)
        {
            for (var i = 0; i < opers.Count; i++)
            {
                if (opers[i].tptas_tptas_id == id)
                {
                    return opers[i].id;
                }
            }
            return null;
        }

        private TC_param_value_temp CreateMaterialParamValue(int? idtc, int idtcparam, int? idtcoperation, int? matmatid, float value)
        {
            var m = new TC_param_value_temp
            {
                id_temp = (int)idtc,
                id_tc_param_temp = idtcparam,
                id_tc_operation_temp = idtcoperation,
                mat_mat_id = (int)matmatid,
                value = value,
                plan = 1
            };
            return m;
        }

        public string DelTC_temp(int id)
        {
            using (var trans = db.Database.BeginTransaction())
            {
                try
                {
                    var tc = db.TC_temp.Where(o => o.id == id).FirstOrDefault();
                    var oldOp = db.TC_operation_temp.Where(o => o.id_temp == id).ToList();
                    var oldP = db.TC_param_value_temp.Where(o => o.id_temp == id).ToList();
                    var oldTech = db.TC_operation_to_Tech_temp.Where(o => o.TC_operation_temp.id_temp == id).ToList();
                    var oldServ =   db.TC_to_service_temp.Where(o => o.id_temp == id).ToList();

                    db.TC_operation_to_Tech_temp.RemoveRange(oldTech);
                    db.TC_param_value_temp.RemoveRange(oldP);
                    db.TC_operation_temp.RemoveRange(oldOp);
                    db.TC_to_service_temp.RemoveRange(oldServ);
                    db.TC_temp.Remove(tc);
                    db.SaveChanges();
                    trans.Commit();
                }
                catch (Exception)
                {
                    trans.Rollback();
                    throw;
                }
            }
            return "success";
        }

        public RateResModel GetRateTemp(RateTechReq req)
        {
            var rate = new RateResModel(); 
            //id_tc_operation
            if (String.IsNullOrEmpty(req.operationId.ToString()))
            {
                rate.Id = (db.TC_operation_temp.Max(t => (int?)t.id) ?? 0) + 1;
            }
            return rate;
        }

        ///////////////////
        public TCTempDetailModel GetTC_tempById(int id)
        {
            var tc = new TCTempDetailModel();
            var works = (from w in db.TC_operation_temp where w.id_temp == id orderby w.date_start select w).ToList();
            var ps = (from p in db.TC_param_value_temp where p.id_temp == id orderby p.id select p).ToList();
            var psForTC = new List<TC_param_value_temp>(); 
            var psForWork = new List<TC_param_value_temp>();
            var psForStaff = new List<TC_param_value_temp>();
            var psForSzr = new List<TC_param_value_temp>();
            var psForFertilizer = new List<TC_param_value_temp>();
            var psForSeed = new List<TC_param_value_temp>();
            var psForDopMaterial = new List<TC_param_value_temp>();

            for (var i = 0; i < ps.Count; i++)
            {
                var p = ps[i];
                if (p.id_tc_param_temp < 4)
                {
                    psForTC.Add(p);
                }
                else if (p.mat_mat_id.HasValue)
                {
                    switch (p.Materials.mat_type_id)
                    {
                        case 1:
                            psForSzr.Add(p);
                            break;
                        case 2:
                            psForFertilizer.Add(p);
                            break;
                        case 4:
                            psForSeed.Add(p);
                            break;
                        case 5:
                            psForDopMaterial.Add(p);
                            break;
                        default:
                            break;
                    }
                }
                else if (p.id_tc_param_temp == 36 || p.id_tc_param_temp == 38 || p.id_tc_param_temp == 64 || p.id_tc_param_temp == 65 || p.id_tc_param_temp == 66)
                {
                    psForStaff.Add(p);
                }
                else
                {
                    psForWork.Add(p);
                }
            }

            tc.TC = (from t in db.TC_temp
                     where t.id == id
                     select new TCTempModel
                     {
                         Id = t.id,
                         PlantId = (int)t.plant_plant_id,
                         SortId = t.tpsort_tpsort_id,
                     }).FirstOrDefault();

           
            tc.DataServices = (from s in db.TC_to_service_temp
                               where s.id_temp == id 
                               select new DataServiceTemp
                               {
                                   Id = s.id,
                                   Cost = s.cost,
                                   NameService = new DictionaryItemsDTO
                                   {
                                       Id = s.id_service,
                                       Name = s.Service.name
                                   }
                               }).ToList();

            //works
            tc.Works = new List<WorkModelTemp>();

            var tech = (from t in db.TC_operation_to_Tech_temp

                        where t.TC_operation_temp.id_temp == id
                        group new { e = t.Equipments, tr = t.Traktors } by new { t.it_tc_operation_temp } into g
                        select new
                        {
                            id = g.Key.it_tc_operation_temp,
                            EqValues = g.Where(x => x.e != null).Select(x => new DictionaryItemsDTO { Id = x.e.equi_id, Name = x.e.name }).ToList(),
                            TrValues = g.Where(x => x.tr != null).Select(x => new DictionaryItemsDTO { Id = x.tr.trakt_id, Name = x.tr.name }).ToList()
                        }).ToList();

            tc.SupportStuff = new List<SupportStaffModelTemp>();
            for (var i = 0; i < works.Count; i++)
            {
                var workDB = works[i];
                var allTechForWork = tech.Where(t => t.id == workDB.id).FirstOrDefault();
                var workload = GetParamValueByIdAndOperation(psForWork, 33, workDB.id);
                var unicFlag = GetParamValueByIdAndOperation(psForWork, 51, workDB.id);
                var w = new WorkModelTemp
                {
                    Id = workDB.id,
                    Name = new DictionaryItemsDTO
                    {
                        Id = workDB.Type_tasks.tptas_id,
                        Name = workDB.Type_tasks.name
                    },
                    Count = workDB.multiplicity,
                    DateStart = workDB.date_start,
                    DateEnd = workDB.date_finish,
                    Factor = workDB.factor,
                    Workload = workload,
                    Equipment = allTechForWork != null ? allTechForWork.EqValues : new List<DictionaryItemsDTO>(),
                    Tech = allTechForWork != null ? allTechForWork.TrValues : new List<DictionaryItemsDTO>(),
                    UnicFlag = unicFlag,
                    index = i + 1,

                };
                tc.Works.Add(w);
            }
            /////staff
            for (var i = 0; i < psForStaff.Count; i++)
            {
                var p = psForStaff[i];
                var Id = GetParamValueById(psForStaff, 64);
                if (!String.IsNullOrEmpty(Id.ToString()))
                {
                    var stuff = new SupportStaffModelTemp
                    {
                        NameSelected = new DictionaryItemsDTO
                        {
                            Id = (int?)Id,
                            Name = db.Type_tasks.Where(t => t.tptas_id == (int)Id).Select(t => t.name).FirstOrDefault()
                        },
                        Count = (int?)GetParamValueById(psForStaff, 36),
                        Price = GetParamValueById(psForStaff, 65),
                        id_tc_oper = (int?)GetParamValueById(psForStaff, 66),
                    };
                    tc.SupportStuff.Add(stuff);
                }
                else { psForStaff.RemoveAt(i); }
                i--;
            }
            //определяем индексы
            for (int i = 0; i < tc.SupportStuff.Count; i++)
            {
                var id_tc_oper = tc.Works.Where(t => tc.SupportStuff[i].id_tc_oper == t.Id).Select(v => v.index).FirstOrDefault();
                if (!String.IsNullOrEmpty(id_tc_oper.ToString()))
                {
                    tc.SupportStuff[i].index = id_tc_oper;
                }
                ;
            }
            tc.SupportStuff = tc.SupportStuff.OrderBy(b => b.index).ToList();


            //materials
            tc.DataSZR = new List<SZRModelTemp>();
            for (var i = 0; i < psForSzr.Count; i++)
            {
                var p = psForSzr[i];
                var szr = new SZRModelTemp
                {
                    NameSelectedWorkForMaterial = p.id_tc_operation_temp.HasValue ? new DictionaryItemsDTO
                    {
                        Id = p.TC_operation_temp.tptas_tptas_id,
                        Name = p.TC_operation_temp.Type_tasks.name
                    } : null,
                    NameSZR = new DictionaryItemsDTO
                    {
                        Id = p.Materials.mat_id,
                        Name = p.Materials.name
                    },
                    ConsumptionRate = GeMaterialValueByIdAndOperation(psForSzr, 48, p.mat_mat_id),
                    Type = p.Materials.mat_type_id.Value,
                    id_tc_oper = (int?)GeMaterialValueByIdAndOperation(psForSzr, 67, p.mat_mat_id),
                };
                tc.DataSZR.Add(szr);
                i--;
            }
            //////определяем индексы
            for (int i = 0; i < tc.DataSZR.Count; i++)
            {
                var id_tc_oper = tc.Works.Where(t => tc.DataSZR[i].id_tc_oper == t.Id).Select(v => v.index).FirstOrDefault();

                if (!String.IsNullOrEmpty(id_tc_oper.ToString()))
                {
                    tc.DataSZR[i].index = id_tc_oper;
                }
            }
            tc.DataSZR = tc.DataSZR.OrderBy(b => b.index).ToList();
            tc.DataChemicalFertilizers = new List<FertilizerModelTemp>();
            for (var i = 0; i < psForFertilizer.Count; i++)
            {
                var p = psForFertilizer[i];
                var szr = new FertilizerModelTemp
                {
                    NameSelectedWorkForMaterial = p.id_tc_operation_temp.HasValue ? new DictionaryItemsDTO
                    {
                        Id = p.TC_operation_temp.tptas_tptas_id,
                        Name = p.TC_operation_temp.Type_tasks.name
                    } : null,
                    NameFertilizer = new DictionaryItemsDTO
                    {
                        Id = p.Materials.mat_id,
                        Name = p.Materials.name
                    },
                    ConsumptionRate = GeMaterialValueByIdAndOperation(psForFertilizer, 48, p.mat_mat_id),
                    Type = p.Materials.mat_type_id.Value,
                    id_tc_oper = (int?)GeMaterialValueByIdAndOperation(psForFertilizer, 67, p.mat_mat_id),

                };
                tc.DataChemicalFertilizers.Add(szr);
                i--;
            }
            //определяем индексы
            for (int i = 0; i < tc.DataChemicalFertilizers.Count; i++)
            {
                var id_tc_oper = tc.Works.Where(t => tc.DataChemicalFertilizers[i].id_tc_oper == t.Id).Select(v => v.index).FirstOrDefault();
                if (!String.IsNullOrEmpty(id_tc_oper.ToString()))
                {
                    tc.DataChemicalFertilizers[i].index = id_tc_oper;
                }
            }
            tc.DataChemicalFertilizers = tc.DataChemicalFertilizers.OrderBy(b => b.index).ToList();
            tc.DataSeed = new List<SeedModelTemp>();
            for (var i = 0; i < psForSeed.Count; i++)
            {
                var p = psForSeed[i];
                var szr = new SeedModelTemp
                {
                    NameSelectedWorkForMaterial = p.id_tc_operation_temp.HasValue ? new DictionaryItemsDTO
                    {
                        Id = p.TC_operation_temp.tptas_tptas_id,
                        Name = p.TC_operation_temp.Type_tasks.name
                    } : null,
                    NameSeed = new DictionaryItemsDTO
                    {
                        Id = p.Materials.mat_id,
                        Name = p.Materials.name
                    },
                    ConsumptionRate = GeMaterialValueByIdAndOperation(psForSeed, 48, p.mat_mat_id),
                    Type = p.Materials.mat_type_id.Value,
                    id_tc_oper = (int?)GeMaterialValueByIdAndOperation(psForSeed, 67, p.mat_mat_id),

                };
                tc.DataSeed.Add(szr);
                i--;
            }
            ////определяем индексы
            for (int i = 0; i < tc.DataSeed.Count; i++)
            {
                var id_tc_oper = tc.Works.Where(t => tc.DataSeed[i].id_tc_oper == t.Id).Select(v => v.index).FirstOrDefault();
                if (!String.IsNullOrEmpty(id_tc_oper.ToString()))
                {
                    tc.DataSeed[i].index = id_tc_oper;
                }
            }
            tc.DataSeed = tc.DataSeed.OrderBy(b => b.index).ToList();
            //
            tc.DataDopMaterial = new List<DopMaterialsModelTemp>();
            for (var i = 0; i < psForDopMaterial.Count; i++)
            {
                var p = psForDopMaterial[i];
                var DopMaterials = new DopMaterialsModelTemp
                {
                    NameSelectedWorkForMaterial = p.id_tc_operation_temp.HasValue ? new DictionaryItemsDTO
                    {
                        Id = p.TC_operation_temp.tptas_tptas_id,
                        Name = p.TC_operation_temp.Type_tasks.name
                    } : null,
                    NameDopMaterial = new DictionaryItemsDTO
                    {
                        Id = p.Materials.mat_id,
                        Name = p.Materials.name
                    },
                    ConsumptionRate = GeMaterialValueByIdAndOperation(psForDopMaterial, 48, p.mat_mat_id),
                    Type = p.Materials.mat_type_id.Value,
                    id_tc_oper = (int?)GeMaterialValueByIdAndOperation(psForDopMaterial, 67, p.mat_mat_id),

                };
                tc.DataDopMaterial.Add(DopMaterials);
                i--;
            }
            ////определяем индексы
            for (int i = 0; i < tc.DataDopMaterial.Count; i++)
            {
                var id_tc_oper = tc.Works.Where(t => tc.DataDopMaterial[i].id_tc_oper == t.Id).Select(v => v.index).FirstOrDefault();
                if (!String.IsNullOrEmpty(id_tc_oper.ToString()))
                {
                    tc.DataDopMaterial[i].index = id_tc_oper;
                }
            }
            tc.DataDopMaterial = tc.DataDopMaterial.OrderBy(b => b.index).ToList();
            //
            return tc;
        }
        ///////////////

        private float? GetParamValueByIdAndOperation(List<TC_param_value_temp> ps, int id, int operId)
        {
            for (var i = 0; i < ps.Count; i++)
            {
                if (ps[i].id_tc_param_temp == id && ps[i].id_tc_operation_temp == operId)
                {
                    var v = ps[i].value;
                    //ps.RemoveAt(i);
                    return ps[i].value;
                }
            }
            return 0;
        }
        private float? GetParamValueById(List<TC_param_value_temp> ps, int id)
        {
            for (var i = 0; i < ps.Count; i++)
            {
                if (ps[i].id_tc_param_temp == id)
                {
                    var v = ps[i].value;
                    ps.RemoveAt(i);
                    return v;
                }
            }
            return null;
        }

        private float? GeMaterialValueByIdAndOperation(List<TC_param_value_temp> ps, int id, int? mat_id)
        {
            for (var i = 0; i < ps.Count; i++)
            {
                if (ps[i].id_tc_param_temp == id && ps[i].mat_mat_id == mat_id)
                {
                    var v = ps[i].value;
                    ps.RemoveAt(i);
                    return v;
                }
            }
            return null;
        }

        //=============================================создание шаблонов из тех карт
        public string CreateTemplateFromTech()
        {
            var year = 2018;
            //truncate table TC_operation_temp 
            //truncate table TC_temp 
            //truncate table TC_param_value_temp 
            //truncate table TC_to_service_temp 
            var techIds = db.TC.Where(t => t.year == year).Select(t => t.id).Distinct().ToList();

            using (var trans = db.Database.BeginTransaction())
            {
                try
                {
                    //temp 
                    var newId = (db.TC_temp.Max(t => (int?)t.id) ?? 0) + 1;
                    var newOperId = (db.TC_operation_temp.Max(t => (int?)t.id) ?? 0) + 1;
                    var newParamValId = (db.TC_param_value.Max(t => (int?)t.id) ?? 0) + 1;
                    var serviceId = (db.TC_to_service.Max(t => (int?)t.id) ?? 0) + 1;
                    var techId = (db.TC_operation_to_Tech.Max(t => (int?)t.id) ?? 0) + 1;

                    //all Data 
                    var tcAllData = db.TC.Where(t => t.year == year).ToList();
                    var tcAllOperData = db.TC_operation.Where(t => t.TC.year == year).ToList();
                    var tcAllParamValueData = db.TC_param_value.Where(t => t.TC.year == year && (t.id_tc_param == 33 || t.id_tc_param == 36 ||
                    t.id_tc_param == 48 || t.id_tc_param == 51 || t.id_tc_param == 64 || t.id_tc_param == 65 || t.id_tc_param == 66 ||
                    t.id_tc_param == 67)).ToList();
                    var tcAllServiceData = db.TC_to_service.Where(t => t.TC.year == year).ToList();
                    var tcAllTechData = db.TC_operation_to_Tech.Where(t => t.TC_operation.TC.year == year).ToList();


                    var tcTempTable = new List<TC_temp>();
                    var tcOperTable = new List<TC_operation_temp>();
                    var tcParamValueTable = new List<TC_param_value_temp>();
                    var tcServiceTable = new List<TC_to_service_temp>();
                    var tcTechTable = new List<TC_operation_to_Tech_temp>();
                    for (int i = 0; i < techIds.Count; i++)
                    {
                        //тех. карты 
                        var curId = techIds[i];

                        var tcData = tcAllData.Where(t => t.id == curId).FirstOrDefault();
                        var tcOperData = tcAllOperData.Where(t => t.id_tc == curId).ToList();
                        var tcParamValueData = tcAllParamValueData.Where(t => t.id_tc == curId).ToList();
                        var tcServiceData = tcAllServiceData.Where(t => t.id_tc == curId).ToList();
                        var tcTechData = tcAllTechData.Where(t => t.TC_operation.TC.id == curId).ToList();
                        //1 main 
                        var tcTemp = new TC_temp
                        {
                            id = newId,
                            plant_plant_id = tcData.plant_plant_id,
                            type_module = (int)TypeModule.Template,
                        };
                        tcTempTable.Add(tcTemp);
                        //2 TC_operation_temp 
                        for (int j = 0; j < tcOperData.Count; j++)
                        {
                            var oldTcOperId = tcOperData[j].id;
                            var curTcParamList = tcParamValueData.Where(t => t.id_tc_operation == oldTcOperId).ToList();
                            var curTcTechList = tcTechData.Where(t => t.it_tc_operation == oldTcOperId).ToList();
                            var dic = new TC_operation_temp
                            {
                                id = newOperId + oldTcOperId,
                                id_temp = newId,
                                tptas_tptas_id = tcOperData[j].tptas_tptas_id,
                                multiplicity = tcOperData[j].multiplicity,
                                date_start = tcOperData[j].date_start,
                                date_finish = tcOperData[j].date_finish,
                                factor = tcOperData[j].factor,
                            };
                            tcOperTable.Add(dic);
                            //3 TC_param_value_temp 
                            for (int l = 0; l < curTcParamList.Count; l++)
                            {
                                var dic1 = new TC_param_value_temp();
                                if (curTcParamList[l].id_tc_param != 66 && curTcParamList[l].id_tc_param != 67)
                                {
                                    dic1 = new TC_param_value_temp
                                    {
                                        id = newParamValId,
                                        id_temp = newId,
                                        id_tc_operation_temp = newOperId + oldTcOperId,
                                        id_tc_param_temp = curTcParamList[l].id_tc_param,
                                        value = (float?)curTcParamList[l].value,
                                        plan = curTcParamList[l].plan,
                                        mat_mat_id = curTcParamList[l].mat_mat_id,
                                    };
                                }
                                //индексы 
                                else
                                {
                                    dic1 = new TC_param_value_temp
                                    {
                                        id = newParamValId,
                                        id_temp = newId,
                                        id_tc_operation_temp = newOperId + oldTcOperId,
                                        id_tc_param_temp = curTcParamList[l].id_tc_param,
                                        value = newOperId + (float?)curTcParamList[l].value,
                                        plan = curTcParamList[l].plan,
                                        mat_mat_id = curTcParamList[l].mat_mat_id,
                                    };

                                }

                                tcParamValueTable.Add(dic1);
                                newParamValId++;
                            }
                            //4 tech_equipment 
                            for (int l = 0; l < curTcTechList.Count; l++)
                            {
                                var dic1 = new TC_operation_to_Tech_temp
                                {
                                    id = techId,
                                    it_tc_operation_temp = newOperId + oldTcOperId,
                                    equi_equi_id = curTcTechList[l].equi_equi_id,
                                    trakt_trakt_id = curTcTechList[l].trakt_trakt_id,
                                };
                                tcTechTable.Add(dic1);
                                techId++;
                            }

                            // 


                            // newOperId++; 
                        }

                        //3 TC_param_value_temp tc_operation=null 
                        var curTcnullparam = tcParamValueData.Where(t => t.id_tc_operation == null).ToList();
                        for (int l = 0; l < curTcnullparam.Count; l++)
                        {
                            var dic1 = new TC_param_value_temp
                            {
                                id = newParamValId,
                                id_temp = newId,
                                id_tc_operation_temp = null,
                                id_tc_param_temp = curTcnullparam[l].id_tc_param,
                                value = (float?)curTcnullparam[l].value,
                                plan = curTcnullparam[l].plan,
                                mat_mat_id = curTcnullparam[l].mat_mat_id,
                            };
                            tcParamValueTable.Add(dic1);
                            newParamValId++;
                        }

                        //4 
                        for (int l = 0; l < tcServiceData.Count; l++)
                        {
                            var dis = new TC_to_service_temp
                            {
                                id = serviceId,
                                id_temp = newId,
                                id_service = tcServiceData[l].id_service,
                                cost = tcServiceData[l].cost,
                            };
                            tcServiceTable.Add(dis);
                            serviceId++;
                        }

                        newId++;
                    }
                    // 
                    db.TC_temp.AddRange(tcTempTable);
                    db.TC_operation_temp.AddRange(tcOperTable);
                    db.TC_param_value_temp.AddRange(tcParamValueTable);
                    db.TC_to_service_temp.AddRange(tcServiceTable);
                    db.TC_operation_to_Tech_temp.AddRange(tcTechTable);
                    // 
                    db.SaveChanges();
                    trans.Commit();
                }
                catch (Exception)
                {
                    trans.Rollback();
                    throw;
                }
            }



            return "success";
        }
        //
        //


        public string CheckSortPlantYear(FieldEventModel req)
        {
            string message = null;
            var tc = (from t in db.TC
                      where  t.plant_plant_id ==  req.PlantId  &&  t.year == req.YearId && 
                     (req.FieldId == null ? t.tpsort_tpsort_id == req.SortId : true)  &&  t.fiel_fiel_id == req.FieldId 
                        select new 
                        {
                           id = t.id,
                           tpsort_tpsort_id = t.tpsort_tpsort_id,
                           plant_plant_id = t.plant_plant_id,
                           year = t.year
                        }).ToList();

            //for (int i = 0; i < req.Values.Count; i++)
            //{
            //    for (int j = 0; j < req.Values[i].Sorts.Count; j++)
            //    {
            //       var count = tc.Where(t => t.tpsort_tpsort_id == req.Values[i].Sorts[j].Sort.Id && t.plant_plant_id == req.Values[i].Plant.Id)
            //           .Select(t => t.id).Count();

            //   //если нет связки создаем тех карту
            //    if (count == 0 && req.Values[i].Plant.Id != 36)
            //    {
            //   message = message + CreateTCBySortPlantYear((int)req.Values[i].Sorts[j].Sort.Id, (int)req.Values[i].Plant.Id, (int)req.YearId);       
            //    }

            //   } 
            // }
            if (tc.Count == 0)
            {
               message = message + CreateTCBySortPlantYear(req.PlantId, (int)req.YearId, req.SortId, req.FieldId, req.TempId,  req.CheckTemp);
            }
            else {
                throw new BllException(15, "Тех.карта уже создана.");
            }

            if (message == null) { message = "empty_message"; }
            return message;
        }

        //создание тех карты по шаблону
     
        public string CreateTCBySortPlantYear(int plant_id, int year, int? sortId, int? fieldId, int? tempId, int? checkTemp )
        {
            //получение данных тех. карты
            TCTempDetailModel list = new TCTempDetailModel();
            TCDetailModel list1 = new TCDetailModel();
            RateResModel rate = new RateResModel();
            RateTechReq req = new RateTechReq();
            var materials = techcard.GetListMaterial();
            var temp = db.TC_temp.Where(t => t.id == tempId).FirstOrDefault();
            var id = 0;
            if (checkTemp != 1)
            {
                id = db.TC_temp.Where(t => (sortId != null ? t.tpsort_tpsort_id == sortId : true) && t.plant_plant_id == plant_id & t.type_module ==
                   (int)TypeModule.Template).Select(t => t.id).FirstOrDefault();
            }
            if (checkTemp == 1)
            {
                id = db.TC_temp.Where(t => (temp.tpsort_tpsort_id != null ? t.tpsort_tpsort_id == temp.tpsort_tpsort_id : true) &&
                t.plant_plant_id == temp.plant_plant_id & t.type_module ==
                      (int)TypeModule.Template).Select(t => t.id).FirstOrDefault();
            }

            var plant_name = db.Plants.Where(t => t.plant_id == plant_id).Select(t => t.name).FirstOrDefault();
            var sort_name =  db.Type_sorts_plants.Where(t => t.tpsort_id == sortId).Select(t => t.name).FirstOrDefault();
            sort_name = sort_name == null ? " " : " -" + sort_name;
            list = GetTC_tempById((int)id);

            list1.TC = new TCModel();
            list1.DataServices = new List<DataService>();
            list1.Works = new List<WorkModel>();
            list1.SupportStuff = new List<SupportStaffModel>();
            list1.DataChemicalFertilizers = new List<FertilizerModel>();
            list1.DataDopMaterial = new List<DopMaterialsModel>();
            list1.DataSeed = new List<SeedModel>();
            list1.DataSZR = new List<SZRModel>();
            list1.DataTotal = new List<DataTotalModel>();
            list1.TC.Id = null;
            //шапка
            if (fieldId != null)
            {
                list1.TC.SortId = null;
                list1.TC.FieldId = fieldId;
                sort_name = "- поле " + db.Fields.Where(t => t.fiel_id == fieldId).Select(t => t.name).FirstOrDefault();
            }
            else {
                list1.TC.SortId = sortId;
            }
            list1.TC.PlantId = plant_id;
            list1.TC.YearId = year;
            list1.TC.Productivity = 0;
            list1.TC.GrossHarvest = 0;
            list1.TC.CheckTemp = 1;
            //проверка
            if (id == 0 && checkTemp != 1)
            {
                var checkSortName = temp.Type_sorts_plants == null ? "" : "-" + temp.Type_sorts_plants.name;
                return "Отсутствует шаблон для " + plant_name + " " + sort_name + "./Создать тех.карту по шаблону " + temp.Plants.name + checkSortName + "?";

            }

            //услуги
            if (list.DataServices != null)
            {
                for (int i = 0; i < list.DataServices.Count; i++)
                {
                    list1.DataServices.Add(new DataService
                        {
                            Id = null,
                            NameService = list.DataServices[i].NameService,
                            Cost = list.DataServices[i].Cost
                        });
                }
            }
            decimal? summaFields = 0;
            var plantfield = techcard.GetPlantSortFieldByYear(year);

            //общая площадь
              for (int i = 0;  i < plantfield.Count; i++) {
                    var p = plantfield[i];
                    if (p.Id == plant_id) {
                        var fields = new List<FieldDictionary>();
                        for (int j = 0; j < p.Values.Count; j++)
                        {
                            var s = p.Values[j];
                                 if (s.Id == sortId) {
                                  fields = s.Fields;
                              }
                        }
                        if (fields.Count  == 0) {
                            fields = p.Fields;
                        }
                        for (int k = 0; k < fields.Count; k++)
                        {
                            if (fieldId != null && fields[k].Id == fieldId)
                            {
                                summaFields += fields[k].Area;
                            }
                            if (fieldId == null)
                            {
                                summaFields += fields[k].Area;
                            }
                        }
                    }
                }
            //назначенные работы, топливо, зарплата
            if (list.Works != null)
            {
                for (int i = 0; i < list.Works.Count; i++)
                {

                    DateTime newStartDate = new DateTime(year, list.Works[i].DateStart.Value.Month, list.Works[i].DateStart.Value.Day);
                    list.Works[i].DateStart = newStartDate;
                    DateTime newEndDate = new DateTime(year, list.Works[i].DateEnd.Value.Month, list.Works[i].DateEnd.Value.Day);
                    list.Works[i].DateEnd = newEndDate;

                    req.tptasId = list.Works[i].Name.Id;
                    if (list.Works[i].Tech.Count != 0) { req.techId = list.Works[i].Tech[0].Id; } else { req.techId = null; }
                    if (list.Works[i].Equipment.Count != 0) { req.equiId = list.Works[i].Equipment[0].Id; } else { req.equiId = null; }
                    req.plantId = list.TC.PlantId;

                    list.Works[i].Id = null;
             
                    rate = techcard.GetRate(req);

                    if (!String.IsNullOrEmpty(rate.Typefuel.ToString())) { list.Works[i].FuelType = rate.Typefuel; } //тип, цена топлива
                    if (list.Works[i].UnicFlag == 0)
                    { //если по площади
                        list.Works[i].Workload = (float)list.Works[i].Count * (float)list.Works[i].Factor * (float)summaFields;//объем работ
                    }
                    if (!String.IsNullOrEmpty(rate.FuelConsumption.ToString()))
                    {
                        list.Works[i].Consumption = (float)rate.FuelConsumption;//расход топлива
                        list.Works[i].FuelCount = list.Works[i].Consumption * list.Works[i].Workload; //потребность
                        list.Works[i].FuelCost = list.Works[i].FuelCount * list.Works[i].FuelType.Price; //стоимость топлива
                    }
                    else
                    {
                        list.Works[i].Consumption = 0;
                        list.Works[i].FuelCount = 0;
                        list.Works[i].FuelCost = 0;
                    }
                 
                        list.Works[i].EmpCost = 0;
                        list.Works[i].Rate = 0;
                        list.Works[i].ShiftCount = 0;
                    if (!String.IsNullOrEmpty(rate.ShiftOutput.ToString())) { list.Works[i].ShiftRate = (float)rate.ShiftOutput; } //см. норма
                    if (list.Works[i].ShiftRate == null) { list.Works[i].ShiftRate = 0; }
                    if (list.Works[i].ShiftRate != 0 && list.Works[i].ShiftRate != null) { list.Works[i].ShiftCount = (float)Math.Round((decimal)(list.Works[i].Workload / list.Works[i].ShiftRate), 2); } //смены
                    if (!String.IsNullOrEmpty(rate.RateShift.ToString())) { list.Works[i].Rate = (float)rate.RateShift; } //тариф
                    if (!String.IsNullOrEmpty(rate.RatePiecework.ToString())) { list.Works[i].EmpPrice = (float)rate.RatePiecework; } //расценка
                    else { list.Works[i].EmpPrice = 0; }
                    list.Works[i].EmpCost = (float)Math.Round((decimal)(list.Works[i].Rate * list.Works[i].ShiftCount + list.Works[i].EmpPrice * list.Works[i].Workload), 2); //стоимость зп

                    list1.Works.Add(new WorkModel
                    {
                      Id=null,
                      Name = list.Works[i].Name,
                      Count = list.Works[i].Count,
                      Factor =  list.Works[i].Factor,
                      Workload = list.Works[i].Workload,
                      DateStart = list.Works[i].DateStart,
                      DateEnd = list.Works[i].DateEnd,
                      Equipment =  list.Works[i].Equipment,
                      Tech =  list.Works[i].Tech,
                      Consumption =  list.Works[i].Consumption,
                      FuelType =  list.Works[i].FuelType,
                      FuelCost = list.Works[i].FuelCost,
                      ShiftRate =  list.Works[i].ShiftRate,
                      ShiftCount = list.Works[i].ShiftCount,
                      EmpPrice = list.Works[i].EmpPrice,
                      EmpCost = list.Works[i].EmpCost,
                      UnicFlag =  list.Works[i].UnicFlag,
                      Rate =  list.Works[i].Rate,
                      FuelId = list.Works[i].FuelId ,
                      FuelCount = list.Works[i].FuelCount,
                      index = list.Works[i].index,
                    });
                }
            }
       
            //вспомогательный персонал
            if (list.SupportStuff != null)
            {
                for (int i = 0; i < list.SupportStuff.Count; i++)
                {
                    list1.SupportStuff.Add(new SupportStaffModel
                           {
                              NameSelected = list.SupportStuff[i].NameSelected,
                              Price = list.SupportStuff[i].Price,
                              Cost = list.SupportStuff[i].Cost,
                              index = list.SupportStuff[i].index,
                           //   UnicFlag = list.SupportStuff[i].UnicFlag,
                              Count = list.SupportStuff[i].Count,
                           });
                }
                //определяем индексы
                var nId = 0;
                for (int j = 0; j < list1.Works.Count; j++)
                {
                    if (j == 0) { nId = (db.TC_operation.Max(t => (int?)t.id) ?? 0) + 1; }
                    if (j != 0) { nId++; }
                    list1.Works[j].Id1 = nId;
                    var updSupportStuff = (from t in list1.SupportStuff where t.index == list1.Works[j].index select t).FirstOrDefault();
                    if (updSupportStuff != null) {
                      updSupportStuff.id_tc_oper = nId;
                        //
                         if (list1.Works[j].UnicFlag == 0)
                    { // по площади
                        var ShiftCount = list1.Works.Where(t => t.index == updSupportStuff.index).Select(t => t.ShiftCount).FirstOrDefault(); // смены   
                         updSupportStuff.Cost = ShiftCount *  updSupportStuff.Count *  updSupportStuff.Price;
                    }
                    else
                    { //штучные
                        var Workload = list1.Works.Where(t => t.index ==  updSupportStuff.index).Select(t => t.Workload).FirstOrDefault(); // объем работ 
                         updSupportStuff.Cost = Workload *  updSupportStuff.Price;
                    }
                    }
                    }
                }
                list1.SupportStuff = list1.SupportStuff.OrderBy(b => b.index).ToList();
            //семена
            if (list.DataSeed != null)
            {
                for (int i = 0; i < list.DataSeed.Count; i++)
                {
                    list.DataSeed[i].MaterialArea = (float)summaFields; //площадь внесения
            
                    for (int j = 0; j < materials.Count; j++)
                    {
                        if (materials[j].Id == list.DataSeed[i].NameSeed.Id) { list.DataSeed[i].Price = (float)materials[j].Price; break; } //цена материла
                    }
                    list.DataSeed[i].Cost = list.DataSeed[i].MaterialArea * list.DataSeed[i].ConsumptionRate * list.DataSeed[i].Price; //стоимость
                    list.DataSeed[i].id_tc_oper = list1.Works.Where(t => t.index == list.DataSeed[i].index).Select(t => t.Id1).FirstOrDefault();
                    list1.DataSeed.Add(new SeedModel
                    {
                        NameSelectedWorkForMaterial = list.DataSeed[i].NameSelectedWorkForMaterial,
                        Price = list.DataSeed[i].Price ?? 0,
                        Cost = list.DataSeed[i].Cost ?? 0,
                        ConsumptionRate = list.DataSeed[i].ConsumptionRate,
                        MaterialArea = list.DataSeed[i].MaterialArea,
                        Type = list.DataSeed[i].Type,
                        NameSeed = list.DataSeed[i].NameSeed,
                        id_tc_oper = list.DataSeed[i].id_tc_oper,
                    });
                }
            }
            //сзр
            if (list.DataSZR != null)
            {
                for (int i = 0; i < list.DataSZR.Count; i++)
                {
                    list.DataSZR[i].MaterialArea = (float)summaFields; //площадь внесения
              
                    for (int j = 0; j < materials.Count; j++)
                    {
                        if (materials[j].Id == list.DataSZR[i].NameSZR.Id) { list.DataSZR[i].Price = (float)materials[j].Price; break; } //цена материла
                    }
                    list.DataSZR[i].Cost = list.DataSZR[i].MaterialArea * list.DataSZR[i].ConsumptionRate * list.DataSZR[i].Price; //стоимость
                    list.DataSZR[i].id_tc_oper = list1.Works.Where(t => t.index == list.DataSZR[i].index).Select(t => t.Id1).FirstOrDefault();
                    list1.DataSZR.Add(new SZRModel
                    {
                        NameSelectedWorkForMaterial = list.DataSZR[i].NameSelectedWorkForMaterial,
                        Price = list.DataSZR[i].Price ?? 0,
                        Cost = list.DataSZR[i].Cost ?? 0,
                        ConsumptionRate = list.DataSZR[i].ConsumptionRate,
                        MaterialArea = (float)summaFields,
                        Type = list.DataSZR[i].Type,
                        NameSZR = list.DataSZR[i].NameSZR,
                        id_tc_oper = list.DataSZR[i].id_tc_oper,
                    });
                }
            }

            //удобрения
            if (list.DataChemicalFertilizers != null)
            {
                for (int i = 0; i < list.DataChemicalFertilizers.Count; i++)
                {
                    list.DataChemicalFertilizers[i].MaterialArea = (float)summaFields; //площадь внесения
                 
                    for (int j = 0; j < materials.Count; j++)
                    {
                        if (materials[j].Id == list.DataChemicalFertilizers[i].NameFertilizer.Id) { list.DataChemicalFertilizers[i].Price = (float)materials[j].Price; break; } //цена материла
                    }
                    list.DataChemicalFertilizers[i].Cost = list.DataChemicalFertilizers[i].MaterialArea * list.DataChemicalFertilizers[i].ConsumptionRate
                      * list.DataChemicalFertilizers[i].Price; //стоимость
                    list.DataChemicalFertilizers[i].id_tc_oper = list1.Works.Where(t => t.index == list.DataChemicalFertilizers[i].index).Select(t => t.Id1).FirstOrDefault();
                    list1.DataChemicalFertilizers.Add(new FertilizerModel
                    {
                        NameSelectedWorkForMaterial = list.DataChemicalFertilizers[i].NameSelectedWorkForMaterial,
                        Price = list.DataChemicalFertilizers[i].Price ?? 0,
                        Cost = list.DataChemicalFertilizers[i].Cost ?? 0,
                        ConsumptionRate = list.DataChemicalFertilizers[i].ConsumptionRate,
                        MaterialArea = (float)summaFields,
                        Type = list.DataChemicalFertilizers[i].Type,
                        NameFertilizer = list.DataChemicalFertilizers[i].NameFertilizer,
                        id_tc_oper = list.DataChemicalFertilizers[i].id_tc_oper,
                    }); 
                }
            }


            //прочие материалы
            if (list.DataDopMaterial != null)
            {
                for (int i = 0; i < list.DataDopMaterial.Count; i++)
                {
                    list.DataDopMaterial[i].MaterialArea = (float)summaFields; //площадь внесения

                    for (int j = 0; j < materials.Count; j++)
                    {
                        if (materials[j].Id == list.DataDopMaterial[i].NameDopMaterial.Id) { list.DataDopMaterial[i].Price = (float)materials[j].Price; break; } //цена материла
                    }
                    list.DataDopMaterial[i].Cost = list.DataDopMaterial[i].MaterialArea * list.DataDopMaterial[i].ConsumptionRate
                      * list.DataDopMaterial[i].Price; //стоимость
                    list.DataDopMaterial[i].id_tc_oper = list1.Works.Where(t => t.index == list.DataDopMaterial[i].index).Select(t => t.Id1).FirstOrDefault();
                    list1.DataDopMaterial.Add(new DopMaterialsModel
                    {
                        NameSelectedWorkForMaterial = list.DataDopMaterial[i].NameSelectedWorkForMaterial,
                        Price = list.DataDopMaterial[i].Price ?? 0,
                        Cost = list.DataDopMaterial[i].Cost ?? 0,
                        ConsumptionRate = list.DataDopMaterial[i].ConsumptionRate,
                        MaterialArea = (float)summaFields,
                        Type = list.DataDopMaterial[i].Type,
                        NameDopMaterial = list.DataDopMaterial[i].NameDopMaterial,
                        id_tc_oper = list.DataDopMaterial[i].id_tc_oper,
                    });
                }
            }
            techcard.CreateUpdateTC(list1);
            return "Coздана технологическая карта для "+plant_name + " "+sort_name+". ";
        }
    }

}
