﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogusSrv.CORE.Exceptions;
using LogusSrv.DAL.DBModel;
using LogusSrv.DAL.Entities.DTO;
using LogusSrv.DAL.Entities.DTO.DayTaskExt;
using LogusSrv.DAL.Entities.Res;
using LogusSrv.DAL.Entities.Req;
using LogusSrv.DAL.Enums;
using LogusSrv.DAL.Utils;

namespace LogusSrv.DAL.Operations
{
    public class DayTasksDb
    {
        protected readonly LogusDBEntities db = new LogusDBEntities();
        protected readonly TechCardDb techCardDb = new TechCardDb();
        protected readonly WayListBase wayListDb = new WayListBase();
        public string res;
        public int number;

        //вывод сменного задания
        public DayTasksModels GetDayTasks(GetDayTasksReq req)
        {
            DayTasksModels taskList = new DayTasksModels();
            taskList.Shift1 = new List<DayTaskDTO>();
            taskList.Shift2 = new List<DayTaskDTO>();
            if (req == null) req = new GetDayTasksReq();
            if (req.DateFilter == null) req.DateFilter = DateTime.Now.Date;
            req.DateFilter = req.DateFilter.Value.Date;

            var checkType = db.Organizations.Where(t => t.org_id == req.OrgId).Select(t => t.tporg_tporg_id).FirstOrDefault();

            var listResult = (from dt in db.Day_task
                              join dtd in db.Day_task_detail on dt.dt_id equals dtd.dt_dt_id
                              where dt.dt_date == req.DateFilter &&
                              (req.FilterOrg != -1 ? dt.org_org_id == req.FilterOrg : true)
                              select new DayTaskDTO
                              {
                                  PlantInfo = new PlantInfoDTO
                                  {
                                      Id = dtd.plant_plant_id,
                                      Name = dtd.plant_plant_id != -1 ?  dtd.Plants.short_name : "Культура",
                                      RepairStatus = dtd.plant_plant_id == -1 ? "color: CornflowerBlue;" : null, 
                                  },
                                  FieldInfo = new FieldInfoDTO
                                  {
                                      Id = dtd.fiel_fiel_id,
                                      Name = dtd.Fields.name,
                                  },
                                  TypeTaskInfo = new TypeTaskInfoDTO
                                  {
                                      Id = dtd.tptas_tptas_id,
                                      Name = dtd.Type_tasks.name,
                                  },
                                  EquipmentsInfo = new GetEquipInfoDTO
                                  {
                                      TracktorInfo = new ElemntInfoDTO
                                      {
                                          Id = dtd.trak_trak_id,
                                          Name = dtd.Traktors.name + " (" + dtd.Traktors.reg_num + ")",
                                      },
                                      EquipInfo = new ElemntInfoDTO
                                      {
                                          Id = dtd.equi_equi_id,
                                          Name = dtd.Equipments.name,
                                      },
                                  },

                                  EmployeesInfo = new GetEmpInfoDTO {
                                      Emp1 = new ElemntInfoDTO
                                      {
                                          Id = dtd.emp1_emp_id,
                                          Name = dtd.Employees.short_name,
                                      },
                                      Emp2 = new ElemntInfoDTO
                                      {
                                          Id = dtd.emp2_emp_id,
                                          Name = dtd.Employees1.short_name,
                                      },
                                  
                                  },
                                  AvailableInfo = new AvailableInfoDTO
                                  {
                                      Id = dtd.otv_emp_id,
                                      Name = dtd.Employees2.short_name,
                                  },
                                  AgroreqInfo = new DictionaryItemsDTO
                                  {
                                      Id = dtd.id_agroreq,
                                      Name = dtd.Agrotech_requirment.name
                                  },
                                  DetailId = dtd.dtd_id,
                                  Shift = (int)dtd.shift_num,
                                  Status = dtd.status,
                                  Comment = !string.IsNullOrEmpty(dtd.comment) ? dtd.comment : null,
                                  MainComment = !string.IsNullOrEmpty(dtd.main_comment) ? dtd.main_comment : null
                              }).ToList();

            //выделение дублирующихся техники с планшета
            var tabletList = listResult.Where(s => s.Status == 4).ToList();
            for (int i = 0; i < tabletList.Count; i++) {
                tabletList[i].CheckOrange = "tablet.png";
                if (tabletList[i].EquipmentsInfo.TracktorInfo.Id != null)
                {
                 var traktId = tabletList[i].EquipmentsInfo.TracktorInfo.Id;
                 var item = listResult.Where(s => s.Status != 4 && s.EquipmentsInfo.TracktorInfo.Id == traktId).FirstOrDefault();
                 if (item != null) {
                     tabletList[i].EquipmentsInfo.TracktorInfo.RepairStatus = "color: Orange;";
                     tabletList[i].CheckOrange = "tablet_orange.png";
                     }
                }
                //ремонт
                var RepairTrakt = db.Repairs_acts.Where(r => r.status != 3 && r.date_start <= req.DateFilter).Select(r => r.trakt_trakt_id).ToList();
                foreach (var item in listResult.Where(f => RepairTrakt.Contains(f.EquipmentsInfo.TracktorInfo.Id)))
                {
                    if (item.EquipmentsInfo.TracktorInfo.Id != null)
                    {
                        item.EquipmentsInfo.TracktorInfo.RepairStatus = "color: red;";
                        tabletList[i].CheckOrange = "tablet.png";
                    }
                }
                //водитель1
                if (tabletList[i].EmployeesInfo.Emp1.Id != null)
                {
                    var emp1Id = tabletList[i].EmployeesInfo.Emp1.Id;
                    var item = listResult.Where(s => s.Status != 4 && s.EmployeesInfo.Emp1.Id == emp1Id).FirstOrDefault();
                    if (item != null)
                    {
                        tabletList[i].EmployeesInfo.Emp1.RepairStatus = "color: Orange;";
                        tabletList[i].CheckOrange = "tablet_orange.png";
                    }
                }
                //водитель2
                if (tabletList[i].EmployeesInfo.Emp2.Id != null)
                {
                    var emp2Id = tabletList[i].EmployeesInfo.Emp2.Id;
                    var item = listResult.Where(s => s.Status != 4 && s.EmployeesInfo.Emp2.Id == emp2Id).FirstOrDefault();
                    if (item != null)
                    {
                        tabletList[i].EmployeesInfo.Emp2.RepairStatus = "color: Orange;";
                        tabletList[i].CheckOrange = "tablet_orange.png";
                    }
                }

            } 

            var all1 = listResult.Where(r => r.Shift == 1 && r.FieldInfo.Id != null && r.PlantInfo.Id != null).OrderBy(r => r.PlantInfo.Name).ToList();
            var all2 = listResult.Where(r => r.Shift == 2 && r.FieldInfo.Id != null && r.PlantInfo.Id != null).OrderBy(r => r.PlantInfo.Name).ToList();
            var onlyPlants1 = listResult.Where(r => r.Shift == 1 && r.FieldInfo.Id == null && r.PlantInfo.Id != null).OrderBy(r => r.PlantInfo.Name).ToList();
            var onlyPlants2 = listResult.Where(r => r.Shift == 2 && r.FieldInfo.Id == null && r.PlantInfo.Id != null).OrderBy(r => r.PlantInfo.Name).ToList();
            var onlyFields1 = listResult.Where(r => r.Shift == 1 && r.FieldInfo.Id != null && r.PlantInfo.Id == null).ToList();
            var onlyFields2 = listResult.Where(r => r.Shift == 2 && r.FieldInfo.Id != null && r.PlantInfo.Id == null).ToList();
            var nothing1 = listResult.Where(r => r.Shift == 1 && r.FieldInfo.Id == null && r.PlantInfo.Id == null).ToList();
            var nothing2 = listResult.Where(r => r.Shift == 2 && r.FieldInfo.Id == null && r.PlantInfo.Id == null).ToList();
            taskList.Shift1.AddRange(all1);
            taskList.Shift2.AddRange(all2);
            taskList.Shift1.AddRange(onlyPlants1);
            taskList.Shift2.AddRange(onlyPlants2);
            taskList.Shift1.AddRange(onlyFields1);
            taskList.Shift2.AddRange(onlyFields2);
            taskList.Shift1.AddRange(nothing1);
            taskList.Shift2.AddRange(nothing2);
            var dTask = db.Day_task.Where(dt => dt.dt_date == req.DateFilter &&
                 (req.FilterOrg != -1 ? dt.org_org_id == req.FilterOrg : true)).FirstOrDefault();
            if ( dTask != null ){
                 taskList.DayTaskId = dTask.dt_id;
                 taskList.Comment = dTask.discription;
            }
            return taskList;
            
        }
        //ЗАПРЕТ РЕДАКТИРОВАНИЯ ПЛ
        public string SetBanDateWayBills(String ban_date)
        {
            using (var trans = db.Database.BeginTransaction())
            {
                var dic = db.Parameters.Where(t => t.name == "DateWayBills").FirstOrDefault();
                if (dic == null)
                {
                    var newDic = new Parameters
                    {
                        par_id = CommonFunction.GetNextId(db, "Parameters"),
                        name = "DateWayBills",
                        value = ban_date,
                    };
                    db.Parameters.Add(newDic);

                }
                else
                {
                    dic.value = ban_date;
                }
                db.SaveChanges();
                trans.Commit();
            }
          return "success";
        }
        //Получение последней даты  запрета
        public DateTime GetBanDateWayBills()
        {         
            var date = DateTime.Parse( db.Parameters.Where(t => t.name == "DateWayBills").Select(t => t.value).FirstOrDefault());
            return date;
        }

        public String GetBanDateTC()
        {
            var date = db.Parameters.Where(t => t.name == "DateTC").Select(t => t.value).FirstOrDefault();
            return date;
        }

        public string SetBanDateTC(String tban_date)
        {
            using (var trans = db.Database.BeginTransaction())
            {
                var dic = db.Parameters.Where(t => t.name == "DateTC").FirstOrDefault();
                if (dic == null)
                {
                    var newDic = new Parameters
                    {
                        par_id = CommonFunction.GetNextId(db, "Parameters"),
                        name = "DateTC",
                        value = tban_date,
                    };
                    db.Parameters.Add(newDic);

                }
                else
                {
                    dic.value = tban_date;
                }
                db.SaveChanges();
                trans.Commit();
            }
            return "success";
        }

        /// печать сменного задания
        public PrintShiftTasks GetPrintShiftTasks(int dayTaskId)
        {
            PrintShiftTasks result = new PrintShiftTasks();
            result.ShiftInfo1 = new List<PrintShiftDTO>();
            result.ShiftInfo2 = new List<PrintShiftDTO>();
      
            var dayTask = db.Day_task.Where(dt => dt.dt_id == dayTaskId).FirstOrDefault();

            if (dayTask != null)
            {
                var datibegin = dayTask.dt_date;
                result.DateShifts = datibegin.Day + "-" + datibegin.AddDays(1).Day + "." + datibegin.ToString("MM");
                result.DateBegin = datibegin;
                result.Comment = dayTask.discription;
            }
         
            var listTask = (from dt in db.Day_task_detail
                              where dt.dt_dt_id == dayTaskId
                              select new PrintShiftDTO
                               {
                                  FieldName = dt.Fields.name,
                                  PlantName = dt.Plants.name,
                                  TraktorName = dt.Traktors.name + " (" + dt.Traktors.reg_num + ")",
                                  EquipmentName = dt.Equipments.name,
                                  Employees = dt.emp2_emp_id != null ? dt.Employees.short_name + " / " + dt.Employees1.short_name : dt.Employees.short_name,
                                  Available = dt.Employees2.short_name,
                                  Shift = dt.shift_num,
                                  TaskPlan = dt.Type_tasks.name,
                                  Comment = dt.comment,
                                  MainComment = dt.main_comment,
                                  AgroreqName = dt.Agrotech_requirment.name
                              }).ToList();

          //  result.ShiftInfo1 = listTask.Where(r => r.Shift == 1).OrderBy(r => r.PlantName).ToList();
         //   result.ShiftInfo2 = listTask.Where(r => r.Shift == 2).OrderBy(r => r.PlantName).ToList();
            var all1 = listTask.Where(r => r.Shift == 1 && r.FieldName!=null && r.PlantName!=null).OrderBy(r => r.PlantName).ToList();
            var all2 = listTask.Where(r => r.Shift == 2 && r.FieldName != null && r.PlantName != null).OrderBy(r => r.PlantName).ToList();
            var onlyPlants1 = listTask.Where(r => r.Shift == 1 && r.FieldName == null && r.PlantName != null).OrderBy(r => r.PlantName).ToList();
            var onlyPlants2 = listTask.Where(r => r.Shift == 2 && r.FieldName == null && r.PlantName != null).OrderBy(r => r.PlantName).ToList();
            var onlyFields1 = listTask.Where(r => r.Shift == 1 && r.FieldName != null && r.PlantName == null).ToList();
            var onlyFields2 = listTask.Where(r => r.Shift == 2 && r.FieldName != null && r.PlantName == null).ToList();
            var nothing1 = listTask.Where(r => r.Shift == 1 && r.FieldName == null && r.PlantName == null).ToList();
            var nothing2 = listTask.Where(r => r.Shift == 2 && r.FieldName == null && r.PlantName == null).ToList();
            //
            result.ShiftInfo1.AddRange(all1);
            result.ShiftInfo2.AddRange(all2);
            result.ShiftInfo1.AddRange(onlyPlants1);
            result.ShiftInfo2.AddRange(onlyPlants2);
            result.ShiftInfo1.AddRange(onlyFields1);
            result.ShiftInfo2.AddRange(onlyFields2);
            result.ShiftInfo1.AddRange(nothing1);
            result.ShiftInfo2.AddRange(nothing2);
            return result;
        }

        public string UpdateCommentDayTask(GetDayTasksReq req )
        {
             var checkType = db.Organizations.Where(t => t.org_id == req.OrgId).Select(t => t.tporg_tporg_id).FirstOrDefault();
             int? org_id = null;
             if (checkType == (int)TypeOrganization.Section) { org_id = req.OrgId; }
            using (var trans = db.Database.BeginTransaction())
            {
                var day = db.Day_task.Where(d => d.dt_date == req.DateFilter &&
                     (checkType == (int)TypeOrganization.Company ? d.org_org_id == null : d.org_org_id == req.OrgId)).FirstOrDefault();
                if (day == null)
                {
                    day = new Day_task();
                    day.dt_id = CommonFunction.GetNextId(db, "Day_task");
                    day.status = 1;
                    day.dt_date = req.DateFilter.Value;
                    day.discription = req.Comment;
                    day.dt_type = 1;
                    day.org_org_id = org_id;
                    db.Day_task.Add(day);
                }
                day.discription = req.Comment;
                db.SaveChanges();
                trans.Commit();
                return "ok";
            }
        }

        public string CreateDayTasksDetails(DayTaskDetailReq req)
        {
            using (var trans = db.Database.BeginTransaction())
            {
                Day_task day = new Day_task();
                req.Date = req.Date.Value.Date;
                day = db.Day_task.Where(d => d.dt_date == req.Date).FirstOrDefault();
                if (day == null)
                {
                    day = new Day_task();
                    day.dt_id = CommonFunction.GetNextId(db, "Day_task");
                    day.status = 1;
                    day.dt_date = req.Date.Value;
                    day.dt_type = 1;
                    day.discription = req.Comment;
                    db.Day_task.Add(day);
                }

                Day_task_detail dtDetail = new Day_task_detail
                {
                    dtd_id = CommonFunction.GetNextId(db, "Day_task_detail"),
                    dt_dt_id = day.dt_id,
                    emp1_emp_id = req.Driver1Id,
                    emp2_emp_id = req.Driver2Id,
                    trak_trak_id = req.TraktorId,
                    equi_equi_id = req.TrailerId,
                    otv_emp_id = req.AvailableId,
                    tptas_tptas_id = req.TaskTypeId.Value,
                    plant_plant_id = (int)req.PlantId,
                    shift_num = req.ShiftNum,
                    fiel_fiel_id = req.FieldId.Value,
                    id_agroreq = req.AgroreqId
                };
                db.Day_task_detail.Add(dtDetail);
                db.SaveChanges();
                trans.Commit();
                return "";
            }
        }


        //public string UpdateShiftTask(UpdateShiftTasksReq reqst)
        //{
        //    using (var trans = db.Database.BeginTransaction())
        //    {

        //        Day_task day = new Day_task();
        //        reqst.Date = reqst.Date.Date;
        //        day = db.Day_task.Where(d => d.dt_date == reqst.Date).FirstOrDefault();

        //        if (day == null)
        //        {
        //            day = new Day_task();
        //            day.dt_id = CommonFunction.GetNextId(db, "Day_task");
        //            day.status = 1;
        //            day.discription = reqst.Comment;
        //            day.dt_date = reqst.Date;
        //            day.dt_type = 1;
        //            db.Day_task.Add(day);
        //        }
        //        foreach (var req in reqst.ShiftInfo)
        //        {
        //            if (req.DetailId == null)
        //            {
        //                Day_task_detail dtDetail = new Day_task_detail();
        //                dtDetail.dtd_id = CommonFunction.GetNextId(db, "Day_task_detail");
        //                dtDetail.dt_dt_id = day.dt_id;
        //                MapShiftTaskFromDTO(dtDetail, req);
        //                dtDetail.status = (int)DayTaskStatus.Create;
        //                db.Day_task_detail.Add(dtDetail);
        //             //   return "create";
        //            }

        //            if (req.DetailId != null)
        //            {
        //                var dtDetail = db.Day_task_detail.Where(d => d.dtd_id == req.DetailId).FirstOrDefault();
        //                MapShiftTaskFromDTO(dtDetail, req);
        //            }
        //        }

        //        db.SaveChanges();
        //        trans.Commit();
        //        return "update";
        //    }
        //}


        public int UpdateSelectShiftTask(UpdateDayTaskReq reqst)
        {
            using (var trans = db.Database.BeginTransaction())
            {
                var req = reqst.ShiftInfo;
                Day_task day = new Day_task();
                Day_task_detail dtDetail = new Day_task_detail();
                var checkType = db.Organizations.Where(t => t.org_id == reqst.OrgId).Select(t => t.tporg_tporg_id).FirstOrDefault();
                int? org_id = null;
                if (checkType == (int)TypeOrganization.Section) { org_id = reqst.OrgId; }
                if (req.DetailId == null)
                {
                    reqst.Date = reqst.Date.Date;
                    day = db.Day_task.Where(d => d.dt_date == reqst.Date && d.org_org_id == org_id).FirstOrDefault();

                    if (day == null)
                    {
                        day = new Day_task();
                        day.dt_id = CommonFunction.GetNextId(db, "Day_task");
                        day.status = 1;
                        day.dt_date = reqst.Date;
                        day.dt_type = 1;
                        day.org_org_id = org_id;
                        db.Day_task.Add(day);
                    }

                    dtDetail.dtd_id = CommonFunction.GetNextId(db, "Day_task_detail");
                    dtDetail.dt_dt_id = day.dt_id;
                    MapShiftTaskFromDTO(dtDetail, req);

                    if (reqst.ShiftInfo.Status == 4)
                    {
                        dtDetail.status = (int)DayTaskStatus.Tablet;
                    }
                    else
                    {
                        dtDetail.status = (int)DayTaskStatus.Create;
                    }
                    db.Day_task_detail.Add(dtDetail);
                    
                }

                if (req.DetailId != null)
                {
                    dtDetail = db.Day_task_detail.Where(d => d.dtd_id == req.DetailId).FirstOrDefault();
                    MapShiftTaskFromDTO(dtDetail, req);
                }

                db.SaveChanges();
                trans.Commit();
                return dtDetail.dtd_id;
            }
        }

        public void MapShiftTaskFromDTO(Day_task_detail dtDetail, DayTaskDTO dto)
        {
            dtDetail.emp1_emp_id = dto.EmployeesInfo.Emp1.Id;
            dtDetail.emp2_emp_id = dto.EmployeesInfo.Emp2.Id;
            dtDetail.trak_trak_id = dto.EquipmentsInfo.TracktorInfo.Id;
            dtDetail.equi_equi_id = dto.EquipmentsInfo.EquipInfo.Id;
            dtDetail.otv_emp_id = dto.AvailableInfo.Id;
            dtDetail.tptas_tptas_id = dto.TypeTaskInfo.Id;
            dtDetail.plant_plant_id = dto.PlantInfo != null ? dto.PlantInfo.Id : null;
            dtDetail.shift_num = dto.Shift;
            dtDetail.comment = dto.Comment;
            dtDetail.main_comment = dto.MainComment;
            dtDetail.fiel_fiel_id = dto.FieldInfo != null ? dto.FieldInfo.Id : null;
            dtDetail.id_agroreq = dto.AgroreqInfo != null ? dto.AgroreqInfo.Id : null;
        }

        public string ApproveShiftTask(CopyShiftTaskReq req)
        {
            using (var trans = db.Database.BeginTransaction())
            {
                var dayTask = db.Day_task.Where(d => d.dt_date == req.Date && (req.FilterOrg != -1 ? d.org_org_id == req.FilterOrg : true))
                    .Select(d => d.dt_id).ToList();
                if (dayTask.Count() == 0) return "нет данных";
                var shiftTasks = db.Day_task_detail.Where(dt => dayTask.Contains(dt.dt_dt_id) && (dt.status == (int)DayTaskStatus.Copy
                || dt.status == (int)DayTaskStatus.Create || dt.status == (int)DayTaskStatus.Tablet)).ToList();
                if (!shiftTasks.Any()) return "нет заданий ";

                foreach (var item in shiftTasks)
                {
                    item.status = (int)DayTaskStatus.Assigned;
                }

                db.SaveChanges();
                trans.Commit();
                return "Успех";
            }
          
        }

        public List<int> CopyLocalShiftTask(List<int> req)  {
            using (var trans = db.Database.BeginTransaction())
            {
                var newIdLIst = new List<int>();
                var newId = CommonFunction.GetNextId(db, "Day_task_detail");
                foreach(var item  in req){
                    var copyDtd = db.Day_task_detail.Where(d => d.dtd_id == item).FirstOrDefault();
                    var newDtd = new Day_task_detail();
                    newDtd = copyDtd;
                    newDtd.status = (int)DayTaskStatus.Copy;
                    newDtd.main_comment = null;
                    newDtd.comment = null;
                    ///Обнуление полей: водители и техника
                    newDtd.emp1_emp_id = null;
                    newDtd.emp2_emp_id = null;
                    newDtd.trak_trak_id = null;
                    //
                    db.Day_task_detail.Add(newDtd);
                    newIdLIst.Add(newId);
                    newId++;
                }
             
                db.SaveChanges();
                trans.Commit();
                return newIdLIst;
            }
        }
        //из предыдущего дня
        public string CopyDayTasksShift(CopyShiftTaskReq req)
        {
            var checkType = db.Organizations.Where(t => t.org_id == req.OrgId).Select(t => t.tporg_tporg_id).FirstOrDefault();
            int? org_id = null;
            if (checkType == (int)TypeOrganization.Section) { org_id = req.OrgId; }
             using (var trans = db.Database.BeginTransaction())
            {
                    DateTime date = req.Date.AddDays(-1);
                    var CopyDayTask = db.Day_task.Where(d => d.dt_date == date && (req.FilterOrg != -1 ? d.org_org_id == req.FilterOrg : true)).ToList();
                    if (CopyDayTask.Count() == 0) return "нет данных";
                    var Ids = CopyDayTask.Select(t => t.dt_id).ToList();

                        var CopyShiftTasks = db.Day_task_detail.Where(dt => Ids.Contains(dt.dt_dt_id)).ToList();
                        if (!CopyShiftTasks.Any()) return "нет заданий ";
                        var newDayId = (db.Day_task.Max(t => (int?)t.dt_id) ?? 0) + 1;
                        var newTasId = (db.Day_task_detail.Max(t => (int?)t.dtd_id) ?? 0) + 1;
                        for (int i = 0; i < CopyDayTask.Count; i++) {

                            Day_task day = new Day_task();
                            req.Date = req.Date.Date;
                            var orgDay = CopyDayTask[i].org_org_id;
                            var idDay = CopyDayTask[i].dt_id;
                            day = db.Day_task.Where(d => d.dt_date == req.Date && d.org_org_id == orgDay).FirstOrDefault();
                            if (day == null)
                            {
                                day = new Day_task();
                                day.dt_id = newDayId;
                                day.status = 1;
                                day.dt_date = req.Date;
                                day.dt_type = 1;
                                day.org_org_id = orgDay;
                                db.Day_task.Add(day);
                            }
                            var shiftTasks = CopyShiftTasks.Where(t => t.dt_dt_id == idDay).ToList();

                            for (int j = 0; j < shiftTasks.Count; j++)
                            {

                                Day_task_detail dtd = new Day_task_detail();
                                dtd.dtd_id = newTasId;
                                dtd = shiftTasks[j];
                                dtd.dt_dt_id = day.dt_id;
                                dtd.status = (int)DayTaskStatus.Copy;
                                dtd.parent_id = dtd.dtd_id;
                                db.Day_task_detail.Add(dtd);
                            }
                            newDayId++; newTasId++;

                        }

                            //
                            //Day_task day = new Day_task();
                            //req.Date = req.Date.Date;
                            //day = db.Day_task.Where(d => d.dt_date == req.Date && d.org_org_id == org_id).FirstOrDefault();

                            //if (day == null)
                            //{
                            //    day = new Day_task();
                            //    day.dt_id = CommonFunction.GetNextId(db, "Day_task");
                            //    day.status = 1;
                            //    day.dt_date = req.Date;
                            //    day.dt_type = 1;
                            //    day.org_org_id = req.OrgId;
                            //    db.Day_task.Add(day);
                            //}

                            //var ShiftTasks = db.Day_task_detail.Where(dt => dt.dt_dt_id == day.dt_id).Select(dt=> dt.parent_id).ToList();
                            //var CopyList = CopyShiftTasks.Where(c => !ShiftTasks.Contains(c.dtd_id));
                            //foreach (var item in CopyList)
                            //{
                            //    Day_task_detail dtd = new Day_task_detail();
                            //    dtd = item;
                            //    dtd.dt_dt_id = day.dt_id;
                            //    dtd.status = (int)DayTaskStatus.Copy;
                            //    dtd.parent_id = item.dtd_id;
                            //    db.Day_task_detail.Add(dtd);
                            //}
                            // //
                            db.SaveChanges();
                            trans.Commit();
                            return "Успех";
            }
        }
        public string UpdateDayTasksDetails(DayTaskDetailReq req)
        {
            using (var trans = db.Database.BeginTransaction())
            {
                var dtDetail = db.Day_task_detail.Where(d=> d.dtd_id == req.DetailId).FirstOrDefault();
                dtDetail.emp1_emp_id = req.Driver1Id;
                dtDetail.emp2_emp_id = req.Driver2Id;
                dtDetail.trak_trak_id = req.TraktorId;
                dtDetail.equi_equi_id = req.TrailerId;
                dtDetail.otv_emp_id = req.AvailableId;
                dtDetail.tptas_tptas_id = req.TaskTypeId.Value;
                dtDetail.plant_plant_id = (int)req.PlantId;
                dtDetail.shift_num = req.ShiftNum;
                dtDetail.fiel_fiel_id = req.FieldId.Value;
                db.SaveChanges();
                trans.Commit();
                return "Update";
            }
        }

        public DayTaskDetailReq GetDayTasksDetailsById(int detailId)
        {
            using (var trans = db.Database.BeginTransaction())
            {
                DayTaskDetailReq detailInfo = new DayTaskDetailReq();
                var dtDetail = db.Day_task_detail.Where(d => d.dtd_id == detailId).FirstOrDefault();
                detailInfo.Driver1Id = dtDetail.emp1_emp_id;
                detailInfo.Driver2Id = dtDetail.emp2_emp_id;
                detailInfo.TraktorId = dtDetail.trak_trak_id;
                detailInfo.TrailerId = dtDetail.equi_equi_id;
                detailInfo.AvailableId = dtDetail.otv_emp_id;
                detailInfo.TaskTypeId = dtDetail.tptas_tptas_id;
                detailInfo.PlantId = dtDetail.plant_plant_id;
                detailInfo.ShiftNum = (int)dtDetail.shift_num;
                detailInfo.FieldId = dtDetail.fiel_fiel_id;
                detailInfo.DetailId = dtDetail.dtd_id;
                detailInfo.Date = dtDetail.Day_task.dt_date;
                db.SaveChanges();
                trans.Commit();
                return detailInfo;
            }
        }
        public string DeleteDayTask(List<int> req)  {
            using (var trans = db.Database.BeginTransaction())
            {
                var notDelet = new List<int>();
                foreach (var item in req)
                {
                    var detailTask = db.Day_task_detail.Where(d => d.dtd_id == item).FirstOrDefault();
                    if (detailTask.status == (int)DayTaskStatus.Issued)
                        notDelet.Add(detailTask.dtd_id);
                    else
                    {
                        db.Day_task_detail.Remove(detailTask);
                    }
                }
                db.SaveChanges();
                trans.Commit();
                if (notDelet.Count() > 0) 
                 throw new BllException(11, "Сменные задания не могут быть удалены со статусом \"Выдан\"");
                return "ok";
            }
        }

        public WaysListForCreateModel GetInfoForCreateDayTasks(UpdateDayTaskReq req)
        {
            WaysListForCreateModel result = new WaysListForCreateModel();
            var checkType = db.Organizations.Where(t => t.org_id == req.OrgId).Select(t => t.tporg_tporg_id).FirstOrDefault();
           
            result.OrganizationsList = db.Organizations.Where(p => p.deleted != true &&
                (checkType == (int)TypeOrganization.Section ? p.org_id == req.OrgId : p.tporg_tporg_id == (int)TypeOrganization.Section)).
                                Select(t => new DictionaryItemsDTO
                                {
                                    Id = t.org_id,
                                    Name = t.name
                                }).OrderBy(t => t.Name).ToList();
            if (checkType == (int)TypeOrganization.Company) {
                result.OrganizationsList.Insert(0, new DictionaryItemsDTO { Id = -1, Name = "Все" });
            }
            result.AgroreqList = db.Agrotech_requirment.Where(p => p.deleted != true).Select(p => new DictionaryItemsDTO { Id = p.id, Name = p.name }).OrderBy(p => p.Name).ToList();
            return result;
        }

        public BalanceDaytasksDTO GetStyleShiftTask(GetDayTasksReq req)
        {
            var res = new BalanceDaytasksDTO();
            if (req.Comment.Equals("AgroreqInfo")) { return res; }

            var DateFilter = req.DateFilter.Value.Date;
            var Shift = req.Shift;
            int? id_tc = null;
            var shiftTask = db.Day_task_detail.Where(t => t.dtd_id == req.DetailId).FirstOrDefault();
            if (req.Comment.Equals("PlantInfo") || req.Comment.Equals("FieldInfo"))
            {
                //поля, культуры
                res.FieldsList = db.Fields
                                   .Where(f => f.type == 2 && f.deleted != true &&
                                   ((req.FilterOrg != -1 && req.checkRes != true) ? f.org_org_id == req.FilterOrg : true)).
                                   Select(t => new DictionaryItemForFields
                                   {
                                       Name = t.name,
                                       Id = t.fiel_id,
                                   }).OrderBy(t => t.Name).ToList();
                res.AllFields = db.Fields
                                    .Where(f => f.deleted == false && (f.type == 1 || f.type == 2) &&
                                        ((req.FilterOrg != -1 && req.checkRes != true) ? f.org_org_id == req.FilterOrg : true)).
                                    Select(t => new DictionaryItemForFields
                                    {
                                        Name = t.name,
                                        Id = t.fiel_id,
                                    }).OrderBy(t => t.Name).ToList();

                var values = wayListDb.GetSelectedPlants();

                res.FieldsAndPlants = (from f in db.Fields_to_plants
                                       where f.status == (int)StatusFtp.Approved && ((f.date_ingathering.Year == DateTime.Now.Year && f.plant_plant_id != 36) ||
                                          values.Contains(f.fielplan_id)) &&
                                       ((req.FilterOrg != -1 && req.checkRes != true) ? f.Fields.org_org_id == req.FilterOrg : true)
                                       group new DictionaryPlantItemsDTO
                                       {
                                           Id = f.fiel_fiel_id,
                                           Name = f.Fields.name
                                       } by new
                                       {
                                           f.plant_plant_id,
                                           f.Plants.name
                                       } into g
                                       select new DictionaryItemForFields
                                       {
                                           Id = g.Key.plant_plant_id,
                                           Name = g.Key.name,
                                           Values = g.ToList()
                                       }).OrderBy(t => t.Name).ToList();

                //культура 
                var cult = new DictionaryItemForFields
                {
                    Id = -1,
                    Name = "Культура",
                    RepairStatus = "color: CornflowerBlue;",
                Values = db.Fields.Where(f => f.type == 1 && f.deleted != true && ((req.FilterOrg != -1 && req.checkRes != true) ? f.org_org_id == req.FilterOrg : true)).
                                   Select(t => new DictionaryPlantItemsDTO
                                   {
                                       Name = t.name,
                                       Id = t.fiel_id,
                                   }).OrderBy(t => t.Name).ToList(),
                 };
                res.FieldsAndPlants.Add(cult);

            }

            if (req.Comment.Equals("EquipInfo"))
            {
                res.EquipmnetsList = db.Equipments.Where(p => p.deleted != true
               && ((req.FilterOrg != -1 && req.checkRes != true) ? p.modequi_modequi_id == req.FilterOrg : true)).
                            Select(e => new DictionaryItemsTraktors
                            {
                                Name = e.name,
                                Id = e.equi_id,
                            }).OrderBy(t => t.Name).ToList();
            }

            if (req.Comment.Equals("TracktorInfo") || req.Comment.Equals("FreeTechEmp"))
            {
                //выделение техника
                if (Shift == 1)
                {
                    var traktorIds1 = db.Day_task_detail.Where(d => d.Day_task.dt_date == DateFilter && d.trak_trak_id.HasValue && d.shift_num == 1).Select(p => p.trak_trak_id).ToList();
                    res.Traktors = db.Traktors.Where(t => t.deleted != true && (t.id_type_way_list == 1 || t.id_type_way_list == 2) &&
                        !traktorIds1.Contains(t.trakt_id) && ((req.FilterOrg != -1 && req.checkRes != true) ? t.Garages.org_org_id == req.FilterOrg : true) &&
                        (t.modtr_modtr_id == 1 || t.modtr_modtr_id == 2) )
                        .Select(t => new DictionaryItemsDTO
                    {
                        Name = t.name + " (" + t.reg_num + ")",
                        Id = t.trakt_id,
                        emp_emp_id = t.emp_emp_id,
                        Description = t.Employees.short_name,
                        index = t.modtr_modtr_id,
                    }).ToList();
                    res.unsedTraktors = res.Traktors.Where(t => t.index == 1).ToList();


                }
                else
                {
                    var traktorIds2 = db.Day_task_detail.Where(d => d.Day_task.dt_date == DateFilter && d.trak_trak_id.HasValue && d.shift_num == 2).Select(p => p.trak_trak_id).ToList();
                    res.Traktors = db.Traktors.Where(t => t.deleted != true && (t.id_type_way_list == 1 || t.id_type_way_list == 2)
                        && !traktorIds2.Contains(t.trakt_id) && ((req.FilterOrg != -1 && req.checkRes != true) ? t.Garages.org_org_id == req.FilterOrg : true) &&
                         (t.modtr_modtr_id == 1 || t.modtr_modtr_id == 2))
                        .Select(t => new DictionaryItemsDTO
                    {
                        Name = t.name + " (" + t.reg_num + ")",
                        Id = t.trakt_id,
                        emp_emp_id = t.emp_emp_id,
                        Description = t.Employees.short_name,
                        index = t.modtr_modtr_id,
                    }).ToList();

                    res.unsedTraktors = res.Traktors.Where(t => t.index == 1).ToList();

                }
                var RepairTrakt = db.Repairs_acts.Where(r => r.status != 3 && r.date_start <= DateFilter).Select(r => r.trakt_trakt_id).ToList();
                foreach (var item in res.Traktors.Where(f => RepairTrakt.Contains(f.Id)))
                {
                    item.RepairStatus = "color: red;";
                    item.Bold = true;
                    item.Balance = 1;
                }
            }
            if (req.Comment.Equals("Emp1") || req.Comment.Equals("Emp2") || req.Comment.Equals("AvailableInfo") || req.Comment.Equals("FreeTechEmp"))
            {
                var dayBeforeEmps1 = db.Day_task_detail.AsEnumerable().Where(d => d.Day_task != null && d.Day_task.dt_date == DateFilter.AddDays(-1) &&
                 d.shift_num == 2 && d.emp1_emp_id.HasValue).Select(p => p.emp1_emp_id).ToList();
                var dayBeforeEmps2 = db.Day_task_detail.AsEnumerable().Where(d => d.Day_task != null && d.Day_task.dt_date == DateFilter.AddDays(-1) &&
                   d.shift_num == 2 && d.emp2_emp_id.HasValue).Select(p => p.emp2_emp_id).ToList();

                //сотрудники
                var OtvIds = db.Day_task_detail.Where(d => d.Day_task.dt_date == DateFilter && d.shift_num == Shift && d.otv_emp_id.HasValue).Select(p => p.otv_emp_id).ToList();
                if (Shift == 1)
                {
                    var Employees1Ids = db.Day_task_detail.Where(d => d.Day_task.dt_date == DateFilter && d.shift_num == 1 && d.emp1_emp_id.HasValue).Select(p => p.emp1_emp_id).ToList();
                    var Employees2Ids = db.Day_task_detail.Where(d => d.Day_task.dt_date == DateFilter && d.shift_num == 1 && d.emp2_emp_id.HasValue).Select(p => p.emp2_emp_id).ToList();
                    res.Employees = db.Employees.Where(e => ((req.FilterOrg != -1 && req.checkRes != true) ? e.org_org_id == req.FilterOrg : true) && (e.pst_pst_id == 1 || e.pst_pst_id == 5) &&
                        (!Employees1Ids.Contains(e.emp_id) && !Employees2Ids.Contains(e.emp_id) && !OtvIds.Contains(e.emp_id) && !e.deleted)
                        && (e.driver_class.Equals("1") || e.driver_class.Equals("2")) )
                        .Select(e => new DictionaryItemsDTO
                    {
                        Id = e.emp_id,
                        Name = e.short_name,
                        Manufacturer = e.driver_class,
                    }).ToList();

                    res.unsedEmployees = res.Employees.Where(t => t.Manufacturer.Equals("1") &&
                        !dayBeforeEmps1.Contains(t.Id) && !dayBeforeEmps2.Contains(t.Id)).ToList();
                }
                else
                {
                    var Employees1_2Ids = db.Day_task_detail.Where(d => d.Day_task.dt_date == DateFilter && d.shift_num == 2 && d.emp1_emp_id.HasValue).Select(p => p.emp1_emp_id).ToList();
                    var Employees2_2Ids = db.Day_task_detail.Where(d => d.Day_task.dt_date == DateFilter && d.shift_num == 2 && d.emp2_emp_id.HasValue).Select(p => p.emp2_emp_id).ToList();
                    res.Employees = db.Employees.Where(e => ((req.FilterOrg != -1 && req.checkRes != true) ? e.org_org_id == req.FilterOrg : true) && (e.pst_pst_id == 1 || e.pst_pst_id == 5) &&
                   (!Employees1_2Ids.Contains(e.emp_id) && !Employees2_2Ids.Contains(e.emp_id) && !OtvIds.Contains(e.emp_id) && !e.deleted) &&
                   (e.driver_class.Equals("1") || e.driver_class.Equals("2")) )
                   .Select(e => new DictionaryItemsDTO
                    {
                        Id = e.emp_id,
                        Name = e.short_name,
                        Manufacturer = e.driver_class,
                    }).ToList();
                    res.unsedEmployees = res.Employees.Where(t => t.Manufacturer.Equals("1") &&
                         !dayBeforeEmps1.Contains(t.Id) && !dayBeforeEmps2.Contains(t.Id)).ToList();
                }
                //
                res.ChiefsList = db.Employees.
                                    Where(e => (e.pst_pst_id == 2 || e.pst_pst_id == 7) && e.deleted != true
                                     && ((req.FilterOrg != -1 && req.checkRes != true) ? e.org_org_id == req.FilterOrg : true)).
                                    Select(e => new DictionaryItemsDTO
                                    {
                                        Name = e.short_name,
                                        Id = e.emp_id,
                                    }).OrderBy(t => t.Name).ToList();

                var mainChefs = res.ChiefsList.Where(c => c.Id == 30 || c.Id == 40 || c.Id == 31).ToList();

                foreach (var chief in mainChefs)
                {
                    res.ChiefsList.Remove(chief);
                    res.ChiefsList.Insert(0, chief);
                }
                ////
            }
            if (req.Comment.Equals("FreeTechEmp")) { return res; }
            //==============================================================
            if (req.Comment.Equals("TypeTaskInfo"))
            {
                res.TypeTasks = db.Type_tasks.Where(p => p.deleted != true).
                                  Select(t => new DictionaryItemsDTO
                                  {
                                      Name = t.name,
                                      Id = t.tptas_id,
                                  }).OrderBy(t => t.Name).ToList();
            }
            if (shiftTask.fiel_fiel_id != null && shiftTask.plant_plant_id != null)
            {
                if (req.Comment.Equals("TypeTaskInfo") || req.Comment.Equals("TracktorInfo"))
                {
                    var fielplanId = db.Fields_to_plants.Where(t => t.plant_plant_id == shiftTask.plant_plant_id && t.fiel_fiel_id == shiftTask.fiel_fiel_id
                      && t.date_ingathering.Year == DateFilter.Year)
                    .Select(t => t.fielplan_id).FirstOrDefault();
                    var sortId = db.Sorts_plants_data.Where(t => t.fielplan_fielplan_id == fielplanId).Select(t => t.tpsort_tpsort_id).FirstOrDefault();

                    id_tc = db.TC.Where(t => t.plant_plant_id == shiftTask.plant_plant_id && t.year == DateFilter.Year &&
                       t.tpsort_tpsort_id == sortId).Select(t => (int?)t.id).FirstOrDefault();
                    if (id_tc == null)
                    {
                        id_tc = db.TC.Where(t => t.plant_plant_id == shiftTask.plant_plant_id && t.year == DateFilter.Year &&
                            t.fiel_fiel_id == shiftTask.fiel_fiel_id).Select(t => t.id).FirstOrDefault();
                    }
                }
                if (req.Comment.Equals("TypeTaskInfo"))
                {
                    //планир. работы
                    if (id_tc != null && shiftTask.trak_trak_id == null)
                    {
                        var operationList = db.TC_operation.AsEnumerable().Where(t => t.id_tc == id_tc && DateFilter >= t.date_start.Value.AddDays(-7).Date &&
                        DateFilter <= t.date_finish.Value.AddDays(7).Date).Select(t => t.tptas_tptas_id).ToList();

                        for (int i = 0; i < res.TypeTasks.Count; i++)
                        {
                            if (operationList.Contains((int)res.TypeTasks[i].Id))
                            {
                                res.TypeTasks[i].Bold = false;
                            }
                        }
                    }
                }
                if (req.Comment.Equals("TracktorInfo"))
                {
                    //==================техника зеленый
                    if (id_tc != null && shiftTask.tptas_tptas_id == null && shiftTask.trak_trak_id == null)
                    {
                        var operationList = db.TC_operation.AsEnumerable().Where(t => t.id_tc == id_tc && DateFilter >= t.date_start.Value.AddDays(-7).Date &&
                        DateFilter <= t.date_finish.Value.AddDays(7).Date).Select(t => t.id).ToList();
                        var techList = db.TC_operation_to_Tech.Where(t => operationList.Contains(t.it_tc_operation)).Select(t => t.trakt_trakt_id).ToList();
                        if (techList.Count != 0)
                        {
                            foreach (var item in res.Traktors.Where(f => techList.Contains(f.Id)))
                            {
                                if (item.Balance != 1)
                                {
                                    item.Bold = false;
                                    item.Balance = 2;
                                }
                                else
                                {
                                    item.Balance = 2;
                                }
                            }
                        }
                    }
                }
                //
            }
            if (req.Comment.Equals("TracktorInfo"))
            {
                //закрашивание зеленым техники
                if (shiftTask.tptas_tptas_id != null && id_tc != null)
                {
                    int? operationId = db.TC_operation.Where(t => t.id_tc == id_tc && t.tptas_tptas_id == shiftTask.tptas_tptas_id)
                    .Select(t => (int?)t.id).FirstOrDefault();
                    if (operationId != null)
                    {
                        var techList = db.TC_operation_to_Tech.Where(t => t.it_tc_operation == operationId).Select(t => t.trakt_trakt_id).ToList();

                        foreach (var item in res.Traktors.Where(f => techList.Contains(f.Id)))
                        {
                            if (item.Balance != 1)
                            {
                                item.Bold = false;
                                item.Balance = 2;
                            }
                            else
                            {
                                item.Balance = 2;
                            }
                        }
                    }
                }
            }
            if (req.Comment.Equals("TypeTaskInfo"))
            {
                //закрашивание по технике
                if (shiftTask.trak_trak_id != null && id_tc != null)
                {
                    var operationList = db.TC_operation.Where(t => t.id_tc == id_tc).Select(t => t.id).ToList();
                    var techOperList = db.TC_operation_to_Tech.Where(t => operationList.Contains(t.it_tc_operation) && t.trakt_trakt_id == shiftTask.trak_trak_id).Select(t => t.it_tc_operation).ToList();
                    var tasIds = db.TC_operation.Where(t => techOperList.Contains(t.id)).Select(t => t.tptas_tptas_id).ToList();

                    for (int i = 0; i < res.TypeTasks.Count; i++)
                    {
                        if (tasIds.Contains((int)res.TypeTasks[i].Id))
                        {
                            res.TypeTasks[i].Bold = false;
                        }
                    }
                }
            }
            if (req.Comment.Equals("TypeTaskInfo"))
            {
                var lst1 = res.TypeTasks.Where(t => t.Bold == false).ToList();
                var lst2 = res.TypeTasks.Where(t => t.Bold == null).ToList();
                res.TypeTasks.Clear();
                res.TypeTasks.AddRange(lst1);
                res.TypeTasks.AddRange(lst2);
            }
            if (req.Comment.Equals("TracktorInfo"))
            {
                var techcard = res.Traktors.Where(t => t.Balance == 2).ToList();
                var techOther = res.Traktors.Where(t => t.Balance != 2).ToList();
                res.Traktors.Clear();
                res.Traktors.AddRange(techcard); res.Traktors.AddRange(techOther);
            }


            return res;
        }

        public BalanceDaytasksDTO GetBalanceTraktorsInDayTask(GetDayTasksReq req)
        {
            var result = new BalanceDaytasksDTO();
            var DateFilter = req.DateFilter.Value.Date;
            var Shift = req.Shift;
            if (Shift == 1)
            {
                var traktorIds1 = db.Day_task_detail.Where(d => d.Day_task.dt_date == DateFilter && d.trak_trak_id.HasValue && d.shift_num == 1).Select(p => p.trak_trak_id).ToList();
                result.Traktors = db.Traktors.Where(t => t.deleted != true && (t.id_type_way_list == 1 || t.id_type_way_list == 2) && !traktorIds1.Contains(t.trakt_id)).Select(t => new DictionaryItemsDTO
                {
                    Name = t.name + " (" + t.reg_num + ")",
                    Id = t.trakt_id,
                    emp_emp_id = t.emp_emp_id,
                    Description = t.Employees.short_name,
                }).ToList();
            }
            else
            {
                var traktorIds2 = db.Day_task_detail.Where(d => d.Day_task.dt_date == DateFilter && d.trak_trak_id.HasValue && d.shift_num == 2).Select(p => p.trak_trak_id).ToList();
                result.Traktors = db.Traktors.Where(t => t.deleted != true && (t.id_type_way_list == 1 || t.id_type_way_list == 2) && !traktorIds2.Contains(t.trakt_id)).Select(t => new DictionaryItemsDTO
                {
                    Name = t.name + " (" + t.reg_num + ")",
                    Id = t.trakt_id,
                    emp_emp_id = t.emp_emp_id,
                    Description = t.Employees.short_name,
                }).ToList();
            }
            var OtvIds = db.Day_task_detail.Where(d => d.Day_task.dt_date == DateFilter && d.shift_num == Shift && d.otv_emp_id.HasValue).Select(p => p.otv_emp_id).ToList();
            if (Shift == 1)
            {
                var Employees1Ids = db.Day_task_detail.Where(d => d.Day_task.dt_date == DateFilter && d.shift_num == 1 && d.emp1_emp_id.HasValue).Select(p => p.emp1_emp_id).ToList();
                var Employees2Ids = db.Day_task_detail.Where(d => d.Day_task.dt_date == DateFilter && d.shift_num == 1 && d.emp2_emp_id.HasValue).Select(p => p.emp2_emp_id).ToList();
                result.Employees = db.Employees.Where(e =>  (!Employees1Ids.Contains(e.emp_id) && !Employees2Ids.Contains(e.emp_id) && !OtvIds.Contains(e.emp_id) && !e.deleted)).Select(e => new DictionaryItemsDTO
                {
                    Id = e.emp_id,
                    Name = e.short_name
                }).ToList();
            }
            else
            {
                var Employees1_2Ids = db.Day_task_detail.Where(d => d.Day_task.dt_date == DateFilter && d.shift_num == 2 && d.emp1_emp_id.HasValue).Select(p => p.emp1_emp_id).ToList();
                var Employees2_2Ids = db.Day_task_detail.Where(d => d.Day_task.dt_date == DateFilter && d.shift_num == 2 && d.emp2_emp_id.HasValue).Select(p => p.emp2_emp_id).ToList();
                result.Employees = db.Employees.Where(e => (!Employees1_2Ids.Contains(e.emp_id) && !Employees2_2Ids.Contains(e.emp_id) && !OtvIds.Contains(e.emp_id) && !e.deleted)).Select(e => new DictionaryItemsDTO
                {
                    Id = e.emp_id,
                    Name = e.short_name
                }).ToList();
            }
            var RepairTrakt = db.Repairs_acts.Where(r => r.status != 3 && r.date_start <= DateFilter).Select(r => r.trakt_trakt_id).ToList();
            foreach (var item in result.Traktors.Where(f => RepairTrakt.Contains(f.Id)))
            {
                item.RepairStatus = "color: red;";
                item.Bold = true;
            }   
            return result;
        }

        // получаем список свободных работников
        public PrintShiftTasksEmployees GetFreeEmployeesInDayTask(int dayTaskId)
        {
            PrintShiftTasksEmployees result = new PrintShiftTasksEmployees();
            result.ShiftInfo1 = new List<PrintShiftEmployees>();
            result.ShiftInfo2 = new List<PrintShiftEmployees>();

            var dayTask = db.Day_task.Where(dt => dt.dt_id == dayTaskId).FirstOrDefault();

            if (dayTask != null)
            {
                var datibegin = dayTask.dt_date;
                result.DateShifts = datibegin.Day + "-" + datibegin.AddDays(1).Day + "." + datibegin.ToString("MM");
                result.DateBegin = datibegin;
                result.Comment = dayTask.discription;
            }

            var Employees1Ids = db.Day_task_detail.Where(d => d.Day_task.dt_date == dayTask.dt_date && d.shift_num == 1 && d.emp1_emp_id.HasValue).Select(p => p.emp1_emp_id).ToList();
            var Employees2Ids = db.Day_task_detail.Where(d => d.Day_task.dt_date == dayTask.dt_date && d.shift_num == 1 && d.emp2_emp_id.HasValue).Select(p => p.emp2_emp_id).ToList();
            var OtvIds = db.Day_task_detail.Where(d => d.Day_task.dt_date == dayTask.dt_date && d.shift_num == 1 && d.otv_emp_id.HasValue).Select(p => p.otv_emp_id).ToList();
            result.ShiftInfo1 = db.Employees.Where(e => (!Employees1Ids.Contains(e.emp_id) && !Employees2Ids.Contains(e.emp_id) && !OtvIds.Contains(e.emp_id) && !e.deleted)).Select(e => new PrintShiftEmployees
            {
                Func = e.Posts.name,
                Name = e.short_name
            }).ToList();

            Employees1Ids = db.Day_task_detail.Where(d => d.Day_task.dt_date == dayTask.dt_date && d.shift_num == 2 && d.emp1_emp_id.HasValue).Select(p => p.emp1_emp_id).ToList();
            Employees2Ids = db.Day_task_detail.Where(d => d.Day_task.dt_date == dayTask.dt_date && d.shift_num == 2 && d.emp2_emp_id.HasValue).Select(p => p.emp2_emp_id).ToList();
            OtvIds = db.Day_task_detail.Where(d => d.Day_task.dt_date == dayTask.dt_date && d.shift_num == 2 && d.otv_emp_id.HasValue).Select(p => p.otv_emp_id).ToList();
            result.ShiftInfo2 = db.Employees.Where(e => (!Employees1Ids.Contains(e.emp_id) && !Employees2Ids.Contains(e.emp_id) && !OtvIds.Contains(e.emp_id) && !e.deleted)).Select(e => new PrintShiftEmployees
            {
                Func = e.Posts.name,
                Name = e.short_name
            }).ToList();

            return result;
        }

        public List<DictionaryItemsDTO> GetOgranizationList(){
            var list = db.Organizations.Select(o => new DictionaryItemsDTO()
            {
                Name = o.name,
                Id = o.org_id
            }).ToList();
            return list;
         }

        public bool? IsDriverWayBillByShiftTaskId(int? stId)
        {
            if (stId == null)
                return null;

            var grtrid = (from dtd in db.Day_task_detail
                          where dtd.dtd_id == stId
                          select dtd.Traktors.id_type_way_list).FirstOrDefault();

            switch (grtrid)
            {
                case 1:
                    return false;
                case 2:
                    return true;
                default:
                    return null;
            }
        }
    }
}
