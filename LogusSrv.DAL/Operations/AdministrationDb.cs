﻿using System;
using System.Data.Entity;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LogusSrv.CORE.Exceptions;
using LogusSrv.DAL.Entities.DTO.ActsSzr;
using LogusSrv.DAL.Entities.DTO;
using LogusSrv.DAL.Entities.Req;
using LogusSrv.DAL.DBModel;
using LogusSrv.DAL.Utils;
using LogusSrv.DAL.Enums;
using LogusSrv.DAL.Entities.Res;
using LogusSrv.DAL.Operations;
using LogusSrv.DAL.Operations.DictionaryOperations;
using System.Configuration;
using System.Data.Entity.Validation;
using System.IO;
using OfficeOpenXml;

namespace LogusSrv.DAL.Operations
{
    public class AdministrationDb
    {
        protected readonly LogusDBEntities db = new LogusDBEntities();
        protected readonly TechCardDb techcardDb = new TechCardDb();
        protected readonly TechCardTempDb techTempDb = new TechCardTempDb();
        public readonly ReportsModuleDb reportsModuleDb = new ReportsModuleDb();
        List<UserModel> checkNum = new List<UserModel>();
        public UserListResponse GetUsers(GridDataReq req)
        {
            var resultList = new UserListResponse();
            var res = (from d in db.Users
                       select new UserModel
                       {
                           Id = d.user_id,
                           UserId = d.user_id,
                           UserLogin = d.login,
                           Password = d.user_password,
                           Note = d.note,
                           NameUser = new DictionaryItemsDTO
                           {
                               Id = d.Employees.emp_id,
                               Name = d.Employees.short_name,
                           },
                           NameRole = new DictionaryItemsDTO
                           {
                               Id = d.Roles.role_id,
                               Name = d.Roles.role_name,
                           },
                       }).ToList();

            var CountItems = res.Count();
            var TotalPage = (double)CountItems / (double)req.PageSize;
            double CountPages = TotalPage < 0 ? 1 : Math.Ceiling(TotalPage);

            var gridData = GridHelpers<UserModel>.GetGridData(res.AsQueryable(), req);
            resultList.Values = gridData.OrderBy(m => m.NameUser.Name).ToList();
            resultList.CountPages = (int)CountPages;
            resultList.CountItems = CountItems;
            return resultList;

        }

        public ReportFile ChangeCultureExcel(string pathTemplate)
        {
            var tmpl = new FileInfo(pathTemplate);
            using (ExcelPackage pck = new ExcelPackage(tmpl, true))
            {
                ExcelWorksheet wsh = pck.Workbook.Worksheets[1];
                ExcelWorksheet wsh2 = pck.Workbook.Worksheets[2];
                wsh.Name = "Путевые листы";
                wsh2.Name = "ТМЦ";
                var year = DateTime.Now.Year;
                var tasks = db.Tasks.Where(t => t.plant_plant_id == -1 && t.Sheet_tracks.date_begin.Year == year).ToList();

                var tmc = db.TMC_Records.Where(t => t.cont_cont_id == -1 && t.TMC_Acts.act_date.Value.Year == year).ToList();

                uint k = 3;
                using (var trans = db.Database.BeginTransaction())
                {
                 try
                {
                     //ПЛ
                 for (int i = 0; i < tasks.Count; i++)
                {
                    var field = tasks[i].fiel_fiel_id;
                    var fields = db.Fields_to_plants.Where(t => t.fiel_fiel_id == field && t.date_ingathering.Year == year && t.plant_plant_id != 36 ).ToList();
                            //замена ПЛ
                            if (fields.Count == 1) {
                        tasks[i].plant_plant_id = fields[0].fielplan_id;
                       wsh.Cells["A" + k.ToString()].Value = tasks[i].Sheet_tracks.num_sheet;
                       wsh.Cells["B" + k.ToString()].Value = tasks[i].Sheet_tracks.date_begin.Hour < tasks[i].Sheet_tracks.date_end.Hour ? 1 : 2;
                       wsh.Cells["C" + k.ToString()].Value = tasks[i].Sheet_tracks.date_begin.ToString("dd.MM.yyyy");
                       wsh.Cells["D" + k.ToString()].Value = tasks[i].Sheet_tracks.Employees.short_name;
                       wsh.Cells["E" + k.ToString()].Value = "Культура";
                       wsh.Cells["F" + k.ToString()].Value = fields[0].Fields.name;
                       wsh.Cells["G" + k.ToString()].Value = tasks[i].Type_tasks.name;
                       wsh.Cells["H" + k.ToString()].Value = fields[0].Plants.name;
                       wsh.Cells["I" + k.ToString()].Value = "Выполнена замена";
                                k++;
                            }
                            //анализ ПЛ
                            if (fields.Count >= 2)
                    {
                                wsh.Cells["A" + k.ToString()].Value = tasks[i].Sheet_tracks.num_sheet;
                                wsh.Cells["B" + k.ToString()].Value = tasks[i].Sheet_tracks.date_begin.Hour < tasks[i].Sheet_tracks.date_end.Hour ? 1 : 2;
                                wsh.Cells["C" + k.ToString()].Value = tasks[i].Sheet_tracks.date_begin.ToString("dd.MM.yyyy");
                                wsh.Cells["D" + k.ToString()].Value = tasks[i].Sheet_tracks.Employees.short_name;
                                wsh.Cells["E" + k.ToString()].Value = "Культура";
                                wsh.Cells["F" + k.ToString()].Value = fields[0].Fields.name;
                                wsh.Cells["G" + k.ToString()].Value = tasks[i].Type_tasks.name;
                                wsh.Cells["I" + k.ToString()].Value = "требует анализа";
                                k++;
                     }

                 }
                        k = 3;
                        //ТМЦ
                        for (int i = 0; i < tmc.Count; i++)
                        {
                            var field = tmc[i].fielplan_fielplan_id;
                            var fields = db.Fields_to_plants.Where(t => t.fiel_fiel_id == field && t.date_ingathering.Year == year && t.plant_plant_id != 36).ToList();
                            var utv_id = tmc[i].TMC_Acts.emp_emp_id;
                            //замена ТМЦ
                            if (fields.Count == 1)
                            {
                                tmc[i].fielplan_fielplan_id = fields[0].fielplan_id;
                                tmc[i].cont_cont_id = 0;
                                wsh2.Cells["A" + k.ToString()].Value = tmc[i].TMC_Acts.act_num;
                                wsh2.Cells["B" + k.ToString()].Value = tmc[i].TMC_Acts.act_date.Value.ToString("dd.MM.yyyy");
                                wsh2.Cells["C" + k.ToString()].Value = db.Employees.Where(t => t.emp_id == utv_id).Select(t => t.short_name).FirstOrDefault();
                                wsh2.Cells["D" + k.ToString()].Value = tmc[i].Employees1.short_name;
                                wsh2.Cells["E" + k.ToString()].Value = "Культура";
                                wsh2.Cells["F" + k.ToString()].Value = fields[0].Fields.name;
                                wsh2.Cells["G" + k.ToString()].Value = fields[0].Plants.name;
                                wsh2.Cells["H" + k.ToString()].Value = "Выполнена замена";
                                k++;
                            }
                            //анализ ТМЦ
                            if (fields.Count >= 2)
                            {
                                wsh2.Cells["A" + k.ToString()].Value = tmc[i].TMC_Acts.act_num;
                                wsh2.Cells["B" + k.ToString()].Value = tmc[i].TMC_Acts.act_date.Value.ToString("dd.MM.yyyy");
                                wsh2.Cells["C" + k.ToString()].Value = db.Employees.Where(t => t.emp_id == utv_id).Select(t => t.short_name).FirstOrDefault();
                                wsh2.Cells["D" + k.ToString()].Value = tmc[i].Employees1 != null ? tmc[i].Employees1.short_name : null;
                                wsh2.Cells["E" + k.ToString()].Value = "Культура";
                                wsh2.Cells["F" + k.ToString()].Value = fields[0].Fields.name;
                                wsh2.Cells["G" + k.ToString()].Value = "";
                                wsh2.Cells["H" + k.ToString()].Value = "требует анализа";
                                k++;
                            }

                        }

                     db.SaveChanges();
                     trans.Commit();
                    }
                    catch (Exception)
                    {
                        trans.Rollback();
                        throw;
                    }
                }

                var r = new ReportFile
                {
                    data = pck.GetAsByteArray(),
                    fileName = "Замена культур по севообороту.xlsx"
                };
                return r;
            }
        }



        public RoleListResponse GetRoles(GridDataReq req)
        {
            var resultList = new RoleListResponse();
            var res = (from d in db.Roles
                       select new RoleModel
                       {
                           Id = d.role_id,
                           Name = d.role_name,
                           EmpList = db.Roles_subsystems.Where(t => t.role_role_id == d.role_id).Select(t =>  new EmpListModel 
                           {
                           Id = t.role_role_id,
                           IdBox = t.subsystem_id,
                           }
                           ).ToList(),
                       }).ToList();

            var CountItems = res.Count();
            var TotalPage = (double)CountItems / (double)req.PageSize;
            double CountPages = TotalPage < 0 ? 1 : Math.Ceiling(TotalPage);

            var gridData = GridHelpers<RoleModel>.GetGridData(res.AsQueryable(), req);
            resultList.Values = gridData.OrderBy(m => m.Name).ToList();
            resultList.CountPages = (int)CountPages;
            resultList.CountItems = CountItems;
            return resultList;

        }
        public UserSelectedPlants GetInfoPlants(int? year)
        {
            UserSelectedPlants result = new UserSelectedPlants();
            result.AllPlants = db.Fields_to_plants
                              .Where(f => f.plant_plant_id != 36 && f.date_ingathering.Year == year).
                              Select(t => new DictionaryItemsDTO
                              {
                                  Name = t.Plants.name,
                                  Id = t.Plants.plant_id,
                              }).Distinct().OrderBy(t => t.Name).ToList();
            var yearList = new List<DictionaryItemsDTO>();
            var endYear = DateTime.Now.Year + 1;
            for (var i = endYear; i >= endYear - 10; i--)
            {
                var item = new DictionaryItemsDTO { Id = i, Name = i.ToString() };
                yearList.Add(item);
            }
            result.YearList = yearList.ToList();
            return result;
        }

        public InfoUser GetInfoUser()
        {
            InfoUser result = new InfoUser();

            result.UserList = (from e in db.Employees where e.deleted == false
                               select new DictionaryItemsDTO
                               {
                                   Id = e.emp_id,
                                   Name = e.short_name
                               }).ToList();

            result.RoleList = (from r in db.Roles
                               select new DictionaryItemsDTO
                               {
                                   Id = r.role_id,
                                   Name = r.role_name
                               }).ToList();

            return result;
        }

        public int UpdateDic(List<SaveUser> req, string saltPostfix)
        {
            //удаляем повторяющиеся строки
            for (int i = 0; i < req.Count; i++)
            {
                for (int j = 0; j < req.Count; j++)
                {

                    if (j != i && req[i].Document.NameUser.Id == req[j].Document.NameUser.Id &&
                    req[i].Document.NameRole.Id == req[j].Document.NameRole.Id && req[i].Document.UserLogin == req[j].Document.UserLogin &&
                    req[i].Document.Password == req[j].Document.Password && req[i].Document.Note == req[j].Document.Note)
                    { req.RemoveAt(j); j--; }
                }
            }
            //проверка  повторяющийся логинов при обновлении 
            List<string> lst = new List<string>();
            //
            for (int j = 0; j < req.Count; j++)
            {
                lst.Add(req[j].Document.UserLogin);
            }

            if (lst.Count != lst.Distinct().ToList().Count) { throw new BllException(14, "Пользователь с таким логином уже существует"); }

            using (var trans = db.Database.BeginTransaction())
            {
                try
                {
                    for (var i3 = 0; i3 < req.Count; i3++)
                    {
                        if (req[i3].Document.Id != 0)
                        {
                            for (var i = 0; i < req.Count; i++)
                            {
                                var list1 = db.Database.SqlQuery<UserModel>(
                                 string.Format(@"select login as UserLogin from Users where user_id=" + req[i].Document.Id)).ToList();

                                if (list1[0].UserLogin.Equals(req[i].Document.UserLogin))
                                {
                                    checkNum = db.Database.SqlQuery<UserModel>(
                                string.Format(@"select login as UserLogin from Users where login<>" + "'" + req[i].Document.UserLogin + "'")).ToList();
                                }
                                else
                                {
                                    checkNum = db.Database.SqlQuery<UserModel>(
                                  string.Format(@"select login as UserLogin from Users")).ToList();
                                }

                                for (int i1 = 0; i1 < checkNum.Count; i1++)
                                {
                                    if (checkNum[i1].UserLogin.Equals(req[i].Document.UserLogin)) 
                                    { throw new BllException(14, "Пользователь с таким логином уже существует"); }
                                }

                                var obj = req[i].Document;
                                var updateDic = db.Users.Where(o => o.user_id == obj.Id).FirstOrDefault();
                                updateDic.login = obj.UserLogin;
                                updateDic.user_password = obj.Password;
                                updateDic.password = LoginDb.GenerateSaltedHash(obj.Password, saltPostfix);
                                updateDic.note = obj.Note;
                                updateDic.role_role_id = obj.NameRole.Id;
                                updateDic.emp_emp_id = (int)obj.NameUser.Id;
                                db.SaveChanges();
                            }
                            db.SaveChanges();

                            try
                            {
                                trans.Commit();

                            }

                            catch (Exception a)
                            {
                            }
    

                        }
                    }
                }
                catch (Exception e)
                {
                    trans.Rollback();
                    throw;
                }
            }
            return req.Count;
        }

        public string AddEditDicRole(List<RoleModel> rows)
        {

            using (var trans = db.Database.BeginTransaction())
            {
                try
                {
                    var newIdRole = (db.Roles.Max(t => (int?)t.role_id) ?? 0) + 1;
                    var newId = (db.Roles_subsystems.Max(t => (int?)t.rss_id) ?? 0) + 1;
                    var arr = new List<Roles_subsystems>();
                    rows = rows.Where(t => t.Id != 10).ToList();
                    for (int i = 0; i < rows.Count; i++)
                    {
                        var id = rows[i].Id;
                        //update rows
                        if (id != null)
                        {
                            var updDic = db.Roles.Where(t => t.role_id == id).FirstOrDefault();

                            if (updDic.role_id == 10 && !rows[i].Name.Equals("Администратор"))
                            { throw new BllException(15, "Редактировать роль 'Администратор' запрещено."); }
                            var name = rows[i].Name;
                            var checkRole = db.Roles.Where(t => t.role_id != updDic.role_id && t.role_name.Equals(name)).FirstOrDefault();
                            if (checkRole != null)
                            {
                                { throw new BllException(15, "С таким именем роль уже существует."); }
                            }
                            updDic.role_name = rows[i].Name;
                            var delDic = db.Roles_subsystems.Where(t => t.role_role_id == id && t.role_role_id != 10).ToList();
                            db.Roles_subsystems.RemoveRange(delDic);
                            arr = new List<Roles_subsystems>();

                            for (int j = 0; j < rows[i].EmpList.Count; j++)
                            {
                                var newDic = new Roles_subsystems
                                {
                                    rss_id = newId,
                                    role_role_id = id,
                                    subsystem_id = rows[i].EmpList[j].IdBox,
                                };
                                arr.Add(newDic);
                                newId++;
                            };
                            db.Roles_subsystems.AddRange(arr);
                        }
                    }
                    rows = rows.Where(t => t.Id == null).ToList();
                    for (int i = 0; i < rows.Count; i++)
                    {
                        var id = rows[i].Id;
                        if (id == null)
                        {
                            var name = rows[i].Name;
                            var checkRole = db.Roles.Where(t => t.role_name.Equals(name)).FirstOrDefault();
                            if (checkRole != null) {
                                { throw new BllException(15, "С таким именем роль уже существует."); }
                            }
                            //save rows
                            var newDicRole = new Roles
                            {
                                role_id = newIdRole,
                                role_name = rows[i].Name,
                            };
                            db.Roles.Add(newDicRole);
                            db.SaveChanges();
                            newIdRole = (db.Roles.Max(t => (int?)t.role_id) ?? 0);
                            arr = new List<Roles_subsystems>();
                            for (int j = 0; j < rows[i].EmpList.Count; j++)
                            {
                                var newDic = new Roles_subsystems
                                {
                                    rss_id = newId,
                                    role_role_id = newIdRole,
                                    subsystem_id = rows[i].EmpList[j].IdBox,
                                };
                               arr.Add(newDic);
                                newId++;
                            };
                        db.Roles_subsystems.AddRange(arr);
                        newIdRole++;
                        }
                    }
                }
                catch (Exception)
                {
                    trans.Rollback();
                    throw;
                }
                db.SaveChanges();
                trans.Commit();
            }

                return null;
        }



        public string AddDic(List<UserModel> rows, string saltPostfix)
        {
            List<string> lst = new List<string>();
            for (int j = 0; j < rows.Count; j++)
            {
                lst.Add(rows[j].UserLogin);
            }

            if (lst.Count != lst.Distinct().ToList().Count) { throw new BllException(14, "Пользователь с таким логином уже существует"); }
            var list = db.Database.SqlQuery<UserModel>(
       string.Format(@"select login as UserLogin from Users")).ToList();
            for (int j = 0; j < rows.Count; j++)
            {
                for (int i = 0; i < list.Count; i++)
                {
                    if (list[i].UserLogin.Equals(rows[j].UserLogin)) { throw new BllException(14, "Пользователь с таким логином уже существует"); }
                }
            }
            using (var trans = db.Database.BeginTransaction())
            {
                try
                {
                    for (var i = 0; i < rows.Count; i++)
                    {
                        var row = rows[i];
                        var gh = new Users
                        {
                            emp_emp_id = (int)row.NameUser.Id,
                            login = row.UserLogin,
                            password = LoginDb.GenerateSaltedHash(row.Password, saltPostfix),
                            user_password = row.Password,
                            note = row.Note,
                            role_role_id = row.NameRole.Id,
                        };
                        db.Users.Add(gh);
                    }

                    db.SaveChanges();
                    trans.Commit();
                }
                catch (Exception)
                {
                    trans.Rollback();
                    throw;
                }
            }
            return "success";
        }

        public string DeleteUser(List<DelRow> req)
        {
            using (var trans = db.Database.BeginTransaction())
            {

                for (int i = 0; i < req.Count; i++)
                {
                    var n = req[i].rows.Id;
                    var doc = db.Users.Where(u => u.user_id == n).FirstOrDefault();

                    if (doc.Employees.short_name.Equals("Администратор")) {
                        throw new BllException(15, "Удалять пользователя Администратор запрещено."); 
                    }

                    if (doc == null)
                        throw new BllException(15, "Документ не был найден , обновите страницу");
                    db.Users.Remove(doc);
                }

                db.SaveChanges();
                trans.Commit();
            }
            return "ok";
        }
        //DeleteRole
        public string DeleteRole(List<DelRow> req)
        {
            using (var trans = db.Database.BeginTransaction())
            {

                for (int i = 0; i < req.Count; i++)
                {
                    var n = req[i].rows.Id;

                    if (n == 10) { throw new BllException(15, "Удалять Роль 'Администратор' запрещено."); }

                    var doc = db.Roles.Where(u => u.role_id == n).FirstOrDefault();
                    var doc2 = db.Roles_subsystems.Where(u => u.role_role_id == n).ToList();
                    if (doc == null)
                        throw new BllException(15, "Документ не был найден , обновите страницу");
                    db.Roles.Remove(doc);
                    db.Roles_subsystems.RemoveRange(doc2);
                }

                db.SaveChanges();
                trans.Commit();
            }
            return "ok";
        }

        public List<string> GetSubsystemsByRole(int role_id)
        {
            var list = db.Roles_subsystems.Where(t => t.role_role_id == role_id).Select(t => t.subsystem_id).ToList();
            return list;
        }


        //============используемые культуры
        public string AddPlant(PlantModel row)
        {
            if (row.year == DateTime.Now.Year) {
                throw new BllException(14, "Данная культура имеется в списке. Сохранение невозможно.");
            }
            var check = db.Selected_Plants.Where(t => t.year == row.year && t.plant_plant_id == row.plantId).FirstOrDefault();
            if (check != null) {
                throw new BllException(14, "Данная культура имеется в списке. Сохранение невозможно.");
            }

            using (var trans = db.Database.BeginTransaction())
            {
                try
                {
                        var arr = new Selected_Plants
                        {
                          plant_plant_id = row.plantId,
                          year = row.year,
                          type = 1,
                        };
                        db.Selected_Plants.Add(arr);

                    db.SaveChanges();
                    trans.Commit();
                }
                catch (Exception)
                {
                    trans.Rollback();
                    throw;
                }
            }
            return "success";
        }
        
        //добавляем урожай
        public string AddHarvestNZP(PlantModel row)
        {

            var check = db.Selected_Plants.Where(t => t.year == row.year && t.plant_plant_id == 36 && t.type == 2).FirstOrDefault();
            if (check != null)
            {
                throw new BllException(14, "Данная культура имеется в списке. Сохранение невозможно.");
            }
            using (var trans = db.Database.BeginTransaction())
            {
                try
                {
                    var arr = new Selected_Plants
                    {
                        plant_plant_id = row.plantId,
                        year = row.year,
                        type = 2,
                    };
                    db.Selected_Plants.Add(arr);

                   //запись урожая по полям
                    var checkGross = db.Fields_to_plants.Where(t => t.plant_plant_id == 36 && t.date_ingathering.Year == row.year).FirstOrDefault();

                    if (checkGross == null)
                    {
                        var fields = db.Fields.Where(t => t.type == 1 && t.deleted != true).ToList();
                        for (int i = 0; i < fields.Count; i++)
                        {
                            var data = new Fields_to_plants
                            {
                                fiel_fiel_id = fields[i].fiel_id,
                                plant_plant_id = 36,
                                date_sowing = new DateTime(row.year, 1, 1),
                                date_ingathering = new DateTime(row.year, 1, 1),
                                area_plant = 0,
                                status = 1,

                            };
                            db.Fields_to_plants.Add(data);
                        }
                    }

                   db.SaveChanges();
                   trans.Commit();
                }
                catch (Exception)
                {
                    trans.Rollback();
                    throw;
                }
            }
            return "success";
        }

        public ParamOperationModel GetParamOrganizationDetails()
        {
            var r = new ParamOperationModel();
            var res = db.Parameters;
            var org = res.Where(t => t.name == "NameOrg").Select(t => t.value).FirstOrDefault();
            r.OrgId =  !String.IsNullOrEmpty(org) ? Int32.Parse(org) : (int?)null;
            r.INN = res.Where(t => t.name == "INN").Select(t => t.value).FirstOrDefault();
            r.KPP = res.Where(t => t.name == "KPP").Select(t => t.value).FirstOrDefault();
            r.OGRN = res.Where(t => t.name == "OGRN").Select(t => t.value).FirstOrDefault();
            r.Key = res.Where(t => t.name == "activateKey").Select(t => t.value).FirstOrDefault();
            r.Key = r.Key.ToUpper();
            var dir = res.Where(t => t.name == "Director").Select(t => t.value ).FirstOrDefault();
            r.DirectorId = !String.IsNullOrEmpty(dir) ? Int32.Parse(dir) : (int?)null;
            return r;
        }
        public ParamOperationModel GetControlTechDetails() {
            var r = new ParamOperationModel();
            var res = db.Parameters;
            r.Stop = res.Where(t => t.name == "Stop").Select(t => t.value).FirstOrDefault();
            r.Speed = res.Where(t => t.name == "Speed").Select(t => t.value).FirstOrDefault();
            r.Parking = res.Where(t => t.name == "Parking").Select(t => t.value).FirstOrDefault();
            r.MaxParking = res.Where(t => t.name == "MaxParking").Select(t => t.value).FirstOrDefault();

            var d = DateTime.Now;
            r.summer_date = res.Where(t => t.name == "summerDate").AsEnumerable().
                Select(t => DateTime.Parse(t.value)).FirstOrDefault();
            r.summer_date = r.summer_date < new DateTime(2,2,2) ? d : r.summer_date;
            r.winter_date = res.Where(t => t.name == "winterDate").AsEnumerable().Select(t => DateTime.Parse(t.value)).FirstOrDefault();
            r.winter_date = r.winter_date < new DateTime(2, 2, 2) ? d : r.winter_date;
            return r;
        }



        public ParamOperDistModel GetInfoParamOrganization()
        {
            var res = new ParamOperDistModel();
             res.EmployeeList = (from f in db.Employees
                       where f.pst_pst_id == 9
                       select new DictionaryItemsDTO
                       {
                           Id = f.emp_id,
                           Name = f.short_name
                       }).ToList();
             res.OrgList = (from f in db.Organizations
                            where f.tporg_tporg_id == (int)TypeOrganization.Company
                                 select new DictionaryItemsDTO
                                 {
                                     Id = f.org_id,
                                     Name = f.name,
                                     Description = f.address,
                                 }).ToList();
            return res;
         }

        public string SaveControlTech(ParamOperationModel data) {
            data.summer_date = data.summer_date.AddHours(3);
            data.winter_date = data.winter_date.AddHours(3);
            var nId = (db.Parameters.Max(t => (int?)t.par_id) ?? 0) + 1;
            var speed = db.Parameters.Where(t => t.name.Equals("Speed")).FirstOrDefault();
            var parking = db.Parameters.Where(t => t.name.Equals("Parking")).FirstOrDefault();
            var stop = db.Parameters.Where(t => t.name.Equals("Stop")).FirstOrDefault();
            var maxParking = db.Parameters.Where(t => t.name.Equals("MaxParking")).FirstOrDefault();
            var summerDate = db.Parameters.Where(t => t.name.Equals("summerDate")).FirstOrDefault();
            var winterDate = db.Parameters.Where(t => t.name.Equals("winterDate")).FirstOrDefault();
            using (var trans = db.Database.BeginTransaction())
            {
                try
                {
                    if (speed == null)
                    {
                        var dic = new Parameters
                        {
                            par_id = nId,
                            name = "Speed",
                            value = data.Speed,
                        };
                        db.Parameters.Add(dic);
                        nId++;
                    }
                    else
                    {
                        speed.value = data.Speed;
                    }
                    //2
                    if (parking == null)
                    {
                        var dic = new Parameters
                        {
                            par_id = nId,
                            name = "Parking",
                            value = data.Parking,
                        };
                        db.Parameters.Add(dic);
                        nId++;
                    }
                    else
                    {
                        parking.value = data.Parking;
                    }
                    //3
                    if (stop == null)
                    {
                        var dic = new Parameters
                        {
                            par_id = nId,
                            name = "Stop",
                            value = data.Stop,
                        };
                        db.Parameters.Add(dic);
                        nId++;
                    }
                    else
                    {
                        stop.value = data.Stop;
                    }
                    //4
                    if (maxParking == null)
                    {
                        var dic = new Parameters
                        {
                            par_id = nId,
                            name = "MaxParking",
                            value = data.MaxParking,
                        };
                        db.Parameters.Add(dic);
                        nId++;
                    }
                    else
                    {
                        maxParking.value = data.MaxParking;
                    }
                    //5
                    if (summerDate == null)
                    {
                        var dic = new Parameters
                        {
                            par_id = nId,
                            name = "summerDate",
                            value = data.summer_date.ToString(),
                        };
                        db.Parameters.Add(dic);
                        nId++;
                    }
                    else
                    {
                        summerDate.value = data.summer_date.ToString();
                    }
                    //6
                    if (winterDate == null)
                    {
                        var dic = new Parameters
                        {
                            par_id = nId,
                            name = "winterDate",
                            value = data.winter_date.ToString(),
                        };
                        db.Parameters.Add(dic);
                        nId++;
                    }
                    else
                    {
                        winterDate.value = data.winter_date.ToString();
                    }
                    db.SaveChanges();
                    trans.Commit();
                }
                catch (Exception) { }
            }

            return "success";
        }

        public string SaveParamOrganization(ParamOperationModel data)
        {
            if (data.INN == null || (data.INN.Length != 10 && data.INN.Length != 12))
            {
                throw new BllException(14, "ИНН должен иметь 10 или 12 символов."); 
            }
            if (data.KPP == null || data.KPP.Length != 9)
            {
                throw new BllException(14, "КПП должен иметь 9 символов.");
            }
            if (data.OGRN == null ||  data.OGRN.Length != 13)
            {
                throw new BllException(14, "ОГРН должен иметь 13 символов.");
            }
            using (var trans = db.Database.BeginTransaction())
            {
                try
                {
                    var updDic = db.Parameters.ToList();
                    var dic = updDic.Where(t => t.name.Equals("NameOrg")).FirstOrDefault();
                    dic.value = data.OrgId.ToString();

                    dic = updDic.Where(t => t.name.Equals("INN")).FirstOrDefault();
                    dic.value = data.INN;

                     dic = updDic.Where(t => t.name.Equals("KPP")).FirstOrDefault();
                     dic.value = data.KPP;

                    dic = updDic.Where(t => t.name.Equals("OGRN")).FirstOrDefault();
                    dic.value = data.OGRN;

                    dic = updDic.Where(t => t.name.Equals("Director")).FirstOrDefault();
                    dic.value = data.DirectorId.ToString();
                    db.SaveChanges();
                    trans.Commit();
                }
                catch (Exception) { }
            }
            return "success";
        }


        //
        public List<PlantModel> GetPlant()
        {
            var data = db.Selected_Plants.Where(t=>t.type == 1).Select(t => new PlantModel { plantId = t.plant_plant_id.Value, year = t.year.Value, id = t.sel_id,
            name = db.Plants.Where(d => d.plant_id == t.plant_plant_id ).Select(d=>d.name).FirstOrDefault() }).ToList();
            return data;
        }
        public List<PlantModel> GetHarvestNZP()
        {
            var date = DateTime.Now.Year;
            var data = db.Selected_Plants.Where(t => t.type == 2 && t.year > date).Select(t => new PlantModel
            {
                plantId = t.plant_plant_id.Value,
                year = t.year.Value,
                id = t.sel_id,
                name = db.Plants.Where(d => d.plant_id == t.plant_plant_id).Select(d => d.name).FirstOrDefault()
            }).ToList();
            return data;
        }
        //срезы GetSectionTechcard
        public List<String> GetSectionTechcard()
        {
            var dates = db.TC_temp.Where(t => t.type_module == 2).Select(t => t.date).Distinct().ToList();
            var data = new List<String>();
            for (int i = 0; i < dates.Count; i++)
            {
                data.Add(dates[i].Value.ToString("dd.MM.yyyy"));
            }
            return data;
        }
        public int DeletePlant(int id)
        {
            using (var trans = db.Database.BeginTransaction())
            {
                try
                {
                    var del = db.Selected_Plants.Where(t => t.sel_id == id && t.type == 1).FirstOrDefault();
                    if (del != null)
                    {
                        db.Selected_Plants.Remove(del);
                    }
                    db.SaveChanges();
                    trans.Commit();
                }
                catch (Exception) { }
            }
            return 0;
        }

        //удаление урожай
        public int DeleteHarvestNZP(int id)
        {
            using (var trans = db.Database.BeginTransaction())
            {
                    var del = db.Selected_Plants.Where(t => t.sel_id == id && t.type == 2).FirstOrDefault();
                    if (del != null)
                    {
                        db.Selected_Plants.Remove(del);
                    }
                    db.SaveChanges();
                    trans.Commit();
            }
            return 0;
        }

        //DeleteSectionTechcard
        public string DeleteSectionTechcard(string date)
        {
            var curDate = Convert.ToDateTime(date);
            using (var trans = db.Database.BeginTransaction())
            {
                var tcIds = db.TC_temp.Where(t => t.type_module == 2 && t.date == curDate).Select(t => t.id).ToList();
                for (int j = 0; j < tcIds.Count; j++)
                {
                    var curId = tcIds[j];
                    var tcData = db.TC_temp.Where(t => t.id == curId).FirstOrDefault();
                    var tcOperData = db.TC_operation_temp.Where(t => t.id_temp == curId).ToList();
                    var tcParamValueData = db.TC_param_value_temp.Where(t => t.id_temp == curId).ToList();
                    var tcServiceData = db.TC_to_service_temp.Where(t => t.id_temp == curId).ToList();

                    db.TC_temp.Remove(tcData);
                    db.TC_operation_temp.RemoveRange(tcOperData);
                    db.TC_param_value_temp.RemoveRange(tcParamValueData);
                    db.TC_to_service_temp.RemoveRange(tcServiceData);
                }
                db.SaveChanges();
                trans.Commit();
            }
            return "ok";
        }

        //срезы тех.карт AddSectionTechcard
        public Boolean AddSectionTechcard()
        {
            var curdate = DateTime.Now.Date;
            var checkDate = db.TC_temp.Where(t => t.date == curdate && t.type_module == 2).FirstOrDefault();
            if (checkDate != null) { throw new BllException(14, "За текущую дату тех. карты уже сохранены."); }

            //nzp
       //     var sevoborot = reportsModuleDb.getTotalNZP(curdate);

            var techIds = db.TC.Where(t => t.year == curdate.Year).Select(t => t.id).ToList();

            using (var trans = db.Database.BeginTransaction())

                {
                try
                {
                    //temp
                    var newId = (db.TC_temp.Max(t => (int?)t.id) ?? 0) + 1;
                    var newOperId = (db.TC_operation_temp.Max(t => (int?)t.id) ?? 0) + 1;
                    var newParamValId = (db.TC_param_value.Max(t => (int?)t.id) ?? 0) + 1;
                    var serviceId = (db.TC_to_service.Max(t => (int?)t.id) ?? 0) + 1;

                    //all Data
                    var tcAllData = db.TC.Where(t => t.year == curdate.Year).ToList();
                    var tcAllOperData = db.TC_operation.Where(t => t.TC.year == curdate.Year).ToList();
                    var tcAllParamValueData = db.TC_param_value.Where(t => t.TC.year == curdate.Year).ToList();
                    var tcAllServiceData = db.TC_to_service.Where(t => t.TC.year == curdate.Year).ToList();

                    var tcTempTable = new List<TC_temp>();
                    var tcOperTable = new List<TC_operation_temp>();
                    var tcParamValueTable = new List<TC_param_value_temp>();
                    var tcServiceTable = new List<TC_to_service_temp>();

            for (int i = 0; i < techIds.Count; i++)
            {
                //тех. карты
                var curId = techIds[i];

                var tcData = tcAllData.Where(t => t.id == curId).FirstOrDefault();
                var tcOperData = tcAllOperData.Where(t => t.id_tc == curId).ToList();
                var tcParamValueData = tcAllParamValueData.Where(t => t.id_tc == curId).ToList();
                var tcServiceData = tcAllServiceData.Where(t => t.id_tc == curId).ToList();

                //нзп
                //var totalNzpList = sevoborot.Where(t => t.plant_id == tcData.plant_plant_id).ToList();
                //decimal totalNzp = 0; float nzp_zp = 0; float nzp_fuel = 0; float nzp_seed = 0;
                //float nzp_szr = 0; float nzp_chemicalFertilizers = 0; float nzp_dop_material = 0;
                //for (int j = 0; j < totalNzpList.Count; j++)
                //{
                //    totalNzp = totalNzp + (decimal)sevoborot[j].total_salary + (decimal)sevoborot[j].consumption +
                //            (decimal)sevoborot[j].szr + (decimal)sevoborot[j].chemicalFertilizers + (decimal)sevoborot[j].dop_salary +
                //            (decimal)sevoborot[j].seed + (decimal)sevoborot[j].dopmaterial + (decimal)sevoborot[j].deduction;
                //   nzp_zp = nzp_zp  + (sevoborot[j].total_salary ?? 0) + (sevoborot[j].dop_salary ?? 0);    
                //   nzp_fuel = nzp_fuel + (sevoborot[j].consumption ?? 0);
                //   nzp_seed = nzp_seed  + (sevoborot[j].seed ?? 0);
                //   nzp_szr = nzp_szr + (sevoborot[j].szr ?? 0);
                //   nzp_chemicalFertilizers = nzp_chemicalFertilizers + (sevoborot[j].chemicalFertilizers ?? 0);
                //   nzp_dop_material = nzp_dop_material  + (sevoborot[j].dopmaterial ?? 0);
                //}
                    //1
                        var tcTemp = new TC_temp
                        {
                            id = newId,
                            tpsort_tpsort_id = tcData.tpsort_tpsort_id,
                            plant_plant_id = tcData.plant_plant_id,
                            date = curdate,
                            type_module = (int)TypeModule.SectionTechcard,
                        };
                        tcTempTable.Add(tcTemp);
                    //2 TC_operation_temp
                     for (int j = 0; j < tcOperData.Count; j++)
                     {
                       var  oldTcOperId = tcOperData[j].id;
                       var curTcParamList = tcParamValueData.Where(t => t.id_tc_operation == oldTcOperId).ToList();
                        var dic = new TC_operation_temp
                        {
                            id = newOperId,
                            id_temp = newId,
                            tptas_tptas_id = tcOperData[j].tptas_tptas_id,
                            multiplicity = tcOperData[j].multiplicity,
                            date_start = tcOperData[j].date_start,
                            date_finish = tcOperData[j].date_finish,
                            factor = tcOperData[j].factor,
                        };
                        tcOperTable.Add(dic);
                       //3 TC_param_value_temp
                        for (int l = 0; l < curTcParamList.Count; l++)
                        {
                            var dic1 = new TC_param_value_temp
                            {
                                id = newParamValId,
                                id_temp = newId,
                                id_tc_operation_temp = newOperId,
                                id_tc_param_temp = curTcParamList[l].id_tc_param,
                                value = (float)curTcParamList[l].value,
                                plan = curTcParamList[l].plan,
                                mat_mat_id = curTcParamList[l].mat_mat_id,
                            };
                            tcParamValueTable.Add(dic1);
                            newParamValId++;
                        }

                        newOperId++;
                     }

                     //3 TC_param_value_temp tc_operation=null
                     var curTcnullparam = tcParamValueData.Where(t => t.id_tc_operation == null).ToList();
                     for (int l = 0; l < curTcnullparam.Count; l++)
                     {
                         var dic1 = new TC_param_value_temp
                         {
                             id = newParamValId,
                             id_temp = newId,
                             id_tc_operation_temp = null,
                             id_tc_param_temp = curTcnullparam[l].id_tc_param,
                             value = (float?)curTcnullparam[l].value,
                             plan = curTcnullparam[l].plan,
                             mat_mat_id = curTcnullparam[l].mat_mat_id,
                         };
                         tcParamValueTable.Add(dic1);
                         newParamValId++;
                     }
                   ////nzp total
                   //   var nzp = new TC_param_value_temp
                   //  {
                   //      id = newParamValId,
                   //      id_temp = newId,
                   //      id_tc_operation_temp = null,
                   //      id_tc_param_temp = 100,  //total_nzp
                   //      value = (float)totalNzp,
                   //      plan = 1,
                   //      mat_mat_id = null,
                   //  };
                   //   db.TC_param_value_temp.Add(nzp);

                    //4
                for (int l = 0; l < tcServiceData.Count; l++)
                        {
                     var dis = new TC_to_service_temp
                     {
                         id = serviceId,
                         id_temp = newId,
                         id_service = tcServiceData[l].id_service,
                         cost = tcServiceData[l].cost,
                     };
                     tcServiceTable.Add(dis);
                    serviceId++;
                }

                     newId++;
                 }
                    //
            db.TC_temp.AddRange(tcTempTable);
            db.TC_operation_temp.AddRange(tcOperTable);
            db.TC_param_value_temp.AddRange(tcParamValueTable);
            db.TC_to_service_temp.AddRange(tcServiceTable);
                    //
              db.SaveChanges();
              trans.Commit();
                }
                catch (Exception)
                {
                    trans.Rollback();
                    throw;
                }
            }

          
            return true;
        }








    }
}
