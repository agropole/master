﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;
using LogusSrv.DAL.DBModel;
using LogusSrv.DAL.Entities.Res;
using LogusSrv.CORE.Exceptions;


namespace LogusSrv.DAL.Operations
{
    public class LoginDb
    {

        protected readonly LogusDBEntities db = new LogusDBEntities();
        public UserModel Login(string login, string password, string saltPostfix)
        {
            UserModel userInfo = new UserModel();
            var user = db.Users.Where(u => u.login.ToLower() == login.ToLower()).FirstOrDefault();
            if (user == null) return userInfo;

            string str12 = GenerateSaltedHash(password, saltPostfix);
            if (user.password != GenerateSaltedHash(password, saltPostfix)) return userInfo;

            userInfo.UserId = user.user_id;
            userInfo.EmpId = user.emp_emp_id;
            userInfo.UserLogin = login;
            userInfo.UserName = user.Employees.short_name; 
            userInfo.OrgId = user.Employees.org_org_id;
            userInfo.TypeOrg = user.Employees.Organizations.tporg_tporg_id;
            userInfo.RoleId = user.role_role_id;
            userInfo.Note = user.Employees.Posts.name;
            userInfo.PostId = user.Employees.Posts.pst_id;
            userInfo.OrgName = user.Employees.Organizations.name;
            userInfo.SubsystemsList = db.Roles_subsystems.Where(t => t.role_role_id == user.role_role_id).Select(t => t.subsystem_id).ToList();
            return userInfo;
        }

        //CheckKey
        public int CheckKey()
        {
            var isActivate = db.Parameters.AsEnumerable().Where(t => t.name.Equals("isActivate")).Select(t => t.value).FirstOrDefault();
            if (isActivate != null && isActivate.Equals("1")) {
                return 1;
            }
            return 2;
        }

        //проверка ключа
        public string ActivateSite(String req)
        {
            var arr = req.ToList(); decimal count = 0;
            for (int i = 0; i < arr.Count; i++) {

                try
                {
                    count = count + Convert.ToInt32(arr[i].ToString(), 16);
                }
                catch (System.FormatException e) { }
            
                }

                       if (count != 100)
                       {
                            throw new BllException(15, "Неверный регистрационный ключ.");
                       }
                       else {
                using (var trans = db.Database.BeginTransaction())
                {
                    var updDic = db.Parameters.AsEnumerable().Where(t => t.name.Equals("isActivate")).FirstOrDefault();

                    if (updDic == null)
                    {
                        db.Parameters.Add(new Parameters
                        {
                            name = "isActivate",
                            value = "1",

                        });
                        db.Parameters.Add(new Parameters
                        {
                            name = "activateKey",
                            value = req,

                        });
                    }
                    else
                    {
                        updDic.value = "1";
                    }
                    db.SaveChanges();
                    trans.Commit();

                }
          }
            return "Активация прошла успешно.";
        }


        public string GetHash(string pass, string salt)
        {
            return GenerateSaltedHash(pass, salt);
        }

        public static string GenerateSaltedHash(string password, string salt)
        {
            HashAlgorithm algorithm = new SHA512Managed();

            byte[] saltBytes = GetBytes(salt);
            byte[] passwordBytes = GetBytes(password);
            byte[] passwordWithSaltBytes = new byte[passwordBytes.Length + saltBytes.Length];

            for (int i = 0; i < passwordBytes.Length; i++)
                passwordWithSaltBytes[i] = passwordBytes[i];
            for (int i = 0; i < saltBytes.Length; i++)
                passwordWithSaltBytes[passwordBytes.Length + i] = saltBytes[i];
            byte[] hash = algorithm.ComputeHash(passwordWithSaltBytes);
            return Convert.ToBase64String(hash);

        }

        static byte[] GetBytes(string str)
        {
            byte[] bytes = new byte[str.Length * sizeof(char)];
            System.Buffer.BlockCopy(str.ToCharArray(), 0, bytes, 0, bytes.Length);
            return bytes;
        }
    }
}
