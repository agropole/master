﻿using System;
using System.Data.Entity;
using System.Collections.Generic;
using System.Linq;
using LogusSrv.CORE.Exceptions;
using LogusSrv.DAL.Entities.DTO.ActsSzr;
using LogusSrv.DAL.Entities.DTO;
using LogusSrv.DAL.Entities.Req;
using LogusSrv.DAL.DBModel;
using LogusSrv.DAL.Utils;
using LogusSrv.DAL.Enums;
using LogusSrv.DAL.Entities.Res;
using OfficeOpenXml.Style;
using OfficeOpenXml;
using System.Drawing;
using System.IO;
using System.Globalization;
using LogusSrv.DAL.Entities.DTO.DayTaskExt;


namespace LogusSrv.DAL.Operations
{

    public class SparePartsDb
    {
        protected readonly LogusDBEntities db = new LogusDBEntities();
        protected readonly TechCardDb tech = new TechCardDb();
        protected readonly ActsSrzDb tmcDb = new ActsSrzDb();
        public int? key;
        public SparePartsListResponse GetSparePartsDetails(GridDataReq req)
        {
            return GetSpareParts(req);
        }
        public SparePartsInvoiceListResponse GetInvoiceActsByDateDetails(GridDataReq req)
        {
            return GetInvoiceActsByDate(req);
        }
        //наряды
        public SparePartsRepairListResponse GetRepairsActsByDateDetails(GridDataReq req)
        {
            return GetRepairsActsByDate(req);
        }
        public SparePartsMovementListResponse GetSparePartsMovementDetails(GridDataReq req)
        {
            return GetMovementSpareParts(req);
        }
        //резерв
        public SparePartsReserveListResponse GetReserveListSpareParts(GridDataReq req)
        {
            return GetReserveSpareParts(req);
        }
        //списание
        public SparePartsWriteOffListResponse GetWriteOffListSpareParts(GridDataReq req)
        {
            return GetWriteOffSpareParts(req);
        }
        public DictionarySpareParts GetDictionarySpareParts()
        {
            var result = new DictionarySpareParts();

            result.StorageList = db.Storages.Where(p=>p.deleted == false).Select(p => new DictionaryItemsDTO { Id = p.store_id, Name = p.name }).ToList();
            result.ContractorsList = db.Contractor.Where(p => p.deleted == false).Select(p => new DictionaryItemsDTO { Id = p.id, Name = p.name }).ToList();
            result.SparePartsList = db.Spare_parts.Where(p => p.deleted == false).Select(p => new DictionaryItemsDTO
            {
                Id = p.spare_id,
                Name = p.name + " - " + db.Type_spare_parts.Where(b=>b.tsp_id == p.tptrak_tptrak_id).Select(b => b.name).FirstOrDefault()+" - "+
                db.Modules.Where(l => l.mod_id == p.mod_mod_id).Select(l => l.name).FirstOrDefault(),
                Description = p.vendor_code,
                Manufacturer = p.manufacturer}).ToList();
            result.VendorCodeList = db.Spare_parts.Where(p => p.deleted == false).Select(p => new DictionaryItemsDTO
            {
                Id = p.spare_id,
                Name = p.vendor_code,
                Description = p.name,
            Manufacturer = p.manufacturer}).ToList();
            result.ModuleList = db.Modules.Where(p => p.deleted == false).Select(p => new DictionaryItemsDTO { Id = p.mod_id, Name = p.name }).ToList();
            result.GroupTractorsList = db.Group_traktors.Where(p => p.deleted == false).Select(p => new DictionaryItemsDTO { Id = p.grtrak_id, Name = p.name }).ToList();
            return result;
        }
        //история запчасти
        public List<DictionaryItemsDTO> GetHistorySpareParts(string str)
        {
            var result = new List<DictionaryItemsDTO>();
            if (!String.IsNullOrEmpty(str))
            {
                result = db.Spare_parts.Where(p=>p.name.Contains(str)).Select(p => new DictionaryItemsDTO
                {
                    Id = p.spare_id,
                    Name = p.name,
                    Description = p.vendor_code,
                }).ToList();
            }
            return result;
        }
        //
        //наряд на ремонт
        public DictionaryRepairs GetDictionaryRepairs(SparePartsDetails req)
        {
            var year = DateTime.Now.Year;
            var result = new DictionaryRepairs();
            result.EmployeesList = db.Employees.Where(p => p.deleted == false && (p.pst_pst_id == 2 || p.pst_pst_id == 7))
                .Select(p => new DictionaryItemsDTO { Id = p.emp_id, Name = p.short_name }).ToList();
            var emptyTraktors = new DictionaryItemsTraktors { Id = -1, Name = "" };
            result.TraktorsList = db.Traktors.Where(p => p.deleted == false).Select(t => new DictionaryItemsTraktors
            {
                Name = t.name,
                Id = t.trakt_id,
                RegNum = t.reg_num,
                Name_fuel = new DictionaryItemsDTO
                {
                    Id = t.tpf_tpf_id,
                    Name = t.Type_fuel.name
                }
            }).OrderBy(e => e.Name).ToList();
            result.TraktorsShiftList = db.Traktors.Where(p => p.deleted == false).Select(t => new DictionaryItemsTraktors
            {
                Name = t.name + " (" + t.reg_num + ")",
                Id = t.trakt_id,
            }).OrderBy(e => e.Name).ToList();
            result.TraktorsList.Insert(0, emptyTraktors);

            result.InvoiceActs = db.Spare_parts_Acts.AsEnumerable().Where(d => d.act_number != null && d.date > DateTime.Now.AddDays(-365)).Select(d => new DictionaryItemsDTO
            {
                Id = d.spss_id,
                Name = "№ "+d.act_number+" от "+d.date.ToString("dd.MM.yyyy"),
            }).OrderByDescending(p => p.Id).ToList();

            result.TypeFuel = db.Type_fuel.Where(d => d.deleted != true && d.fuel_flag == true).Select(d => new DictionaryItemsDTO
            {
                Id = d.id,
                Name = d.name,
                Price = d.current_price
            }).OrderBy(p => p.Name).ToList();
            result.EquipmnetsList = db.Equipments.Where(e => e.deleted == false).
                                    Select(e => new DictionaryItemsTraktors
                                    {
                                        Name = String.IsNullOrEmpty(e.reg_num) ? e.name : e.name + " (" + e.reg_num + ")",
                                        Id = e.equi_id,
                                        RegNum = e.Type_equipments.name,
                                        Width = e.width
                                    }).OrderBy(e => e.Name).ToList();
            var emptyEquip = new DictionaryItemsTraktors { Id = -1, Name = "", RegNum = "", Width = 0 };
            result.EquipmnetsList.Insert(0, emptyEquip);
            var emptyModule = new DictionaryItemsDTO { Id = -1, Name = "" };
            result.ModuleList = db.Modules.Where(p => p.deleted != true).Select(p => new DictionaryItemsDTO { Id = p.mod_id, Name = p.name }).ToList();
            result.ModuleList.Insert(0, emptyModule);
            result.TypeTasksList = db.Type_tasks.Where(p => p.deleted != true && p.auxiliary_rate == 3).Select(p => new DictionaryItemsDTO { Id = p.tptas_id, Name = p.name }).ToList();
            result.TypeSpareList = db.Type_spare_parts.Where(p => p.deleted == false).Select(t => new DictionaryItemsDTO
            {
                Id = t.tsp_id,
                Name = t.name,
            }).OrderBy(e => e.Name).ToList();
            result.TypeSpareList.Insert(0, emptyModule);
            
            int temp;
            var numbers = db.Repairs_acts.Where(x => x.date_start.Year == req.repairStartDate.Value.Year && x.is_require != 1).Select(x => x.number_act).ToList();
            var numbers2 = db.Repairs_acts.Where(x => x.date_start.Year == req.repairStartDate.Value.Year && x.is_require == 1).Select(x =>new DictionaryItemsDTO {Name = x.number_act}).ToList();
            if (numbers.Count == 0) { result.NewActNumber = 1; }
        else
        {
         result.NewActNumber = numbers.Select(n => Int32.TryParse(n, out temp) ? temp : 0).Max() + 1;
        }
          if (numbers2.Count == 0) { result.NewRequireNumber = 1; }
         else
         {
          result.NewRequireNumber = numbers2.Max(x => Int32.Parse(x.Name.Split('/')[1])) + 1 ; 
         }
        return result;

        }
        //отмена списания
        public string ChangeStatusRepair(SparePartsDetails req)
        {
             using (var trans = db.Database.BeginTransaction())
            {
              var updateDic = db.Repairs_acts.Where(t => t.rep_id == req.ActId).FirstOrDefault();
              updateDic.status = 2;
              updateDic.date_end = null;
              var  updateDic2 = db.Spare_parts_Records.Where(t => t.spss_spss_id == req.ActId && t.store_from_id!=0 && t.store_to_id==0).ToList();
              db.Spare_parts_Records.RemoveRange(updateDic2);
              db.SaveChanges();
              trans.Commit();
            }
            return "success";
        }
    // данные с баланса
        public DictionarySpareParts GetDictionarySparePartsBalance()
        {
            var result = new DictionarySpareParts();
            //запчасти
            result.SparePartsList = db.Spare_parts.Where(p => p.deleted == false).Select(p => new DictionaryItemsDTO
            {
                Id = p.spare_id,
                Name = p.name + " - " + db.Group_traktors.Where(b => b.grtrak_id == p.tptrak_tptrak_id).Select(b => b.name).FirstOrDefault() + " - " +
                db.Modules.Where(l => l.mod_id == p.mod_mod_id).Select(l => l.name).FirstOrDefault(),
                Description = p.vendor_code,
                Manufacturer = p.manufacturer
            }).ToList();
            //артикул
            result.VendorCodeList = db.Spare_parts.Where(p => p.deleted == false).Select(p => new DictionaryItemsDTO
            {
                Id = p.spare_id,
                Name = p.vendor_code,
                Description = p.name,
                Manufacturer = p.manufacturer
            }).ToList();

            result.FullStorageList = db.Storages.Where(p => p.deleted == false).Select(p => new DictionaryItemsDTO
            {
                Id = p.store_id,
                Name = p.name,
            }).Distinct().ToList(); 

            var list = db.Spare_parts_Current.Select(l => new DictionaryItemsDTO { Id = l.spare_spare_id }).Distinct().ToList();
            for (int i = 0; i < result.SparePartsList.Count; i++)
            {
                var l = result.SparePartsList[i].Id;
                var t = list.Where(k => k.Id == l).FirstOrDefault();
                if (t == null) { result.SparePartsList.RemoveAt(i); if (i != 0) { i--; } }
            }
            for (int i = 0; i < result.VendorCodeList.Count; i++)
            {
                var l = result.VendorCodeList[i].Id;
                var t = list.Where(k => k.Id == l).FirstOrDefault();
                if (t == null) { result.VendorCodeList.RemoveAt(i); if (i != 0) { i--; } }
            }
            return result;
        }

        //список актов поступления
        public SparePartsInvoiceListResponse GetInvoiceActsByDate(GridDataReq req)
        {
            if (req.DateStart != null)
            {
                req.DateStart = req.DateStart.Value.AddDays(1);
            }
            var res = new List<SparePartsItem>();
            var resultList = new SparePartsInvoiceListResponse();
            res = (from t in db.Spare_parts_Acts
                   where (req.DateStart != null ? DbFunctions.TruncateTime(t.date) >=  DbFunctions.TruncateTime(req.DateStart) : true) &&
                   (req.DateEnd != null ? DbFunctions.TruncateTime(t.date) <= DbFunctions.TruncateTime(req.DateEnd) : true)
                   && ((req.ContractorsId != null && req.ContractorsId != -1) ? t.contr_id==req.ContractorsId :true)
                   select new SparePartsItem
                   {
                       Id = t.spss_id,
                       DateBegin1 = t.date,
                       Number = t.act_number,
                       Contractor = new DictionaryItemsDTO { Id = t.contr_id, Name = db.Contractor.Where(v => v.id == t.contr_id).Select(v => v.name).FirstOrDefault() },
                       Storage = new DictionaryItemsDTO { Id = t.store_store_id, Name = db.Storages.Where(v => v.store_id == t.store_store_id).Select(v => v.name).FirstOrDefault() },
                       Torg12 = t.torg_id,
                       Cost = (float?)Math.Round((double) (db.Spare_parts_Records.Where(v => v.spss_spss_id == t.spss_id).Select(v => v.price * v.count + (v.price * v.count * v.nds/100 )).Sum() ?? 0) , 2),
                   }).OrderByDescending(t => t.DateBegin1).ToList();
            for (int i = 0; i < res.Count; i++)
            {
                res[i].DateBegin = res[i].DateBegin1.ToString("dd.MM.yyyy");
            }

            if (req.CheckSearch != null && req.CheckSearch!="")
            {
                res = res.Where(t =>t.Number!=null && t.Number.Contains(req.CheckSearch)).ToList();
            }
            var CountItems = res.Count();
            var TotalPage = (double)CountItems / (double)req.PageSize;
            double CountPages = TotalPage < 0 ? 1 : Math.Ceiling(TotalPage);
            var gridData = GridHelpers<SparePartsItem>.GetGridData(res.AsQueryable(), req);
            resultList.Values = gridData.OrderByDescending(m => m.DateBegin1).ToList();
            resultList.CountPages = (int)CountPages;
            resultList.CountItems = CountItems;
            return resultList;
        }
        //список нарядов на ремонт
        public SparePartsRepairListResponse GetRepairsActsByDate(GridDataReq req)
        {
            req.repairStartDate = req.repairStartDate.Value.AddDays(1);
            var res = new List<RepairsItem>();
            var resultList = new SparePartsRepairListResponse();
            res = (from t in db.Repairs_acts
                   where (req.Torg12 == false && String.IsNullOrEmpty(req.Name) && String.IsNullOrEmpty(req.CheckSearch)) ? ((req.repairStartDate != null ? DbFunctions.TruncateTime(t.date_start) >= DbFunctions.TruncateTime(req.repairStartDate) : true)
                   && (req.repairEndDate != null ? DbFunctions.TruncateTime(t.date_start) <= DbFunctions.TruncateTime(req.repairEndDate) : true)) : true &&
                   (req.Torg12 == true && String.IsNullOrEmpty(req.Name) && String.IsNullOrEmpty(req.CheckSearch)) ? t.status != 3 : true

                   let EquipmentsName =
                   db.Equipments.Where(l => l.equi_id == t.equi_equi_id).Select(l => l.name).FirstOrDefault() != null ?
                   "-" + db.Equipments.Where(l => l.equi_id == t.equi_equi_id).Select(l => l.name).FirstOrDefault() : ""
                   select new RepairsItem
                   {
                       Id = t.rep_id,
                       Number = t.number_act,
                       DateBegin1 = t.date_start,
                       DateEnd1 = t.date_end,
                       CarsName = db.Traktors.Where(v => v.trakt_id == t.trakt_trakt_id).Select(v => v.name).FirstOrDefault() + EquipmentsName,
                       CarsNum = db.Traktors.Where(v => v.trakt_id == t.trakt_trakt_id).Select(v => v.reg_num).FirstOrDefault(),
                       Status = t.status == 1 ? "Создан" : t.status == 2 ? "Открыт" : "Закрыт",
                   }).OrderByDescending(m => m.DateBegin1).ToList();

            for (int i = 0; i < res.Count; i++)
            {
                res[i].DateBegin = res[i].DateBegin1.ToString("dd.MM.yyyy");
                if (!res[i].Status.Equals("Закрыт")) { res[i].DateEnd = ""; }
                else
                {
                    if (res[i].DateEnd1 != null) { res[i].DateEnd = res[i].DateEnd1.Value.ToString("dd.MM.yyyy"); }
                }
            }
            if (req.Name != null && req.Name != "")
            {
                res = res.Where(t => (t.CarsName != null && t.CarsName.ToLower().Contains(req.Name.ToLower())) || (t.CarsNum != null && t.CarsNum.ToLower().Contains(req.Name.ToLower()))).ToList();
            }
            if (req.CheckSearch != null && req.CheckSearch != "")
            {
                res = res.Where(t => t.Number != null && t.Number.ToLower().Contains(req.CheckSearch.ToLower())).ToList();
            }
            var CountItems = res.Count();
            var TotalPage = (double)CountItems / (double)req.PageSize;
            double CountPages = TotalPage < 0 ? 1 : Math.Ceiling(TotalPage);
            var gridData = GridHelpers<RepairsItem>.GetGridData(res.AsQueryable(), req);
            resultList.Values = gridData.OrderByDescending(m => m.DateBegin1).ToList();
            resultList.CountPages = (int)CountPages;
            resultList.CountItems = CountItems;
            return resultList;
        }

        //вывод - поступление - просмотр
        public SparePartsListResponse GetSpareParts(GridDataReq req)
        {
            List<SparePartsModel> list = new List<SparePartsModel>();
            var resultList = new SparePartsListResponse();
            list = (from s in db.Spare_parts_Records
                    where req.MatTypeId != 0 ? s.store_to_id == req.MatTypeId : true
                    join k1 in db.Spare_parts on s.spare_spare_id equals k1.spare_id into left1
                    from t1 in left1.DefaultIfEmpty()
                    join k2 in db.Storages on s.store_to_id equals k2.store_id into left2
                    from t2 in left2.DefaultIfEmpty()
                    join k3 in db.Spare_parts_Acts on s.spss_spss_id equals k3.spss_id into left3
                    from t3 in left3.DefaultIfEmpty()
                    where (s.store_to_id != 0 && s.store_to_id != null) && (s.store_from_id == 0 || s.store_from_id == null)
                    group new
                    {
                        DateBegin = t3.date
                    }
                        by new
                        {
                            Id = s.sps_id,
                            DateBegin = t3.date,
                            Price = s.price,
                            Count = s.count,
                            Cost = Math.Round(s.price * s.count + (float)s.nds, 2),
                            Act_number = s.act_number,
                            SpareParts = new DictionaryItemsDTO
                            {
                                Id = t1.spare_id,
                                Name = t1.name,
                            },
                            VendorCode = new DictionaryItemsDTO
                            {
                                Id = t1.spare_id,
                                Name = t1.vendor_code,
                            },
                            Storage = new DictionaryItemsDTO
                            {
                                Id = t2.store_id,
                                Name = t2.name,
                            },
                        } into g
                    select new SparePartsModel
                    {
                        Id = g.Key.Id,
                        DateBegin = g.Key.DateBegin,
                        SpareParts = g.Key.SpareParts,
                        Price = g.Key.Price,
                        Count = g.Key.Count,
                        Cost3 = g.Key.Cost,
                        Storage = g.Key.Storage,
                        VendorCode = g.Key.VendorCode,

                    }).OrderByDescending(m => m.DateBegin).ToList();
            for (int l = 0; l < list.Count; l++)
            {
                list[l].DateBegin1 = list[l].DateBegin.Value.ToString("dd.MM.yyyy");
            }

            var CountItems = list.Count();
            var TotalPage = (double)CountItems / (double)req.PageSize;
            double CountPages = TotalPage < 0 ? 1 : Math.Ceiling(TotalPage);

            var gridData = GridHelpers<SparePartsModel>.GetGridData(list.AsQueryable(), req);
            resultList.Values = gridData.OrderByDescending(m => m.DateBegin).ToList();
            resultList.CountPages = (int)CountPages;
            resultList.CountItems = CountItems;
            return resultList;
        }
        //история
        public HistorySpareParts GetHistorySpareList(int id)
        {
          HistorySpareParts res = new HistorySpareParts();
            //приход
            res.InvoiceList = (from s in db.Spare_parts_Records
                    join k1 in db.Spare_parts on s.spare_spare_id equals k1.spare_id into left1
                    from t1 in left1.DefaultIfEmpty()
                    where t1.spare_id == id
                    join k2 in db.Storages on s.store_to_id equals k2.store_id into left2
                    from t2 in left2.DefaultIfEmpty()
                    where (s.store_to_id != 0 && s.store_to_id != null) && (s.store_from_id == 0 || s.store_from_id == null)
                   join k3 in db.Spare_parts_Acts on s.spss_spss_id equals k3.spss_id into left3
                   from t3 in left3.DefaultIfEmpty()
                    group new
                    {
                        DateBegin = t3.date
                    }
                        by new
                        {
                            Id = s.sps_id,
                            DateBegin = t3.date,
                            Price = s.price,
                            Count = s.count,
                            Cost = Math.Round(s.price * s.count, 2),
                            Act_number = t3.act_number,
                            SpareParts = t1.name,
                            Storage = t2.name,
                            ActId = t3.spss_id,
                        } into g
                    select new SparePartsModel
                    {
                        Id = g.Key.Id,
                        DateBegin = g.Key.DateBegin,
                        SpareParts1 = g.Key.SpareParts,
                        Price = g.Key.Price,
                        Count = g.Key.Count,
                        Cost3 = g.Key.Cost,
                        Storage1 = g.Key.Storage,
                        Act_number = g.Key.Act_number,
                        ActId = g.Key.ActId,
                    }).OrderByDescending(m => m.DateBegin).ToList();
            foreach (var v in res.InvoiceList)
            {
                v.DateBegin1 = v.DateBegin.Value.ToString("dd.MM.yyyy");
            }
            //списание
            res.WriteOffList = (from s in db.Spare_parts_Records
                    join k1 in db.Spare_parts on s.spare_spare_id equals k1.spare_id into left1
                    from t1 in left1.DefaultIfEmpty()
                    where t1.spare_id == id
                    join k2 in db.Storages on s.store_from_id equals k2.store_id into left2
                    from t2 in left2.DefaultIfEmpty()
                    where (s.store_from_id != 0 && s.store_from_id != null) && (s.store_to_id == 0 || s.store_to_id == null)

                    join k3 in db.Repairs_acts on s.spss_spss_id equals k3.rep_id into left3
                    from t3 in left3.DefaultIfEmpty()
                    let nameEquip = t3.equi_equi_id != null ? (t3.trakt_trakt_id != null ? "-" : "") + db.Equipments.Where(p => p.equi_id == t3.equi_equi_id).Select(p => p.name).FirstOrDefault() : ""
                   let nameCars = t3.trakt_trakt_id != null ? db.Traktors.Where(l => l.trakt_id == t3.trakt_trakt_id).Select(l => l.name).FirstOrDefault() : ""
                    select new SparePartsWriteOffModel
                    {
                        Id = s.sps_id,
                        Date = t3.date_end,
                        ActId = t3.rep_id,
                        Act_number = t3.number_act,
                        Count = s.count,
                        SpareParts1 = t1.name,
                        CheckWrite = "Да",
                        CarsName = nameCars + nameEquip,
                        CarsNum = t3.trakt_trakt_id != null ? db.Traktors.Where(l => l.trakt_id == t3.trakt_trakt_id).Select(l => l.reg_num).FirstOrDefault() : null,
                    }).OrderByDescending(t => t.Date).ToList();
            //резерв
        var reserve = (from s in db.Repairs_Records
                    where s.count != 0
                    join k1 in db.Spare_parts on s.spare_spare_id equals k1.spare_id into left1
                    from t1 in left1.DefaultIfEmpty()
                    where t1.spare_id == id
                    join k2 in db.Storages on s.store_store_id equals k2.store_id into left2
                    from t2 in left2.DefaultIfEmpty()
                    join k3 in db.Repairs_acts on s.rep_rep_id equals k3.rep_id into left3
                    from t3 in left3.DefaultIfEmpty()
                    where t3.status != 3
                    let nameEquip = t3.equi_equi_id != null ? (t3.trakt_trakt_id != null ? "-" : "") + db.Equipments.Where(p => p.equi_id == t3.equi_equi_id).Select(p => p.name).FirstOrDefault() : ""
                    let nameCars = t3.trakt_trakt_id != null ? db.Traktors.Where(l => l.trakt_id == t3.trakt_trakt_id).Select(l => l.name).FirstOrDefault() : ""
                       select new SparePartsWriteOffModel
                    {
                        Id = s.repair_id,
                        Date = t3.date_start,
                        Act_number = t3.number_act,
                        ActId = t3.rep_id,
                        Count = s.count,
                        SpareParts1 = t1.name,
                        CheckWrite = "Нет",
                        CarsName = nameCars + nameEquip,
                        CarsNum = t3.trakt_trakt_id != null ? db.Traktors.Where(l => l.trakt_id == t3.trakt_trakt_id).Select(l => l.reg_num).FirstOrDefault() : null,
                    }).OrderByDescending(t => t.Date).ToList();
        res.WriteOffList.AddRange(reserve);
        foreach (var v in res.WriteOffList)
        {
            v.Date1 = v.Date.Value.ToString("dd.MM.yyyy");
        }
            return res;
        }

        // //вывод - резерв - просмотр
        public SparePartsReserveListResponse GetReserveSpareParts(GridDataReq req)
        {
            List<SparePartsReserveModel> list = new List<SparePartsReserveModel>();
            var resultList = new SparePartsReserveListResponse();
            list = (from s in db.Repairs_Records
                    where s.count!=0
                    where req.MatTypeId != 0 ? s.store_store_id == req.MatTypeId : true
                    join k1 in db.Spare_parts on s.spare_spare_id equals k1.spare_id into left1
                    from t1 in left1.DefaultIfEmpty()
                    join k2 in db.Storages on s.store_store_id equals k2.store_id into left2
                    from t2 in left2.DefaultIfEmpty()
                    join k3 in db.Repairs_acts on s.rep_rep_id equals k3.rep_id into left3
                    from t3 in left3.DefaultIfEmpty()
                    where t3.status!=3
                    let nameEquip = t3.equi_equi_id != null ? (t3.trakt_trakt_id != null ? "-" : "") + db.Equipments.Where(p => p.equi_id == t3.equi_equi_id).Select(p => p.name).FirstOrDefault() : ""
                    let nameCars = t3.trakt_trakt_id != null ? db.Traktors.Where(l => l.trakt_id == t3.trakt_trakt_id).Select(l => l.name).FirstOrDefault() : ""
                    select new SparePartsReserveModel
                    {
                        Id = s.repair_id,
                        Date = t3.date_start,
                        Act_number = t3.number_act,
                        Count = s.count,
                        SpareParts = new DictionaryItemsDTO
                        {
                            Id = t1.spare_id,
                            Name = t1.name,
                        },
                        VendorCode = new DictionaryItemsDTO
                        {
                            Id = t1.spare_id,
                            Name = t1.vendor_code,
                        },
                        CarsName = nameCars + nameEquip,
                       CarsNum = t3.trakt_trakt_id !=null ? db.Traktors.Where(l => l.trakt_id == t3.trakt_trakt_id).Select(l => l.reg_num).FirstOrDefault() : null,
                    }).OrderByDescending(t=>t.Date).ToList();
            var CountItems = list.Count();
            var TotalPage = (double)CountItems / (double)req.PageSize;
            double CountPages = TotalPage < 0 ? 1 : Math.Ceiling(TotalPage);

            var gridData = GridHelpers<SparePartsReserveModel>.GetGridData(list.AsQueryable(), req);
            resultList.Values = gridData.OrderByDescending(m => m.Date).ToList();
            resultList.CountPages = (int)CountPages;
            resultList.CountItems = CountItems;
            return resultList;
        }
        //просмотр списания 
        public SparePartsWriteOffListResponse GetWriteOffSpareParts(GridDataReq req)
        {
            List<SparePartsWriteOffModel> list = new List<SparePartsWriteOffModel>();
            var resultList = new SparePartsWriteOffListResponse();
            //новое
            list = (from s in db.Repairs_Records
                    where (req.MatTypeId != 0 ? s.store_store_id == req.MatTypeId : true) 
                    join k1 in db.Spare_parts on s.spare_spare_id equals k1.spare_id into left1
                    from t1 in left1.DefaultIfEmpty()
                    join k2 in db.Storages on s.store_store_id equals k2.store_id into left2
                    from t2 in left2.DefaultIfEmpty()
                    join k3 in db.Repairs_acts on s.rep_rep_id equals k3.rep_id into left3
                    from t3 in left3.DefaultIfEmpty()
                    where t3.status == 3
                    select new SparePartsWriteOffModel
                    {
                        Id = s.repair_id,
                        Date = s.date,
                        Act_number = t3.number_act,
                        Count = s.count,
                        Price = (float?)db.Spare_parts_Records.Where(w => w.spss_spss_id == s.rep_rep_id &&
                              w.spare_spare_id == s.spare_spare_id && w.store_from_id == s.store_store_id ).Select(w => w.price).FirstOrDefault(),
                        Cost = 0,
                        SpareParts = new DictionaryItemsDTO
                        {
                            Id = t1.spare_id,
                            Name = t1.name,
                        },
                        VendorCode = new DictionaryItemsDTO
                        {
                            Id = t1.spare_id,
                            Name = t1.vendor_code,
                        },
                        Storage = new DictionaryItemsDTO
                        {
                            Id = t2.store_id,
                            Name = t2.name,
                        },
                    }).OrderByDescending(t => t.Date).ToList();
            for (int l = 0; l < list.Count; l++)
            {
                list[l].Price = (float?)Math.Round((decimal)(list[l].Price ?? 0), 2);
                list[l].Cost = (float?)Math.Round((decimal)((list[l].Price ?? 0) * (list[l].Count ?? 0)), 2);
            }
            var CountItems = list.Count();
            var TotalPage = (double)CountItems / (double)req.PageSize;
            double CountPages = TotalPage < 0 ? 1 : Math.Ceiling(TotalPage);

            var gridData = GridHelpers<SparePartsWriteOffModel>.GetGridData(list.AsQueryable(), req);
            resultList.Values = gridData.OrderByDescending(m => m.Date).ToList();
            resultList.CountPages = (int)CountPages;
            resultList.CountItems = CountItems;
            return resultList;
        }

        public DateTime GetDailyBanDate()
        {
            var date = db.Parameters.Where(t => t.name == "DateDaily").Select(t => t.value).FirstOrDefault();
            var ndate = DateTime.Parse(date);
            return ndate;
        }

         public DateTime SetDailyBanDateAuto()
        {
            var date = DateTime.Now;
            using (var trans = db.Database.BeginTransaction())
            { 
                var ndate = new DateTime(date.Year, date.Month, 1);
                var dic = db.Parameters.Where(t => t.name == "DateDaily").FirstOrDefault();
                if (dic == null)
                {
                    var newDic = new Parameters
                    {
                        par_id = CommonFunction.GetNextId(db, "Parameters"),
                        name = "DateDaily",
                        value = ndate.ToString(),
                    };
                    db.Parameters.Add(newDic);

                }
                else
                {
                    dic.value = ndate.ToString();
                }
                db.SaveChanges();
                trans.Commit();
            }
            return date;
        }

        public DateTime SetDailyBanDate(DateTime date)
        {
            using (var trans = db.Database.BeginTransaction())
            {
                var dic = db.Parameters.Where(t => t.name == "DateDaily").FirstOrDefault();
                if (dic == null)
                {
                    var newDic = new Parameters
                                {
                                    par_id = CommonFunction.GetNextId(db, "Parameters"),
                                    name = "DateDaily",
                                    value = date.ToString(),
                                };
                    db.Parameters.Add(newDic);

                }
                else
                {
                    dic.value = date.ToString();
                }
                db.SaveChanges();
                trans.Commit();
            }
            return date;
        }


        public DictionarySpareParts GetCountGenaralWorks()
        {
            var year = DateTime.Now.Year;
            var result = new DictionarySpareParts();
            result.Agr = (from e in db.Equipments
                          where e.deleted == false
                          join r in db.Repairs_acts on e.equi_id equals r.equi_equi_id into leftTask1
                          from t1 in leftTask1.DefaultIfEmpty()
                          select new DictionaryItemsTraktors
                          {
                              Name = String.IsNullOrEmpty(e.reg_num) ? e.name : e.name + " (" + e.reg_num + ")",
                              Id = e.equi_id,
                              RegNum = e.reg_num
                          }).ToList();
            result.Employees = db.Employees.Where(p => p.deleted == false).Select(p => new DictionaryItemsDTO
            {
                Id = p.emp_id,
                Name = p.short_name
            }).ToList();
            result.TaskTypes = db.Type_tasks.Where(p => p.deleted != true && p.auxiliary_rate == 3).Select(p => new DictionaryItemsDTO
            {
                Id = p.tptas_id,
                Name = p.name
            }).ToList();
            result.TraktorsList = db.Traktors.Where(p => p.deleted != true).Select(p => new DictionaryItemsTraktors
            {
                Id = p.trakt_id,
                Name = p.name
            }).ToList();

            result.CountGeneralWorks = db.Daily_acts.Where(r => r.rep_id == -1 && r.date1.Year == year).Count() + 1;
            return result;
        }

        public List<DictionaryItemsDTO> GetRepairsActs(DateTime date_start, string typeoper)
        {
            var result = new List<DictionaryItemsDTO>();
            result = db.Repairs_acts.Where(r => r.status != 3 && r.is_require != 1 && DbFunctions.TruncateTime(r.date_start) <= DbFunctions.TruncateTime(date_start))
                .Select(p => new DictionaryItemsDTO
            {
                Id = p.rep_id,
                Name = db.Repairs_acts.Where(r => r.status != 3 && r.number_act.Equals(p.number_act)).Count() > 1 ? p.number_act + " (" + p.date_start.Year + ")" : p.number_act,
            }).ToList();
            if (typeoper != null && typeoper.Equals("create"))
            {
                var list = db.Daily_acts.Where(t => DbFunctions.TruncateTime(t.date1) == DbFunctions.TruncateTime(date_start)).Select(t => t.rep_id).Distinct().ToList();
                for (int i = 0; i < list.Count; i++)
                {
                    for (int j = 0; j < result.Count; j++)
                    {
                        if (list[i] == result[j].Id)
                        {
                            result.RemoveAt(j); j--;
                        }
                    }
                }
            }
                return result;
        }

        //LoadRepairs
        public string LoadRepairs(GetDayTasksReq req)
        {
            using (var trans = db.Database.BeginTransaction())
            {
                var day = db.Day_repairs.AsEnumerable().Where(d => d.dr_date.Date == req.DateFilter.Value.Date
                    ).FirstOrDefault();
                if (day == null)
                {
                    day = new Day_repairs();
                    day.dr_id = CommonFunction.GetNextId(db, "Day_repairs");
                    day.dr_date = req.DateFilter.Value;
                    day.dr_type = 1;
                    db.Day_repairs.Add(day);
                    db.SaveChanges();
                    day = db.Day_repairs.AsEnumerable().Where(d => d.dr_date.Date == req.DateFilter.Value.Date
                    ).FirstOrDefault();
                }

                var id = db.Day_repairs.AsEnumerable().Where(t => t.dr_date.Date == req.DateFilter.Value.Date).Select(t => t.dr_id).FirstOrDefault();
                var repDayList = db.Day_repairs_detail.Where(t => t.dr_dr_id == id).ToList();
                    db.Day_repairs_detail.RemoveRange(repDayList);

                var openActs = db.Repairs_acts.AsEnumerable().Where(t => t.status != 3  && t.date_start.Date.Year == req.DateFilter.Value.Date.Year).ToList();

                for (int i = 0; i < openActs.Count; i++) {

                    var dic = new Day_repairs_detail
                    {
                        dr_dr_id = day.dr_id,
                        equi_equi_id = openActs[i].equi_equi_id,
                        trak_trak_id = openActs[i].trakt_trakt_id,
                        otv_emp_id = openActs[i].emp_emp_id,
                        number = openActs[i].number_act,
                    };
                    db.Day_repairs_detail.Add(dic);
                };

                db.SaveChanges();
                trans.Commit();
            }
            
            return "ok";
        }


        public DayTasksModels GetDayRepairs(GetDayTasksReq req)
        {
            DayTasksModels taskList = new DayTasksModels();
            taskList.Shift1 = new List<DayTaskDTO>();
            taskList.FreeEmp = new List<DictionaryItemsDTO>();
            if (req == null) req = new GetDayTasksReq();
            if (req.DateFilter == null) req.DateFilter = DateTime.Now.Date;
            req.DateFilter = req.DateFilter.Value.Date;

            var checkType = db.Organizations.Where(t => t.org_id == req.OrgId).Select(t => t.tporg_tporg_id).FirstOrDefault();

            var listResult = (from dt in db.Day_repairs
                              join dtd in db.Day_repairs_detail on dt.dr_id equals dtd.dr_dr_id
                              where dt.dr_date == req.DateFilter &&
                              (req.FilterOrg != -1 ? dt.org_org_id == req.FilterOrg : true)
                              select new DayTaskDTO
                              {
                                  TypeTaskInfo = new TypeTaskInfoDTO
                                  {
                                      Id = dtd.tptas_tptas_id,
                                      Name = db.Type_tasks.Where(t => t.tptas_id == dtd.tptas_tptas_id).Select(t=>t.name).FirstOrDefault(),
                                  },
                                  EquipmentsInfo = new GetEquipInfoDTO
                                  {
                                      TracktorInfo = new ElemntInfoDTO
                                      {
                                          Id = dtd.trak_trak_id,
                                          Name = db.Traktors.Where(t => t.trakt_id == dtd.trak_trak_id).
                                          Select(t => t.name + "(" + t.reg_num + ")" ).FirstOrDefault(),
                                      },
                                      EquipInfo = new ElemntInfoDTO
                                      {
                                          Id = dtd.equi_equi_id,
                                          Name = db.Equipments.Where(t => t.equi_id == dtd.equi_equi_id).Select(t => t.name).FirstOrDefault(),
                                      },
                                  },
                                  AvailableInfo = new AvailableInfoDTO
                                  {
                                      Id = dtd.otv_emp_id,
                                      Name = db.Employees.Where(t => t.emp_id == dtd.otv_emp_id).Select(t => t.short_name).FirstOrDefault(),
                                  },
                                  DetailId = dtd.drd_id,
                                  Status = dtd.status,
                                  Comment = !string.IsNullOrEmpty(dtd.comment) ? dtd.comment : null,
                                  Number = dtd.number,  
                                  
                              }).ToList();

                          for (int i = 0; i < listResult.Count; i++) {

                              var dai_id = db.Daily_acts.AsEnumerable().Where(t => t.number_dai_act.Split('/')[0].Equals(listResult[i].Number) &&
                                 t.date1.Date == req.DateFilter.Value.Date).Select(t => t.dai_id).FirstOrDefault();
                              if (dai_id != 0)   {
                                  listResult[i].NumDailyId = db.Daily_acts.Where(t => t.dai_id == dai_id).Select(t => t.number_dai_act).FirstOrDefault();
                                  listResult[i].EmpList = new List<DictionaryItemsDTO>();
                                  var empIds = db.Repairs_Tasks_Spendings.Where(t => t.dai_dai_id == dai_id).Select(t => t.emp_emp_id).ToList();
                                  var tptasId = db.Repairs_Tasks_Spendings.Where(t => t.dai_dai_id == dai_id).Select(t => t.tptas_tptas_id).FirstOrDefault();
                                  var hours = db.Repairs_Tasks_Spendings.Where(t => t.dai_dai_id == dai_id).Sum(t => t.hours);
                                  var sum = db.Repairs_Tasks_Spendings.Where(t => t.dai_dai_id == dai_id).Sum(t => ((t.rate_piecework ?? 0) * (t.hours ?? 0) + (t.rate_shift ?? 0)));

                                  listResult[i].Hours = hours;
                                  listResult[i].Summa = sum;
                                  for (int j = 0; j < empIds.Count; j++)
                                  {
                                      var id = empIds[j];
                                      var name = db.Employees.Where(t => t.emp_id == id).Select(t => t.short_name).FirstOrDefault();
                                      listResult[i].EmpList.Add(new DictionaryItemsDTO { Id = empIds[j], Name = name });
                                  }
                                 var nameTas = db.Type_tasks.Where(t => t.tptas_id == tptasId).Select(t => t.name).FirstOrDefault();
                                 listResult[i].TypeTaskInfo = (new TypeTaskInfoDTO { Id = tptasId, Name = nameTas });
                              }
                             }
                          taskList.Comment = db.Day_repairs.AsEnumerable().Where(t => t.dr_date.Date == 
                              req.DateFilter.Value.Date).Select(t => t.discription).FirstOrDefault();

               var busyEmp = db.Repairs_Tasks_Spendings.AsEnumerable().Where(t => t.date1.Value.Date == req.DateFilter).Select(t => t.emp_emp_id).ToList();
               var freeEmp = db.Employees.Where(p => p.deleted == false && (p.pst_pst_id == 2 || p.pst_pst_id == 7) && !busyEmp.Contains(p.emp_id))
                 .Select(p => new DictionaryItemsDTO { Id = p.emp_id, Name = p.short_name, Description = p.Posts.name }).ToList();

                taskList.Shift1 = listResult;
                taskList.FreeEmp = freeEmp;
                taskList.DayTaskId = db.Day_repairs.AsEnumerable().Where(t => t.dr_date.Date == req.DateFilter).Select(t => t.dr_id).FirstOrDefault();
                return taskList;

                }


        public DictionarySpareParts GetDictionaryDailyInfo(int id_act, int? dail_id, DateTime date_start, string typeoper)
        {
            var result = new DictionarySpareParts();
            result.RepairsActs = GetRepairsActs(date_start, typeoper);
            result.VendorCodeList = db.Spare_parts.Where(p => p.deleted == false).Select(p => new DictionaryItemsDTO
            {
                Id = p.spare_id,
                Name = p.vendor_code,
                Description = p.name,
                Manufacturer = p.manufacturer
            }).ToList();
            result.Employees = db.Employees.Where(p => p.deleted == false).Select(p => new DictionaryItemsDTO
            {
                Id = p.emp_id,
                Name = p.short_name
            }).ToList();
            result.TaskTypes = db.Type_tasks.Where(p => p.deleted != true && p.auxiliary_rate == 3).Select(p => new DictionaryItemsDTO
            {
                Id = p.tptas_id,
                Name = p.name
            }).ToList();
            result.TraktorsList = (from t in db.Traktors
                                   where t.deleted == false
                                   join r in db.Repairs_acts on t.trakt_id equals r.trakt_trakt_id into leftTask1
                                   from t1 in leftTask1.DefaultIfEmpty()
                                   where t1.rep_id == id_act
                                   select new DictionaryItemsTraktors
                                   {
                                       Name = t.name,
                                       Id = t.trakt_id,
                                       RegNum = t.reg_num
                                   }).ToList();

            result.Agr = (from e in db.Equipments
                          where e.deleted == false
                          join r in db.Repairs_acts on e.equi_id equals r.equi_equi_id into leftTask1
                          from t1 in leftTask1.DefaultIfEmpty()
                          where t1.rep_id == id_act
                          select new DictionaryItemsTraktors
                          {
                              Name = String.IsNullOrEmpty(e.reg_num) ? e.name : e.name + " (" + e.reg_num + ")",
                              Id = e.equi_id,
                              RegNum = e.reg_num
                          }).ToList();
            var nums = db.Daily_acts.Where(v => v.dai_id != dail_id && v.rep_id == id_act).Select(v => v.number_dai_act).ToList();
            var max = 0;
           
              //  if (dail_id == null)
              //  {
                    for (int i = 0; i < nums.Count; i++)
                    {
                        if (nums.Count == 1) { max = Int32.Parse(nums[i].Split('/')[1]); }
                        if (i != 0)
                        {
                            max = Int32.Parse(nums[i].Split('/')[1]) > Int32.Parse(nums[i - 1].Split('/')[1]) ? Int32.Parse(nums[i].Split('/')[1]) :
                                Int32.Parse(nums[i - 1].Split('/')[1]);
                        }
                    }
           //     }

                var check = db.Daily_acts.Where(t => t.dai_id == dail_id).Select(t => t.rep_id).FirstOrDefault();
                if (check != id_act)
                {
                result.NewDailyActs = db.Repairs_acts.Where(l => l.rep_id == id_act).Select(l => l.number_act).FirstOrDefault() + "/" +
                       (max + 1).ToString();
                }
                else {
              result.NewDailyActs = db.Daily_acts.Where(t => t.dai_id == dail_id).Select(t => t.number_dai_act).FirstOrDefault();
                }
            return result;
        }


        public string DeleteDayRep(List<int?> req)
        {
            using (var trans = db.Database.BeginTransaction())
            {
                var notDelet = new List<int>();
                foreach (var item in req)
                {
                    var detailTask = db.Day_repairs_detail.Where(d => d.drd_id == item).FirstOrDefault();
                    if (detailTask != null)
                    {
                        db.Day_repairs_detail.Remove(detailTask);
                    }
                }
                db.SaveChanges();
                trans.Commit();
                return "ok";
            }
        }


        //Метод получения цены при выборе запчасти
        public SparePartsModel GetSparePartCost(SparePartsModel req)
        {
            //данные о ценах в Spare_Parts_Current или Spare_parts_Records
            //брать из другой таблицы
            var list = (from s in db.Spare_parts_Records
                        join k1 in db.Spare_parts on s.spare_spare_id equals k1.spare_id into left1
                        from t1 in left1.DefaultIfEmpty()
                        join k2 in db.Storages on s.store_from_id equals k2.store_id into left2
                        from t2 in left2.DefaultIfEmpty()
                        where s.spare_spare_id == req.Id
                        select new SparePartsModel
                        {
                            Id = s.spare_spare_id,
                            Price = (float?)s.price,
                            Count = s.count
                        }).ToList();
            if (list.Count >= 1)
                return list[0];
            else
                return null;
        }


        public RateResModel GetTaskRateSpareParts(RateResModel req)
        {
            return tech.GetTechRate(req.TptasId, null, req.TraktId, req.EquiId, null);
        }

        public DailyRepairsModel GetDailyRepairsItem(EmpTaskInfo id)
        {
         //   var year = DateTime.Now.Year;
            DailyRepairsModel result = new DailyRepairsModel();
            GeneralInfoDaily gen = db.Daily_acts.Where(d => d.dai_id == id.Id).Select(p => new GeneralInfoDaily
            {
                Id = p.dai_id,
                Rep_id = p.rep_id,
                DailyNum = p.number_dai_act,
                RepairsActs = p.rep_id,
                Date = p.date1,
                timeStart = p.time_start,
                timeEnd = p.time_end,
         //       Agr = p.rep_id == -1 ? p.equi_equi_id : (p.equi_equi_id != null ? p.equi_equi_id : null),
                Agr = p.equi_equi_id,
               GeneralWorks = p.general_works==1 ? true : false,
           //    CountGeneralWorks = db.Daily_acts.Where(r=>r.rep_id == -1 && r.date1.Year == year).Count() ,
            }).FirstOrDefault();
            result.Main = gen;
            if (result.Main.Rep_id != -1) {
                var trakt = db.Repairs_acts.Where(t=>t.rep_id == result.Main.Rep_id).Select(t=>t.trakt_trakt_id).FirstOrDefault();
                var equip = db.Repairs_acts.Where(t => t.rep_id == result.Main.Rep_id).Select(t => t.equi_equi_id).FirstOrDefault();
                result.Main.TraktorId = trakt;
                result.Main.Agr = equip;
            }

            List<TableDataDaily> tab = db.Repairs_Tasks_Spendings.Where(s => s.dai_dai_id == id.Id).Select(s => new TableDataDaily
            {
                Id = s.sp_id,
                Date1 = s.date1.Value,
                Employees = new EmpTaskInfo
                {
                    Id = s.emp_emp_id.Value,
                    Name = db.Employees.Where(e => e.emp_id == s.emp_emp_id).Select(r => r.short_name).FirstOrDefault()
                },
                Tasks = new EmpTaskInfo
                {
                    Id = s.tptas_tptas_id.Value,
                    Name = db.Type_tasks.Where(t => t.tptas_id == s.tptas_tptas_id).Select(r => r.name).FirstOrDefault()
                },
                RateShift = s.rate_shift,
                RatePiecework = s.rate_piecework,
                Hours = s.hours
            }).ToList();
            result.Spendings = tab;

            int i = 1;
            foreach (TableDataDaily t in result.Spendings)
            {
                t.Number1 = i++;
                t.SumCost = (t.RatePiecework ?? 0) * (t.Hours ?? 0) + (t.RateShift ?? 0);
            }
         
            return result;
        }

        public List<DailyRepairsModelList> GetDailyRepairsList(GridDataReq req)
        {
            var res = new List<DailyRepairsModelList>();
              var res3 = new List<DailyRepairsModelList>();
            res = (from t in db.Daily_acts
                   join r in db.Repairs_acts on t.rep_id equals r.rep_id into left1
                   from t1 in left1.DefaultIfEmpty()
                   join tr in db.Traktors on t1.trakt_trakt_id equals tr.trakt_id into left2
                   from t2 in left2.DefaultIfEmpty()
                   join ag in db.Equipments on t1.equi_equi_id equals ag.equi_id into left3
                   from t3 in left3.DefaultIfEmpty()
                   join ag2 in db.Equipments on t.equi_equi_id equals ag2.equi_id into left4
                   from t4 in left4.DefaultIfEmpty()
                   where (String.IsNullOrEmpty(req.Name) && String.IsNullOrEmpty(req.CheckSearch)) ? (
                   DbFunctions.TruncateTime(t.date1) >= DbFunctions.TruncateTime(req.DateStart) &&
                   DbFunctions.TruncateTime(t.date1) <= DbFunctions.TruncateTime(req.DateEnd)) : 
                   
                   (!String.IsNullOrEmpty(req.Name) && String.IsNullOrEmpty(req.CheckSearch)) ?
                   (t2.name.ToLower().Contains(req.Name.ToLower()) || t2.reg_num.ToLower().Contains(req.Name.ToLower()) || t2.reg_num.ToLower().Contains(req.Name.ToLower())
                   || t3.name.ToLower().Contains(req.Name.ToLower()) || t4.name.ToLower().Contains(req.Name.ToLower())) :
                   (t.number_dai_act.ToLower().Contains(req.CheckSearch.ToLower()))

                   select new DailyRepairsModelList
                   {
                       Id = t.dai_id,
                       Date1 = t.date1,
                       Number = t.number_dai_act,
                       CarsName = t2.name,
                       CarsNum = t2.reg_num,
                       Agr = t.rep_id == -1 ? db.Equipments.Where(g=>g.equi_id==t.equi_equi_id).Select(g=>g.name).FirstOrDefault() :t3.name,
                       Status = (t.status == 1) ? "Открыт" : "Закрыт"
                   }).ToList();
            for (int i = 0; i < res.Count; i++)
            {
                res[i].Date = res[i].Date1.ToString("dd.MM.yyyy");
            }

        //    var df = Int32.Parse("М/582/1".Split('/')[0]);
        //    var re33 = res.Where(t => t.Number.Split('/')[0] != "ОХР").ToList();

            var res1 = res.Where(t => t.Number.Split('/')[0] != "ОХР").OrderBy(t => Int32.Parse(t.Number.Split('/')[0])).ThenBy(t => Int32.Parse(t.Number.Split('/')[1])).ToList();
          var  res2 = res.Where(t => t.Number.Split('/')[0] == "ОХР").OrderBy(t => Int32.Parse(t.Number.Split('/')[1])).ToList();
          res3.AddRange(res1); res3.AddRange(res2);
            return res3;
        }

        //ежедневный из сменного задания
        public string CreateShiftDaily(UpdateDayTaskReq req)
        {
            var data = new DailyRepairsModel();
            data.Main = new GeneralInfoDaily();
            data.Spendings = new List<TableDataDaily>();
            var dailyAct = "";
            var acts = db.Daily_acts.AsEnumerable().Where(t => t.date1.Year == req.Date.Year && 
                t.number_dai_act.Split('/')[0].Equals(req.ShiftInfo.Number)).Select(t => Int32.Parse(t.number_dai_act.Split('/')[1]) ).ToList();

            var rep_id = db.Repairs_acts.AsEnumerable().Where(t => t.date_start.Year == req.Date.Year && t.rep_id != -1
                && t.number_act.Equals(req.ShiftInfo.Number)).Select(t => t.rep_id).FirstOrDefault();

            if (acts.Count == 0)
            {
                dailyAct = req.ShiftInfo.Number + "/1";
            }
            else {
                dailyAct = req.ShiftInfo.Number + "/" + (acts.Max() + 1);
            }

            data.Main.Date = req.Date;
            data.Main.DailyNum = dailyAct;
            data.Main.RepairsActs = rep_id;
            data.Main.timeStart = "08:00";
            data.Main.timeEnd = "17:00";

            for (int i = 0; i < req.ShiftInfo.EmpList.Count; i++) {


              var empDic = new EmpTaskInfo {Id = req.ShiftInfo.EmpList[i].Id  };
              var tasDic = new EmpTaskInfo {Id = req.ShiftInfo.TypeTaskInfo.Id  };

                data.Spendings.Add(new TableDataDaily {
                 Employees = empDic,
                 Tasks = tasDic,
                });

                   }
                CreateUpdateDailyRepairsAct(data);
                return dailyAct;
        }

        public string UpdateCommentRepTask(GetDayTasksReq req)
        {
            //var checkType = db.Organizations.Where(t => t.org_id == req.OrgId).Select(t => t.tporg_tporg_id).FirstOrDefault();
            //int? org_id = null;
            //if (checkType == (int)TypeOrganization.Section) { org_id = req.OrgId; }
            using (var trans = db.Database.BeginTransaction())
            {
                var day = db.Day_repairs.Where(d => d.dr_date == req.DateFilter
               //     && (checkType == (int)TypeOrganization.Company ? d.org_org_id == null : d.org_org_id == req.OrgId)
                    ).FirstOrDefault();
                if (day == null)
                {
                    day = new Day_repairs();
                    day.dr_id = CommonFunction.GetNextId(db, "Day_repairs");
                    day.dr_date = req.DateFilter.Value;
                    day.discription = req.Comment;
                    day.dr_type = 1;
                 //   day.org_org_id = org_id;
                    db.Day_repairs.Add(day);
                }
                day.discription = req.Comment;
                db.SaveChanges();
                trans.Commit();
                return "ok";
            }
        }




        public int CreateShiftRep(UpdateDayTaskReq req)
        {
            var checkType = db.Organizations.Where(t => t.org_id == req.OrgId).Select(t => t.tporg_tporg_id).FirstOrDefault();
                int? org_id = null;
                if (checkType == (int)TypeOrganization.Section) { org_id = req.OrgId; }
                var newDrdId = 0; var newDrId = 0;
                var equi_equi_id = req.ShiftInfo.EquipmentsInfo.EquipInfo != null ? req.ShiftInfo.EquipmentsInfo.EquipInfo.Id : null;
                var trak_trak_id = req.ShiftInfo.EquipmentsInfo.TracktorInfo != null ? req.ShiftInfo.EquipmentsInfo.TracktorInfo.Id : null;
              using (var trans = db.Database.BeginTransaction())
            {

                var checkDate = db.Day_repairs.Where(t => t.dr_date == req.Date).FirstOrDefault();
                newDrdId = (db.Day_repairs_detail.Max(t => (int?)t.drd_id) ?? 0) + 1;
                if (checkDate == null)
                {
                    newDrId = (db.Day_repairs.Max(t => (int?)t.dr_id) ?? 0) + 1;
                    var newDic = new Day_repairs
                    {
                        dr_id = newDrId,
                        dr_date = req.Date,
                        org_org_id = org_id,
                    };
                    db.Day_repairs.Add(newDic);
                }
                else {
                    newDrId = checkDate.dr_id;
                }

                db.Day_repairs_detail.Add(new Day_repairs_detail
                {
                    drd_id = newDrdId,
                    dr_dr_id = newDrId,
                    comment = req.ShiftInfo.Comment,
                    otv_emp_id = req.ShiftInfo.AvailableInfo.Id,
                    equi_equi_id = equi_equi_id,
                    trak_trak_id = trak_trak_id,
                    number = req.ShiftInfo.Number,
                });
                db.SaveChanges();
                trans.Commit();
            }

              //создание НН
              var data = new CreateUpdateRepairs();
              data.GeneralInfo = new GeneralInfo();
              data.TableInfo1 = new List<TableInfo1>();
              data.TableInfo2 = new List<TableInfo2>();
              data.TableInfo3 = new List<TableInfo3>();
              data.TableInfo4 = new List<TableInfo4>();
              data.TableInfo5 = new List<TableInfo5>();
              data.GeneralInfo.dateStart = req.Date;
              data.GeneralInfo.RepairNumber = req.ShiftInfo.Number;
              data.GeneralInfo.EmpId = req.ShiftInfo.AvailableInfo.Id;
              data.GeneralInfo.AgroreqId = equi_equi_id;
              data.GeneralInfo.TraktorId = trak_trak_id;
              data.GeneralInfo.Comment = req.ShiftInfo.Comment;
              data.GeneralInfo.Plan1 = true;
              CreateUpdateRepairsDetail(data);

              return newDrdId;
        }


        public DateTime CreateUpdateDailyRepairsAct(DailyRepairsModel req)
        {
            var banDate = db.Parameters.Where(t => t.name == "DateDaily").Select(t => t.value).FirstOrDefault();
            if (banDate != null && DateTime.Parse(banDate).Date > req.Main.Date.Date) {
                throw new BllException(15, "Сохранение невозможно. Обратитесь к администратору");
            } 

            DateTime nDate = DateTime.Now;
            var year = nDate.Year;
            using (var trans = db.Database.BeginTransaction())
            {

                    if (req.Main.Id == null)
                    {
                        var str = req.Main.DailyNum.Split('/')[0].ToString();

                        var DaysActs = db.Daily_acts.Where(d => DbFunctions.TruncateTime(d.date1) == DbFunctions.TruncateTime(req.Main.Date) && d.rep_id != -1)
                            .Select(t => new GeneralInfoDaily { DailyNum = t.number_dai_act, Date = t.date1, Id = t.rep_id }).ToList(); //акты за день
                        int? isDayActs = null;
                        for (int i = 0; i < DaysActs.Count; i++) {
                            if (DaysActs[i].DailyNum.Split('/')[0].ToString().Contains(str) && DaysActs[i].Date.Date == req.Main.Date.Date &&
                                req.Main.RepairsActs == DaysActs[i].Id)
                            {
                                isDayActs = 1; break;
                            }
                        }

                        if (req.Main.RepairsActs!=-1 && isDayActs != null)
                        {
                            throw new BllException(15, "По выбранной дате уже существует Ежедневный наряд");
                        }
                            db.Daily_acts.Add(new Daily_acts
                            {
                                number_dai_act = req.Main.DailyNum,
                                rep_id = req.Main.RepairsActs,
                                date1 = req.Main.Date,
                                time_start = req.Main.timeStart,
                                time_end = req.Main.timeEnd,
                                status = 1,
                                general_works = req.Main.GeneralWorks == true ? 1 : 0,
                                equi_equi_id = req.Main.RepairsActs == -1 ? req.Main.Agr : null,  
                            });
                            db.SaveChanges();
                            //меняем статус НН на Открыт и дату открытия
                            if (req.Main.RepairsActs != -1)
                            {
                                var upd = db.Repairs_acts.Where(t => t.rep_id == req.Main.RepairsActs).FirstOrDefault();
                                var smallestDates = (from d in db.Daily_acts where d.rep_id == req.Main.RepairsActs select d.date1).ToList();
                                if (smallestDates.Count != 0)
                                {
                                    upd.date_open = smallestDates.Min();
                                    nDate = smallestDates.Min();
                                }
                                else
                                {
                                    upd.date_open = req.Main.Date;
                                    nDate = req.Main.Date;
                                }
                                upd.status = 2;
                            }
                            db.SaveChanges();

                            int id = db.Daily_acts.Max(t => t.dai_id);

                            foreach (TableDataDaily a in req.Spendings)
                            {
                                db.Repairs_Tasks_Spendings.Add(new Repairs_Tasks_Spendings
                                {
                                    dai_dai_id = id,
                                    date1 = req.Main.Date,
                                    emp_emp_id = (a.Employees != null) ? a.Employees.Id : 0,
                                    tptas_tptas_id = (a.Tasks != null) ? a.Tasks.Id : 0,
                                    rate_shift = a.RateShift,
                                    rate_piecework = a.RatePiecework,
                                    hours = a.Hours
                                });
                            }
                    }
                    else
                    {
                        var chechStatus = db.Daily_acts.Where(d => d.dai_id == req.Main.Id).FirstOrDefault();

                        if (req.Main.TypeOper.Equals("close") || (req.Main.TypeOper.Equals("edit") && chechStatus.status == 2))
                        {
                            for (int i = 0; i < req.Spendings.Count; i++)
                            {
                                if (req.Spendings[i].Employees == null || req.Spendings[i].Employees.Id == null || req.Spendings[i].Tasks == null || req.Spendings[i].Tasks.Id == null
                                || req.Spendings[i].SumCost == 0 || req.Spendings[i].SumCost == null || req.Spendings[i].Hours == 0 || req.Spendings[i].Hours == null)
                                {
                                    throw new BllException(15, "Заполните данные по ежедневному наряду");
                                }
                            }
                        }

                        var str = req.Main.DailyNum.Split('/')[0].ToString();

                        var DaysActs = db.Daily_acts.Where(d => DbFunctions.TruncateTime(d.date1) == DbFunctions.TruncateTime(req.Main.Date) && d.dai_id != req.Main.Id)
                           .Select(t => new GeneralInfoDaily { DailyNum = t.number_dai_act, Date = t.date1, Id = t.rep_id }).ToList(); //акты за день
                        int? isDayActs = null;
                        for (int i = 0; i < DaysActs.Count; i++)
                        {
                            if (DaysActs[i].DailyNum.Split('/')[0].ToString().Contains(str) && DaysActs[i].Date.Date == req.Main.Date.Date && req.Main.RepairsActs == DaysActs[i].Id)
                            {
                                isDayActs = 1; break;
                            }
                        }

                        if (req.Main.RepairsActs != -1 && isDayActs != null)
                        {
                            throw new BllException(15, "По выбранной дате уже существует Ежедневный наряд");
                        }

                            var upd = db.Repairs_acts.Where(t => t.rep_id == req.Main.RepairsActs).FirstOrDefault();

                            if (upd!=null && upd.status == 3)
                                throw new BllException(15, "Редактирование невозможно. Наряд на ремонт закрыт. Обратитесь к администратору");

                            //основное
                            var update = db.Daily_acts.Where(d => d.dai_id == req.Main.Id).FirstOrDefault();
                            update.date1 = req.Main.Date;
                            update.number_dai_act = req.Main.DailyNum;
                            update.rep_id = req.Main.RepairsActs;
                            update.time_start = req.Main.timeStart;
                            update.time_end = req.Main.timeEnd;
                            update.general_works = req.Main.GeneralWorks == true ? 1 : 0;
                            update.equi_equi_id = req.Main.GeneralWorks == true ? req.Main.Agr : null;
                            if (req.Main.RepairsActs != -1)
                            {
                                var smallestDates = (from d in db.Daily_acts where d.rep_id == req.Main.RepairsActs select d.date1).ToList();
                                if (smallestDates.Count != 0)
                                {
                                    upd.date_open = smallestDates.Min();
                                    nDate = smallestDates.Min();
                                }
                                else
                                {
                                    upd.date_open = req.Main.Date;
                                    nDate = req.Main.Date;
                                }
                            }
                            //таблица
                            db.Repairs_Tasks_Spendings.RemoveRange(db.Repairs_Tasks_Spendings.Where(d => d.dai_dai_id == req.Main.Id));

                            foreach (TableDataDaily a in req.Spendings)
                            {
                                db.Repairs_Tasks_Spendings.Add(new Repairs_Tasks_Spendings
                                {
                                    dai_dai_id = req.Main.Id.Value,
                                    date1 = req.Main.Date,
                                    emp_emp_id = (a.Employees != null) ? a.Employees.Id : 0,
                                    tptas_tptas_id = (a.Tasks != null) ? a.Tasks.Id : 0,
                                    rate_shift = a.RateShift,
                                    rate_piecework = a.RatePiecework,
                                    hours = a.Hours
                                });
                            }
                    }
                db.SaveChanges();
            //    trans.Commit();
                //обновляем дату открытия
                var upd1 = db.Repairs_acts.Where(t => t.rep_id == req.Main.RepairsActs).FirstOrDefault();
                var dates = (from d in db.Daily_acts where d.rep_id == req.Main.RepairsActs select d.date1).ToList();
                if (upd1 != null && dates.Count != 0)
                {
                    upd1.date_open = dates.Min();
                }
                db.SaveChanges();
                trans.Commit();
            }
            return nDate;
        }

        public bool DeleteDailyRepairsAct(TableDataDaily req)
        {
            using (var trans = db.Database.BeginTransaction())
            {
                var doc = db.Daily_acts.Where(s => s.dai_id == req.Id).FirstOrDefault();
                if (doc == null)
                    throw new BllException(15, "Документ не был найден, обновите страницу");
                if (doc.status == 2 && req.Number1 != 10)
                    throw new BllException(15, "Документ был закрыт! Удаление невозможно");
                db.Daily_acts.Remove(doc);
                db.Repairs_Tasks_Spendings.RemoveRange(db.Repairs_Tasks_Spendings.Where(d => d.dai_dai_id == req.Id));
                db.SaveChanges();
                trans.Commit();
            }
            return true;
        }

        public bool CloseDailyRepairsAct(int req)
        {
            using (var trans = db.Database.BeginTransaction())
            {

                var doc = db.Daily_acts.Where(s => s.dai_id == req).FirstOrDefault();
                if (doc != null)
                {
                    if (doc.status == 1 || doc.rep_id==-1)
                    {
                        doc.status = 2;
                        db.SaveChanges();
                        trans.Commit();
                    }
                    else
                        throw new BllException(15, "Документ уже закрыт");
                }
                else
                {
                    throw new BllException(15, "Документ не был найден , обновите страницу");
                }
                return true;
            }
        }
       //удаление приходной накладной
        public string DeleteInvoiceAct(List<DelRow> req)
        {
            var check = 0; string message = "Запчасти были списаны ранее. Удаление невозможно. Запчасти: ";

            var arr = db.Spare_parts_Records.AsEnumerable().Where(t => t.spss_spss_id == req[0].rows.Id &&
                t.store_from_id == 0 && t.store_to_id != 0).ToList();

            for (int i = 0; i < arr.Count; i++)
            {
                var cur = db.Spare_parts_Current.AsEnumerable().Where(t => t.spare_spare_id == arr[i].spare_spare_id && t.store_store_id == arr[i].store_to_id).FirstOrDefault();
                if (cur == null && arr[i].count != 0 && arr[i].price != 0)
                    {
                        check = 1;
                    }
                    else
                    {
                        if (cur != null && cur.count < arr[i].count)
                        {
                            check = 1; message = message + db.Spare_parts.AsEnumerable().Where(t => t.spare_id == arr[i].spare_spare_id).Select(t => t.name).FirstOrDefault() + ", ";
                        }
                    } 
            }

            if (check == 0)
            {
                using (var trans = db.Database.BeginTransaction())
                {
                    var n = req[0].rows.Id;
                    var doc = db.Spare_parts_Acts.Where(s => s.spss_id == n).FirstOrDefault();
                    if (doc == null)
                        throw new BllException(15, "Документ не был найден , обновите страницу");
                    db.Spare_parts_Acts.Remove(doc);

                    var doc2 = db.Spare_parts_Records.Where(s => s.spss_spss_id == n && s.store_to_id != 0 && s.store_from_id == 0).ToList();
                    db.Spare_parts_Records.RemoveRange(doc2);
                    db.SaveChanges();
                    trans.Commit();
                }
                return "success";
            }
            else { return message; }
        }
        //обновление
        public int UpdateSpareParts(List<SaveSpareParts> req)
        {
            //удаляем повторяющиеся строки
            for (int i = 0; i < req.Count; i++)
            {
                for (int j = 0; j < req.Count; j++)
                {

                    if (j != i && req[i].Document!=null && req[j].Document!=null && req[i].Document.Id == req[j].Document.Id)
                    { req.RemoveAt(j); j--; }
                }
            }
            using (var trans = db.Database.BeginTransaction())
            {
                try
                {
                    for (var i = 0; i < req.Count; i++)
                    {
                        var obj = req[i].Document;
                        if (obj != null)
                        {
                            var updateDic = db.Spare_parts_Records.Where(o => o.sps_id == obj.Id).FirstOrDefault();
                            updateDic.date = (DateTime)obj.DateBegin;
                            updateDic.spare_spare_id = (int)obj.SpareParts.Id;
                            updateDic.price = (int)obj.Price;
                            updateDic.count = (int)obj.Count;
                        }
                    }
                    db.SaveChanges();
                    trans.Commit();
                }
                catch (Exception)
                {
                    trans.Rollback();
                    throw;
                }
            }
            return req.Count;
        }
        //приходная накладная
        public int UpdateInvoiceGeneralInfo(SparePartsDetails req)
        {
            if (req.Id != null)
            {
                var curYear = DateTime.Now.Year;
                var numbers = db.Spare_parts_Acts.Where(t => t.date.Year == curYear && t.spss_id != req.Id)
                    .Select(t => t.act_number).ToList();

                var contractors = db.Spare_parts_Acts.Where(t => t.date.Year == curYear &&
                    t.spss_id != req.Id).Select(t => t.contr_id).ToList();
                var dates = db.Spare_parts_Acts.Where(t => t.date.Year == curYear &&
                    t.spss_id != req.Id).Select(t => DbFunctions.TruncateTime(t.date)).ToList();

                if (contractors.Contains(req.ContractorsId) && dates.Contains(req.Date.Value.Date) &&
                    numbers.Contains(req.Number))
                {
                    throw new BllException(15, "Приходная накладная с таким номером, поставщиком и датой уже существует.");
                }
            }
            using (var trans = db.Database.BeginTransaction())
            {
                if (!String.IsNullOrEmpty(req.Date.ToString()) && !String.IsNullOrEmpty(req.Id.ToString()))
                {
                    var updateDic = db.Spare_parts_Acts.Where(o => o.spss_id == req.Id).FirstOrDefault();
                    updateDic.date = req.Date.Value; //дата
                }
                if (!String.IsNullOrEmpty(req.ContractorsId.ToString()) && !String.IsNullOrEmpty(req.Id.ToString()))
                {
                    var updateDic = db.Spare_parts_Acts.Where(o => o.spss_id == req.Id).FirstOrDefault();
                    updateDic.contr_id = req.ContractorsId; 
                }
                //склад
                if (!String.IsNullOrEmpty(req.StorageId.ToString()) && !String.IsNullOrEmpty(req.Id.ToString()))
                {
                    var updateDic = db.Spare_parts_Acts.Where(o => o.spss_id == req.Id).FirstOrDefault();
                    updateDic.store_store_id = req.StorageId;
                }
                if (!String.IsNullOrEmpty(req.Torg12.ToString()) && !String.IsNullOrEmpty(req.Id.ToString()))
                {
                    var updateDic = db.Spare_parts_Acts.Where(o => o.spss_id == req.Id ).FirstOrDefault();
                    updateDic.torg_id =  req.Torg12 == true ? 1 : 0; 
                }
                //наличный счет
                if (!String.IsNullOrEmpty(req.Cash.ToString()) && !String.IsNullOrEmpty(req.Id.ToString()))
                {
                    var updateDic = db.Spare_parts_Acts.Where(o => o.spss_id == req.Id).FirstOrDefault();
                    updateDic.is_cash = req.Cash == true ? 1 : 0;
                }
                if (!String.IsNullOrEmpty(req.Service.ToString()) && !String.IsNullOrEmpty(req.Id.ToString()))
                {
                    var updateDic = db.Spare_parts_Acts.Where(o => o.spss_id == req.Id).FirstOrDefault();
                    updateDic.type_act = req.Service == true ? 1 : 0;
                }
                if (req.Number!=null && !String.IsNullOrEmpty(req.Id.ToString()) && !String.IsNullOrEmpty(req.Number.ToString()))
                {
                    var updateDic = db.Spare_parts_Acts.Where(o => o.spss_id == req.Id).FirstOrDefault();
                    updateDic.act_number = req.Number; //номер акта
                }
                db.SaveChanges();
                trans.Commit();
            }
            return 0;
        }

        //приходная накладная создание
        public int CreateUpdateInvoiceDetail(List<SaveRowSpareParts> req)
        {
            var idAct = req[0].Id;
            var curYear = DateTime.Now.Year;
            var numbers = db.Spare_parts_Acts.Where(t => t.date.Year == curYear && t.spss_id != idAct)
                .Select(t => t.act_number).ToList();

            var contractors = db.Spare_parts_Acts.Where(t => t.date.Year == curYear && t.spss_id != idAct
                ).Select(t => t.contr_id).ToList();
            var dates = db.Spare_parts_Acts.Where(t => t.date.Year == curYear && t.spss_id != idAct).
                Select(t => DbFunctions.TruncateTime(t.date)).ToList();

            if (contractors.Contains(req[0].ContractorsId) && dates.Contains(req[0].Date.Value.Date) &&
                numbers.Contains(req[0].Number))
            {
                throw new BllException(15, "Приходная накладная с таким номером, поставщиком и датой уже существует.");
            }

            //новый акт
            using (var trans = db.Database.BeginTransaction())
            {
                if (!String.IsNullOrEmpty(req[0].Id.ToString()))
                {
                    var id = req[0].Id;
                    var updateDic = db.Spare_parts_Acts.Where(o => o.spss_id == id).FirstOrDefault();
                    updateDic.act_number = req[0].Number; //номер акта
                }
                if (String.IsNullOrEmpty(req[0].Id.ToString()))
                {
                    var newDic = new Spare_parts_Acts
                    {
                       spss_id = CommonFunction.GetNextId(db, "Spare_parts_Acts"),
                       date = req[0].Date.Value,
                       act_number = req[0].Number,
                       contr_id = req[0].ContractorsId,
                       store_store_id = req[0].StorageId,
                       type_act = req[0].Service == true ? 1 : 0,
                       torg_id = req[0].Torg12 == true ? 1 : 0,
                       is_cash = req[0].Cash == true ? 1 : 0,
                    };
                    db.Spare_parts_Acts.Add(newDic);
                    ;
                }
                db.SaveChanges();
                trans.Commit();
                ;
            }
            using (var trans = db.Database.BeginTransaction())
            {
                if (String.IsNullOrEmpty(req[0].Pk.ToString()) || req[0].Pk == 0)
                {
              var list = db.Database.SqlQuery<BalanceMaterials>(
              string.Format(@"select max(spss_id) as Id from Spare_parts_Acts")).ToList();
               key = list[0].Id;
                }
                if (!String.IsNullOrEmpty(req[0].Pk.ToString()) && req[0].Pk != 0)
                {
                    key = req[0].Pk;
                }

                for (int i = 0; i < req.Count; i++)
                {
                    if (req[i].Document != null)
                    {
                        var newDic1 = new Spare_parts_Records
                        {
                            sps_id = CommonFunction.GetNextId(db, "Spare_parts_Records"),
                            spare_spare_id = req[i].Document.SpareParts.Id.Value,
                            nds = (float?)req[i].Document.Cost2.Value,
                            date = (DateTime)req[i].Date,
                            count = (float)req[i].Document.Count,
                            price = (float)req[i].Document.Price,
                            store_from_id = req[0].TypeAct == 1 ? 0 : req[i].Document.Storage.Id,
                            store_to_id = req[0].TypeAct == 1 ? req[i].Document.Storage.Id.Value : 0,
                            //    act_number = ()req[i].Document.Act_number,
                            spss_spss_id = key
                        };
                        db.Spare_parts_Records.Add(newDic1);
                    }
                }
                db.SaveChanges();
                trans.Commit();
            }
            return (int)key;
        }

     
        //список актов списания
        public List<SparePartsWriteOffModel> GetWriteOffActsByDate(DateTime date)
        {
            var res = new List<SparePartsWriteOffModel>();
            res = (from t3 in db.Repairs_acts
                   where DbFunctions.TruncateTime(t3.date_end) == date.Date &&
                    DbFunctions.TruncateTime(t3.date_end) == date.Date
                   let nameEquip = t3.equi_equi_id != null ? (t3.trakt_trakt_id != null ? "-" : "") + db.Equipments.Where(p => p.equi_id == t3.equi_equi_id).Select(p => p.name).FirstOrDefault() : ""
                   let nameCars = t3.trakt_trakt_id != null ? db.Traktors.Where(l => l.trakt_id == t3.trakt_trakt_id).Select(l => l.name).FirstOrDefault() : ""
                   let carNum = t3.trakt_trakt_id != null ? db.Traktors.Where(l => l.trakt_id == t3.trakt_trakt_id).Select(l => l.reg_num).FirstOrDefault() : ""
                   select new SparePartsWriteOffModel
                   {
                       Id = t3.rep_id,
                       Date = t3.date_end,
                       Tech = nameCars + nameEquip,
                       Act_number = t3.number_act,
                       CarNum = carNum,
                       Comment = t3.comment,
                   }).Distinct().ToList();

            for (int i = 0; i < res.Count; i++)
            {
                res[i].Date1 = res[i].Date.Value.ToString("dd.MM.yyyy");
            }
            var res1 = res.Where(t => !t.Act_number.Contains("М")).OrderBy(t => Int32.Parse(t.Act_number)).ToList();
            var res2 = res.Where(t => t.Act_number.Contains("М")).OrderBy(t => Int32.Parse(t.Act_number.Split('/')[1])).ToList();
            res.Clear(); res.AddRange(res1); res.AddRange(res2);
            return res;
        }

        //требования-накладные 
        public List<SparePartsWriteOffModel> GetDemandInvoiceByDate(SparePartsDetails req)
        {
            var res = new List<SparePartsWriteOffModel>();
            res = (from s in db.Repairs_Records
                   join k3 in db.Repairs_acts on s.rep_rep_id equals k3.rep_id into left3
                   from t3 in left3.DefaultIfEmpty()
                   where ((String.IsNullOrEmpty(req.Number)) ? (DbFunctions.TruncateTime(s.date) ==  DbFunctions.TruncateTime(req.Date)) : true) && t3.rep_id != null && s.count != 0
                   let nameEquip = t3.equi_equi_id != null ? (t3.trakt_trakt_id != null ? "-" : "") + db.Equipments.Where(p => p.equi_id == t3.equi_equi_id).Select(p => p.name).FirstOrDefault() : ""
                   let nameCars = t3.trakt_trakt_id != null ? db.Traktors.Where(l => l.trakt_id == t3.trakt_trakt_id).Select(l => l.name).FirstOrDefault() : ""
                  let carNum = t3.trakt_trakt_id != null ? db.Traktors.Where(l => l.trakt_id == t3.trakt_trakt_id).Select(l => l.reg_num).FirstOrDefault() : ""
                   select new SparePartsWriteOffModel
                   {
                      Id = t3.rep_id,
                      Date = s.date,
                      Tech = nameCars + nameEquip,
                      Act_number = t3.number_act,
                      CarNum = carNum,
                      Comment = t3.comment,
                   }).Distinct().OrderBy(t=>t.Date).ToList();
            for (int i = 0; i < res.Count; i++)
            {
                res[i].Date1 = res[i].Date.Value.ToString("dd.MM.yyyy");
            }
            if (String.IsNullOrEmpty(req.Number))
            {
                for (int i = 0; i < res.Count; i++)
                {
                    if (i != 0 && res[i].Act_number.Equals(res[i - 1].Act_number)) { res.RemoveAt(i); i--; }
                }
            }
            for (int i = 0; i < res.Count; i++)
            {
                var id = res[i].Id;
                var countRepairs = db.Repairs_Records.Where(t => t.rep_rep_id == id).Select(t=>DbFunctions.TruncateTime(t.date)).Distinct().ToList();

            for (int j = 0; j < countRepairs.Count; j++)
            {
                if (res[i].Date.Value.Date == countRepairs[j].Value.Date) { res[i].Act_number = res[i].Act_number + "/" + (j + 1); break; }
            }
            }
            res = res.OrderBy(t => t.Date).ToList();
            if (!String.IsNullOrEmpty(req.Number)) {
                res = res.Where(t => t.Act_number.Contains(req.Number)).ToList();    
            }

            return res;
        }
        //требования накладная данные
        public WriteOffActsDetails GetDemandInvoiceDetails(SparePartsDetails req)
        {
            var result = new WriteOffActsDetails();
            var status = db.Repairs_acts.Where(p => p.rep_id == req.ActId).Select(t => t.status).FirstOrDefault();
                result = db.Repairs_acts.Where(p => p.rep_id == req.ActId).Select(g => new WriteOffActsDetails
               {
                   Id = g.rep_id,
                   Comment = g.comment,
                   CarsName = g.trakt_trakt_id != null ? db.Traktors.Where(l => l.trakt_id == g.trakt_trakt_id).Select(l => l.name).FirstOrDefault() : "", //техника
                   CarNum = g.trakt_trakt_id != null ? db.Traktors.Where(l => l.trakt_id == g.trakt_trakt_id).Select(l => l.reg_num).FirstOrDefault() : "",
                   CarsEquip = g.equi_equi_id != null ? db.Equipments.Where(p => p.equi_id == g.equi_equi_id).Select(p => p.name).FirstOrDefault() : "",
                   SparePartsList = db.Repairs_Records.Where(p => p.rep_rep_id == req.ActId && DbFunctions.TruncateTime(p.date) == DbFunctions.TruncateTime(req.Date))
                   .Select(m => new SparePartsModel
                   {
                       SpareParts = new DictionaryItemsDTO
                       {
                           Id = (int)m.spare_spare_id,
                           Name = db.Spare_parts.Where(v => v.spare_id == m.spare_spare_id).Select(v => v.name).FirstOrDefault()
                       },
                       VendorCode = new DictionaryItemsDTO
                       {
                           Id = (int)m.spare_spare_id,
                           Name = db.Spare_parts.Where(v => v.spare_id == m.spare_spare_id).Select(v => v.vendor_code).FirstOrDefault()
                       },
                       Storage1 = db.Storages.Where(v => v.store_id == m.store_store_id).Select(v => v.name).FirstOrDefault(),
                       Store_id = (int)m.store_store_id,
                       Manufacturer = db.Spare_parts.Where(v => v.spare_id == m.spare_spare_id).Select(v => v.manufacturer).FirstOrDefault(),
                       Count = m.count,
                       Price = db.Spare_parts_Current.Where(v => v.spare_spare_id == m.spare_spare_id && v.store_store_id == m.store_store_id)
                       .Select(v => v.price).FirstOrDefault(),
                       Id = m.repair_id,
                   }).ToList(),
               }).First();
                if (status != 3)
                {
                    for (int i = 0; i < result.SparePartsList.Count; i++)
                    {
                        result.SparePartsList[i].Price = (float)Math.Round((decimal)(result.SparePartsList[i].Price ?? 0), 2);
                        result.SparePartsList[i].Cost1 = (double)Math.Round((decimal)((result.SparePartsList[i].Price ?? 0) * (result.SparePartsList[i].Count ?? 0)), 2);
                    }
                }
                else {
                    for (int i = 0; i < result.SparePartsList.Count; i++)
                    {
                        var spareId = result.SparePartsList[i].SpareParts.Id;
                        var storeId = result.SparePartsList[i].Store_id;
                        result.SparePartsList[i].Price = db.Spare_parts_Records.Where(t => t.store_from_id != 0
                        && t.store_to_id == 0 && t.spare_spare_id == spareId && t.store_from_id == storeId && t.spss_spss_id == req.ActId)
                        .Select(t => t.price).FirstOrDefault();
                        result.SparePartsList[i].Price = (float)Math.Round((decimal)(result.SparePartsList[i].Price ?? 0), 2);
                        result.SparePartsList[i].Cost1 = (double)Math.Round((decimal)((result.SparePartsList[i].Price ?? 0) * (result.SparePartsList[i].Count ?? 0)), 2);
                    }
                }
            result.Date1 = req.Date.Value.ToString("dd.MM.yyyy");
            return result;
        }




        //данные по акту поступления
        public SparePartsDetails GetActInvoiceDetails(int ActId)
        {
            var result = db.Spare_parts_Acts.Where(p => p.spss_id == ActId).Select(g => new SparePartsDetails
            {
                Id = g.spss_id,
                ActId = g.spss_id,
                Date = g.date,
                Number = g.act_number,
                ContractorsId = g.contr_id,
                StorageId = g.store_store_id,
                Torg12 = (g.torg_id == null || g.torg_id == 0) ? false : true ,
                Service = (g.type_act == null || g.type_act == 0) ? false : true,
                Cash = (g.is_cash == 1) ? true : false,
                SparePartsList = db.Spare_parts_Records.Where(p => p.spss_spss_id == ActId && p.store_from_id==0 && p.store_to_id!=0).Select(m => new SparePartsModel
                {
                    SpareParts = new DictionaryItemsDTO
                    {
                        Id = (int)m.spare_spare_id,
                        Name = db.Spare_parts.Where(v => v.spare_id == m.spare_spare_id).Select(v => v.name).FirstOrDefault() 
                    },
                    VendorCode = new DictionaryItemsDTO
                    {
                        Id = (int)m.spare_spare_id,
                        Name = db.Spare_parts.Where(v => v.spare_id == m.spare_spare_id).Select(v => v.vendor_code).FirstOrDefault()
                    },
                    Storage = new DictionaryItemsDTO
                    {
                        Id = (int)m.store_to_id,
                        Name = db.Storages.Where(v => v.store_id == m.store_to_id).Select(v => v.name).FirstOrDefault()
                    },
                    Manufacturer = db.Spare_parts.Where(v => v.spare_id == m.spare_spare_id).Select(v => v.manufacturer).FirstOrDefault(),
                    Count = m.count,
                    Price = m.price,
                    Cost1 = Math.Round(m.price * m.count,2),
                    Cost2 =  Math.Round(m.nds ?? 0,2),
                    Cost3 = Math.Round(m.price * m.count + (float)((m.price * m.count) * (m.nds / 100)), 2),
                    Id = m.sps_id
                }).ToList(),
            }).First();

            for (int i = 0; i < result.SparePartsList.Count;i++)
            {
                result.SparePartsList[i].Number = i + 1;
            }

            return result;
        }
        //кнопка заполнить
        public SparePartsDetails GetInvoiceDetailsForRepair(SparePartsModel req)
        {
            var result = db.Spare_parts_Acts.Where(p => p.spss_id == req.ActId).Select(g => new SparePartsDetails
            {
                RepairsList = db.Spare_parts_Records.Where(p => p.spss_spss_id == req.ActId && p.store_from_id == 0 && p.store_to_id != 0).Select(m => new InvoiceDetailsForRepair
                {
                    SpareParts = new DictionaryItemsDTO
                    {
                        Id = (int)m.spare_spare_id,
                        Name = db.Spare_parts.Where(v => v.spare_id == m.spare_spare_id).Select(v => v.name).FirstOrDefault()
                    },
                    VendorCode = new DictionaryItemsDTO
                    {
                        Id = (int)m.spare_spare_id,
                        Name = db.Spare_parts.Where(v => v.spare_id == m.spare_spare_id).Select(v => v.vendor_code).FirstOrDefault()
                    },
                    Storage = new DictionaryItemsDTO
                    {
                        Id = (int)m.store_to_id,
                        Name = db.Storages.Where(v => v.store_id == m.store_to_id).Select(v => v.name).FirstOrDefault()
                    },
                    Count = m.count,  //количество 
                    Reserve = (from s in db.Repairs_acts
                                  where s.status != 3
                                  join t in db.Repairs_Records on s.rep_id equals t.rep_rep_id into leftTask
                                  from t1 in leftTask.DefaultIfEmpty()
                           where t1.spare_spare_id == m.spare_spare_id && t1.store_store_id == m.store_to_id
                                  select 
                                  t1.count ?? 0  // резерв
                                  ).Sum(),
                    Reserve2 = (from s in db.Repairs_acts
                                where s.status != 3 && (req.Pk != null ? s.rep_id != req.Pk : true)
                                join t in db.Repairs_Records on s.rep_id equals t.rep_rep_id into leftTask
                                from t1 in leftTask.DefaultIfEmpty()
                                where t1.spare_spare_id == m.spare_spare_id && t1.store_store_id == m.store_to_id
                                select
                                t1.count ?? 0  // резерв2
                    ).Sum(),
                    Balance = db.Spare_parts_Current.Where(t => t.spare_spare_id == m.spare_spare_id && t.store_store_id == m.store_to_id).Select(t => t.count).FirstOrDefault(),
                    Id = m.sps_id,
                }).ToList(),
            }).First();
            for (int i = 0; i < result.RepairsList.Count; i++)
            {
           result.RepairsList[i].Reserve2 = result.RepairsList[i].Reserve2 ?? 0;
           result.RepairsList[i].Reserve = (float?)Math.Round((result.RepairsList[i].Reserve ?? 0) + (result.RepairsList[i].Count ?? 0), 2);
          //result.RepairsList[i].Available = (float?)Math.Round((result.RepairsList[i].Balance ?? 0) - (result.RepairsList[i].Reserve ?? 0), 2);  
            }
            return result;
        }
        //
        //данные по акту списания
        public WriteOffActsDetails GetWriteOffDetails(int ActId, DateTime? date)
        {
            var result = db.Repairs_acts.Where(p => p.rep_id == ActId).Select(g => new WriteOffActsDetails
            {
                Id = g.rep_id,
             //   Date = db.Spare_parts_Records.Where(p => p.spss_spss_id == ActId && p.store_from_id != 0 && p.store_to_id == 0).Select(p=>p.date).FirstOrDefault(),
                Date = g.date_end,
                Act_number = g.number_act,
                Comment = g.comment,
                CarsName = g.trakt_trakt_id != null ? db.Traktors.Where(l => l.trakt_id == g.trakt_trakt_id).Select(l => l.name).FirstOrDefault() : "", //техника
                CarNum = g.trakt_trakt_id != null ? db.Traktors.Where(l => l.trakt_id == g.trakt_trakt_id).Select(l => l.reg_num).FirstOrDefault() : "",
                CarsEquip = g.equi_equi_id != null ?  db.Equipments.Where(p => p.equi_id == g.equi_equi_id).Select(p => p.name).FirstOrDefault() : "",
                SparePartsList = db.Spare_parts_Records.Where(p => p.spss_spss_id == ActId && p.store_from_id != 0 && p.store_to_id == 0 && 
                 (date != null ? DbFunctions.TruncateTime(p.date) == DbFunctions.TruncateTime(date) : true)).Select(m => new SparePartsModel
                {
                    SpareParts = new DictionaryItemsDTO
                    {
                        Id = (int)m.spare_spare_id,
                        Name = db.Spare_parts.Where(v => v.spare_id == m.spare_spare_id).Select(v => v.name).FirstOrDefault()
                    },
                    VendorCode = new DictionaryItemsDTO
                    {
                        Id = (int)m.spare_spare_id,
                        Name = db.Spare_parts.Where(v => v.spare_id == m.spare_spare_id).Select(v => v.vendor_code).FirstOrDefault()
                    },
                    Storage1 = db.Storages.Where(v => v.store_id == m.store_from_id).Select(v => v.name).FirstOrDefault(),
                    Manufacturer = db.Spare_parts.Where(v => v.spare_id == m.spare_spare_id).Select(v => v.manufacturer).FirstOrDefault(),
                    Count = m.count,
                    Price = (float?)Math.Round(m.price,2),
                    Cost1 = Math.Round(m.price * m.count, 2),
                    Id = m.sps_id,
                }).ToList(),
            }).First();
         result.Date1 = result.Date.Value.ToString("dd.MM.yyyy");

            return result;
        }
   





        //обновление акта прихода
        public string UpdateInvoiceAct(List<SaveRowSpareParts> req)
        {
            using (var trans = db.Database.BeginTransaction())
            {
                try
                {
                    for (var i = 0; i < req.Count; i++)
                    {
                        var obj = req[i].Document;
                        var updateDic = db.Spare_parts_Records.Where(o => o.sps_id == obj.Id).FirstOrDefault();
                        if (!String.IsNullOrEmpty(req[i].Document.SpareParts.Id.ToString()) && req[i].Document.Id != 0)
                        {
                            updateDic.spare_spare_id = obj.SpareParts.Id.Value; 
                        }
                        if (!String.IsNullOrEmpty(req[i].Document.Storage.Id.ToString()) && req[i].Document.Storage.Id != 0)
                        {
                            updateDic.store_to_id = obj.Storage.Id.Value;
                        }
                        if (req[i].Document.Count == null) { req[i].Document.Count = 0; }
                        if (!String.IsNullOrEmpty(req[i].Document.Count.ToString()) && req[i].Document.Id != 0)
                        {
                            updateDic.count = obj.Count.Value; 
                        }
                        if (req[i].Document.Price == null) { req[i].Document.Price = 0; }
                        if (!String.IsNullOrEmpty(req[i].Document.Price.ToString()) && req[i].Document.Id != 0)
                        {
                            updateDic.price = obj.Price.Value; 
                        }
                        if (req[i].Document.Cost2 == null) { req[i].Document.Cost2 = 0; }
                        if (!String.IsNullOrEmpty(req[i].Document.Cost2.ToString()) && req[i].Document.Id != 0)
                        {
                            updateDic.nds = (float?)obj.Cost2;
                        }
                    }
                    db.SaveChanges();
                    trans.Commit();
                }
                catch (Exception)
                {
                    trans.Rollback();
                    throw;
                }
            }
            return "success";
        } 

        //удаление транзакций из актов поступления
        public string DeleteInvoiceTransactions(List<SparePartsModel> req)
        {
            var check = 0; string massage = "Запчасти были списаны ранее. Удаление невозможно. Запчасти: ";
            for (int i = 0; i < req.Count; i++)
            {
                if (!String.IsNullOrEmpty(req[i].Id.ToString()))
                {
                    var cur = db.Spare_parts_Current.AsEnumerable().Where(t => t.spare_spare_id == req[i].SpareParts.Id && t.store_store_id == req[i].Storage.Id).FirstOrDefault();
                    if (cur == null && req[i].Count != 0 && req[i].Price != 0)
                    {
                        check = 1;
                    }
                    else {
                        if (cur != null && cur.count < req[i].Count) {
                            check = 1; massage = massage + req[i].SpareParts.Name + ", "; 
                        }
                    }
                }
            }

            if (check == 0)
            {
                using (var trans = db.Database.BeginTransaction())
                {
                    for (int i = 0; i < req.Count; i++)
                    {
                        if (!String.IsNullOrEmpty(req[i].Id.ToString()))
                        {
                            var n = req[i].Id;
                            var doc = db.Spare_parts_Records.Where(s => s.sps_id == n).FirstOrDefault();
                            if (doc == null)
                                throw new BllException(15, "Документ не был найден , обновите страницу");
                            db.Spare_parts_Records.Remove(doc);
                        }
                    }
                    db.SaveChanges();
                    trans.Commit();
                }
                return "success";
            }
            else { return massage; }
        }
        //balance
        public List<SparePartsModel> GetBalanceSpareList(int id)
        {

            var list = (from s in db.Spare_parts_Current
                        join k1 in db.Spare_parts on s.spare_spare_id equals k1.spare_id into left1
                        from t1 in left1.DefaultIfEmpty()
                        join k2 in db.Storages on s.store_store_id equals k2.store_id into left2
                        from t2 in left2.DefaultIfEmpty()
                       let reserve =  (from l in db.Repairs_acts
                                  where l.status != 3
                                  join t in db.Repairs_Records on l.rep_id equals t.rep_rep_id into leftTask
                                  from l1 in leftTask.DefaultIfEmpty()
                                       where l1.spare_spare_id == t1.spare_id && l1.store_store_id == t2.store_id
                                  select 
                                  l1.count ?? 0  // резерв
                                  ).Sum()
                        group new
                        {
                            DateBegin = s.date
                        }
                            by new
                            {
                                Id = s.sp_id,
                                spare_id = t1.spare_id,
                                store_id = t2.store_id,
                                DateBegin = s.date,
                                Price = s.price,
                                Count = Math.Round(s.count,3),
                                SpareParts = t1.name,
                                VendorCode = t1.vendor_code,
                                Storage = t2.name,
                                Contractors = t1.manufacturer,
                                Reserve = reserve,
                                Storage2 = (float?)Math.Round((s.count - (reserve == null ? 0 : reserve)), 3),
                            } into g
                        select new SparePartsModel
                        {
                            Id = g.Key.Id,
                            DateBegin = g.Key.DateBegin,
                            SpareParts1 = g.Key.SpareParts,
                            Price = (float?)g.Key.Price,
                            Count = (float?)g.Key.Count,
                            Storage1 = g.Key.Storage,
                            VendorCode1 = g.Key.VendorCode,
                            Contractor = g.Key.Contractors,
                            Spare_id = g.Key.spare_id,
                            Store_id = g.Key.store_id,
                            Reserve = g.Key.Reserve,
                            Storage2 = g.Key.Storage2,
                        }).ToList();
            foreach (var a in list)
            {
                a.Cost3 = Math.Round(((double)a.Price) * ((double)a.Count), 2);
                if (id == 0)
                {
                    a.Price = (float)Math.Round((decimal)a.Price, 2);
                }
            }
            return list;
        }
        //перемещение - сохранение
        public string SaveMovementSpareParts(List<SparePartsMovementModel> rows)
        {
            List<string> lst = new List<string>();
            for (int j = 0; j < rows.Count; j++)
            {
                lst.Add(rows[j].Act_number);
            }
            if (lst.Count != lst.Distinct().ToList().Count) { throw new BllException(14, "Акт с таким номером уже существует"); }
            var list = db.Database.SqlQuery<ServiceModel>(
       string.Format(@"select act_number as Act_number1 from Spare_parts_Records where act_number is not null")).ToList();
            for (int j = 0; j < rows.Count; j++)
            {
                for (int i = 0; i < list.Count; i++)
                {
                    if (list[i].Act_number1.ToString().Equals(rows[j].Act_number)) { throw new BllException(14, "Акт с таким номером уже существует"); }
                }
            }
            //

            using (var trans = db.Database.BeginTransaction())
            {
                try
                {
                    for (var i = 0; i < rows.Count; i++)
                    {
                        var row = rows[i];
                        var gh = new Spare_parts_Records
                        {
                            date = row.Date,
                            act_number = Int32.Parse(row.Act_number),
                            price = row.Price.Value,
                            count = row.Count.Value,
                            spare_spare_id = row.SpareParts.Id.Value,
                            store_to_id  = row.StorageComing.Id.Value,
                            store_from_id = row.StorageWriteOff.Id.Value,
                        };
                        db.Spare_parts_Records.Add(gh);
                    }
                    db.SaveChanges();
                    trans.Commit();
                }
                catch (Exception)
                {
                    trans.Rollback();
                    throw;
                }
            }
            return "success";
        }
        // //вывод перемещение
        public SparePartsMovementListResponse GetMovementSpareParts(GridDataReq req)
        {
            List<SparePartsMovementModel> list = new List<SparePartsMovementModel>();
            var resultList = new SparePartsMovementListResponse();
            list = (from s in db.Spare_parts_Records
                    join k1 in db.Spare_parts on s.spare_spare_id equals k1.spare_id into left1
                    from t1 in left1.DefaultIfEmpty()
                    join k2 in db.Storages on s.store_to_id equals k2.store_id into left2
                    from t2 in left2.DefaultIfEmpty()
                    join k3 in db.Storages on s.store_from_id equals k3.store_id into left3
                    from t3 in left3.DefaultIfEmpty()
                    where (s.store_from_id != 0 && s.store_from_id != null) && (s.store_to_id != 0 && s.store_to_id != null)
                    group new
                    {
                        DateBegin = s.date
                    }
                        by new
                        {
                            Id = s.sps_id,
                            DateBegin = s.date,
                            Price = s.price,
                            Count = s.count,
                            Cost = Math.Round(s.price * s.count, 2),
                            Act_number = s.act_number,
                            SpareParts = new DictionaryItemsDTO
                            {
                                Id = t1.spare_id,
                                Name = t1.name,
                            },
                            VendorCode = new DictionaryItemsDTO
                            {
                                Id = t1.spare_id,
                                Name = t1.vendor_code,
                            },
                            StorageWriteOff = new DictionaryItemsDTO
                            {
                                Id = s.store_from_id,
                                Name = t3.name,
                            },
                            StorageComing = new DictionaryItemsDTO
                            {
                                Id = s.store_to_id,
                                Name = t2.name,
                            },
                        } into g
                    select new SparePartsMovementModel
                    {
                        Id = g.Key.Id,
                        Date = g.Key.DateBegin,
                        SpareParts = g.Key.SpareParts,
                        Price = g.Key.Price,
                        Count = g.Key.Count,
                        Cost = g.Key.Cost,
                        StorageWriteOff = g.Key.StorageWriteOff,
                        StorageComing = g.Key.StorageComing,
                        VendorCode = g.Key.VendorCode,
                        Act_number = g.Key.Act_number.ToString(),
                    }).OrderByDescending(m => m.Date).ToList();
            for (int l = 0; l < list.Count; l++)
            {
                list[l].Date1 = list[l].Date.ToString("dd.MM.yyyy");
            }

            var CountItems = list.Count();
            var TotalPage = (double)CountItems / (double)req.PageSize;
            double CountPages = TotalPage < 0 ? 1 : Math.Ceiling(TotalPage);

            var gridData = GridHelpers<SparePartsMovementModel>.GetGridData(list.AsQueryable(), req);
            resultList.Values = gridData.OrderByDescending(m => m.Date).ToList();
            resultList.CountPages = (int)CountPages;
            resultList.CountItems = CountItems;
            return resultList;
        }

        //обновление
        public int UpdateMovementSpareParts(List<SaveMoveSpareParts> req)
        {
            List<SparePartsMovementModel> checkNum = new List<SparePartsMovementModel>();
            for (int i = 0; i < req.Count; i++)
            {
                if (req[i].Document == null) { req.RemoveAt(i); i--; }
            }
            ////удаляем повторяющиеся строки
            for (int i = 0; i < req.Count; i++)
            {
                if (i != 0 && req[i].Document.Id == req[i - 1].Document.Id)
                { req.RemoveAt(i - 1); i--; }
            }

            var delRow = req.Where(t => t.Document.Id == 0).FirstOrDefault();
            req.Remove(delRow);

            //проверка повторяющийся номеров при обновлении 
            List<string> lst = new List<string>();
            //
            for (int j = 0; j < req.Count; j++)
            {

                lst.Add(req[j].Document.Act_number);
            }

            if (lst.Count != lst.Distinct().ToList().Count) { throw new BllException(14, "Акт с таким номером уже существует"); }



            using (var trans = db.Database.BeginTransaction())
            {
                try
                {
                    for (var i = 0; i < req.Count; i++)
                    {
                        //проверка изменился ли номер
                        var list1 = db.Database.SqlQuery<SparePartsMovementModel>(
                        string.Format(@"select act_number as Act_number1 from Spare_parts_Records where sps_id=" + req[i].Document.Id)).ToList();

                        if (list1.Count != 0 && list1[0].Act_number1.ToString().Equals(req[i].Document.Act_number))
                        {
                            checkNum = db.Database.SqlQuery<SparePartsMovementModel>(
                            string.Format(@"select act_number as Act_number1 from Spare_parts_Records where act_number <> " + req[i].Document.Act_number)).ToList();
                        }
                        else
                        {
                            checkNum = db.Database.SqlQuery<SparePartsMovementModel>(
                            string.Format(@"select act_number as Act_number1 from Spare_parts_Records where act_number is not null")).ToList();
                        }

                        for (int i1 = 0; i1 < checkNum.Count; i1++)
                        {
                            if (checkNum[i1].Act_number1.ToString().Equals(req[i].Document.Act_number)) { throw new BllException(14, "Акт с таким номером уже существует"); }
                        }

                        var obj = req[i].Document;
                        if (obj.Id != 0)
                        {
                            var updateDic = db.Spare_parts_Records.Where(o => o.sps_id == obj.Id).FirstOrDefault();
                            if (updateDic != null)
                            {
                                updateDic.date = obj.Date;
                                updateDic.act_number = Int32.Parse(obj.Act_number);
                                updateDic.spare_spare_id = obj.SpareParts.Id.Value;
                                updateDic.date = obj.Date;
                                updateDic.store_to_id = obj.StorageComing.Id.Value;
                                updateDic.store_from_id = obj.StorageWriteOff.Id.Value;
                                updateDic.price = obj.Price.Value;
                                updateDic.count = obj.Count.Value;
                            }
                        }
                    }
                    db.SaveChanges();
                    trans.Commit();
                }
                catch (Exception)
                {
                    trans.Rollback();
                    throw;
                }
            }
            return req.Count;
        }

        //удаление перемещение
        public string DeleteMovementSpareParts(List<DelMoveRow> req)
        {
            using (var trans = db.Database.BeginTransaction())
            {

                for (int i = 0; i < req.Count; i++)
                {
                    var n = req[i].rows.Id;
                    var doc = db.Spare_parts_Records.Where(s => s.sps_id == n).FirstOrDefault();
                    if (doc == null)
                        throw new BllException(15, "Документ не был найден , обновите страницу");
                    db.Spare_parts_Records.Remove(doc);
                }

                db.SaveChanges();
                trans.Commit();
            }
            return "ok";
        }
        //цена с баланса и резерв
        public List<SparePartsModel> GetPriceCountBalance(SparePartsModel req)
        {
            var price = db.Spare_parts_Current.Where(t => t.spare_spare_id == req.Spare_id && t.store_store_id == req.Store_id).Select(t => t.price).FirstOrDefault();
            var count = db.Spare_parts_Current.Where(t => t.spare_spare_id == req.Spare_id && t.store_store_id == req.Store_id).Select(t => t.count).FirstOrDefault();
            var SparePartsList = (from s in db.Repairs_acts
                                  where s.status != 3
                                  join t in db.Repairs_Records on s.rep_id equals t.rep_rep_id into leftTask
                                  from t1 in leftTask.DefaultIfEmpty()
                                  where t1.spare_spare_id == req.Spare_id && t1.store_store_id == req.Store_id
                                  select new DictionaryItemsDTO
                                  {
                                      Balance = t1.count
                                  }).ToList();
            var reserve2 = (from s in db.Repairs_acts
                                  where s.status != 3 && (req.Pk != null ? s.rep_id != req.Pk : true)
                                  join t in db.Repairs_Records on s.rep_id equals t.rep_rep_id into leftTask
                                  from t1 in leftTask.DefaultIfEmpty()
                                  where t1.spare_spare_id == req.Spare_id && t1.store_store_id == req.Store_id
                                  select new DictionaryItemsDTO
                                  {
                                      Balance = t1.count
                                  }).ToList();
            float? reserve = SparePartsList.Sum(t => t.Balance) ?? 0;
            float reserve3 = reserve2.Sum(t => t.Balance) ?? 0;
            if (reserve == null) { reserve = 0; }
            var res = new SparePartsModel
            {
                Price = price,
                Count = count,
                Reserve = reserve,
                Reserve2 = reserve3,
            };
            return new List<SparePartsModel> { res };
        }
        //склады с баланса
        public List<DictionaryItemsDTO> GetListStorageSpareParts(SparePartsModel req)
        {
          var  StorageList = db.Spare_parts_Current.Where(p=>p.spare_spare_id == req.Spare_id).Select(p => new DictionaryItemsDTO
            {
                Id = p.store_store_id,
                Name = db.Storages.Where(i => i.store_id == p.store_store_id).Select(i => i.name).FirstOrDefault()
            }).Distinct().ToList();

          return StorageList;
        }
        //запчасти в наряды
        public List<DictionaryItemsDTO> GetListRepairsSpareParts(SparePartsRepairs req)
        {
            var SparePartsList = (from s in db.Spare_parts_Current

                       join t in db.Spare_parts on s.spare_spare_id equals t.spare_id into leftTask
                       from t1 in leftTask.DefaultIfEmpty()
                        where t1.deleted == false
                   //    join k in db.Group_traktors on t1.tptrak_tptrak_id equals k.grtrak_id into leftTask1
                   //    from t2 in leftTask1.DefaultIfEmpty()

                       join p in db.Type_spare_parts on t1.tptrak_tptrak_id equals p.tsp_id into leftTask3
                       from t4 in leftTask3.DefaultIfEmpty()
                        where (req.TraktorId != null && req.TraktorId!=-1) ? t4.tsp_id == req.TraktorId : true

                       join l in db.Modules on t1.mod_mod_id equals l.mod_id into leftTask2
                       from t3 in leftTask2.DefaultIfEmpty()
                       where (req.ModId != null && req.ModId!=-1) ? t1.mod_mod_id == req.ModId : true

                      let nameModule = (req.ModId == null || req.ModId==-1) ? "-" + t3.name : ""
                     let nameTypeSpareParts = (req.TraktorId == null || req.TraktorId == -1) ? "-" + t4.name : ""
                        select new DictionaryItemsDTO
                        {
                         Id = t1.spare_id,
                         Name = t1.name + nameTypeSpareParts + nameModule,
                         Description = t1.vendor_code
                        }).Distinct().ToList();
            return SparePartsList;
        }
        ////код для техники в справочники
        public string GetUniqCodeForTech(SparePartsRepairs req)
        {
            string totalCode = null; string month = null; string day = null; string nNum = null;
            month = DateTime.Now.Month.ToString().Length < 2 ? "0" + DateTime.Now.Month.ToString() : DateTime.Now.Month.ToString();
            day = DateTime.Now.Day.ToString().Length < 2 ? "0" + DateTime.Now.Day.ToString() : DateTime.Now.Day.ToString();
            int number1 = db.Spare_parts.Where(t => t.spare_id != req.ModId).ToList().Count() + 1;
            string number = number1.ToString();
            nNum = number.Length == 1 ? "000" + number : number.Length == 2 ? "00" + number : number.Length == 3 ? "0" + number : number;
            if (req.TraktorId != null)
            {
                var code = db.Type_spare_parts.Where(t => t.tsp_id == req.TraktorId).Select(t => t.uniq_code).FirstOrDefault();
                if (code == null) { code = "000"; }
              
                if (req.ModId != null)
                {
                    string number2 = db.Spare_parts.Where(t => t.spare_id == req.ModId).Select(t => t.code).FirstOrDefault();
                    if (number2 != null) { nNum = number2.Split('.')[2].ToString(); }
                }

                totalCode = code + "." + month + day + "." + nNum;
            }
            else {
                if (req.ModId != null)
                {
                    string number2 = db.Spare_parts.Where(t => t.spare_id == req.ModId).Select(t => t.code).FirstOrDefault();
                    if (number2 != null) { nNum = number2.Split('.')[2].ToString(); }
                    totalCode = "000." + month + day + "." + nNum;
                }
                else {
                    totalCode = "000." + month + day + "." + nNum;
                }
            }

            return totalCode;
        }

        //оборудование
        public List<DictionaryItemsDTO> GetListEquipSpareParts(SparePartsRepairs req)
        {
            var SparePartsList = db.Spare_parts.Where(p => p.deleted == false).Select(p => new DictionaryItemsDTO
            {
                Id = p.spare_id,
                Name = p.name + " - " + db.Group_traktors.Where(b => b.grtrak_id == p.tptrak_tptrak_id).Select(b => b.name).FirstOrDefault() + " - " +
                db.Modules.Where(l => l.mod_id == p.mod_mod_id).Select(l => l.name).FirstOrDefault(),
                Description = p.vendor_code,
                Manufacturer = p.manufacturer
            }).ToList();
            var list = db.Spare_parts_Current.Select(l => new DictionaryItemsDTO { Id = l.spare_spare_id }).Distinct().ToList();
            for (int i = 0; i < SparePartsList.Count; i++)
            {
                var l = SparePartsList[i].Id;
                var t = list.Where(k => k.Id == l).FirstOrDefault();
                if (t == null) { SparePartsList.RemoveAt(i); if (i != 0) { i--; } }
            }
            return SparePartsList;
        }

        ///сохранение нарядов
        public string CreateUpdateRepairsDetail(CreateUpdateRepairs req)
        {
            var check = 0; string massage = "Запчасти с нулевой ценой не могут быть списаны. Запчасти: ";
            if (!req.GeneralInfo.IsDaily && req.GeneralInfo.dateEnd.HasValue && req.GeneralInfo.dateOpen > req.GeneralInfo.dateEnd.Value)
            { throw new BllException(14, "Дата закрытия наряда не может быть раньше даты открытия наряда"); }

            if (!req.GeneralInfo.IsDaily && req.GeneralInfo.dateOpen != null && req.GeneralInfo.dateStart.Date > req.GeneralInfo.dateOpen.Value)
            { throw new BllException(14, "Дата открытия наряда не может быть раньше даты создания наряда"); }
            if (req.GeneralInfo.dateEnd1 == null && req.GeneralInfo.WriteOff) { throw new BllException(14, "Выберите дату закрытия"); }
            if ((req.GeneralInfo.IsDaily || req.GeneralInfo.AdminDates==true) && req.GeneralInfo.dateEnd1.HasValue && 
                req.GeneralInfo.dateOpen1 > req.GeneralInfo.dateEnd1.Value)
            { throw new BllException(14, "Дата закрытия наряда не может быть раньше даты открытия наряда"); }

            if ((req.GeneralInfo.IsDaily || req.GeneralInfo.AdminDates == true) && req.GeneralInfo.dateOpen1 != null && 
                req.GeneralInfo.dateStart.Date > req.GeneralInfo.dateOpen1.Value)
            { throw new BllException(14, "Дата открытия наряда не может быть раньше даты создания наряда"); }

            for (int i = 0; i < req.TableInfo3.Count; i++) {
                var start = new DateTime(req.GeneralInfo.dateStart.Date.Year, req.GeneralInfo.dateStart.Date.Month, req.GeneralInfo.dateStart.Date.Day);
                var dateSpare = new DateTime(req.TableInfo3[i].Date.Value.Year, req.TableInfo3[i].Date.Value.Month, req.TableInfo3[i].Date.Value.Day);

                if (start > dateSpare)
                {
                    throw new BllException(14, "Дата списания запчасти не может быть раньше даты создания наряда");  
                }
            }
            var nId = 0;
                if (req.GeneralInfo.ActId == null)
                {
                    //новый наряд
                    using (var trans = db.Database.BeginTransaction())
                    {
                        var list2 = db.Repairs_acts.Where(t => t.number_act != null && t.date_start.Year == req.GeneralInfo.dateStart.Year).Select(t => new DictionaryItemsDTO { Name = t.number_act }).ToList();
                        for (int i = 0; i < list2.Count; i++)
                        {
                            if (list2[i].Name.Equals(req.GeneralInfo.RepairNumber)) { throw new BllException(14, "Документ с таким номером уже существует"); }
                        }
                        var typePlan = 0; var isDaily = 0; var IsRequire = 0; 
                        if (req.GeneralInfo.Plan1) { typePlan = 1; }
                        if (req.GeneralInfo.Plan2) { typePlan = 2; }
                        if (req.GeneralInfo.Plan3) { typePlan = 3; }
                        if (req.GeneralInfo.Plan4) { typePlan = 4; }
                        if (req.GeneralInfo.IsDaily) { isDaily = 1; }
                        else
                        {
                            isDaily = 0;
                        }
                        if (req.GeneralInfo.IsRequire) { IsRequire = 1; }
                        else { IsRequire = 0; }
                        var newDic = new Repairs_acts
                        {
                            rep_id = CommonFunction.GetNextId(db, "Repairs_acts"),
                            number_act = req.GeneralInfo.RepairNumber,
                            date_start = req.GeneralInfo.dateStart,
                            date_end = (isDaily == 1 || req.GeneralInfo.AdminDates == true) ? req.GeneralInfo.dateEnd1 : req.GeneralInfo.dateEnd,
                            date_open = (isDaily == 1 || req.GeneralInfo.AdminDates == true) ? req.GeneralInfo.dateOpen1 : req.GeneralInfo.dateOpen,
                            emp_emp_id = req.GeneralInfo.EmpId,
                            trakt_trakt_id = req.GeneralInfo.TraktorId != -1 ? req.GeneralInfo.TraktorId : null,
                            mileage = req.GeneralInfo.Run,
                            moto = req.GeneralInfo.Moto,
                            mod_mod_id = req.GeneralInfo.ModuleId,
                            equi_equi_id = req.GeneralInfo.AgroreqId != -1 ? req.GeneralInfo.AgroreqId : null,
                            type_repairs = typePlan,
                            type_rep_parts = req.GeneralInfo.TypeSpareId,
                            status = IsRequire == 1 ? 3 : 1,
                            is_daily = isDaily,
                            is_require = IsRequire,
                            comment = req.GeneralInfo.Comment,
                            is_admin_dates = req.GeneralInfo.AdminDates == true ? 1 : 0,
                        };
                        if (IsRequire == 1)
                        {
                            //проверка даты
                            for (int i = 0; i < req.TableInfo3.Count; i++)
                            {
                                if (req.TableInfo3[i].Date.Value.Date > newDic.date_end.Value.Date)
                                {
                                    throw new BllException(14, "Дата списания запчасти не может быть позже даты закрытия наряда");
                                }
                            }
                        }

                        db.Repairs_acts.Add(newDic);
                        db.SaveChanges();
                        trans.Commit();
                    }
                }
                    //ActId != null
                else
                {
                    var checkDaily = db.Daily_acts.Where(t => t.rep_id == req.GeneralInfo.ActId).Count();
                    if (checkDaily != 0 && req.GeneralInfo.IsDaily)
                    {
                        throw new BllException(14, "По текущему наряду на ремонт открыты ежедневные наряды. Необходимо убрать 'Без ЕН'");
                    }

                    if (req.GeneralInfo.WriteOff == true)
                    {
                        if (!req.GeneralInfo.IsDaily && !req.GeneralInfo.IsRequire)
                        {
                            var count = db.Daily_acts.Where(t => t.rep_id == req.GeneralInfo.ActId).Count();
                            if (count == 0)
                            {
                                throw new BllException(14, "По текущему наряду на ремонт не были открыты ежедневные наряды. Необходимо выбрать 'Без ЕН'");
                            }
                        }

                        var con = db.Daily_acts.Where(t => t.rep_id == req.GeneralInfo.ActId && t.status == 1).FirstOrDefault();
                        if (con != null) { throw new BllException(14, "По текущему наряду на ремонт существуют открытые ежедневные наряды. Необходимо закрыть"); }
                        //проверка нулевой цены
                        for (int i = 0; i < req.TableInfo3.Count; i++)
                        {
                            var cur = db.Spare_parts_Current.AsEnumerable().Where(t => t.spare_spare_id == req.TableInfo3[i].SpareParts.Id && t.store_store_id == req.TableInfo3[i].Storage.Id).FirstOrDefault();
                            if (cur != null)
                            {
                                if (cur.price == 0)
                                {
                                    check = 1; massage = massage + req.TableInfo3[i].SpareParts.Name + ", ";
                                }
                            }
                        }
                    }
                    //обновляем наряд
                    using (var trans = db.Database.BeginTransaction())
                    {
                        var typePlan1 = 0; var isDaily1 = 0; var IsRequire1 = 0;
                        if (req.GeneralInfo.Plan1) { typePlan1 = 1; }
                        if (req.GeneralInfo.Plan2) { typePlan1 = 2; }
                        if (req.GeneralInfo.Plan3) { typePlan1 = 3; }
                        if (req.GeneralInfo.Plan4) { typePlan1 = 4; }
                        if (req.GeneralInfo.IsDaily) { isDaily1 = 1; }
                        else { isDaily1 = 0; }
                        if (req.GeneralInfo.IsRequire) { IsRequire1 = 1; }
                        else { IsRequire1 = 0; }

                        var updateDic = db.Repairs_acts.Where(o => o.rep_id == req.GeneralInfo.ActId).FirstOrDefault();
                        if (updateDic.status == 3)
                            throw new BllException(15, "Документ был закрыт! Редактирование невозможно. Нажмите на кнопку \"Отмена\"");
                        // условие, если по Нар.на.рем. есть Еж.нар. и технику или оборудование пытаются изменить - то уведомление о невозможности изменения
                        int dailyCnt = db.Daily_acts.Where(w => w.rep_id == updateDic.rep_id).Count(); // кол-во еж.нар.
                        if (dailyCnt != 0 && (updateDic.trakt_trakt_id != req.GeneralInfo.TraktorId || updateDic.equi_equi_id != req.GeneralInfo.AgroreqId))
                            throw new BllException(15, "По текущему наряду на ремонт ведутся работы. Изменение техники/оборудования невозможно.");
                        updateDic.mileage = req.GeneralInfo.Run;
                        updateDic.date_start = req.GeneralInfo.dateStart;

                        if (req.GeneralInfo.WriteOff == true)
                        {
                            DateTime? DateDayAct = db.Daily_acts.Where(t => t.rep_id == req.GeneralInfo.ActId).Max(t => (DateTime?)t.date1);

                            if (DateDayAct != null && req.GeneralInfo.AdminDates == true && DateDayAct.Value.Date > req.GeneralInfo.dateEnd1.Value.Date)
                            {
                                throw new BllException(14, "Дата закрытия НН не может быть меньше даты открытия ЕН");
                            }
                            if (DateDayAct != null && req.GeneralInfo.AdminDates == true &&  req.GeneralInfo.dateOpen1.Value.Date  > DateDayAct.Value.Date )
                            {
                                throw new BllException(14, "Дата открытия НН не может быть больше даты открытия ЕН");
                            }


                            if (isDaily1 == 1 || req.GeneralInfo.AdminDates == true)
                            {
                                updateDic.date_end = req.GeneralInfo.dateEnd1;
                            }
                            if (IsRequire1 == 1)
                            {
                                updateDic.date_end = req.GeneralInfo.dateEnd;
                                ////сравнение дат
                             //   var sdate = req.TableInfo3.Max(t => t.Date);
                            //    if (sdate > updateDic.date_end) { updateDic.date_end = sdate; }
                            }
                            if (isDaily1 != 1 && IsRequire1 != 1)
                            {
                                updateDic.date_end = DateDayAct;

                             ////сравнение дат
                             var sdate = req.TableInfo3.Max(t => t.Date);
                             if (sdate > DateDayAct) { updateDic.date_end = sdate; }
                            }
                            //проверка даты
                            for (int i = 0; i < req.TableInfo3.Count; i++)
                            {
                                if (req.TableInfo3[i].Date.Value.Date > updateDic.date_end.Value.Date)
                                {
                                    throw new BllException(14, "Дата списания запчасти не может быть позже даты закрытия наряда");
                                }
                                //ЕН  и редак-е дат 
                                if ((req.GeneralInfo.IsDaily || req.GeneralInfo.AdminDates == true) &&
                                    req.TableInfo3[i].Date.Value.Date > req.GeneralInfo.dateEnd1)
                                {
                                    throw new BllException(14, "Дата списания запчасти не может быть позже даты закрытия наряда");
                                }
                            }
                        }
                        updateDic.date_open = (isDaily1 == 1 || req.GeneralInfo.AdminDates == true) ? req.GeneralInfo.dateOpen1 : req.GeneralInfo.dateOpen;


                        updateDic.mod_mod_id = req.GeneralInfo.ModuleId;
                        updateDic.moto = req.GeneralInfo.Moto;
                        updateDic.status = updateDic.status != 2 ? 1 : 2;
                        updateDic.trakt_trakt_id = req.GeneralInfo.TraktorId != -1 ? req.GeneralInfo.TraktorId : null;   
                        updateDic.type_repairs = typePlan1;
                        updateDic.is_daily = isDaily1;
                        updateDic.is_require = IsRequire1;
                        updateDic.emp_emp_id = req.GeneralInfo.EmpId;
                        updateDic.equi_equi_id = req.GeneralInfo.AgroreqId != -1 ? req.GeneralInfo.AgroreqId : null;    
                        updateDic.status = (req.GeneralInfo.WriteOff == true && check == 0) ? 3 : updateDic.status;
                        updateDic.type_rep_parts = req.GeneralInfo.TypeSpareId;
                        updateDic.comment = req.GeneralInfo.Comment;
                        updateDic.is_admin_dates = req.GeneralInfo.AdminDates == true ? 1 : 0;
                        if (req.GeneralInfo.RepairNumber != null)
                        {
                            //проверка номера акта
                            var list2 = db.Repairs_acts.Where(t => t.rep_id != req.GeneralInfo.ActId && t.number_act != null && t.date_start.Year == req.GeneralInfo.dateStart.Year).Select(t => new DictionaryItemsDTO { Name = t.number_act }).ToList();
                            for (int i = 0; i < list2.Count; i++)
                            {
                                if (list2[i].Name.Equals(req.GeneralInfo.RepairNumber)) { throw new BllException(14, "Документ с таким номером уже существует"); }
                            }
                            updateDic = db.Repairs_acts.Where(o => o.rep_id == req.GeneralInfo.ActId).FirstOrDefault();
                            updateDic.number_act = req.GeneralInfo.RepairNumber; //номер акта
                        }
                        db.SaveChanges();
                        trans.Commit();
                    }
                }
                using (var trans = db.Database.BeginTransaction())
                {
                    if (req.GeneralInfo.ActId == null)
                    {
                         nId = db.Repairs_acts.Max(t => t.rep_id);
                    }
                    else {
                        nId = req.GeneralInfo.ActId.Value;

                        var dist = db.Defect_Records.Where(t => t.rep_rep_id == req.GeneralInfo.ActId);
                        db.Defect_Records.RemoveRange(dist);

                       var dist1 = db.Repairs_Records.Where(t => t.rep_rep_id == req.GeneralInfo.ActId);
                        db.Repairs_Records.RemoveRange(dist1);

                        var dist2 = db.Repairs_Service.Where(t => t.rep_rep_id == req.GeneralInfo.ActId);
                        db.Repairs_Service.RemoveRange(dist2);

                        db.SaveChanges();
                    }

                    //таблица1
                    for (int i = 0; i < req.TableInfo1.Count; i++)
                    {
                        var newDic1 = new Defect_Records
                        {
                            def_id = CommonFunction.GetNextId(db, "Defect_Records"),
                            defect = req.TableInfo1[i].Malfunction,
                            rep_rep_id = nId,
                            defect_type = 1
                        };
                        db.Defect_Records.Add(newDic1);
                    }
                    //таблица2
                    for (int i = 0; i < req.TableInfo2.Count; i++)
                    {
                        var newDic1 = new Defect_Records
                        {
                            def_id = CommonFunction.GetNextId(db, "Defect_Records"),
                            defect = req.TableInfo2[i].Malfunction,
                            tptas_tptas_id = req.TableInfo2[i].TypeTask.Id,
                            rep_rep_id = nId,
                            defect_type = 2
                        };
                        db.Defect_Records.Add(newDic1);
                    }
                    //таблица5
                    for (int i = 0; i < req.TableInfo5.Count; i++)
                    {
                        var newDic = new Repairs_Service
                        {
                            repserv_id = CommonFunction.GetNextId(db, "Repairs_Service"),
                            name_service = req.TableInfo5[i].NameService,
                            count = req.TableInfo5[i].Count,
                            price = req.TableInfo5[i].Price,
                            rep_rep_id = nId,
                            nds = req.TableInfo5[i].Cost2,
                        };
                        db.Repairs_Service.Add(newDic);
                    }
                    //таблица3
                    for (int i = 0; i < req.TableInfo3.Count; i++)
                    {
                        var newId = 0;
                        if (req.GeneralInfo.IsRequire)
                        {
                            newId = db.Repairs_acts.Max(t => t.rep_id);
                        }
                        //списываем запчасти
                        if (req.GeneralInfo.WriteOff == true && check == 0) {

                            var spareId = req.TableInfo3[i].SpareParts.Id;
                            var storeId = req.TableInfo3[i].Storage.Id;
                            var newDic2 = new Spare_parts_Records
                            {
                                sps_id = CommonFunction.GetNextId(db, "Spare_parts_Records"),
                                date = DateTime.Now,
                                spare_spare_id = req.TableInfo3[i].SpareParts.Id.Value,
                                count = req.TableInfo3[i].Count.Value,
                                store_to_id = 0,
                                store_from_id = req.TableInfo3[i].Storage.Id,
                                price = db.Spare_parts_Current.Where(t => t.spare_spare_id == spareId &&
                               t.store_store_id == storeId).Select(t => t.price).FirstOrDefault(),
                                spss_spss_id = (req.GeneralInfo.IsRequire && req.GeneralInfo.ActId == null) ? newId : req.GeneralInfo.ActId,
                            };
                            db.Spare_parts_Records.Add(newDic2);   
                        }

                        var newDic1 = new Repairs_Records
                        {
                            repair_id = CommonFunction.GetNextId(db, "Repairs_Records"),
                            spare_spare_id = req.TableInfo3[i].SpareParts.Id,
                            store_store_id = req.TableInfo3[i].Storage.Id,
                            rep_rep_id = nId,
                            count = req.TableInfo3[i].Count,
                            date = req.TableInfo3[i].Date,
                        };
                        db.Repairs_Records.Add(newDic1);
                    }
                    db.SaveChanges();
                    trans.Commit();
                }

                if (check == 0)
                {
                   return nId.ToString();
             //       return "success";
                }
                else { return massage; }
        }
        //данные по наряду на ремонт
        public CreateUpdateRepairs GetRepairsDetails(GeneralInfo req)
        {
            DateTime? DateDayAct = db.Daily_acts.Where(t => t.rep_id == req.ActId).Max(t => (DateTime?)t.date1);
            DateTime? DateOpenAct = db.Daily_acts.Where(t => t.rep_id == req.ActId).Min(t => (DateTime?)t.date1);
            var result = db.Repairs_acts.Where(p => p.rep_id == req.ActId).Select(g => new CreateUpdateRepairs
            {
              GeneralInfo = new GeneralInfo {
                ActId = g.rep_id,
                dateStart = g.date_start,
                dateEnd = (g.status == 3 && g.is_require != 1) ? (g.date_end < DateDayAct ? DateDayAct : g.date_end) :
                ((g.status == 3 && g.is_require == 1) ? g.date_end : null ),
                dateEnd1 = g.date_end == null ? DateTime.Now : g.date_end,
                dateOpen = DateOpenAct,
                dateOpen1 = g.date_open == null ? DateTime.Now : g.date_open,
                EmpId = g.emp_emp_id,
                AgroreqId = g.equi_equi_id,
                ModuleId = g.mod_mod_id,
                RepairNumber = g.number_act,
                Moto = g.moto.Value,
                Run = g.mileage.Value,
                Comment = g.comment,
                TraktorId = g.trakt_trakt_id,
                Plan1 = g.type_repairs == 1 ? true : false,
                Plan2 = g.type_repairs == 2 ? true : false,
                Plan3 = g.type_repairs == 3 ? true : false,
                Plan4 = g.type_repairs == 4 ? true : false,
                IsDaily = g.is_daily == 1 ? true : false,
                AdminDates = g.is_admin_dates == 1 ? true : false,
                Status = g.status,
                TypeSpareId = g.type_rep_parts,
                IsRequire = g.is_require == 1 ? true : false,
              },
              TableInfo1 = db.Defect_Records.Where(p => p.rep_rep_id == req.ActId && p.defect_type == 1).Select(m => new TableInfo1
              {
                  Id = m.def_id,
                  Malfunction = m.defect,                 
              }).ToList(),
              TableInfo2 = db.Defect_Records.Where(p => p.rep_rep_id == req.ActId && p.defect_type == 2).Select(v => new TableInfo2
              {
                  Id = v.def_id,
                  Malfunction = v.defect,
                  TypeTask = new DictionaryItemsDTO
                    {
                        Id = (int)v.tptas_tptas_id,
                        Name = db.Type_tasks.Where(k => k.tptas_id == v.tptas_tptas_id).Select(k => k.name).FirstOrDefault()
                    },
              }).ToList(),
              TableInfo5 = db.Repairs_Service.Where(p => p.rep_rep_id == req.ActId).Select(n => new TableInfo5
              {
                  Id = n.repserv_id,
                  NameService = n.name_service,
                  Count = n.count,
                  Price = n.price,
                  Cost2 = n.nds,
              }).ToList(),
              TableInfo3 = db.Repairs_Records.Where(p => p.rep_rep_id == req.ActId).Select(m => new TableInfo3
              {
                  SpareParts = new DictionaryItemsDTO
                  {
                      Id = (int)m.spare_spare_id,
                      Name = db.Spare_parts.Where(v => v.spare_id == m.spare_spare_id).Select(v => v.name).FirstOrDefault()
                  },
                  VendorCode = new DictionaryItemsDTO
                  {
                      Id = (int)m.spare_spare_id,
                      Name = db.Spare_parts.Where(v => v.spare_id == m.spare_spare_id).Select(v => v.vendor_code).FirstOrDefault()
                  },
                 Storage = new DictionaryItemsDTO
                  {
                      Id = (int)m.store_store_id,
                      Name = db.Storages.Where(v => v.store_id == m.store_store_id).Select(v => v.name).FirstOrDefault()
                  },
                  Count = m.count,
                  Reserve = (from s in db.Repairs_acts
                                  where s.status != 3
                                  join t in db.Repairs_Records on s.rep_id equals t.rep_rep_id into leftTask
                                  from t1 in leftTask.DefaultIfEmpty()
                              where t1.spare_spare_id == m.spare_spare_id && t1.store_store_id == m.store_store_id
                                  select new
                                  {
                                      Balance = t1.count
                                  }).ToList().Sum(t => t.Balance),
                  Reserve2 = (from s in db.Repairs_acts
                              where s.status != 3 && s.rep_id != req.ActId
                             join t in db.Repairs_Records on s.rep_id equals t.rep_rep_id into leftTask
                             from t1 in leftTask.DefaultIfEmpty()
                             where t1.spare_spare_id == m.spare_spare_id && t1.store_store_id == m.store_store_id
                             select new
                             {
                                 Balance = t1.count ?? 0
                             }).ToList().Sum(t => t.Balance),
                  Balance = (float?)db.Spare_parts_Current.Where(t => t.spare_spare_id == m.spare_spare_id && t.store_store_id == m.store_store_id).Select(t => t.count).FirstOrDefault(),
                  Cost = (float?)db.Spare_parts_Current.Where(t => t.spare_spare_id == m.spare_spare_id && t.store_store_id == m.store_store_id).Select(t => m.count*t.price).FirstOrDefault(),
                  Id = m.repair_id,
                  Date = m.date,
              }).ToList(),
              //Ежедневные наряды
              TableInfo4 = (from s in db.Daily_acts
                            where s.rep_id == req.ActId
                       join t in db.Repairs_Tasks_Spendings on s.dai_id equals t.dai_dai_id into leftTask
                       from t1 in leftTask.DefaultIfEmpty()
                            group new
                            {
                               SumCost = (t1.rate_piecework ?? 0) * (t1.hours ?? 0) + (t1.rate_shift ?? 0),
                               Hours = t1.hours, 
                            }
                           by new
                           {
                        Id = t1.dai_dai_id,
                        DayAct = s.dai_id,
                        Date = s.date1,
                        Number = s.number_dai_act,
                        Status = s.status == 1 ? "Открыт" : "Закрыт",
                           } into v
                        select new TableInfo4
                        {
                         Id = v.Key.Id,
                         Number = v.Key.Number,
                         SumCost = v.Sum(l=>l.SumCost),
                         Hours = v.Sum(l => l.Hours),
                         Status = v.Key.Status,
                         Date = v.Key.Date,
                         DailyAct = v.Key.DayAct,
                        }).ToList(),
            }).First();
            var count = 1;
            for (int i = 0; i < result.TableInfo1.Count; i++)
            {
                result.TableInfo1[i].Number = count;
                count++;
            }
            count = 1;
            for (int i = 0; i < result.TableInfo2.Count; i++)
            {
                result.TableInfo2[i].Number = count;
                count++;
            }
            count = 1;
            for (int i = 0; i < result.TableInfo5.Count; i++)
            {
                result.TableInfo5[i].Number = count;
                result.TableInfo5[i].Cost1 = (float?)Math.Round((result.TableInfo5[i].Count ?? 0) * (result.TableInfo5[i].Price ?? 0), 2);
                result.TableInfo5[i].Cost3 = result.TableInfo5[i].Cost1 + (float?)Math.Round((result.TableInfo5[i].Cost2 ?? 0) / 100 * (result.TableInfo5[i].Cost1 ?? 0), 2);
                count++;
            }
            count = 1;
            for (int i = 0; i < result.TableInfo3.Count; i++)
            {
                result.TableInfo3[i].Number = count;
                result.TableInfo3[i].Reserve2 = result.TableInfo3[i].Reserve2 ?? 0;


      //        result.TableInfo3[i].Reserve2 = (float?)Math.Round(((double) (result.TableInfo3[i].Reserve ?? 0) ) - ((double)(result.TableInfo3[i].Count ?? 0)), 3);
          //     result.TableInfo3[i].Reserve2 = result.TableInfo3[i].Reserve;  // new
                result.TableInfo3[i].Available = (float?)Math.Round(((double)(result.TableInfo3[i].Balance ?? 0)) - ((double)(result.TableInfo3[i].Reserve ?? 0)), 3);
                count++;
            }
            for (int i = 0; i < result.TableInfo4.Count; i++)
            {
                result.TableInfo4[i].Date1 = result.TableInfo4[i].Date.Value.ToString("dd.MM.yyyy");
            }
            result.GeneralInfo.CheckDailyAct = db.Daily_acts.Where(t => t.rep_id == req.ActId && DbFunctions.TruncateTime(t.date1) == DbFunctions.TruncateTime(DateTime.Now)).Select(t => t.rep_id).Count();
           return result;
        }
        //обновить дату открытия
        public String SaveRepairsDateOpen(int? ActId)
        {
            using (var trans = db.Database.BeginTransaction())
            {

                if (ActId != null)
                {
                    //открыт ли уже наряд
                    var check = db.Daily_acts.Where(t => t.rep_id == ActId &&
                   DbFunctions.TruncateTime(t.date1) == DbFunctions.TruncateTime(DateTime.Now)).FirstOrDefault();
                    if (check != null) { throw new BllException(14, "Наряд уже открыт в сегоднешнем дне."); }

                    var updateDic = db.Repairs_acts.Where(t => t.rep_id == ActId).FirstOrDefault();
                    if (updateDic.status == 3) { throw new BllException(14, "Наряд закрыт."); }
                    updateDic.date_open = DateTime.Now;
                    updateDic.status = 2;
                }
                db.SaveChanges();
                trans.Commit();
            }
            return "success";
        }
        //удалить наряд на ремонт
        public int DeleteRepairs(int req)
        {
            using (var trans = db.Database.BeginTransaction())
            {
                var doc = db.Repairs_acts.Where(s => s.rep_id == req).FirstOrDefault();
                if (doc == null)
                {
                    throw new BllException(15, "Документ не был найден, обновите страницу.");
                }
                var countDailyActs = db.Daily_acts.Where(t => t.rep_id == req).Count();
                if (doc.status == 3) {
                    throw new BllException(15, "НН был закрыт. Удаление невозможно."); 
                }
                if (countDailyActs != 0)
                {
                    throw new BllException(15, "По выбранному НН существуют ЕН. Удаление невозможно.");
                }

                db.Repairs_acts.Remove(doc);
                db.Repairs_Records.RemoveRange(db.Repairs_Records.Where(d => d.rep_rep_id == req));
                db.Defect_Records.RemoveRange(db.Defect_Records.Where(d => d.rep_rep_id == req));
                db.SaveChanges();
                trans.Commit();
            }
            return 0;
        }
        // Антон Отчеты
        public List<SparePartsModel> GetSparePartsBalance(int storage_id)
        {
            List<SparePartsModel> list = new List<SparePartsModel>();
            list = (from s in db.Spare_parts_Current
                    join k1 in db.Spare_parts on s.spare_spare_id equals k1.spare_id into left1
                    from t1 in left1.DefaultIfEmpty()
                    join k2 in db.Storages on s.store_store_id equals k2.store_id into left2
                    from t2 in left2.DefaultIfEmpty()
                    where ((storage_id != 0) ? t2.store_id == storage_id : true)
                    let reserve = (from l in db.Repairs_acts
                                   where l.status != 3
                                   join t in db.Repairs_Records on l.rep_id equals t.rep_rep_id into leftTask
                                   from l1 in leftTask.DefaultIfEmpty()
                                   where l1.spare_spare_id == t1.spare_id && l1.store_store_id == t2.store_id
                                   select
                                   l1.count ?? 0  // резерв
                                ).Sum()
                    group new
                    {
                        DateBegin = s.date
                    }
                        by new
                        {
                            Id = s.sp_id,
                            Name = t1.name,
                            spare_id = t1.spare_id,
                            store_id = t2.store_id,
                            DateBegin = s.date,
                            Price = s.price,
                            Count = s.count,
                            SpareParts = t1.name,
                            VendorCode = t1.vendor_code,
                            Storage = t2.name,
                            Contractors = t1.manufacturer,
                            Reserve = reserve,
                            Storage2 = (float?)Math.Round(s.count - (reserve == null ? 0 : reserve), 3),
                        } into g
                    select new SparePartsModel
                    {
                        Id = g.Key.Id,
                        Name = g.Key.Name,
                        Manufacturer = g.Key.Contractors,
                        DateBegin = g.Key.DateBegin,
                        SpareParts1 = g.Key.SpareParts,
                        Price = (float?)Math.Round(g.Key.Price, 2),
                        Count = g.Key.Count,
                        Storage1 = g.Key.Storage,
                        VendorCode1 = g.Key.VendorCode,
                        Contractor = g.Key.Contractors,
                        Spare_id = g.Key.spare_id,
                        Store_id = g.Key.store_id,
                        Reserve = g.Key.Reserve,
                        Storage2 = g.Key.Storage2,
                    }).ToList();
            return list;
        }

        public List<SparePartsModel> GetSparePartsIntake(int storage_id, DateTime start, DateTime end)
        {
            List<SparePartsModel> list = new List<SparePartsModel>();
            var resultList = new SparePartsListResponse();
            //новая версия
            list = (from s in db.Repairs_Records
                    where storage_id != 0 ? s.store_store_id == storage_id : true
                    join k1 in db.Spare_parts on s.spare_spare_id equals k1.spare_id into left1
                    from t1 in left1.DefaultIfEmpty()
                    join k2 in db.Storages on s.store_store_id equals k2.store_id into left2
                    from t2 in left2.DefaultIfEmpty()
                    join k3 in db.Repairs_acts on s.rep_rep_id equals k3.rep_id into left3
                    from t3 in left3.DefaultIfEmpty()
                    where (DbFunctions.TruncateTime(s.date) >= DbFunctions.TruncateTime(start)
                    && DbFunctions.TruncateTime(s.date) <= DbFunctions.TruncateTime(end)
                    && t3.status == 3)
                    group new
                    {
                        DateBegin = s.date
                    }
                        by new
                        {
                            Id = s.repair_id,
                            DateBegin = s.date,
                            Price = db.Spare_parts_Records.Where(w => w.spss_spss_id == s.rep_rep_id &&
                              w.spare_spare_id == s.spare_spare_id && w.store_from_id == s.store_store_id ).Select(w => w.price).FirstOrDefault(),
                            Count = s.count,
                            Cost = 0,
                            Act_number = db.Repairs_acts.Where(w => w.rep_id == s.rep_rep_id).Select(s1 => s1.number_act).FirstOrDefault(),
                            SpareParts = new DictionaryItemsDTO
                            {
                                Id = t1.spare_id,
                                Name = t1.name,
                            },
                            VendorCode = new DictionaryItemsDTO
                            {
                                Id = t1.spare_id,
                                Name = t1.vendor_code,
                            },
                            Storage = new DictionaryItemsDTO
                            {
                                Id = t2.store_id,
                                Name = t2.name,
                            },
                        } into g
                    select new SparePartsModel
                    {
                        Id = g.Key.Id,
                        Name = g.Key.SpareParts.Name,
                        DateBegin = g.Key.DateBegin,
                        SpareParts = g.Key.SpareParts,
                        Price = (float?)Math.Round(g.Key.Price, 2),
                        Count = g.Key.Count,
                        Cost3 = g.Key.Cost,
                        Storage = g.Key.Storage,
                        Storage1 = g.Key.Storage.Name,
                        VendorCode = g.Key.VendorCode,
                        VendorCode1 = g.Key.VendorCode.Name,
                        Act_number = g.Key.Act_number.ToString()
                    }).OrderByDescending(m => m.DateBegin).ToList();
            for (int l = 0; l < list.Count; l++)
            {
                list[l].DateBegin1 = list[l].DateBegin.Value.ToString("dd.MM.yyyy");
                list[l].Cost3 = (float?)Math.Round((decimal)((list[l].Price ?? 0) * (list[l].Count ?? 0)),2);
            }
            return list;
        }

        //учет рабочего времени ОБЫЧНЫЙ
        public List<DictionaryItemsDTO> GetRepairsSpendingsListResExcel(int emp_id, DateTime start, DateTime end)
        {
            List<DictionaryItemsDTO> result = new List<DictionaryItemsDTO>();
            result = db.Daily_acts.Where(s => DbFunctions.TruncateTime(s.date1) >= DbFunctions.TruncateTime(start)
                                            && DbFunctions.TruncateTime(s.date1) <= DbFunctions.TruncateTime(end) && s.status == 2).Join(
                db.Repairs_Tasks_Spendings,
                d => d.dai_id,
                s => s.dai_dai_id,
                (d, s) => new
                {
                    S_Id = s.sp_id,
                    Emp_id = s.emp_emp_id,
                    Sum = (s.rate_piecework ?? 0) * (s.hours ?? 0) + (s.rate_shift ?? 0)
                })
                .Join(
                db.Employees,
                e => e.Emp_id,
                e1 => e1.emp_id,
                (e, e1) => new
                {
                    S_Id = e.S_Id,
                    Name = new { Id = e1.emp_id, Name = e1.short_name },
                    Price = e.Sum
                }).Where(ee => (emp_id == 0) ? true : ee.Name.Id == emp_id)
                .GroupBy(
                g1 => g1.Name,
                g2 => g2.Price,
                (gg1, gg2) => new
                {
                    Name = gg1,
                    Price = gg2.Sum()
                }).Select(a => new DictionaryItemsDTO
                {
                    Name = a.Name.Name,
                    Price = (float?)a.Price,
                    Id = a.Name.Id
                }).ToList();
            return result;
        }

        //учет рабочего времени РАСШИРЕННЫЙ
        public List<TableDataDaily> GetRepairsSpendingsBigListResExcel(int emp_id, DateTime start, DateTime end)
        {
            List<TableDataDaily> result = new List<TableDataDaily>();
            result = db.Daily_acts.Where(s => DbFunctions.TruncateTime(s.date1) >= DbFunctions.TruncateTime(start)
                                        && DbFunctions.TruncateTime(s.date1) <= DbFunctions.TruncateTime(end) && s.status == 2).Join(
                db.Repairs_Tasks_Spendings,
                d => d.dai_id,
                s => s.dai_dai_id,
                (d, s) => new
                {
                    S_Id = s.sp_id,
                    Dai_id = s.dai_dai_id,
                    Emp_id = s.emp_emp_id,
                    Number = d.number_dai_act,
                    Sum = (s.rate_piecework ?? 0) * (s.hours ?? 0) + (s.rate_shift ?? 0),
                    Task = db.Type_tasks.Where(tpt => tpt.tptas_id == s.tptas_tptas_id).Select(tpt1 => tpt1.name).FirstOrDefault(),
                    Date1 = s.date1,
                    Hours = (s.hours ?? 0),
                    TaskId = db.Type_tasks.Where(tpt => tpt.tptas_id == s.tptas_tptas_id).Select(tpt1 => tpt1.tptas_id).FirstOrDefault(),
                    RatePiecework = s.rate_piecework, //расценка
                    RateShift = s.rate_shift,
                    Trakt = new DictionaryItemsDTO
                    {
                        Id = db.Repairs_acts.Where(i => i.rep_id == db.Daily_acts.Where(d1 => d1.dai_id == s.dai_dai_id).Select(s1 => s1.rep_id).FirstOrDefault()).Select(
                                s2 => s2.trakt_trakt_id).FirstOrDefault(),
                        Name = db.Traktors.Where(y => y.trakt_id == db.Repairs_acts.Where(
                                i => i.rep_id == db.Daily_acts.Where(
                                d1 => d1.dai_id == s.dai_dai_id).Select(s1 => s1.rep_id).FirstOrDefault()).Select(
                                s2 => s2.trakt_trakt_id).FirstOrDefault()).Select(tr => tr.name + " (" + tr.reg_num + ")").FirstOrDefault().ToString()
                    },
                    Agr = new DictionaryItemsDTO
                    {
                        Id = db.Repairs_acts.Where(i => i.rep_id == db.Daily_acts.Where(d1 => d1.dai_id == s.dai_dai_id).Select(s1 => s1.rep_id).FirstOrDefault()).Select(
                                s2 => s2.equi_equi_id).FirstOrDefault(),
                        Name = db.Equipments.Where(y => y.equi_id == db.Repairs_acts.Where(i => i.rep_id == db.Daily_acts.Where(
                                d1 => d1.dai_id == s.dai_dai_id).Select(s1 => s1.rep_id).FirstOrDefault()).Select(
                                s2 => s2.equi_equi_id).FirstOrDefault()).Select(eq => eq.name).FirstOrDefault().ToString()
                    }
                }).Join(
                db.Employees,
                e => e.Emp_id,
                e1 => e1.emp_id,
                (e, e1) => new
                {
                    S_Id = e.S_Id,
                    Dai_id = e.Dai_id,
                    Name = new { Id = e1.emp_id, Name = e1.short_name },
                    Number = e.Number,
                    Price = e.Sum,
                    Date1 = e.Date1,
                    Task = e.Task,
                    TaskId = e.TaskId,
                    Trakt = e.Trakt.Name,
                    TraktId = e.Trakt.Id,
                    Hours = e.Hours,
                    Agr = e.Agr.Name,
                    AgrId = e.Agr.Id,
                    RatePieceWork = e.RatePiecework,
                }).Where(ee => (emp_id == 0) ? true : ee.Name.Id == emp_id).Select(a => new TableDataDaily
                {
                    Id = a.S_Id,
                    Date1 = a.Date1,
                    ActNumber = a.Number,
                    EmpName = a.Name.Name,

                    OtvName = db.Employees.Where(t3 => t3.emp_id ==
                     db.Repairs_acts.Where(t1 => t1.rep_id == db.Daily_acts.Where(t => t.dai_id == a.Dai_id)
                   .Select(t => t.rep_id).FirstOrDefault()).Select(t1 => t1.emp_emp_id).FirstOrDefault()
                   ).Select(t3 => t3.short_name).FirstOrDefault(),


                    EmpId = a.Name.Id,
                    SumCost = a.Price,
                    TaskName = a.Task,
                    TaskId = a.TaskId,
                    Trakt = a.Trakt,
                    Agr = a.Agr,
                    Hours = a.Hours,
                    TraktId = a.TraktId,
                    AgrId = a.AgrId,
                    RatePiecework = a.RatePieceWork,
                }).ToList();

           // for (int i = 0; i < result.Count; i++)
           // {
           //     var d1 = db.Daily_acts.Where(t => t.dai_id == result[i].Id).Select(t => t.rep_id).FirstOrDefault();
           //     var d2 = db.Repairs_acts.Where(t1 => t1.rep_id == d1).Select(t => t.emp_emp_id).FirstOrDefault();
           ////     var d3 = 
            
           // }


                return result;
        }

        // затраты на ремонт
        public List<SparePartsModel> GetSparePartsRepairsSpendings(int tech_id, int equi_id, DateTime start, DateTime end)
        {
            // условие либо техника, либо оборудование, либо все сразу
            // если id = -1, то все, если 0 - ничего
            List<SparePartsModel> list = new List<SparePartsModel>();

            if (tech_id != 0 || equi_id != 0)
            {
                list = (from s in db.Repairs_acts
                        where (DbFunctions.TruncateTime(s.date_start) >= DbFunctions.TruncateTime(start)
                               && DbFunctions.TruncateTime(s.date_end) <= DbFunctions.TruncateTime(end)
                               &&
                               (
                                   (
                                        (tech_id != 0 && tech_id != null) ?
                                        (
                                            (tech_id != -1) ?
                                                s.trakt_trakt_id == tech_id
                                            :
                                                (s.trakt_trakt_id != null && s.trakt_trakt_id != 0)
                                        )
                                        :
                                        false
                                   )
                                   ||
                                   (
                                        (equi_id != 0 && equi_id != null) ?
                                        (
                                            (equi_id != -1) ?
                                                s.equi_equi_id == equi_id
                                            :
                                                (s.equi_equi_id != null && s.equi_equi_id != 0)
                                        )
                                        :
                                            false
                                   )
                               )
                               )
                        join k1 in db.Spare_parts_Records on s.rep_id equals k1.spss_spss_id
                        where k1.store_from_id != null && k1.store_from_id != 0 && (k1.store_to_id == null || k1.store_to_id == 0)
                        join k2 in db.Spare_parts on k1.spare_spare_id equals k2.spare_id into left1
                        from t1 in left1.DefaultIfEmpty()
                        group new
                        {
                            DateBegin = s.date_start
                        }
                            by new
                            {
                                Id = k1.sps_id,
                                Date = k1.date,
                                TechName = ((s.trakt_trakt_id != 0 && s.trakt_trakt_id != null) ? db.Traktors.Where(w => w.trakt_id == s.trakt_trakt_id).Select(se => se.name + " (" + se.reg_num + ")").FirstOrDefault()
                                                    + ((s.equi_equi_id != 0 && s.equi_equi_id != null) ? " / " : "") : "")
                                         + ((s.equi_equi_id != 0 && s.equi_equi_id != null) ? db.Equipments.Where(w => w.equi_id == s.equi_equi_id).Select(se => se.name).FirstOrDefault() : ""),
                                Price = Math.Round(k1.price, 2),
                                Count = k1.count,
                                Cost = Math.Round(k1.price * k1.count, 2),
                                SpareParts = new DictionaryItemsDTO
                                {
                                    Id = t1.spare_id,
                                    Name = t1.name
                                },
                                VendorCode = new DictionaryItemsDTO
                                {
                                    Id = t1.spare_id,
                                    Name = t1.vendor_code
                                }
                            } into g
                        select new SparePartsModel
                        {
                            Id = g.Key.Id,
                            DateBegin = g.Key.Date,
                            Name = g.Key.SpareParts.Name,
                            TechName = g.Key.TechName,
                            SpareParts = g.Key.SpareParts,
                            Price = (float?)g.Key.Price,
                            Count = g.Key.Count,
                            Cost3 = g.Key.Cost,
                            VendorCode = g.Key.VendorCode,
                            VendorCode1 = g.Key.VendorCode.Name
                        }).OrderByDescending(m => m.DateBegin).ToList();
            }

            return list;
        }

        //РЕЕСТР ДОКУМЕНТОВ - реестр приходных накладных
        public List<SparePartsItem> GetInvoiceActsRegistry(DateTime start, DateTime end)
        {
            var res = new List<SparePartsItem>();
            res = (from t in db.Spare_parts_Acts
                   where (DbFunctions.TruncateTime(t.date) >= DbFunctions.TruncateTime(start)
                                          && DbFunctions.TruncateTime(t.date) <= DbFunctions.TruncateTime(end))
                   select new SparePartsItem
                   {
                       Id = t.spss_id,
                       DateBegin1 = t.date,
                       Number = t.act_number,
                       Contractor = new DictionaryItemsDTO { Id = t.contr_id, Name = db.Contractor.Where(v => v.id == t.contr_id).Select(v => v.name).FirstOrDefault() },
                       Cost = db.Spare_parts_Records.Where(s => s.store_from_id == 0 && s.store_to_id != 0 &&
                       s.spss_spss_id == t.spss_id).Sum(s => s.price * s.count) // ?? 0
                   }).ToList();
            for (int i = 0; i < res.Count; i++)
            {
                res[i].DateBegin = res[i].DateBegin1.ToString("dd.MM.yyyy");
            }
            return res;
        }

        //РЕЕСТР ДОКУМЕНТОВ - реестр расходных накладных
        public List<SparePartsModel> GetSpendingActsRegistry(DateTime start, DateTime end)
        {
            List<SparePartsModel> list = new List<SparePartsModel>();

            list = db.Repairs_acts.Where(w => DbFunctions.TruncateTime(w.date_end) >= DbFunctions.TruncateTime(start)
                                           && DbFunctions.TruncateTime(w.date_end) <= DbFunctions.TruncateTime(end) && 
                                           w.status == 3
                                        ).Join(db.Spare_parts_Records.Where(t=>t.store_to_id == 0 && t.store_from_id != 0),
                                               a => a.rep_id,
                                               r => r.spss_spss_id,
                                               (a, r) => new
                                               {
                                                   Info = new
                                                   {
                                                       Date = a.date_start,
                                                       Number = a.number_act,
                                                       TechName = ((a.trakt_trakt_id != 0 && a.trakt_trakt_id != null) ? db.Traktors.Where(w => w.trakt_id == a.trakt_trakt_id).Select(se => se.name + " (" + se.reg_num + ")").FirstOrDefault()
                                                       + ((a.equi_equi_id != 0 && a.equi_equi_id != null) ? " / " : "") : "")
                                                    + ((a.equi_equi_id != 0 && a.equi_equi_id != null) ? db.Equipments.Where(w => w.equi_id == a.equi_equi_id).Select(se => se.name).FirstOrDefault() : ""),

                                                   },
                                                   Sum = (r.price != null && r.count != null) ? Math.Round(r.price * r.count, 2) : 0
                                               }
                                              ).GroupBy(g1 => g1.Info,
                                                          g2 => g2.Sum,
                                                          (gg1, gg2) => new
                                                          {
                                                              Info = gg1,
                                                              Cost = gg2.Sum()
                                                          }
                                                       ).Select(s => new SparePartsModel
                                                       {
                                                           DateBegin = s.Info.Date,
                                                           TechName = s.Info.TechName,
                                                           Cost3 = s.Cost,
                                                           Act_number = s.Info.Number
                                                       }).OrderByDescending(m => m.DateBegin).ToList();
            return list;
        }

        //РЕЕСТР ДОКУМЕНТОВ - реестр нарядов
        public List<RepairsItem> GetRepairsActsRegistry(DateTime start, DateTime end)
        {
            var res = new List<RepairsItem>();

            res = db.Repairs_acts.Join(
                db.Daily_acts.Where(s => DbFunctions.TruncateTime(s.date1) >= DbFunctions.TruncateTime(start)
                       && DbFunctions.TruncateTime(s.date1) <= DbFunctions.TruncateTime(end)),
                d => d.rep_id,
                s => s.rep_id,
                (d, s) => new
                {
                    rep_id = d.rep_id,
                    dai_id = s.dai_id,
                    DateStart = d.date_start,
                    DateEnd = d.date_end,
                    DateOpen = d.date_open,
                    Number = d.number_act,
                    CarsName = db.Traktors.Where(v => v.trakt_id == d.trakt_trakt_id).Select(v => v.name + " (" + v.reg_num + ") ").FirstOrDefault(),
                    EqName = db.Equipments.Where(l => l.equi_id == d.equi_equi_id).Select(l => l.name).FirstOrDefault(),
                    Status = d.status == 1 ? "Создан" : d.status == 2 ? "Открыт" : "Закрыт"
                }
                ).Join(
                db.Repairs_Tasks_Spendings,
                d => d.dai_id,
                s => s.dai_dai_id,
                (d, s) => new
                {
                    Info = new
                    {
                        rep_id = d.rep_id,
                        DateStart = d.DateStart,
                        DateEnd = d.DateEnd,
                        DateOpen = d.DateOpen,
                        Number = d.Number,
                        CarsName = d.CarsName + " / " + d.EqName,
                        Status = d.Status
                    },
                    Sum =  ((s.rate_piecework ?? 0) * (s.hours ?? 0)) + (s.rate_shift ?? 0)
                }).GroupBy(
                g1 => g1.Info,
                g2 => g2.Sum,
                (gg1, gg2) => new
                {
                    Info = gg1,
                    Cost = gg2.Sum()
                }).Select(a => new RepairsItem
                {
                    Number = a.Info.Number,
                    CarsName = a.Info.CarsName,
                    Cost = (float)a.Cost ,
                    DateBegin1 = a.Info.DateStart,
                    DateEnd1 = a.Info.DateEnd,
                    DateOpen1 = a.Info.DateOpen,
                    Status = a.Info.Status
                }).ToList();
            for (int i = 0; i < res.Count; i++)
            {
                res[i].DateBegin = res[i].DateBegin1.ToString("dd.MM.yyyy");
                res[i].DateEnd =  res[i].DateEnd1.HasValue ? res[i].DateEnd1.Value.ToString("dd.MM.yyyy") : null;
                res[i].DateOpen = res[i].DateOpen1.Value.ToString("dd.MM.yyyy");
            }
            var res1 = res.Where(t => t.Status.Equals("Создан")).ToList();
            var res2 = res.Where(t => t.Status.Equals("Открыт")).ToList();
            var res3 = res.Where(t => t.Status.Equals("Закрыт")).ToList();
            res.Clear(); res.AddRange(res3); res.AddRange(res2); res.AddRange(res1);
            return res;
        }
      
        public PrintRepairsModel GetPrintRepairsInfo(int req)
        {
            PrintRepairsModel result = new PrintRepairsModel();
            result.query1 = (from ra in db.Repairs_acts
                             where ra.rep_id == req
                             join trak in db.Traktors on ra.trakt_trakt_id equals trak.trakt_id into trakLj
                             from t in trakLj.DefaultIfEmpty()
                             join eq in db.Equipments on ra.equi_equi_id equals eq.equi_id into eqLj
                             from e in eqLj.DefaultIfEmpty()
                             join emp in db.Employees on ra.emp_emp_id equals emp.emp_id into empLj
                             from em in empLj.DefaultIfEmpty()
                             join dr in db.Defect_Records on ra.rep_id equals dr.rep_rep_id into drLj
                             from d in drLj.DefaultIfEmpty()

                             select new PrintRepairsDTO1
                             {
                                 NumberAct = ra.number_act,
                                 CarsName = t.name,
                                 EqName = e.name,
                                 ReqNum = t.reg_num,
                                 MotoHours = ra.moto,
                                 Mileage = ra.mileage,
                                 Employee = em.second_name + " " + em.first_name + " " + em.middle_name,
                                 RepairsType = (int)ra.type_repairs - 1,
                             }).ToList();

            result.query1[0].RepairsTypeMark = new string[4];
            result.query1[0].RepairsTypeMark[0] = "";
            result.query1[0].RepairsTypeMark[1] = "";
            result.query1[0].RepairsTypeMark[2] = "";
            result.query1[0].RepairsTypeMark[3] = "";
            if (result.query1[0].RepairsType != -1)
            {
                result.query1[0].RepairsTypeMark[result.query1[0].RepairsType] = "V";
            }
            result.query2 = (from dr in db.Defect_Records
                             where dr.defect_type == 1 && dr.rep_rep_id == req

                             select new PrintRepairsDTO2
                             {
                                 DefectName = dr.defect,
                             }).ToList();
            int i = 1;
            foreach (PrintRepairsDTO2 t in result.query2)
            {
                t.DefectId = i++;

            }
            result.query3 = (from dr in db.Defect_Records
                             where dr.defect_type == 2 && dr.rep_rep_id == req
                             join tas in db.Type_tasks on dr.tptas_tptas_id equals tas.tptas_id into tasLj
                             from t in tasLj.DefaultIfEmpty()

                             select new PrintRepairsDTO3
                             {
                                 DefectName = dr.defect,
                                 TaskName = t.name,
                             }).ToList();
            int i1 = 1;
            foreach (PrintRepairsDTO3 t in result.query3)
            {
                t.DefectId = i1++;
            }

            result.query4 = (from rr in db.Repairs_Records
                             where rr.rep_rep_id == req
                             join sp in db.Spare_parts on rr.spare_spare_id equals sp.spare_id into spLj
                             from s in spLj.DefaultIfEmpty()

                             select new PrintRepairsDTO4
                             {
                                 SpareName = s.name,
                                 Count = (int)rr.count,
                             }).ToList();
            int i2 = 1;
            foreach (PrintRepairsDTO4 t in result.query4)
            {
                t.SpareId = i2++;
            }

            return result;
        }

        //требование-накладная
        public WriteOffActsDetails GetPrintDemandInfo(int? req, string act_number, DateTime date)
        {
            var data = new WriteOffActsDetails();
            var param = new SparePartsDetails();
            param.ActId = req;
            param.Date = date.Date;
            data = GetDemandInvoiceDetails(param);
            data.Act_number = act_number;
            data.Date = date.Date;
            data.ShortName = ((data.CarsName != null) ? data.CarsName + " " + data.CarNum +
                     ((data.CarsEquip != null) ? "  " + data.CarsEquip : "") : ((data.CarsEquip != null) ? data.CarsEquip : ""));
            data.SumCost = data.SparePartsList.Sum(t => t.Cost1);
            data.SumCount = data.SparePartsList.Sum(t => t.Count);
            data.SumCost = (double) Math.Round((decimal)data.SumCost,2);
            data.SumCount = (double)Math.Round((decimal)data.SumCount, 2);
            var idOrg = Int32.Parse(db.Parameters.Where(t => t.name.Equals("NameOrg")).Select(t => t.value).FirstOrDefault());
            data.OrgName = db.Organizations.Where(t => t.org_id == idOrg).Select(t => t.name).FirstOrDefault();
            return data;
        }


        public ReportFile GetPrintWriteoffInfo(int req, string pathTemplate, int isReq, DateTime? date, string act_number)
        {
            // номер и дата дефектной ведомости
            ReportFile result = new ReportFile();
            var data = new WriteOffActsDetails();

            if (!act_number.Equals("MoveAct"))
            {
                if (date == null)
                {
                    data = (from ra in db.Repairs_acts
                            where ra.rep_id == req && ra.status == 3
                            join trak in db.Traktors on ra.trakt_trakt_id equals trak.trakt_id into trakLj
                            from t in trakLj.DefaultIfEmpty()
                            join eq in db.Equipments on ra.equi_equi_id equals eq.equi_id into eqLj
                            from e in eqLj.DefaultIfEmpty()
                            join emp in db.Employees on ra.emp_emp_id equals emp.emp_id into empLj
                            from em in empLj.DefaultIfEmpty()
                            join dr in db.Defect_Records on ra.rep_id equals dr.rep_rep_id into drLj
                            from d in drLj.DefaultIfEmpty()

                            select new WriteOffActsDetails
                            {
                                Act_number = ra.number_act,
                                CarsName = t.name,
                                CarsEquip = e.name,
                                CarsNum = t.reg_num,
                                CarsModel = t.model,
                                OtvName = em.second_name + " " + em.first_name + " " + em.middle_name,
                                ShortName = em.short_name,
                                Date = ra.date_end,
                                NodeRepairs = db.Modules.Where(w => w.mod_id == ra.mod_mod_id).Select(s => s.name).FirstOrDefault(),

                                SumCount = Math.Round((double)db.Spare_parts_Records.Where(w => w.spss_spss_id == req && w.store_from_id != 0 && w.store_to_id == 0).Select(s => Math.Round(s.count, 3)).ToList().Sum(), 3),
                                SumCost = Math.Round((double)db.Spare_parts_Records.Where(w => w.spss_spss_id == req && w.store_from_id != 0 && w.store_to_id == 0).Select(s => Math.Round((double)(s.count * s.price), 2)).ToList().Sum(), 2),

                                SparePartsList = (from rr in db.Spare_parts_Records
                                                  where rr.spss_spss_id == req && rr.store_from_id != 0 && rr.store_to_id == 0
                                                  join sp in db.Spare_parts on rr.spare_spare_id equals sp.spare_id into spLj
                                                  from s in spLj.DefaultIfEmpty()

                                                  select new SparePartsModel
                                                  {
                                                      Name = s.name,
                                                      Count = rr.count,
                                                      Price = rr.price,
                                                      Cost1 = Math.Round((double)(rr.count * rr.price), 2),
                                                      TechName = t.model + " " + t.reg_num,
                                                      NodeRepairs = db.Modules.Where(w => w.mod_id == ra.mod_mod_id).Select(se => se.name).FirstOrDefault(),
                                                  }).ToList()

                            }).FirstOrDefault();

                }
                else
                {
                    var param = new SparePartsDetails();
                    param.ActId = req;
                    param.Date = date.Value.Date;
                    data = GetDemandInvoiceDetails(param);
                    data.Act_number = act_number;
                    data.Date = date.Value.Date;
                }
            }
                //перемещение
            else {

                var movedata = tmcDb.GetActTmcMoveDetails(req);
                data.SparePartsList = movedata.SparePartsList;
                data.Date = movedata.Date;
                data.Act_number = movedata.Number;
                data.ShortName = db.Employees.Where(t => t.emp_id == movedata.ContractorsId).Select(t => t.short_name).FirstOrDefault();
                data.OtvName = db.Employees.Where(t => t.emp_id == movedata.StorageId).Select(t => t.short_name).FirstOrDefault();

            }

            int tmp = 1;
            data.SparePartsList.ForEach(it => it.Id = tmp++);
            var tmpl = new FileInfo(pathTemplate);
            using (ExcelPackage pck = new ExcelPackage(tmpl, true))
            {
                ExcelWorksheet wsh = pck.Workbook.Worksheets[1];
                wsh.Name = "Форма акта списания запчастей";
                //M-11
                  var idOrg = Int32.Parse(db.Parameters.Where(t => t.name.Equals("NameOrg")).Select(t => t.value).FirstOrDefault());
                  var orgName = db.Organizations.Where(t => t.org_id == idOrg).Select(t => t.name).FirstOrDefault();
                if (isReq == 1)
                {
                    wsh.Cells["B4"].Value = orgName; 
                    uint t = 16;
                    wsh.Cells["B2"].Value = "ТРЕБОВАНИЕ-НАКЛАДНАЯ № " + data.Act_number;
                    wsh.Cells["b8"].Value = data.Date.Value.ToString("dd.MM.yyyy");

                    if (!act_number.Equals("MoveAct"))
                    {
                        wsh.Cells["g8"].Value = ((data.CarsModel != null) ? data.CarsModel + " " + data.CarsNum +
                       ((data.CarsEquip != null) ? " / " + data.CarsEquip : "") : ((data.CarsEquip != null) ? data.CarsEquip : ""));

                        if (date != null)
                        {
                           wsh.Cells["g8"].Value = ((data.CarsName != null) ? data.CarsName + " " + data.CarNum +
                         ((data.CarsEquip != null) ? "  " + data.CarsEquip : "") : ((data.CarsEquip != null) ? data.CarsEquip : ""));
                        }
                    }
                        //movement
                    else {
                        wsh.Cells["g8"].Value = data.ShortName;
                        wsh.Cells["d8"].Value = data.OtvName;
                    }

                    for (int i = 0; i < data.SparePartsList.Count; i++)
                    {
                        if (!act_number.Equals("MoveAct"))
                        {
                            wsh.Cells["d" + t.ToString()].Value = data.SparePartsList[i].Name;
                        }

                        wsh.Cells["i" + t.ToString()].Value = "796";
                        wsh.Cells["j" + t.ToString()].Value = "шт";
                        wsh.Cells["j" + t.ToString()].Value = data.SparePartsList[i].Count;
                        wsh.Cells["r" + t.ToString()].Value = data.SparePartsList[i].Count;
                        wsh.Cells["z" + t.ToString()].Value = data.SparePartsList[i].Price;
                        wsh.Cells["ai" + t.ToString()].Value = data.SparePartsList[i].Price * data.SparePartsList[i].Count;
                        if (date != null || act_number.Equals("MoveAct"))
                        {
                            wsh.Cells["d" + t.ToString()].Value = data.SparePartsList[i].SpareParts.Name;
                        }
                        t++;
                    }
                    wsh.Cells["j" + t.ToString()].Value = data.SparePartsList.Sum(с=> (decimal)(с.Count ?? 0));
                    wsh.Cells["r" + t.ToString()].Value = data.SparePartsList.Sum(с => (decimal)(с.Count ?? 0));
                    wsh.Cells["ai" + t.ToString()].Value = data.SparePartsList.Sum(с => (decimal)(с.Count ?? 0) * (decimal)(с.Price ?? 0));

                    wsh.Cells["j" + t.ToString() + ":t" + t.ToString()].Style.Font.Bold = true;
                    wsh.Cells["ak" + t.ToString()].Style.Font.Bold = true;

                    setBorder(wsh.Cells["j" + t.ToString() + ":y" + t.ToString()].Style.Border);
                    setBorder(wsh.Cells["ai" + t.ToString() + ":ap" + t.ToString()].Style.Border);

                    setBorder(wsh.Cells["B16:AW" + (t - 1).ToString()].Style.Border);
                    t=t+2;
                    wsh.Cells["B" + t.ToString()].Value = "Отпустил";
                    wsh.Cells["c" + (t + 1).ToString()].Value = "должность";
                    setBorderThinTop(wsh.Cells["c" + (t + 1).ToString()].Style.Border);

                    wsh.Cells["d" + (t + 1).ToString()].Value = "подпись";
                    setBorderThinTop(wsh.Cells["d" + (t + 1).ToString() + ":f" + (t + 1).ToString()].Style.Border);
                    wsh.Cells["d" + (t + 1).ToString()].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;


                    wsh.Cells["f" + (t + 1).ToString()].Value = "расшифровка подписи";
                    setBorderThinTop(wsh.Cells["g" + (t + 1).ToString() + ":i" + (t + 1).ToString()].Style.Border);

                    wsh.Cells["j" + t.ToString()].Value = "Получил";
               //     setBorderThinTop(wsh.Cells["i" + (t + 1).ToString() + ":k" + (t + 1).ToString()].Style.Border);

                    wsh.Cells["r" + (t + 1).ToString()].Value = "должность";
                    setBorderThinTop(wsh.Cells["l" + (t + 1).ToString() + ":aa" + (t + 1).ToString()].Style.Border);

                    wsh.Cells["z" + (t + 1).ToString()].Value = "подпись";
                    setBorderThinTop(wsh.Cells["t" + (t + 1).ToString() + ":aa" + (t + 1).ToString()].Style.Border);
                    wsh.Cells["t" + (t + 1).ToString() + ":aa" + (t + 1).ToString()].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;


                    wsh.Cells["ai" + (t + 1).ToString()].Value = "расшифровка подписи";
                    setBorderThinTop(wsh.Cells["ab" + (t + 1).ToString() + ":ap" + (t + 1).ToString()].Style.Border);
                    wsh.Cells["ab" + t.ToString()].Value = data.ShortName;

                }

                //обычный акт списания
                if (isReq == 0)
                {
                    //реестр приходных накладных
                  
                    uint k = 13;
                    double sum = 0;
                    double count = 0;
                    ExcelFill template = wsh.Row(13).Style.Fill;

                    wsh.Cells["b2"].Value = orgName;
                    wsh.Cells["b3"].Value = db.Organizations.Where(t => t.org_id == idOrg).Select(t => t.address).FirstOrDefault();
                    wsh.Cells["B5"].Value = "№ ВОЛА-" + data.Act_number + " от " + data.Date.Value.ToString("dd.MM.yyyy") + " г.";
                    wsh.Cells["B11"].Value = "Марка и номер транспортного средства: " + ((data.CarsModel != null) ? data.CarsModel + " " + data.CarsNum +
                        ((data.CarsEquip != null) ? " / " + data.CarsEquip : "") : ((data.CarsEquip != null) ? data.CarsEquip : ""));

                    for (int i = 0; i < data.SparePartsList.Count; i++)
                    {
                        wsh.InsertRow((int)k, 1);
                        wsh.Row((int)k).Height = 33;
                        wsh.Cells["B" + k.ToString()].Value = data.SparePartsList[i].Id;
                        wsh.Cells["C" + k.ToString()].Value = data.SparePartsList[i].Name;
                        wsh.Cells["D" + k.ToString()].Value = "";
                        wsh.Cells["E" + k.ToString()].Value = data.SparePartsList[i].Count ?? 0;
                        wsh.Cells["F" + k.ToString()].Style.Numberformat.Format = "#,##0.00";
                        wsh.Cells["G" + k.ToString()].Style.Numberformat.Format = "#,##0.00";
                        wsh.Cells["G" + k.ToString()].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        wsh.Cells["F" + k.ToString()].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        wsh.Cells["E" + k.ToString()].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        wsh.Cells["F" + k.ToString()].Value = data.SparePartsList[i].Price != null ? Math.Round(data.SparePartsList[i].Price.Value, 2) : 0;
                        wsh.Cells["G" + k.ToString()].Value = data.SparePartsList[i].Cost1 ?? 0;
                        wsh.Cells["H" + k.ToString()].Value = data.SparePartsList[i].NodeRepairs;
                        sum += data.SparePartsList[i].Cost1 ?? 0;
                        count += data.SparePartsList[i].Count ?? 0;
                        k++;
                    }
                    wsh.Cells["B13:K" + (k - 1).ToString()].Style.Fill = template;

                    if (data.SparePartsList.Count > 0)
                        setBorder(wsh.Cells["B13:J" + (k - 1).ToString()].Style.Border);

                    wsh.Cells["B" + (k + 2).ToString()].Value = "Общее количество: " + Math.Round(count, 3).ToString() + " единиц, на сумму: " + Math.Round(sum, 2).ToString() + " руб.";
                }

                result = new ReportFile
                {
                    data = pck.GetAsByteArray(),
                    fileName = "Форма акта списания запчастей.xlsx"
                };
            }
            return result;
        }

        private void setBorder(Border modelTable)
        {
            modelTable.Top.Style = ExcelBorderStyle.Thin;
            modelTable.Left.Style = ExcelBorderStyle.Thin;
            modelTable.Right.Style = ExcelBorderStyle.Thin;
            modelTable.Bottom.Style = ExcelBorderStyle.Thin;
        }
        private void setBorderTopAndBottom(Border modelTable)
        {
            modelTable.Top.Style = ExcelBorderStyle.Thin;
            modelTable.Bottom.Style = ExcelBorderStyle.Thin;
        }
        private void setBorderBoldTop(Border modelTable)
        {
            modelTable.Top.Style = ExcelBorderStyle.Medium;
        }
        private void setBorderThinTop(Border modelTable)
        {
            modelTable.Top.Style = ExcelBorderStyle.Thin;
        }
        private void setBorderRight(Border modelTable)
        {
            modelTable.Right.Style = ExcelBorderStyle.Thin;
        }
        private void setColor(ExcelWorksheet wsht, string rangePath, string color)
        {
            using (ExcelRange rng = wsht.Cells[rangePath])
            {
                Color colFromHex = System.Drawing.ColorTranslator.FromHtml(color);
                SetCellStyle(rng, colFromHex);
            }
        }
        private void SetCellStyle(ExcelRange cell, Color color)
        {
            cell.Style.Fill.PatternType = ExcelFillStyle.Solid;
            cell.Style.Fill.BackgroundColor.SetColor(color);
        }


    }

}
