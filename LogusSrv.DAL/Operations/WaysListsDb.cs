﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LogusSrv.DAL.DBModel;
using LogusSrv.DAL.Entities.Res;
using LogusSrv.DAL.Entities.Req;
using LogusSrv.DAL.Entities.DTO;
using LogusSrv.DAL.Enums;
using LogusSrv.DAL.Utils;
using System.Data.Entity.Spatial;
using LogusSrv.CORE.Exceptions;
using System.Globalization;
using LogusSrv.DAL.Entities.Req.CloseWaysList;
using LogusSrv.DAL.Entities.DTO.WaysList;
using System.Data.Entity;

namespace LogusSrv.DAL.Operations
{
    public class WaysListsDb : WayListBase
    {
        public WaysListForCreateModel GetInfoForCreateWaysList(GetWaysListsReq req)
        {
            return GetInfoForCreateWaysList(1, req.OrgId, req.WaysListId, req.TaskId, req.DayTaskId);
        }

        public List<WaysListsDTO> GetWaysLists(GetWaysListsReq req)
        {
            var orgId = req.TporgId == 1 ? req.OrgId : null;

            GetWayListModel waysList = new GetWayListModel();
            var list = (from s in db.Sheet_tracks
                        where s.shtr_type_id == 1 // Путевой лист механизатора
                        && (orgId != null ? s.org_org_id == orgId : true) &&
                   (!String.IsNullOrEmpty(req.RegNum) ? (s.Traktors.reg_num.Contains(req.RegNum))  :true) &&
                 ((DbFunctions.TruncateTime(s.date_begin) >= (DbFunctions.TruncateTime(req.DateFilter)))
                    && (DbFunctions.TruncateTime(s.date_begin) <= DbFunctions.TruncateTime(req.DateFilterEnd) ))
                        select new WaysListsDTO
                        {
                            WaysListId = s.shtr_id,
                            WaysListNum = s.num_sheet,
                            Date = s.date_begin,
                            FioDriver = s.Employees.short_name,
                            CarsNum = s.Traktors.reg_num,
                            CarsName = s.Traktors.name,
                            NameEq = s.equi_equi_id != null ? s.Equipments.name : "нет данных",
                            Status = (decimal)s.status,
                            ShiftNum = s.date_begin.Hour < s.date_end.Hour ? 1 : 2,
                            IsRepair = s.is_repair == null ? false : s.is_repair,
                        }).OrderBy(l => l.ShiftNum).ThenBy(l => l.WaysListNum).OrderBy(l=>l.Date).ToList();

            foreach (var elm in list)
            {
                elm.DateBegin = elm.Date.ToString("dd.MM.yyyy");
            }
            return list;
        }

        public int CreateWaysList(CreateWaysListReq req)
        {
            //TODO checkNum
            req.WayListNum = checkWayListNum(req.WayListNum, req.IsRepair, null, 1);
            using (var trans = db.Database.BeginTransaction())
            {

                Sheet_tracks waysList = new Sheet_tracks();
                waysList.shtr_id = CommonFunction.GetNextId(db, "Sheet_tracks");
                waysList.status = (int)WayListStatus.Issued;
                MapWaysListFromDTO(waysList, req);
                db.Sheet_tracks.Add(waysList);

                var taskId = CommonFunction.GetNextId(db, "Tasks");
                Tasks task = new Tasks();
                task.tas_id = taskId;
                task.shtr_shtr_id = waysList.shtr_id;
                task.task_num = 1;
                MapTaskFromDTO(task, req);
                db.Tasks.Add(task);

                db.SaveChanges();
                trans.Commit();
                return waysList.shtr_id;
            }
        }

        

        public CreateWaysListReq GetWaysListById(GetWaysReq req)
        {
            var firstTask = db.Tasks.Where(t => t.shtr_shtr_id == req.WaysListId).ToList();
            if (firstTask.Any())
            {
                var num = firstTask.Min(t => t.task_num);
                if (num != 1)
                {
                    using (var trans = db.Database.BeginTransaction())
                    {
                        var task = firstTask.Where(t => t.task_num == num).FirstOrDefault();
                        task.task_num = 1;
                        db.SaveChanges();
                        trans.Commit();
                    }
                }
            }
            var resultList = (from s in db.Sheet_tracks
                              where s.shtr_type_id == 1 // Путевой лист механизатора
                              join t in db.Tasks.Where(t => t.task_num == 1) on s.shtr_id equals t.shtr_shtr_id into leftTask
                              from tas in leftTask.DefaultIfEmpty()
                              where s.shtr_id == req.WaysListId
                              select new CreateWaysListReq
                                {
                                    WaysListId = s.shtr_id,
                                    OrgId = s.org_org_id,
                                    TraktorId = s.trakt_trakt_id,
                                    WayListNum = s.num_sheet,
                                    DateBegin = s.date_begin,
                                    DateEnd = s.date_end,
                                    DriverId = s.emp_emp_id,
                                    TrailerId = s.equi_equi_id,
                                    UserId = (int)s.user_user_id,
                                    TaskId = tas.tas_id,
                                    AvailableId = tas.emp_emp_id,
                                    TaskTypeId = tas.tptas_tptas_id,
                                    SatusId = tas.status,
                                    FieldId = (int)tas.fiel_fiel_id,
                                    PlantId = tas.plant_plant_id,
                                    Comment = !string.IsNullOrEmpty(tas.description) ? tas.description : null,
                                    IsRepair = s.is_repair,
                                    EquipWidth = tas.width_equip,
                                    TypeFuel = s.id_type_fuel.Value,
                                    AgroreqId = s.id_agroreq,
                                    AllResource  = s.exported == 1 ? true : false,
                                }).FirstOrDefault();

            if (resultList != null)
            {
                resultList.timeStart = resultList.DateBegin.Value.ToString("HH:mm");
                resultList.timeEnd = resultList.DateEnd.Value.ToString("HH:mm");
            }
            return resultList;
        }

        public bool? checkAllResource(CreateWaysListReq list, bool traktors, List<int?> employees, bool equipment, bool fieldAndPlant, bool field)
        {
            var checkOrg = db.Organizations.Where(t => t.org_id == list.OrgId).Select(t => t.tporg_tporg_id).FirstOrDefault();
            if (checkOrg != 1) { return false; }
            if (traktors && list.TraktorId != null)
            {
             var otherId =  db.Traktors.Where(t => t.trakt_id == list.TraktorId).Select(t => t.Garages.org_org_id).FirstOrDefault();
             if (list.OrgId != otherId) { return true; }
            }
            if (employees.Count != 0)
            {
                foreach (var item in employees)
                {
                    if (item != null)
                    {
                        var otherId = db.Employees.Where(t => t.emp_id == item).Select(t => t.org_org_id).FirstOrDefault();
                        if (list.OrgId != otherId) { return true; }
                    }
                }
            }
            if (equipment && list.TrailerId != null)
            {
                var otherId = db.Equipments.Where(t => t.equi_id == list.TrailerId).Select(t => t.modequi_modequi_id).FirstOrDefault(); // org_id
                if (list.OrgId != otherId) { return true; }
            }
            if (fieldAndPlant && list.PlantId != null)
            {
                var otherId = db.Fields_to_plants.Where(t => t.fielplan_id == list.PlantId).Select(t => t.Fields.org_org_id).FirstOrDefault();
                if (list.OrgId != otherId) { return true; }
            }
            if (field && list.FieldId != null)
            {
                var otherId = db.Fields.Where(t => t.fiel_id == list.FieldId).Select(t => t.org_org_id).FirstOrDefault();
                if (list.OrgId != otherId) { return true; }
            }
            return false;
        }




        /// <summary>
        /// to do delte method 
        /// </summary>
        /// <param name="detailId"></param>
        /// <returns></returns>
        public string ApproveShiftTaskDetail(int detailId)
        {
            using (var trans = db.Database.BeginTransaction())
            {
                var detail = db.Day_task_detail.Where(d => d.dtd_id == detailId).FirstOrDefault();
                detail.status = (int)DayTaskStatus.Issued;
                db.SaveChanges();
                trans.Commit();
            }
            return "approve";
        }

        public CreateWaysListReq GetWaysListByShiftTask(int detailId)
        {
            var result = new CreateWaysListReq();
            var detail = db.Day_task_detail.Where(d => d.dtd_id == detailId).FirstOrDefault();
            result.AvailableId = detail.otv_emp_id;
            result.DriverId = detail.emp1_emp_id;
            result.TaskTypeId = detail.tptas_tptas_id;
           result.FieldId = detail.fiel_fiel_id;

            if (detail.plant_plant_id != -1)
            {
                result.PlantId = db.Fields_to_plants.Where(f => f.fiel_fiel_id == detail.fiel_fiel_id &&
                (detail.plant_plant_id != 36 ? f.date_ingathering.Year == detail.Day_task.dt_date.Year : true) &&
                f.plant_plant_id == detail.plant_plant_id).OrderByDescending(t => t.fielplan_id)
                .Select(f => f.fielplan_id).FirstOrDefault();
            }
            //культура - "культура"
            else {
                result.PlantId = detail.plant_plant_id;
            }

            result.PlantId = result.PlantId == 0 ? null : result.PlantId;
            result.TraktorId = db.Traktors.Where(p => p.trakt_id == detail.trak_trak_id).Select(p => p.trakt_id).FirstOrDefault();
            result.TraktorId = result.TraktorId == 0 ? null : result.TraktorId;
            result.TrailerId = db.Equipments.Where(t => t.equi_id == detail.equi_equi_id).Select(t => t.equi_id).FirstOrDefault();
            result.TrailerId = result.TrailerId == 0 ? null : result.TrailerId;
            result.TypeFuel = 3;
            result.AgroreqId = detail.id_agroreq;
            if (detail.shift_num == 1)
            {
                result.timeStart = "07:00";
                result.timeEnd = "19:00";
            }
            if (detail.shift_num == 2)
            {
                result.timeStart = "19:00";
                result.timeEnd = "07:00";
            }
            //check Res
            var employees = new List<int?>();
            employees.Add(detail.otv_emp_id);
            employees.Add(detail.emp1_emp_id);
            var lst = new CreateWaysListReq();
            lst.OrgId = detail.Day_task.org_org_id;
            lst.TrailerId = detail.equi_equi_id;
            lst.TraktorId = detail.trak_trak_id;
            lst.FieldId = detail.fiel_fiel_id;
            result.AllResource = checkAllResource(lst, true, employees, true, false, true);
            //
            return result;
        }

        public void MapTaskFromDTO(Tasks task, CreateWaysListReq dto)
        {
            task.fiel_fiel_id = (int)dto.FieldId;
            task.tptas_tptas_id = (int)dto.TaskTypeId;
            task.emp_emp_id = dto.AvailableId;
            task.status = dto.SatusId != null ? dto.SatusId : 1;
            task.plant_plant_id = dto.PlantId;
            task.equi_equi_id = dto.TrailerId == 0 ? null : dto.TrailerId;
            task.description = !string.IsNullOrEmpty(dto.Comment) ? dto.Comment : null;
            task.width_equip = dto.EquipWidth;  //ширина
            task.id_agroreq = dto.AgroreqId;

            var plantId = db.Fields_to_plants.Where(p => p.fielplan_id == dto.PlantId).Select(p => p.plant_plant_id).FirstOrDefault();
            var tptasid = dto.TaskTypeId.Value;
            int? techid = null;
            if (dto.TraktorId.HasValue) {
                techid = db.Traktors.Where(p => p.trakt_id == dto.TraktorId).First().id_grtrakt;
            }
            int? equiid = dto.TrailerId;
            var rate = GetRate(tptasid, dto.AgroreqId, techid, equiid, plantId);

            if (rate != null) {
                task.id_rate = rate.Id;
            }
            task.equi_equi_id = equiid;
        }

        public void MapWaysListFromDTO(Sheet_tracks waysList, CreateWaysListReq dto)
        {
            if (dto.IsRepair == false && dto.TraktorId == null)
                throw new BllException(25, "Не выбрано значение в поле \"Техника\"");
            waysList.is_repair = dto.IsRepair == null ? false : dto.IsRepair;
            waysList.trakt_trakt_id = dto.TraktorId;
            waysList.num_sheet = dto.WayListNum;
            waysList.emp_emp_id = (int)dto.DriverId;
            waysList.equi_equi_id = dto.TrailerId == 0 ? null : dto.TrailerId;
            waysList.org_org_id = (int)dto.OrgId;
            waysList.user_user_id = (int)dto.UserId;
            waysList.id_agroreq = dto.AgroreqId;
            waysList.shtr_type_id = 1; // Путевой лист механизатора

            //обнуем топливо при редактировании
            waysList.left_fuel = null;

            //save org_id
            var employees = new List<int?>();
            employees.Add(dto.DriverId);
            employees.Add(dto.AvailableId);
            var lst = new CreateWaysListReq();
            lst.OrgId = dto.OrgId; lst.TrailerId = dto.TrailerId; lst.TraktorId = dto.TraktorId; lst.PlantId = dto.PlantId;
            waysList.exported = (checkAllResource(lst, true, employees, true, true, false) == true) ? 1 : 0 ;
            //
            if (dto.TypeFuel != null)
            {
                waysList.id_type_fuel = dto.TypeFuel;
                waysList.fuel_price = (decimal?)db.Type_fuel.Where(p => p.id == dto.TypeFuel).Select(p => p.current_price).FirstOrDefault();
            }


            if (dto.DetailId != null)
            {
                waysList.dtd_dtd_id = dto.DetailId;
                var detail = db.Day_task_detail.Where(d => d.dtd_id == dto.DetailId).FirstOrDefault();
                detail.status = (int)DayTaskStatus.Issued;
            }
            TimeSpan timeStart = new TimeSpan(Int32.Parse(dto.timeStart.Substring(0, 2)), Int32.Parse(dto.timeStart.Substring(3, 2)), 0);
            TimeSpan timeEnd = new TimeSpan(Int32.Parse(dto.timeEnd.Substring(0, 2)), Int32.Parse(dto.timeEnd.Substring(3, 2)), 0);
            waysList.date_begin = dto.DateBegin.Value.Date + timeStart;
            if (timeStart > timeEnd) waysList.date_end = dto.DateBegin.Value.Date.AddDays(1) + timeEnd;
            else waysList.date_end = dto.DateBegin.Value.Date + timeEnd;
        }

        public bool UpdateWaysListById(CreateWaysListReq req)
        {

            using (var trans = db.Database.BeginTransaction())
            {
                var list = db.Sheet_tracks.Where(s => s.shtr_id == req.WaysListId && s.shtr_type_id == 1 /* Путевой лист механизатора */).FirstOrDefault();
                MapWaysListFromDTO(list, req);
                var task = db.Tasks.Where(t => t.tas_id == req.TaskId).FirstOrDefault();
                MapTaskFromDTO(task, req);
                db.SaveChanges();
                trans.Commit();
            }
            return true;
        }

        public PrintWaysListModel PrintWaysList(GetWaysReq req)
        {

            PrintWaysListModel result = new PrintWaysListModel();
            result.query1 = db.Database.SqlQuery<PrintDTO1>(
             string.Format(@"Select
                                 shtr_id,
		                         num_sheet as q1_num,
                                 YEAR(shtr.date_begin) as q1_year,
                                 MONTH(shtr.date_begin) as q1_month,
                                 DAY(shtr.date_begin) as q1_day,
                                 DATEPART(mi, date_begin) as q1_db_mi,
                                 DATEPART(hh, date_begin) as q1_db_hh,   
                                 DATEPART(mi, date_end) as q1_de_mi,
                                 DATEPART(hh, date_end) as q1_de_hh,  
                                 shtr.date_end as q1_date_end,
                                 org.name as q1_garage_name,
                                 emp_mech.short_name q1_m_name,
                                 trk.name as q1_traktor_name,
                                 trk.reg_num as q1_traktor_num,
                                 left_fuel as q1_left_fuel,
                                 issued_fuel as q1_issued_fuel,
                                 remain_fuel as q1_remain_fuel,
                                 moto_start as q1_moto_start,
                                 moto_end as q1_moto_end
                             FROM Sheet_tracks shtr,
	                              Organizations org,
                                  Employees emp_mech,
                                  Traktors trk
                             WHERE shtr.org_org_id = org.org_id  
                               AND emp_mech.emp_id = shtr.emp_emp_id   
                               AND trk.trakt_id = shtr.trakt_trakt_id 
                                and shtr.shtr_id = {0}", req.WaysListId)).ToList();

            result.query2 = db.Database.SqlQuery<PrintDTO2>(
                              string.Format(@"
                                               select
                                                   tas.tas_id,
                                                   fiel.name as q2_field_name,
                                                   fiel.fiel_id as q2_field_id,
                                                   tptas.name as q2_tptas_name,
                                                   equi.shot_name as q2_equip_name,
                                                   agrotech.name as q2_agrotech_name,
                                                   emp.short_name as q2_otv_name,
                                                   plant.short_name as q2_plant,
                                                   tas.workdays as q2_workdays,
                                                   tas.worktime as q2_worktime,
                                                   tas.price_days as q2_price_days,
                                                   tas.moto_hours as q2_moto_hours,
                                                   tas.volume_fact as q2_volume_fact,
                                                   tas.area as q2_area,
                                                   tas.salary as q2_salary,
                                                   tas.price_add as q2_price_add, 
                                                   tas.price_add * tas.area as q2_add_price,
                                                   tas.consumption_rate as q2_consumption_rate,
                                                   tas.area * tas.consumption_rate as q2_consumption,
                                                   tas.consumption_fact as q2_consumption_fact,
												   tas.salary * tas.workdays as q2_primary_payment,
                                                   tas.total_salary as q2_total_salary,
                                                   tas.description as q2_comment
                                               FROM Tasks tas
                                               inner join Type_tasks tptas on tas.tptas_tptas_id = tptas.tptas_id
                                               left join Employees emp on emp.emp_id = tas.emp_emp_id
                                               left outer join Equipments equi on equi.equi_id = tas.equi_equi_id
                                               left outer join Agrotech_requirment agrotech on agrotech.id = tas.id_agroreq
                                               left outer join Fields fiel on tas.fiel_fiel_id = fiel.fiel_id 
                                               left outer join Fields_to_plants ftp on tas.plant_plant_id = ftp.fielplan_id
                                               left outer join Plants plant on ftp.plant_plant_id = plant.plant_id 
                                               where tas.shtr_shtr_id = {0}
                                               order by tas.task_num ", req.WaysListId)).ToList();

            var idOrg = Int32.Parse(db.Parameters.Where(t => t.name.Equals("NameOrg")).Select(t => t.value).FirstOrDefault());
            result.query1[0].org_name = db.Organizations.Where(t => t.org_id == idOrg).Select(t => t.name).FirstOrDefault();
            result.query1[0].ogrn = db.Parameters.Where(t => t.name.Equals("OGRN")).Select(t => t.value).FirstOrDefault();
            result.query2[0].org_name = db.Organizations.Where(t => t.org_id == idOrg).Select(t => t.name).FirstOrDefault();
            result.query2[0].ogrn = db.Parameters.Where(t => t.name.Equals("OGRN")).Select(t => t.value).FirstOrDefault();
            result.query2[0].adress = db.Organizations.Where(t => t.org_id == idOrg).Select(t => t.address).FirstOrDefault();
            foreach (var item in result.query2)
            {
                if (item.q2_field_id == 115)
                {
                    item.q2_field_name = item.q2_comment;
                    item.q2_otv_name = null;
                }
      
                if (item.q2_area == null || item.q2_area == 0)
                {
                 //   item.q2_area = item.q2_volume_fact;
                    item.q2_add_price = item.q2_price_add * item.q2_volume_fact;
                }
                item.q2_total = Math.Round((item.q2_total_salary ?? 0) + (item.q2_add_price ?? 0), 2);
                item.q2_consumption = item.q2_consumption_rate != null && item.q2_area != null && item.q2_area != 0 ? item.q2_area * item.q2_consumption_rate :
              (item.q2_consumption_rate != null && item.q2_volume_fact != null ? item.q2_volume_fact * item.q2_consumption_rate : null); //расход горючего всего, л
            }

            result.sum1 = result.query2.Sum(g => g.q2_worktime);
            result.sum2 = result.query2.Sum(g => g.q2_workdays);
            result.sum3 = result.query2.Sum(g => g.q2_total_salary);
            result.sum4 = result.query2.Sum(g => g.q2_consumption_fact);
            result.sum5 = result.query2.Sum(g => g.q2_total);
            result.query3 = db.Database.SqlQuery<PrintDTO3>(
                              string.Format(@"
                                              select 
                                                sum(tas.workdays) q3_workdays,
                                                sum(tas.worktime) q3_worktime,
                                                sum(tas.salary) q3_salary,
                                                sum(tas.consumption_fact) q3_consumption_fact
                                            from Tasks tas
                                            where tas.shtr_shtr_id = {0} ", req.WaysListId)).ToList();

            result.query4 = db.Database.SqlQuery<PrintDTO4>(
                              string.Format(@"
                                            select rf.volume as q4_volume,
	                                               (select short_name from Employees e where e.emp_id = rf.emp_emp_id) as q4_name
                                            from Refuelings rf 
                                            where rf.shtr_shtr_id = {0} 
                                            order by rf.order_num  ", req.WaysListId)).ToList();

            return result;
        }

        public ClosedWayListModel GetCloseInfoByWaysList(GetWaysReq req)
        {
            var list = db.Sheet_tracks.Where(s => s.shtr_id == req.WaysListId && s.shtr_type_id == 1 /* Путевой лист механизатора */).Select(s => new ClosedWayListModel
            {
                WaysListId = s.shtr_id,
                DriverName = s.Employees.short_name,
                TraktorId = s.trakt_trakt_id,
                DateBegin = s.date_begin,
                TraktorName = !string.IsNullOrEmpty(s.Traktors.reg_num) ? s.Traktors.name + " (" + s.Traktors.reg_num + ")" : s.Traktors.name,
                MotoStart = s.moto_start,
                MotoEnd = s.moto_end,
                RemainFuel = s.remain_fuel,
                leftFuel = s.left_fuel,
                WaysListNum = s.num_sheet,
                IsRepair = s.is_repair,
                OrgId = s.org_org_id,
            }).FirstOrDefault();

            if (list.TraktorId != null && (list.IsRepair == false || list.IsRepair == null))
            {
                var lastClosedList = db.Sheet_tracks.
                                Where(s =>
                                            s.trakt_trakt_id == list.TraktorId &&
                                            s.date_begin < list.DateBegin &&
                                            (s.is_repair == false || s.is_repair == null) &&
                                            s.shtr_type_id == 1
                                        ).
                                OrderByDescending(s => s.date_begin).
                                Take(1).FirstOrDefault();

                if ((list.MotoStart == null || list.MotoStart == 0) && lastClosedList != null) list.MotoStart = lastClosedList.moto_end;
                if ((list.leftFuel == null || list.leftFuel == 0) && lastClosedList != null) list.leftFuel = lastClosedList.remain_fuel;
            }
            var refuelings = db.Refuelings.Where(r => r.shtr_shtr_id == req.WaysListId).ToList();

            var taskList = db.Tasks.Where(t => t.shtr_shtr_id == req.WaysListId).Select(t => new GetTaskDTO
            {
                TaskId = t.tas_id,
                FieldName = t.Fields.name,
                PlantName = t.plant_plant_id != -1 ? t.Fields_to_plants.Plants.name : "Культура",
                TaskName = t.Type_tasks.name,
                EquipmentName = t.Equipments.shot_name,
                WorkTime = t.worktime,
                WorkDay = t.workdays,
                PriceDays = t.price_days,
                MotoHours = t.moto_hours,
                Volumefact = t.volume_fact,
                Area = t.area,
                Salary = t.salary,
                TypePrice = t.type_price,
                PriceAdd = ((TypePrice)(int)t.type_price) == TypePrice.Ga ? t.area * t.price_add : t.volume_fact * t.price_add,
                ConsumptionRate = t.consumption_rate,
                Consumption = t.consumption_rate != null && t.area != null && t.area!=0 ? t.area * t.consumption_rate :
                (t.consumption_rate != null && t.volume_fact != null ? t.volume_fact* t.consumption_rate : null) , //расход горючего всего, л
                ConsumptionFact = t.consumption_fact,
                AvailableName = t.Employees.short_name,
                TaskNum = t.task_num,
                PrimaryPayment = t.salary * t.workdays,
                PriceTotal = t.total_salary,
            }).OrderBy(t => t.TaskNum).ToList();


            list.IssuedFuel = refuelings.Any() ? refuelings.Sum(r => r.volume) : 0;
            var TotalConsuptionFact = new decimal?();
            if (taskList.Any())
            {
                TotalConsuptionFact = taskList.Sum(t => t.ConsumptionFact ?? 0);
            }
            list.RemainFuel = (list.leftFuel ?? 0) + (list.IssuedFuel ?? 0) - (TotalConsuptionFact ?? 0);
            list.Tasks = taskList;
            list.SumWorkTime = list.Tasks.Sum(t => t.WorkTime);
            list.SumWorkDay = list.Tasks.Sum(t => t.WorkDay);


            //check org_id
            CreateWaysListReq lst = new CreateWaysListReq(); lst.OrgId = list.OrgId;
            var employees = refuelings.Select(t => t.emp_emp_id).ToList();
            list.AllResource = checkAllResource(lst, false, employees, false, false, false);
            return list;
        }

        public List<RefuelingDTO> GetRefuelingsByIdList(int wayListId)
        {
            var refuelingsList = db.Refuelings.Where(re => re.shtr_shtr_id == wayListId).Select(re => new RefuelingDTO
            {
                RefuelId = re.rf_id,
                Volume = re.volume,
                NameId = re.emp_emp_id,
                OrderNum = re.order_num,
            }).OrderBy(r => r.OrderNum).ToList();
            return refuelingsList;
        }


        public CloseWayListMapDTO GetCloseInfoForMap(int TaskId)
        {
            // TaskId = 287;
            db.Database.CommandTimeout = 600;
            CloseWayListMapDTO result = new CloseWayListMapDTO();
            CloseMapInfoDTO info = new CloseMapInfoDTO();
            result.features = new List<CloseMapInfoDTO>();
            result.type = "FeatureCollection";
            info.type = "Feature";
            var taskInfo = db.Tasks.FirstOrDefault(t => t.tas_id == TaskId);
            if (taskInfo == null)
            {
                throw new BllException(13, "Задание не было найдено");
            }
            if (taskInfo.Fields.type == 1)
            {
                var plants = (from f in db.Fields
                              where f.fiel_id == taskInfo.fiel_fiel_id

                              join fieldsToPlants in db.Fields_to_plants on f.fiel_id equals fieldsToPlants.fiel_fiel_id into
                                  fieldsToPlantsLeftJoin
                              from ftp in fieldsToPlantsLeftJoin.DefaultIfEmpty()
                              where ftp.status == (int)StatusFtp.Approved && ftp.date_ingathering.Year.CompareTo(DateTime.Now.Year) >= 0
                              join plants_ in db.Plants on ftp.plant_plant_id equals plants_.plant_id into plantsLeftJoin
                              from p in plantsLeftJoin.DefaultIfEmpty()

                              select new
                              {
                                  p.Type_plants.color,
                                  p.name
                              }).ToList();

                var foo = new CoordGeoJsonDTO
                {
                    Kk = DbGeography.FromText(
                        taskInfo.Fields.coord.AsText())
                };

                var jsonText = CommonFunction.ParseCoord(foo.Kk).ToObject<List<List<decimal[]>>>();

                info.properties = new CloseMapPropInfoDTO();
                info.geometry = new CloseGeometryInfoDTO();
                info.geometry.coordinates = jsonText;
                info.geometry.type = "Polygon";
                info.properties.color = /*taskInfo.Fields_to_plants != null && taskInfo.Fields_to_plants.Plants != null && taskInfo.Fields_to_plants.Plants.Type_plants != null
                                        ? taskInfo.Fields_to_plants.Plants.Type_plants.color 
                                        : */"0xc8961d";
                info.properties.plant = string.Join(", ", plants.Select(p => p.name));
                info.properties.name = taskInfo.Fields.name;
                result.features = new List<CloseMapInfoDTO>();
                result.features.Add(info);
            }

            // Рисуем трек по gps для задания в поле 
            if (taskInfo.Sheet_tracks.Traktors != null)
            {
                var track = (from l in db.GPS_track_archive
                            where l.id_task == TaskId
                            select l.coord).FirstOrDefault();
                if (track != null)
                {
                    var jsonLine = CommonFunction.ParseCoord(track).ToObject<List<decimal[]>>();
                    CloseMapInfoDTO infoWaysList = new CloseMapInfoDTO();
                    infoWaysList.type = "Feature";
                    infoWaysList.properties = new CloseMapPropInfoDTO();
                    infoWaysList.properties.color = "0xff0000";
                    infoWaysList.geometry = new CloseGeometryInfoDTO();
                    infoWaysList.geometry.coordinates = jsonLine;
                    infoWaysList.geometry.type = "LineString";
                    result.features.Add(infoWaysList);
                }
                else
                {
                    if (taskInfo.date_begin == null || taskInfo.date_begin.Value.Year < DateTime.Now.Year - 3) taskInfo.date_begin = taskInfo.Sheet_tracks.date_begin;
                    if (taskInfo.date_end == null || taskInfo.date_end.Value.Year < DateTime.Now.Year - 3) taskInfo.date_end = taskInfo.Sheet_tracks.date_end;
                    if (taskInfo.Sheet_tracks.Traktors.track_id != null && taskInfo.date_begin != null && taskInfo.date_end != null)
                    {
                        //проверка на архив
                        TimeSpan ts = new TimeSpan(01, 15, 0);
                        var checkDate = (DateTime.Now.Date + ts).AddMonths(-3);
                        string q1 = null, q = null; List<GPSmodel> coordWaysList = new List<GPSmodel>() ;
                        List<GPSmodel>  coordWaysList2 = new List<GPSmodel>();
                        if (taskInfo.date_begin <= checkDate)
                        {
                             q1 = string.Format(@" select GPS_archive.coord, GPS_archive.track_id, GPS_archive.date  from  GPS_archive 
                                inner join
                                (select max(gps_id) as maxID,coord.STAsText() as text  from GPS_archive 
                                 where track_id = {0} and date between '{1}' and '{2}'  group  by coord.STAsText())
                                 gpsG on gpsG.maxID = GPS_archive.gps_id ORDER BY GPS_archive.date",
                                    taskInfo.Sheet_tracks.Traktors.track_id, taskInfo.date_begin.Value.ToString("yyyy-MM-ddTHH:mm:ss.fff"), taskInfo.date_end.Value.ToString("yyyy-MM-ddTHH:mm:ss.fff"));
                        
                        }

                        if (taskInfo.date_begin >= checkDate)
                        {
                             q = string.Format(@" select GPS.coord, GPS.track_id, GPS.date  from  GPS 
                                inner join
                                (select max(gps_id) as maxID,coord.STAsText() as text  from GPS 
                                 where track_id = {0} and date between '{1}' and '{2}'  group  by coord.STAsText())
                                 gpsG on gpsG.maxID = GPS.gps_id ORDER BY GPS.date",
                                         taskInfo.Sheet_tracks.Traktors.track_id, taskInfo.date_begin.Value.ToString("yyyy-MM-ddTHH:mm:ss.fff"), taskInfo.date_end.Value.ToString("yyyy-MM-ddTHH:mm:ss.fff"));
                        }
                        if (q1 != null)
                        {
                            coordWaysList2 = db.Database.SqlQuery<GPSmodel>(q1).ToList();
                        }
                        if (q != null)
                        {
                            coordWaysList = db.Database.SqlQuery<GPSmodel>(q).ToList();
                        }
                        coordWaysList.AddRange(coordWaysList2);
                        coordWaysList = coordWaysList.OrderBy(t => t.date).ToList();
                        if (coordWaysList.Count() > 1)
                        {
                            string lineString = string.Join(", ", coordWaysList.Select(p => p.coord.Longitude + " " + p.coord.Latitude));
                            List<string> lineArr = new List<string>();
                            foreach (var coord in coordWaysList)
                            {
                                lineArr.Add(string.Format(CultureInfo.InvariantCulture.NumberFormat, "{0} {1}", coord.coord.Longitude, coord.coord.Latitude));
                            }
                            string lineString2 = string.Join(", ", lineArr.Select(p => p));
                            var line = string.Format(CultureInfo.InvariantCulture.NumberFormat, "LineString({0})", lineString2);
                            var foo2 = new CoordGeoJsonDTO
                            {
                                Kk = DbGeography.LineFromText(
                                    line, 4326)
                            };

                            var jsonLine = CommonFunction.ParseCoord(foo2.Kk).ToObject<List<decimal[]>>();

                            CloseMapInfoDTO infoWaysList = new CloseMapInfoDTO();
                            infoWaysList.type = "Feature";
                            infoWaysList.properties = new CloseMapPropInfoDTO();
                            infoWaysList.properties.color = "0xff0000";
                            infoWaysList.geometry = new CloseGeometryInfoDTO();
                            infoWaysList.geometry.coordinates = jsonLine;
                            infoWaysList.geometry.type = "LineString";
                            result.features.Add(infoWaysList);
                        }
                    }
                }
            }

            //Добавляем обработанные полигоны 
            var areasList = db.ThreadedArea.Where(t => t.tas_tas_id == taskInfo.tas_id).Select(t => t.coord).ToList();
            if (areasList.Any())
            {
                foreach (var area in areasList)
                {
                    try
                    {
                        CloseMapInfoDTO infoWaysList = new CloseMapInfoDTO();
                        if (area.SpatialTypeName == "MultiPolygon")
                        {
                            var jsonCoord = CommonFunction.ParseCoord(DbGeography.FromText(area.AsText())).ToObject<List<List<List<decimal[]>>>>();
                            infoWaysList.type = "Feature";
                            infoWaysList.properties = new CloseMapPropInfoDTO();
                            infoWaysList.properties.color = "0xff0000";
                            infoWaysList.geometry = new CloseGeometryInfoDTO();
                            infoWaysList.geometry.coordinates = jsonCoord;
                            infoWaysList.geometry.type = "MultiPolygon";
                        }
                        else
                        {
                            var jsonCoord = CommonFunction.ParseCoord(DbGeography.FromText(area.AsText())).ToObject<List<List<decimal[]>>>();
                            infoWaysList.type = "Feature";
                            infoWaysList.properties = new CloseMapPropInfoDTO();
                            infoWaysList.properties.color = "0xff0000";
                            infoWaysList.geometry = new CloseGeometryInfoDTO();
                            infoWaysList.geometry.coordinates = jsonCoord;
                            infoWaysList.geometry.type = "Polygon";
                        }

                        result.features.Add(infoWaysList);
                    }
                    catch (Exception e) { }
                }
            }

            return result;
        }

        public CloseWayListMapDTO GetCloseInfoForMapByField(int? FieldId, int? TasksId)
        {
            CloseWayListMapDTO result = new CloseWayListMapDTO();
            result.features = new List<CloseMapInfoDTO>();
            CloseMapInfoDTO info = new CloseMapInfoDTO();
            result.type = "FeatureCollection";
            info.type = "Feature";

            var field = (from f in db.Fields
                         where f.fiel_id == FieldId
                         select new
                         {
                             f.type,
                             f.coord,
                             f.name
                         }).FirstOrDefault();
            if (field.type == 1)
            {
                var plants = (from f in db.Fields
                              where f.fiel_id == FieldId

                              join fieldsToPlants in db.Fields_to_plants on f.fiel_id equals fieldsToPlants.fiel_fiel_id into
                                  fieldsToPlantsLeftJoin
                              from ftp in fieldsToPlantsLeftJoin.DefaultIfEmpty()
                              where ftp.status == (int)StatusFtp.Approved && ftp.date_ingathering.Year.CompareTo(DateTime.Now.Year) >= 0
                              join plants_ in db.Plants on ftp.plant_plant_id equals plants_.plant_id into plantsLeftJoin
                              from p in plantsLeftJoin.DefaultIfEmpty()

                              select new
                              {
                                  p.Type_plants,
                                  p.name
                              }).ToList();

                var firstColor = plants.Select(p => p.Type_plants.color).FirstOrDefault();

                if (field != null && field.coord != null)
                {
                    var foo = new CoordGeoJsonDTO
                    {
                        Kk = DbGeography.FromText(
                            field.coord.AsText())
                    };

                    var jsonText = CommonFunction.ParseCoord(foo.Kk);

                    info.properties = new CloseMapPropInfoDTO();
                    info.geometry = new CloseGeometryInfoDTO();
                    info.geometry.coordinates = jsonText.ToObject<List<List<decimal[]>>>();
                    info.geometry.type = "Polygon";
                    info.properties.color = firstColor;
                    info.properties.plant = string.Join(", ", plants.Select(p => p.name));
                    info.properties.name = field.name;
                    result.features = new List<CloseMapInfoDTO> { info };
                }


                if (TasksId != null)
                {
                    var taskInfo = db.Tasks.FirstOrDefault(t => t.tas_id == TasksId);
                    if (taskInfo == null)
                    {
                        throw new BllException(13, "Задание не было найдено");
                    }

                    if (taskInfo.Sheet_tracks.Traktors != null)
                    {
                        if (taskInfo.date_begin == null) taskInfo.date_begin = taskInfo.Sheet_tracks.date_begin;
                        if (taskInfo.date_end == null) taskInfo.date_end = taskInfo.Sheet_tracks.date_end;
                        if (taskInfo.Sheet_tracks.Traktors.track_id != null && taskInfo.date_begin != null && taskInfo.date_end != null)
                        {
                            var coordWaysList = db.Database.SqlQuery<GPSmodel>(
                            string.Format(@"select GPS.coord, GPS.track_id, GPS.date 
                                    from dbo.GPS
                            WHERE GPS.track_id = {0} and 
                                    GPS.date between '{1}' and '{2}' 
                                    ORDER BY GPS.date",
                                            taskInfo.Sheet_tracks.Traktors.track_id, taskInfo.date_begin.Value.ToString("yyyy-MM-dd HH:mm:ss.fff"), taskInfo.date_end.Value.ToString("yyyy-MM-dd HH:mm:ss.fff"))).ToList(); //370019

                            if (coordWaysList.Count() > 2)
                            {
                                string lineString = string.Join(", ", coordWaysList.Select(p => p.coord.Longitude + " " + p.coord.Latitude));
                                List<string> lineArr = new List<string>();
                                foreach (var coord in coordWaysList)
                                {

                                    lineArr.Add(string.Format(CultureInfo.InvariantCulture.NumberFormat, "{0} {1}", coord.coord.Longitude, coord.coord.Latitude));
                                }
                                string lineString2 = string.Join(", ", lineArr.Select(p => p));
                                var line = string.Format(CultureInfo.InvariantCulture.NumberFormat, "LineString({0})", lineString2);
                                var foo2 = new CoordGeoJsonDTO
                                {
                                    Kk = DbGeography.LineFromText(
                                        line, 4326)
                                };

                                var jsonLine = CommonFunction.ParseCoord(foo2.Kk).ToObject<List<decimal[]>>();

                                CloseMapInfoDTO infoWaysList = new CloseMapInfoDTO();
                                infoWaysList.type = "Feature";
                                infoWaysList.properties = new CloseMapPropInfoDTO();
                                infoWaysList.properties.color = "0xff0000";
                                infoWaysList.geometry = new CloseGeometryInfoDTO();
                                infoWaysList.geometry.coordinates = jsonLine;
                                infoWaysList.geometry.type = "LineString";
                                result.features.Add(infoWaysList);
                            }
                        }
                    }
                }
            }

            return result;
        }

        public string UpdateRefuelings(UpdateRefuelingsReq req)
        {
            using (var trans = db.Database.BeginTransaction())
            {
                if (req.Refuelings != null)
                {
                    var updateRefuel = req.Refuelings.Where(r => r.RefuelId != null).ToList();
                    var addRefuel = req.Refuelings.Where(r => r.RefuelId == null).ToList();
                    var wasRefuel = db.Refuelings.Where(r => r.shtr_shtr_id == req.WaysListId).ToList();
                    var deleteRef = wasRefuel.Where(r => !updateRefuel.Select(u => u.RefuelId).Contains(r.rf_id)).ToList();

                    if (deleteRef.Any())
                    {
                        db.Refuelings.RemoveRange(deleteRef);
                    }

                    foreach (var elm in updateRefuel)
                    {
                        if (elm.Volume != null)
                        {
                            var fuel = db.Refuelings.Where(e => e.rf_id == elm.RefuelId).FirstOrDefault();
                            fuel.volume = (decimal)elm.Volume;
                            fuel.order_num = elm.OrderNum;
                            fuel.emp_emp_id = elm.NameId;
                        }

                    }

                    if (addRefuel.Any())
                    {
                        var id = CommonFunction.GetNextId(db, "Refuelings");
                        //     var numOrder = updateRefuel.Count() + 1;
                        foreach (var elm in addRefuel)
                        {
                            if (elm.Volume != null)
                            {
                                Refuelings refuel = new Refuelings();
                                refuel.rf_id = id;
                                refuel.order_num = elm.OrderNum;
                                refuel.volume = (decimal)elm.Volume;
                                refuel.emp_emp_id = elm.NameId;
                                refuel.shtr_shtr_id = req.WaysListId;
                                id++;
                                db.Refuelings.Add(refuel);
                            }
                        }
                    }
                }
                db.SaveChanges();
                trans.Commit();
                return "ok";
            }
        }
        public int CopyTaskInWayList(int TaskId)
        {
            using (var trans = db.Database.BeginTransaction())
            {
                var newId = CommonFunction.GetNextId(db, "Tasks");
                var copyTask = db.Tasks.Where(d => d.tas_id == TaskId).FirstOrDefault();
                var newTask = new Tasks();
                newTask = copyTask;
                newTask.status = (int)WayListStatus.Issued;
                db.Tasks.Add(newTask);
                db.SaveChanges();
                trans.Commit();
                if (copyTask.Sheet_tracks.is_repair == false || copyTask.Sheet_tracks.is_repair == null) CalculationMotoEndInWayList(copyTask.shtr_shtr_id);
                return newId;
            }
        }
        public List<DictionaryItemsDTO> GetRefuellerInfo(GetWaysListsReq req)
        {
            //заправщики
          var checkOrg = db.Organizations.Where(t => t.org_id == req.OrgId).Select(t => t.tporg_tporg_id).FirstOrDefault();
          var newOrgId = checkOrg == 1 ? (int?)req.OrgId : null;
            int? checkRes = null;
            if (req.WaysListId != null)
            {
                //check org_id
                CreateWaysListReq lst = new CreateWaysListReq(); lst.OrgId = req.OrgId;
                var employees = db.Refuelings.Where(t => t.shtr_shtr_id == req.WaysListId).Select(t => t.emp_emp_id).ToList();
                checkRes = (checkAllResource(lst, false, employees, false, false, false) == true) ? 1 : 0;

            }
            var listR = db.Employees.Where(e => e.pst_pst_id == 3 && e.deleted == false
               && ((newOrgId != null && checkRes != 1) ? e.org_org_id == newOrgId : true)).
                                  Select(e => new DictionaryItemsDTO
                                        {
                                            Name = e.short_name,
                                            Id = e.emp_id,
                                        }).OrderBy(e => e.Name).ToList();
            return listR;
        }

        public string DeleteTaskFromWaysList(int taskId)
        {
            var Task = new Tasks();
            var isRepair = new Boolean?();
            using (var trans = db.Database.BeginTransaction())
            {
                var track = db.GPS_track_archive.Where(t => t.id_task == taskId).ToList();
                db.GPS_track_archive.RemoveRange(db.GPS_track_archive);
                Task = db.Tasks.Where(t => t.tas_id == taskId).FirstOrDefault();
                isRepair = db.Sheet_tracks.Where(s => s.shtr_id == Task.shtr_shtr_id).Select(s => s.is_repair).FirstOrDefault();
                db.Tasks.Remove(Task);
                db.SaveChanges();
                trans.Commit();

            }
            if (isRepair == false || isRepair == null) CalculationMotoEndInWayList(Task.shtr_shtr_id);
            return "";
        }

        public TaskFullDTO GetTaskById(int taskId)
        {
            var t = db.Tasks.Where(p => p.tas_id == taskId).Select(p => p).FirstOrDefault();

            if (t.id_rate == null) {
                using (var trans = db.Database.BeginTransaction())
                {
                    var rate = GetRate(t.tptas_tptas_id, null, t.Sheet_tracks.trakt_trakt_id,
                        t.equi_equi_id, t.plant_plant_id);
                    if (rate != null)
                    {
                        t.id_rate = rate.Id;
                    }
                    db.SaveChanges();
                    trans.Commit();
                }
            }

            var taskList = new TaskFullDTO
            {
                TaskId = t.tas_id,
                FieldId = t.fiel_fiel_id,
                PlantId = t.plant_plant_id,
                TaskTypeId = t.tptas_tptas_id,
                EquipmentId = t.equi_equi_id,
                WorkTime = t.worktime,
                WorkDay = t.workdays,
                PriceDays = t.price_days,
                MotoHours = t.moto_hours,
                Volumefact = t.volume_fact,
                Area = t.area,
                AreaAuto = t.area_auto,
                Salary = t.salary,
                TypePrice = t.type_price,
                PriceAdd = t.price_add,
                AgroreqId = t.id_agroreq,
                WialonConsFact = t.consumption_auto,
                Consumption = t.consumption_rate != null && t.area != null ? t.area * t.consumption_rate : 0,
                ConsumptionFact = t.consumption_fact,
                AvailableId = t.emp_emp_id,
                TaskNum = t.task_num,
                EquipWidth = t.width_equip,
                Comment = !string.IsNullOrEmpty(t.description) ? t.description : null,
                Traktor = new DictionaryItemsDTO
                {
                    Id = t.Sheet_tracks.trakt_trakt_id
                },
                TypeFuel = new DictionaryItemsDTO
                {
                    Id = t.Sheet_tracks.id_type_fuel,
                    Name = t.Sheet_tracks.Type_fuel.name,
                    Price = t.Sheet_tracks.Type_fuel.current_price
                },
                FuelCost = t.fuel_cost,
                FuelConsumption = t.consumption_rate.HasValue ? t.consumption_rate : ((t.id_rate != null && t.Task_rate != null) ?
                t.Task_rate.fuel_consumption : null),
                RateShift = t.salary.HasValue ? t.salary.Value : ((t.id_rate != null && t.Task_rate != null) ? t.Task_rate.rate_shift : 0),
                RatePiecework = t.price_add.HasValue ? t.price_add.Value : ((t.id_rate != null && t.Task_rate != null) ? t.Task_rate.rate_piecework : 0),
                ConsumptionRate = t.consumption_rate.HasValue ? t.consumption_rate : ((t.id_rate != null && t.Task_rate != null) ?
                t.Task_rate.fuel_consumption : null),
            };

            //check org_id
            CreateWaysListReq lst = new CreateWaysListReq(); lst.OrgId = t.Sheet_tracks.org_org_id;
            var employees = new List<int?>(); employees.Add(t.emp_emp_id);
            taskList.AllResource = checkAllResource(lst, false, employees, false, false, false);

            return taskList;
        }

        public string CloseWayListUpdateTasks(UpdateTaskReq elm)
        {
            //проверка культуры
            var typeField = db.Fields.Where(t => t.fiel_id == (int)elm.FieldId).Select(t => t.type).FirstOrDefault();
            if (typeField != 2 && elm.PlantId == null)
            {
                throw new BllException(21, "Заполните поле 'Культура'");
            }
            using (var trans = db.Database.BeginTransaction())
            {
                var isRepairWayList = db.Sheet_tracks.Where(s => s.shtr_id == elm.WaysListId).Select(s => s.is_repair).FirstOrDefault();
                if (elm.TaskId != null)
                {  
                    var task = db.Tasks.Where(e => e.tas_id == elm.TaskId).FirstOrDefault();
                    task.fiel_fiel_id = (int)elm.FieldId;
                    task.plant_plant_id = elm.PlantId;
                    task.tptas_tptas_id = (int)elm.TaskTypeId;
                    task.equi_equi_id = elm.EquipmentId;
                    task.worktime = elm.WorkTime;
                    task.workdays = elm.WorkDay;
                    task.price_days = elm.PriceDays;
                    task.moto_hours = elm.MotoHours;
                    task.volume_fact = elm.Volumefact;
                    task.area = elm.Area;
                    task.salary = elm.RateShift;
                    task.consumption_rate = elm.ConsumptionRate;
                    task.type_price = elm.TypePrice;
                    task.id_rate = elm.IdRate;
                    task.price_add = elm.RatePiecework;
                    task.width_equip = elm.EquipWidth;   //ширина оборудования.
                    task.consumption_fact = elm.ConsumptionFact;
                    task.emp_emp_id = elm.AvailableId;
                    task.description = !string.IsNullOrEmpty(elm.Comment) ? elm.Comment : null;
                    task.fuel_cost = elm.CostFuel;
                    task.id_agroreq = elm.AgroreqId;
                    task.consumption_auto = elm.WialonConsFact;
                    UpdateTotalSalaryWayList(task);
                }

                if (elm.TaskId == null)
                {
                    var taskid = CommonFunction.GetNextId(db, "Tasks");

                    var taskList = db.Tasks.Where(t => t.shtr_shtr_id == elm.WaysListId).ToList();
                    var num = taskList.Count() != 0 ? taskList.Max(t => t.task_num) : 1;
                    Tasks newTask = new Tasks();
                    newTask.tas_id = taskid;
                    newTask.shtr_shtr_id = (int)elm.WaysListId;
                    newTask.fiel_fiel_id = (int)elm.FieldId;
                    newTask.plant_plant_id = elm.PlantId;
                    newTask.tptas_tptas_id = (int)elm.TaskTypeId;
                    newTask.equi_equi_id = elm.EquipmentId;
                    newTask.worktime = elm.WorkTime;
                    newTask.workdays = elm.WorkDay;
                    newTask.price_days = elm.PriceDays;
                    newTask.moto_hours = elm.MotoHours;
                    newTask.volume_fact = elm.Volumefact;
                    newTask.area = elm.Area;
                    newTask.id_rate = elm.IdRate;
                    newTask.salary = elm.RateShift;
                    newTask.consumption_rate = elm.ConsumptionRate;
                    newTask.type_price = elm.TypePrice;
                    newTask.price_add = elm.RatePiecework;
                    newTask.description = !string.IsNullOrEmpty(elm.Comment) ? elm.Comment : null;
                    newTask.width_equip = elm.EquipWidth ;   //ширина
                    newTask.consumption_fact = elm.ConsumptionFact;
                    newTask.emp_emp_id = elm.AvailableId;
                    newTask.status = (int)WayListTaskSatuts.Create;
                    newTask.task_num = num + 1;
                    UpdateTotalSalaryWayList(newTask);
                    db.Tasks.Add(newTask);
                }
                db.SaveChanges();
                trans.Commit();

                if (isRepairWayList == false || isRepairWayList == null) CalculationMotoEndInWayList(elm.WaysListId);
                return "ok";
            }
        }

        private void UpdateTotalSalaryWayList(Tasks task)
        {
            decimal? k;
            if (task.type_price.HasValue)
            {
                switch (task.type_price.Value)
                {
                    case (int)TypePrice.Ga:
                        k = task.workdays * task.salary + (task.price_add.HasValue && task.area.HasValue ? task.area * task.price_add : 0);
                        break;
                    case (int)TypePrice.Tonn:
                        k = task.workdays * task.salary + (task.price_add.HasValue && task.volume_fact.HasValue ? task.volume_fact * task.price_add : 0);
                        break;
                    default:
                        k = task.workdays * task.salary;
                        break;
                }
            }
            else
            {
                k = task.workdays * task.salary;
            }
            task.total_salary = k;
        }

        public string UpdateTotalPriceInAllTask()
        {
            using (var trans = db.Database.BeginTransaction())
            {
                try
                {
                    var tasks = db.Tasks.Where(t => t.Sheet_tracks.shtr_type_id == 1).ToList();
                    foreach (var t in tasks)
                    {
                        UpdateTotalSalaryWayList(t);
                    }

                    db.SaveChanges();
                    trans.Commit();
                    return "ok";
                }
                catch (Exception)
                {
                    trans.Rollback();
                    throw;
                }
            }
        }

        public string ClosedWaysList(int WaysListId)
        {
            var tasks = db.Tasks.Where(t => t.shtr_shtr_id == WaysListId && t.plant_plant_id == null && t.Fields.type == 1).FirstOrDefault();
            if (tasks != null) {
                throw new BllException(21, "Закрытие невозможно. Заполните поле 'Культура'");
            }

            using (var trans = db.Database.BeginTransaction())
            {
                var list = db.Sheet_tracks.Where(s => s.shtr_id == WaysListId && s.shtr_type_id == 1 /* Путевой лист механизатора */).FirstOrDefault();
                if ((list.is_repair == false) && (list.left_fuel == null || list.remain_fuel == null || list.moto_start == null))
                {
                    throw new BllException(21, "Путевой лист не может быть закрыт если у него не заполены значения полей \"Моточасы на начало\" , \"Было горючего\", \"Отстаток горючего\"");
                }
                list.status = (int)WayListStatus.Closed;
                db.SaveChanges();
                trans.Commit();
                return "ok";
            }
        }

        private void CalculationMotoEndInWayList(int WayListId)
        {
            using (var trans = db.Database.BeginTransaction())
            {
                var list = db.Sheet_tracks.Where(s => s.shtr_id == WayListId && s.shtr_type_id == 1).FirstOrDefault();
                var taskList = db.Tasks.Where(t => t.shtr_shtr_id == WayListId).ToList();
                var sumMotoHour = 0;
                var sumConsumptionFact = new decimal?();
                if (taskList.Any())
                {
                    sumMotoHour = taskList.Sum(t => t.moto_hours ?? 0);
                    sumConsumptionFact = taskList.Sum(t => t.consumption_fact ?? 0);
                }

                var refuelings = db.Refuelings.Where(r => r.shtr_shtr_id == WayListId).ToList();
                list.remain_fuel = list.left_fuel + (refuelings.Any() ? refuelings.Sum(r => r.volume ?? 0) : 0) - sumConsumptionFact;

                list.moto_end = (list.moto_start ?? 0) + sumMotoHour;

                var olderList = (from s in db.Sheet_tracks
                                 where s.trakt_trakt_id == list.trakt_trakt_id &&
                                       s.date_begin > list.date_begin &&
                                       (s.is_repair == false || s.is_repair == null) && s.shtr_type_id == 1
                                 let reful = db.Refuelings.Where(r => r.shtr_shtr_id == s.shtr_id).ToList()
                                 let tasks = db.Tasks.Where(t => t.shtr_shtr_id == s.shtr_id).ToList()
                                 orderby s.date_begin
                                 select new WayListCalcInfoDto
                                 {
                                     s = s,
                                     reful = reful.Any() ? reful.Sum(r => r.volume ?? 0) : 0,
                                     ConsumptionFact = tasks.Sum(t => t.consumption_fact ?? 0),
                                     MotoHour = tasks.Sum(t => t.moto_hours ?? 0),
                                     MileageTotal = tasks.Sum(t => t.mileage_total ?? 0)
                                 }).ToList();

                var MotoEnd = list.moto_end;
                var RemainFuel = list.remain_fuel;

                foreach (var e in olderList)
                {

                    e.s.moto_start = MotoEnd ?? 0;
                    e.s.left_fuel = RemainFuel ?? 0;
                    MotoEnd = e.s.moto_end = e.s.moto_start + e.MotoHour;
                    RemainFuel = e.s.remain_fuel = e.s.left_fuel + e.reful - e.ConsumptionFact;
                }

                db.SaveChanges();
                trans.Commit();
            }
        }

        public string DeleteWayLists(int WaysListId)
        {
            using (var trans = db.Database.BeginTransaction())
            {
                var wayList = db.Sheet_tracks.Where(s => s.shtr_id == WaysListId && s.shtr_type_id == 1 /* Путевой лист механизатора */).FirstOrDefault();
                if (wayList == null)
                    throw new BllException(13, "Путевой лист не был найден обновите страницу");
              //  if (wayList.status == (int)WayListStatus.Closed)
             //       throw new BllException(12, "Путевой лист не может быть удален со статусом \"Закрыт\"");

                var taskList = db.Tasks.Where(t => t.shtr_shtr_id == WaysListId).ToList();
                var refList = db.Refuelings.Where(r => r.shtr_shtr_id == WaysListId).ToList();

                var dtd = db.Day_task_detail.Where(d => d.dtd_id == wayList.dtd_dtd_id).FirstOrDefault();
                if (dtd != null) dtd.status = (int)DayTaskStatus.Assigned;

                db.Refuelings.RemoveRange(refList);
                db.Tasks.RemoveRange(taskList);
                db.Sheet_tracks.Remove(wayList);
                db.SaveChanges();
                trans.Commit();
                return "delete";

            }
        }
        public string UpdateCloseInfoByWaysList(ClosedWayListModel req)
        {
            using (var trans = db.Database.BeginTransaction())
            {
                var list = db.Sheet_tracks.Where(s => s.shtr_id == req.WaysListId && s.shtr_type_id == 1 /* Путевой лист механизатора */).FirstOrDefault();

                list.moto_start = req.MotoStart;
                list.moto_end = req.MotoEnd;
                list.issued_fuel = req.IssuedFuel;
                list.remain_fuel = req.RemainFuel;
                list.left_fuel = req.leftFuel;

                db.SaveChanges();
                trans.Commit();
                if (list.is_repair == null || list.is_repair == false) CalculationMotoEndInWayList(list.shtr_id);
                return "ok";
            }
        }


        public int InsertNewField(NewFieldsReq req)
        {
            using (var trans = db.Database.BeginTransaction())
            {
                Fields newFields = new Fields();
                newFields.fiel_id = CommonFunction.GetNextId(db, "Fields");
                newFields.org_org_id = (int)req.OrgId;
                newFields.area = (decimal)req.Area;
                newFields.name = req.Name;
                newFields.type = 1;

                var polygon = string.Format(CultureInfo.InvariantCulture.NumberFormat, "POLYGON(({0}))", req.Coordinates[0]);
                newFields.coord = DbGeography.PolygonFromText(polygon, 4326);

                if (newFields.coord.Area > 10000000000)
                {
                    var polygon2 = string.Format(CultureInfo.InvariantCulture.NumberFormat, "POLYGON(({0}))", req.Coordinates[1]);
                    newFields.coord = DbGeography.PolygonFromText(polygon2, 4326);
                }

                var point = string.Format(CultureInfo.InvariantCulture.NumberFormat, "POINT({0})", req.Center);
                newFields.center = DbGeography.PointFromText(point, 4326);

                Fields_to_plants newFToP = new Fields_to_plants();
                newFToP.fiel_fiel_id = newFields.fiel_id;
                newFToP.plant_plant_id = (int)req.PlantId;
                newFToP.date_ingathering = req.Date;
                newFToP.date_sowing = req.Date;
                newFToP.status = (int)StatusFtp.Approved;
                db.Fields.Add(newFields);
                db.Fields_to_plants.Add(newFToP);
                db.SaveChanges();
                trans.Commit();
                return newFields.fiel_id;
            }
        }

        

        public WaysListForCreateModel GetInfoForCreateByDayTask(int daytaskid)
        {
            return GetInfoForCreateWaysListByShiftTasks(daytaskid, 1);
        }
    }
}
