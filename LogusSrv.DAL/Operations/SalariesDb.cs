﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LogusSrv.DAL.DBModel;
using LogusSrv.DAL.Entities.DTO;
using LogusSrv.DAL.Entities.DTO.Salaries;
using LogusSrv.DAL.Entities.Req;
using LogusSrv.DAL.Entities.Req.Salaries;
using LogusSrv.DAL.Entities.Res.Salaries;
using LogusSrv.DAL.Enums;
using LogusSrv.DAL.Utils;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using System.Globalization;
using LogusSrv.CORE.Exceptions;
using System.IO;

//
namespace LogusSrv.DAL.Operations
{
    public class SalariesDb
    {
        protected readonly LogusDBEntities db = new LogusDBEntities();
        public readonly WayListBase wayList = new WayListBase();
        List<ShortDocInfoDTO> list1 = new List<ShortDocInfoDTO>();
        public ListShortDocInfo GetSalariesDocumentList(GridDataReq req)
        {
            var resultList = new ListShortDocInfo();
            var list = db.Salaries
                        .Select(s => new ShortDocInfoDTO
                        {
                            SalaryDocId = s.sal_id,
                            Summa = 0,
                            //   Type = s.tpsal_tpsal_id,
                            DateDoc = s.doc_data,
                            // Organization = s.Organizations.name,
                            Number = s.doc_num,
                            Responsible = s.salemp_salemp_id != null ? s.Salaries_employees.second_name + " " + s.Salaries_employees.first_name + " " + s.Salaries_employees.middle_name : null,
                            Comment = s.comments,
                        }).ToList();

            if (req.DateFilter != null) list = list.Where(l => l.DateDoc.Value.Date == req.DateFilter.Value.Date).ToList();
            foreach (var elm in list)
            {
                elm.Date = elm.DateDoc.Value.ToString("dd.MM.yyyy");
            }

            var CountItems = list.ToList().Count();
            var TotalPage = (double)CountItems / (double)req.PageSize;
            double CountPages = TotalPage < 0 ? 1 : Math.Ceiling(TotalPage);
              for (int i = 0; i < list.Count; i++)
            {
             var   list2 = db.Database.SqlQuery<ShortDocInfoDTO>(
            string.Format(@"SELECT sum(sum) as Summa FROM Salary_details where Salary_details.sal_sal_id=" + list[i].SalaryDocId.ToString() + "")).ToList();
             if (!String.IsNullOrEmpty(list2[0].Summa.ToString())) { list[i].Summa = list2[0].Summa; } else { list[i].Summa = 0; }
           ; }
            var gridData = GridHelpers<ShortDocInfoDTO>.GetGridData(list.AsQueryable(), req);
            resultList.Values = gridData.ToList();
            resultList.CountPages = (int)CountPages;
            resultList.CountItems = CountItems;

          
                return resultList;
        }


        public SalariesSprDTO GetSalariesDictionary(bool req)
        {
            SalariesSprDTO result = new SalariesSprDTO();
            result.OgranizationList = db.Organizations.Where(o => o.tporg_tporg_id == (int)TypeOrganization.Section).
                                        Select(o => new DictionaryItemsDTO
                                        {
                                            Id = o.org_id,
                                            Name = o.name
                                        }).OrderBy(e => e.Name).ToList();
            result.TypeDocList = db.Type_salary_doc.Where(t => t.deleted != true).   //фильтр для типов оплаты
                                    Select(t => new DictionaryItemsDTO
                                    {
                                        Id = t.tpsal_id,
                                        Name = t.name
                                    }).OrderBy(e => e.Name).ToList();
            result.ResponsibleList = db.Salaries_employees.Select(r => new DictionaryItemsDTO
            {
                Id = r.salemp_id,
                Name = r.second_name + " " + r.first_name + " " + r.middle_name,
            }).OrderBy(e => e.Name).ToList();
            result.TasksList = db.Type_tasks.Where(e => e.deleted != true). //фильтр для типа работы
                               Select(t => new DictionaryItemsDTO
                               {
                                   Name = t.name,
                                   Id = t.tptas_id,
                               }).OrderBy(e => e.Name).ToList();

            var ObjectList = db.Fields.Where(e => e.deleted != true)
                                .Where(f => f.type == 2).
                                Select(t => new DictionaryItemForFields
                                {
                                    Name = t.name,
                                    Id = t.fiel_id,
                                }).OrderBy(t => t.Name).ToList();

            var values = wayList.GetSelectedPlants();

            result.FieldsAndPlants = (from f in db.Fields_to_plants
                                      where f.status == (int)StatusFtp.Approved && ((f.date_ingathering.Year == DateTime.Now.Year && f.plant_plant_id != 36) ||
                                      values.Contains(f.fielplan_id))
                                     group new DictionaryPlantItemsDTO
                                      {
                                          plantId = f.plant_plant_id,
                                          Name = f.Plants.name + " (" + f.date_ingathering.Year + ")",
                                          Id = f.fielplan_id
                                      } by new
                                      {
                                          f.fiel_fiel_id,
                                          f.Fields.name
                                      } into g
                                      select new DictionaryItemForFields
                                      {
                                          Id = g.Key.fiel_fiel_id,
                                          Name = g.Key.name,
                                          Values = g.OrderBy(v => v.Name).ToList()
                                      }).OrderBy(g => g.Name).ToList();

            //культура
            foreach (var item in result.FieldsAndPlants)
            {
                item.Values.Add(new DictionaryItemForFields
                {
                    Id = -1,
                    Name = "Культура",
                });
            }


            result.PlantsByYear = (from f in db.Fields_to_plants
                                   where f.status == (int)StatusFtp.Approved &&
                                      f.Plants.deleted != true
                                   group f by new
                                   {
                                       f.plant_plant_id,
                                       f.date_ingathering,
                                       f.Plants.name
                                   } into gcs
                                   select new DictionaryItemsDTO
                                   {
                                       Id = gcs.Max(g => g.fielplan_id),
                                       Name = gcs.Key.name + " (" + gcs.Key.date_ingathering.Year + ")"
                                   }).OrderBy(g => g.Name).ToList();
            result.FieldsAndPlants.AddRange(ObjectList);
            result.ShiftList = new List<DictionaryItemsDTO>();
            var firstShift = new DictionaryItemsDTO() { Id = (int)TypeShift.First, Name = ((int)TypeShift.First).ToString() };
            var secondShift = new DictionaryItemsDTO() { Id = (int)TypeShift.Second, Name = ((int)TypeShift.Second).ToString() };
            result.ShiftList.Add(firstShift);
            result.ShiftList.Add(secondShift);

            return result;
        }

        public NewDocInfoRes GetNewInfoSalariesDoc(int req)
        {
            var info = new NewDocInfoRes();
            var id = CommonFunction.GetNextId(db, "Salaries");
            string number = "ВОЛА000000";
            if (id.ToString().Length >= 7)
            {
                for (var i = 6; i < id.ToString().Length; i++)
                {
                    number = number + "0";
                }
            }
            number = number.Substring(0, number.Length - id.ToString().Length);
            number = number.Insert(number.Length, id.ToString());
            info.Number = number;
            info.Date = DateTime.UtcNow.AddHours(req).ToString("dd.MM.yyyy HH:mm:ss");
            return info;
        }

        public int SaveNewMainDocInfo(SaveDocInfoReq req)
        {

            var id = CommonFunction.GetNextId(db, "Salaries");
            var item = new Salaries();
            item.sal_id = id;
            MapMainDocInfoWithDTO(item, req);
            db.Salaries.Add(item);
            return id;
        }

        public void MapMainDocInfoWithDTO(Salaries item, SaveDocInfoReq dto)
        {
            //    item.tpsal_tpsal_id = dto.TypeId;
            item.doc_num = dto.Number;
            item.otd_org_id = dto.OrgId;
            item.salemp_salemp_id = dto.ResponsibleId;
            item.doc_data = DateTime.ParseExact(dto.Date, "dd.MM.yyyy", CultureInfo.InvariantCulture);
            item.comments = dto.Comment;
        }




        public int UpdateMainDocInfo(FullDocInfoDTO req)
        {
            using (var trans = db.Database.BeginTransaction())
            {
                if (req.SalaryDocId == null)
                {
                    req.SalaryDocId = SaveNewMainDocInfo(req);
                }
                else
                {
                    var changeItem = db.Salaries.Where(s => s.sal_id == req.SalaryDocId).FirstOrDefault();
                    MapMainDocInfoWithDTO(changeItem, req);
                }

                foreach (var detailDoc in req.DetailList)
                {
                    if (detailDoc.detailDocId == null)
                    {
                        var newDetail = new Salary_details();
                        newDetail.sal_sal_id = req.SalaryDocId;
                        MapDetailDocInfoWithDTO(newDetail, detailDoc);
                        db.Salary_details.Add(newDetail);
                    }
                    else
                    {
                        var changeDetail = db.Salary_details.Where(s => s.sald_id == detailDoc.detailDocId).FirstOrDefault();
                        MapDetailDocInfoWithDTO(changeDetail, detailDoc);
                    }
                }

                db.SaveChanges();
                trans.Commit();
            }
            return (int)req.SalaryDocId;
        }

        public FullDocInfoDTO GetSalariesDocInfoById(int id)
        {
            var DocInfo = db.Salaries.Where(s => s.sal_id == id).
                            Select(s => new FullDocInfoDTO
                            {
                                SalaryDocId = s.sal_id,
                                TypeId = s.tpsal_tpsal_id,
                                DateBegin = s.doc_data,
                                OrgId = s.otd_org_id,
                                Number = s.doc_num,
                                ResponsibleId = s.salemp_salemp_id,
                                ResponsibleName = s.Salaries_employees.second_name + " " + s.Salaries_employees.first_name + " " + s.Salaries_employees.middle_name,
                                Comment = s.comments,
                            }).FirstOrDefault();

            DocInfo.DetailList = db.Salary_details.
                                Where(s => s.sal_sal_id == id).Select(s => new DetailDocInfoDTO
                                {
                                    detailDocId = s.sald_id,
                                    Date = s.date,
                                    Date1 = s.date.ToString(),
                                    Date2 =s.date,
                                    Value = (decimal)s.value,
                                    Sum = (decimal) s.sum * s.value != 0 ? s.price * s.value : null,
                                    Prices = (decimal)s.price,
                                    Employee = new ElemntInfoDTO
                                    {
                                        Id = s.salemp_salemp_id,
                                        Name = s.salemp_salemp_id != null ? s.Salaries_employees.second_name + " " + s.Salaries_employees.first_name + " " + s.Salaries_employees.middle_name : string.Empty
                                    },
                                    Shift = new ElemntInfoDTO
                                    {
                                        Id = s.shift,
                                        Name = s.shift == 1 ? "1" : "2",
                                    },
                                    Field = new ElemntInfoDTO
                                    {
                                        Id = s.fiel_fiel_id,
                                        Name = s.Fields.name
                                    },
                                    TypeTask = new ElemntInfoDTO
                                    {
                                        Id = s.tptas_tptas_id,
                                        Name = s.Type_tasks.name
                                    },
                                    TypeSalary = new ElemntInfoDTO
                                    {
                                        Id = s.tpsal_tpsal_id,
                                        Name = s.Type_salary_doc.name
                                    },
                                    Plant = new ElemntInfoDTO
                                    {
                                        Id = s.Fields_to_plants.fielplan_id,
                                        Name =  s.plant_plant_id != -1 ? s.Fields_to_plants.Plants.name : "Культура",
                                    }

                                }).ToList();
            DocInfo.Date = DocInfo.DateBegin.Value.ToString("dd.MM.yyyy HH:mm:ss");
            for (int i = 0; i < DocInfo.DetailList.Count; i++)
            {
                DocInfo.DetailList[i].Date1 = DocInfo.DetailList[i].Date2.Value.ToString("dd.MM.yyyy");
            }
            return DocInfo;
        }

        public void MapDetailDocInfoWithDTO(Salary_details item, DetailDocInfoDTO dto)
        {
            item.date = dto.Date2;
            // item.field_day = dto.FieldDay;
            // item.worked = dto.WorkedTime;
            item.value = Convert.ToInt64(dto.Value);
            item.comment = dto.Comment;
            item.sum = Convert.ToInt64(dto.Prices * dto.Value);
            item.price =Convert.ToInt64(dto.Prices);
            item.plant_plant_id = dto.Plant.Id;
            item.tptas_tptas_id = dto.TypeTask.Id;
            item.tpsal_tpsal_id = dto.TypeSalary.Id;
            item.fiel_fiel_id = dto.Field.Id;
            item.salemp_salemp_id = dto.Employee.Id;
            item.shift = dto.Shift.Id;
        }

        public string DeleteSalaryDoc(int docId)
        {
            using (var trans = db.Database.BeginTransaction())
            {
                var doc = db.Salaries.Where(s => s.sal_id == docId).FirstOrDefault();

                if (doc == null)
                    throw new BllException(15, "Документ небыл найден , обновите страницу");
                var detailDoc = db.Salary_details.Where(d => d.sal_sal_id == docId).ToList();

                db.Salary_details.RemoveRange(detailDoc);
                db.Salaries.Remove(doc);
                db.SaveChanges();
                trans.Commit();
            }
            return "ok";
        }

        public string DeleteEmployeeSalaryInfo(int detaildocId)
        {
            using (var trans = db.Database.BeginTransaction())
            {
                var detailDoc = db.Salary_details.Where(s => s.sald_id == detaildocId).FirstOrDefault();
                if (detailDoc == null)
                    throw new BllException(15, "Информация о работе сортрудника небыла найдена ,сохраните и обновите страницу");
                db.SaveChanges();
                trans.Commit();
            }
            return "ok";
        }

        public LoadExcelInfoDTO loadExcel(int id, string pathTemplate)
        {
            byte[] byteArray = File.ReadAllBytes(pathTemplate);
            using (MemoryStream stream = new MemoryStream())
            {
                stream.Write(byteArray, 0, (int)byteArray.Length);
                var info = new LoadExcelInfoDTO();
                using (var document = SpreadsheetDocument.Open(stream, true))
                {
                    var SheetName = "Начисление ЗП";
                    // 1 лист отчета 
                    Sheet sheet;
                    try
                    {
                        sheet = document.WorkbookPart.Workbook.GetFirstChild<Sheets>().Elements<Sheet>().SingleOrDefault(s => s.Name == SheetName);
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(String.Format("Возможно в документе существует два листа с названием \"{0}\"!\n", SheetName), ex);
                    }

                    if (sheet == null)
                    {
                        throw new Exception(String.Format("В шаблоне не найден \"{0}\"!\n", SheetName));
                    }


                    var worksheetPart = (WorksheetPart)document.WorkbookPart.GetPartById(sheet.Id.Value);
                    var sheetData = worksheetPart.Worksheet.GetFirstChild<SheetData>();
                    var list = GetSalariesDocInfoById(id);
                    uint i = 6;

                    Cell cellDat = GetCell(worksheetPart.Worksheet, "C", 4);
                    cellDat.CellValue = new CellValue(list.Number);
                    cellDat.DataType = new EnumValue<CellValues>(CellValues.String);

                    Cell cellDat2 = GetCell(worksheetPart.Worksheet, "E", 4);
                    cellDat2.CellValue = new CellValue(list.Date);
                    cellDat2.DataType = new EnumValue<CellValues>(CellValues.String);

                    Cell cellDat3 = GetCell(worksheetPart.Worksheet, "G", 4);
                    cellDat3.CellValue = new CellValue(list.ResponsibleName);
                    cellDat3.DataType = new EnumValue<CellValues>(CellValues.String);

                    Cell cellDat4 = GetCell(worksheetPart.Worksheet, "I", 4);
                    cellDat4.CellValue = new CellValue(list.Comment);
                    cellDat4.DataType = new EnumValue<CellValues>(CellValues.String);


                    foreach (var item in list.DetailList)
                    {
                        Row refRowC = sheetData.Elements<Row>().Where(r => 6 == r.RowIndex).First();
                        var row = new Row();
                        if (i == 6)
                        {
                            row = refRowC;
                        }
                        else
                        {
                            row = (Row)refRowC.Clone();
                            row.RowIndex = new UInt32Value(i);
                        }

                        foreach (var cell in row.Elements<Cell>())
                        {
                            SetShiftTasksCellsValue(cell, item);
                            cell.CellReference = new StringValue(cell.CellReference.Value.Replace(refRowC.RowIndex.Value.ToString(), row.RowIndex.ToString()));
                            worksheetPart.Worksheet.Save();
                        }

                        if (i != 6) sheetData.Append(row);
                        worksheetPart.Worksheet.Save();
                        i++;
                    }

                    info.Content = stream.ToArray();
                    info.NameFile = list.Number;
                    document.Close();
                }

                return info;
            }
        }

        private void SetShiftTasksCellsValue(Cell cell, DetailDocInfoDTO item)
        {
            string cellReference = cell.CellReference.Value;

            switch (cellReference)
            {
                //  case "A6":
                //      SetCellValue(cell, item.TitleColumn);
                //      break;
                case "B6":
                    SetCellValue(cell, item.Date.Value.ToString("dd-MM-yyyy"));
                    break;
                case "C6":
                    SetCellValue(cell, item.Employee.Name);
                    break;
                case "D6":
                    SetCellValue(cell, item.Shift.Name);
                    break;
                case "E6":
                    SetCellValue(cell, item.TypeTask.Name);
                    break;
                case "F6":
                    SetCellValue(cell, item.Field.Name);
                    break;
                case "G6":
                    SetCellValue(cell, item.Plant.Name);
                    break;
                case "H6":
                    SetCellValue(cell, item.TypeSalary.Name);
                    break;
                case "I6":
                    SetCellValue(cell, item.Value.ToString());
                    break;
                case "J6":
                    SetCellValue(cell, item.Prices.ToString());
                    break;
                case "K6":
                    SetCellValue(cell, item.Sum.ToString());
                    break;
            }

        }

        private static void SetCellValue(Cell cell, string value)
        {
            cell.CellValue = new CellValue(string.IsNullOrEmpty(value) ? "" : value.ToString());
            cell.DataType = new EnumValue<CellValues>(CellValues.String);
        }

        private static Cell GetCell(Worksheet worksheet, string columnName, uint rowIndex)
        {
            Row row = GetRow(worksheet, rowIndex);

            if (row == null)
                return null;

            return row.Elements<Cell>().Where(c => string.Compare
                   (c.CellReference.Value, columnName +
                   rowIndex, true) == 0).First();
        }

        // Given a worksheet and a row index, return the row.
        private static Row GetRow(Worksheet worksheet, uint rowIndex)
        {
            return worksheet.GetFirstChild<SheetData>().
              Elements<Row>().Where(r => r.RowIndex == rowIndex).First();
        }
    }
}
