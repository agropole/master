﻿using LogusSrv.DAL.DBModel;
using LogusSrv.DAL.Entities.Res;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogusSrv.DAL.Operations
{
    public class TypeSparePartsDict
    {
        public List<TypeSparePartsDictModel> GetTypeSpareParts(LogusDBEntities db)
        {
            var res = db.Type_spare_parts.Where(t => t.deleted != true).Select(t => new TypeSparePartsDictModel
                       {
                           tsp_id = t.tsp_id,
                           name = t.name,
                           deleted = t.deleted,
                           uniq_code = t.uniq_code,
                       }).ToList();
            return res;
        }

        public string UpdateTypeSpareParts(List<ModelUpdateTypeSpareParts> updateObject, LogusDBEntities db)
        {
            using (var trans = db.Database.BeginTransaction())
            {
                try
                {
                    var newId = (db.Type_spare_parts.Max(t => (int?)t.tsp_id) ?? 0) + 1;
                    for (var i = 0; i < updateObject.Count; i++)
                    {
                        var obj = updateObject[i].Document;
                        switch (updateObject[i].command)
                        {
                            case "del":
                                var delDic = db.Type_spare_parts.Where(o => o.tsp_id == obj.tsp_id).FirstOrDefault();
                                delDic.deleted = true;
                                break;
                            case "update":
                                var updateDic = db.Type_spare_parts.Where(o => o.tsp_id == obj.tsp_id).FirstOrDefault();
                                updateDic.name = obj.name;
                                updateDic.uniq_code = obj.uniq_code;
                                break;
                            case "insert":
                                var newDic = new Type_spare_parts
                                {
                                    tsp_id = newId,
                                    name = obj.name,
                                    deleted = false,
                                    uniq_code = obj.uniq_code,
                                };
                                newId++;
                                db.Type_spare_parts.Add(newDic);
                                break;
                        }
                    }
       
                    db.SaveChanges();
                    trans.Commit();
                }
                catch (Exception)
                {
                    trans.Rollback();
                    throw;
                }
            }
            return "success";
        }
    }

    public class TypeSparePartsDictModel
    {
        public int? tsp_id { get; set; }
        public string code { get; set; }
        public string name { get; set; }
        public string uniq_code { get; set; }
        public string description { get; set; }
        public Nullable<bool> deleted { get; set; }
    }
    public class ModelUpdateTypeSpareParts : UpdateDictModel
    {
        public TypeSparePartsDictModel Document { get; set; }
    }
}
