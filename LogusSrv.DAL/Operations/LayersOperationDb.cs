﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LogusSrv.DAL.DBModel;
using LogusSrv.DAL.Entities.DTO;
using LogusSrv.DAL.Entities.Res;
using System.Data.Entity.Spatial;
using System.Data.SqlClient;
using System.Globalization;
using System.Diagnostics;
using System.Net;
using Newtonsoft.Json.Linq;
using Microsoft.CSharp.RuntimeBinder;

namespace LogusSrv.DAL.Operations
{
    public class LayersOperationDb
    {
        protected readonly LogusDBEntities db = new LogusDBEntities();

        public List<Polygons_to_gps> _reqArray = new List<Polygons_to_gps>();
        public Polygons_to_gps polygonDB = new Polygons_to_gps();
        public int id = 1;
        public int group_id = 0;
        public double Witdh = 0;
        public decimal? calcThreadedArea(decimal taskId, DateTime dateEnd, bool webCalc)
        {
            db.Database.CommandTimeout = 1000;
            decimal? area = 0;
            List<AutogenerateTask> listtasks = new List<AutogenerateTask>();
            using (var trans = db.Database.BeginTransaction())
            {
                var oldArea = db.ThreadedArea.Where(t => t.tas_tas_id == taskId).FirstOrDefault();

                if (oldArea != null)
                {
                    db.ThreadedArea.Remove(oldArea);
                }

                var task = (from t in db.Tasks
                            where t.tas_id == taskId
                            join tr in db.Traktors on t.Sheet_tracks.trakt_trakt_id equals tr.trakt_id
                            join eq in db.Equipments on t.Sheet_tracks.equi_equi_id equals eq.equi_id into lefEq
                            from res in lefEq.DefaultIfEmpty()

                            select new AutogenerateTask
                            {
                                tas_id = t.tas_id,
                                tpas_tpas_id = t.tptas_tptas_id,
                                fiel_fiel_id = t.fiel_fiel_id,
                                shtr_shtr_id = t.shtr_shtr_id,
                                plant_plant_id = t.plant_plant_id,
                                emp_emp_id = t.emp_emp_id,
                                date_begin = (t.date_begin.Value.Year > DateTime.Now.Year - 3) ? t.date_begin.Value : t.Sheet_tracks.date_begin,
                                date_end = (t.date_end.Value.Year > DateTime.Now.Year - 3) ? t.date_end.Value : t.Sheet_tracks.date_end,
                                width = (t.width_equip != null ? t.width_equip : tr.width), 
                                status = t.status,
                                track_id = tr.track_id,
                                trakt_id = t.Sheet_tracks.trakt_trakt_id,
                                equi_equi_id = t.equi_equi_id
                            }).FirstOrDefault();

             //   Witdh = (double)(task.width + 3) / 2;
                Witdh = (double)(task.width ) / 2;          //
                group_id = 0;

                id = 1;

                var newFieldsList = new List<FieldsForAutogenerate>();

                if (!webCalc)
                {
                    //расчет после завершения путевого листа на планшете
                    var q = string.Format(@"select a.coord, a.date, Fields.fiel_id  FROM (select * from  GPS inner join (select max(gps_id) as maxID,coord.STAsText() as text 
                                        from GPS 
                                        where track_id = {0} and date between '{1}' and '{2}'
                                        group  by coord.STAsText()) gpsG on gpsG.maxID = GPS.gps_id) AS a 
                                        join Fields ON Fields.coord.STIntersects(a.coord) = 1
                                        where Fields.deleted!='true' and Fields.type=1
                                        ORDER BY Fields.fiel_id, a.date",
                                    task.track_id, task.date_begin.ToString("yyyy-MM-ddTHH:mm:ss.fff"), task.date_end.ToString("yyyy-MM-ddTHH:mm:ss.fff"));

                    var gpsFields = db.Database.SqlQuery<GPSmodel>(q).ToArray();


                    List<GPSmodel> curList = null;
                    AutogenerateTask curTask = null;
                    Tasks newTask;
                    var li = gpsFields.Length;

                    for (int i = 0; i < li; i++)
                    {
                        if (curList == null || (curTask.fiel_fiel_id != gpsFields[i].fiel_id) || i == li - 1)
                        {
                            if (curList != null && curTask.fiel_fiel_id != gpsFields[i].fiel_id || i == li - 1)
                            {
                                newTask = new Tasks();
                                newTask.fiel_fiel_id = (int)curTask.fiel_fiel_id;
                                newTask.shtr_shtr_id = (int)task.shtr_shtr_id;
                                newTask.equi_equi_id = task.equi_equi_id;
                                newTask.width_equip = task.width;
                                newTask.tptas_tptas_id = (int)task.tpas_tpas_id;
                           //     newTask.plant_plant_id = task.plant_plant_id;  не нужно
                                newTask.emp_emp_id = curTask.emp_emp_id = task.emp_emp_id;
                                newTask.date_begin = curTask.date_begin = task.date_begin;
                                newTask.date_end = curTask.date_end = task.date_end;
                                Debug.WriteLine(i.ToString() + "!!!! " + curList.Count.ToString());
                                if ((newTask.fiel_fiel_id != task.fiel_fiel_id || i == li - 1) && curList.Count > 30)
                                {
                                    Debug.WriteLine("new task " + newTask.shtr_shtr_id.ToString() + " " + newTask.fiel_fiel_id);
                                    var taskByField = (from t in db.Tasks
                                                       where t.shtr_shtr_id == task.shtr_shtr_id && t.fiel_fiel_id == newTask.fiel_fiel_id
                                                       join tr in db.Traktors on t.Sheet_tracks.trakt_trakt_id equals tr.trakt_id
                                                       join eq in db.Equipments on t.Sheet_tracks.equi_equi_id equals eq.equi_id into lefEq
                                                       from res in lefEq.DefaultIfEmpty()

                                                       select new AutogenerateTask
                                                       {
                                                           tas_id = t.tas_id,
                                                           tpas_tpas_id = t.tptas_tptas_id,
                                                           fiel_fiel_id = t.fiel_fiel_id,
                                                           shtr_shtr_id = t.shtr_shtr_id,
                                                           plant_plant_id = t.plant_plant_id,
                                                           emp_emp_id = t.emp_emp_id,
                                                           date_begin = t.date_begin != null ? t.date_begin.Value : t.Sheet_tracks.date_begin,
                                                           date_end = dateEnd != null ? dateEnd : (t.date_end != null ? t.date_end.Value : t.Sheet_tracks.date_end),
                                                           width = (t.width_equip != null ? t.width_equip : tr.width) , 
                                                           status = t.status,
                                                           track_id = tr.track_id,
                                                           trakt_id = t.Sheet_tracks.trakt_trakt_id,
                                                           equi_equi_id = t.equi_equi_id
                                                       }).ToList();
                                    if (taskByField.Count == 0)
                                    {
                                        db.Tasks.Add(newTask);
                                        db.SaveChanges();
                                        curTask.tas_id = newTask.tas_id;
                                        calculateList(curList, curTask, false);
                                        listtasks.Add(curTask);
                                    }
                                    else
                                    {
                                        calculateList(curList, taskByField.ElementAt(0), false);
                                        listtasks.Add(taskByField.ElementAt(0));
                                    }
                                }
                                else if (newTask.fiel_fiel_id == task.fiel_fiel_id || i == li - 1 && curList.Count > 30)
                                {
                                    area = calculateList(curList, task, false);
                                    listtasks.Add(task);
                                }
                            }
                            curList = new List<GPSmodel>();
                            curTask = new AutogenerateTask();
                            curTask.fiel_fiel_id = gpsFields[i].fiel_id;
                            curTask.shtr_shtr_id = task.shtr_shtr_id;
                            curTask.width = task.width;
                            curTask.tpas_tpas_id = task.tpas_tpas_id;
                            curTask.track_id = task.track_id;
                            curTask.trakt_id = task.trakt_id;
                            curList.Add(gpsFields[i]);
                        }
                        curList.Add(gpsFields[i]);
                    }
                }
                else
                {
                    //расчет после нажатия кнопки рассчитать в веб-форме
                    if (task.track_id == null) return 0;
                    var q = string.Format(@"select a.coord, a.track_id, a.date  FROM (
                                        select * from  GPS 
                                        inner join
                                        (select max(gps_id) as maxID, coord.STAsText() as text  from GPS 
                                         where track_id = {0} and date between '{2}' and '{3}'  group  by coord.STAsText())
                                        gpsG on gpsG.maxID = GPS.gps_id
                                        ) AS  a
                                        join Fields ON Fields.coord.STIntersects(a.coord) = 1
                                        WHERE Fields.fiel_id = {1}   
                                              ORDER BY Fields.fiel_id, a.date",
                                     task.track_id, task.fiel_fiel_id, task.date_begin.ToString("yyyy-MM-ddTHH:mm:ss.fff"), task.date_end.ToString("yyyy-MM-ddTHH:mm:ss.fff"));
                    var list = db.Database.SqlQuery<GPSmodel>(q).ToList();
                    area = calculateList(list, task, true);
                    listtasks.Add(task);
                }
                db.SaveChanges();
                trans.Commit();
            }

            doArchiveLine(listtasks, 0);
            return area;
        }

        //Wialon 
        public List<double?> getConsWialon(decimal taskId)
        {
            var task = (from t in db.Tasks
                        where t.tas_id == taskId
                        join tr in db.Traktors on t.Sheet_tracks.trakt_trakt_id equals tr.trakt_id
                        select new AutogenerateTask
                        {
                            tas_id = t.tas_id,
                            fiel_fiel_id = t.fiel_fiel_id,
                            date_begin = (t.date_begin.Value.Year > DateTime.Now.Year - 3) ? t.date_begin.Value : t.Sheet_tracks.date_begin,
                            date_end = (t.date_end.Value.Year > DateTime.Now.Year - 3) ? t.date_end.Value : t.Sheet_tracks.date_end,
                            track_id = tr.track_id,
                        }).FirstOrDefault();

            var q = string.Format(@"select a.coord, a.track_id, a.date  FROM (
                                        select * from  GPS 
                                        inner join
                                        (select max(gps_id) as maxID, coord.STAsText() as text  from GPS 
                                         where track_id = {0} and date between '{2}' and '{3}'  group  by coord.STAsText())
                                        gpsG on gpsG.maxID = GPS.gps_id
                                        ) AS  a
                                        join Fields ON Fields.coord.STIntersects(a.coord) = 1
                                        WHERE Fields.fiel_id = {1}   
                                              ORDER BY Fields.fiel_id, a.date",
                                  task.track_id, task.fiel_fiel_id, task.date_begin.ToString("yyyy-MM-ddTHH:mm:ss.fff"), 
                                  task.date_end.ToString("yyyy-MM-ddTHH:mm:ss.fff"));
            var list = db.Database.SqlQuery<GPSmodel>(q).ToList();
            List<double?> val_final = new List<double?>();

            if (list.Count == 0)
            {
                double g = 0;
                val_final.Add(g);
                return val_final;
            }
            var startDate = list.Min(t => t.date);
            var endDate = list.Max(t => t.date);
            //test
        //  endDate = new DateTime(2019, 07, 19, 17, 12, 00);
        //  startDate = new DateTime(2019, 07, 24, 10, 37, 00);
            //отчет
            Int32 unixStartDate = (int)((startDate.ToUniversalTime().Ticks - 621355968000000000) / 10000000);
            Int32 unixEndDate = (int)((endDate.ToUniversalTime().Ticks - 621355968000000000) / 10000000);


            var json = new WebClient().DownloadString("https://hst-api.wialon.com/wialon/ajax.html?svc=token/login&params={" +
          " \"token\" : \"56743f56d2aaf835f9186f7128de783343E7AA58529C13BE3B53E53B3D0D4F6CF6082CE2\"   }");
            dynamic data = JObject.Parse(json);
            string sid = data.eid;  //id session

            //автограф
            //var session = new WebClient().DownloadString("https://navi.gps36.ru/ServiceJSON/Login?UserName=logusagro&Password=logusagro");
            //var schema = new WebClient().DownloadString("https://navi.gps36.ru/ServiceJSON/EnumSchemas?session=" + session);
            //JArray schemaArr = JArray.Parse(schema);
            //string schemaId = schemaArr.FirstOrDefault()?["ID"]?.Value<String>();

            var WebClient = new WebClient();
            WebClient.Encoding = Encoding.UTF8;
            //расход топлива за период единицы техники
            var fuel = WebClient.DownloadString("https://hst-api.wialon.com/wialon/ajax.html?svc=report/exec_report&params={" +
                "\"reportResourceId\" : 21171314 , \"reportTemplateId\" : 1, \"reportObjectId\" :" + task.track_id + " , \"reportObjectSecId\" : 0, \"interval\" :{ \"from\" :" + unixStartDate +
                ", \"to\" : " + unixEndDate + ", \"flags\" :0} }&sid=" + sid);
            try
            {
             dynamic fuel_val = JObject.Parse(fuel);
             var val = ((string)fuel_val.reportResult.stats[5][1]).Split(' ')[0];
             var val_final1 = double.Parse(val, CultureInfo.InvariantCulture);
             val_final.Add(val_final1);
            }
            catch {
                double g = 0;
                val_final.Add(g);
                return val_final;
            }



            var logout = new WebClient().
            DownloadString("https://hst-api.wialon.com/wialon/ajax.html?svc=core/logout&params={}&sid=" + sid);

            return val_final;
        }
        //===

        public decimal? calculateList(List<GPSmodel> list, AutogenerateTask task, bool web)
        {
            List<VectorsGPSDTO> vectors = new List<VectorsGPSDTO>();

            for (var i = 0; i < list.Count() - 1; i++)
            {
                vectors.Add(new VectorsGPSDTO
                {
                    points = new DbGeography[] { list[i].coord.PointAt(1), list[i + 1].coord.PointAt(1) },
                    width = (double)task.width,
                    scalar = getScalar(new DbGeography[] { list[i].coord.PointAt(1), list[i + 1].coord.PointAt(1) }),
                    date = list[i].date,
                    id_traktor = list[i].id_track
                });
            }
            List<VectorsGPSDTO> checkedVectors = new List<VectorsGPSDTO>();
            for (var i = 0; i < vectors.Count() - 1; i++)
            {
                var v1 = vectors[i];
                var v2 = vectors[i + 1];
                var cos = getCosA(vectors[i].scalar, vectors[i + 1].scalar);
                if (cos < 0.55 || i == vectors.Count() - 2)
                {
                    if (i >= 2 || i == vectors.Count() - 2)
                    {
                        var selectedVectors = vectors.Skip(0).Take(i + 1).ToList();
                        if (checkedVectors.Count() == 0)
                        {
                            checkedVectors = selectedVectors;
                        }
                        else
                        {
                            checkedVectors.AddRange(selectedVectors);
                        }
                        doDPolygons(selectedVectors);
                    }
                    vectors.RemoveRange(0, i + 1);
                    i = -1;
                }
            }

            if (polygonDB.coordinates != null)
            {
                ThreadedArea thread = new ThreadedArea();
                polygonDB.coordinates = checkPolygon(polygonDB.coordinates);
                thread.coord = polygonDB.coordinates;
                thread.id_traktor = task.track_id;
                thread.date = _reqArray.Max(l => l.date);
                thread.tas_tas_id = (int)task.tas_id;
                var area = (decimal)thread.coord.Area;
                var taskE = db.Tasks.Where(t => t.tas_id == task.tas_id).FirstOrDefault();
                taskE.area = Math.Round(area / 10000, 2);
                if (!web)
                {
                    taskE.area_auto = taskE.area;
                }
                db.ThreadedArea.Add(thread);
                var a = Math.Round(area / 10000, 2);
                //doArchiveLine(task, a);
                _reqArray = new List<Polygons_to_gps>();
                polygonDB = new Polygons_to_gps();
                return a;//5.92;
            }
            else
            {
                //doArchiveLine(task, 0);
                _reqArray = new List<Polygons_to_gps>();
                polygonDB = new Polygons_to_gps();
                return 0;
            }
        }

        private DbGeography checkPolygon(DbGeography pol)
        {
            try
            {

                if (pol.Area > 10000000000)
                {
                    var coord2 = pol.AsText();
                    coord2 = coord2.Replace("MULTIPOLYGON (((", "").Replace(")))", "").Replace("POLYGON ((", "").Replace("))", "").Replace(", ", ",");
                    var ars = coord2.Split(',');
                    var coordNew = "POLYGON ((";
                    for (var i = ars.Length - 1; i >= 0; i--)
                    {
                        coordNew += ars[i];
                        if (i != 0)
                        {
                            coordNew += ", ";
                        }
                        else
                        {
                            coordNew += "))";
                        }
                    }
                    var reorientedPol = DbGeography.PolygonFromText(coordNew, 4326);
                    if (reorientedPol.Area != 0)
                    {
                        return reorientedPol;
                    }
                    else
                    {
                        return null;
                    }
                }
                else
                {
                    if (pol.Area != 0)
                    {
                        return pol;
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            catch (Exception)
            {
                return null;
            }
        }

        private void doArchiveLine(List<AutogenerateTask> listtasks, decimal area)
        {
            //await Task.Run(() =>
            //{
                using (var trans = db.Database.BeginTransaction())
                {
                    for (var j = 0; j < listtasks.Count; j++)
                    {
                        var task = listtasks[j];
                        var deltrack = db.GPS_track_archive.Where(p => p.id_task == task.tas_id).Select(p => p).ToList();
                        db.GPS_track_archive.RemoveRange(deltrack);

                        List<GPSmodel> list = new List<GPSmodel>();
                        list = db.GPS.Where(p => p.track_id == task.track_id &&
                                            p.date > task.date_begin &&
                                            p.date < task.date_end).Select(p => new GPSmodel
                                            {
                                                coord = p.coord,
                                                date = p.date.Value,
                                            }).OrderBy(p => p.date).ToList();
                        var c = "LINESTRING(";
                        var ids = new List<int>();
                        for (var i = 0; i < list.Count; i++)
                        {
                          //  c += list[i].coord.Longitude + " " + list[i].coord.Latitude;
                            c += string.Format(CultureInfo.InvariantCulture.NumberFormat, "{1} {0}", list[i].coord.Latitude, list[i].coord.Longitude);
                            if (i != list.Count - 1)
                            {
                                c += ", ";
                            }
                            else
                            {
                                c += ")";
                            }
                            ids.Add(list[i].id);
                        }
                        var coord = DbGeography.LineFromText(c, 4326);
                        var l = new GPS_track_archive
                        {
                            date_start = task.date_begin,
                            date_end = task.date_end,
                            id_task = task.tas_id.Value,
                            id_trakt = task.trakt_id.Value,
                            coord = coord,
                            width = task.width,
                            length = (decimal)coord.Length.Value,
                            area = area
                        };
                        db.GPS_track_archive.Add(l);
                    }
                    db.SaveChanges();
                    trans.Commit();
                }
            //});
        }

        public void doDPolygons(List<VectorsGPSDTO> vectors)
        {
            List<string> reqs = new List<string>();
            for (var i = 0; i < vectors.Count(); i++)
            {
                var v = vectors[i];
                var lon0 = v.points[0].Longitude.Value;
                var lat0 = v.points[0].Latitude.Value;
                var lon1 = v.points[1].Longitude.Value;
                var lat1 = v.points[1].Latitude.Value;

                //прямая коллинеальная вектору
                var k = (lat1 - lat0) / (lon1 - lon0);

                var b = lat0 - lon0 * k;

                //прямые нормальные вектору

                double p1Lon;
                double p1Lat;
                double p2Lat;
                double p2Lon;
                double p3Lat;
                double p3Lon;
                double p4Lat;
                double p4Lon;

                if (k != 0)
                {
                    double k1;
                    if (k != 0) k1 = -1 / k;
                    else k1 = Math.Tan(Math.PI / 2);
                    var b1 = lat0 - lon0 * k1;
                    var b2 = lat1 - lon1 * k1;

                    var cos = Math.Sqrt(1 / (1 + k1 * k1));
                    var sin = Math.Sqrt(k1 * k1 / (1 + k1 * k1));

                    var dw = v.width / 2;
                    var dwLon = dw * cos;
                    var dwLat = dw * sin;
                    var delta = dwLon / dwLat;
                    var step = 0.0000001;
                    double de1 = 1000;
                    var pTestLon = lon0 + step;
                    var pTestLat = k1 * pTestLon + b1;
                    var de2 = Math.Abs(distance(lon0, pTestLon, lat0, pTestLat) - Witdh);
                    while (de1 > de2)
                    {
                        pTestLon += step;
                        pTestLat = k1 * pTestLon + b1;
                        de1 = de2;
                        de2 = Math.Abs(distance(lon0, pTestLon, lat0, pTestLat) - Witdh);
                    }
                    var deltaLon = pTestLon - lon0;

                    p1Lon = lon0 - deltaLon;
                    p1Lat = k1 * p1Lon + b1;

                    p4Lon = lon0 + deltaLon;
                    p4Lat = k1 * p4Lon + b1;

                    p2Lon = lon1 - deltaLon;
                    p2Lat = k1 * p2Lon + b2;

                    p3Lon = lon1 + deltaLon;
                    p3Lat = k1 * p3Lon + b2;
                }
                else
                {
                    var step = 0.0000001;
                    double de1 = 1000;
                    var pTestLat = lat0 + step;
                    var de2 = Math.Abs(distance(lon0, lon0, lat0, pTestLat) - Witdh);
                    while (de1 > de2)
                    {
                        pTestLat += step;
                        de1 = de2;
                        de2 = Math.Abs(distance(lon0, lon0, lat0, pTestLat) - Witdh);
                    }
                    /// var deltaLon = pTestLon - lon0;
                    var deltaLat = pTestLat - lat0;

                    p1Lon = lon0;
                    p1Lat = lat0 - deltaLat;

                    p4Lon = lon0;
                    p4Lat = lat0 + deltaLat;

                    p2Lon = lon1;
                    p2Lat = lat0 - deltaLat;

                    p3Lon = lon1;
                    p3Lat = lat0 + deltaLat;
                }

                var d1 = distance(p1Lon, p4Lon, p1Lat, p4Lat);
                var d2 = distance(p2Lon, p3Lon, p2Lat, p3Lat);
                v.p1 = new PointDTO { lon = p1Lon, lat = p1Lat };
                v.p2 = new PointDTO { lon = p2Lon, lat = p2Lat };
                v.p3 = new PointDTO { lon = p3Lon, lat = p3Lat };
                v.p4 = new PointDTO { lon = p4Lon, lat = p4Lat };

                var dx = lon1 - lon0;
                var dy = lat1 - lat0;
                Polygons_to_gps polygon = new Polygons_to_gps();
                try
                {
                    if (dy < 0 || (dy == 0 && dx > 0))
                    {
                        var coord = string.Format(CultureInfo.InvariantCulture.NumberFormat, "POLYGON(({0} {1},{2} {3},{4} {5},{6} {7},{8} {9}))",
                            v.p1.lon, v.p1.lat, v.p2.lon, v.p2.lat, v.p3.lon, v.p3.lat, v.p4.lon, v.p4.lat, v.p1.lon, v.p1.lat);
                        polygon.coordinates = DbGeography.PolygonFromText(coord, 4326);
                    }
                    else if (dy > 0 || (dy == 0 && dx < 0))
                    {
                        var coord = string.Format(CultureInfo.InvariantCulture.NumberFormat, "POLYGON(({0} {1},{2} {3},{4} {5},{6} {7},{8} {9}))",
                            v.p1.lon, v.p1.lat, v.p4.lon, v.p4.lat, v.p3.lon, v.p3.lat, v.p2.lon, v.p2.lat, v.p1.lon, v.p1.lat);
                        polygon.coordinates = DbGeography.PolygonFromText(coord, 4326);
                    }

                    polygon.coordinates = checkPolygon(polygon.coordinates);
                }
                catch (Exception e)
                {
                }

                try
                {
                    if (polygon.coordinates != null && polygon.coordinates.Area < 10000000000 && polygon.coordinates.Area > 10)
                    {
                        _reqArray.Add(polygon);
                        if (id == 1 || polygonDB.coordinates == null)
                        {
                            polygonDB.coordinates = polygon.coordinates;
                            if (polygon.coordinates.Area > 10000)
                            {
                                Debug.WriteLine(polygon.coordinates.Area);
                            }
                        }
                        else if (polygon.coordinates != null && polygon.coordinates.Area < 10000000000 && polygon.coordinates.Area > 10)
                        {
                            polygonDB.coordinates = polygonDB.coordinates.Union(polygon.coordinates);
                            if (polygon.coordinates.Area > 10000)
                            {
                                Debug.WriteLine(polygon.coordinates.Area);
                            }
                        }
                    }
                    id++;

                }
                catch (Exception e)
                {

                }
            }
            group_id++;
        }


        public double distance(double lon1, double lon2, double lat1, double lat2)
        {

            var lon1_rad = DegToRad(lon1);
            var lon2_rad = DegToRad(lon2);
            var lat1_rad = DegToRad(lat1);
            var lat2_rad = DegToRad(lat2);
            var radZ = 6372.795;
            var a = Math.Pow(Math.Sin((lat1_rad - lat2_rad) / 2), 2) + Math.Cos(lat1_rad) * Math.Cos(lat2_rad) * Math.Pow(Math.Sin((lon1_rad - lon2_rad) / 2), 2);
            var s = radZ * 2 * Math.Atan2(Math.Sqrt(a), Math.Sqrt(1 - a));

            return s * 1000;
        }

        public double getCosA(double[] sc1, double[] sc2)
        {
            var cos = (sc1[0] * sc2[0] + sc1[1] * sc2[1]) / (Math.Sqrt(sc1[0] * sc1[0] + sc1[1] * sc1[1]) * Math.Sqrt(sc2[0] * sc2[0] + sc2[1] * sc2[1]));
            //console.log(cos);
            return cos;
        }

        public double[] getScalar(DbGeography[] vector)
        {
            var x1 = LonToX(vector[0].Longitude.Value, 16);
            var x2 = LonToX(vector[1].Longitude.Value, 16);
            var y1 = LatToY(vector[0].Latitude.Value, 16);
            var y2 = LatToY(vector[1].Latitude.Value, 16);
            return new double[] { x2 - x1, y2 - y1 };
        }
        // перевод пиксела ось Y в широту Lat
        public double YtoLat(double y, double z)
        {
            double lat = 0;
            var numTiles = Math.Pow(2, z);
            var bitmapSize = numTiles * 256;
            var k = (y - bitmapSize / 2) / -(bitmapSize / (2 * Math.PI));
            lat = (2 * Math.Atan(Math.Exp(k)) - Math.PI / 2) * 180 / Math.PI;
            return lat;
        }

        // перевод широты Lat в координаты пиксела ось Y
        public double LatToY(double lat, double z)
        {
            double y = 0;
            var numTiles = Math.Pow(2, z);
            var bitmapSize = numTiles * 256;
            var k = Math.Sin(lat * Math.PI / 180);
            y = Math.Floor(bitmapSize / 2 - 0.5 * Math.Log((1 + k) / (1 - k)) * bitmapSize / (2 * Math.PI));
            return Math.Round(y);
        }

        // перевод пиксела ось X в долготу Lon
        public double XtoLon(double x, double z)
        {
            double lon = 0;
            var numTiles = Math.Pow(2, z);
            var bitmapSize = numTiles * 256;
            lon = (x - bitmapSize / 2.0) / (bitmapSize / 360.0);
            return lon;
        }

        // перевод долготы Lon в координаты пиксела ось X
        public double LonToX(double lon, double z)
        {
            double x = 0;
            var numTiles = Math.Pow(2, z);
            var bitmapSize = numTiles * 256;
            x = Math.Floor(bitmapSize / 2 + lon * bitmapSize / 360);
            return x;
        }
        public double DegToRad(double deg)
        {
            return deg * Math.PI / 180;
        }

        public async Task RecountBills()
        {
            await Task.Run(() =>
            {
                var datenow = DateTime.Now;
                var datebefor = datenow.AddDays(-1);
                var dateShiftTwo = datenow.AddDays(-1.5);
                // 1 смена попадает
                var list = db.Tasks.Where(p => p.status != 3 && p.Sheet_tracks.status != 3 &&
                //1 и 2 смена
                ((p.date_end > datebefor && datenow > p.date_end && p.date_begin > dateShiftTwo && p.date_begin < datenow) 
                || (p.Sheet_tracks.date_end > datebefor && datenow > p.Sheet_tracks.date_end &&
                p.Sheet_tracks.date_begin > dateShiftTwo && p.Sheet_tracks.date_begin < datenow))       
                 && !p.area.HasValue).ToList();

                var success = new List<GPS_archive_log>();
                var id = (db.GPS_archive_log.Max(t => (int?)t.id) ?? 0) + 1;
                for (var i = 0; i < list.Count; i++)
                {
                    var l = list[i];
                    try
                    {
                        calcThreadedArea(l.tas_id, l.date_end != null ? l.date_end.Value : l.Sheet_tracks.date_end, false);
                        success.Add(new GPS_archive_log
                        {
                            id = id,
                            ctrl = "LayersOperationDb",
                            func = "RecountBills",
                            date = DateTime.Now,
                            msg = "Task were counted",
                            flag = "success",
                            id_task = l.tas_id
                        });
                        id++;

                    }
                    catch (Exception e)
                    {
                        success.Add(new GPS_archive_log
                        {
                            id = id,
                            ctrl = "LayersOperationDb",
                            func = "RecountBills",
                            date = DateTime.Now,
                            msg = "Count error",
                            flag = "error",
                            id_task = l.tas_id
                        });
                        id++;
                    }
                }

                using (var trans = db.Database.BeginTransaction())
                {

                    db.GPS_archive_log.AddRange(success);
                    //заправки виалон
                    AddRefuelingsInSheetTracks();
                    //===

                    //литры в заданиях
                    foreach (var item in list) {

                        double? litrs = getConsWialon(item.tas_id)[0];

                        var task = db.Tasks.Where(t => t.tas_id == item.tas_id).FirstOrDefault();
                        task.consumption_auto = (decimal?)litrs;
                        if (task.consumption_fact == null) {
                            task.consumption_fact = (decimal?)litrs;
                        }
                        db.SaveChanges();
                    }

                    db.SaveChanges();
                    trans.Commit();
                }
            });
        }

        public void AddRefuelingsInSheetTracks() 
        {
            var datenow = DateTime.Now;
            var datebefor = datenow.AddDays(-1);
            var dateShiftTwo = datenow.AddDays(-1.5);
            //заправки виалон
            var listSheet = db.Sheet_tracks.Where(p => p.status != 3 &&
             (p.date_end > datebefor && datenow > p.date_end &&
             p.date_begin > dateShiftTwo && p.date_begin < datenow)).ToList();

            foreach (var item in listSheet)   {

                var checkRef = db.Refuelings.Where(t => t.shtr_shtr_id == item.shtr_id).FirstOrDefault();

                if (checkRef == null)
                {
                    //     var end_date = new DateTime(2019, 07, 18, 19, 00, 00);
                    //     var start_date = new DateTime(2019, 07, 12, 07, 00, 00);
                    var start_date = item.date_begin;
                    var end_date = item.date_end;
                    var track_id = item.Traktors.track_id;
                    Int32 unixStartDate = (int)((start_date.ToUniversalTime().Ticks - 621355968000000000) / 10000000);
                    Int32 unixEndDate = (int)((end_date.ToUniversalTime().Ticks - 621355968000000000) / 10000000);

                    var json = new WebClient().DownloadString("https://hst-api.wialon.com/wialon/ajax.html?svc=token/login&params={" +
                    " \"token\" : \"56743f56d2aaf835f9186f7128de783343E7AA58529C13BE3B53E53B3D0D4F6CF6082CE2\"   }");
                    dynamic data = JObject.Parse(json);

                    string sid = data.eid;  //id session

                 //   track_id = 15163;
                    var WebClient = new WebClient();
                    WebClient.Encoding = Encoding.UTF8;
                    var refuel = WebClient.DownloadString("https://hst-api.wialon.com/wialon/ajax.html?svc=report/exec_report&params={" +
                        "\"reportResourceId\" : 21171314 , \"reportTemplateId\" : 2, \"reportObjectId\" :" + track_id + " , \"reportObjectSecId\" :0, \"interval\" :{ \"from\" :" + unixStartDate +
                        ", \"to\" : " + unixEndDate + ", \"flags\" :0} }&sid=" + sid);

                    dynamic json1 = JObject.Parse(refuel);
                    try
                    {
                        var liters = double.Parse(((string)json1.reportResult.stats[12][1]).Split(' ')[0], CultureInfo.InvariantCulture);

                        if (liters != 0)
                        {
                            id = (db.Refuelings.Max(t => (int?)t.rf_id) ?? 0) + 1;

                                var dic = new Refuelings
                                {
                                    rf_id = id,
                                    emp_emp_id = 89,
                                    shtr_shtr_id = item.shtr_id,
                                    order_num = 1,
                                    volume = (decimal)liters,

                                };

                                db.Refuelings.Add(dic);
                                db.SaveChanges(); 
                        }
                    }
                    catch (RuntimeBinderException e) { }
                    var logout = new WebClient().
                    DownloadString("http://sdk-04.garage-gps.com/wialon/ajax.html?svc=core/logout&params={}&sid=" + sid);

                }
            }
        }
    }
}

