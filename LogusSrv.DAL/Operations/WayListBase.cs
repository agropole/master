﻿using System;
using System.Collections.Generic;
using System.Linq;
using LogusSrv.CORE.Exceptions;
using LogusSrv.DAL.DBModel;
using LogusSrv.DAL.Entities.DTO;
using LogusSrv.DAL.Entities.Req;
using LogusSrv.DAL.Entities.Res;
using LogusSrv.DAL.Enums;
using LogusSrv.DAL.Utils;
using LogusSrv.DAL.Entities.DTO.DriverWayBills;

namespace LogusSrv.DAL.Operations
{
    public class WayListBase
    {
        protected readonly LogusDBEntities db = new LogusDBEntities(); 
     
        protected RateResModel GetRate(int? tptasid, int? agroreqid, int? techid, int? equiid, int? plantid)
        {
            var rates = db.Task_rate.Where(r => r.id_tptas == tptasid && (!r.id_agroreq.HasValue || r.id_agroreq == agroreqid)
                                               && (!r.id_grtrakt.HasValue || r.id_grtrakt == techid)
                                               && (!r.id_equi.HasValue || r.id_equi == equiid)
                                               && (!r.id_plant.HasValue || r.id_plant == plantid)).Select(r => new RateResModel
                                               {
                                                   Id = r.id,
                                                   TptasId = r.id_tptas.Value,
                                                   PlantId = r.id_plant,
                                                   AgroreqId = r.id_agroreq,
                                                   EquiId = r.id_equi,
                                                   TraktId = r.id_grtrakt,
                                                   ShiftOutput = r.shift_output,
                                                   RateShift = r.rate_shift,
                                                   RatePiecework = r.rate_piecework,
                                                   FuelConsumption = r.fuel_consumption,
                                                   GroupTrId = techid,
                                                   CountCheck = 0 + (r.id_plant.HasValue ? 1 : 0)
                                                                  + (r.id_agroreq.HasValue ? 1 : 0)
                                                                  + (r.id_equi.HasValue ? 1 : 0)
                                                                  + (r.id_plant.HasValue ? 1 : 0)
                                                                  + (r.id_grtrakt.HasValue ? 1 : 0)
                                               }).OrderByDescending(r => r.CountCheck).ToList();
            if (rates.Count > 0)
            {
                return rates[0];
            }
            else
            {
                return null;
            }

        }

        protected WaysListForCreateModel GetCommonInfoList(int waylisttype, int? newOrgId, int? checkRes)
        {
            var result = new WaysListForCreateModel();
            result.NumWaysList = CommonFunction.GetNextId(db, "Sheet_tracks");

            result.EquipmnetsList = db.Equipments.Where(e => e.deleted == false
                && ((newOrgId != null && checkRes != 1) ? e.modequi_modequi_id == newOrgId : true)).
                                    Select(e => new DictionaryItemsTraktors
                                    {
                                        Name = String.IsNullOrEmpty(e.reg_num) ? e.name : e.name + " (" + e.reg_num + ")",
                                        Id = e.equi_id,
                                        RegNum = e.reg_num,
                                        Width = e.width
                                    }).OrderBy(e => e.Name).ToList();
            var empty = new DictionaryItemsEquip
            {
                Name = "         ",
                Id = null,
                RegNum = "",
                Bold = false,
            };
            result.EquipmnetsList.Insert(0, empty);

            result.AgroreqList = db.Agrotech_requirment.Where(p => p.deleted != true).Select(p => new DictionaryItemsDTO
            {
                Id = p.id,
                Name = p.name
            }).OrderBy(p => p.Name).ToList();

            result.TypeFuel = db.Type_fuel.Where(d => d.deleted != true && d.fuel_flag == true).Select(d => new DictionaryItemsDTO
            {
                Id = d.id,
                Name = d.name,
                Price = d.current_price
            }).OrderBy(p => p.Name).ToList();

            var tpprice = waylisttype == 1 ? new[] { 2, 3 } : new[] { 2, 3, 4, 5 };
            result.TypePriceList = db.Type_salary_doc.
                Where(t => tpprice.Contains(t.tpsal_id) && t.deleted != true).

                                Select(t => new DictionaryItemsDTO
                                {
                                    Name = t.short_name,
                                    Id = t.tpsal_id,
                                }).OrderBy(t => t.Name).ToList();
            
            if (waylisttype == 2) {
                result.TypePriceList.Insert(0, new DictionaryItemsDTO {Id=-1, Name = "без расценки" });
            }
                
            result.TasksList = db.Type_tasks.Where(t => t.deleted != true).
                                Select(t => new DictionaryItemsDTO
                                {
                                    Name = t.name,
                                    Id = t.tptas_id,
                                }).OrderBy(e => e.Name).ToList();

            result.ChiefsList = db.Employees.Where(e => e.deleted != true).
                                Where(e => (e.pst_pst_id == 2 || e.pst_pst_id == 7) && ((newOrgId != null && checkRes != 1) ? e.org_org_id == newOrgId : true)).
                                Select(e => new DictionaryItemsDTO
                                {
                                    Name = e.short_name,
                                    Id = e.emp_id,
                                }).OrderBy(e => e.Name).ToList();

            result.TraktorsList = db.Traktors.Where(p => p.deleted != true && p.id_type_way_list == waylisttype
                && ((newOrgId != null && checkRes != 1) ? p.Garages.org_org_id == newOrgId : true)).Select(t => new DictionaryItemsTraktors
            {
                Name = t.name,
                Id = t.trakt_id,
                RegNum = t.reg_num,
                Name_fuel = new DictionaryItemsDTO
                {
                    Id = t.tpf_tpf_id, 
                    Name = t.Type_fuel.name
                }
            }).OrderBy(e => e.Name).ToList();

            return result;
        }

        protected WaysListForCreateModel GetInfoForCreateWaysList(int waylisttype, int? orgId, int? wayListId, int? tasId, int? dayTasId)
        {
            var wLDb = new WaysListsDb();
            var checkOrg = db.Organizations.Where(t => t.org_id == orgId).Select(t => t.tporg_tporg_id).FirstOrDefault();
            var newOrgId = checkOrg == 1 ? (int?)orgId : null;
            int? checkRes = null;
            //sheet
            if (wayListId != null && waylisttype == 1)
            {
                var waysReq = new GetWaysReq(); waysReq.WaysListId = (int)wayListId;
                var sheet = wLDb.GetWaysListById(waysReq);
                var employees = new List<int?>();
                employees.Add(sheet.AvailableId); //ответс.
                var lst = new CreateWaysListReq();
                lst.OrgId = sheet.OrgId; lst.TrailerId = sheet.TrailerId; lst.PlantId = sheet.PlantId;
                checkRes = (wLDb.checkAllResource(lst, false, employees, true, true,false) == true) ? 1 : 0;
                using (var trans = db.Database.BeginTransaction())
                {
                    var sh = db.Sheet_tracks.Where(t => t.shtr_id == wayListId).FirstOrDefault();
                    sh.exported = checkRes;
                    db.SaveChanges();
                    trans.Commit();
                }
            }
            //driver
            if (wayListId != null && waylisttype == 2)
            {
                var driverDb = new DriverWayBillsDb();
                var sheet = driverDb.GetDriverWayBillItemById((int)wayListId);
                var lst = new CreateWaysListReq();
                var employees = new List<int?>();
                employees.Add(sheet.DriverId);
                lst.OrgId = sheet.OrgId; lst.TrailerId = sheet.TraktorId;
                checkRes = (wLDb.checkAllResource(lst, false, employees, false, false, false) == true) ? 1 : 0;
                using (var trans = db.Database.BeginTransaction())
                {
                    var sh = db.Sheet_tracks.Where(t => t.shtr_id == wayListId).FirstOrDefault();
                    sh.exported = checkRes;
                    db.SaveChanges();
                    trans.Commit();
                }
            
            }

            //tasks
            if (tasId != null && newOrgId != null)
            {
                var tasList = db.Tasks.Where(t => t.tas_id == tasId).FirstOrDefault();
                var employees = new List<int?>();
                employees.Add(tasList.Sheet_tracks.emp_emp_id);
                employees.Add(tasList.emp_emp_id);
                var lst = new CreateWaysListReq();
                lst.OrgId = tasList.Sheet_tracks.org_org_id; lst.TrailerId = tasList.equi_equi_id; lst.PlantId = tasList.plant_plant_id;
                checkRes = (wLDb.checkAllResource(lst, false, employees, true, true,false) == true) ? 1 : 0;
            }
            //from dayTask
            if (dayTasId != null && newOrgId != null)
            {
                var detail = db.Day_task_detail.Where(d => d.dtd_id == dayTasId).FirstOrDefault();
                var employees = new List<int?>();
                employees.Add(detail.otv_emp_id);
                employees.Add(detail.emp1_emp_id);
                var lst = new CreateWaysListReq();
                lst.OrgId = detail.Day_task.org_org_id;
                lst.TrailerId = detail.equi_equi_id;
                lst.TraktorId = detail.trak_trak_id;
                lst.FieldId = detail.fiel_fiel_id;
                checkRes = (wLDb.checkAllResource(lst, true, employees, true, false, true) == true) ? 1 : 0;

            }
            //
            var result = GetCommonInfoList(waylisttype, newOrgId, checkRes);
      //     var listOrg = GetOrgList(orgId);
            result.EmployeesList = db.Employees.
                                    Where(e => (new[] { 1, 5 }).Contains(e.pst_pst_id) && e.deleted != true
                                     && ((newOrgId != null && checkRes != 1) ? e.org_org_id == newOrgId : true)
                                    ).
                                    Select(e => new DictionaryItemsDTO
                                    {
                                        Name = e.short_name,
                                        Id = e.emp_id,
                                    }).OrderByDescending(g => g.Name).ToList();
            result.AllFields = db.Fields.
                                    Where(e => e.deleted != true && ((newOrgId != null && checkRes != 1) ? e.org_org_id == newOrgId : true)).
                                    Select(e => new DictionaryItemForFields
                                    {
                                        Name = e.name,
                                        Id = e.fiel_id

                                    }).ToList();

                 var values = GetSelectedPlants();
                 result.FieldsAndPlants = (from f in db.Fields_to_plants
                                          where f.status == (int)StatusFtp.Approved && ((f.date_ingathering.Year == DateTime.Now.Year && 
                                          f.plant_plant_id != 36) ||
                                          values.Contains(f.fielplan_id)) && ((newOrgId != null && checkRes != 1) ?
                                          f.Fields.org_org_id == newOrgId : true)
                                          group new DictionaryPlantItemsDTO
                                          {
                                              plantId = f.plant_plant_id,
                                              Name = f.Plants.name + " (" + f.date_ingathering.Year + ")",
                                              Id = f.fielplan_id
                                          }
                                          by new
                                          {
                                              f.fiel_fiel_id,
                                              f.Fields.name,
                                          } into g
                                          select new DictionaryItemForFields
                                          {
                                              Id = g.Key.fiel_fiel_id,
                                              Name = g.Key.name,
                                              Values = g.ToList()
                                          }).OrderByDescending(g => g.Name).ToList();

            //культура 
          foreach (var item in result.FieldsAndPlants)
            {
                item.Values.Add(new DictionaryItemForFields
                {
                    Id = -1,
                    Name = "Культура",
                });
            }

            var dopObjects = db.Fields.Where(p => p.deleted != true && p.type == 2
                 && ((newOrgId != null && checkRes != 1) ? p.org_org_id == newOrgId : true)).Select(p => new DictionaryItemForFields
            {
                Id = p.fiel_id,
                Name = p.name
            }).ToList();
            result.FieldsAndPlants.AddRange(dopObjects);

            return result;
        }

        //используемые культуры
        public List<int> GetSelectedPlants() {
            var curYear = DateTime.Now.Year;
            var fielplan = db.Selected_Plants.Where(t => t.type == 1 || (t.type == 2 && t.year > curYear)).ToList();
            var fielplanIds = new List<int>();
            var values = new List<int>();
            for (int i = 0; i < fielplan.Count; i++)
            {
                var plantId = fielplan[i].plant_plant_id;
                var year = fielplan[i].year;
                var v = db.Fields_to_plants.Where(t => t.plant_plant_id == plantId && t.date_ingathering.Year == year).
             Select(t => t.fielplan_id).ToList();
                values.AddRange(v);
            }
            return values;
        } 

        protected WaysListForCreateModel GetInfoForCreateWaysListByShiftTasks(int DetailId, int type)
        {
            var detail = db.Day_task_detail.Where(d => d.dtd_id == DetailId).FirstOrDefault();

            var traktorList = db.Trakt_to_group_trakt.Where(t => t.grtrak_grtrak_id == detail.grtrak_grtrak_id).Select(t => t.trakt_trakt_id).ToList();
            var EquiList = db.Equip_to_group_equip.Where(t => t.gpequi_gpequi_id == detail.grequi_grequi_id).Select(t => t.equi_equi_id).ToList();
            List<int> empList = new List<int>();
            if (detail.emp1_emp_id != null) empList.Add((int)detail.emp1_emp_id);
            if (detail.emp2_emp_id != null) empList.Add((int)detail.emp2_emp_id);

            var result = GetCommonInfoList(1, null, null);
            result.EquipmnetsList.ForEach(p => p.Bold = p.Id.HasValue && EquiList.Contains((int)p.Id) ? true : false);
            result.TasksList.ForEach(p => p.Bold = p.Id.HasValue && detail.tptas_tptas_id == p.Id ? true : false);

            result.TraktorsList = db.Traktors.Where(t => t.deleted != true).
                                   Where(t => t.id_type_way_list == type).
                                   Select(t => new DictionaryItemsTraktors
                                   {
                                       Name = t.name,
                                       Id = t.trakt_id,
                                       RegNum = t.reg_num,
                                       Name_fuel = new DictionaryItemsDTO
                                       {
                                           Id = t.tpf_tpf_id,
                                           Name = t.Type_fuel.name
                                       },
                                       Bold = traktorList.Contains((int)t.trakt_id) ? true : false
                                   }).OrderByDescending(g => g.Bold).ThenBy(g => g.Name).ToList();

            result.EmployeesList = db.Employees.Where(e => e.deleted != true && (e.pst_pst_id == 1 || e.pst_pst_id == 5)).
                                    Select(e => new DictionaryItemsDTO
                                    {
                                        Name = e.short_name,
                                        Id = e.emp_id,
                                        Bold = empList.Contains((int)e.emp_id) ? true : false,
                                    }).OrderByDescending(g => g.Bold).ThenBy(g => g.Name).ToList();

            var mainChefs = result.ChiefsList.Where(c => c.Id == 30 || c.Id == 40 || c.Id == 31).ToList();

            foreach (var chief in mainChefs)
            {
                result.ChiefsList.Remove(chief);
                result.ChiefsList.Insert(0, chief);
            }

            result.FieldsAndPlants = (from f in db.Fields_to_plants
                                      where f.status == (int)StatusFtp.Approved && f.Fields.deleted != true && f.Plants.deleted != true
                                       && f.date_ingathering.Year.CompareTo(DateTime.Now.Year) >= 0
                                      group new DictionaryPlantItemsDTO
                                      {
                                          plantId = f.plant_plant_id,
                                          Name = f.Plants.name + " (" + f.date_ingathering.Year + ")",
                                          Id = f.fielplan_id
                                      } by new
                                      {
                                          f.fiel_fiel_id,
                                          f.Fields.name,
                                          Bold = f.Fields.fiel_id == detail.fiel_fiel_id ? true : false
                                      } into g
                                      select new DictionaryItemForFields
                                      {
                                          Id = g.Key.fiel_fiel_id,
                                          Name = g.Key.name,
                                          Bold = g.Key.Bold,
                                          Values = g.ToList()
                                      }).OrderByDescending(l => l.Bold).ThenBy(g => g.Name).ToList();
            //культура 
            foreach (var item in result.FieldsAndPlants)
            {
                item.Values.Add(new DictionaryItemForFields
                {
                    Id = -1,
                    Name = "Культура",
                });
            }
            var dopObjects = db.Fields.Where(p => p.deleted != true && p.type == 2).
                Select(p => new DictionaryItemForFields
               {
                   Id = p.fiel_id,
                   Name = p.name
               }).ToList();
            result.FieldsAndPlants.AddRange(dopObjects);
            return result;
        }

        protected string checkWayListNum(string num, bool? is_repair, int? id_way_bill, int wltype)
        {
            if (is_repair.HasValue && is_repair.Value)
            {
                if (id_way_bill.HasValue)
                {
                    return num;
                }
                else
                {
                    using (var trans = db.Database.BeginTransaction())
                    {
                        var nCol = db.Shtr_num.Where(p => p.id == 1).First();
                        var curM = DateTime.Now.Month;
                        if (nCol.month < curM)
                        {
                            nCol.month = curM;
                            nCol.num = 1;
                        }
                        var n = "Р/" + (curM.ToString().Length == 1 ? ("0" + curM.ToString()) : curM.ToString()) + "/" + nCol.num.ToString();
                        nCol.num++;
                        db.SaveChanges();
                        trans.Commit();
                        return n;
                    }
                }
            }
            else
            {
                var n = db.Sheet_tracks.Where(p => p.shtr_type_id == wltype && p.date_begin.Year == DateTime.Now.Year && p.num_sheet == num).FirstOrDefault();
                if (n != null)
                {
                    throw new BllException(14, "Путевый лист с таким именем уже был создан", new List<string> { num });
                }
                else
                {
                    return num;
                }
            }
        }
        //пересчет топлива в путевых листах
        public string RecountFuelForSheet() {
            var tasks = db.Tasks.Where(t => t.Sheet_tracks.date_begin.Year == 2018).ToList();
            using (var trans = db.Database.BeginTransaction())
            {
                for (int i = 0; i < tasks.Count; i++)
                {
                    if (tasks[i].Sheet_tracks.Traktors != null)
                    {
                        tasks[i].fuel_cost = (tasks[i].consumption_fact ?? 0) * (decimal)(tasks[i].Sheet_tracks.Traktors.Type_fuel.current_price ?? 0);
                    }
                    if (i % 100 == 0) {
                        db.SaveChanges();
                    }
                }
                db.SaveChanges();
                trans.Commit();
            }


       return "df";
       }

        public RateResModel GetRate(RateReq req)
        {
            int? grId = 0;
            if (req.IdTrakt.HasValue)
            {
                grId = db.Traktors.Where(p => p.trakt_id == req.IdTrakt).Select(p => p.id_grtrakt).FirstOrDefault();
            }
            else {
                var trakt_id = db.Sheet_tracks.Where(t => t.shtr_id == req.SheetId).Select(t => t.trakt_trakt_id).FirstOrDefault();
                grId = db.Traktors.Where(p => p.trakt_id == trakt_id).Select(p => p.id_grtrakt).FirstOrDefault();
            }

          req.IdPlant = db.Fields_to_plants.Where(t => t.fielplan_id == req.IdPlant).Select(t => t.plant_plant_id).FirstOrDefault();

            return GetRate(req.IdTptas, req.IdAgroreq, grId, req.IdEqui, req.IdPlant);
        }


    }
}
