﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LogusSrv.CORE.Exceptions;
using LogusSrv.DAL.DBModel;
using LogusSrv.DAL.Entities.DTO;
using LogusSrv.DAL.Entities.DTO.DayTaskExt;
using LogusSrv.DAL.Entities.Res;
using LogusSrv.DAL.Entities.Req;
using LogusSrv.DAL.Enums;
using LogusSrv.DAL.Utils;
using LogusSrv.DAL.Entities.DTO.SeedingBridle;
using LogusSrv.DAL.Entities.DTO.ExportExcel;
using System.Globalization;
using System.Data.Entity.SqlServer;
using System.Data.Entity;
using LogusSrv.DAL.Entities.DTO.ActsSzr;
using System.IO;
using OfficeOpenXml;

namespace LogusSrv.DAL.Operations
{
    public class TechCardDb
    {
        protected readonly LogusDBEntities db = new LogusDBEntities();
       // protected readonly WayListBase wl = new WayListBase();
        public decimal? salary = 0; 
        public decimal? fuel_cost = 0;
        public double? szr1 = 0;
        public double? chemicalFertilizers1 = 0;
        public double? seed1 = 0;
        public double? dopMaterial = 0;
        public int? tech = null; public decimal? sum = 0;
        public List<DictionaryItemsDTO>  type_fuel;
        public List<TechCardModel> GetTechCards()
        {
            var list = (from tc in db.TC
                        where tc.id_tc == null && tc.Plants.deleted == false && tc.Type_sorts_plants.deleted == false
                         && tc.Fields.deleted == false
                        select new TechCardModel
                        {
                            id = tc.id,
                            plant_plant_id = tc.plant_plant_id,
                            plant_name = tc.Plants.name,
                            tpsort_tpsort_id = tc.tpsort_tpsort_id,
                            tpsort_name = tc.Type_sorts_plants.name,
                            fields = (from tc1 in db.TC
                                      where tc1.id_tc == tc.id
                                      select new Field
                                      {
                                          id = tc1.Fields.fiel_id,
                                          name = tc1.Fields.name,
                                          area = tc1.Fields.area
                                      }
                                   ).ToList()
                        }
                ).OrderBy(p => p.plant_name).ToList();
            return list;
        }

        public List<Field> GetFields()
        {
            var list = (from f in db.Fields
                        join tcDb in db.TC on f.fiel_id equals tcDb.fiel_fiel_id into tcLJ
                        from tc in tcLJ.DefaultIfEmpty()
                        where tc.Plants.deleted == false && tc.Fields.deleted
                        select new Field
                        {
                            id = f.fiel_id,
                            name = f.name,
                            area = f.area,
                            plant = tc.Plants.name
                        }).ToList();
            return list;
        }

        public List<TCTask> GetWayLists()
        {
            var list = (from t in db.Tasks
                        where t.Sheet_tracks.date_begin.Year == DateTime.Now.Year && t.Fields_to_plants.Plants.deleted == false && t.Fields.deleted==false
                        orderby t.Sheet_tracks.date_begin
                        select new TCTask
                        {
                            id = t.tas_id,
                            date = t.Sheet_tracks.date_begin,
                            trakt = t.Sheet_tracks.Traktors.model + (string.IsNullOrEmpty(t.Sheet_tracks.Traktors.reg_num) ? "(" + t.Sheet_tracks.Traktors.reg_num + ")" : ""),
                            id_tc = t.id_tc,
                            plant_id = t.Fields_to_plants.Plants.plant_id,
                            plant_name = t.Fields_to_plants.Plants.name,
                            field_id = t.fiel_fiel_id,
                            field_name = t.Fields.name
                        }).ToList();
            return list;
        }

        public string SetWayListToTC(TCTask req)
        {
            using (var trans = db.Database.BeginTransaction())
            {
                var wl = db.Tasks.Where(d => d.tas_id == req.id).FirstOrDefault();
                wl.id_tc = req.add_value ? req.id_tc : null;
                db.SaveChanges();
                trans.Commit();
                return "";
            }
        }

        public InfoForTCDetail GetInfoForTc()
        {
            InfoForTCDetail result = new InfoForTCDetail();
            var yearList = new List<DictionaryItemsDTO>();
            var endYear = DateTime.Now.Year + 1;
            for (var i = endYear; i >= endYear-10; i--)
            {
                var item = new DictionaryItemsDTO { Id = i, Name = i.ToString() };
                yearList.Add(item);
            }
            result.YearList = yearList.ToList();

            result.WorkList = (from task in db.Type_tasks
                               where task.deleted != true
                               select new WorkDictionary
                               {
                                   Id = task.tptas_id,
                                   Name = task.name,
                                   Consumption = task.fuel_consumption,
                                   EmpPrice = task.rate,
                                   ShiftRate = task.shift_output
                               }).ToList();
            result.EquipmentList = (from task in db.Equipments
                                    where task.deleted != true
                                    select new DictionaryItemsDTO
                                    {
                                        Id = task.equi_id,
                                        Name = task.name
                                    }).ToList();
            result.TechList = (from task in db.Traktors
                               where task.deleted != true
                               select new DictionaryItemsDTO
                               {
                                   Id = task.trakt_id,
                                   Name = task.name + " ("+ task.reg_num + ")",
                               }).ToList();
            result.TypeFuelList = db.Type_fuel.Where(p => p.fuel_flag == true && p.deleted != true).Select(tf => new DictionaryItemsDTO
            {
                Id = tf.id,
                Name = tf.name,
                Price = tf.current_price
            }).ToList();
            result.ServiceList = (from s in db.Service
                                  where s.deleted != true 
                                  select new DictionaryItemsDTO
                                  {
                                      Id = s.id,
                                      Name = s.name
                                  }).ToList();
            ///
          var material = GetListMaterial();

          var dopmat = db.Database.SqlQuery<DictionaryMaterialDTO>(
          string.Format(@"select m.mat_id as Id, m.name as Name, m.mat_type_id as Mat_type_id from Materials m where deleted=0")).ToList();
            //выбираем которых не хватает
          for (int i = 0; i < material.Count; i++) {
              for (int j = 0; j < dopmat.Count; j++)
              {
                  if (material[i].Id == dopmat[j].Id) {
                      dopmat.RemoveAt(j); j--; break;
                  }
              }
          }
          for (int j = 0; j < dopmat.Count; j++)
          {
              material.Add(dopmat[j]);
          }


          result.SzrList = new List<DictionaryMaterialDTO>();
          result.FertilizerList = new List<DictionaryMaterialDTO>();
          result.SeedList = new List<DictionaryMaterialDTO>();
          result.DopMaterialList = new List<DictionaryMaterialDTO>();
           for (int i=0; i<material.Count;i++) {
               switch (material[i].Mat_type_id)
                {
                    case 1:
                        result.SzrList.Add(material[i]);
                        break;
                    case 2:
                        result.FertilizerList.Add(material[i]);
                        break;
                    case 4:
                        result.SeedList.Add(material[i]);
                        break;
                    case 5:
                        result.DopMaterialList.Add(material[i]);
                        break;
                }
            }
            return result;
        }
        //получение с материалов с баланса ТМЦ
        public List<DictionaryMaterialDTO> GetListMaterial()
        {
            var material = db.Database.SqlQuery<DictionaryMaterialDTO>(
           string.Format(@"select m.mat_id as Id, m.name as Name,m.mat_type_id as Mat_type_id, Convert(numeric(19,2),AVG(t.price)) as Price 
          from TMC_Current t, Materials m where t.mat_mat_id=m.mat_id
          group by m.mat_id,m.name,m.mat_type_id")).ToList();
            return material;
        }

        public List<PlantSortFieldDictionary> GetPlantSortFieldByYear(int year)
        {
            var PlantsAndSorts = (

                                  from ftp in db.Fields_to_plants
                                  where ftp.date_ingathering.Year == year && ftp.Plants.deleted == false && ftp.Fields.deleted == false 

                                  join sortPlan in db.Sorts_plants_data on ftp.fielplan_id equals sortPlan.fielplan_fielplan_id into spdLj
                                  from spd in spdLj.DefaultIfEmpty()

                                  join indSort in db.Ind_sorts_plants_data.Where(i => i.indsort_indsort_id == 1) on spd.sortdat_id equals indSort.sortdat_sortdat_id into indSortLj
                                  from ispd in indSortLj.DefaultIfEmpty()

                                  group new { ftp, spd, ispd } by new { ftp.plant_plant_id, ftp.Plants.name } into g
                                  select new PlantSortFieldDictionary
                                  {
                                      Id = g.Key.plant_plant_id,
                                      Name = g.Key.name,
                                      Values = (
                                                from s in g.Where(ga => ga.spd != null && ga.spd.Type_sorts_plants.deleted == false && ga.ftp.Fields.deleted == false)
                                                group new FieldDictionary
                                                {
                                                    Id = s.ftp.fiel_fiel_id,
                                                    Name = s.ftp.Fields.name,
                                                    Area = s.ispd.value_plan.HasValue ? s.ispd.value_plan : s.ispd.value_fact //do
                                                } by new
                                                {
                                                    s.spd.tpsort_tpsort_id,
                                                    s.spd.Type_sorts_plants.name
                                                } into g2
                                                select new SortFieldDictionary
                                                {
                                                    Id = g2.Key.tpsort_tpsort_id,
                                                    Name = g2.Key.name,
                                                    Fields = g2.ToList()

                                                }).ToList(),
                                      Fields = (from k in g.Select(x => new { x.ftp, x.ispd })
                                                group k by new { k.ftp.fiel_fiel_id, k.ftp.Fields.name } into fg
                                                select new FieldDictionary
                                                {
                                                    Id = fg.Key.fiel_fiel_id,
                                                    Name = fg.Key.name,
                                                    Area = fg.Sum(p => p.ispd.value_plan.HasValue ? p.ispd.value_plan : (p.ispd.value_fact ?? 0)) //do
                                                }).ToList()
                                  }).OrderBy(g => g.Name).ToList();

            return PlantsAndSorts;
        }

        public string CreateUpdateTC(TCDetailModel tc)
        {
            for (int i = 0; i < tc.Works.Count;i++)
            {
                //utc время
                tc.Works[i].DateStart = tc.Works[i].DateStart.Value.AddHours(3);
                tc.Works[i].DateEnd = tc.Works[i].DateEnd.Value.AddHours(3);

         var dis = tc.Works.Where(v => DateTime.Compare((DateTime)tc.Works[i].DateStart, (DateTime)v.DateStart) == 0 && v.Name.Name == tc.Works[i].Name.Name
         && v.Id != tc.Works[i].Id && DateTime.Compare((DateTime)tc.Works[i].DateEnd, (DateTime)v.DateEnd) == 0 &&
        v.UnicFlag == tc.Works[i].UnicFlag && v.Count == tc.Works[i].Count && v.Factor == tc.Works[i].Factor &&
        v.Workload == tc.Works[i].Workload).Select(v => v.index).Sum();
  if (dis > 0) { throw new BllException(14, "Невозможно сохранить две одинаковые записи в назначенных работах"); }
                if (String.IsNullOrEmpty(tc.Works[i].Name.Name)) { throw new BllException(14, "Не выбрано название работы"); }
                if (String.IsNullOrEmpty(tc.Works[i].ShiftRate.ToString())) { throw new BllException(14, "Не заполнено поле Сменная норма выработки"); }
            }

            for (int i = 0; i < tc.SupportStuff.Count; i++)
            {
            if (String.IsNullOrEmpty(tc.SupportStuff[i].NameSelected.Name)) { throw new BllException(14, "Не заполнено поле Наименование работы для Вспомогательного персонала"); }
            }
            for (int i = 0; i < tc.DataServices.Count; i++)
            {
                if (String.IsNullOrEmpty(tc.DataServices[i].NameService.Name)) { throw new BllException(14, "Не выбран тип услуг"); }
            }

            for (int i = 0; i < tc.DataChemicalFertilizers.Count; i++)
            {
                if (tc.DataChemicalFertilizers[i].NameFertilizer == null  || tc.DataChemicalFertilizers[i].NameFertilizer.Name == null) { throw new BllException(14, "Не заполнено поле Материал"); }
                if (String.IsNullOrEmpty(tc.DataChemicalFertilizers[i].NameSelectedWorkForMaterial.Name)) { throw new BllException(14, "Не заполнено поле Наименование работы для материалов"); }
            }

            for (int i = 0; i < tc.DataSeed.Count; i++)
            {
                if (tc.DataSeed[i].NameSeed == null || tc.DataSeed[i].NameSeed.Name == null) { throw new BllException(14, "Не заполнено поле Материал"); }
                if (String.IsNullOrEmpty(tc.DataSeed[i].NameSelectedWorkForMaterial.Name)) { throw new BllException(14, "Не заполнено поле Наименование работы для материалов"); }
            }
            for (int i = 0; i < tc.DataSZR.Count; i++)
            {
                if (tc.DataSZR[i].NameSZR == null || tc.DataSZR[i].NameSZR.Name == null) { throw new BllException(14, "Не заполнено поле Материал"); }
                if (String.IsNullOrEmpty(tc.DataSZR[i].NameSelectedWorkForMaterial.Name)) { throw new BllException(14, "Не заполнено поле Наименование работы для материалов"); }
            }
            for (int i = 0; i < tc.DataDopMaterial.Count; i++)
            {
                if (tc.DataDopMaterial[i].NameDopMaterial == null || tc.DataDopMaterial[i].NameDopMaterial.Name == null) { throw new BllException(14, "Не заполнено поле Материал"); }
                if (String.IsNullOrEmpty(tc.DataDopMaterial[i].NameSelectedWorkForMaterial.Name)) { throw new BllException(14, "Не заполнено поле Наименование работы для материалов"); }
            }

            using (var trans = db.Database.BeginTransaction())
            {
                try
                {
                    var curTC = tc.TC;
                    int? TempStatus = null;
                    if (!curTC.Id.HasValue)
                    {
                        if (curTC.CheckTemp == 1) { TempStatus = 1; }
                        else { TempStatus = 0; }
                        //insert TC
                        var newId = (db.TC.Max(t => (int?)t.id) ?? 0) + 1;
                        curTC.Id = newId;
                        db.TC.Add(new TC
                        {
                            id = (int)curTC.Id,
                            plant_plant_id = curTC.PlantId,
                            tpsort_tpsort_id = curTC.SortId,
                            year = curTC.YearId,
                            fiel_fiel_id = curTC.FieldId,
                            changing_date = DateTime.Now,
                            id_tc = TempStatus,
                        });
                    }
                    else
                    {
                        //update TC
                        var updatedTC = (from t in db.TC where t.id == curTC.Id select t).FirstOrDefault();
                        updatedTC.plant_plant_id = curTC.PlantId;
                        updatedTC.tpsort_tpsort_id = curTC.SortId;
                        updatedTC.year = curTC.YearId;
                        updatedTC.fiel_fiel_id = curTC.FieldId;
                        updatedTC.changing_date = DateTime.Now;
                        updatedTC.id_tc = 0;
                        var oldP = db.TC_param_value.Where(o => o.id_tc == curTC.Id).ToList();
                        var oldTech = db.TC_operation_to_Tech.Where(o => o.TC_operation.id_tc == curTC.Id).ToList();

                        db.TC_operation_to_Tech.RemoveRange(oldTech);
                        //db.TC_operation.RemoveRange(oldOp);
                        db.TC_param_value.RemoveRange(oldP);
                        //TODO
                    }

                    var oldOp = db.TC_operation.Where(o => o.id_tc == curTC.Id).ToList();

                    db.TC_param_value.Add(new TC_param_value
                    {
                        id_tc = (int)curTC.Id,
                        id_tc_param = 1,
                        value = curTC.Area,
                        plan = 1
                    });
                    db.TC_param_value.Add(new TC_param_value
                    {
                        id_tc = (int)curTC.Id,
                        id_tc_param = 2,
                        value = curTC.Productivity,
                        plan = 1
                    });
                    db.TC_param_value.Add(new TC_param_value
                    {
                        id_tc = (int)curTC.Id,
                        id_tc_param = 3,
                        value = curTC.GrossHarvest,
                        plan = 1
                    });

                    //insert параметры свода затрат
                    var total = tc.DataTotal;
                    var li = total.Count;
                    for (var i = 0; i < li; i++)
                    {
                        var parameters = total[i].ParamIds;
                        addParam(curTC.Id.Value, parameters[0], total[i].Total);
                        addParam(curTC.Id.Value, parameters[1], total[i].PerT);
                        addParam(curTC.Id.Value, parameters[2], total[i].PerGa);
                    }

                    //НЗП
                    if (tc.NZP != null && tc.NZP.Salary != null && tc.NZP.Salary != 0) addParam(curTC.Id.Value, 53, tc.NZP.Salary);
                    if (tc.NZP != null && tc.NZP.Energy != null && tc.NZP.Energy != 0) addParam(curTC.Id.Value, 54, tc.NZP.Energy);
                    if (tc.NZP != null && tc.NZP.SZR != null && tc.NZP.SZR != 0) addParam(curTC.Id.Value, 55, tc.NZP.SZR);
                    if (tc.NZP != null && tc.NZP.ChemicalFertilizers != null && tc.NZP.ChemicalFertilizers != 0) addParam(curTC.Id.Value, 56, tc.NZP.ChemicalFertilizers);
                    if (tc.NZP != null && tc.NZP.Seed != null && tc.NZP.Seed != 0) addParam(curTC.Id.Value, 57, tc.NZP.Seed);
                    if (tc.NZP != null && tc.NZP.DopMaterial != null && tc.NZP.DopMaterial != 0) addParam(curTC.Id.Value, 60, tc.NZP.DopMaterial);
                    //insert услуги
                    if (tc.DataServices != null)
                    {
                      
                        for (int i = 0; i < tc.DataServices.Count; i++)
                        {
                            tc.DataServices[i].Id = null;
                        }

                        var doc = db.TC_to_service.Where(s => s.id_tc == curTC.Id.Value).ToList();
                        if (doc != null)
                        {
                            db.TC_to_service.RemoveRange(doc);
                        }

                        db.SaveChanges();    
                        insertServices(tc.DataServices, curTC.Id.Value);
                    }

                    //insert задачи
                    var works = tc.Works;
                    li = works.Count;
            var workList = new List<TC_operation>();
                    var pIDs = new[] { 33, 32, 35, 39, 50, 51, 52 , 58, 59};
                    var pNames = new[] { "Workload", "ShiftCount", "EmpCost", "FuelCost", "ShiftRate", "UnicFlag", "EmpPrice", "Rate", "Consumption" };
            var newOperId = db.TC_operation.Max(t => (int?)t.id) ?? 0;
            newOperId++;

            for (var i = 0; i < li; i++)
            {
                var w = works[i];
                var wType = w.GetType();
                int operId;
                TC_operation oper;
                if (w.Id != null)
                {
                    operId = w.Id.Value;
                            oper = (from o in db.TC_operation
                                    where o.id == operId
                                    select o).FirstOrDefault();
                    oper.id_tc = curTC.Id.Value;
                    oper.tptas_tptas_id = w.Name.Id.Value;
                    oper.multiplicity = w.Count;
                    oper.date_start = w.DateStart;
                    oper.date_finish = w.DateEnd;
                    oper.factor = w.Factor;

                    oldOp.Remove(oldOp.Where(p => p.id == w.Id.Value).FirstOrDefault());
                }
                else
                {
                    oper = new TC_operation
                    {
                        id = newOperId,
                        id_tc = (int)curTC.Id,
                        tptas_tptas_id = (int)w.Name.Id,
                        multiplicity = w.Count,
                        date_start = w.DateStart,
                        date_finish = w.DateEnd,
                                factor = w.Factor
                    };
                    db.TC_operation.Add(oper);
                    operId = newOperId;
                    newOperId++;
                }

                workList.Add(oper);
                for (var j = 0; j < pIDs.Length; j++)
                {
                    var newParamValue = new TC_param_value
                    {
                        id_tc = (int)curTC.Id,
                        plan = 1,
                        id_tc_operation = operId,
                        id_tc_param = pIDs[j],
                        value = (float?)wType.GetProperty(pNames[j]).GetValue(w),
                    };
                    db.TC_param_value.Add(newParamValue);
                }

                var equip = w.Equipment;
                if (equip != null)
                {
                    for (var j = 0; j < equip.Count; j++)
                    {
                        db.TC_operation_to_Tech.Add(new TC_operation_to_Tech
                        {
                            it_tc_operation = operId,
                            equi_equi_id = equip[j].Id
                        });
                    }
                }

                var traks = w.Tech;
                if (traks != null)
                {
                    for (var j = 0; j < traks.Count; j++)
                    {
                        db.TC_operation_to_Tech.Add(new TC_operation_to_Tech
                        {
                            it_tc_operation = operId,
                            trakt_trakt_id = traks[j].Id
                        });
                    }
                }
            }
            if (oldOp.Count >= 0)
            {
                db.TC_operation.RemoveRange(oldOp);
            }
            db.SaveChanges();

                    //insert вспомогательный персонал
                    var support = tc.SupportStuff;
                    for (var i = 0; i < support.Count; i++)
                    {
                        var s = support[i];
                        var staffCount = new TC_param_value
                        {
                            id_tc = (int)curTC.Id,
                            id_tc_param = 36,
                            id_tc_operation = (int)GetOperationId(workList, (int)s.NameSelected.Id),
                            value = s.Count,
                            plan = 1
                        };
                        var staffCost = new TC_param_value
                        {
                            id_tc = (int)curTC.Id,
                            id_tc_param = 38,
                            id_tc_operation = (int)GetOperationId(workList, (int)s.NameSelected.Id),
                            value = s.Cost,
                            plan = 1
                        };
                        //расценка р/ПД
                        var staffPrice = new TC_param_value
                        {
                            id_tc = (int)curTC.Id,
                            id_tc_param = 65,
                            id_tc_operation = (int)GetOperationId(workList, (int)s.NameSelected.Id),
                            value = s.Price,
                            plan = 1
                        };
                        //наименование работ
                        var staffNameWork = new TC_param_value
                        {
                            id_tc = (int)curTC.Id,
                            id_tc_param = 64,
                            id_tc_operation = (int)GetOperationId(workList, (int)s.NameSelected.Id),
                            value = s.NameSelected.Id,
                            plan = 1
                        };
                        //id из TC_Operation
                        var staffIndex = new TC_param_value
                        {
                            id_tc = (int)curTC.Id,
                            id_tc_param = 66,
                            id_tc_operation = (int)GetOperationId(workList, (int)s.NameSelected.Id),
                            value = s.id_tc_oper,
                            plan = 1
                        };
                   
                        db.TC_param_value.Add(staffCount);
                        db.TC_param_value.Add(staffCost);
                        db.TC_param_value.Add(staffNameWork);
                        db.TC_param_value.Add(staffPrice);
                        db.TC_param_value.Add(staffIndex);
                    }
                    db.SaveChanges();

                    var szr = tc.DataSZR;
                    for (var i = 0; i < szr.Count; i++)
                    {
                        var s = szr[i];
              //          saveMaterials(curTC.Id, GetOperationId(workList, (int)s.NameSelectedWorkForMaterial.Id), s.NameSZR.Id,
                        saveMaterials(curTC.Id, s.id_tc_oper, s.NameSZR.Id,
                             (float)s.Price, (float)s.Cost, (float)s.ConsumptionRate, s.MaterialArea, s.id_tc_oper);
                    }

                    var fertilizers = tc.DataChemicalFertilizers;
                    for (var i = 0; i < fertilizers.Count; i++)
                    {
                        var s = fertilizers[i];
                //        saveMaterials(curTC.Id, GetOperationId(workList, (int)s.NameSelectedWorkForMaterial.Id), s.NameFertilizer.Id,
                        saveMaterials(curTC.Id, s.id_tc_oper, s.NameFertilizer.Id,
                             (float)s.Price, (float)s.Cost, (float)s.ConsumptionRate, s.MaterialArea, s.id_tc_oper);
                    }

                    var seeds = tc.DataSeed;
                    for (var i = 0; i < seeds.Count; i++)
                    {
                        var s = seeds[i];
                //        saveMaterials(curTC.Id, GetOperationId(workList, (int)s.NameSelectedWorkForMaterial.Id), s.NameSeed.Id,
                        saveMaterials(curTC.Id, s.id_tc_oper, s.NameSeed.Id,
                             (float)s.Price, (float)s.Cost, (float)s.ConsumptionRate, s.MaterialArea, s.id_tc_oper);
                    }
                    var dopMaterials = tc.DataDopMaterial;
                    for (var i = 0; i < dopMaterials.Count; i++)
                    {
                        var s = dopMaterials[i];
                  //      saveMaterials(curTC.Id, GetOperationId(workList, (int)s.NameSelectedWorkForMaterial.Id), s.NameDopMaterial.Id,
                        saveMaterials(curTC.Id, s.id_tc_oper, s.NameDopMaterial.Id,
                             (float)s.Price, (float)s.Cost, (float)s.ConsumptionRate, s.MaterialArea, s.id_tc_oper);
                    }
                    db.SaveChanges();
                    trans.Commit();
                }
                catch (Exception)
                {
                    trans.Rollback();
                    throw;
                }
            }
            return "success";
        }

        private void insertServices(List<DataService> list, int id_tc)
        {
            var ss = new List<TC_to_service>();
            for (var i = 0; i < list.Count; i++)
            {
                var l = list[i];
                TC_to_service s;
                if (l.Id.HasValue)
                {
                    s = db.TC_to_service.Where(p => p.id == l.Id.Value).First();
                    s.id_service = l.NameService.Id.Value;
                    s.cost =(float) l.Cost;
                }
                else
                {
                    s = new TC_to_service
                    {
                        id_tc = id_tc,
                        id_service = l.NameService.Id.Value,
                        cost = (float)l.Cost
                    };
                    ss.Add(s);
                }
            }
            if (ss.Count > 0)
            {
                db.TC_to_service.AddRange(ss);
            }
            //
          
      
            
            db.SaveChanges();
        }

        private void addParam(int id_tc, int id_tc_param, double? value) // float? value)
        {
            db.TC_param_value.Add(new TC_param_value
            {
                id_tc = id_tc,
                id_tc_param = id_tc_param,
                value = value,
                plan = 1
            });
        }

        private void saveMaterials(int? tc_id, int? operId, int? matId, float price, float cost, float rate, float? mat_area, int? id_tc_oper)
        {
            db.TC_param_value.Add(CreateMaterialParamValue(tc_id, 44, operId, matId, price));
            db.TC_param_value.Add(CreateMaterialParamValue(tc_id, 42, operId, matId, cost));
            db.TC_param_value.Add(CreateMaterialParamValue(tc_id, 48, operId, matId, rate));
            db.TC_param_value.Add(CreateMaterialParamValue(tc_id, 67, operId, matId, (float)id_tc_oper));  //индексы для ТМЦ
            if (mat_area.HasValue)
            {
                db.TC_param_value.Add(CreateMaterialParamValue(tc_id, 49, operId, matId, mat_area.Value));
            }
        }

        private int? GetOperationId(List<TC_operation> opers, int id)
        {
            for (var i = 0; i < opers.Count; i++)
            {
                if (opers[i].tptas_tptas_id == id)
                {
                    return opers[i].id;
                }
            }
            return null;
        }

        private TC_param_value CreateMaterialParamValue(int? idtc, int idtcparam, int? idtcoperation, int? matmatid, float value)
        {
            var m = new TC_param_value
            {
                id_tc = (int)idtc,
                id_tc_param = idtcparam,
                id_tc_operation = idtcoperation,//(int)GetOperationId(workList, (int)m.NameSelectedWorkForMaterial.Id),
                mat_mat_id = (int)matmatid,
                value = value,
                plan = 1
            };
            return m;
        }

        public TCDetailModel GetTCById(int id)
        {
            var tc = new TCDetailModel();
            tc.NZP = new NZPModel();
            var works = (from w in db.TC_operation where w.id_tc == id orderby w.date_start select w).ToList();
            var ps = (from p in db.TC_param_value where p.id_tc == id orderby p.id select p).ToList();
            var psForTC = new List<TC_param_value>();     //done
            var psForTotal = new List<TC_param_value>();   //done
            var psForNZP = new List<TC_param_value>();    //TODO
            var psForWork = new List<TC_param_value>();   //done
            var psForStaff = new List<TC_param_value>();   //done
            var psForSzr = new List<TC_param_value>();  //done
            var psForFertilizer = new List<TC_param_value>();  //done
            var psForSeed = new List<TC_param_value>();  //done
            var psForDopMaterial = new List<TC_param_value>();

            for (var i = 0; i < ps.Count; i++)
            {
                var p = ps[i];
                if (p.id_tc_param < 4)
                {
                    psForTC.Add(p);
                }
                else if (p.id_tc_param <= 30)
                {
                    psForTotal.Add(p);
                }
                else if (p.id_tc_param >= 61 && p.id_tc_param <= 63)
                {
                    psForTotal.Add(p);
                }
                else if (p.mat_mat_id.HasValue)
                {
                    switch (p.Materials.mat_type_id)
                    {
                        case 1:
                            psForSzr.Add(p);
                            break;
                        case 2:
                            psForFertilizer.Add(p);
                            break;
                        case 4:
                            psForSeed.Add(p);
                            break;
                        case 5:
                            psForDopMaterial.Add(p);
                            break;
                        default:
                            break;
                    }
                }
                else if (p.id_tc_param == 36 || p.id_tc_param == 38 || p.id_tc_param == 64 || p.id_tc_param == 65 || p.id_tc_param == 66)
                {
                    psForStaff.Add(p);
                }
                else if (p.id_tc_param >= 53 && p.id_tc_param <= 57)
                {
                    if (p.id_tc_param == 53)
                        tc.NZP.Salary = (float?)p.value ?? 0;
                    else if (p.id_tc_param == 54)
                        tc.NZP.Energy = (float?)p.value ?? 0;
                    else if (p.id_tc_param == 55)
                        tc.NZP.SZR = (float?)p.value ?? 0;
                    else if (p.id_tc_param == 56)
                        tc.NZP.ChemicalFertilizers = (float?)p.value ?? 0;
                    else if (p.id_tc_param == 57)
                        tc.NZP.Seed = (float?)p.value ?? 0;
                }
                else if (p.id_tc_param == 60)
                    tc.NZP.DopMaterial = (float?)p.value ?? 0;
                else
                {
                    psForWork.Add(p);
                }
            }

            tc.TC = (from t in db.TC
                     where t.id == id
                     select new TCModel
                     {
                         Id = t.id,
                         PlantId = (int)t.plant_plant_id,
                         SortId = t.tpsort_tpsort_id,
                         YearId = (int)t.year,
                         FieldId = (int)t.fiel_fiel_id
                     }).FirstOrDefault();

            tc.TC.Area = (float?)GetParamValueById(psForTC, 1);
            tc.TC.Productivity = (float?)GetParamValueById(psForTC, 2);
            tc.TC.GrossHarvest = (float?)GetParamValueById(psForTC, 3);

            //total
            var DataTotal = new List<DataTotalModel>();
            var k = 0;
            var totalNames = new List<string> { "Всего", "НЗП", "ЗП", "Отчисления", "Топливо", "Семена", "Удобрения", "СЗР", "Услуги" };
            DataTotalModel CurrentTotal = new DataTotalModel();
            for (var i = 4; i <= 30; i++)
            {
                if (k == 3)
                {
                    DataTotal.Add(CurrentTotal);
                    totalNames.RemoveAt(0);
                    k = 0;
                }

                if (k == 0)
                {
                    CurrentTotal = new DataTotalModel
                    {
                        Name = totalNames[0],
                        ParamIds = new List<int> { i, i + 1, i + 2 }
                    };
                }

                var v = GetParamValueById(psForTotal, i);
                var value = v.HasValue ? (float)Math.Round(v.Value, 2) : 0;
                switch (k)
                {
                    case 0:
                        CurrentTotal.Total = value;
                        break;
                    case 1:
                        CurrentTotal.PerT = value;
                        break;
                    case 2:
                        CurrentTotal.PerGa = value;
                        break;
                    default:
                        break;
                }

                k++;
            }
            
            DataTotal.Add(CurrentTotal);
            tc.DataTotal = DataTotal;
            ////////////////////////////////////////////////
             DataTotal = new List<DataTotalModel>();
             k = 0;
             totalNames = new List<string> { "Пр. материалы" };
            CurrentTotal = new DataTotalModel();
            for (var i = 61; i <= 63; i++)
            {
                if (k == 3)
                {
                    DataTotal.Add(CurrentTotal);
                    totalNames.RemoveAt(0);
                    k = 0;
                }

                if (k == 0)
                {
                    CurrentTotal = new DataTotalModel
                    {
                        Name = totalNames[0],
                        ParamIds = new List<int> { i, i + 1, i + 2 }
                    };
                }

                var v = GetParamValueById(psForTotal, i);
                var value = v.HasValue ? (float)Math.Round(v.Value, 2) : 0;
                switch (k)
                {
                    case 0:
                        CurrentTotal.Total = value;
                        break;
                    case 1:
                        CurrentTotal.PerT = value;
                        break;
                    case 2:
                        CurrentTotal.PerGa = value;
                        break;
                    default:
                        break;
                }

                k++;
            }
            ///////////////////////////////
            tc.DataTotal.Insert(8, CurrentTotal);

            tc.DataServices = (from s in db.TC_to_service
                               where s.id_tc == id
                               select new DataService
                               {
                                   Id = s.id,
                                   Cost = s.cost,
                                   NameService = new DictionaryItemsDTO
                                   {
                                       Id = s.id_service,
                                       Name = s.Service.name
                                   }
                               }).ToList();

            //works
            tc.Works = new List<WorkModel>();

            var tech = (from t in db.TC_operation_to_Tech

                        where t.TC_operation.id_tc == id
                        group new { e = t.Equipments, tr = t.Traktors } by new { t.it_tc_operation } into g
                        select new
                        {
                            id = g.Key.it_tc_operation,
                            EqValues = g.Where(x => x.e != null).Select(x => new DictionaryItemsDTO { Id = x.e.equi_id, Name = x.e.name }).ToList(),
                            TrValues = g.Where(x => x.tr != null).Select(x => new DictionaryItemsDTO { Id = x.tr.trakt_id,
                                Name = x.tr.name + " (" + x.tr.reg_num + ")"  }).ToList()
                        }).ToList();

            tc.SupportStuff = new List<SupportStaffModel>();
            for (var i = 0; i < works.Count; i++)
            {
                var workDB = works[i];
                var allTechForWork = tech.Where(t => t.id == workDB.id).FirstOrDefault();
                var workload = GetParamValueByIdAndOperation(psForWork, 33, workDB.id);
                var fuelCost = GetParamValueByIdAndOperation(psForWork, 39, workDB.id);
                var shiftCount = GetParamValueByIdAndOperation(psForWork, 32, workDB.id);
                var shiftRate = GetParamValueByIdAndOperation(psForWork, 50, workDB.id);
                var rate = GetParamValueByIdAndOperation(psForWork, 58, workDB.id);     //тариф
                var fuelConsumption = GetParamValueByIdAndOperation(psForWork, 59, workDB.id);     //расход топлива из бд
                var unicFlag = GetParamValueByIdAndOperation(psForWork, 51, workDB.id);
                var empCost = GetParamValueByIdAndOperation(psForWork, 35, workDB.id);
                var price = GetParamValueByIdAndOperation(psForWork, 52, workDB.id);
                var empPrice = price;
                var FuelType = new List<DictionaryItemsDTO>();
                //
                if (allTechForWork != null && allTechForWork.TrValues.Count != 0)
                {
                   int idtech = (int) allTechForWork.TrValues[0].Id;
                   FuelType = (from t in db.Traktors
                               where t.trakt_id == idtech

                                    select new DictionaryItemsDTO
                                    {
                                        Id = t.Type_fuel.id,
                                        Name = t.Type_fuel.name,
                                        Price = t.Type_fuel.current_price,
                                    }).ToList();
                }
                else { FuelType = null; }
                //


                var w = new WorkModel
                {
                    Id = workDB.id,
                    Name = new DictionaryItemsDTO
                    {
                        Id = workDB.Type_tasks.tptas_id,
                        Name = workDB.Type_tasks.name
                    },
                    Count = workDB.multiplicity,
                    DateStart = workDB.date_start,
                    DateEnd = workDB.date_finish,
                    Factor = workDB.factor,
                    Workload = (float?)workload,
                    Rate = (float?)rate,
                    EmpCost = (float?)empCost,
                    FuelCost = (float?)fuelCost,
                    Comment = workDB.comment,
                    Consumption = (float?)fuelConsumption,
                    FuelCount = (float?)(fuelConsumption * workload),
                    FuelType = FuelType!=null ? new DictionaryItemsDTO
                    {
                    Id = FuelType[0].Id,
                    Name = FuelType[0].Name,
                    Price = FuelType[0].Price,
                    } : null,
                    //
                    ShiftRate = (float?)(shiftRate.HasValue && shiftRate != 0 ? shiftRate
                        : ((shiftCount != 0 && workload.HasValue)
                            ? workload.Value / shiftCount
                            : (float?)workDB.Type_tasks.shift_output)),
                    ShiftCount = (float?)shiftCount,
                    EmpPrice = (float?)empPrice,
                    Equipment = allTechForWork != null ? allTechForWork.EqValues : new List<DictionaryItemsDTO>(),
                    Tech = allTechForWork != null ? allTechForWork.TrValues : new List<DictionaryItemsDTO>(),
                    UnicFlag = (float?)unicFlag,
                    index=i+1,
                
                };
                tc.Works.Add(w);
            }
            /////staff
            for (var i = 0; i < psForStaff.Count; i++)
            {
                var p = psForStaff[i];
                var Id = GetParamValueById(psForStaff, 64);
                if (!String.IsNullOrEmpty(Id.ToString()))
                {
                    var stuff = new SupportStaffModel
                    {
                        NameSelected = new DictionaryItemsDTO
                        {
                            Id = (int?)Id,
                            Name = db.Type_tasks.Where(t => t.tptas_id == (int)Id).Select(t => t.name).FirstOrDefault()
                        },
                        Count = (int?)GetParamValueById(psForStaff, 36),
                        Cost = (float?)GetParamValueById(psForStaff, 38),
                        Price = (float?)GetParamValueById(psForStaff, 65),
                        id_tc_oper = (int?)GetParamValueById(psForStaff, 66),
                    };
                    tc.SupportStuff.Add(stuff);
                }
                else { psForStaff.RemoveAt(i); }
                i--;
            }
            //определяем индексы
            for (int i = 0; i < tc.SupportStuff.Count; i++)
            {  
              var id_tc_oper=tc.Works.Where(t=>tc.SupportStuff[i].id_tc_oper==t.Id).Select(v=>v.index).FirstOrDefault();
                if (!String.IsNullOrEmpty(id_tc_oper.ToString())) {
                    tc.SupportStuff[i].index = id_tc_oper;
                }
            ;}
            tc.SupportStuff = tc.SupportStuff.OrderBy(b => b.index).ToList();


                //materials
                tc.DataSZR = new List<SZRModel>();
            for (var i = 0; i < psForSzr.Count; i++)
            {
                var p = psForSzr[i];
                var szr = new SZRModel
                {
                    NameSelectedWorkForMaterial = (p.id_tc_operation.HasValue && p.TC_operation!=null) ? new DictionaryItemsDTO
                    {
                        Id   = p.TC_operation.tptas_tptas_id ,
                        Name = p.TC_operation.Type_tasks.name
                    } : null,
                    NameSZR = new DictionaryItemsDTO
                    {
                        Id = p.Materials.mat_id,
                        Name = p.Materials.name
                    },
                    Cost = (float?)GeMaterialValueByIdAndOperation(psForSzr, 42, p.mat_mat_id),
                    Price = (float?)GeMaterialValueByIdAndOperation(psForSzr, 44, p.mat_mat_id),
                    ConsumptionRate = (float?)GeMaterialValueByIdAndOperation(psForSzr, 48, p.mat_mat_id),
                    MaterialArea = (float?)GeMaterialValueByIdAndOperation(psForSzr, 49, p.mat_mat_id),
                    Type = p.Materials.mat_type_id.Value,
                    id_tc_oper = (int?)GeMaterialValueByIdAndOperation(psForSzr, 67, p.mat_mat_id),
                };
                tc.DataSZR.Add(szr);
                i--;
            }
            decimal? summaFields = 0;
            var plantfield = GetPlantSortFieldByYear(tc.TC.YearId);

            //общая площадь
            var allFields = new List<FieldDictionary>();
            for (int i = 0; i < plantfield.Count; i++)
            {
                var p = plantfield[i];
                if (p.Id == tc.TC.PlantId)
                {
                    var FieldList = p.Fields;
                    if (tc.TC.SortId != null)
                    {
                        for (int j = 0; j < p.Values.Count; j++)
                        {
                            var s = p.Values[j];
                            if (s.Id == tc.TC.SortId)
                            {
                                allFields = s.Fields;
                            }
                        }
                    }
                    if (allFields.Count == 0)
                    {
                        allFields = p.Fields;
                    }
                    for (int l = 0; l < allFields.Count; l++)
                    {

                        if (tc.TC.SortId == null && allFields[l].Id == tc.TC.FieldId) { summaFields += allFields[l].Area; }
                        if (tc.TC.SortId != null) { summaFields += allFields[l].Area; }
                        if (tc.TC.SortId == null && tc.TC.FieldId == null) { summaFields += allFields[l].Area; }
                    }

                }
            }
            //проверяем наличие изменений
            tc.TC.CheckСhange = 0;
            for (int j = 0; j < tc.Works.Count; j++)
            {
                if (tc.Works[j].UnicFlag == 0 && tc.Works[j].Workload != (float?)(tc.Works[j].Count * tc.Works[j].Factor * summaFields)) { tc.TC.CheckСhange = 1; break; }
            }
            ////пересчет объема работ
            for (var i = 0; i < tc.Works.Count; i++)
            {
                if (tc.Works[i].UnicFlag == 0) { tc.Works[i].Workload = (float?)(summaFields * tc.Works[i].Count * tc.Works[i].Factor); }
            }

            //////определяем индексы
            for (int i = 0; i < tc.DataSZR.Count; i++)
            {
                var id_tc_oper = tc.Works.Where(t => tc.DataSZR[i].id_tc_oper == t.Id).Select(v => v.index).FirstOrDefault();

                if (!String.IsNullOrEmpty(id_tc_oper.ToString()))
                {
                    tc.DataSZR[i].index = id_tc_oper;
                }
            }
            tc.DataSZR = tc.DataSZR.OrderBy(b => b.index).ToList();
            tc.DataChemicalFertilizers = new List<FertilizerModel>();
            for (var i = 0; i < psForFertilizer.Count; i++)
            {
                var p = psForFertilizer[i];
                var szr = new FertilizerModel
                {
                    NameSelectedWorkForMaterial = (p.id_tc_operation.HasValue && p.TC_operation != null) ? new DictionaryItemsDTO
                    {
                        Id = p.TC_operation.tptas_tptas_id,
                        Name = p.TC_operation.Type_tasks.name
                    } : null,
                    NameFertilizer = new DictionaryItemsDTO
                    {
                        Id = p.Materials.mat_id,
                        Name = p.Materials.name
                    },
                    Cost = (float?)GeMaterialValueByIdAndOperation(psForFertilizer, 42, p.mat_mat_id),
                    Price = (float?)GeMaterialValueByIdAndOperation(psForFertilizer, 44, p.mat_mat_id),
                    ConsumptionRate = (float?)GeMaterialValueByIdAndOperation(psForFertilizer, 48, p.mat_mat_id),
                    MaterialArea = (float?)GeMaterialValueByIdAndOperation(psForFertilizer, 49, p.mat_mat_id),
                    Type = p.Materials.mat_type_id.Value,
                    id_tc_oper = (int?)GeMaterialValueByIdAndOperation(psForFertilizer, 67, p.mat_mat_id),

                };
                tc.DataChemicalFertilizers.Add(szr);
                i--;
            }
            //определяем индексы
            for (int i = 0; i < tc.DataChemicalFertilizers.Count; i++)
            {
                var id_tc_oper = tc.Works.Where(t => tc.DataChemicalFertilizers[i].id_tc_oper == t.Id).Select(v => v.index).FirstOrDefault();
                if (!String.IsNullOrEmpty(id_tc_oper.ToString()))
                {
                    tc.DataChemicalFertilizers[i].index = id_tc_oper;
                }
            }
            tc.DataChemicalFertilizers = tc.DataChemicalFertilizers.OrderBy(b => b.index).ToList();
            tc.DataSeed = new List<SeedModel>();
            for (var i = 0; i < psForSeed.Count; i++)
            {
                var p = psForSeed[i];
                var szr = new SeedModel
                {
                    NameSelectedWorkForMaterial = (p.id_tc_operation.HasValue && p.TC_operation != null) ? new DictionaryItemsDTO
                    {
                        Id = p.TC_operation.tptas_tptas_id,
                        Name = p.TC_operation.Type_tasks.name
                    } : null,
                    NameSeed = new DictionaryItemsDTO
                    {
                        Id = p.Materials.mat_id,
                        Name = p.Materials.name
                    },
                    Cost = (float?)GeMaterialValueByIdAndOperation(psForSeed, 42, p.mat_mat_id),
                    Price = (float?)GeMaterialValueByIdAndOperation(psForSeed, 44, p.mat_mat_id),
                    ConsumptionRate = (float?)GeMaterialValueByIdAndOperation(psForSeed, 48, p.mat_mat_id),
                    MaterialArea = (float?)GeMaterialValueByIdAndOperation(psForSeed, 49, p.mat_mat_id),
                    Type = p.Materials.mat_type_id.Value,
                    id_tc_oper = (int?)GeMaterialValueByIdAndOperation(psForSeed, 67, p.mat_mat_id),

                };
                tc.DataSeed.Add(szr);
                i--;
            }
            ////определяем индексы
            for (int i = 0; i < tc.DataSeed.Count; i++)
            {
                var id_tc_oper = tc.Works.Where(t => tc.DataSeed[i].id_tc_oper == t.Id).Select(v => v.index).FirstOrDefault();
                if (!String.IsNullOrEmpty(id_tc_oper.ToString()))
                {
                    tc.DataSeed[i].index = id_tc_oper;
                }
            }
            tc.DataSeed = tc.DataSeed.OrderBy(b => b.index).ToList();
            
            tc.DataDopMaterial = new List<DopMaterialsModel>();
            for (var i = 0; i < psForDopMaterial.Count; i++)
            {
                var p = psForDopMaterial[i];
                var DopMaterials = new DopMaterialsModel
                {
                    NameSelectedWorkForMaterial = (p.id_tc_operation.HasValue && p.TC_operation != null) ? new DictionaryItemsDTO
                    {
                        Id = p.TC_operation.tptas_tptas_id,
                        Name = p.TC_operation.Type_tasks.name
                    } : null,
                    NameDopMaterial = new DictionaryItemsDTO
                    {
                        Id = p.Materials.mat_id,
                        Name = p.Materials.name
                    },
                    Cost = (float?)GeMaterialValueByIdAndOperation(psForDopMaterial, 42, p.mat_mat_id),
                    Price = (float?)GeMaterialValueByIdAndOperation(psForDopMaterial, 44, p.mat_mat_id),
                    ConsumptionRate = (float?)GeMaterialValueByIdAndOperation(psForDopMaterial, 48, p.mat_mat_id),
                    MaterialArea = (float?)GeMaterialValueByIdAndOperation(psForDopMaterial, 49, p.mat_mat_id),
                    Type = p.Materials.mat_type_id.Value,
                    id_tc_oper = (int?)GeMaterialValueByIdAndOperation(psForDopMaterial, 67, p.mat_mat_id),

                };
                tc.DataDopMaterial.Add(DopMaterials);
                i--;
            }
            ////определяем индексы
            for (int i = 0; i < tc.DataDopMaterial.Count; i++)
            {
                var id_tc_oper = tc.Works.Where(t => tc.DataDopMaterial[i].id_tc_oper == t.Id).Select(v => v.index).FirstOrDefault();
                if (!String.IsNullOrEmpty(id_tc_oper.ToString()))
                {
                    tc.DataDopMaterial[i].index = id_tc_oper;
                }
            }
            tc.DataDopMaterial = tc.DataDopMaterial.OrderBy(b => b.index).ToList();
            //
            return tc;
        }

        private double? GetParamValueById(List<TC_param_value> ps, int id)
        {
            for (var i = 0; i < ps.Count; i++)
            {
                if (ps[i].id_tc_param == id)
                {
                    var v = ps[i].value;
                    ps.RemoveAt(i);
                    return v;
                }
            }
            return null;
        }

        private double? GeMaterialValueByIdAndOperation(List<TC_param_value> ps, int id, int? mat_id)
        {
            for (var i = 0; i < ps.Count; i++)
            {
                if (ps[i].id_tc_param == id && ps[i].mat_mat_id == mat_id)
                {
                    var v = ps[i].value;
                    ps.RemoveAt(i);
                    return v;
                }
            }
            return null;
        }

        private double? GetParamValueByIdAndOperation(List<TC_param_value> ps, int id, int operId)
        {
            for (var i = 0; i < ps.Count; i++)
            {
                if (ps[i].id_tc_param == id && ps[i].id_tc_operation == operId)
                {
                    var v = ps[i].value;
                    //ps.RemoveAt(i);
                    return ps[i].value;
                }
            }
            return 0;
        }

        public List<TCItem> GetTCByYear(int year)
        {
            var res = new List<TCItem>();
            res = (from t in db.TC
                   where t.year == year  
                   let tcparValue = t.TC_param_value.Where(x => x.id_tc_param == 4).Select(x => x.value).FirstOrDefault()
                   let count = t.TC_param_value.Where(x => x.id_tc_param == 33 && x.id_tc==t.id && x.value==0).Select(x => x.value).Count()

                   select new TCItem
                   {
                       Id = t.id,
                       Plant = t.Plants.name,
                       Sort = t.Type_sorts_plants.name,
                       Field = t.Fields.name,
                       Cost = tcparValue ?? 0,
                       Status = count,
                       Changing_date = t.changing_date,
                       TemplateStatus = t.id_tc,
                       
                   }).ToList();
            for (int i = 0; i < res.Count; i++)
            {
                var d = Math.Round((double)(res[i].Cost ?? 0), 2);
                var d1 = Math.Round((decimal)(res[i].Cost ?? 0), 2);

                res[i].Changing_date1 = res[i].Changing_date.ToString("dd.MM.yyyy");
                res[i].Cost = Math.Round((res[i].Cost ?? 0), 2);
            }
          
                return res;
        }

        public string DelTC(int id)
        {
            using (var trans = db.Database.BeginTransaction())
            {
                try
                {
                    var tc = db.TC.Where(o => o.id == id).FirstOrDefault();
                    var oldOp = db.TC_operation.Where(o => o.id_tc == id).ToList();
                    var oldP = db.TC_param_value.Where(o => o.id_tc == id).ToList();
                    var oldTech = db.TC_operation_to_Tech.Where(o => o.TC_operation.id_tc == id).ToList();
                    var oldService = db.TC_to_service.Where(o => o.id_tc == id).ToList();

                    db.TC_operation_to_Tech.RemoveRange(oldTech);
                    db.TC_param_value.RemoveRange(oldP);
                    db.TC_operation.RemoveRange(oldOp);
                    db.TC_to_service.RemoveRange(oldService);
                    db.TC.Remove(tc);
                    db.SaveChanges();
                    trans.Commit();
                }
                catch (Exception)
                {
                    trans.Rollback();
                    throw;
                }
            }
            return "success";
        }

        public List<NZPRes> GetNZP(NZPReq req)
        {
            string str = null; string str1 = null; string str2 = null;
            //
            for (int i = 0; i < req.Fields.Count; i++)
            {
                if (req.Fields[i].Area == 0) { req.Fields.RemoveAt(i); i--; }
            }

            //зп и топливо
            for (int i = 0; i < req.Fields.Count; i++)
            {
                if (req.Fields.Count - 1 != i) { str = str + "Fields_to_plants.fiel_fiel_id=" + req.Fields[i].Id + " or "; }
                if (req.Fields.Count - 1 == i) { str = str + "Fields_to_plants.fiel_fiel_id=" + req.Fields[i].Id; }
                if (req.Fields.Count - 1 != i) { str1 = str1 + "Fields_to_plants.fiel_fiel_id=" + req.Fields[i].Id + " or "; }
                if (req.Fields.Count - 1 == i) { str1 = str1 + "Fields_to_plants.fiel_fiel_id=" + req.Fields[i].Id; }

                if (req.Fields.Count - 1 != i) { str2 = str2 + "fiel_id=" + req.Fields[i].Id + " or "; }
                if (req.Fields.Count - 1 == i) { str2 = str2 + "fiel_id=" + req.Fields[i].Id; }
            }
            //зп и топливо
            var list = db.Database.SqlQuery<TaskNZPCountModel>(
            string.Format(@"select total_salary as salary, fuel_cost as fuel_cost, Fields_to_plants.fiel_fiel_id as fiel_id  from Tasks, Fields_to_plants where Fields_to_plants.fielplan_id=Tasks.plant_plant_id and Fields_to_plants.Plant_plant_id=36
             and (" + str + ")  and YEAR(date_ingathering)=" + req.YearId)).ToList();
            //сзр
            var list3 = db.Database.SqlQuery<TaskNZPCountModel>(
  string.Format(@"select count*price as szr, fiel_fiel_id as fiel_id from TMC_Records, Materials, Fields_to_plants where   Materials.mat_id=TMC_Records.mat_mat_id and emp_to_id=0 and emp_from_id!=0 
       and Fields_to_plants.fielplan_id=TMC_Records.fielplan_fielplan_id and Fields_to_plants.plant_plant_id=36 and mat_type_id=1 and YEAR(date_ingathering)=" + req.YearId +
  "and (" + str + ")")).ToList();
            //удобрения
            var list4 = db.Database.SqlQuery<TaskNZPCountModel>(
      string.Format(@"select count*price as chemicalFertilizers, fiel_fiel_id as fiel_id from TMC_Records, Materials, Fields_to_plants where   Materials.mat_id=TMC_Records.mat_mat_id and emp_to_id=0 and emp_from_id!=0 
        and Fields_to_plants.fielplan_id=TMC_Records.fielplan_fielplan_id and Fields_to_plants.plant_plant_id=36 and mat_type_id=2 and YEAR(date_ingathering)=" + req.YearId +
   "and (" + str + ")")).ToList();
            //семена
            var list5 = db.Database.SqlQuery<TaskNZPCountModel>(
      string.Format(@"select count*price as seed, fiel_fiel_id as fiel_id from TMC_Records, Materials, Fields_to_plants where   Materials.mat_id=TMC_Records.mat_mat_id and emp_to_id=0 and emp_from_id!=0 
       and Fields_to_plants.fielplan_id=TMC_Records.fielplan_fielplan_id and Fields_to_plants.plant_plant_id=36 and mat_type_id=4 and YEAR(date_ingathering)=" + req.YearId +
   "and (" + str + ")")).ToList();
            //dop material
            var list8 = db.Database.SqlQuery<TaskNZPCountModel>(
      string.Format(@"select count*price as dopMaterial, fiel_fiel_id as fiel_id from TMC_Records, Materials, Fields_to_plants where   Materials.mat_id=TMC_Records.mat_mat_id and emp_to_id=0 and emp_from_id!=0 
       and Fields_to_plants.fielplan_id=TMC_Records.fielplan_fielplan_id and Fields_to_plants.plant_plant_id=36 and mat_type_id=5 and YEAR(date_ingathering)=" + req.YearId +
   "and (" + str + ")")).ToList();
            //сдельная ЗП
            var list6 = db.Database.SqlQuery<TaskNZPCountModel>(
           string.Format(@"select sum as summa , Fields_to_plants.fiel_fiel_id as fiel_id from Salary_details,Fields_to_plants  where Fields_to_plants.plant_plant_id=36 and Fields_to_plants.fielplan_id=Salary_details.plant_plant_id and (" + str1 + ") and YEAR(date_ingathering)=" + req.YearId)).ToList();
           //все связки культуры-поля
            var list7 = db.Database.SqlQuery<TaskNZPCountModel>(
          string.Format(@"  SELECT sum(area_plant) as area, fiel_fiel_id as fiel_id  from Fields_to_plants  where (" + str1 + ") and YEAR(date_ingathering)=" + req.YearId + "  group by fiel_fiel_id")).ToList();
            
            
            //szr
            if (list3.Count != 0) { 
             for (int i = 0; i < list3.Count; i++)
            {
                for (int j = 0; j <req.Fields.Count; j++)
                {
                    if (list3[i].fiel_id == req.Fields[j].Id)
                    {
                        decimal? allArea = list7.Where(x => x.fiel_id == req.Fields[j].Id).Select(x => x.area).FirstOrDefault();
                        szr1 = szr1 + (list3[i].szr) * (double)(req.Fields[j].Area / allArea); break;
                    }
                }
            }
            }
            //seed
            if (list5.Count != 0)
            {
                for (int i = 0; i < list5.Count; i++)
                {
                    for (int j = 0; j < req.Fields.Count; j++)
                    {
                        if (list5[i].fiel_id == req.Fields[j].Id)
                        {
                           decimal? allArea = list7.Where(x => x.fiel_id == req.Fields[j].Id).Select(x => x.area).FirstOrDefault();
                            seed1 = seed1 + list5[i].seed * (double)(req.Fields[j].Area / allArea); break;
                        }
                    }
                }
            }
            //dop material
            if (list8.Count != 0)
            {
                for (int i = 0; i < list8.Count; i++)
                {
                    for (int j = 0; j < req.Fields.Count; j++)
                    {
                        if (list8[i].fiel_id == req.Fields[j].Id)
                        {
                            decimal? allArea = list7.Where(x => x.fiel_id == req.Fields[j].Id).Select(x => x.area).FirstOrDefault();
                            dopMaterial = (dopMaterial ?? 0) + list8[i].dopMaterial * (double)(req.Fields[j].Area / allArea); break;
                        }
                    }
                }
            }
            //удобрения
            if (list4.Count != 0)
            {
                for (int i = 0; i < list4.Count; i++)
                {
                    for (int j = 0; j < req.Fields.Count; j++)
                    {
                        if (list4[i].fiel_id == req.Fields[j].Id)
                        {
                            decimal? allArea = list7.Where(x => x.fiel_id == req.Fields[j].Id).Select(x => x.area).FirstOrDefault();
                            chemicalFertilizers1 = (chemicalFertilizers1 ?? 0) + list4[i].chemicalFertilizers * (double)(req.Fields[j].Area / allArea); break;
                        }
                    }
                }
            }
         //зарплата и топливо
            if (list.Count != 0)
            {
                for (int i = 0; i < list.Count; i++)
                {
                    for (int j = 0; j < req.Fields.Count; j++)
                    {
                        if (list[i].fiel_id == req.Fields[j].Id)
                        {
                            decimal? allArea = list7.Where(x => x.fiel_id == req.Fields[j].Id).Select(x => x.area).FirstOrDefault();
                            salary = salary + (list[i].salary ?? 0) * (decimal)(req.Fields[j].Area / allArea) + (decimal?)((double?)(list[i].salary ?? 0) *0.321) * (decimal)(req.Fields[j].Area / allArea);
                            fuel_cost = fuel_cost + (list[i].fuel_cost ?? 0) * (decimal)(req.Fields[j].Area / allArea); break;
                        }
                    }
                }
            }
           // сдельная зп
            if (list6.Count != 0)
            {
                for (int i = 0; i < list6.Count; i++)
                {
                    for (int j = 0; j < req.Fields.Count; j++)
                    {
                        if (list6[i].fiel_id == req.Fields[j].Id)
                        {
                            decimal? allArea = list7.Where(x => x.fiel_id == req.Fields[j].Id).Select(x => x.area).FirstOrDefault();
                            sum = sum + (decimal)(list6[i].summa ?? 0) * (decimal)(req.Fields[j].Area / allArea) + (decimal)((int?)((double?)list6[i].summa ?? 0) *0.321 ?? 0) * (decimal)(req.Fields[j].Area / allArea);
                            break;
                        }
                    }
                }
            }
            //
            if (String.IsNullOrEmpty(szr1.ToString())) { szr1 = 0; }
            if (String.IsNullOrEmpty(chemicalFertilizers1.ToString())) { chemicalFertilizers1 = 0; }
            if (String.IsNullOrEmpty(seed1.ToString())) { seed1 = 0; }
            if (String.IsNullOrEmpty(salary.ToString())) { salary = 0; }
            if (String.IsNullOrEmpty(fuel_cost.ToString())) { fuel_cost = 0; }
            if (String.IsNullOrEmpty(sum.ToString())) { sum = 0; }
            if (String.IsNullOrEmpty(dopMaterial.ToString())) { dopMaterial = 0; } 
            var res = new NZPRes
            {
                Salary = Math.Round((decimal)(salary + sum), 2),
                Energy = Math.Round((decimal)(fuel_cost), 2),
                SZR = Math.Round((decimal)szr1, 2),
                ChemicalFertilizers = Math.Round((decimal)chemicalFertilizers1, 2),
                Seed = Math.Round((decimal)seed1, 2),
                DopMaterial = Math.Round((decimal)dopMaterial, 2)
            };
            return new List<NZPRes> { res };
        }
        //расценки для тех. карт
        public RateResModel GetRate(RateTechReq req)
        {

            if (!String.IsNullOrEmpty(req.techId.ToString()))
            {
                var trakt_id = db.Database.SqlQuery<RateTechReq>(
           string.Format(@"select id_grtrakt as techId from Traktors where trakt_id=" + req.techId)).ToList();
                tech = trakt_id[0].techId;

                 type_fuel = db.Database.SqlQuery<DictionaryItemsDTO>(
    string.Format(@"select t.id as Id, t.name as Name, t.current_price as Price from Traktors tr, Type_fuel t where tr.tpf_tpf_id=t.id and tr.trakt_id=" + req.techId)).ToList();
               
            }
            else { tech = null;  }
            
            var rate = GetTechRate(req.tptasId, null, tech, req.equiId, req.plantId);
            if (!String.IsNullOrEmpty(req.techId.ToString()))
            {
            rate.Typefuel.Id = type_fuel[0].Id;
            rate.Typefuel.Name = type_fuel[0].Name;
            rate.Typefuel.Price = type_fuel[0].Price;
            rate.Typefuel.Area_plant = 0;
             }
         //id_tc_operation
            if (String.IsNullOrEmpty(req.operationId.ToString()))
            {
                rate.Id = (db.TC_operation.Max(t => (int?)t.id) ?? 0) + 1;
            
            }




           return rate;
        }
        //

        public RateResModel GetTechRate(int? tptasid, int? agroreqid, int? techid, int? equiid, int? plantid)
        {
            var rates = db.Task_rate.Where(r => r.id_tptas == tptasid && (!r.id_agroreq.HasValue || r.id_agroreq == agroreqid)
                                               && (!r.id_grtrakt.HasValue || r.id_grtrakt == techid)
                                               && (!r.id_equi.HasValue || r.id_equi == equiid)
                                               && (!r.id_plant.HasValue || r.id_plant == plantid)).Select(r => new RateResModel
                                               {
                                                   Id = r.id,
                                                   TptasId = r.id_tptas.Value,
                                                   PlantId = r.id_plant,
                                                   AgroreqId = r.id_agroreq,
                                                   EquiId = r.id_equi,
                                                   TraktId = r.id_grtrakt,
                                                   ShiftOutput = r.shift_output,
                                                   RateShift = r.rate_shift,
                                                   RatePiecework = r.rate_piecework,
                                                   FuelConsumption = r.fuel_consumption,
                                                   Typefuel = new DictionaryItemsDTO 
                                                   {
                                                   Id =0,
                                                   Name ="",
                                                   Price=0
                                                   },
                                                   CountCheck = 0 + (r.id_plant.HasValue ? 1 : 0)
                                                                  + (r.id_agroreq.HasValue ? 1 : 0)
                                                                  + (r.id_equi.HasValue ? 1 : 0)
                                                                  + (r.id_plant.HasValue ? 1 : 0)
                                               }).OrderByDescending(r => r.CountCheck).ToList();



            if (rates.Count == 0)
            {
             RateResModel rates1 = new RateResModel();
                rates1.Typefuel = new DictionaryItemsDTO();
                return rates1;
            }
            else
            {
                return rates[0];
            }
           
        }
        //
        /////////////////copy techcard
        public string CopyTCReportById(int id)
        {
            //получение данных тех. карты
            TCDetailModel list = new TCDetailModel();
            RateResModel rate = new RateResModel();
            RateTechReq req = new RateTechReq();
            var materials = GetListMaterial();
            List<PlantSortFieldDictionary> plantfield = new List<PlantSortFieldDictionary>();
            list = GetTCById(id);
            list.TC.Id = null;
            list.TC.Area = null;
            list.TC.SortId = null;
          //  list.TC.FieldId = null;
            list.TC.YearId = list.TC.YearId + 1;
            list.TC.Productivity = 0;
            //услуги
            if (list.DataServices != null)
            {
                for (int i = 0; i < list.DataServices.Count; i++)
                {
                    list.DataServices[i].Id = null;
                    list.DataServices[i].Cost = 0;
                }
            }

       
            decimal? summaFields = 0;
            plantfield = GetPlantSortFieldByYear(list.TC.YearId);

            //общая площадь
            for (int i = 0; i < plantfield.Count; i++)
            {
                if (plantfield[i].Id == list.TC.PlantId) {
                    if (plantfield[i].Fields.Count != 0)
                    {
                        for (int j = 0; j < plantfield[i].Fields.Count; j++)
                        {
                            summaFields = summaFields + plantfield[i].Fields[j].Area;
                        }
                        break;
                    }
                }
            }

                //назначенные работы, топливо, зарплата
                if (list.Works != null)
                {
                    var newId = (db.TC_operation.Max(t => (int?)t.id) ?? 0) ;
                    for (int i = 0; i < list.Works.Count; i++)
                    {
                        req.tptasId = list.Works[i].Name.Id;
                        if (list.Works[i].Tech.Count != 0) { req.techId = list.Works[i].Tech[0].Id; } else { req.techId = null; }
                        if (list.Works[i].Equipment.Count != 0) { req.equiId = list.Works[i].Equipment[0].Id; } else { req.equiId = null; }
                        req.plantId = list.TC.PlantId;

                        list.Works[i].Id = null;
                        DateTime newStartDate = new DateTime(list.Works[i].DateStart.Value.Year+1, list.Works[i].DateStart.Value.Month, list.Works[i].DateStart.Value.Day);
                        list.Works[i].DateStart = newStartDate;
                        DateTime newEndDate = new DateTime(list.Works[i].DateEnd.Value.Year + 1, list.Works[i].DateEnd.Value.Month, list.Works[i].DateEnd.Value.Day);
                        list.Works[i].DateEnd = newEndDate;
                        newId++;
                        list.Works[i].Id1 = newId;
                        rate = GetRate(req);

                        if (!String.IsNullOrEmpty(rate.Typefuel.ToString())) { list.Works[i].FuelType = rate.Typefuel; } //тип, цена топлива
                        if (list.Works[i].UnicFlag == 0)
                        { //если по площади
                            list.Works[i].Workload = (float)list.Works[i].Count * (float)list.Works[i].Factor * (float)summaFields;//объем работ
                        }
                        if (!String.IsNullOrEmpty(rate.FuelConsumption.ToString())) { list.Works[i].Consumption = (float)rate.FuelConsumption;//расход топлива
                        list.Works[i].FuelCount = list.Works[i].Consumption * list.Works[i].Workload; //потребность
                        list.Works[i].FuelCost = list.Works[i].FuelCount * list.Works[i].FuelType.Price; //стоимость топлива
                        } 
                        else {
                            list.Works[i].Consumption = 0;
                            list.Works[i].FuelCount = 0;
                            list.Works[i].FuelCost = 0;
                        }

                         if (!String.IsNullOrEmpty(rate.ShiftOutput.ToString())) { list.Works[i].ShiftRate = (float)rate.ShiftOutput; } //см. норма
                         if (list.Works[i].ShiftRate != 0) { list.Works[i].ShiftCount = (float)Math.Round((decimal)(list.Works[i].Workload / list.Works[i].ShiftRate),2); } //смены
                         if (!String.IsNullOrEmpty(rate.RateShift.ToString())) { list.Works[i].Rate = (float)rate.RateShift; } //тариф
                         if (!String.IsNullOrEmpty(rate.RatePiecework.ToString())) { list.Works[i].EmpPrice = (float)rate.RatePiecework; } //расценка
                         else { list.Works[i].EmpPrice = 0; }
                         list.Works[i].EmpCost =(float) Math.Round((decimal)( (list.Works[i].Rate ?? 0) * (list.Works[i].ShiftCount ?? 0)
                             + (list.Works[i].EmpPrice ?? 0) * (list.Works[i].Workload ?? 0 )) ,2); //стоимость зп

                      
                    }
                }
            //НЗП
            if (list.NZP!= null)
            {
                list.NZP.ChemicalFertilizers = 0;
                list.NZP.Energy = 0;
                list.NZP.Salary = 0;
                list.NZP.Seed = 0;
                list.NZP.SZR = 0;        
            }
         //вспомогательный персонал
            if (list.SupportStuff != null)
            {
                for (int i = 0; i < list.SupportStuff.Count; i++)
                {
               //     list.SupportStuff[i].Price = 0;
              //      list.SupportStuff[i].Cost = 0;
            list.SupportStuff[i].id_tc_oper = list.Works.Where(t => list.SupportStuff[i].index == t.index).Select(v => v.Id1).FirstOrDefault();     

                }
            }  

            //
            if (list.DataTotal != null) //итого
            {
              for (int i = 0; i < list.DataTotal.Count; i++)
             {
              list.DataTotal[i].Total = 0;
            }
            }
            //семена
            if (list.DataSeed != null)
            {
                for (int i = 0; i < list.DataSeed.Count; i++)
                {
                list.DataSeed[i].id_tc_oper = list.Works.Where(t => t.Name.Id == list.DataSeed[i].NameSelectedWorkForMaterial.Id).Select(t => t.Id1).FirstOrDefault();
                list.DataSeed[i].MaterialArea = (float)summaFields; //площадь внесения
           //   list.DataSeed[i].ConsumptionRate = 0; //расход
           //     list.DataSeed[i].Cost = 0; //стоимость

                    for (int j = 0; j < materials.Count; j++)
                    {
                        if (materials[j].Id == list.DataSeed[i].NameSeed.Id) { list.DataSeed[i].Price = (float)materials[j].Price; break; } //цена материала
                    }
                    list.DataSeed[i].Cost = (float)Math.Round((double)((list.DataSeed[i].MaterialArea ?? 0) * (list.DataSeed[i].Price ?? 0) * 
                      (list.DataSeed[i].ConsumptionRate ?? 0)),2);
                }
            }  
            //сзр
            if (list.DataSZR != null)
            {
                for (int i = 0; i < list.DataSZR.Count; i++)
                {
                    list.DataSZR[i].id_tc_oper = list.Works.Where(t => t.Name.Id == list.DataSZR[i].NameSelectedWorkForMaterial.Id).Select(t => t.Id1).FirstOrDefault();
                    list.DataSZR[i].MaterialArea = (float)summaFields; //площадь внесения
                 //   list.DataSZR[i].ConsumptionRate = 0; //расход
                //    list.DataSZR[i].Cost = 0; //стоимость

                    for (int j = 0; j < materials.Count; j++)
                    {
                        if (materials[j].Id == list.DataSZR[i].NameSZR.Id) { list.DataSZR[i].Price = (float)materials[j].Price; break; } //цена материала
                    }
                    list.DataSZR[i].Cost = (float)Math.Round((double)((list.DataSZR[i].MaterialArea ?? 0) * (list.DataSZR[i].Price ?? 0) *
                     (list.DataSZR[i].ConsumptionRate ?? 0)), 2);
                }
            } 

            //удобрения
            if (list.DataChemicalFertilizers != null)
            {
                for (int i = 0; i < list.DataChemicalFertilizers.Count; i++)
                {
                    list.DataChemicalFertilizers[i].id_tc_oper = list.Works.Where(t => t.Name.Id == list.DataChemicalFertilizers[i].NameSelectedWorkForMaterial.Id).Select(t => t.Id1).FirstOrDefault();
                    list.DataChemicalFertilizers[i].MaterialArea = (float)summaFields; //площадь внесения
               //     list.DataChemicalFertilizers[i].ConsumptionRate = 0; //расход
               //     list.DataChemicalFertilizers[i].Cost = 0; //стоимость

                    for (int j = 0; j < materials.Count; j++)
                    {
                        if (materials[j].Id == list.DataChemicalFertilizers[i].NameFertilizer.Id) { list.DataChemicalFertilizers[i].Price = (float)materials[j].Price; break; } //цена материла
                    }
                    list.DataChemicalFertilizers[i].Cost = (float)Math.Round((double)((list.DataChemicalFertilizers[i].MaterialArea ?? 0) *
                    (list.DataChemicalFertilizers[i].Price ?? 0) * (list.DataChemicalFertilizers[i].ConsumptionRate ?? 0)), 2);
                }
            } 
            //прочие материалы
            if (list.DataDopMaterial != null)
            {
                for (int i = 0; i < list.DataDopMaterial.Count; i++)
                {
                    list.DataDopMaterial[i].id_tc_oper = list.Works.Where(t => t.Name.Id == list.DataDopMaterial[i].NameSelectedWorkForMaterial.Id).Select(t => t.Id1).FirstOrDefault();
                    list.DataDopMaterial[i].MaterialArea = (float)summaFields; //площадь внесения
                  //  list.DataDopMaterial[i].ConsumptionRate = 0; //расход
                //    list.DataDopMaterial[i].Cost = 0; //стоимость

                    for (int j = 0; j < materials.Count; j++)
                    {
                        if (materials[j].Id == list.DataDopMaterial[i].NameDopMaterial.Id) { list.DataDopMaterial[i].Price = (float)materials[j].Price; break; } //цена материла
                    }
                    list.DataDopMaterial[i].Cost = (float)Math.Round((double)((list.DataDopMaterial[i].MaterialArea ?? 0) * (list.DataDopMaterial[i].Price ?? 0) *
                     (list.DataDopMaterial[i].ConsumptionRate ?? 0)), 2);

                }
            }
           CreateUpdateTC(list);
            return "success";
        }
        //


        //
        public GantTech GetTechReport(ReqGantt req)
        {
            var tech = (from t in db.TC_operation_to_Tech
                        where t.TC_operation.date_start.HasValue && t.TC_operation.date_finish.HasValue &&
                            (req.DateStart.HasValue && req.DateEnd.HasValue ? t.TC_operation.date_finish.Value > req.DateStart.Value && t.TC_operation.date_start < req.DateEnd.Value : true) &&
                            (t.trakt_trakt_id.HasValue && req.TypeTraktorId.HasValue ? t.Traktors.tptrak_tptrak_id == req.TypeTraktorId.Value : true) &&
                            (t.equi_equi_id.HasValue && req.TypeEquiId.HasValue ? t.Equipments.tpequi_tpequi_id == req.TypeEquiId : true)
                        orderby t.TC_operation.date_start
                        group t by new
                        {
                            EquiId = t.equi_equi_id,
                            EquiName = t.Equipments.name,
                            TraktId = t.trakt_trakt_id,
                            TraktName = t.Traktors.name,
                            TraktReg = t.Traktors.reg_num
                        } into g
                        select new
                        {
                            TraktId = g.Key.TraktId,
                            TraktName = g.Key.TraktName + " (" + g.Key.TraktReg + ")",
                            EquiId = g.Key.EquiId,
                            EquiName = g.Key.EquiName,
                            Values = (from o in g

                                      let dstr1 = SqlFunctions.DatePart("mm", o.TC_operation.date_start.Value) + "/" + SqlFunctions.DatePart("dd", o.TC_operation.date_start.Value) + "/" + SqlFunctions.DatePart("yyyy", o.TC_operation.date_start.Value)
                                      let dstr2 = SqlFunctions.DatePart("mm", o.TC_operation.date_finish.Value) + "/" + SqlFunctions.DatePart("dd", o.TC_operation.date_finish.Value) + "/" + SqlFunctions.DatePart("yyyy", o.TC_operation.date_finish.Value)

                                      select new GantTaskModel
                                      {
                                          Id = o.id,
                                          Name = o.TC_operation.TC.Plants.name + ": " + o.TC_operation.Type_tasks.name,
                                          BaselineStartDate = dstr1,
                                          StartDate = dstr1,
                                          Duration = DbFunctions.DiffDays(o.TC_operation.date_start.Value, o.TC_operation.date_finish.Value) + 1 ?? 1,
                                          BaselineEndDate = dstr2,
                                          Date1 = o.TC_operation.date_start.Value,
                                          Date2 = o.TC_operation.date_finish.Value,
                                      }).ToList()
                        }).ToList();
            var TraktList = (from t in tech
                             where t.TraktId != null

                             let date1 = t.Values.Min(p => p.Date1).Value
                             let date2 = t.Values.Min(p => p.Date2).Value
                             let dstr1 = date1.ToString("MM/dd/yyyy")
                             let dstr2 = date2.ToString("MM/dd/yyyy")


                             select new GantTaskModel
                             {
                                 Id = t.TraktId.Value,
                                 Name = t.TraktName,
                                 StartDate = dstr1,
                                 BaselineStartDate = dstr1,
                                 BaselineEndDate = dstr2,
                                 Duration = (int)(date2 - date1).TotalDays + 1,
                                 Children = t.Values
                             }).ToList();
            var idList = TraktList.Select(x => x.Id).ToList();
            var FreeTraktList = (from t in db.Traktors
                                 where req.TypeTraktorId.HasValue ? t.tptrak_tptrak_id == req.TypeTraktorId.Value : true &&
                                        !idList.Contains(t.trakt_id) &&  t.deleted == false
                                 select new DictionaryItemsDTO
                                 {
                                     Id = t.trakt_id,
                                     Name = t.name + " (" + t.reg_num + ")"
                                 }).ToList();
            var EquiList = (from t in tech
                            where t.EquiId != null

                            let date1 = t.Values.Min(p => p.Date1).Value
                            let date2 = t.Values.Min(p => p.Date2).Value
                            let dstr1 = date1.ToString("MM/dd/yyyy")
                            let dstr2 = date2.ToString("MM/dd/yyyy")

                            select new GantTaskModel
                            {
                                Id = t.EquiId.Value,
                                Name = t.EquiName,
                                StartDate = dstr1,
                                BaselineStartDate = dstr1,
                                BaselineEndDate = dstr2,
                                Duration = (int)(date2 - date1).TotalDays + 1,
                                Children = t.Values
                            }).ToList();
            var idList2 = EquiList.Select(x => x.Id).ToList();
            var FreeEquiList = (from t in db.Equipments
                                where t.deleted == false
                                        && req.TypeEquiId.HasValue ? t.tpequi_tpequi_id == req.TypeEquiId.Value : true
                                        && !idList2.Contains(t.equi_id)
                                select new DictionaryItemsDTO
                                {
                                    Id = t.equi_id,
                                    Name = t.name
                                }).ToList();
            var list = new GantTech
            {
                TraktList = TraktList,
                FreeTrakt = FreeTraktList,
                EquiList = EquiList,
                FreeEqui = FreeEquiList
            };
            return list;

        }

        //========распределение техники - новое
        public GantTMC GetTechEqiepReport(ReqGantt req)
        {
            var tmc = (from t in db.TC_operation_to_Tech
                       where t.TC_operation.date_start.HasValue && t.TC_operation.date_finish.HasValue &&
                           (req.DateStart.HasValue && req.DateEnd.HasValue ? t.TC_operation.date_finish.Value > req.DateStart.Value &&
                           t.TC_operation.date_start < req.DateEnd.Value : true  ) &&
                           (t.trakt_trakt_id.HasValue && req.TypeTraktorId.HasValue ? t.Traktors.tptrak_tptrak_id == req.TypeTraktorId.Value : true) &&
                           (t.equi_equi_id.HasValue && req.TypeEquiId.HasValue ? t.Equipments.tpequi_tpequi_id == req.TypeEquiId : true)

                       orderby t.TC_operation.date_start
                       group t by new
                       {
                           EquiId = t.equi_equi_id,
                           EquiName = t.Equipments.name,
                           TraktId = t.trakt_trakt_id,
                           TraktName = t.Traktors.name,
                           TraktReg = t.Traktors.reg_num
                       } into g
                       select new DHXGantModel
                       {
                           id = g.Key.TraktId != null ? (int)g.Key.TraktId : (g.Key.EquiId ?? 0),
                           text = g.Key.TraktId != null ? ( g.Key.TraktName + "(" + g.Key.TraktReg + ")") : g.Key.EquiName,
                           parent = null,
                           open = false,
                           type = "project",
                           EquipOrTech = (g.Key.TraktId != null && g.Key.EquiId != null) ? 3 : (g.Key.TraktId != null ? 1 : 2),
                           Children = (from o in g
                                       select new DHXGantModel1
                                       {
                                           id = o.id,
                                           text = o.TC_operation.TC.Plants.name + ": " + o.TC_operation.Type_tasks.name,
                                           Date1 = o.TC_operation.date_start.Value,
                                           Date2 = o.TC_operation.date_finish.Value,
                                       }).ToList(),
                       }).ToList();
            tmc = tmc.Where(t => t.id != 0).ToList();
            int k = 0;
            if (tmc.Count != 0)
            {
                k = tmc.Max(t => t.id) + 1;
            }
            for (int i = 0; i < tmc.Count; i++)
            {
                if (tmc[i].parent == null)
                {
                    for (int j = 0; j < tmc[i].Children.Count; j++)
                    {
                        tmc.Add(new DHXGantModel
                        {
                            id = k,
                            EquipOrTech = tmc[i].EquipOrTech,
                            text = tmc[i].Children[j].text,
                            start_date = tmc[i].Children[j].Date1.ToString("dd-MM-yyyy"),
                            parent = tmc[i].id,
                            duration = tmc[i].Children[j].Date2.DayOfYear - tmc[i].Children[j].Date1.DayOfYear + 1,
                        });
                        k++;
                    }

                    tmc[i].start_date = tmc[i].Children.Min(t => t.start_date);
                    tmc[i].end_date = tmc[i].Children.Max(t => t.end_date);
                }
            }

            var techList = tmc.Where(t => t.EquipOrTech == 1 || t.EquipOrTech == 3).ToList();
            var equipList = tmc.Where(t => t.EquipOrTech == 2 || t.EquipOrTech == 3).ToList();
            var list = new GantTMC { data = techList, data2 = equipList };
           return list;
        }

        //==





        /////////////////распределение ТМЦ
        public GantTMC GetTMCReport(ReqTMC req)
        {
            //новое
            var tmc = (from t in db.TC_param_value
                       where t.TC_operation.date_start.HasValue && t.TC_operation.date_finish.HasValue &&
                           ((req.DateStart.HasValue && req.DateEnd.HasValue) ? t.TC_operation.date_finish.Value >= req.DateStart.Value 
                           && t.TC_operation.date_start <= req.DateEnd.Value : true)
                           && (req.TmcId != -1 ? t.Materials.mat_type_id == req.TmcId : true)
                           && t.mat_mat_id != null
                       orderby t.TC_operation.date_start
                       group t by new
                       {
                           MaterialId = t.Materials.mat_id,
                           MaterialName = t.Materials.name,
                       } into g
                       select new DHXGantModel
                        {
                            id = g.Key.MaterialId,
                            text = g.Key.MaterialName,
                            consumption = 0,
                            cost = 0,
                            parent = null,
                            open = false,
                            type = "project",
                            Children = (from o in g
                                        where (o.id_tc_param == 42 || o.id_tc_param == 48 || o.id_tc_param == 49)
                                        select new DHXGantModel1
                                      {
                                          id = o.id,
                                          text = o.TC_operation.TC.Plants.name + ": " + o.TC_operation.Type_tasks.name,
                                          Date1 = o.TC_operation.date_start.Value,
                                          Date2 = o.TC_operation.date_finish.Value,
                                          id_tc_param = o.id_tc_param,
                                          value = (float?)o.value,
                                      }).ToList(),
                        }).ToList();

            int k = 0;
            if (tmc.Count != 0)
            {
                k = tmc.Max(t => t.id) + 1;
            }
            for (int i = 0; i < tmc.Count; i++)
            {
                if (tmc[i].parent == null)
                {
                    for (int j = 0; j < tmc[i].Children.Count; j++)
                    {
                        tmc.Add(new DHXGantModel
                        {
                            id = k,
                            text = tmc[i].Children[j].text,
                            cost = (float)Math.Round((decimal)tmc[i].Children[j].value, 2),
                            consumption = (float) Math.Round( (decimal)(tmc[i].Children[j + 1].value * tmc[i].Children[j + 2].value),3),
                            start_date = tmc[i].Children[j].Date1.ToString("dd-MM-yyyy"),
                            parent = tmc[i].id,
                            duration = tmc[i].Children[j].Date2.DayOfYear - tmc[i].Children[j].Date1.DayOfYear + 1,
                        });
                        j = j + 2; k++;
                    }
                    tmc[i].consumption = (float)Math.Round((decimal)tmc.Where(c => c.parent == tmc[i].id).Sum(c => c.consumption),3);
                    tmc[i].cost = (float)Math.Round((decimal)tmc.Where(c => c.parent == tmc[i].id).Sum(c => c.cost),2);
                    tmc[i].start_date = tmc[i].Children.Min(t => t.start_date);
                    tmc[i].end_date = tmc[i].Children.Max(t => t.end_date);
                }
            }

            var list = new GantTMC { data = tmc };
            return list;
        }


        public GanttInfo GetTechReportInfo()
        {
            var res = new GanttInfo();

            res.TypeTraktor = (from t in db.Type_traktors
                               select new DictionaryItemsDTO
                               {
                                   Id = t.tptrak_id,
                                   Name = t.name
                               }).ToList();
            res.TypeEqui = (from t in db.Type_equipments
                            select new DictionaryItemsDTO
                            {
                                Id = t.tpequi_id,
                                Name = t.name
                            }).ToList();
            return res;
        }
        public MaterialDto GetValueByMatTypeId(int mat_type_id, DateTime? DateStart, DateTime? DateEnd,int? TmcId)
        {
            MaterialDto res = new MaterialDto();
            var mat_cost = (decimal?)db.TC_param_value.Where(n => n.id_tc_param == 42 && n.Materials.mat_type_id == mat_type_id
                && (DateStart.HasValue && DateEnd.HasValue ? n.TC_operation.date_finish.Value > DateStart.Value && n.TC_operation.date_start < DateEnd.Value : true)
                         && (TmcId != -1 ? n.Materials.mat_type_id == TmcId : true)
             ).Select(n => n.value).Sum() ?? 0;
            res.Sum = Math.Round((decimal)mat_cost, 2);
            return res;
        }

        public List<MaterialDto> GetTmcMaterial(ReqTMC req)
        {
            var res = new TmcInfo();
            res.TmcList = new List<MaterialDto>();
            List<string> names = new List<string>() { "СЗР", "Мин. удобрения", "Семена", "Прочие материалы" };
            List<int> id = new List<int>() { 1, 2, 4, 5 };
            if (req.TmcId != -1)
            {
                for (int i = 0; i < id.Count; i++)
                {
                    if (id[i] != req.TmcId) { id.RemoveAt(i); names.RemoveAt(i); i--; }
                }
            }
            for (int i = 0; i < id.Count; i++)
            {
                var l = GetValueByMatTypeId(id[i],req.DateStart,req.DateEnd,req.TmcId);
                res.TmcList.Add(new MaterialDto
                {
                    Name = names[i],
                    Sum = (decimal)l.Sum,
                });
            }
            if (req.TmcId == -1)
            {
                res.TmcList.Add(new MaterialDto
                {
                    Name = "Итого",
                    Sum = res.TmcList.Sum(l => Math.Round((decimal)l.Sum, 2)),
                });
            }
            return res.TmcList;
        }


       
    }
}
