﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using LogusSrv.DAL.DBModel;
using LogusSrv.DAL.Entities.DTO;
using LogusSrv.DAL.Entities.DTO.ExportExcel;
using System.IO;
using LogusSrv.DAL.Entities.Res;
using LogusSrv.DAL.Entities.Req;
namespace LogusSrv.DAL.Operations
{
    public class ResultShiftReport
    {
        public byte[] bytes { get; set; }
        public DateTime date { get; set; }
    }

    public class ExportToExcel
    {
        protected readonly LogusDBEntities db = new LogusDBEntities();
        public DayTasksDb dayTasksDb = new DayTasksDb();

   //   public string pathTemplate = "/ExcelReports/Template";
        public string SheetName = "Сменное задание";
        public string SecondNameList = "План-Факт";
        public string ThirdNameList = "Факт";
        public string FourNameList = "Незадействованный персонал";


        public ResultShiftReport loadExcel(int id, string pathTemplate)
        {
            byte[] byteArray = File.ReadAllBytes(pathTemplate);
            var list = dayTasksDb.GetPrintShiftTasks(id);
            using (MemoryStream stream = new MemoryStream())
            {
                stream.Write(byteArray, 0, (int)byteArray.Length);

                using (var document = SpreadsheetDocument.Open(stream, true))
                {
                    // 1 лист отчета 
                    Sheet sheet;
                    try
                    {
                        sheet = document.WorkbookPart.Workbook.GetFirstChild<Sheets>().Elements<Sheet>().SingleOrDefault(s => s.Name == SheetName);
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(String.Format("Возможно в документе существует два листа с названием \"{0}\"!\n", SheetName), ex);
                    }

                    if (sheet == null)
                    {
                        throw new Exception(String.Format("В шаблоне не найден \"{0}\"!\n", SheetName));
                    }

                  
                    var worksheetPart = (WorksheetPart)document.WorkbookPart.GetPartById(sheet.Id.Value);
                    var sheetData = worksheetPart.Worksheet.GetFirstChild<SheetData>();

                    Cell cellDat = GetCell(worksheetPart.Worksheet, "C", 3);
                    cellDat.CellValue = new CellValue(list.Comment);
                    cellDat.DataType = new EnumValue<CellValues>(CellValues.String);
                    //данные по 2 смене 
                    uint i = 6;
                    foreach (var item in list.ShiftInfo2)
                    {

                        Row refRowC = sheetData.Elements<Row>().Where(r => 6 == r.RowIndex).First();
                        var row = new Row();
                        if (i == 6)
                        {
                            row = refRowC;
                        }
                        else
                        {
                            row = (Row)refRowC.Clone();
                            row.RowIndex = new UInt32Value(i);
                        }
                     
                        foreach (var cell in row.Elements<Cell>())
                        {
                            SetShiftTasksCellsValue(cell, item);
                            cell.CellReference = new StringValue(cell.CellReference.Value.Replace(refRowC.RowIndex.Value.ToString(), row.RowIndex.ToString()));
                          //  worksheetPart.Worksheet.Save();
                        }

                        if(i != 6 ) sheetData.Append(row);
                      //  worksheetPart.Worksheet.Save();
                        i++;
                      
                    }
                    UpdateAndCopyHeaderTable(worksheetPart, list.DateBegin, (uint)i);
                    uint k = i+4;
                    foreach (var item in list.ShiftInfo1)
                    {

                        Row refRowC = sheetData.Elements<Row>().Where(r => 6 == r.RowIndex).First();
                        var row = (Row)refRowC.Clone();
                        row.RowIndex = new UInt32Value(k);
                        foreach (var cell in row.Elements<Cell>())
                        {
                            SetShiftTasksCellsValue(cell, item );
                            cell.CellReference = new StringValue(cell.CellReference.Value.Replace(refRowC.RowIndex.Value.ToString(), row.RowIndex.ToString()));
                          //  worksheetPart.Worksheet.Save();
                        }

                        k++;
                        sheetData.Append(row);
                      //  worksheetPart.Worksheet.Save();
                    }
                    worksheetPart.Worksheet.Save();

                    //2 лист отчета 
                    var data2 = GetPlanFactShiftTask(id);

                    SetSecondListValues(document, data2);


                    SetSThirdListValues(document, data2);

                    //Антон
                    var freeEmployees = dayTasksDb.GetFreeEmployeesInDayTask(id);
                    var nMech = freeEmployees.ShiftInfo1.Where(t => t.Func.Equals("Механизатор")).OrderBy(t=>t.Name).ToList();
                    var nDriver = freeEmployees.ShiftInfo1.Where(t => t.Func.Equals("Водитель")).OrderBy(t => t.Name).ToList();
                    var nEmp = freeEmployees.ShiftInfo1.Where(t => t.Func.Equals("Ответственный")).OrderBy(t => t.Name).ToList();
                    freeEmployees.ShiftInfo1.Clear();
                    freeEmployees.ShiftInfo1.AddRange(nMech); freeEmployees.ShiftInfo1.AddRange(nDriver); freeEmployees.ShiftInfo1.AddRange(nEmp);
                    var nMech2 = freeEmployees.ShiftInfo2.Where(t => t.Func.Equals("Механизатор")).OrderBy(t => t.Name).ToList();
                    var nDriver2 = freeEmployees.ShiftInfo2.Where(t => t.Func.Equals("Водитель")).OrderBy(t => t.Name).ToList();
                    var nEmp2 = freeEmployees.ShiftInfo2.Where(t => t.Func.Equals("Ответственный")).OrderBy(t => t.Name).ToList();
                    freeEmployees.ShiftInfo2.Clear();
                    freeEmployees.ShiftInfo2.AddRange(nMech2); freeEmployees.ShiftInfo2.AddRange(nDriver2); freeEmployees.ShiftInfo2.AddRange(nEmp2);

                    SetFourListValues(document, freeEmployees);

                    document.Close();
                }
                return new ResultShiftReport
                {
                    bytes = stream.ToArray(),
                    date = list.DateBegin
                };
            }
        }

        private void UpdateAndCopyHeaderTable(WorksheetPart worksheetPart, DateTime date, uint index)
        {
            var sheetData = worksheetPart.Worksheet.GetFirstChild<SheetData>();
            // установка даты начала 2 смены 
            Cell cellDat = GetCell(worksheetPart.Worksheet, "C", 4);
            cellDat.CellValue = new CellValue(date.ToString("dd-MM-yyyy"));
            cellDat.DataType = new EnumValue<CellValues>(CellValues.String);

        

            Row refRow = sheetData.Elements<Row>().Where(r => 4 == r.RowIndex).First();

            var newRow = (Row)refRow.Clone();

            // установка даты начала 1 смены в хедоре 
            newRow.RowIndex = new UInt32Value(index + 2);
            foreach (var cell in newRow.Elements<Cell>())
            {
                string cellReference = cell.CellReference.Value;

                cell.CellReference = new StringValue(cellReference.Replace(refRow.RowIndex.Value.ToString(), newRow.RowIndex.ToString()));
                if (cellReference == "C4")
                {
                    cell.CellValue = new CellValue(date.AddDays(1).ToString("dd-MM-yyyy"));
                    cell.DataType = new EnumValue<CellValues>(CellValues.String);
                }
                if (cellReference == "D4")
                {
                    cell.CellValue = new CellValue("1-я смена");
                    cell.DataType = new EnumValue<CellValues>(CellValues.String);
                }

                worksheetPart.Worksheet.Save();
            }
            sheetData.Append(newRow);

            // копи хедер таблицы
            Row refRow2 = sheetData.Elements<Row>().Where(r => 5 == r.RowIndex).First();
            var newRow2 = (Row)refRow2.Clone();
            newRow2.RowIndex = new UInt32Value(index + 3);
            foreach (var cell in newRow2.Elements<Cell>())
            {
                string cellReference = cell.CellReference.Value;
                cell.CellReference = new StringValue(cellReference.Replace(refRow2.RowIndex.Value.ToString(), newRow2.RowIndex.ToString()));
            }
            sheetData.Append(newRow2); 
        }

        private void SetSecondListValues(SpreadsheetDocument document ,PlanFactModel data )
        {
            Sheet sheet;
            try
            {
                sheet = document.WorkbookPart.Workbook.GetFirstChild<Sheets>().Elements<Sheet>().SingleOrDefault(s => s.Name == SecondNameList);
            }
            catch (Exception ex)
            {
                throw new Exception(String.Format("Возможно в документе существует два листа с названием \"{0}\"!\n", SheetName), ex);
            }

            if (sheet == null)
            {
                throw new Exception(String.Format("В шаблоне не найден \"{0}\"!\n", SheetName));
            }

       
            var worksheetPart = (WorksheetPart)document.WorkbookPart.GetPartById(sheet.Id.Value);
            var sheetData = worksheetPart.Worksheet.GetFirstChild<SheetData>();

         

            //данные по 2 смене 
            uint i = 6;
            foreach (var item in data.ShiftInfo2)
            {
                Row refRowC = sheetData.Elements<Row>().Where(r => 6 == r.RowIndex).First();
                var row = new Row();
                if (i == 6)
                {
                    row = refRowC;
                }
                else
                {
                    row = (Row)refRowC.Clone();
                    row.RowIndex = new UInt32Value(i);
                }

                foreach (var cell in row.Elements<Cell>())
                {
                    SetShiftTasksCellsValue(cell, item);
                    cell.CellReference = new StringValue(cell.CellReference.Value.Replace(refRowC.RowIndex.Value.ToString(), row.RowIndex.ToString()));
                 //   worksheetPart.Worksheet.Save();
                }

                if (i != 6) sheetData.Append(row);
               // worksheetPart.Worksheet.Save();
                i++;

                foreach (var iteFm in item.Fact.OrderBy(f => f.PlantName))
                {
                    Row refRowCF = sheetData.Elements<Row>().Where(r => 7 == r.RowIndex).First();
                    var rowF = new Row();
                    if (i == 7)
                    {
                        rowF = refRowCF;
                    }
                    else
                    {
                        rowF = (Row)refRowCF.Clone();
                        rowF.RowIndex = new UInt32Value(i);
                    }

                    foreach (var cell in rowF.Elements<Cell>())
                    {
                        SetShiftTasksFactCellsValue(cell, iteFm);
                        cell.CellReference = new StringValue(cell.CellReference.Value.Replace(refRowCF.RowIndex.Value.ToString(), rowF.RowIndex.ToString()));
                     //   worksheetPart.Worksheet.Save();
                    }


                    if (i != 7) sheetData.Append(rowF);
                   // worksheetPart.Worksheet.Save();
                    i++;
                }
            }


            UpdateAndCopyHeaderTable(worksheetPart, data.DateBegin, (uint)i);

       
            uint k = i + 4;
            foreach (var item in data.ShiftInfo1)
            {

                Row refRowC = sheetData.Elements<Row>().Where(r => 6 == r.RowIndex).First();
                var row = (Row)refRowC.Clone();
                row.RowIndex = new UInt32Value(k);
                foreach (var cell in row.Elements<Cell>())
                {
                    SetShiftTasksCellsValue(cell, item);
                    cell.CellReference = new StringValue(cell.CellReference.Value.Replace(refRowC.RowIndex.Value.ToString(), row.RowIndex.ToString()));
                  //  worksheetPart.Worksheet.Save();
                }

                k++;
                sheetData.Append(row);
               // worksheetPart.Worksheet.Save();

                foreach (var iteFm in item.Fact.OrderBy(f => f.PlantName))
                {
                    Row refRowCF = sheetData.Elements<Row>().Where(r => 7 == r.RowIndex).First();
                    var rowF = new Row();
                    rowF = (Row)refRowCF.Clone();
                    rowF.RowIndex = new UInt32Value(k);

                    foreach (var cell in rowF.Elements<Cell>())
                    {
                        SetShiftTasksFactCellsValue(cell, iteFm);
                        cell.CellReference = new StringValue(cell.CellReference.Value.Replace(refRowCF.RowIndex.Value.ToString(), rowF.RowIndex.ToString()));
                    //    worksheetPart.Worksheet.Save();
                    }


                    sheetData.Append(rowF);
                  //  worksheetPart.Worksheet.Save();
                    k++;
                }
            }
            worksheetPart.Worksheet.Save();
        }

        private void SetSThirdListValues(SpreadsheetDocument document ,PlanFactModel data )
        {
            Sheet sheet;
            try
            {
                sheet = document.WorkbookPart.Workbook.GetFirstChild<Sheets>().Elements<Sheet>().SingleOrDefault(s => s.Name == ThirdNameList);
            }
            catch (Exception ex)
            {
                throw new Exception(String.Format("Возможно в документе существует два листа с названием \"{0}\"!\n", SheetName), ex);
            }

            if (sheet == null)
            {
                throw new Exception(String.Format("В шаблоне не найден \"{0}\"!\n", SheetName));
            }


            var worksheetPart = (WorksheetPart)document.WorkbookPart.GetPartById(sheet.Id.Value);
            var sheetData = worksheetPart.Worksheet.GetFirstChild<SheetData>();


            //данные по 2 смене 
            uint i = 6;
            var list =from d in data.ShiftInfo2.Select(d=>d.Fact)
                      from subD in d
                      where subD.FieldName != null && subD.PlantName != null 
                      select subD ;
            foreach (var item in list.OrderBy(f => f.PlantName))
            {
                
                Row refRowC = sheetData.Elements<Row>().Where(r => 6 == r.RowIndex).First();
                var row = new Row();
                if (i == 6)
                {
                    row = refRowC;
                }
                else
                {
                    row = (Row)refRowC.Clone();
                    row.RowIndex = new UInt32Value(i);
                }

                foreach (var cell in row.Elements<Cell>())
                {
                    SetShiftTasksFact2CellsValue(cell, item);
                    cell.CellReference = new StringValue(cell.CellReference.Value.Replace(refRowC.RowIndex.Value.ToString(), row.RowIndex.ToString()));
                  //  worksheetPart.Worksheet.Save();
                }

                if (i != 6) sheetData.Append(row);
               // worksheetPart.Worksheet.Save();
                i++;

            }
            UpdateAndCopyHeaderTable(worksheetPart, data.DateBegin, (uint)i);
            uint k = i + 4;

             var list2 =from d in data.ShiftInfo1.Select(d=>d.Fact)
                      from subD in d
                      where subD.FieldName != null && subD.PlantName != null 
                      select subD ;
            foreach (var item in list2.OrderBy(f => f.PlantName))
            {

                Row refRowC = sheetData.Elements<Row>().Where(r => 6 == r.RowIndex).First();
                var row = (Row)refRowC.Clone();
                row.RowIndex = new UInt32Value(k);
                foreach (var cell in row.Elements<Cell>())
                {
                    SetShiftTasksFact2CellsValue(cell, item);
                    cell.CellReference = new StringValue(cell.CellReference.Value.Replace(refRowC.RowIndex.Value.ToString(), row.RowIndex.ToString()));
                 //   worksheetPart.Worksheet.Save();
                }

                k++;
                sheetData.Append(row);
                worksheetPart.Worksheet.Save();
            }
            worksheetPart.Worksheet.Save(); 
        }
        public static void SetCellValue(Cell cell,string value )
        {
            cell.CellValue = new CellValue(string.IsNullOrEmpty(value) ? "" : value.ToString());
            cell.DataType = new EnumValue<CellValues>(CellValues.String);
        }

        //Антон----------------------------------------------------------------------------------------------------------------------------------------
        private void SetFourListValues(SpreadsheetDocument document, PrintShiftTasksEmployees data)
        {
            Sheet sheet;
            try
            {
                sheet = document.WorkbookPart.Workbook.GetFirstChild<Sheets>().Elements<Sheet>().SingleOrDefault(s => s.Name == FourNameList);
            }
            catch (Exception ex)
            {
                throw new Exception(String.Format("Возможно в документе существует два листа с названием \"{0}\"!\n", SheetName), ex);
            }

            if (sheet == null)
            {
                throw new Exception(String.Format("В шаблоне не найден \"{0}\"!\n", SheetName));
            }


            var worksheetPart = (WorksheetPart)document.WorkbookPart.GetPartById(sheet.Id.Value);
            var sheetData = worksheetPart.Worksheet.GetFirstChild<SheetData>();


            //данные по 2 смене 
            uint i = 6;

            var list = data.ShiftInfo2;

            foreach (var item in list )//.OrderBy(e => e.Name))
            {

                Row refRowC = sheetData.Elements<Row>().Where(r => 6 == r.RowIndex).First();
                var row = new Row();
                if (i == 6)
                {
                    row = refRowC;
                }
                else
                {
                    row = (Row)refRowC.Clone();
                    row.RowIndex = new UInt32Value(i);
                }

                foreach (var cell in row.Elements<Cell>())
                {
                    SetShiftTasksFact4CellsValue(cell, item);
                    cell.CellReference = new StringValue(cell.CellReference.Value.Replace(refRowC.RowIndex.Value.ToString(), row.RowIndex.ToString()));
                    //  worksheetPart.Worksheet.Save();
                }

                if (i != 6) sheetData.Append(row);
                // worksheetPart.Worksheet.Save();
                i++;

            }

            UpdateAndCopyHeader4Table(worksheetPart, data.DateBegin, (uint)i);
            uint k = i + 4;

            var list1 = data.ShiftInfo1;

            foreach (var item in list1)//.OrderBy(e => e.Name))
            {

                Row refRowC = sheetData.Elements<Row>().Where(r => 6 == r.RowIndex).First();
                var row = (Row)refRowC.Clone();
                row.RowIndex = new UInt32Value(k);
                foreach (var cell in row.Elements<Cell>())
                {
                    SetShiftTasksFact4CellsValue(cell, item);
                    cell.CellReference = new StringValue(cell.CellReference.Value.Replace(refRowC.RowIndex.Value.ToString(), row.RowIndex.ToString()));
                    //   worksheetPart.Worksheet.Save();
                }

                k++;
                sheetData.Append(row);
                worksheetPart.Worksheet.Save();
            }
            worksheetPart.Worksheet.Save();
        }
        //---------------------------------------------------------------------------------------------------------------------------------------------

        //Антон----------------------------------------------------------------------------------------------------------------------------------------
        private void SetShiftTasksFact4CellsValue(Cell cell, PrintShiftEmployees item)
        {
            string cellReference = cell.CellReference.Value;

            switch (cellReference)
            {
                case "A6":
                    SetCellValue(cell, item.Name);
                    break;
                case "B6":
                    SetCellValue(cell, item.Func);
                    break;
            }
        }
        //---------------------------------------------------------------------------------------------------------------------------------------------

        //Антон----------------------------------------------------------------------------------------------------------------------------------------
        private void UpdateAndCopyHeader4Table(WorksheetPart worksheetPart, DateTime date, uint index)
        {
            var sheetData = worksheetPart.Worksheet.GetFirstChild<SheetData>();
            // установка даты начала 2 смены 
            Cell cellDat = GetCell(worksheetPart.Worksheet, "B", 4);
            cellDat.CellValue = new CellValue(date.ToString("dd-MM-yyyy"));
            cellDat.DataType = new EnumValue<CellValues>(CellValues.String);

            Row refRow = sheetData.Elements<Row>().Where(r => 4 == r.RowIndex).First();

            var newRow = (Row)refRow.Clone();

            // установка даты начала 1 смены в хедоре 
            newRow.RowIndex = new UInt32Value(index + 2);
            foreach (var cell in newRow.Elements<Cell>())
            {
                string cellReference = cell.CellReference.Value;

                cell.CellReference = new StringValue(cellReference.Replace(refRow.RowIndex.Value.ToString(), newRow.RowIndex.ToString()));
                if (cellReference == "B4")
                {
                    cell.CellValue = new CellValue(date.AddDays(1).ToString("dd-MM-yyyy"));
                    cell.DataType = new EnumValue<CellValues>(CellValues.String);
                }
                if (cellReference == "C4")
                {
                    cell.CellValue = new CellValue("1-я смена");
                    cell.DataType = new EnumValue<CellValues>(CellValues.String);
                }

                worksheetPart.Worksheet.Save();
            }
            sheetData.Append(newRow);

            // копи хедер таблицы
            Row refRow2 = sheetData.Elements<Row>().Where(r => 5 == r.RowIndex).First();
            var newRow2 = (Row)refRow2.Clone();
            newRow2.RowIndex = new UInt32Value(index + 3);
            foreach (var cell in newRow2.Elements<Cell>())
            {
                string cellReference = cell.CellReference.Value;
                cell.CellReference = new StringValue(cellReference.Replace(refRow2.RowIndex.Value.ToString(), newRow2.RowIndex.ToString()));
            }
            sheetData.Append(newRow2);
        }
        //---------------------------------------------------------------------------------------------------------------------------------------------

        private void SetShiftTasksCellsValue(Cell cell, PrintShiftDTO item )
        {
            string cellReference = cell.CellReference.Value;

            switch (cellReference)
            {
                case "A6":
                    SetCellValue(cell, item.TitleColumn);
                    break;
                case "B6":
                    SetCellValue(cell, item.PlantName);
                    break;
                case "C6":
                    SetCellValue(cell, item.FieldName);
                    break;
                case "D6":
                    SetCellValue(cell, item.TaskPlan);
                    break;
                case "E6":
                    SetCellValue(cell, item.TraktorName);
                    break;
                case "F6":
                    SetCellValue(cell, item.EquipmentName);
                    break;
                case "G6":
                    SetCellValue(cell, item.Employees);
                    break;
                case "H6":
                    SetCellValue(cell, item.Available);
                    break;
                case "I6":
                    SetCellValue(cell, item.Comment);
                    break;
               /* case "J6":
                    SetCellValue(cell, item.MainComment);
                    break; */
            }
        
        }
        private void SetShiftTasksFactCellsValue(Cell cell, FactDTO item)
        {
            string cellReference = cell.CellReference.Value;

            switch (cellReference)
            {
                case "A7":
                    SetCellValue(cell, item.TitleColumn);
                    break;
                case "B7":
                    SetCellValue(cell, item.PlantName);
                    break;
                case "C7":
                    SetCellValue(cell, item.FieldName);
                    break;
                case "D7":
                    SetCellValue(cell, item.TaskPlan);
                    break;
                case "E7":
                    SetCellValue(cell, item.TraktorName);
                    break;
                case "F7":
                    SetCellValue(cell, item.EquipmentName);
                    break;
                case "G7":
                    SetCellValue(cell, item.Employees);
                    break;
                case "H7":
                    SetCellValue(cell, item.Available);
                    break;
                case "I7":
                    SetCellValue(cell, item.WorkTime.ToString());
                    break;
                case "J7":
                    SetCellValue(cell, item.Salary.ToString());
                    break;
                case "K7":
                    SetCellValue(cell, item.Area.ToString());
                    break;
                case "L7":
                    SetCellValue(cell, item.ConsumptionFact.ToString());
                    break;
            }
        }

        private void SetShiftTasksFact2CellsValue(Cell cell, FactDTO item)
        {
            string cellReference = cell.CellReference.Value;

            switch (cellReference)
            {
                case "A6":
                    SetCellValue(cell, item.TitleColumn);
                    break;
                case "B6":
                    SetCellValue(cell, item.PlantName);
                    break;
                case "C6":
                    SetCellValue(cell, item.FieldName);
                    break;
                case "D6":
                    SetCellValue(cell, item.TaskPlan);
                    break;
                case "E6":
                    SetCellValue(cell, item.TraktorName);
                    break;
                case "F6":
                    SetCellValue(cell, item.EquipmentName);
                    break;
                case "G6":
                    SetCellValue(cell, item.Employees);
                    break;
                case "H6":
                    SetCellValue(cell, item.Available);
                    break;
                case "I6":
                    SetCellValue(cell, item.WorkTime.ToString());
                    break;
                case "J6":
                    SetCellValue(cell, item.Salary.ToString());
                    break;
                case "K6":
                    SetCellValue(cell, item.Area.ToString());
                    break;
                case "L6":
                    SetCellValue(cell, item.ConsumptionFact.ToString());
                    break;
            }
        }

        public static Cell GetCell(Worksheet worksheet,string columnName, uint rowIndex)
        {
            Row row = GetRow(worksheet, rowIndex);

            if (row == null)
                return null;

            return row.Elements<Cell>().Where(c => string.Compare
                   (c.CellReference.Value, columnName +
                   rowIndex, true) == 0).First();
            //return row.Elements<Cell>().Where(c => c.CellReference.Value == (columnName + rowIndex.ToString())).First();
        }

        public static void SetValueForCellByRow(Row row, string columnName, uint rowIndex, object value)
        {
            Cell cellDat2 = row.Elements<Cell>().Where(c => string.Compare
                            (c.CellReference.Value, columnName +
                            rowIndex, true) == 0).FirstOrDefault();
            cellDat2.CellValue = new CellValue(value == null ? " " : value.ToString());
            cellDat2.DataType = new EnumValue<CellValues>(CellValues.String);
        }

        public static void SetValueForDecimalCellByRow(Row row, string columnName, uint rowIndex, decimal? value)
        {
            Cell cellDat2 = row.Elements<Cell>().Where(c => string.Compare
                            (c.CellReference.Value, columnName +
                            rowIndex, true) == 0).FirstOrDefault();
            cellDat2.CellValue = new CellValue(value == null ? "" : value.Value.ToString().Replace(',','.'));
            cellDat2.DataType = new EnumValue<CellValues>(CellValues.Number);
        }
    

        // Given a worksheet and a row index, return the row.
        public static Row GetRow(Worksheet worksheet, uint rowIndex)
        {
            return worksheet.GetFirstChild<SheetData>().
              Elements<Row>().Where(r => r.RowIndex == rowIndex).First();
        }

        private PlanFactModel GetPlanFactShiftTask(int id)
        {

            PlanFactModel result = new PlanFactModel();
            var datibegin = db.Day_task.Where(dt => dt.dt_id == id).Select(dt => dt.dt_date).FirstOrDefault();
            var listTask = (from dt in db.Day_task_detail
                            where dt.dt_dt_id == id
                            join shtr in db.Sheet_tracks on dt.dtd_id equals shtr.dtd_dtd_id into shtrLeft
                            from sh in shtrLeft.DefaultIfEmpty()

                            join tasks in db.Tasks on sh.shtr_id equals tasks.shtr_shtr_id into tasksLeft
                            from t in tasksLeft.DefaultIfEmpty()
                            group new FactDTO {
                                   FieldName =  t.Fields_to_plants.Fields.name,
                                   PlantName =  t.Fields_to_plants.Plants.name,
                                   AreaPlant = t.Fields_to_plants.area_plant,
                                   TraktorName = sh.Traktors.name,
                                   Employees = t.Sheet_tracks.Employees.short_name,
                                   Available = t.Employees.short_name,
                                   TaskPlan = t.Type_tasks.name,
                                   EquipmentName = t.Sheet_tracks.Equipments.name,
                                   WorkTime = t.worktime,
                                   Salary = t.salary,
                                   Area = t.area,
                                   ConsumptionFact= t.consumption_fact,
                                   TitleColumn = "Факт",
                            }
                            by dt into g
                            select new PlanAndFactDTO
                            {
                                Id = g.Key.dtd_id,
                                FieldName = g.Key.Fields.name,
                                PlantName = g.Key.Plants.name,
                                EquipmentName = g.Key.Group_equipments.name,
                                TraktorName = g.Key.Group_traktors.name,
                                Employees = g.Key.emp2_emp_id != null ?  g.Key.Employees.short_name + " / " +  g.Key.Employees1.short_name :  g.Key.Employees.short_name,
                                Available =g.Key.Employees2.short_name,
                                Shift = g.Key.shift_num,
                                TaskPlan = g.Key.Type_tasks.name,
                                Comment = g.Key.comment,
                                TitleColumn = "План",
                                Fact = g.ToList()
                            }).ToList();

            result.ShiftInfo1 = listTask.Where(r => r.Shift == 1).OrderBy(l => l.PlantName).ToList();
            result.ShiftInfo2 = listTask.Where(r => r.Shift == 2).OrderBy(l => l.PlantName).ToList();
            result.DateBegin = datibegin;
            return result;
               
           
        }

    }
}

