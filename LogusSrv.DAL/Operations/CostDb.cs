﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using System.Threading.Tasks;
using LogusSrv.DAL.DBModel;
using LogusSrv.DAL.Enums;
using LogusSrv.DAL.Entities.DTO.ExportExcel;
using LogusSrv.DAL.Entities.DTO;
using LogusSrv.CORE.Exceptions;
using LogusSrv.DAL.Entities.DTO.DayTaskExt;
using LogusSrv.DAL.Entities.Res;
using LogusSrv.DAL.Entities.Req;
using LogusSrv.DAL.Utils;
using LogusSrv.DAL.Entities.DTO.SeedingBridle;
using LogusSrv.CORE.REST;
using LogusSrv.DAL.Operations;
using System.Data;
using LogusSrv.DAL.Entities.DTO.ActsSzr;
using EPPlus;
using LogusSrv.DAL.Entities.SQL;
using System.Data.Entity.SqlServer;
using System.Text.RegularExpressions;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System.Net;
using System.Reflection;
using System.Collections;
using LogusSrv.DAL.Operations.DictionaryOperations;
using System.Drawing;
using System.Xml;
using System.Threading;
using System.Globalization;
namespace LogusSrv.DAL.Operations
{

    public class CostDb : ExportToExcel
    {
        protected readonly LogusDBEntities db = new LogusDBEntities();
        private readonly RateDb rateDb = new RateDb();
        public readonly TcReportDb tcReportDb = new TcReportDb();
        public readonly ReportsModuleDb reportsModuleDb = new ReportsModuleDb();
        public TechCardDb techcard = new TechCardDb();
        public DictionaryDb dictionary = new DictionaryDb();
        private readonly SparePartsDb spareDb = new SparePartsDb();
        public ReportFile GetCostListResExcel(string pathTemplate)
        {
            var tmpl = new FileInfo(pathTemplate);
            using (ExcelPackage pck = new ExcelPackage(tmpl, true))
            {
                ExcelWorksheet wsh = pck.Workbook.Worksheets[1];
                wsh.Name = "Расценки";
                var list = rateDb.GetWorks();
                uint k = 5;
                for (int i = 0; i < list.Count; i++)
                {
                    if (list[i].Values.Count != 0)
                    {
                        for (int j = 0; j < list[i].Values.Count; j++)
                        {   
                            wsh.Cells["A" + k.ToString()].Value = list[i].Name;
                            wsh.Cells["B" + k.ToString()].Value = list[i].Values[j].IdPlant.Name;
                            wsh.Cells["C" + k.ToString()].Value = list[i].Values[j].IdAgroReq.Name;
                            wsh.Cells["D" + k.ToString()].Value = list[i].Values[j].IdTrak.Name;
                            wsh.Cells["E" + k.ToString()].Value = list[i].Values[j].IdEqui.Name;
                            wsh.Cells["G" + k.ToString()].Value = list[i].Values[j].ShiftOutput;
                            wsh.Cells["H" + k.ToString()].Value = list[i].Values[j].FuelConsumption;
                            wsh.Cells["I" + k.ToString()].Value = list[i].Values[j].RateShift;
                            wsh.Cells["J" + k.ToString()].Value = list[i].Values[j].RatePiecework;
                            k++;

                        }
                    }
                }
                if (k != 5)
                {
                    setBorder(wsh.Cells["A5:J" + (k - 1).ToString()].Style.Border);
                }
                var r = new ReportFile
                {
                    data = pck.GetAsByteArray(),
                    fileName = "Расценки.xlsx"
                };
                return r;
            }
        }
        //план-факт
        public ReportFile GetPlanFactExcel(string pathTemplate, int year, bool isBig)
        {
            var tmpl = new FileInfo(pathTemplate);
            using (ExcelPackage pck = new ExcelPackage(tmpl, true))
            {
                ExcelWorksheet wsh = pck.Workbook.Worksheets[1];
                wsh.Name = "Статьи затрат";
                var list = reportsModuleDb.GetCostsByCulture(year, null);
                uint k = 3;
                for (int i = 0; i < list.Count; i++)
                {
                    wsh.Cells["A" + k.ToString()].Value = list[i].plant_name;
                    wsh.Cells["b" + k.ToString()].Value = list[i].area;
                    wsh.Cells["c" + k.ToString()].Value = list[i].gross_harvest;
                    wsh.Cells["d" + k.ToString()].Value = list[i].salary_total;
                    wsh.Cells["e" + k.ToString()].Value = list[i].razx_top;
                    wsh.Cells["f" + k.ToString()].Value = list[i].minud_fact;
                    wsh.Cells["g" + k.ToString()].Value = list[i].szr_fact; 
                    wsh.Cells["h" + k.ToString()].Value = list[i].posevmat_fact;
                    wsh.Cells["i" + k.ToString()].Value = list[i].dop_material_fact;
                    wsh.Cells["j" + k.ToString()].Value = list[i].service_total;
                    wsh.Cells["k" + k.ToString()].Value = list[i].nzp;
                    wsh.Cells["l" + k.ToString()].Value = list[i].total_plan;
                    wsh.Cells["m" + k.ToString()].Value = list[i].total_fact;
                    wsh.Cells["o" + k.ToString()].Value = list[i].total_delta;
                    wsh.Cells["p" + k.ToString()].Value = list[i].cost_ga_plan;
                    wsh.Cells["q" + k.ToString()].Value = list[i].cost_ga_fact;
                    wsh.Cells["r" + k.ToString()].Value = list[i].cost_ga_delta;
                    wsh.Cells["s" + k.ToString()].Value = list[i].cost_t_plan;
                    wsh.Cells["t" + k.ToString()].Value = list[i].cost_t_fact;
                    wsh.Cells["u" + k.ToString()].Value = list[i].cost_t_delta;
                    k++;
                    if (isBig) {
                        wsh.Cells["d" + k.ToString()].Value = list[i].plan_salary_total;
                        wsh.Cells["e" + k.ToString()].Value = list[i].plan_razx_top;
                        wsh.Cells["f" + k.ToString()].Value = list[i].plan_minud;
                        wsh.Cells["g" + k.ToString()].Value = list[i].plan_szr;
                        wsh.Cells["h" + k.ToString()].Value = list[i].plan_posevmat;
                        wsh.Cells["i" + k.ToString()].Value = list[i].plan_dop_material;
                        wsh.Cells["j" + k.ToString()].Value = list[i].plan_service_total;
                        k++;
                    }
                }
                if (k != 3)
                {
                    setBorder(wsh.Cells["A3:U" + (k - 1).ToString()].Style.Border);
                }
                //лист2
                k = 4;
                ExcelWorksheet wsh2 = pck.Workbook.Worksheets[2];
                for (int i = 0; i < list.Count; i++)
                {
                    wsh2.Cells["A" + k.ToString()].Value = list[i].plant_name;
                    wsh2.Cells["b" + k.ToString()].Value = list[i].salary_total;
                    wsh2.Cells["c" + k.ToString()].Value = list[i].razx_top;
                    wsh2.Cells["d" + k.ToString()].Value = list[i].minud_fact;
                    wsh2.Cells["e" + k.ToString()].Value = list[i].szr_fact;
                    wsh2.Cells["f" + k.ToString()].Value = list[i].posevmat_fact;
                    wsh2.Cells["g" + k.ToString()].Value = list[i].dop_material_fact;
                    wsh2.Cells["h" + k.ToString()].Value = list[i].service_total;
                    k++;
                }
                //лист3
                var ListOfPlantId = new List<int>();
                var list2 = new List<TCPlanFactModel>();
                ExcelWorksheet wsh3 = pck.Workbook.Worksheets[3];
                float cost = 0, cost_salary = 0, cost_fuel = 0, cost_szr = 0,cost_min = 0, cost_seed = 0,
                  cost_service = 0, cost_dop_material = 0;
                for (int j = 0; j < list.Count; j++)
                {
                    ListOfPlantId.Add(list[j].plant_id);
                }
                for (int j = 0; j < ListOfPlantId.Count; j++)
                {
                    list2.Add(reportsModuleDb.GetPlanFactTC(year, ListOfPlantId[j], -1));
                }
                for (int j = 0; j < ListOfPlantId.Count; j++)
                {
                    cost = cost + list2[j].plan.cost;
                    cost_salary = cost_salary + list2[j].plan.cost_salary;
                    cost_fuel = cost_fuel + list2[j].plan.cost_fuel;
                    cost_szr = cost_szr + list2[j].plan.cost_szr ?? 0;
                    cost_min = cost_min + list2[j].plan.cost_min ?? 0;
                    cost_seed = cost_seed + list2[j].plan.cost_seed ?? 0;
                    cost_service = cost_service + (float)list2[j].plan.service_total;
                    cost_dop_material = cost_dop_material + (float)list2[j].plan.dop_material_fact;
                }
                List<float?> plan = new List<float?>();
                List<float?> fact = new List<float?>();
                plan.Add(cost); plan.Add(cost_salary); plan.Add(cost_fuel); plan.Add(cost_szr); plan.Add(cost_min);
                plan.Add(cost_seed); plan.Add(cost_dop_material); plan.Add(cost_service);

                fact.Add((float)list[list.Count - 1].total_fact);
                fact.Add((float)list[list.Count - 1].salary_total);
                fact.Add((float)list[list.Count - 1].razx_top);
                fact.Add(list[list.Count - 1].szr_fact);
                fact.Add((float)list[list.Count - 1].minud_fact);
                fact.Add(list[list.Count - 1].posevmat_fact);
                fact.Add((float)list[list.Count - 1].dop_material_fact);
                fact.Add((float)list[list.Count - 1].service_total);
                k = 3;
                for (int j = 0; j < plan.Count; j++)
                {
                    wsh3.Cells["b" + k.ToString()].Value = plan[j];
                    k++;
                }
                k = 3;
                for (int j = 0; j < fact.Count; j++)
                {
                    wsh3.Cells["c" + k.ToString()].Value = fact[j];
                    k++;
                }

                var r = new ReportFile
                {
                    data = pck.GetAsByteArray(),
                    fileName = "План-фактный анализ.xlsx"
                };
                return r;
            }
        }

        //GetDayListRepairExcel
        public ReportFile GetDayListRepairExcel(string pathTemplate, int id, DateTime date)
        {
            var tmpl = new FileInfo(pathTemplate);
            using (ExcelPackage pck = new ExcelPackage(tmpl, true))
            {
                ExcelWorksheet wsh = pck.Workbook.Worksheets[1];
                wsh.Name = "Сменное задание";

                var arr = new GetDayTasksReq();
                arr.DateFilter = date;
                var list = spareDb.GetDayRepairs(arr);
                wsh.Cells["c3"].Value = list.Comment;
                wsh.Cells["c4"].Value = date.ToString("dd.MM.yyyy");
                uint k = 6;
                var data = list.Shift1;
                for (int i = 0; i < data.Count; i++)
                        {
                            wsh.Cells["b" + k.ToString()].Value = data[i].Number;
                            wsh.Cells["c" + k.ToString()].Value = data[i].TypeTaskInfo.Name;
                            wsh.Cells["d" + k.ToString()].Value = data[i].EquipmentsInfo.TracktorInfo.Name;
                            wsh.Cells["e" + k.ToString()].Value = data[i].EquipmentsInfo.EquipInfo.Name;
                            if (data[i].EmpList != null)
                            {
                                var name = "";
                                for (int l = 0; l < data[i].EmpList.Count; l++)
                                {
                                    name = name + "/" + data[i].EmpList[l].Name;
                                }
                                wsh.Cells["f" + k.ToString()].Value = name;
                            }
                            wsh.Cells["g" + k.ToString()].Value = data[i].AvailableInfo.Name;
                            wsh.Cells["h" + k.ToString()].Value = data[i].Comment;
                            wsh.Cells["i" + k.ToString()].Value = data[i].NumDailyId;
                            k++;

                        }
                if (k != 6)
                {
                    setBorder(wsh.Cells["b6:i" + (k - 1).ToString()].Style.Border);
                    SetCellStyle(wsh.Cells["b6:i" + (k - 1).ToString()], Color.FromArgb( 218, 238, 243));
                }
                //2 стр.
                k = 6;
                wsh = pck.Workbook.Worksheets[2];
                wsh.Name = "Факт";
                wsh.Cells["c4"].Value = date.ToString("dd.MM.yyyy");
                for (int i = 0; i < data.Count; i++)
                {
                    wsh.Cells["b" + k.ToString()].Value = data[i].Number;
                    wsh.Cells["c" + k.ToString()].Value = data[i].TypeTaskInfo.Name;
                    wsh.Cells["d" + k.ToString()].Value = data[i].EquipmentsInfo.TracktorInfo.Name;
                    wsh.Cells["e" + k.ToString()].Value = data[i].EquipmentsInfo.EquipInfo.Name;
                    if (data[i].EmpList != null)
                    {
                        var name = "";
                        for (int l = 0; l < data[i].EmpList.Count; l++)
                        {
                            name = name + "/" + data[i].EmpList[l].Name;
                        }
                        wsh.Cells["f" + k.ToString()].Value = name;
                    }
                    wsh.Cells["g" + k.ToString()].Value = data[i].AvailableInfo.Name;
                    wsh.Cells["h" + k.ToString()].Value = data[i].Hours;
                    wsh.Cells["i" + k.ToString()].Value = data[i].Summa;
                    k++;

                }
                if (k != 6)
                {
                    setBorder(wsh.Cells["b6:i" + (k - 1).ToString()].Style.Border);
                    SetCellStyle(wsh.Cells["b6:i" + (k - 1).ToString()], Color.FromArgb(235, 241, 222));
                }
                // 3 стр.
                k = 6;
                wsh = pck.Workbook.Worksheets[3];
                wsh.Name = "Незадействованный персонал";
                wsh.Cells["b3"].Value = list.Comment;
                wsh.Cells["b4"].Value = date.ToString("dd.MM.yyyy");
                for (int i = 0; i < list.FreeEmp.Count; i++)
                {
                    wsh.Cells["a" + k.ToString()].Value = list.FreeEmp[i].Name;
                    wsh.Cells["b" + k.ToString()].Value = list.FreeEmp[i].Description;
                    k++;
                }
                if (k != 6)
                {
                    setBorder(wsh.Cells["a6:b" + (k - 1).ToString()].Style.Border);
                    SetCellStyle(wsh.Cells["a6:b" + (k - 1).ToString()], Color.FromArgb(235, 241, 222));
                }



                var r = new ReportFile
                {
                    data = pck.GetAsByteArray(),
                    fileName = "Сменное задание.xlsx"
                };
                return r;
            }
        }

        //списание движение
        public List<WriteOffActsDetails> GetSpareWriteoffMove(DateTime start, DateTime end)
        {
            var result = db.Repairs_acts.Where(s => s.status == 3).Select(g => new WriteOffActsDetails
            {
                Id = g.rep_id,
                Date = g.date_end,
                SparePartsList = db.Repairs_Records.Where(p => p.rep_rep_id == g.rep_id //&& p.spare_spare_id == 67 //test1
               && DbFunctions.TruncateTime(p.date) >= DbFunctions.TruncateTime(start)
               && DbFunctions.TruncateTime(p.date) <= DbFunctions.TruncateTime(end)
               ).Select(m => new SparePartsModel
               {
                   SpareParts = new DictionaryItemsDTO
                   {
                       Id = (int)m.spare_spare_id,
                       Name = db.Spare_parts.Where(v => v.spare_id == m.spare_spare_id).Select(v => v.name).FirstOrDefault()
                   },
                   Count = m.count,
                   Price = db.Spare_parts_Records.Where(p => p.spss_spss_id == g.rep_id && p.store_from_id != 0 && p.store_to_id == 0 &&
                       p.spare_spare_id == m.spare_spare_id && p.store_from_id == m.store_store_id).Select(t => t.price).FirstOrDefault(),
            //       Cost1 = Math.Round(m.price * m.count, 2),
                   Id = m.repair_id,
                   Store_id = (int)m.store_store_id,
                   DateBegin = m.date,
                   Pk = 1,
               }).ToList(),
            }).ToList();
            for (int i = 0; i < result.Count; i++) {
                for (int j = 0; j < result[i].SparePartsList.Count; j++)
                {
                    result[i].SparePartsList[j].Cost1 = result[i].SparePartsList[j].Price * result[i].SparePartsList[j].Count;
                }
            }
                return result;
        }

        //резерв 
        public List<SparePartsReserveModel> GetReserveMove(DateTime start, DateTime? end)
        {
            var list = (from s in db.Repairs_Records
                        where s.count != 0 
                        join k1 in db.Spare_parts on s.spare_spare_id equals k1.spare_id into left1
                        from t1 in left1.DefaultIfEmpty()
                        join k2 in db.Storages on s.store_store_id equals k2.store_id into left2
                        from t2 in left2.DefaultIfEmpty()
                        join k3 in db.Repairs_acts on s.rep_rep_id equals k3.rep_id into left3
                        from t3 in left3.DefaultIfEmpty()
                        where (end == null ? (t3.status == 1 && DbFunctions.TruncateTime(s.date) <= start) : true)  && 

                    (end != null ? (t3.status == 1 && DbFunctions.TruncateTime(s.date) >= DbFunctions.TruncateTime(start) 
                    && DbFunctions.TruncateTime(s.date) <= DbFunctions.TruncateTime(end)) : true)
                        group new
                        {
                            count = s.count,
                        } by new
                        {
                            SpareParts = new DictionaryItemsDTO
                            {
                                Id = t1.spare_id,
                                Name = t1.name,
                            },
                            Store_id = t2.store_id,
                        } into g
                        select new SparePartsReserveModel
                        {
                            Count = g.Sum(t => t.count),
                            Store_id = g.Key.Store_id,
                            SpareParts = g.Key.SpareParts,
                        }).ToList();
            return list;
        }


        //получение списания, поступления, баланса
        public MoveSpareParts GetIncomeWriteOffBalance(List<SparePartsDetails> income, List<WriteOffActsDetails> writeoff, bool checkBalance)
        {
            var balance = new List<SparePartsModel>();
            var spartpartslist = new List<SparePartsModel>();
            var writeOfflist = new List<SparePartsModel>();
            var moveList = new MoveSpareParts();

            var invoiceWriteOff = new List<SparePartsModel>();
            //списание-поступление
            var id = 1;
            for (int i = 0; i < income.Count; i++)
            {
                for (int j = 0; j < income[i].SparePartsList.Count; j++)
                { 
                invoiceWriteOff.Add(new SparePartsModel {
                    Id = id,
                    Spare_id = income[i].SparePartsList[j].SpareParts.Id,
                    Store_id = income[i].SparePartsList[j].Store_id,
                    SpareParts = income[i].SparePartsList[j].SpareParts,
                    Price = income[i].SparePartsList[j].Price,
                    Count = income[i].SparePartsList[j].Count,
                    Cost1 = income[i].SparePartsList[j].Price * income[i].SparePartsList[j].Count,
                    DateBegin = income[i].Date,
                    Pk = 1,
                });
                id++;
                }
            }
            for (int i = 0; i < writeoff.Count; i++)
            {
                for (int j = 0; j < writeoff[i].SparePartsList.Count; j++)
                {
                    invoiceWriteOff.Add(new SparePartsModel
                    {
                        Id = id,
                        Price = writeoff[i].SparePartsList[j].Price,
                        Count = writeoff[i].SparePartsList[j].Count,
                        Spare_id = writeoff[i].SparePartsList[j].SpareParts.Id,
                        Store_id = writeoff[i].SparePartsList[j].Store_id,
                        DateBegin = writeoff[i].SparePartsList[j].DateBegin,
                        SpareParts = writeoff[i].SparePartsList[j].SpareParts,
                        Cost1 = writeoff[i].SparePartsList[j].Price*writeoff[i].SparePartsList[j].Count,
                        Pk = 2,
                    });
                    id++;
                }
            }
            /////////////////списание и приход
            invoiceWriteOff = invoiceWriteOff.OrderBy(t => t.DateBegin).ToList();
            for (int i = 0; i < invoiceWriteOff.Count; i++)
            {
                if (checkBalance)
                {
                    var bal = new SparePartsModel();
                    //баланс
                    bal = balance.Where(t => t.Spare_id == invoiceWriteOff[i].SpareParts.Id
                    && t.Store_id == invoiceWriteOff[i].Store_id).FirstOrDefault();
                    if (bal == null)
                    {
                        //если поступление
                        if (invoiceWriteOff[i].Pk == 1)
                        {
                            balance.Add(new SparePartsModel
                            {
                                Spare_id = invoiceWriteOff[i].SpareParts.Id,
                                Store_id = invoiceWriteOff[i].Store_id,
                                Price = invoiceWriteOff[i].Price,
                                Count = invoiceWriteOff[i].Count,
                                SpareParts1 = invoiceWriteOff[i].SpareParts.Name,
                            });
                        }
                        //если списание
                        if (invoiceWriteOff[i].Pk == 2)
                        {
                            balance.Add(new SparePartsModel
                            {
                                Spare_id = invoiceWriteOff[i].SpareParts.Id,
                                Store_id = invoiceWriteOff[i].Store_id,
                                Price = invoiceWriteOff[i].Price,
                                Count = - invoiceWriteOff[i].Count,
                                SpareParts1 = invoiceWriteOff[i].SpareParts.Name,
                            });
                        }
                    }
                    else
                    {
                        //если приход
                        if (invoiceWriteOff[i].Pk == 1)
                        {
                            var newCount = (decimal)(bal.Count + invoiceWriteOff[i].Count);
                            if (newCount != 0)
                            {
                                bal.Price = (float)(((decimal)(bal.Price ?? 0) * (decimal)(bal.Count ?? 0) +
                               (decimal)(invoiceWriteOff[i].Price ?? 0) * (decimal)(invoiceWriteOff[i].Count ?? 0)) / newCount);
                            }
                            else { bal.Price = 0; }
                            bal.Count = (float)newCount;
                        }
                        //если списание
                        else
                        {
                            bal.Count = bal.Count - invoiceWriteOff[i].Count;
                        }
                    }
                }

                    if (invoiceWriteOff[i].Pk == 1)
                    {
                        //========суммируем приходы
                        if (spartpartslist.Where(t => t.SpareParts.Id == invoiceWriteOff[i].SpareParts.Id &&
                        t.Store_id == invoiceWriteOff[i].Store_id).FirstOrDefault() != null)
                        {
                            //if (invoiceWriteOff[i].SpareParts.Id == 149)
                            //{
                            //    var df = 0;
                            //}
                            var upd = spartpartslist.Where(t => t.SpareParts.Id == invoiceWriteOff[i].SpareParts.Id &&
                            t.Store_id == invoiceWriteOff[i].Store_id).FirstOrDefault();
                            upd.Count = upd.Count + invoiceWriteOff[i].Count;
                            upd.Cost1 = upd.Cost1 + (invoiceWriteOff[i].Price * invoiceWriteOff[i].Count);
                        }
                        else
                        {
                            var item = (SparePartsModel)invoiceWriteOff[i].Clone();
                            spartpartslist.Add(item);
                        }
                    }
                    else {
                        //========суммируем списание
                        if (writeOfflist.Where(t => t.SpareParts.Id == invoiceWriteOff[i].SpareParts.Id &&
                       t.Store_id == invoiceWriteOff[i].Store_id).FirstOrDefault() != null)
                        {
                            var upd = writeOfflist.Where(t => t.SpareParts.Id == invoiceWriteOff[i].SpareParts.Id &&
                            t.Store_id == invoiceWriteOff[i].Store_id).FirstOrDefault();
                            upd.Count = upd.Count + invoiceWriteOff[i].Count;
                            upd.Cost1 = upd.Cost1 + (invoiceWriteOff[i].Price * invoiceWriteOff[i].Count);
                        }
                        else
                        {
                            var item = (SparePartsModel)invoiceWriteOff[i].Clone();
                            writeOfflist.Add(item);
                        }
                    }  

            }
            moveList.BalanceList = balance;
            moveList.InvoiceList = spartpartslist;
            moveList.WriteOffList = writeOfflist;
            return moveList;
        }

        //движение запчастей MoveSparePartsReportExcel
        public ReportFile MoveSparePartsReportExcel(string pathTemplate, DateTime start, DateTime end)
        {
            var tmpl = new FileInfo(pathTemplate);
            var firstDay = new DateTime(0001, 01, 01);

            using (ExcelPackage pck = new ExcelPackage(tmpl, true))
            {
                ExcelWorksheet wsh = pck.Workbook.Worksheets[1];
                wsh.Name = "Движение запчастей";
                var income = GetSpareIncomeXML(start, end, false);  // приход в периоде

                //var s = 0.0;
                //for (int i = 0; i < income.Count; i++) {
                //    s = s + income[i].SparePartsList.Sum(t => t.Cost3 ?? 0);
                //}

                var writeoff = GetSpareWriteoffMove(start, end ); //списание в периоде
                var income2 = GetSpareIncomeXML(firstDay, start.AddDays(-1), false);  // приходы все
                var writeoff2 = GetSpareWriteoffMove(firstDay, start.AddDays(-1)); //списание все

                var income3 = GetSpareIncomeXML(firstDay, end, false);  // приход  для конца
                var writeoff3 = GetSpareWriteoffMove(firstDay, end); //списание для конца
                //test spare
            //    writeoff = writeoff.Where(t => t.SparePartsList.Count != 0).ToList();
             //    income = income.Where(t => t.SparePartsList.Count != 0).ToList();
                //
                var balance = new List<SparePartsModel>();

                var reserveList = GetReserveMove(start, end);                 //резерв
                var reserveStart = GetReserveMove(start.AddDays(1), null);
                var reserveEnd = GetReserveMove(end.AddDays(1), null);
 
                var data = GetIncomeWriteOffBalance(income2, writeoff2, true); // баланс для начала
                var dataEnd = GetIncomeWriteOffBalance(income3, writeoff3, true); // баланс для конца

                List<SparePartsModel> balance2 = data.BalanceList.Select(i => (SparePartsModel)i.Clone()).ToList();

                var data2 = GetIncomeWriteOffBalance(income, writeoff, false);  //списание , поступление в периоде

                //
                uint k = 9;
                wsh.Cells["B3"].Value = "C " + start.ToString("dd.MM.yyyy") + " по " + end.ToString("dd.MM.yyyy");
                decimal totalStartbal = 0, totalInvoice = 0, totalConsumption = 0, totalWriteOff = 0, totalEndBal = 0; 
                //вывод
                for (int j = 0; j < dataEnd.BalanceList.Count; j++)
                {
                    float CurCount = 0; float? curSum = 0; decimal curOffSum = 0;
                    var curWriteoff = data2.WriteOffList.Where(t => t.SpareParts.Id == dataEnd.BalanceList[j].Spare_id
                    && t.Store_id == dataEnd.BalanceList[j].Store_id).FirstOrDefault();
                    var curIncome = data2.InvoiceList.Where(t => t.SpareParts.Id == dataEnd.BalanceList[j].Spare_id
                    && t.Store_id == dataEnd.BalanceList[j].Store_id).FirstOrDefault();

                    var curReserve = reserveList.Where(t => t.SpareParts.Id == dataEnd.BalanceList[j].Spare_id
                    && t.Store_id == dataEnd.BalanceList[j].Store_id).FirstOrDefault();

                     var startBalance = data.BalanceList.Where(t => t.Spare_id == dataEnd.BalanceList[j].Spare_id
                    && t.Store_id == dataEnd.BalanceList[j].Store_id).FirstOrDefault();

                    var curReserveStart = reserveStart.Where(t => t.SpareParts.Id == dataEnd.BalanceList[j].Spare_id
                    && t.Store_id == dataEnd.BalanceList[j].Store_id).FirstOrDefault();
                    var curReserveEnd = reserveEnd.Where(t => t.SpareParts.Id == dataEnd.BalanceList[j].Spare_id
                    && t.Store_id == dataEnd.BalanceList[j].Store_id).FirstOrDefault();

                    double? invoicePrice = 0; double? writeoffPrice = 0;
                    if (curIncome != null)
                    {
                        invoicePrice = (curIncome.Cost1 ?? 0) / curIncome.Count;
                    }

                    if (curWriteoff != null)
                    {
                        writeoffPrice = (curWriteoff.Cost1 ?? 0) / curWriteoff.Count;
                    }

                    var finalPrice = startBalance != null ? startBalance.Price : 0;
                    var startCount = (startBalance != null ? startBalance.Count : 0) - (curReserveStart != null ? (curReserveStart.Count ?? 0) : 0);
                    CurCount = curReserve != null ? (curReserve.Count ?? 0) : 0;
                    var countBalEnd = (dataEnd.BalanceList[j].Count) - (curReserveEnd != null ? (curReserveEnd.Count ?? 0) : 0);

                    //проверка если везде 0
                    if (startCount == 0 && curReserveStart == null && curIncome == null && curWriteoff == null 
                        && countBalEnd == 0 && curReserveEnd == null)
                    {
                        continue;
                    }

                    wsh.Cells["B" + k.ToString()].Value = dataEnd.BalanceList[j].SpareParts1;
                    wsh.Cells["c" + k.ToString()].Value = startCount;
                    wsh.Cells["d" + k.ToString()].Value = Math.Round((decimal)(finalPrice * startCount), 2);
                    totalStartbal = totalStartbal + Math.Round((decimal)(finalPrice * startCount), 2); 
                    //резерв начало
                    if (curReserveStart != null)
                    {
                        wsh.Cells["e" + k.ToString()].Value = curReserveStart.Count;
                        wsh.Cells["f" + k.ToString()].Value = Math.Round((decimal)(finalPrice * curReserveStart.Count), 2); 
                    }

                    //списание
                    if (curWriteoff != null)
                    {
                        curOffSum = Math.Round((decimal)(curWriteoff.Cost1 ?? 0), 2);
                        wsh.Cells["k" + k.ToString()].Value = curWriteoff.Count;
                        wsh.Cells["l" + k.ToString()].Value = curOffSum;
                        totalWriteOff = totalWriteOff + curOffSum;
                    }
                    //поступление
                    if (curIncome != null)
                    {
                        wsh.Cells["g" + k.ToString()].Value = curIncome.Count;
                        wsh.Cells["h" + k.ToString()].Value = Math.Round((decimal)(curIncome.Cost1 ?? 0), 2);
                        totalInvoice = totalInvoice + Math.Round((decimal)(curIncome.Cost1 ?? 0), 2);
                    }
                    //резерв
                    if (curReserve != null)
                    {
             //           CurCount = (curReserve.Count ?? 0);
                        curSum = (float)finalPrice * CurCount;   

                        wsh.Cells["i" + k.ToString()].Value = CurCount < 0 ? 0 : CurCount;
                        wsh.Cells["j" + k.ToString()].Value = CurCount < 0 ? 0 : Math.Round((decimal)(curSum), 2);  //sum
                    }

                    //расход со склада всего
                    var totalCount = (CurCount < 0 ? 0 : CurCount) + (curWriteoff != null ? curWriteoff.Count : 0);
                    if (totalCount != 0)
                    {
                        wsh.Cells["m" + k.ToString()].Value = totalCount;
                        wsh.Cells["n" + k.ToString()].Value = (decimal)(CurCount < 0 ? 0 : curSum) + curOffSum;  //sum
                        totalConsumption = totalConsumption + ((decimal)(CurCount < 0 ? 0 : curSum) + curOffSum);
                    }

                    //баланс на конец 
                    wsh.Cells["o" + k.ToString()].Value = countBalEnd;
                    wsh.Cells["p" + k.ToString()].Value = Math.Round((decimal)(dataEnd.BalanceList[j].Price * countBalEnd), 2);  
                    totalEndBal = totalEndBal + Math.Round((decimal)(dataEnd.BalanceList[j].Price * countBalEnd), 2);
                    //резерв на конец
                    if (curReserveEnd != null)
                    {
                        wsh.Cells["q" + k.ToString()].Value = curReserveEnd.Count;
                        wsh.Cells["r" + k.ToString()].Value = Math.Round((decimal)(dataEnd.BalanceList[j].Price * curReserveEnd.Count), 2); 
                    }
                    k++;
                }
                wsh.Cells["b" + k.ToString()].Value = "Итого";
                wsh.Cells["d" + k.ToString()].Value = totalStartbal;
                wsh.Cells["h" + k.ToString()].Value = totalInvoice;
                wsh.Cells["l" + k.ToString()].Value = totalWriteOff;
                wsh.Cells["n" + k.ToString()].Value = totalConsumption;
                wsh.Cells["p" + k.ToString()].Value = totalEndBal;
                wsh.Cells["b" + k.ToString() + ":p" + k.ToString()].Style.Font.Bold = true;
                if (k != 9)
                {
                    setBorder(wsh.Cells["b9:r" + (k - 1).ToString()].Style.Border);
                }
                var r = new ReportFile
                {
                    data = pck.GetAsByteArray(),
                    fileName = "Движение запчастей.xlsx"
                };
                return r;
            }
        }
        //нзп по месяцам
        public List<SevModelMonth> getNzpOrPlantsSpending2(int year, int checkNzp) 
        {
            var res = new List<SevModelMonth>();
            var list = (from ftp in db.Fields_to_plants 
                      where ftp.date_ingathering.Year == year && ftp.Fields.type == 1 && (checkNzp == 1 ? ftp.plant_plant_id == 36 : ftp.plant_plant_id != 36) 
                      && (checkNzp == 2 ? ftp.area_plant != 0 : true) 

                       let full_area = ftp.Fields.area == 0 ? 1 : ftp.Fields.area 
                       let dop_area = db.Fields_to_plants.Where(t => t.date_ingathering.Year == year && t.fiel_fiel_id == ftp.fiel_fiel_id).Sum(t => t.area_plant)

                      let total_salary = db.Tasks.Where(t => t.plant_plant_id == t.Fields_to_plants.fielplan_id
                      && t.Fields_to_plants.date_ingathering.Year == year && t.Fields_to_plants.fiel_fiel_id == ftp.fiel_fiel_id && t.Fields_to_plants.plant_plant_id == 36
                      && t.Sheet_tracks.date_begin.Year == year - 1 && t.Sheet_tracks.status != 1)
                     .Select(t => new SevooborotModel
                     {
                         id = t.tas_id,
                         total_salary = (float)(checkNzp == 2 ? ((t.total_salary ?? 0) * (ftp.area_plant / full_area) ) : t.total_salary ?? 0),
                         consumption = (float)(checkNzp == 2 ? ((t.fuel_cost ?? 0) * (ftp.area_plant / full_area)) : t.fuel_cost ?? 0),
                         consumption_fact = t.consumption_fact ?? 0, //топливо в литрах
                         month = t.Sheet_tracks.date_begin.Month,
                         type_material = null,
                         tptas_id = t.Type_tasks.tptas_id,
                         name_task = t.Type_tasks.name,
                     }).ToList()

                       let dop_salary = db.Salary_details.Where(t => t.plant_plant_id == t.Fields_to_plants.fielplan_id 
                       && t.Fields_to_plants.date_ingathering.Year == year && t.Fields_to_plants.fiel_fiel_id == ftp.fiel_fiel_id &&
                       t.Fields_to_plants.plant_plant_id == 36 && t.date.Value.Year == year - 1 )
                       .Select(t => new SevooborotModel
                        {
                            id = t.sald_id,
                            total_salary = (float)(checkNzp == 2 ? ((t.sum ?? 0) * (ftp.area_plant / full_area)) : t.sum ?? 0),
                            consumption = 0,
                            consumption_fact = 0,
                            month = t.date.Value.Month,
                            type_material = null,
                            tptas_id = t.Type_tasks.tptas_id,
                            name_task = t.Type_tasks.name,
                        }).ToList()

                      let tmc = db.TMC_Records.Where(t => t.fielplan_fielplan_id == t.Fields_to_plants.fielplan_id && t.emp_to_id == 0 && t.emp_from_id != 0
                      && t.Materials.mat_id == t.mat_mat_id  && t.TMC_Acts.act_date.Value.Year == year - 1
                      && t.Fields_to_plants.date_ingathering.Year == year && t.Fields_to_plants.fiel_fiel_id == ftp.fiel_fiel_id && t.Fields_to_plants.plant_plant_id == 36)
                     .Select(t => new SevooborotModel
                     {
                         id = t.tmr_id,
                         total_salary = (checkNzp == 2 ? ((t.count * t.price) * ((float)ftp.area_plant / (float)full_area)) : (t.count * t.price)),
                         consumption = 0,
                         consumption_fact = 0,
                         month = t.TMC_Acts.act_date.Value.Month,
                         type_material = t.Materials.mat_type_id,
                         tptas_id = 0,
                         name_task = null,
                     }).ToList()
            select new SevModelMonth
         {
            id = ftp.fielplan_id, 
            total_salary_list = total_salary,
            dop_salary_list = dop_salary,
            tmc_list = tmc,
            field_id = ftp.Fields.fiel_id,
            plant_id = ftp.Plants.plant_id, 
            area_plant = ftp.area_plant == null ? 0 : ftp.area_plant, 
            field_name = ftp.Fields.name, 
            plant_name = ftp.Plants.name,
            full_area = full_area,
            dop_area = dop_area, 
         }).ToList();
          return list; 
         }

        //НЗП отчет
        //public List<SevooborotModel> getNzpOrPlantsSpending(int year, int month, int checkNzp)
        //{
        //    var list = (from ftp in db.Fields_to_plants
        //                where ftp.date_ingathering.Year == year && ftp.Fields.type == 1 && (checkNzp == 1 ? ftp.plant_plant_id == 36 : ftp.plant_plant_id != 36)
        //               && (checkNzp == 2 ? ftp.area_plant != 0 : true)

        //                let full_area = ftp.Fields.area

        //                let dop_area = db.Fields_to_plants.Where(t => t.date_ingathering.Year == year && t.fiel_fiel_id == ftp.fiel_fiel_id).Sum(t => t.area_plant)

        //                let total_salary = (float?)db.Tasks.Where(t => t.plant_plant_id == t.Fields_to_plants.fielplan_id
        //                && t.Fields_to_plants.date_ingathering.Year == year && t.Sheet_tracks.date_begin.Year == year - 1
        //               && (month != 13 ? t.Sheet_tracks.date_begin.Month == month : true)
        //                && t.Fields_to_plants.fiel_fiel_id == ftp.fiel_fiel_id && t.Fields_to_plants.plant_plant_id == 36).Select(t => t.total_salary).Sum()

        //                let consumption = (float?)db.Tasks.Where(t => t.plant_plant_id == t.Fields_to_plants.fielplan_id
        //                && t.Fields_to_plants.date_ingathering.Year == year && t.Sheet_tracks.date_begin.Year == year - 1
        //               && (month != 13 ? t.Sheet_tracks.date_begin.Month == month : true)
        //                && t.Fields_to_plants.fiel_fiel_id == ftp.fiel_fiel_id && t.Fields_to_plants.plant_plant_id == 36).Select(t => t.fuel_cost).Sum()

        //                let dop_salary = (float?)db.Salary_details.Where(t => t.plant_plant_id == t.Fields_to_plants.fielplan_id
        //                && t.Fields_to_plants.date_ingathering.Year == year && t.date.Value.Year == year - 1
        //                && (month != 13 ? t.date.Value.Month == month : true)
        //                && t.Fields_to_plants.fiel_fiel_id == ftp.fiel_fiel_id && t.Fields_to_plants.plant_plant_id == 36).Select(t => t.sum).Sum()

        //                let szr = (float?)db.TMC_Records.Where(t => t.fielplan_fielplan_id == t.Fields_to_plants.fielplan_id && t.emp_to_id == 0 && t.emp_from_id != 0
        //                && t.Materials.mat_id == t.mat_mat_id && t.Materials.mat_type_id == 1 && t.TMC_Acts.act_date.Value.Year == year - 1
        //                && t.Fields_to_plants.date_ingathering.Year == year
        //                && (month != 13 ? t.TMC_Acts.act_date.Value.Month == month : true)
        //                && t.Fields_to_plants.fiel_fiel_id == ftp.fiel_fiel_id &&
        //                t.Fields_to_plants.plant_plant_id == 36).Select(t => t.count * t.price).Sum()

        //                let chemicalFertilizers = (float?)db.TMC_Records.Where(t => t.fielplan_fielplan_id == t.Fields_to_plants.fielplan_id && t.emp_to_id == 0 && t.emp_from_id != 0
        //                && t.Materials.mat_id == t.mat_mat_id && t.Materials.mat_type_id == 2 && t.TMC_Acts.act_date.Value.Year == year - 1
        //                && t.Fields_to_plants.date_ingathering.Year == year && t.Fields_to_plants.fiel_fiel_id == ftp.fiel_fiel_id
        //                && (month != 13 ? t.TMC_Acts.act_date.Value.Month == month : true)
        //                && t.Fields_to_plants.plant_plant_id == 36).Select(t => t.count * t.price).Sum()

        //                let seed = (float?)db.TMC_Records.Where(t => t.fielplan_fielplan_id == t.Fields_to_plants.fielplan_id && t.emp_to_id == 0 && t.emp_from_id != 0
        //                && t.Materials.mat_id == t.mat_mat_id && t.Materials.mat_type_id == 4 && t.TMC_Acts.act_date.Value.Year == year - 1
        //                && t.Fields_to_plants.date_ingathering.Year == year && t.Fields_to_plants.fiel_fiel_id == ftp.fiel_fiel_id
        //                && (month != 13 ? t.TMC_Acts.act_date.Value.Month == month : true)
        //                && t.Fields_to_plants.plant_plant_id == 36).Select(t => t.count * t.price).Sum()

        //                let dopmaterial = (float?)db.TMC_Records.Where(t => t.fielplan_fielplan_id == t.Fields_to_plants.fielplan_id && t.emp_to_id == 0 && t.emp_from_id != 0
        //                && t.Materials.mat_id == t.mat_mat_id && t.Materials.mat_type_id == 5 && t.TMC_Acts.act_date.Value.Year == year - 1
        //                && t.Fields_to_plants.date_ingathering.Year == year && t.Fields_to_plants.fiel_fiel_id == ftp.fiel_fiel_id
        //               && (month != 13 ? t.TMC_Acts.act_date.Value.Month == month : true)
        //                && t.Fields_to_plants.plant_plant_id == 36).Select(t => t.count * t.price).Sum()
        //                group new
        //                {
        //                    plant_id = ftp.plant_plant_id,
        //                    field_id = ftp.fiel_fiel_id,
        //                    total_salary = total_salary ?? 0,
        //                    consumption = consumption ?? 0,
        //                    dop_salary = dop_salary ?? 0,
        //                    szr = szr ?? 0,
        //                    full_area = (full_area == null || full_area == 0) ? 1 : full_area,
        //                    chemicalFertilizers = chemicalFertilizers ?? 0,
        //                    seed = seed ?? 0,
        //                    dopmaterial = dopmaterial ?? 0
        //                }
        //                    by new
        //                    {
        //                        id = ftp.fielplan_id,
        //                        field_id = ftp.fiel_fiel_id,
        //                        plant_id = ftp.plant_plant_id,
        //                        area_plant = ftp.area_plant == null ? 0 : ftp.area_plant,
        //                        name_field = ftp.Fields.name,
        //                        name_plant = ftp.Plants.name,
        //                        total_salary = total_salary ?? 0,
        //                        consumption = consumption ?? 0,
        //                        dop_salary = dop_salary ?? 0,
        //                        sum_area = (full_area == null || full_area == 0) ? 1 : full_area,
        //                        full_area1 = full_area == null ? 0 : full_area,
        //                        szr = szr ?? 0,
        //                        chemicalFertilizers = chemicalFertilizers ?? 0,
        //                        seed = seed ?? 0,
        //                        dopmaterial = dopmaterial ?? 0,
        //                        dop_area = dop_area,
        //                    } into g
        //                select new SevooborotModel
        //                {
        //                    id = g.Key.id,
        //                    consumption = checkNzp == 2 ? ((float?)g.Key.consumption * ((float)g.Key.area_plant / (float)g.Key.sum_area) ?? 0) : (float)(g.Key.consumption),
        //                    total_salary = checkNzp == 2 ? ((float?)g.Key.total_salary * ((float)g.Key.area_plant / (float)g.Key.sum_area) ?? 0) : (float)(g.Key.total_salary),
        //                    dop_salary = checkNzp == 2 ? ((float?)g.Key.dop_salary * ((float)g.Key.area_plant / (float)g.Key.sum_area) ?? 0) : (float)(g.Key.dop_salary),
        //                    szr = checkNzp == 2 ? ((float?)g.Key.szr * ((float)g.Key.area_plant / (float)g.Key.sum_area) ?? 0) : (float)(g.Key.szr),
        //                    chemicalFertilizers = checkNzp == 2 ? ((float?)g.Key.chemicalFertilizers * ((float)g.Key.area_plant / (float)g.Key.sum_area) ?? 0) : (float)(g.Key.chemicalFertilizers),
        //                    seed = checkNzp == 2 ? ((float?)g.Key.seed * ((float)g.Key.area_plant / (float)g.Key.sum_area) ?? 0) : (float)(g.Key.seed),
        //                    dopmaterial = checkNzp == 2 ? ((float?)g.Key.dopmaterial * ((float)g.Key.area_plant / (float)g.Key.sum_area) ?? 0) : (float)(g.Key.dopmaterial),
        //                    field_id = g.Key.field_id,
        //                    plant_id = g.Key.plant_id,
        //                    area_plant = g.Key.area_plant,
        //                    field_name = g.Key.name_field,
        //                    plant_name = g.Key.name_plant,
        //                    full_area = g.Key.full_area1,
        //                    dop_area = g.Key.dop_area,
        //                }).ToList();
        //    return list;
        //}

        public ExcelWorksheet Copy(ExcelWorkbook workbook, string existingWorksheetName, string newWorksheetName)
        {
            ExcelWorksheet worksheet = workbook.Worksheets.Copy(existingWorksheetName, newWorksheetName);
            return worksheet;
        }

        public List<SevooborotModel> getNzpFromModelMonth(List<SevModelMonth> list,  int n, bool? isExtend)
        {
            var listOut = new  List<SevooborotModel>();

            for (int i = 0; i < list.Count; i++)
            {
                var tasksList = new List<TasksSevModel>();
                if (isExtend == true)
                {
                    tasksList = (from f in list[i].total_salary_list.Where(t => n != 13 ? t.month == n : true)
                                     group f by new { f.tptas_id, f.name_task } into g
                                     select new TasksSevModel
                                     {
                                         tptas_id = g.Key.tptas_id,
                                         name_task = g.Key.name_task,
                                         total_salary = g.Sum(t => t.total_salary),
                                         consumption = g.Sum(t => t.consumption),
                                     }).ToList();
                    var dopList = (from f in list[i].dop_salary_list.Where(t => n != 13 ? t.month == n : true)
                                   group f by new { f.tptas_id, f.name_task } into g
                                   select new TasksSevModel
                                   {
                                       tptas_id = g.Key.tptas_id,
                                       name_task = g.Key.name_task,
                                       total_salary = g.Sum(t => t.total_salary),
                                   }).ToList();
                    for (int j = 0; j < dopList.Count; j++)
                    {
                        var id = dopList[j].tptas_id;
                        var dic = tasksList.Where(t => t.tptas_id == id).FirstOrDefault();
                        if (dic != null)
                        {
                            tasksList[j].total_salary = tasksList[j].total_salary + dopList[j].total_salary;
                        }
                        else
                        {
                            tasksList.Add(new TasksSevModel
                            {
                                tptas_id = dopList[j].tptas_id,
                                name_task = dopList[j].name_task,
                                total_salary = dopList[j].total_salary,

                            });
                        }
                    }
                }
                listOut.Add(new SevooborotModel
                {
                    id = list[i].id,
                    total_salary = list[i].total_salary_list.Where(t => n != 13 ? t.month == n : true).Sum(t => t.total_salary),
                    consumption = list[i].total_salary_list.Where(t => n != 13 ? t.month == n : true).Sum(t => t.consumption),
                    dop_salary = list[i].dop_salary_list.Where(t => n != 13 ? t.month == n : true).Sum(t => t.total_salary),
                    seed = list[i].tmc_list.Where(t => (n != 13 ? t.month == n : true) && t.type_material == 4).Sum(t => t.total_salary),
                    szr = list[i].tmc_list.Where(t => (n != 13 ? t.month == n : true) && t.type_material == 1).Sum(t => (t.total_salary ?? 0)),
                    chemicalFertilizers = list[i].tmc_list.Where(t => (n != 13 ? t.month == n : true) && t.type_material == 2).Sum(t => t.total_salary),
                    dopmaterial = list[i].tmc_list.Where(t => (n != 13 ? t.month == n : true) && t.type_material == 5).Sum(t => t.total_salary),
                    field_id = list[i].field_id,
                    plant_id = list[i].plant_id,
                    area_plant = list[i].area_plant,
                    field_name = list[i].field_name,
                    full_area = list[i].full_area,
                    dop_area = list[i].dop_area,
                    plant_name = list[i].plant_name,
                    tasksList = tasksList,
                });
            }

            return listOut;
        }

        public ReportFile GetNZPSimpleExcel(string pathTemplate, int year)
        {  
            var tmpl = new FileInfo(pathTemplate);
            var data = getNzpOrPlantsSpending2(year, 1).OrderBy(t => t.field_id).ToList();

            using (ExcelPackage pck = new ExcelPackage(tmpl, true))
            {
                ExcelWorksheet wsh = pck.Workbook.Worksheets[1];
                wsh.Name = "НЗП";
                uint k = 4;
                wsh.Cells["b3"].Value = "Топливо, л";
                wsh.Cells["c3"].Value = "Сдельная зарплата";
                if (data.Count != 0)
                {
                    var dates = (from p in db.Sheet_tracks
                                 join c in db.Tasks on p.shtr_id equals c.shtr_shtr_id
                                 where p.date_begin.Year == year - 1 && c.plant_plant_id == 36
                                 select new {
                                     Date = p.date_begin
                                 });

                    var acts = (from a in db.TMC_Acts
                                join rec in db.TMC_Records on a.tma_id equals rec.tma_tma_id
                                join f in db.Fields_to_plants on rec.fielplan_fielplan_id equals f.fielplan_id
                                where a.act_date.Value.Year == year - 1 && f.plant_plant_id == 36
                                select new
                                {
                                    Date = a.act_date.Value
                                });

                    wsh.Cells["a1"].Value = "Путевые листы: c " + dates.Min(t=>t.Date).ToString("dd.MM.yyyy") + " по " + dates.Max(t => t.Date).ToString("dd.MM.yyyy");
                    wsh.Cells["a2"].Value = "Акты списания ТМЦ: c " + acts.Min(t => t.Date).ToString("dd.MM.yyyy") + " по " + acts.Max(t => t.Date).ToString("dd.MM.yyyy");
                }
                for (int i = 0; i < data.Count; i++)
                {
                    var salary = Math.Round((double)data[i].total_salary_list.Sum(t => t.total_salary ?? 0), 2);
                    var consump_litr = Math.Round((double)data[i].total_salary_list.Sum(t => t.consumption_fact ?? 0), 2);
                    var dop_salary = Math.Round((double)data[i].dop_salary_list.Sum(t => t.total_salary ?? 0), 2);
                    var consumption = Math.Round((double)data[i].total_salary_list.Sum(t => t.consumption), 2); ;
                    var szr = Math.Round((double)data[i].tmc_list.Where(t => t.type_material == 1).Sum(t => t.total_salary), 2); 
                    var material = Math.Round((double)data[i].tmc_list.Where(t => t.type_material == 2).Sum(t => t.total_salary), 2); 
                    var seed = Math.Round((double)data[i].tmc_list.Where(t => t.type_material == 4).Sum(t => t.total_salary), 2);

                    if (salary == 0 && consumption == 0 && szr == 0 && material == 0 && seed == 0)
                    {
                        continue;
                    }

                    wsh.Cells["A" + k].Value = data[i].field_name;
                    wsh.Cells["b" + k].Value = consump_litr;
                    wsh.Cells["c" + k].Value = dop_salary;
                    wsh.Cells["D" + k].Value = salary;
                    wsh.Cells["E" + k].Value = consumption;
                    wsh.Cells["f" + k].Value = szr;
                    wsh.Cells["g" + k].Value = material;
                    wsh.Cells["h" + k].Value = seed;
                    wsh.Cells["i" + k].Value = Math.Round((double)(salary + consumption + szr + material + seed) , 2);

                    k++;
                }
                var r = new ReportFile
                {
                    data = pck.GetAsByteArray(),
                    fileName = "НЗП.xlsx"
                };
                return r;
            }

        }

            //НЗП отчет
            public ReportFile GetNZPListResExcel(string pathTemplate, int year, bool? extend)
        {
            var tmpl = new FileInfo(pathTemplate);
            using (ExcelPackage pck = new ExcelPackage(tmpl, true))
            {
                ExcelWorksheet wsh = pck.Workbook.Worksheets[1];
                wsh.Name = "НЗП";
                var countSheet = 1;
                var plantNZP = new List<SevooborotModel>();
                var totalNZP = new List<SevooborotModel>();

               var plantNZP2 = getNzpOrPlantsSpending2(year, 2).OrderBy(t => t.plant_name).ToList();
               var totalNZP2 = getNzpOrPlantsSpending2(year, 1).OrderBy(t => t.field_id).ToList();

             //  var df = plantNZP2.Where(t => t.field_name.Equals("№039 Т")).ToList();
             //  var df1 = totalNZP2.Where(t => t.field_name.Equals("№039 Т")).ToList();
               for (int n = 1; n <= 13; n++)
              {
                        plantNZP.Clear();
                        plantNZP = getNzpFromModelMonth(plantNZP2, n, extend);
                        totalNZP.Clear();
                        totalNZP = getNzpFromModelMonth(totalNZP2, n, extend);

                    //total1

                    decimal t_salary = totalNZP.Sum(t => (decimal)(t.dop_salary ?? 0)) + totalNZP.Sum(t => (decimal)(t.total_salary ?? 0));
                    decimal t_consumption = totalNZP.Sum(t => (decimal)(t.consumption ?? 0));
                    decimal t_szr = totalNZP.Sum(t => (decimal)(t.szr ?? 0));
                    decimal t_chemicalFertilizers = totalNZP.Sum(t => (decimal)(t.chemicalFertilizers ?? 0));
                    decimal t_seed = totalNZP.Sum(t => (decimal)(t.seed ?? 0));
                    decimal total_sum = t_salary + t_consumption + t_szr + t_chemicalFertilizers + t_seed;
                    if (total_sum == 0) { continue; }

                    if (n != 13)
                    {
                        Copy(pck.Workbook, "НЗП", new DateTime(2015, n, 1).ToString("MMMM", CultureInfo.CreateSpecificCulture("ru")) + " " + (year - 1).ToString());
                        countSheet++;
                        wsh = pck.Workbook.Worksheets[countSheet];
                    }
                    else
                    {
                        wsh = pck.Workbook.Worksheets[1];
                    }

                    wsh.Cells["A1"].Value = "Затраты на НЗП на " + year + " год";
                    wsh.Cells["A4"].Value = "Урожай " + year;
                    wsh.Cells["a4:i4"].Style.Font.Bold = true;
                    setColor(wsh, "a4:i4", "#95b3d7");
                    //
                    uint k = 5;
                    //вычитаем нзп-факт
                    for (int i = 0; i < totalNZP.Count; i++)
                    {
                        for (int j = 0; j < plantNZP.Count; j++)
                        {
                            if (totalNZP[i].field_id == plantNZP[j].field_id)
                            {
                                totalNZP[i].dop_salary = (float)(Math.Round((decimal)(totalNZP[i].dop_salary ?? 0), 2) - Math.Round((decimal)(plantNZP[j].dop_salary ?? 0), 2));
                                totalNZP[i].total_salary = (float)(Math.Round((decimal)(totalNZP[i].total_salary ?? 0), 2) - Math.Round((decimal)(plantNZP[j].total_salary ?? 0), 2));
                                totalNZP[i].consumption = (float)(Math.Round((decimal)(totalNZP[i].consumption ?? 0), 2) - Math.Round((decimal)(plantNZP[j].consumption ?? 0), 2));
                                totalNZP[i].szr = (float)(Math.Round((decimal)(totalNZP[i].szr ?? 0), 2) - Math.Round((decimal)(plantNZP[j].szr ?? 0), 2));
                                totalNZP[i].chemicalFertilizers = (float)(Math.Round((decimal)(totalNZP[i].chemicalFertilizers ?? 0), 2) - Math.Round((decimal)(plantNZP[j].chemicalFertilizers ?? 0), 2));
                                totalNZP[i].seed = (float)(Math.Round((decimal)(totalNZP[i].seed ?? 0), 2) - Math.Round((decimal)(plantNZP[j].seed ?? 0), 2));
                                if (extend == true)
                                {
                                    //расширенный
                                    for (int l = 0; l < totalNZP[i].tasksList.Count; l++)
                                    {
                                        for (int p = 0; p < plantNZP[j].tasksList.Count; p++)
                                        {
                                            if (totalNZP[i].tasksList[l].tptas_id == plantNZP[j].tasksList[p].tptas_id)
                                            {

                                                totalNZP[i].tasksList[l].dop_salary = (float)(Math.Round((decimal)(totalNZP[i].tasksList[l].dop_salary ?? 0), 2) - Math.Round((decimal)(plantNZP[j].tasksList[p].dop_salary ?? 0), 2));
                                                totalNZP[i].tasksList[l].total_salary = (float)(Math.Round((decimal)(totalNZP[i].tasksList[l].total_salary ?? 0), 2) - Math.Round((decimal)(plantNZP[j].tasksList[p].total_salary ?? 0), 2));
                                                totalNZP[i].tasksList[l].consumption = (float)(Math.Round((decimal)(totalNZP[i].tasksList[l].consumption ?? 0), 2) - Math.Round((decimal)(plantNZP[j].tasksList[p].consumption ?? 0), 2));
                                                totalNZP[i].tasksList[l].szr = (float)(Math.Round((decimal)(totalNZP[i].tasksList[l].szr ?? 0), 2) - Math.Round((decimal)(plantNZP[j].tasksList[p].szr ?? 0), 2));
                                                totalNZP[i].tasksList[l].chemicalFertilizers = (float)(Math.Round((decimal)(totalNZP[i].tasksList[l].chemicalFertilizers ?? 0), 2) - Math.Round((decimal)(plantNZP[j].tasksList[p].chemicalFertilizers ?? 0), 2));
                                                totalNZP[i].tasksList[l].seed = (float)(Math.Round((decimal)(totalNZP[i].tasksList[l].seed ?? 0), 2) - Math.Round((decimal)(plantNZP[j].tasksList[p].seed ?? 0), 2));

                                            }
                                        }
                                    }
                                }
                                //

                            }
                        }
                        if (totalNZP[i].dop_salary == 0 && totalNZP[i].total_salary == 0 &&
                            totalNZP[i].consumption == 0 && totalNZP[i].szr == 0 && totalNZP[i].chemicalFertilizers == 0 && totalNZP[i].seed == 0)
                        {
                            totalNZP[i].id = -1;
                        }
                    }
                    //total урожай
                    decimal t_salary1 = totalNZP.Sum(t => (decimal)(t.dop_salary ?? 0)) + totalNZP.Sum(t => (decimal)(t.total_salary ?? 0));
                    wsh.Cells["d4"].Value = t_salary1;
                    decimal t_consumption1 = totalNZP.Sum(t => (decimal)(t.consumption ?? 0));
                    wsh.Cells["e4"].Value = t_consumption1;
                    decimal t_szr1 = totalNZP.Sum(t => (decimal)(t.szr ?? 0));
                    wsh.Cells["f4"].Value = t_szr1;
                    decimal t_chemicalFertilizers1 = totalNZP.Sum(t => (decimal)(t.chemicalFertilizers ?? 0));
                    wsh.Cells["g4"].Value = t_chemicalFertilizers1;
                    decimal t_seed1 = totalNZP.Sum(t => (decimal)(t.seed ?? 0));
                    wsh.Cells["h4"].Value = t_seed1;
                    decimal t_full_area1 = totalNZP.Where(t => t.id != -1).Sum(t => t.full_area);
                    wsh.Cells["b4"].Value = t_full_area1;
                    decimal t_area_plant1 = totalNZP.Where(t => t.id != -1).Sum(t => (decimal)(t.dop_area ?? 0));
                    wsh.Cells["c4"].Value = t_area_plant1;
                    wsh.Cells["i4"].Value = t_salary1 + t_consumption1 + t_szr1 + t_chemicalFertilizers1 + t_seed1;
                    //
                    for (int i = 0; i < totalNZP.Count; i++)
                    {
                        if (totalNZP[i].id != -1)
                        {
                            decimal salary = (decimal)(totalNZP[i].dop_salary ?? 0) + (decimal)(totalNZP[i].total_salary ?? 0);
                            decimal consumption = (decimal)(totalNZP[i].consumption ?? 0);
                            decimal szr = (decimal)(totalNZP[i].szr ?? 0);
                            decimal chemicalFertilizers = (decimal)(totalNZP[i].chemicalFertilizers ?? 0);
                            decimal seed = (decimal)(totalNZP[i].seed ?? 0);
                            wsh.Cells["A" + k.ToString()].Value = totalNZP[i].field_name;
                            wsh.Cells["b" + k.ToString()].Value = totalNZP[i].full_area;  //площадь поля
                            wsh.Cells["c" + k.ToString()].Value = totalNZP[i].dop_area;
                            wsh.Cells["d" + k.ToString()].Value = salary;
                            wsh.Cells["e" + k.ToString()].Value = consumption;
                            wsh.Cells["f" + k.ToString()].Value = szr;
                            wsh.Cells["g" + k.ToString()].Value = chemicalFertilizers;
                            wsh.Cells["h" + k.ToString()].Value = seed;
                            wsh.Cells["i" + k.ToString()].Value = salary + consumption + szr + chemicalFertilizers + seed;
                            setColor(wsh, "a" + k.ToString() + ":i" + k.ToString(), "#dce6f1");
                            k++;
                            if (extend == true)
                            {
                                //расширенный
                                for (int l = 0; l < totalNZP[i].tasksList.Count; l++)
                                {
                                    wsh.Cells["A" + k.ToString()].Value = totalNZP[i].tasksList[l].name_task;
                                    wsh.Cells["E" + k.ToString()].Value = totalNZP[i].tasksList[l].consumption;
                                    wsh.Cells["D" + k.ToString()].Value = totalNZP[i].tasksList[l].total_salary;
                                    k++;
                                }
                            }


                        }
                    }
                    decimal? total_dop_area = 0;
                    for (int i = 0; i < plantNZP.Count; i++)
                    {
                        if (plantNZP[i].id != -1 && (plantNZP[i].consumption != 0 || plantNZP[i].total_salary != 0 || plantNZP[i].dop_salary != 0 ||
                         plantNZP[i].chemicalFertilizers != 0 || plantNZP[i].szr != 0 || plantNZP[i].seed != 0))
                        {
                            wsh.Cells["A" + k.ToString()].Value = plantNZP[i].plant_name;
                            wsh.Cells["A" + k.ToString()].Style.Font.Bold = true;

                            var fieldList = plantNZP.Where(t => t.plant_id == plantNZP[i].plant_id && t.id != -1).ToList();
                            //total plants
                            decimal t_salary2 = fieldList.Sum(t => (decimal)(t.dop_salary ?? 0)) + fieldList.Sum(t => (decimal)(t.total_salary ?? 0));
                            wsh.Cells["d" + k.ToString()].Value = t_salary2;
                            decimal t_consumption2 = fieldList.Sum(t => (decimal)(t.consumption ?? 0));
                            wsh.Cells["e" + k.ToString()].Value = t_consumption2;
                            decimal t_szr2 = fieldList.Sum(t => (decimal)(t.szr ?? 0));
                            wsh.Cells["f" + k.ToString()].Value = t_szr2;
                            decimal t_chemicalFertilizers2 = fieldList.Sum(t => (decimal)(t.chemicalFertilizers ?? 0));
                            wsh.Cells["g" + k.ToString()].Value = t_chemicalFertilizers2;
                            decimal t_seed2 = fieldList.Sum(t => (decimal)(t.seed ?? 0));
                            wsh.Cells["h" + k.ToString()].Value = t_seed2;
                            wsh.Cells["b" + k.ToString()].Value = fieldList.Where(t => t.consumption != 0 || t.total_salary != 0 ||
                                t.dop_salary != 0 || t.chemicalFertilizers != 0 || t.szr != 0 || t.seed != 0).Sum(t => t.full_area);

                            var t_dop_area2 = fieldList.Where(t => t.consumption != 0 || t.total_salary != 0 ||
                               t.dop_salary != 0 || t.chemicalFertilizers != 0 || t.szr != 0 || t.seed != 0).Sum(t => t.area_plant);
                            total_dop_area = total_dop_area + t_dop_area2;
                            wsh.Cells["c" + k.ToString()].Value = t_dop_area2;

                            wsh.Cells["i" + k.ToString()].Value = t_salary2 + t_consumption2 + t_szr2 + t_chemicalFertilizers2 + t_seed2;
                            //
                            wsh.Cells["a" + k.ToString() + ":i" + k.ToString()].Style.Font.Bold = true;
                            setColor(wsh, "a" + k.ToString() + ":i" + k.ToString(), "#95b3d7");
                            k++;
                            for (int j = 0; j < fieldList.Count; j++)
                            {
                                if (fieldList[j].consumption != 0 || fieldList[j].total_salary != 0 || fieldList[j].dop_salary != 0 ||
                                 fieldList[j].chemicalFertilizers != 0 || fieldList[j].szr != 0 || fieldList[j].seed != 0)
                                {
                                    var upd = plantNZP.Where(t => t.plant_id == plantNZP[i].plant_id && t.field_id == fieldList[j].field_id).FirstOrDefault();
                                    upd.id = -1;
                                    wsh.Cells["A" + k.ToString()].Value = fieldList[j].field_name;
                                    wsh.Cells["b" + k.ToString()].Value = fieldList[j].full_area;
                                    wsh.Cells["c" + k.ToString()].Value = fieldList[j].area_plant;
                                    decimal salary2 = (decimal)(fieldList[j].dop_salary ?? 0) + (decimal)(fieldList[j].total_salary ?? 0);
                                    decimal consumption2 = (decimal)(fieldList[j].consumption ?? 0);
                                    decimal szr2 = (decimal)(fieldList[j].szr ?? 0);
                                    decimal chemicalFertilizers2 = (decimal)(fieldList[j].chemicalFertilizers ?? 0);
                                    decimal seed2 = (decimal)(fieldList[j].seed ?? 0);
                                    wsh.Cells["d" + k.ToString()].Value = salary2;
                                    wsh.Cells["e" + k.ToString()].Value = consumption2;
                                    wsh.Cells["f" + k.ToString()].Value = szr2;
                                    wsh.Cells["g" + k.ToString()].Value = chemicalFertilizers2;
                                    wsh.Cells["h" + k.ToString()].Value = seed2;
                                    wsh.Cells["i" + k.ToString()].Value = salary2 + consumption2 + szr2 + chemicalFertilizers2 + seed2;
                                    setColor(wsh, "a" + k.ToString() + ":i" + k.ToString(), "#dce6f1");
                                    k++;
                                    if (extend == true)
                                    {
                                        //расширенный культура-поле
                                        for (int l = 0; l < fieldList[j].tasksList.Count; l++)
                                        {
                                            wsh.Cells["A" + k.ToString()].Value = fieldList[j].tasksList[l].name_task;
                                            wsh.Cells["E" + k.ToString()].Value = fieldList[j].tasksList[l].consumption;
                                            wsh.Cells["D" + k.ToString()].Value = fieldList[j].tasksList[l].total_salary;
                                            k++;
                                        }
                                    }
                                }
                            }
                        }
                    }
                    wsh.Cells["A" + k.ToString()].Value = "Общий итог";
                    wsh.Cells["A" + k.ToString()].Style.Font.Bold = true;
                    //
                    wsh.Cells["c" + k.ToString()].Value = total_dop_area;
                    wsh.Cells["d" + k.ToString()].Value = t_salary;
                    wsh.Cells["e" + k.ToString()].Value = t_consumption;
                    wsh.Cells["f" + k.ToString()].Value = t_szr;
                    wsh.Cells["g" + k.ToString()].Value = t_chemicalFertilizers;
                    wsh.Cells["h" + k.ToString()].Value = t_seed;
                    wsh.Cells["i" + k.ToString()].Value = total_sum;
                    //
                    wsh.Cells["a" + k.ToString() + ":i" + k.ToString()].Style.Font.Bold = true;
                    setColor(wsh, "a" + k.ToString() + ":i" + k.ToString(), "#95b3d7");

                    if (totalNZP.Count != 0 || plantNZP.Count != 0)
                    {
                        setBorder(wsh.Cells["A4:i" + (k).ToString()].Style.Border);
                        wsh.Cells["b4:i" + (k).ToString()].Style.Numberformat.Format = "#,##0.00";
                    }
               }

                var r = new ReportFile
                {
                    data = pck.GetAsByteArray(),
                    fileName = "НЗП.xlsx"
                };
                return r;
            }
        }
     
        //табель рабочего времени 
        public ReportFile GetDailyEmployeesWorkTime(string pathTemplate, DateTime startDate, int? position, DateTime endDate)
        {

            var tmpl = new FileInfo(pathTemplate);
            var  endDate2 = endDate.AddDays(1);
            using (ExcelPackage pck = new ExcelPackage(tmpl, true))
            {
                ExcelWorksheet wsh = pck.Workbook.Worksheets[1];
                wsh.Name = "Табель рабочего времени";
                wsh.Cells["b1"].Value = startDate.ToString("dd.MM.yyyy");
                wsh.Cells["b2"].Value = endDate.ToString("dd.MM.yyyy");
                var list = new List<GrpTaskForTimeSheet>();
                var list2 = new List<GrpTaskForTimeSheet>();
                //из ЕН
                    list = (from t in db.Daily_acts
                            where t.date1 != null && t.date1 >= startDate && t.date1 <= endDate2 

                                join k in db.Repairs_Tasks_Spendings on t.dai_id equals k.dai_dai_id into taskLeft
                                from k in taskLeft.DefaultIfEmpty()
                                where k.emp_emp_id != null && k.emp_emp_id != 0 && k.hours != null

                                join e in db.Employees on k.emp_emp_id equals e.emp_id into taskLeft2
                                from k1 in taskLeft2.DefaultIfEmpty()

                                group new
                                {
                                    worktime = k.hours ?? 0,
                                    date = DbFunctions.TruncateTime(k.date1),
                                } by new
                                {
                                    k.emp_emp_id,
                                    k1.short_name,
                                    k.date1,
                                    t.status,
                                } into g
                                select new GrpTaskForTimeSheet
                                {
                                    Employees = g.Key.short_name,
                                    date = DbFunctions.TruncateTime(g.Key.date1),
                                    worktime = g.Sum(t => t.worktime),
                                    emp_emp_id = g.Key.emp_emp_id,
                                    status = g.Key.status,
                                    type_emp_id = 2,
                                }).ToList();
                //из путевых листов
                if (position != null)
                {
                    switch (position)
                    {
                        case 1:
                            wsh.Cells["t2"].Value = "Должность: механизаторы";
                            break;
                        case 2:
                            wsh.Cells["t2"].Value = "Должность: водители";
                            break;
                        case 3:
                            wsh.Cells["t2"].Value = "Должность: механизаторы,водители";
                            break;
                    }

                    list2 = (from shtr in db.Sheet_tracks
                             where shtr.date_begin != null && shtr.date_begin >= startDate &&
                             shtr.date_begin <= endDate2

                            join task in db.Tasks on shtr.shtr_id equals task.shtr_shtr_id into taskLeft
                            from t in taskLeft.DefaultIfEmpty()

                            join e in db.Employees on shtr.emp_emp_id equals e.emp_id into taskLeft2
                            from k1 in taskLeft2.DefaultIfEmpty()
                             where position == 1 ? k1.pst_pst_id == 1 : (position == 2 ? k1.pst_pst_id == 5 : (k1.pst_pst_id == 1 || k1.pst_pst_id == 5)) 
                            group new
                            {
                                worktime = t.worktime ?? 0,
                                date = shtr.date_begin,
                                status = shtr.status,

                            } by new
                            {
                                shtr.emp_emp_id,
                                shtr.Employees.short_name,
                                shtr.date_begin,
                                shtr.status,
                            }
                                into g
                                select new GrpTaskForTimeSheet
                                {
                                    Employees = g.Key.short_name,
                                    emp_emp_id = g.Key.emp_emp_id,
                                    date = g.Key.date_begin,
                                    status = g.Key.status,
                                    worktime = g.Sum(t => t.worktime),
                                    type_emp_id = 1,
                                }).ToList();
                    list.AddRange(list2);
                }

                list = list.OrderBy(t => t.Employees).ToList();
                var ls = new List<string>();
                char h = 'C'; var h1 = 'A';
                for (int j = 0; j < 31; j++)
                {
                    if (j < 24)
                    {
                        ls.Add(h.ToString());
                        h++;
                    }
                    else
                    {
                        if (j == 24) { h = 'A'; }
                        ls.Add(h.ToString() + h1.ToString());
                        h1++;
                    }
                }

              uint d = 7;
              var  list_emp = list.Select(t => t.emp_emp_id).Distinct().ToList();
              for (int i = 0; i < list_emp.Count; i++)
                {
                    wsh.Cells["A" + d.ToString()].Value = i + 1;
                    wsh.Cells["B" + d.ToString()].Value = list.Where(t=>t.emp_emp_id == list_emp[i]).Select(t=>t.Employees).FirstOrDefault();
                    for (int j = 0; j < 31; j++)
                    {
                        var sum = list.Where(t => t.emp_emp_id == list_emp[i] && t.date.Value.Day == j + 1).Sum(t => t.worktime);
                        var checkStatus = list.Where(t => t.emp_emp_id == list_emp[i] && t.date.Value.Day == j + 1 && t.status == 1).Count();
                        if (sum != 0 && position == null)
                        {
                            if (checkStatus != 0 && position == null)
                            {
                                setColor(wsh, ls[j] + d.ToString(), "#ef0e30"); 
                            }
                            if (checkStatus != 0 && position != null)
                            { 
                            
                            }
                            wsh.Cells[ls[j] + d.ToString()].Value = sum;
                        }
                        // c путевыми
                        if (position != null) {
                          var checkDay = list.Where(t => t.emp_emp_id == list_emp[i] && t.date.Value.Day == j + 1).Count();
                          var sheetStatus = list.Where(t => t.emp_emp_id == list_emp[i] && t.date.Value.Day == j + 1 && t.status == 1 && t.type_emp_id == 1).Count();
                          if (checkDay != 0 && sheetStatus != 0) { setColor(wsh, ls[j] + d.ToString(), "#ef0e30"); }
                          if (checkDay != 0) { wsh.Cells[ls[j] + d.ToString()].Value = sum; }
                        }

                    }
                    wsh.Cells["AH" + d.ToString()].Value = list.Where(t => t.emp_emp_id == list_emp[i]).Sum(t => t.worktime);
                    d++;
                }

                if (list.Count != 0)
                {
                    setBorder(wsh.Cells["A7:AH" + (d - 1).ToString()].Style.Border);
                }
                    var r = new ReportFile
                    {
                        data = pck.GetAsByteArray(),
                        fileName = "Табель рабочего времени.xlsx"
                    };
                    return r;
                }
            }
       
        //свод затрат
        public ReportFile RepairTechEqiupExcel(string pathTemplate, DateTime start, DateTime end, int tId, int eqId)
        {
            end = end.AddDays(1);
            var tmpl = new FileInfo(pathTemplate);
            using (ExcelPackage pck = new ExcelPackage(tmpl, true))
            {
                ExcelWorksheet wsh = pck.Workbook.Worksheets[1];
                wsh.Name = "Свод затрат";

                var list1 = (from s in db.Repairs_acts
                             where s.status == 3 && s.date_end >= start && s.date_end <= end &&
                            ((tId != 0 && tId != -1) ? s.trakt_trakt_id == tId : (tId == 0 ? s.trakt_trakt_id == null : true)) &&
                            ((eqId != 0 && eqId != -1) ? s.equi_equi_id == eqId : (eqId == 0 ? s.equi_equi_id == null : true))
                             join t in db.Daily_acts on s.rep_id equals t.rep_id into leftTask
                             from t1 in leftTask.DefaultIfEmpty()
                             where t1.status == 2
                             join d in db.Repairs_Tasks_Spendings on t1.dai_id equals d.dai_dai_id into leftTask2
                             from d1 in leftTask2.DefaultIfEmpty()
                             let techOrEquip = s.trakt_trakt_id != null ? 1 : 2
                             group new
                             {
                              sumCost = (d1.rate_piecework ?? 0) * (d1.hours ?? 0) + (d1.rate_shift ?? 0), //зп
                             }
                            by new
                            {
                                IdTech = s.trakt_trakt_id != null ? s.trakt_trakt_id : s.equi_equi_id,
                                techOrEquip = techOrEquip,
                                Rep_id =  s.rep_id,
                            } into v
                             select new RepairTechEqiupModel
                             {
                                 Id = v.Key.IdTech,
                                 SumCost = v.Sum(l => l.sumCost), //зп из ен
                                 techOrEquip = v.Key.techOrEquip,
                                Rep_Id = v.Key.Rep_id,
                             });

                //cost spare parts
                var list2 = (from s in db.Repairs_acts
                             where s.status == 3 && s.date_end >= start && s.date_end <= end &&
                            ((tId != 0 && tId != -1) ? s.trakt_trakt_id == tId : (tId == 0 ? s.trakt_trakt_id == null : true)) &&
                          ((eqId != 0 && eqId != -1) ? s.equi_equi_id == eqId : (eqId == 0 ? s.equi_equi_id == null : true))
                             join l in db.Spare_parts_Records on s.rep_id equals l.spss_spss_id into leftTask3
                             from l1 in leftTask3.DefaultIfEmpty()
                             where l1.store_from_id != 0 && l1.store_to_id == 0
                             let techOrEquip = s.trakt_trakt_id != null ? 1 : 2
                             group new
                             {
                                 tech = s.trakt_trakt_id != null ? s.trakt_trakt_id : s.equi_equi_id,
                                 SumSpare = l1.count * l1.price,
                             }
                            by new
                            {
                                IdTech  = s.trakt_trakt_id != null ? s.trakt_trakt_id : s.equi_equi_id,
                                techOrEquip = techOrEquip
                            } into v
                             select new RepairTechEqiupModel
                             {
                                 Id = v.Key.IdTech,
                                 SumSpare = v.Sum(l => l.SumSpare),   // запчасти
                                 techOrEquip = v.Key.techOrEquip  
                             });
                //service
                var list3 = (from s in db.Repairs_acts
                             where s.status == 3 && s.date_end >= start && s.date_end <= end &&
                            ((tId != 0 && tId != -1) ? s.trakt_trakt_id == tId : (tId == 0 ? s.trakt_trakt_id == null : true)) &&
                           ((eqId != 0 && eqId != -1) ? s.equi_equi_id == eqId : (eqId == 0 ? s.equi_equi_id == null : true))
                             join q in db.Repairs_Service on s.rep_id equals q.rep_rep_id into leftTask4
                             from q1 in leftTask4.DefaultIfEmpty()
                             let techOrEquip = s.trakt_trakt_id != null ? 1 : 2
                             group new
                             {
                            IdTech = s.trakt_trakt_id != null ? s.trakt_trakt_id : s.equi_equi_id,
                            serviceSum = ((q1.count ?? 0) * (q1.price ?? 0)) + (((q1.nds ?? 0) / 100) * (q1.count ?? 0) * (q1.price ?? 0)), //услуги
                             }
                            by new
                            {
                                IdTech = s.trakt_trakt_id != null ? s.trakt_trakt_id : s.equi_equi_id,
                                techOrEquip = techOrEquip,
                            } into v
                             select new RepairTechEqiupModel
                             {
                                 Id = v.Key.IdTech,
                                 ServiceSum = v.Sum(l => l.serviceSum),
                                 techOrEquip = v.Key.techOrEquip,
                             });
                //все ключи
                var nlist = (from s in db.Repairs_acts
                            where s.status == 3 && s.date_end >= start &&  s.date_end <= end &&
                            ((tId != 0 && tId != -1) ? s.trakt_trakt_id == tId : (tId == 0 ? s.trakt_trakt_id == null : true)) &&
                            ((eqId != 0 && eqId != -1) ? s.equi_equi_id == eqId : (eqId == 0 ? s.equi_equi_id == null : true)) 
                             let EquipTechName = s.equi_equi_id != null ?
                             db.Equipments.Where(c => c.equi_id == s.equi_equi_id).Select(c => c.name).FirstOrDefault() :
                             db.Traktors.Where(v => v.trakt_id == s.trakt_trakt_id).Select(v => v.name).FirstOrDefault() + ", "+
                             db.Traktors.Where(v => v.trakt_id == s.trakt_trakt_id).Select(v => v.reg_num).FirstOrDefault()
                             group new
                             {
                                 IdTech = s.trakt_trakt_id != null ? s.trakt_trakt_id : s.equi_equi_id,
                             }
                           by new
                           {
                               IdTech = s.trakt_trakt_id != null ? s.trakt_trakt_id : s.equi_equi_id,
                               techOrEquip =  s.trakt_trakt_id != null ? 1 : 2,
                               techName = EquipTechName,
                           } into v
                            select new RepairTechEqiupModel
                            {
                                Id = v.Key.IdTech,
                                techOrEquip = v.Key.techOrEquip,
                                NameTech = v.Key.techName,
                            }).ToList();

                for (int i = 0; i < nlist.Count; i++)
                {
                    var id = nlist[i].Id; var techOrEquip = nlist[i].techOrEquip; var techId = nlist[i].Id;
                    nlist[i].SumCost = list1.Where(r => r.Id == id && r.techOrEquip == techOrEquip).Select(r => r.SumCost).Sum();
                    nlist[i].SumSpare = list2.Where(r => r.Id == id && r.techOrEquip == techOrEquip).Select(r => r.SumSpare).FirstOrDefault();
                    nlist[i].ServiceSum = list3.Where(r => r.Id == id && r.techOrEquip == techOrEquip).Select(r => r.ServiceSum).FirstOrDefault();

                    if (nlist[i].techOrEquip == 1)
                    {
                        nlist[i].Days = db.Repairs_acts.AsEnumerable().Where(t => t.trakt_trakt_id == techId && t.status == 3).
                            Select(t => (t.date_end.Value.Date - t.date_open.Value.Date).Days + 1).Sum();
                        //t.date_end.Value.DayOfYear + 1 - t.date_open.Value.DayOfYear


                    }
                    else {
                        nlist[i].Days = db.Repairs_acts.AsEnumerable().Where(t => t.equi_equi_id == techId && t.status == 3).
                            Select(t => (t.date_end.Value.Date - t.date_open.Value.Date).Days + 1).Sum(); 
                    }
                }
                wsh.Cells["b3"].Value = "на ремонт техники, оборудования за период: "+start.ToString("dd.MM.yyyy")+" - "+end.ToString("dd.MM.yyyy");
                var countTech = nlist.Where(t=>t.techOrEquip == 1).Count();
                var countEquip = nlist.Where(t => t.techOrEquip == 2).Count();
                wsh.Cells["b7"].Value = "ТЕХНИКА (Кол-во единиц: " + countTech + ")";
                var listTech = nlist.Where(t => t.techOrEquip == 1).OrderBy(t => t.NameTech).ToList();
                var listEquip = nlist.Where(t => t.techOrEquip == 2).OrderBy(t => t.NameTech).ToList();
                uint k = 8;
                k = DrawTech(listTech, k, wsh, "Итого по Технике:");
                var tSumSpare =  Decimal.Parse(wsh.Cells["e" + (k - 2).ToString()].Text);
                var tSumCost = Decimal.Parse( wsh.Cells["f" + (k - 2).ToString()].Text);
                var ServiceSum =  Decimal.Parse(wsh.Cells["g" + (k - 2).ToString()].Text);

                wsh.Cells["e" + k.ToString()].Value = "ОБОРУДОВАНИЕ (Кол-во единиц: " + countEquip + ")";
                wsh.Cells["e" + k.ToString()].Style.Font.Bold = true; k++;
                k = DrawTech(listEquip, k, wsh, "Итого по Оборудованию:");
                var tSumSpare2 =  Decimal.Parse(wsh.Cells["e" + (k - 2).ToString()].Text);
                var tSumCost2 =  Decimal.Parse(wsh.Cells["f" + (k - 2).ToString()].Text);
                var ServiceSum2 =  Decimal.Parse(wsh.Cells["g" + (k - 2).ToString()].Text);

               setBorder(wsh.Cells["b8:h" + (k-2).ToString()].Style.Border);
               wsh.Cells["d" + k.ToString()].Value = "ВСЕГО затрат, руб.:";
               wsh.Cells["d" + k.ToString()].Style.Font.Bold = true;
               wsh.Cells["d" + k.ToString()].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;

               wsh.Cells["e" + k.ToString()].Value = ((decimal?)tSumSpare ?? 0) + ((decimal?)tSumSpare2 ?? 0);
               wsh.Cells["f" + k.ToString()].Value = ((decimal?)tSumCost ?? 0) + ((decimal?)tSumCost2 ?? 0);
               wsh.Cells["g" + k.ToString()].Value = ((decimal?)ServiceSum ?? 0) + ((decimal?)ServiceSum2 ?? 0);
               wsh.Cells["h" + k.ToString()].Value = Decimal.Parse(wsh.Cells["e" + k.ToString()].Text) + Decimal.Parse(wsh.Cells["f" + k.ToString()].Text) +
                Decimal.Parse(wsh.Cells["g" + k.ToString()].Text);
               setBorder(wsh.Cells["e" + k.ToString()+":h"+ k.ToString()].Style.Border);
               wsh.Cells["e" + k.ToString() + ":h" + k.ToString()].Style.Font.Bold = true;
               wsh.Cells["c" + (k + 3).ToString()].Value = "Ответственный ___________________";
                var res = new ReportFile
                {
                    data = pck.GetAsByteArray(),
                    fileName = "Свод затрат.xlsx"
                };
                return res;
            }
        }

        public uint DrawTech(List<RepairTechEqiupModel> listTech, uint k, ExcelWorksheet wsh, string total)
        {
            for (int i = 0; i < listTech.Count; i++)
            {
                wsh.Cells["B" + k.ToString()].Value = i + 1;
                wsh.Cells["C" + k.ToString()].Value = listTech[i].NameTech;
                wsh.Cells["D" + k.ToString()].Value = listTech[i].Days;
                wsh.Cells["E" + k.ToString()].Value = listTech[i].SumSpare;
                wsh.Cells["f" + k.ToString()].Value = listTech[i].SumCost;
                wsh.Cells["g" + k.ToString()].Value = listTech[i].ServiceSum;
                wsh.Cells["h" + k.ToString()].Value = ((decimal)(listTech[i].SumSpare ?? 0) + (listTech[i].SumCost ?? 0) +
               (decimal)(listTech[i].ServiceSum ?? 0));
                k++;
            }
            wsh.Cells["c" + k.ToString()].Value = total;
            wsh.Cells["c" + k.ToString()].Style.Font.Bold = true;
            wsh.Cells["c" + k.ToString()].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;

            wsh.Cells["d" + k.ToString()].Value = listTech.Sum(t => t.Days);
            wsh.Cells["e" + k.ToString()].Value = listTech.Sum(t => t.SumSpare ?? 0);
            wsh.Cells["f" + k.ToString()].Value = listTech.Sum(t => t.SumCost ?? 0);
            wsh.Cells["g" + k.ToString()].Value = listTech.Sum(t => t.ServiceSum ?? 0);
            wsh.Cells["h" + k.ToString()].Value = (decimal)(listTech.Sum(t => t.SumSpare ?? 0)) + listTech.Sum(t => t.SumCost ?? 0) + (decimal)listTech.Sum(t => t.ServiceSum);
            k = k + 2;
            return k;
        }


        //ЕН печать
        public ReportFile GetDailyListResExcel(string pathTemplate, int ActId)
        {
            var tmpl = new FileInfo(pathTemplate);
            using (ExcelPackage pck = new ExcelPackage(tmpl, true))
            {
                ExcelWorksheet wsh = pck.Workbook.Worksheets[1];
                wsh.Name = "Ежедневный наряд";
                var empTaskInfo = new EmpTaskInfo();
                empTaskInfo.Id = ActId;
                var list = spareDb.GetDailyRepairsItem(empTaskInfo);
                wsh.Cells["F5"].Value = list.Main.Date.ToString("dd.MM.yyyy");
                wsh.Cells["F4"].Value = list.Main.DailyNum;
                var RepAct = db.Repairs_acts.Where(t => t.rep_id == list.Main.RepairsActs).FirstOrDefault();
                string name_emp = "";
                string tech = ""; 
                if (RepAct != null) {
                name_emp = db.Employees.Where(t => t.emp_id == RepAct.emp_emp_id).Select(t => t.short_name).FirstOrDefault();
                if (RepAct.equi_equi_id != null)
                {
                    tech = db.Equipments.Where(t => t.equi_id == RepAct.equi_equi_id).Select(t => t.name).FirstOrDefault();
                }
                else {
                    tech = db.Traktors.Where(t => t.trakt_id == RepAct.trakt_trakt_id).Select(t => t.name +" "+t.reg_num).FirstOrDefault();
                }
                }
                wsh.Cells["b9"].Value = "Отвественный: "+name_emp;
                wsh.Cells["b11"].Value = "Техника (марка, гос. номер, наименование агрегата): "+ tech;

                uint k = 14;
                for (int i = 0; i < list.Spendings.Count; i++)
                {
                    wsh.Cells["B" + k.ToString()].Value = i + 1;
                    wsh.Cells["C" + k.ToString()].Value = list.Spendings[i].Employees.Name;
                    wsh.Cells["D" + k.ToString()].Value = list.Spendings[i].Tasks.Name;
                    wsh.Cells["E" + k.ToString()].Value = list.Spendings[i].RatePiecework;
                    wsh.Cells["F" + k.ToString()].Value = list.Spendings[i].Hours;
                    wsh.Cells["G" + k.ToString()].Value = list.Spendings[i].SumCost;
                    k++;
                }
                if (list.Spendings.Count != 0)
                {
                    setBorder(wsh.Cells["b14:G" + (k - 1).ToString()].Style.Border);
                }
              wsh.Cells["E" + k.ToString()].Value = "Итого:";
              wsh.Cells["F" + k.ToString()].Value = list.Spendings.Sum(t => t.Hours);
              wsh.Cells["G" + k.ToString()].Value = list.Spendings.Sum(t => t.SumCost);
             var idOrg = Int32.Parse(db.Parameters.Where(t => t.name.Equals("NameOrg")).Select(t => t.value).FirstOrDefault());
             wsh.Cells["b2"].Value = "Организация: " + db.Organizations.Where(t => t.org_id == idOrg).Select(t => t.name).FirstOrDefault(); 

              if (list.Spendings.Count != 0)
              {
                  setBorder(wsh.Cells["E" + k.ToString() + ":" + "G" + k.ToString()].Style.Border);
              }
              wsh.Cells["E" + k.ToString() + ":" + "G" + k.ToString()].Style.Font.Bold = true;
              wsh.Cells["b" + (k + 3).ToString()].Value = "Подпись ответственного                       ________________ /"+name_emp+"/";
              wsh.Cells["b" + (k + 5).ToString()].Value = "Подпись руководителя подразделения ________________ /Трухачев А.Е./";
                var r = new ReportFile
                {
                    data = pck.GetAsByteArray(),
                    fileName = "Ежедневный наряд.xlsx"
                };
                return r;
            }

        }


        //история запчасти
        public ReportFile GetHistoryResExcel(string pathTemplate, int ActId)
        {
            var tmpl = new FileInfo(pathTemplate);
            using (ExcelPackage pck = new ExcelPackage(tmpl, true))
            {
                ExcelWorksheet wsh = pck.Workbook.Worksheets[1];
                wsh.Name = "Приход";
                var list = spareDb.GetHistorySpareList(ActId);
                uint k = 5;
                        for (int i = 0; i < list.InvoiceList.Count; i++)
                        {
                            wsh.Cells["A" + k.ToString()].Value = list.InvoiceList[i].SpareParts1;
                            wsh.Cells["B" + k.ToString()].Value = list.InvoiceList[i].Act_number;
                            wsh.Cells["C" + k.ToString()].Value = list.InvoiceList[i].DateBegin1;
                            wsh.Cells["D" + k.ToString()].Value = list.InvoiceList[i].Price;
                            wsh.Cells["E" + k.ToString()].Value = list.InvoiceList[i].Count;
                            wsh.Cells["F" + k.ToString()].Value = list.InvoiceList[i].Cost3;
                            wsh.Cells["G" + k.ToString()].Value = list.InvoiceList[i].Storage1;
                            k++;
                        }
                        if (list.InvoiceList.Count != 0)
                        {
                            setBorder(wsh.Cells["A5:G" + (k - 1).ToString()].Style.Border);
                        }
                //расход и резерв
                        ExcelWorksheet wsh2 = pck.Workbook.Worksheets[2];
                        wsh2.Name = "Расход";
                        k = 5;
                        for (int i = 0; i < list.WriteOffList.Count; i++)
                        {
                            wsh2.Cells["A" + k.ToString()].Value = list.WriteOffList[i].SpareParts1;
                            wsh2.Cells["B" + k.ToString()].Value = list.WriteOffList[i].Act_number;
                            wsh2.Cells["C" + k.ToString()].Value = list.WriteOffList[i].Date1;
                            wsh2.Cells["D" + k.ToString()].Value = list.WriteOffList[i].Count;
                            wsh2.Cells["E" + k.ToString()].Value = list.WriteOffList[i].CarsName;
                            wsh2.Cells["F" + k.ToString()].Value = list.WriteOffList[i].CarsNum;
                            wsh2.Cells["G" + k.ToString()].Value = list.WriteOffList[i].CheckWrite;
                            k++;
                        }
                        if (list.WriteOffList.Count != 0)
                        {
                            setBorder(wsh2.Cells["A5:G" + (k - 1).ToString()].Style.Border);
                        }
                var r = new ReportFile
                {
                    data = pck.GetAsByteArray(),
                    fileName = "История запчасти.xlsx"
                };
                return r;
            }
        }
        //
        ///............................. Расход ТМЦ
        public ReportFile GetFullConsumptionExcel(string pathTemplate, DateTime startDate, DateTime endDate, int? matId, int? PlantId,Boolean check)
        {
            var tmpl = new FileInfo(pathTemplate);
            using (ExcelPackage pck = new ExcelPackage(tmpl, true))
            {
                ExcelWorksheet wsh = pck.Workbook.Worksheets[1];
                wsh.Name = "Расход ТМЦ";
                wsh.Cells["c4"].Value = startDate.ToString("dd.MM.yyyy");
                wsh.Cells["e4"].Value = endDate.ToString("dd.MM.yyyy");
                char ch;
                if (!check) {  ch = 'A'; } else {  ch = 'C'; }
                var idOrg = Int32.Parse(db.Parameters.Where(t => t.name.Equals("NameOrg")).Select(t => t.value).FirstOrDefault());
                var org_name = db.Organizations.Where(t => t.org_id == idOrg).Select(t => t.name).FirstOrDefault();
                switch (matId)
                { 
                    case 1 :
                        wsh.Cells["c2"].Value = "Сравнительная характеристика использования средств защиты растений и микроудобрений в " + org_name + " в " + startDate.Year + " г.";
                        wsh.Cells[ch + "6"].Value = "Наименование препарата";
                        ch++;ch++;
                        wsh.Cells[ch + "6"].Value = "Площадь внесения,га";
                        ch++;

                        if (check) { wsh.Cells[ch + "6"].Value = "Норма внесения на 1 га(л/кг)"; ch++; }
                        wsh.Cells[ch + "6"].Value = "Стоимость руб за ед. (л/кг)";
                        ch++;
                        wsh.Cells[ch + "6"].Value = "Количество на всю площадь, кг/л";
                        break;
                    case 2 :
                        wsh.Cells["c2"].Value = "Сравнительная характеристика использования минеральных и органических  удобрений в " + org_name + " в " + startDate.Year + " г.";
                        wsh.Cells[ch + "6"].Value = "Наименование удобрения";
                        ch++;ch++;
                        wsh.Cells[ch + "6"].Value = "Площадь внесения,га";
                        ch++;
                        if (check) { wsh.Cells[ch + "6"].Value = "Норма внесение на 1 га(кг)"; ch++; }
                       
                        wsh.Cells[ch + "6"].Value = "Стоимость руб за ед. (кг)";
                        ch++;
                        wsh.Cells[ch + "6"].Value = "Количество на всю площадь, кг";
                        break;
                    case  4 :
                        wsh.Cells["c2"].Value = "Сравнительная характеристика расхода посевного /посадочного материала в " + org_name + " в " + startDate.Year + " г.";
                   break;
                    case 5 :
                   wsh.Cells["c2"].Value = "Сравнительная характеристика использования прочих материалов в " + org_name + " в " + startDate.Year + " г.";
                        wsh.Cells[ch + "6"].Value = "Наименование пр. материала";
                          ch++;ch++;
                        wsh.Cells[ch + "6"].Value = "Площадь внесения,га";
                        ch++;
                        if (check) { wsh.Cells[ch + "6"].Value = "Норма внесение на 1 га(шт)"; ch++; }
                     
                        wsh.Cells[ch + "6"].Value = "Стоимость руб за ед. (шт)";
                        ch++;
                        wsh.Cells[ch + "6"].Value = "Количество на всю площадь, шт";
                        break;
                        //все
                    case -1:
                        wsh.Cells["c2"].Value = "Сравнительная характеристика использования СЗР, мин. удобрений, семян, прочих материалов в " + org_name + " в " + startDate.Year + " г.";
                        wsh.Cells[ch + "6"].Value = "Наименование";
                        ch++; ch++;
                        wsh.Cells[ch + "6"].Value = "Площадь внесения,га";
                        ch++;
                        if (check) { wsh.Cells[ch + "6"].Value = "Норма внесение на 1 га(л/кг/шт)"; ch++; }

                        wsh.Cells[ch + "6"].Value = "Стоимость руб за ед. (л/кг/шт)";
                        ch++;
                        wsh.Cells[ch + "6"].Value = "Количество на всю площадь, кг/л/шт";
                        break;

                }

                endDate = endDate.AddDays(1);

             var factList = db.TMC_Records.Where(v=>
            v.emp_from_id != 0 && v.emp_to_id == 0 && (matId != -1 ? v.Materials.mat_type_id == matId : true) && 
            v.TMC_Acts.act_date >= startDate && v.TMC_Acts.act_date <= endDate
         && (PlantId != -1 ? v.Fields_to_plants.Plants.plant_id == PlantId :true)
         ).AsEnumerable().Select(v=>
                            new FactTmcModel
                            {
                            id = v.tmr_id,
                            material_name = v.Materials.name,
                            material_id = v.Materials.mat_id,
                            plant_name =  v.Fields_to_plants != null ? v.Fields_to_plants.Plants.name : null,
                            plant_id = v.Fields_to_plants != null ? v.Fields_to_plants.Plants.plant_id : -1,
                            field_name = v.Fields_to_plants != null ? v.Fields_to_plants.Fields.name : null,
                            field_id = v.Fields_to_plants != null ? v.Fields_to_plants.Fields.fiel_id : -1,
                            area_plant = (decimal?)v.area,
                            consumption = (decimal?)(v.area != 0 ? v.count / v.area : 0),
                            summa_kol = (decimal?)v.price,
                            kol = (decimal?)v.count,
                            total_sum = (decimal?)(v.count * v.price),
                            summa_ga = (decimal?)(v.area != 0 ? (v.count * v.price) / v.area : 0),
                           }).OrderBy(t=>t.plant_name).ToList();

            var factlst = factList.Where(t => t.plant_name != null).OrderBy(t => t.plant_name).ToList();
             var factlst2 = factList.Where(t => t.plant_name == null).ToList();
             if (check) //расширенный
             {
                 factList.Clear(); factList.AddRange(factlst); 
             }
             if (!check)
             {
                 factList = factList.OrderBy(t => t.material_id).ToList();
                 for (int j = 0; j < factList.Count; j++)
                 {

                     if (j > 0)
                     {
                         if (factList[j].material_id == factList[j - 1].material_id)
                         {
                             factList[j].area_plant = (factList[j].area_plant ?? 0) + (factList[j - 1].area_plant ?? 0);
                             factList[j].kol = (factList[j].kol ?? 0) + (factList[j - 1].kol ?? 0);
                             factList[j].total_sum = (factList[j].total_sum ?? 0) + (factList[j - 1].total_sum ?? 0);
                             factList[j].summa_ga = (factList[j].total_sum ?? 0) / (factList[j].area_plant ?? 0);
                             factList.RemoveAt(j - 1);
                             j--;
                         }
                     }
                 };
             }
             var paramsList = (from tc in db.TC_param_value where
                                   tc.TC.year == startDate.Year
                                   && (matId != -1 ? tc.Materials.mat_type_id == matId : true)
                                   && (PlantId != -1 ? tc.TC.plant_plant_id == PlantId : true) &&
                                   (tc.id_tc_param == 42 || tc.id_tc_param == 44 || tc.id_tc_param == 48 || tc.id_tc_param == 49 )
                               let tcCost = db.TC_param_value.Where(x => x.id_tc_param == 4 && x.id_tc == tc.id_tc).Select(x => x.value).FirstOrDefault()
                                   group new {}
                              by new
                                  {
                                      id = tc.id,
                                      id_tc = tc.id_tc,
                                      material_name_plan = tc.Materials.name,
                                      material_id = tc.Materials.mat_id,
                                      plant_name_plan = tc.TC.Plants.name,
                                      plant_id = tc.TC.Plants.plant_id,
                                      id_tc_param = tc.id_tc_param,
                                      value = tc.value,
                                      costTC = tcCost,
                                  } into g
                               select new
                                 {
                                     id = g.Key.id,
                                     id_tc = g.Key.id_tc,
                                     material_name_plan = g.Key.material_name_plan,
                                     material_id = g.Key.material_id,
                                     plant_name_plan = g.Key.plant_name_plan,
                                     plant_id = g.Key.plant_id,
                                     id_tc_param = g.Key.id_tc_param,
                                     value = g.Key.value,
                                     costTC = g.Key.costTC,
                                 }).ToList();
             var planList = new List<PlanFactTmcModel>();

             for (int i = 0; i < paramsList.Count; i++)
             {
                 if (paramsList[i].costTC != 0)
                 {
                     planList.Add(new PlanFactTmcModel
                     {
                         id = paramsList[i].id,
                         material_name_plan = paramsList[i].material_name_plan,
                         material_id = paramsList[i].material_id,
                         plant_name_plan = paramsList[i].plant_name_plan,
                         plant_id = paramsList[i].plant_id,
                         area_plant_plan = (decimal?)paramsList[i + 3].value,
                         consumption = (decimal?)paramsList[i + 2].value,
                         summa_kol_plan = (decimal?)paramsList[i].value,
                         total_sum_plan = (decimal?)paramsList[i + 1].value,  //стоимость
                     });
                 }
                 i = i + 3;
             }
             for (int i = 0; i < planList.Count; i++)
             {
                 planList[i].kol_plan = (planList[i].area_plant_plan ?? 0) * (planList[i].consumption ?? 0);
             }


             if (!check) {
                 planList = planList.OrderBy(t => t.material_id).ToList();
                 for (int j = 0; j < planList.Count; j++)
                 {

                     if (j > 0)
                     {
                         if (planList[j].material_id == planList[j - 1].material_id)
                         {
                             planList[j].area_plant_plan = (planList[j].area_plant_plan ?? 0) + (planList[j - 1].area_plant_plan ?? 0);
                             planList[j].kol_plan = (planList[j].kol_plan ?? 0) + (planList[j - 1].kol_plan ?? 0);
                          planList[j].total_sum_plan = (planList[j].total_sum_plan ?? 0) + (planList[j - 1].total_sum_plan ?? 0);
                   //          planList[j].summa_ga_plan = (planList[j].total_sum_plan ?? 0) / ((decimal?)planList[j].area_plant_plan ?? 0);
                          //if (planList[j].area_plant_plan == 0 || !planList[j].area_plant_plan.HasValue) {
                          //    var count11 = 1;
                          //}

                             planList[j].summa_ga_plan = (planList[j].area_plant_plan == 0 || !planList[j].area_plant_plan.HasValue) ? 0 :
                                 (planList[j].total_sum_plan ?? 0) / ((decimal?)planList[j].area_plant_plan ?? 0);
                             planList.RemoveAt(j - 1);
                             j--;
                         }
                     }
                 }  
             }
             var planList1 = new List<PlanFactTmcModel>();
                //расширенный отчет
             if (check)
             {
                 var plantfield = techcard.GetPlantSortFieldByYear(startDate.Year);
                 for (int i = 0; i < planList.Count; i++)
                 {
                     //
                     decimal? summaFields = 0;
                     var fields = new List<FieldDictionary>();
                    
                     //общая площадь
                     for (int i1 = 0; i1 < plantfield.Count; i1++)
                     {
                         var p = plantfield[i1];
                         if (p.Id == planList[i].plant_id)
                         {
                             for (int j2 = 0; j2 < p.Values.Count; j2++)
                             {
                                 var s = p.Values[j2];
                             }
                             if (fields.Count == 0)
                             {
                                 fields = p.Fields;
                             }
                             for (int k1 = 0; k1 < fields.Count; k1++)
                             {
                                 summaFields += fields[k1].Area;
                             }
                         }
                     }
                     //
                     for (int j = 0; j < fields.Count; j++)
                     {
                         if (fields[j].Area != 0)
                         {
                             planList1.Add(new PlanFactTmcModel
                               {
                                   id = planList[i].id,
                                   field_id = fields[j].Id,
                                   material_id = planList[i].material_id,
                                   plant_name_plan = planList[i].plant_name_plan,
                                   plant_id = planList[i].plant_id,
                                   material_name_plan = planList[i].material_name_plan,
                                   field_name_plan = fields[j].Name,
                                   area_plant_plan = planList[i].area_plant_plan * (fields[j].Area / fields.Sum(t => t.Area)),
                                   consumption = planList[i].consumption,
                                   summa_kol_plan = planList[i].summa_kol_plan,
                                   kol_plan = planList[i].kol_plan * (fields[j].Area / fields.Sum(t => t.Area)),
                                   total_sum_plan = planList[i].total_sum_plan * (decimal?)(fields[j].Area / fields.Sum(t => t.Area)),
                                   summa_ga_plan = planList[i].summa_ga_plan * (decimal?)(fields[j].Area / fields.Sum(t => t.Area)),
                               });
                         }
                     }
                 }
             }
                //

             int k = 7, u = 8, o = 9; decimal total_area_fact = 0, total_area_plan = 0, total_kol_fact = 0, total_kol_plan = 0,
              total_sum_fact = 0; decimal total_sum_plan = 0;
             for (int i = 0; i < factList.Count; i++)
             {
                 if (check)
                 {
                     ch = 'c';
                     wsh.Cells["a" + k.ToString()].Value = factList[i].plant_name;
                     wsh.Cells["b" + k.ToString()].Value = factList[i].field_name;
                 }
                 else {
                     ch = 'a';
                 }
                 wsh.Cells[ch + k.ToString()].Value = factList[i].material_name;
             //факт
                 ch++; ch++;
                 wsh.Cells[ch + u.ToString()].Value = factList[i].area_plant;
              total_area_fact = total_area_fact + (decimal)(factList[i].area_plant ?? 0);
              ch++;
              if (check)
              {
                  wsh.Cells[ch + u.ToString()].Value = factList[i].consumption; ch++;  //расход
                  wsh.Cells[ch + u.ToString()].Value = factList[i].summa_kol; 
              }
              else { wsh.Cells[ch + u.ToString()].Value = (factList[i].total_sum ?? 0) / (factList[i].kol ?? 0); }   //цена
              if (check) { ch++; wsh.Cells[ch + u.ToString()].Value = factList[i].kol; }
              else { ch++; wsh.Cells[ch + u.ToString()].Value = factList[i].kol; }
                 ch++;
                 wsh.Cells[ch + u.ToString()].Value = factList[i].total_sum;
                 ch++;
                 total_kol_fact = total_kol_fact + (decimal)(factList[i].kol ?? 0);
                 total_sum_fact = total_sum_fact + (decimal)(factList[i].total_sum ?? 0);
              wsh.Cells[ch + u.ToString()].Value = factList[i].summa_ga;
                 ///////////////////////////////////////////////сжатый отчет план
              if (!check) {
                  var planList2 = planList.Where(t => t.material_id == factList[i].material_id)
                .Select(t => new PlanFactTmcModel
                {
                    area_plant_plan = t.area_plant_plan,
                    consumption = t.consumption,
                    summa_kol_plan = t.summa_kol_plan,
                    kol_plan = t.kol_plan,
                   total_sum_plan = t.total_sum_plan,
                    summa_ga_plan = t.summa_ga_plan,
                }).ToList();
                  if (planList2.Count != 0)
                  {
                      var doc = planList.Where(t =>  t.material_id == factList[i].material_id).FirstOrDefault();
                      planList.Remove(doc);
                      //план
                      wsh.Cells["c" + k.ToString()].Value = planList2[0].area_plant_plan;
                      total_area_plan = total_area_plan + ((decimal?)planList2[0].area_plant_plan ?? 0);
                      wsh.Cells["d" + k.ToString()].Value = (planList2[0].total_sum_plan ?? 0)/(planList2[0].kol_plan ?? 0);
                      wsh.Cells["e" + k.ToString()].Value = planList2[0].kol_plan;
                      wsh.Cells["f" + k.ToString()].Value = planList2[0].total_sum_plan;  //стоимость всего
                      total_kol_plan = total_kol_plan + ((decimal?)planList2[0].kol_plan ?? 0);
                      total_sum_plan = total_sum_plan + (planList2[0].total_sum_plan ?? 0);
                      wsh.Cells["g" + k.ToString()].Value = ((decimal?)planList2[0].total_sum_plan ?? 0) / (decimal?)planList2[0].area_plant_plan;

                      wsh.Cells["c" + o.ToString()].Value = planList2[0].area_plant_plan - (decimal?)factList[i].area_plant;
                      wsh.Cells["d" + o.ToString()].Value = (decimal?)planList2[0].summa_kol_plan - (decimal?)factList[i].summa_kol;
                      wsh.Cells["e" + o.ToString()].Value = planList2[0].kol_plan - (decimal?)factList[i].kol;
                      wsh.Cells["f" + o.ToString()].Value = planList2[0].total_sum_plan - (decimal?)factList[i].total_sum;
                      wsh.Cells["g" + o.ToString()].Value = planList2[0].summa_ga_plan - (decimal?)factList[i].summa_ga;
                  }
              }
                 //расширенный отчет
              if (check) { 
              var planList2 = planList1.Where(t => t.field_id == factList[i].field_id && t.material_id == factList[i].material_id && t.plant_id == factList[i].plant_id)
                  .Select(t => new PlanFactTmcModel { 
                  area_plant_plan = t.area_plant_plan,
                  consumption = t.consumption,
                  summa_kol_plan = t.summa_kol_plan,
                  kol_plan = t.kol_plan,
              //  total_sum_plan = t.total_sum_plan,
                  summa_ga_plan = t.summa_ga_plan,
                  }).ToList();
              if (planList2.Count != 0)
              {
                  var doc = planList1.Where(t => t.field_id == factList[i].field_id && t.material_id == factList[i].material_id && t.plant_id == factList[i].plant_id).FirstOrDefault();
                  planList1.Remove(doc);
                  //план
                  wsh.Cells["e" + k.ToString()].Value = planList2[0].area_plant_plan;
                  total_area_plan = total_area_plan + ((decimal?)planList2[0].area_plant_plan ?? 0);
                  wsh.Cells["f" + k.ToString()].Value = planList2[0].consumption;
                  wsh.Cells["g" + k.ToString()].Value = planList2[0].summa_kol_plan;
                  wsh.Cells["h" + k.ToString()].Value = planList2[0].kol_plan;
                  wsh.Cells["i" + k.ToString()].Value = planList2[0].kol_plan * planList2[0].summa_kol_plan;
                  total_kol_plan = total_kol_plan + ((decimal?)planList2[0].kol_plan ?? 0);
                  total_sum_plan = total_sum_plan + (((decimal?)planList2[0].kol_plan ?? 0) * ((decimal?)planList2[0].summa_kol_plan ?? 0));
                  wsh.Cells["j" + k.ToString()].Value = planList2[0].kol_plan * planList2[0].summa_kol_plan / planList2[0].area_plant_plan;

                  wsh.Cells["e" + o.ToString()].Value = planList2[0].area_plant_plan - (decimal?)factList[i].area_plant;
                  wsh.Cells["f" + o.ToString()].Value = planList2[0].consumption - (decimal?)factList[i].consumption;
                  wsh.Cells["g" + o.ToString()].Value = planList2[0].summa_kol_plan - (decimal?)factList[i].summa_kol;
                  wsh.Cells["h" + o.ToString()].Value = planList2[0].kol_plan - (decimal?)factList[i].kol;
                  wsh.Cells["i" + o.ToString()].Value = planList2[0].total_sum_plan - (decimal?)factList[i].total_sum;
                  wsh.Cells["j" + o.ToString()].Value = planList2[0].summa_ga_plan - (decimal?)factList[i].summa_ga;
              }
              }
              k = k + 3; u = u + 3; o = o + 3;
               }

             if (check)
             {
                 //что осталось
                 for (int i = 0; i < planList1.Count; i++)
                 {
                     wsh.Cells["a" + k.ToString()].Value = planList1[i].plant_name_plan;
                     wsh.Cells["b" + k.ToString()].Value = planList1[i].field_name_plan;
                     wsh.Cells["c" + k.ToString()].Value = planList1[i].material_name_plan;

                     wsh.Cells["e" + k.ToString()].Value = planList1[i].area_plant_plan;
                     total_area_plan = total_area_plan + ((decimal?)planList1[i].area_plant_plan ?? 0);
                     wsh.Cells["f" + k.ToString()].Value = planList1[i].consumption;
                     wsh.Cells["g" + k.ToString()].Value = planList1[i].summa_kol_plan;
                     wsh.Cells["h" + k.ToString()].Value = planList1[i].kol_plan;
                     wsh.Cells["i" + k.ToString()].Value = planList1[i].kol_plan * planList1[i].summa_kol_plan;
                     wsh.Cells["j" + k.ToString()].Value = planList1[i].kol_plan * planList1[i].summa_kol_plan / planList1[i].area_plant_plan;

                     wsh.Cells["e" + o.ToString()].Value = planList1[i].area_plant_plan - 0;
                     wsh.Cells["f" + o.ToString()].Value = planList1[i].consumption - 0;
                     wsh.Cells["g" + o.ToString()].Value = planList1[i].summa_kol_plan - 0;
                     wsh.Cells["h" + o.ToString()].Value = planList1[i].kol_plan - 0;
                     wsh.Cells["i" + o.ToString()].Value = planList1[i].total_sum_plan - 0;

                     total_kol_plan = total_kol_plan + ((decimal?)planList1[i].kol_plan ?? 0);
                     total_sum_plan = total_sum_plan + (((decimal?)planList1[i].kol_plan ?? 0) * ((decimal?)planList1[i].summa_kol_plan ?? 0));
                     wsh.Cells["j" + o.ToString()].Value = planList1[i].summa_ga_plan - 0;
                     o = o + 3; k = k + 3; u = u + 3;
                 }
             }
                 //ПЛАН СЖАТЫЙ
             else {
                 for (int i = 0; i < planList.Count; i++)
                 {
                     wsh.Cells["a" + k.ToString()].Value = planList[i].material_name_plan;
                     wsh.Cells["c" + k.ToString()].Value = planList[i].area_plant_plan;
                     total_area_plan = total_area_plan + ((decimal?)planList[i].area_plant_plan ?? 0);
                     wsh.Cells["d" + k.ToString()].Value = (planList[i].kol_plan == 0 || !planList[i].kol_plan.HasValue) ? 0 :
                    ((planList[i].total_sum_plan ?? 0) / ((decimal?)planList[i].kol_plan ?? 0));
                     wsh.Cells["e" + k.ToString()].Value = planList[i].kol_plan;
                     wsh.Cells["f" + k.ToString()].Value = planList[i].total_sum_plan;  //стоимость всего
                     wsh.Cells["g" + k.ToString()].Value = (planList[i].area_plant_plan == 0 || !planList[i].area_plant_plan.HasValue) ? 0 :
                    ( (planList[i].total_sum_plan ?? 0) / (decimal?)planList[i].area_plant_plan );
                     wsh.Cells["c" + o.ToString()].Value = planList[i].area_plant_plan - 0;
                     wsh.Cells["d" + o.ToString()].Value = planList[i].summa_kol_plan - 0;
                     wsh.Cells["e" + o.ToString()].Value = planList[i].kol_plan - 0;
                     wsh.Cells["f" + o.ToString()].Value = planList[i].total_sum_plan - 0;

                     total_kol_plan = total_kol_plan + ((decimal?)planList[i].kol_plan ?? 0);
                     total_sum_plan = total_sum_plan + (planList[i].total_sum_plan ?? 0);
                     wsh.Cells["g" + o.ToString()].Value = planList[i].summa_ga_plan - 0;
                     o = o + 3; k = k + 3; u = u + 3;
                 }
             }
             //========================================================строки без культуры и поля
             if (check)
             {
                 for (int i = 0; i < factlst2.Count; i++)
                 {
                  ch = 'c';
                  wsh.Cells["a" + k.ToString()].Value = factlst2[i].plant_name;
                  wsh.Cells["b" + k.ToString()].Value = factlst2[i].field_name;
                  wsh.Cells[ch + k.ToString()].Value = factlst2[i].material_name;
                     //факт
                     ch++; ch++;
                     wsh.Cells[ch + u.ToString()].Value = factlst2[i].area_plant;
                     total_area_fact = total_area_fact + (decimal)(factlst2[i].area_plant ?? 0);
                     ch++;
                     wsh.Cells[ch + u.ToString()].Value = factlst2[i].consumption; ch++;  //расход
                     wsh.Cells[ch + u.ToString()].Value = factlst2[i].summa_kol;

                     ch++; wsh.Cells[ch + u.ToString()].Value = factlst2[i].kol; 
                     ch++;
                     wsh.Cells[ch + u.ToString()].Value = factlst2[i].total_sum;
                     ch++;
                     total_kol_fact = total_kol_fact + (decimal)(factlst2[i].kol ?? 0);
                     total_sum_fact = total_sum_fact + (decimal)(factlst2[i].total_sum ?? 0);
                     wsh.Cells[ch + u.ToString()].Value = factlst2[i].summa_ga;
                     k = k + 3; u = u + 3; o = o + 3;
                 }
             }
            //========================================================================

                //ИТОГО
             if (check)
             {
                 ch = 'e';
             }
             else { ch = 'c'; }

             wsh.Cells["a" + k.ToString()].Value = "ИТОГО";
             wsh.Cells["a" + k.ToString()].Style.Font.Bold = true;

             wsh.Cells[ch + k.ToString()].Value = total_area_plan;
             wsh.Cells[ch + (k + 1).ToString()].Value = total_area_fact;
             wsh.Cells[ch + (k + 2).ToString()].Value = total_area_plan - total_area_fact;
             ch++; ch++; if (check) { ch++; }
             wsh.Cells[ch + k.ToString()].Value = total_kol_plan;
             wsh.Cells[ch + (k + 1).ToString()].Value = total_kol_fact;
             wsh.Cells[ch + (k + 2).ToString()].Value = total_kol_plan - total_kol_fact;
             ch++;

             var total_summa_plan = planList.Sum(t => t.total_sum_plan ?? 0);
             wsh.Cells[ch + k.ToString()].Value = total_sum_plan;

             wsh.Cells[ch + (k + 1).ToString()].Value = total_sum_fact;
             wsh.Cells[ch + (k + 2).ToString()].Value = total_sum_plan - total_sum_fact;

             k = k + 3;
             ch++;
             setBorder(wsh.Cells["A7:" + ch + (k - 1).ToString()].Style.Border);

             if (factList.Count != 0)
             {
                 setBorder(wsh.Cells["A7:" + ch + (k - 1).ToString()].Style.Border);
             }
                var l = 7;

                if (!check) { ch = 'b'; } else {
                    ch = 'd';
                }

                for (int i = 0; i < (k-6) / 3; i++)
                {
                    wsh.Cells[ch + l.ToString()].Value = "План";
                    wsh.Cells[ch + l.ToString()].Style.Font.Bold = true;
                    l++;
                    wsh.Cells[ch + l.ToString()].Value = "Факт";
                    wsh.Cells[ch + l.ToString()].Style.Font.Bold = true;
                    l++;
                    wsh.Cells[ch + l.ToString()].Value = "Отклонение";
                    wsh.Cells[ch + l.ToString()].Style.Font.Bold = true;
                    l++;
                }

                var r = new ReportFile
                {
                    data = pck.GetAsByteArray(),
                    fileName = "Расход ТМЦ.xlsx"
                };
                return r;
            }
        }
        public ReportFile GetDictionaryExcel(string pathTemplate, int idDictionary)
        {
            var tmpl = new FileInfo(pathTemplate);
            using (ExcelPackage pck = new ExcelPackage(tmpl, true))
            {
                var list1 = new Object();
                ExcelWorksheet wsh = pck.Workbook.Worksheets[1];
                wsh.Name = "Справочники";
                var distInfo = dictionary.GetDictionaryById(idDictionary);
                var request = distInfo.Request.Split('/')[1];
                if (request.Equals("GetMaterials")) { list1 = dictionary.GetMaterials(null); }
                else
                if (request.Equals("GetSZR")) {  list1 = dictionary.GetMaterials(1); } else
                if (request.Equals("GetSeed")) {  list1 = dictionary.GetMaterials(4); } else
                    if (request.Equals("GetFertilizer")) {  list1 = dictionary.GetMaterials(2); }
                    else
                    {
                   list1 = typeof(DictionaryDb).GetMethod(request).Invoke(dictionary, null);
            
                    }
                wsh.Cells["a1"].Value = db.Dictionary.Where(t => t.id == idDictionary).Select(t => t.name).FirstOrDefault();
                //столбцы
                var list = (IList)list1;
                char s = 'A';
                for (int i = 0; i < distInfo.ColumnDefs.Count; i++)
                {
                    if (!String.IsNullOrEmpty(distInfo.ColumnDefs[i].displayName.Replace(" ", string.Empty)) && !(distInfo.ColumnDefs[i].field.Equals("rate_piecework")))
                    {
                        wsh.Cells[s.ToString() + 2].Value = distInfo.ColumnDefs[i].displayName;
                        wsh.Cells[s.ToString() + 2].Style.Font.Bold = true;
                        s++;
                    }
                }
                s = 'A'; int p = 3;
                    for (int j = 0; j < distInfo.ColumnDefs.Count; j++)
                    {
                        p = 3;
                        if (!distInfo.ColumnDefs[j].width.Equals("0%") && !distInfo.ColumnDefs[j].field.Equals("deleted") && !distInfo.ColumnDefs[j].field.Equals("delete")
                        && !(distInfo.ColumnDefs[j].field.Equals("rate_piecework")))
                        {
                            for (int i = 0; i < list.Count; i++)
                            {
                                var value = list[i].GetType().GetProperty(distInfo.ColumnDefs[j].field).GetValue(list[i], null);

                                if (value != null && (value is GeoCoord))
                                {
                                    var df = value.GetType().GetProperty("area").GetValue(value, null);
                               wsh.Cells[s + p.ToString()].Value = value.GetType().GetProperty("area").GetValue(value, null);
                                } else

                                if (value != null && (value is DictionaryItemsDTO))
                                {
                                    wsh.Cells[s + p.ToString()].Value = value.GetType().GetProperty("Name").GetValue(value, null);
                                } else 
                               if (value != null)
                                {
                                    wsh.Cells[s + p.ToString()].Value = value;
                                }
                  p++;
                            }
                            s++;
                        }
                    p++;
                }
                  
                    if (list.Count != 0)
                    {
                        setBorder(wsh.Cells["A2:" + (--s).ToString() + (list.Count+2).ToString()].Style.Border);
                    }
                var r = new ReportFile
                {
                    data = pck.GetAsByteArray(),
                    fileName = "Справочники.xlsx"
                };
                return r;
            }

        }

        //План-график культур
        public ReportFile GetPlanChartCultResExcel(string pathTemplate, int? cultId, int? year)
        {
            var tmpl = new FileInfo(pathTemplate);
            using (ExcelPackage pck = new ExcelPackage(tmpl, true))
            {
                ExcelWorksheet wsh = pck.Workbook.Worksheets[1];
                wsh.Name = "План-график культур";
                wsh.Cells["A1"].Value = "План-графики культур за " + year + " год.";
                //
                List<PlanRequestDto> totalplan1 = new List<PlanRequestDto>();
                var totalplan = (from tc in db.TC_param_value
                                 where tc.TC.year == year && (cultId != 0 ? tc.id_tc == cultId : true) &&
                                 (tc.id_tc_param == 1 || tc.id_tc_param == 2 || tc.id_tc_param == 3)
                                 group new
                                 {

                                 }
                                     by new
                                     {
                                         plant_name = tc.TC.Type_sorts_plants.name == null ? tc.TC.Plants.name : tc.TC.Plants.name + " (" + tc.TC.Type_sorts_plants.name + ")",
                                         id_tc = tc.id_tc,
                                         id_tc_param = tc.id_tc_param,
                                         value = tc.value,
                                         id = tc.id,
                                         grtask_id = tc.TC_operation.Type_tasks.auxiliary_rate,
                                     } into g
                                 select new PlanRequestDto
                                 {
                                     plant_name = g.Key.plant_name,
                                     id_tc = g.Key.id_tc,
                                     id_tc_param1 = g.Key.id_tc_param,
                                     value = (float?)g.Key.value,
                                     id = g.Key.id,
                                     grtask_id = g.Key.grtask_id
                                 }).OrderBy(l => l.plant_name).ToList();

                if (totalplan.Count == 0) {
                    var r = new ReportFile
                    {
                        data = pck.GetAsByteArray(),
                        fileName = "План-график культур.xlsx"
                    };
                    return r; 
                }

                 int j3 = 0;
                    var totplan = totalplan.Select(s => s.id_tc).Distinct().ToList();
                    for (j3 = 0; j3 < totplan.Count; j3++)
                    {
                        int id = totplan[j3];
                        var area = totalplan.Where(s => s.id_tc == totplan[j3] && s.id_tc_param1 == 1).Select(s => s.value).FirstOrDefault();
                        var gross_harvest = totalplan.Where(s => s.id_tc == totplan[j3] && s.id_tc_param1 == 3).Select(s => s.value).FirstOrDefault();
                        totalplan1.Add(new PlanRequestDto
                           {
                               id_tc = id,
                               plant_name = totalplan.Where(s => s.id_tc == totplan[j3]).Select(s => s.plant_name).FirstOrDefault(),
                               area = (decimal?)area,
                               productivity = totalplan.Where(s => s.id_tc == totplan[j3] && s.id_tc_param1 == 2).Select(s => s.value).FirstOrDefault(),
                               gross_harvest = gross_harvest,
                           });

                    }
                    totalplan1.Add(new PlanRequestDto
                      {
                          plant_name = "Итого",
                          area = totalplan1.Sum(l => l.area),
                      });
                //закраска по месяцам
                    var months = (from tc in db.TC_operation
                                  where tc.date_start.Value.Year == year &&
                                  (cultId != 0 ? tc.id_tc == cultId : true)
                                  group new
                                  {

                                  }
                                      by new PlanMonthRequestDto
                                      {
                                          date_start = tc.date_start,
                                          date_finish = tc.date_finish,
                                          id = tc.id,
                                          id_tc = tc.id_tc,
                                          mat_type_id = tc.Type_tasks.type_fuel_id  //id этап выращивания
                                      } into g
                                  select new PlanMonthRequestDto
                                  {
                                      date_start = g.Key.date_start,
                                      date_finish = g.Key.date_finish,
                                      id = g.Key.id,
                                      id_tc = g.Key.id_tc,
                                      mat_type_id =g.Key.mat_type_id,
                                  }).ToList();
                    setColor(wsh, "e6:ne" + (totalplan1.Count + 4).ToString(), "#C4BD97"); //ожидание
                //столбцы для всех дней;
                    var ls = new List<string>(); ls.Add("");
                    char h ='A';var h1='A';
                    for (int j = 0; j < DateTime.Now.DayOfYear; j++)
                    {
                        if (j == 0) { h = 'E'; } h1 = 'A';
                        for (int l = 0; l < 26; l++)
                        {

                            if (j == 0) { ls.Add(h.ToString()); h++; }
                            if (j > 0) { ls.Add(h.ToString()+h1.ToString()); }
                            h1++;
                            if (l == 21 && j == 0) { h = 'A'; break; }
                        }
                        if (j > 0) { h++; }
                    }
                //
                    int k = 6; 
                    for (int i = 0; i < totalplan1.Count; i++)
                    {
                      //  рамка для всех
                        int sb = 7, con = 0, con1 = 0;
                        for (int l = 0; l < 52; l++)
                        {
                            con++; con1++;
                           if (l != 0)
                           {
                               if (con1 == 16 || con1==24 || con1==36 || con1==44) { sb = sb + 9; con = 0; } else
                               if (con % 4==0 && con != 0 && con1!=8) { sb = sb + 10; con = 0;  } 
                                   else
                                   {
                                       sb = sb + 7;
                                   }
                           }
                           setBorderRight(wsh.Cells[ls[sb] + (i + 6).ToString()].Style.Border);
                        }
                        
                        if (totalplan1.Count - 1 == i) {
                            wsh.Cells["a" + k.ToString()].Style.Font.Bold = true;
                            wsh.Cells["b" + k.ToString()].Style.Font.Bold = true;
                        }
                        wsh.Cells["A" + k.ToString()].Value = totalplan1[i].plant_name;
                        wsh.Cells["B" + k.ToString()].Value = totalplan1[i].area;
                        wsh.Cells["B" + k.ToString()].Style.Numberformat.Format = "#,##0.00";
                        wsh.Cells["C" + k.ToString()].Style.Numberformat.Format = "#,##0.00";
                        wsh.Cells["D" + k.ToString()].Style.Numberformat.Format = "#,##0.00";
                        wsh.Cells["C" + k.ToString()].Value = totalplan1[i].productivity;
                        wsh.Cells["D" + k.ToString()].Value = totalplan1[i].gross_harvest;

                        for (int j = 0; j < months.Count; j++)
                        {
                            string color = null;
                       //ожидание
                       if (months[j].mat_type_id == 1) {   color="#8DB4E2";}  //посев
                       if (months[j].mat_type_id == 2) { 
                           color = "#92D050"; 
                       }  //уходные работы
                       if (months[j].mat_type_id == 3) { color = "#FFFF50"; }  //уборка
                       if (months[j].mat_type_id == 4) { color = "#948A54"; }  //почваподготовка
                       if (totalplan1.Count - 1 != i && months[j].id_tc == totalplan1[i].id_tc && months[j].mat_type_id!=null)
                            {
                                var date_start = (DateTime)months[j].date_start;
                                var date_finish = (DateTime)months[j].date_finish;

                                var vf = ls[date_start.DayOfYear] + (i + 6).ToString() + ":" + ls[date_finish.DayOfYear] + (i + 6).ToString();
                                setColor(wsh, ls[date_start.DayOfYear] + (i + 6).ToString() + ":" + ls[date_finish.DayOfYear] + (i + 6).ToString(), color);
                            }
                        }
                       
                      k++;
                    }
                //рамка для всех 
                   setBorder(wsh.Cells["a6:d" + (totalplan1.Count+5).ToString()].Style.Border);
                    setBorderTopAndBottom(wsh.Cells["e6:ne" + (totalplan1.Count + 5).ToString()].Style.Border);
                   setColor(wsh, "a" + (totalplan1.Count + 5).ToString() + ":ne" + (totalplan1.Count + 5).ToString(), "#D9D9D9"); //итого
                //обозначения
                   wsh.Cells["A" + (totalplan1.Count + 7).ToString()].Value = "Посев";
                   setColor(wsh, "A" + (totalplan1.Count + 7).ToString(), "#8DB4E2");

                   wsh.Cells["A" + (totalplan1.Count + 8).ToString()].Value = "Уходные работы";
                   setColor(wsh, "A" + (totalplan1.Count + 8).ToString(), "#92D050");

                   wsh.Cells["A" + (totalplan1.Count + 9).ToString()].Value = "Уборка";
                   setColor(wsh, "A" + (totalplan1.Count + 9).ToString(), "#FFFF50");

                   wsh.Cells["A" + (totalplan1.Count + 10).ToString()].Value = "Почвоподготовка активная";
                   setColor(wsh, "A" + (totalplan1.Count + 10).ToString(), "#948A54");

                   wsh.Cells["A" + (totalplan1.Count + 11).ToString()].Value = "Ожидание";
                   setColor(wsh, "A" + (totalplan1.Count + 11).ToString(), "#C4BD97");
                   setBorder(wsh.Cells["a" + (totalplan1.Count + 7).ToString() + ":" + "a" + (totalplan1.Count + 11).ToString()].Style.Border);

                var r1 = new ReportFile
                {
                    data = pck.GetAsByteArray(),
                    fileName = "План-график культур.xlsx"
                };
                return r1;
            }

        }

        //СЕБЕСТОИМОСТЬ ПЛАН-ФАКТ
        public ReportFile GetPlanFactDeviationExcel(DateTime startDate, DateTime endDate, int? plantId, string pathTemplate, string selDate)
        {
            var tmpl = new FileInfo(pathTemplate);
            using (ExcelPackage pck = new ExcelPackage(tmpl, true))
            {
                ExcelWorksheet wsh = pck.Workbook.Worksheets[1];
                wsh.Name = "План-факт отчет по культурам";
               var sevoborot = reportsModuleDb.getTotalNZP(startDate);
               var totalplan = new List<PlanRequestDto>();
               if (selDate.Equals("-1"))
               {
                    totalplan = reportsModuleDb.getTotalPlan(startDate, endDate, plantId, sevoborot);
               }
               else {
                   var selectDate = Convert.ToDateTime(selDate);
                   totalplan = reportsModuleDb.getTotalPlanSectionTechcard(startDate, endDate, plantId, sevoborot, selectDate);
               
               }

                var totalfact = reportsModuleDb.getTotalFact(startDate, endDate, plantId, sevoborot);
                wsh.Cells["a1"].Value = "План-факт отчет по культурам за период c " + startDate.ToString("dd.MM.yyyy") + " по " + endDate.ToString("dd.MM.yyyy");
                wsh.Cells["f4"].Value = "НЗП на " + startDate.Year + " год";
                int j3 = 7; int pl = 6; int ft = 7; int dv = 8;

                var plant = new List<DictionaryItemsDTO>();
                for (int i = 0; i < totalplan.Count; i++)
                {
                    plant.Add(new DictionaryItemsDTO
                    {
                        Id = totalplan[i].id_tc,
                        Name = totalplan[i].plant_name
                    });
                }
                for (int i = 0; i < totalfact.Count; i++)
                {
                    if (plant.Where(t => t.Id == totalfact[i].id_tc).Select(t => t.Id).FirstOrDefault() == null)
                    {
                        plant.Add(new DictionaryItemsDTO
                        {
                            Id = totalfact[i].id_tc,
                            Name = totalfact[i].plant_name
                        });
                    }
                }
                plant = plant.OrderBy(p => p.Name).ToList();

                totalplan = reportsModuleDb.sumTotalPlanFact(totalplan);
                totalfact = reportsModuleDb.sumTotalPlanFact(totalfact);

                string[] names = { "area", "productivity", "gross_harvest", "nzp", "salary", "dopsalary", "deduction", "deduction_watering", 
                "total_salary", "fuel_cost", "consumption", "fuel_cost_ga", "fuel_cost_watering", "consumption_watering", "fuel_cost_ga_watering", "total_consumption", "total_cost", "seed", "szr",
                "szr_ga", "chemicalFertilizers", "chemicalFertilizers_ga", "dop_material", 
                "dop_material_ga", "combine", "loader", "autotransport", "desiccation", "analyzes", "total_service", "total", "total_ga", "total_t"};

                string[] indexes = { "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "x", "y", "z", "aa", "ad", "ae", "ah", "ai", "aj", "ak", "al", "am", "an", "ao", "ap" };

                for (int i = 0; i < totalplan.Count; i++)
                {
                    for (int j = 0; j < totalfact.Count; j++)
                    {
                        if (totalplan[i].id_tc == totalfact[j].id_tc && totalplan[i].id_tc!=-1)
                        {
                            wsh.Cells["a" + j3.ToString()].Value = totalplan[i].plant_name;
                            wsh.Cells["b" + pl.ToString()].Value = "План";
                            wsh.Cells["b" + ft.ToString()].Value = "Факт";
                            wsh.Cells["b" + dv.ToString()].Value = "Отклонение(факт-план)";
                            char bt = 'c';
                            for (int v = 0; v < 7; v++)
                            {
                                setValueForPlanFact(bt.ToString(), pl.ToString(), ft.ToString(), dv.ToString(), i, Convert.ToDecimal((totalplan[i].GetType().GetProperty(names[v]).GetValue(totalplan[i], null) ?? 0).ToString()),
                                Convert.ToDecimal((totalfact[j].GetType().GetProperty(names[v]).GetValue(totalfact[j], null) ?? 0).ToString()), wsh);
                               
                                bt++;
                            }
                            setValueForPlanFact("j", pl.ToString(), ft.ToString(), dv.ToString(), i, (decimal?)(totalplan[i].salary_watering ?? 0) + (totalplan[i].dopsalary_watering ?? 0), (totalfact[j].salary_watering ?? 0) + (totalfact[j].dopsalary_watering ?? 0), wsh);

                            int v1 = 7;
                            for (int s = 0; s< indexes.Length; s++)
                            {
                                setValueForPlanFact(indexes[s], pl.ToString(), ft.ToString(), dv.ToString(), i, Convert.ToDecimal((totalplan[i].GetType().GetProperty(names[v1]).GetValue(totalplan[i], null) ?? 0).ToString()),
                                Convert.ToDecimal((totalfact[j].GetType().GetProperty(names[v1]).GetValue(totalfact[j], null) ?? 0).ToString()), wsh);
                                v1++;
                            }
                            totalplan[i].id_tc = -1; totalfact[j].id_tc = -1;
                            setBorderBoldTop(wsh.Cells["a" + pl.ToString() + ":" + "ap" + pl.ToString()].Style.Border);
                            j3 = j3 + 3; pl = pl + 3; ft = ft + 3; dv = dv + 3;
                        }
                    }
                }
                //остаток плана
                for (int i = 0; i < totalplan.Count; i++)
                {
                    if (totalplan[i].id_tc == -1) { continue; }
                    wsh.Cells["a" + j3.ToString()].Value = totalplan[i].plant_name;

                    wsh.Cells["b" + pl.ToString()].Value = "План";
                    wsh.Cells["b" + ft.ToString()].Value = "Факт";
                    wsh.Cells["b" + dv.ToString()].Value = "Отклонение(факт-план)";
                    char bt = 'c';
                    for (int v = 0; v < 7; v++)
                    {
                        setValueForPlanFact(bt.ToString(), pl.ToString(), ft.ToString(), dv.ToString(), i, Convert.ToDecimal((totalplan[i].GetType().GetProperty(names[v]).GetValue(totalplan[i], null) ?? 0).ToString()),
                        0, wsh);
                        bt++;
                    }
                    setValueForPlanFact("j", pl.ToString(), ft.ToString(), dv.ToString(), i, (decimal?)(totalplan[i].salary_watering ?? 0) + (totalplan[i].dopsalary_watering ?? 0),0, wsh);

                    int v1 = 7;
                    for (int s = 0; s < indexes.Length; s++)
                    {
                        setValueForPlanFact(indexes[s], pl.ToString(), ft.ToString(), dv.ToString(), i, Convert.ToDecimal((totalplan[i].GetType().GetProperty(names[v1]).GetValue(totalplan[i], null) ?? 0).ToString()),
                        0, wsh);
                        v1++;
                    }
                    setBorderBoldTop(wsh.Cells["a" + pl.ToString() + ":" + "ap" + pl.ToString()].Style.Border);
                    j3 = j3 + 3; pl = pl + 3; ft = ft + 3; dv = dv + 3;
                }
                //отстаток факта
                for (int j = 0; j < totalfact.Count; j++)
                {
                    if (totalfact[j].id_tc == -1) { continue; }
                    wsh.Cells["a" + j3.ToString()].Value = totalfact[j].plant_name;

                    wsh.Cells["b" + pl.ToString()].Value = "План";
                    wsh.Cells["b" + ft.ToString()].Value = "Факт";
                    wsh.Cells["b" + dv.ToString()].Value = "Отклонение(факт-план)";

                    char bt = 'c';
                    for (int v = 0; v < 7; v++)
                    {
                        setValueForPlanFact(bt.ToString(), pl.ToString(), ft.ToString(), dv.ToString(), j, 0,
                        Convert.ToDecimal((totalfact[j].GetType().GetProperty(names[v]).GetValue(totalfact[j], null) ?? 0).ToString()), wsh);
                        bt++;
                    }
                    setValueForPlanFact("j", pl.ToString(), ft.ToString(), dv.ToString(), j, 0, (totalfact[j].salary_watering ?? 0) + (totalfact[j].dopsalary_watering ?? 0), wsh);

                    int v1 = 7;
                    for (int s = 0; s < indexes.Length; s++)
                    {
                        setValueForPlanFact(indexes[s], pl.ToString(), ft.ToString(), dv.ToString(), j, 0,
                        Convert.ToDecimal((totalfact[j].GetType().GetProperty(names[v1]).GetValue(totalfact[j], null) ?? 0).ToString()), wsh);
                        v1++;
                    }
                    setBorderBoldTop(wsh.Cells["a" + pl.ToString() + ":" + "ap" + pl.ToString()].Style.Border);
                    j3 = j3 + 3; pl = pl + 3; ft = ft + 3; dv = dv + 3;
                }
           //     ИТОГО
                wsh.Cells["a" + j3.ToString()].Value = "Итого";
                wsh.Cells["b" + pl.ToString()].Value = "План";
                wsh.Cells["b" + ft.ToString()].Value = "Факт";
                wsh.Cells["b" + dv.ToString()].Value = "Отклонение(факт-план)";
                setBorderBoldTop(wsh.Cells["a" + (dv + 1).ToString() + ":" + "ap" + (dv + 1).ToString()].Style.Border);

                wsh.Cells["a" + (j3 - 1).ToString() + ":" + "ap" + dv.ToString()].Style.Font.Bold = true;
                setColor(wsh, "a" + (j3 - 1).ToString() + ":" + "ap" + dv.ToString(), "#d9d9d9");
                //
                char bt1 = 'c';
                for (int v = 0; v < 7; v++)
                {
                    if (!names[v].Equals("productivity") && !names[v].Equals("gross_harvest"))
                    {
                        setValueForPlanFact(bt1.ToString(), pl.ToString(), ft.ToString(), dv.ToString(), 0, Convert.ToDecimal((totalplan[totalplan.Count - 1].GetType().GetProperty(names[v]).GetValue(totalplan[totalplan.Count - 1], null) ?? 0).ToString()),
                        Convert.ToDecimal((totalfact[totalfact.Count - 1].GetType().GetProperty(names[v]).GetValue(totalfact[totalfact.Count - 1], null) ?? 0).ToString()), wsh);
                    }
                    bt1++;
                }
                setValueForPlanFact("j", pl.ToString(), ft.ToString(), dv.ToString(), 0, (decimal?)(totalplan[totalplan.Count - 1].salary_watering ?? 0) + (totalplan[totalplan.Count - 1].dopsalary_watering ?? 0), (totalfact[totalfact.Count - 1].salary_watering ?? 0) + (totalfact[totalfact.Count - 1].dopsalary_watering ?? 0), wsh);

                int v2 = 7;
                for (int s = 0; s < indexes.Length; s++)
                {
                    if (!names[v2].Equals("fuel_cost_ga") && !names[v2].Equals("fuel_cost_ga_watering") && !names[v2].Equals("szr_ga") && !names[v2].Equals("fuel_cost_ga_watering")
                        && !names[v2].Equals("chemicalFertilizers_ga") && !names[v2].Equals("dop_material_ga") && !names[v2].Equals("total_ga") && !names[v2].Equals("total_t"))
                    {
                        setValueForPlanFact(indexes[s], pl.ToString(), ft.ToString(), dv.ToString(), 0, Convert.ToDecimal((totalplan[totalplan.Count - 1].GetType().GetProperty(names[v2]).GetValue(totalplan[totalplan.Count - 1], null) ?? 0).ToString()),
                        Convert.ToDecimal((totalfact[totalfact.Count - 1].GetType().GetProperty(names[v2]).GetValue(totalfact[totalfact.Count - 1], null) ?? 0).ToString()), wsh);
                    }
                    v2++;
                }
                //
                setBorderBoldTop(wsh.Cells["a" + pl.ToString() + ":" + "ap" + pl.ToString()].Style.Border);
                j3 = j3 + 3; pl = pl + 3; ft = ft + 3; dv = dv + 3;
                ////

                wsh.Cells["a" + (j3+1).ToString()].Value = "Инвестиционные расходы на "+startDate.Year+" год";
                j3 = j3 + 2; pl = pl + 6; ft = ft + 6; dv = dv + 6;

                //копирование заголовков
                for (int j = 4; j < 6; j++)
                {
                    bt1 = 'a';
                    for (int v = 0; v < 26; v++)
                    {
                        wsh.Cells[bt1.ToString() + j3.ToString()].Value = wsh.Cells[bt1.ToString() + j.ToString()].Value;
                        bt1++;
                    }

                    v2 = 14;
                    for (int s = 0; s < indexes.Length - 14; s++)
                    {
                        wsh.Cells[indexes[v2].ToString() + j3.ToString()].Value = wsh.Cells[indexes[v2].ToString() + j.ToString()].Value;
                        v2++;
                    }
                    j3++;
                }
                 wsh.Cells["a" + (j3-2).ToString() + ":" + "ap" + (j3-1).ToString()].Style.Font.Bold = true;
                 setColor(wsh, "a" + (j3 - 2).ToString() + ":" + "ap" + (j3 - 1).ToString(), "#d9d9d9");
                 setBorder(wsh.Cells["a" + (j3 - 2).ToString() + ":" + "ap" + (j3 - 1).ToString()].Style.Border);
                  j3=j3+2;
                //вывод списка культур
                 for (int i = 0; i < plant.Count; i++)
                 {
                     wsh.Cells["a" + j3.ToString()].Value = plant[i].Name;
                     wsh.Cells["b" + pl.ToString()].Value = "План";
                     wsh.Cells["b" + ft.ToString()].Value = "Факт";
                     wsh.Cells["b" + dv.ToString()].Value = "Отклонение(факт-план)";
                     setBorderBoldTop(wsh.Cells["a" + pl.ToString() + ":" + "ap" + pl.ToString()].Style.Border);
                     j3 = j3 + 3; pl = pl + 3; ft = ft + 3; dv = dv + 3;
                 }
                 wsh.Cells["a" + j3.ToString()].Value = "Итого";
                 wsh.Cells["b" + pl.ToString()].Value = "План";
                 wsh.Cells["b" + ft.ToString()].Value = "Факт";
                 wsh.Cells["b" + dv.ToString()].Value = "Отклонение(факт-план)";
                 setBorderBoldTop(wsh.Cells["a" + pl.ToString() + ":" + "ap" + pl.ToString()].Style.Border);
                 setBorderBoldTop(wsh.Cells["a" + (dv + 1).ToString() + ":" + "ap" + (dv + 1).ToString()].Style.Border);
                 wsh.Cells["a" + (j3-1).ToString() + ":" + "ap" + (j3 + 1).ToString()].Style.Font.Bold = true;
                 setColor(wsh, "a" + (j3 - 1).ToString() + ":" + "ap" + (j3 + 1).ToString(), "#d9d9d9");
                var r = new ReportFile
                {
                    data = pck.GetAsByteArray(),
                    fileName = "План-факт отчет по культурам.xlsx"
                };
                return r;
            }

        }
        //
        //Антон--------------------------------------------------------------------------------------------------------------------------------------------------
        //ОТЧЕТЫ
        //Информация в формы
        public SpareReportsInfo GetRepairsReportsInfo()
        {
            SpareReportsInfo result = new SpareReportsInfo();

            //склады
            result.Storages = db.Storages.Select(s => new DictionaryItemsDTO
            {
                Id = s.store_id,
                Name = s.name,
            }).ToList();

            return result;
        }

        //Список возможных сотрудников, привлекавшихся к ежедневным нарядам за период
        public List<DictionaryItemsDTO> GetRespRepairsEmployees(DateTime start, DateTime end)
        {
            start = start.AddHours(3); end = end.AddHours(3);
            List<DictionaryItemsDTO> result = new List<DictionaryItemsDTO>();

            result = (from s in db.Daily_acts
                      where (DbFunctions.TruncateTime(s.date1) >= DbFunctions.TruncateTime(start) && DbFunctions.TruncateTime(s.date1) <= DbFunctions.TruncateTime(end)
                      && s.status == 2)
                      join k1 in db.Repairs_Tasks_Spendings on s.dai_id equals k1.dai_dai_id into left1
                      from t1 in left1.DefaultIfEmpty()
                      join k2 in db.Employees on t1.emp_emp_id equals k2.emp_id into left2
                      from t2 in left2.DefaultIfEmpty()
                      select new DictionaryItemsDTO
                      {
                          Id = t2.emp_id,
                          Name = t2.short_name
                      }).ToList();
            result = result.GroupBy(a => a.Id).Select(s => s.First()).ToList();

            if (result.Count != 0)
                result.Insert(0,new DictionaryItemsDTO { Id = 0, Name = "Все" });
            result.Remove(result.Where(v => v.Id == null).FirstOrDefault());
            return result;
        }

        public List<DictionaryItemsDTO> GetRespSheetsEmployees(DateTime start, DateTime end)
        {
            start = start.AddHours(3); end = end.AddHours(3);
            List<DictionaryItemsDTO> result = new List<DictionaryItemsDTO>();

            result = (from s in db.Sheet_tracks
                      where (DbFunctions.TruncateTime(s.date_begin) >= DbFunctions.TruncateTime(start) &&
                      DbFunctions.TruncateTime(s.date_end) <= DbFunctions.TruncateTime(end) )
                      select new DictionaryItemsDTO
                      {
                          Id = s.emp_emp_id,
                          Name = s.Employees.short_name,
                      }).ToList();
            result = result.GroupBy(a => a.Id).Select(s => s.First()).ToList();

            if (result.Count != 0)
                result.Insert(0, new DictionaryItemsDTO { Id = 0, Name = "Все" });
            result.Remove(result.Where(v => v.Id == null).FirstOrDefault());
            return result;
        }


        // список доступной техники и оборудования
        public RepairsReportsInfo GetRespRepairsTech(DateTime start, DateTime end)
        {
            RepairsReportsInfo result = new RepairsReportsInfo();

            result.Tech = (from s in db.Repairs_acts
                           where (DbFunctions.TruncateTime(s.date_start) >= DbFunctions.TruncateTime(start) && DbFunctions.TruncateTime(s.date_end) <= DbFunctions.TruncateTime(end))
                           join k1 in db.Traktors on s.trakt_trakt_id equals k1.trakt_id into left1
                           from t1 in left1.DefaultIfEmpty()
                           where (t1.trakt_id != null)
                           select new DictionaryItemsDTO
                           {
                               Id = t1.trakt_id,
                               Name = t1.name + " (" + t1.reg_num + ")"
                           }).ToList();
            result.Tech = result.Tech.GroupBy(a => a.Id).Select(s => s.First()).ToList();

            result.Eq = (from s in db.Repairs_acts
                         where (DbFunctions.TruncateTime(s.date_start) >= DbFunctions.TruncateTime(start) && DbFunctions.TruncateTime(s.date_end) <= DbFunctions.TruncateTime(end))
                         join k1 in db.Equipments on s.equi_equi_id equals k1.equi_id into left1
                         from t1 in left1.DefaultIfEmpty()
                         where (t1.equi_id != null)
                         select new DictionaryItemsDTO
                         {
                             Id = t1.equi_id,
                             Name = t1.name
                         }).ToList();
            result.Eq = result.Eq.GroupBy(a => a.Id).Select(s => s.First()).ToList();

            if (result.Tech.Count != 0)
            {
                result.Tech.Insert(0, new DictionaryItemsDTO { Id = -1, Name = "Все" });
                result.Tech.Insert(1, new DictionaryItemsDTO { Id = 0, Name = "Не выбрано" });

            }
            else {
                result.Tech.Add(new DictionaryItemsDTO { Id = 0, Name = "Не выбрано" });
            }

                if (result.Eq.Count != 0)
                {
                    result.Eq.Insert(0, new DictionaryItemsDTO { Id = -1, Name = "Все" });
                    result.Eq.Insert(1, new DictionaryItemsDTO { Id = 0, Name = "Не выбрано" });
                }
                else
                {
                    result.Eq.Add(new DictionaryItemsDTO { Id = 0, Name = "Не выбрано" });
                }
            return result;
        }

        //остатки по запчастям
        public ReportFile GetSparePartsBalanceListResExcel(string pathTemplate, int storage_id, Boolean storage, Boolean reserve)
        {
            var tmpl = new FileInfo(pathTemplate);
            decimal total = 0; string filename = "Остатки_по_запчастям.xlsx";
            using (ExcelPackage pck = new ExcelPackage(tmpl, true))
            {
                ExcelWorksheet wsh = pck.Workbook.Worksheets[1];
                wsh.Name = "Остатки по запчастям";
                var list = spareDb.GetSparePartsBalance(storage_id);
                if (list.Count == 0) { 
                var r1 = new ReportFile
                {
                    data = pck.GetAsByteArray(),
                    fileName = filename
                };
                return r1;
                }

                if (storage_id == 0)
                    wsh.Cells["B2"].Value = "Все";
                else
                    wsh.Cells["B2"].Value = db.Storages.Where(s => s.store_id == storage_id).Select(s => s.name).FirstOrDefault();

                uint k = 4;
                for (int i = 0; i < list.Count; i++)
                {
                    wsh.Cells["A" + k.ToString()].Value = list[i].Name;
                    wsh.Cells["B" + k.ToString()].Value = list[i].VendorCode1;
                    wsh.Cells["C" + k.ToString()].Value = list[i].Manufacturer;
                    wsh.Cells["D" + k.ToString()].Value = list[i].Storage1;
                    wsh.Cells["E" + k.ToString()].Value = list[i].Count;
                  
                    if (!reserve & !storage)
                    {
                        wsh.Cells["F" + k.ToString()].Value = list[i].Storage2 ?? 0;
                        wsh.Cells["G" + k.ToString()].Value = list[i].Reserve ?? 0;
                        wsh.Cells["H" + k.ToString()].Value = list[i].Price;
                        total = total + (decimal)((list[i].Price ?? 0) * (list[i].Count ?? 0));
                        wsh.Cells["I" + k.ToString()].Value = list[i].Price * list[i].Count;
                    }
                    if (reserve) {
                        wsh.Cells["e" + k.ToString()].Value = list[i].Reserve ?? 0;
                        wsh.Cells["f" + k.ToString()].Value = list[i].Price;
                        total = total + (decimal)(list[i].Price * (list[i].Reserve ?? 0));
                        wsh.Cells["g" + k.ToString()].Value = list[i].Price * (list[i].Reserve ?? 0);
                        wsh.Cells["a1"].Value = "В резерве";
                        wsh.Cells["e3"].Value = "В резерве";
                        filename = "В резерве.xlsx";
                    }
                    if (storage)
                    {
                        total = total + (decimal)(list[i].Price * (list[i].Storage2 ?? 0));
                        wsh.Cells["e" + k.ToString()].Value = list[i].Storage2 ?? 0;
                        wsh.Cells["f" + k.ToString()].Value = list[i].Price;
                        wsh.Cells["g" + k.ToString()].Value = list[i].Price * (list[i].Storage2 ?? 0);
                        filename = "На складе.xlsx";
                    }
                    k++;
                }
                if (list.Count > 0)
                    if (!reserve & !storage)
                    {
                        wsh.Cells["h" + k.ToString()].Value = "Итого:";
                        wsh.Cells["i" + k.ToString()].Value = total;
                        setBorder(wsh.Cells["A4:I" + (k - 1).ToString()].Style.Border);
                    }
                if (reserve || storage) {
                    wsh.Cells["f" + k.ToString()].Value = "Итого:";
                    wsh.Cells["g" + k.ToString()].Value = total;
                    setBorder(wsh.Cells["A4:g" + (k - 1).ToString()].Style.Border);
                }
                var r = new ReportFile
                {
                    data = pck.GetAsByteArray(),
                    fileName = filename
                };
                return r;
            }
        }

        //расход по запчастям
        public ReportFile GetSparePartsIntakeListResExcel(string pathTemplate, int storage_id, DateTime start, DateTime end)
        {
            var tmpl = new FileInfo(pathTemplate);
            using (ExcelPackage pck = new ExcelPackage(tmpl, true))
            {
                ExcelWorksheet wsh = pck.Workbook.Worksheets[1];
                wsh.Name = "Расход по запчастям";
                var list = spareDb.GetSparePartsIntake(storage_id, start, end);

                if (storage_id == 0)
                    wsh.Cells["B2"].Value = "Все";
                else
                    wsh.Cells["B2"].Value = db.Storages.Where(s => s.store_id == storage_id).Select(s => s.name).FirstOrDefault();
                wsh.Cells["D2"].Value = start.ToString("dd.MM.yyyy");
                wsh.Cells["F2"].Value = end.ToString("dd.MM.yyyy");

                uint k = 4;
                double sum = 0;
                for (int i = 0; i < list.Count; i++)
                {
                    wsh.Cells["A" + k.ToString()].Value = list[i].DateBegin1;
                    wsh.Cells["B" + k.ToString()].Value = list[i].Act_number;
                    wsh.Cells["C" + k.ToString()].Value = list[i].Name;
                    wsh.Cells["D" + k.ToString()].Value = list[i].VendorCode1;
                    wsh.Cells["E" + k.ToString()].Value = list[i].Storage1;
                    wsh.Cells["F" + k.ToString()].Value = list[i].Count;
                    wsh.Cells["G" + k.ToString()].Value = list[i].Price ?? 0;
                    wsh.Cells["H" + k.ToString()].Value = list[i].Price * list[i].Count;
                    sum += list[i].Price.Value * list[i].Count.Value;
                    k++;
                }
                if (list.Count > 0)
                    setBorder(wsh.Cells["A4:H" + (k - 1).ToString()].Style.Border);
                wsh.Cells["G" + k.ToString()].Value = "Итого сумма:";
                wsh.Cells["H" + k.ToString()].Value = sum;
                if (list.Count > 0)
                    setBorder(wsh.Cells["G" + k.ToString() + ":H" + k.ToString()].Style.Border);

                var r = new ReportFile
                {
                    data = pck.GetAsByteArray(),
                    fileName = "Расход_по_запчастям.xlsx"
                };
                return r;
            }
        }

        //Учет заработной платы (обычный)
        public ReportFile GetRepairsSpendingsListResExcel(string pathTemplate, int emp_id, DateTime start, DateTime end, int isSheet)
        {
            var tmpl = new FileInfo(pathTemplate);
            using (ExcelPackage pck = new ExcelPackage(tmpl, true))
            {
                ExcelWorksheet wsh = pck.Workbook.Worksheets[1];
                wsh.Name = "Учет заработной платы";
                var list = spareDb.GetRepairsSpendingsListResExcel(emp_id, start, end);

              var  list2 = (from f in db.Tasks where
                            DbFunctions.TruncateTime(f.Sheet_tracks.date_begin) >= start 
                            && DbFunctions.TruncateTime(f.Sheet_tracks.date_begin) <= end
                                && (emp_id != 0 ? f.Sheet_tracks.emp_emp_id == emp_id : true)
                         group f by new { f.Sheet_tracks.emp_emp_id, f.Sheet_tracks.Employees.short_name } into g
                         select new DictionaryItemsDTO
                         {
                             Id = g.Key.emp_emp_id,
                             Current_price = g.Sum(t => t.total_salary ?? 0),
                             Name = g.Key.short_name,
                         }).ToList();

                wsh.Cells["A2"].Value = "С: " + start.ToString("dd.MM.yyyy");
                wsh.Cells["B2"].Value = "По: " + end.ToString("dd.MM.yyyy");
                if (isSheet == 3) {
                    wsh.Cells["B3"].Value = "Начислено Агрогараж, руб";
                    wsh.Cells["c3"].Value = "Начислено Растениеводство, руб";
                    wsh.Cells["d3"].Value = "Начислено Итого, руб";
                    setBorder(wsh.Cells["C3:D3"].Style.Border);
                }
                if (isSheet == 2) {
                    wsh.Cells["b3"].Value = "Начислено Растениеводство, руб";
                }

                uint k = 4;
                double sum = 0; decimal sum2 = 0; 
                for (int i = 0; i < list.Count; i++)
                {
                    wsh.Cells["A" + k.ToString()].Value = list[i].Name;

                    if (isSheet != 2)
                    {
                        wsh.Cells["B" + k.ToString()].Value = list[i].Price ?? 0;
                    }
                    //ПЛ
                    if (isSheet == 2)
                    {
                        var total = list2.Where(t => t.Id == list[i].Id).FirstOrDefault();
                        if (total != null && total.Current_price != 0)
                        {
                            wsh.Cells["B" + k.ToString()].Value = total.Current_price;
                            sum2 += total.Current_price ?? 0;
                            total.Id = -1;
                        }
                    }


                    if (isSheet == 3) {
                        var total = list2.Where(t => t.Id == list[i].Id).FirstOrDefault();
                        wsh.Cells["d" + k.ToString()].Value = (decimal?)(list[i].Price ?? 0)
                            + (total != null ? (total.Current_price ?? 0) : 0 );

                        if (total != null && total.Current_price != 0)
                        {
                           wsh.Cells["C" + k.ToString()].Value = total.Current_price;
                           sum2 += total.Current_price ?? 0;
                           total.Id = -1;
                        }
                    }

                    sum += list[i].Price ?? 0;
                    k++;
                }
                //только ПЛ
                if (isSheet == 2)
                {
                    list2 = list2.Where(t => t.Id != -1).ToList();
                    for (int i = 0; i < list2.Count; i++)
                    {
                        if (list2[i].Current_price != 0)
                        {
                            wsh.Cells["A" + k.ToString()].Value = list2[i].Name;
                            wsh.Cells["B" + k.ToString()].Value = list2[i].Current_price;
                            sum2 += (list2[i].Current_price ?? 0);
                            k++;
                        }
                    }
                }

                if (isSheet == 3)
                {
                    list2 = list2.Where(t => t.Id != -1).ToList();
                    for (int i = 0; i < list2.Count; i++)
                    {
                        if (list2[i].Current_price != 0)
                        {
                            wsh.Cells["A" + k.ToString()].Value = list2[i].Name;
                            wsh.Cells["C" + k.ToString()].Value = list2[i].Current_price;
                            wsh.Cells["D" + k.ToString()].Value = (list2[i].Current_price ?? 0);

                            sum2 += (list2[i].Current_price ?? 0);
                            k++;
                        }
                    }
                }

                if (list.Count > 0 || list2.Count > 0)
                {
                    if (isSheet != 3)
                    {
                        setBorder(wsh.Cells["A4:B" + (k - 1).ToString()].Style.Border);
                    }
                    else {
                        setBorder(wsh.Cells["A4:D" + (k - 1).ToString()].Style.Border);
                    }
                }
                if (list.Count > 1 || list2.Count > 1)
                {
                    wsh.Cells["A" + k.ToString()].Value = "Итого сумма:";
                    if (isSheet != 2)
                    {
                        wsh.Cells["B" + k.ToString()].Value = sum;
                    }
                    else {
                        wsh.Cells["B" + k.ToString()].Value = sum2;
                    }
                    if (isSheet != 3)
                    {
                        setBorder(wsh.Cells["A" + k.ToString() + ":B" + k.ToString()].Style.Border);
                        SetCellStyle(wsh.Cells["A" + k.ToString() + ":B" + k.ToString()], Color.Aqua);
                    }
                    else {
                        wsh.Cells["C" + k.ToString()].Value = sum2;
                        wsh.Cells["d" + k.ToString()].Value = (decimal)sum + sum2;
                        setBorder(wsh.Cells["A" + k.ToString() + ":D" + k.ToString()].Style.Border);
                        SetCellStyle(wsh.Cells["A" + k.ToString() + ":D" + k.ToString()], Color.Aqua);
                    }
                   
                }
                var r = new ReportFile
                {
                    data = pck.GetAsByteArray(),
                    fileName = "Учет заработной платы.xlsx"
                };
                return r;
            }
        }

        //Учет заработной платы (расширенный)
        public ReportFile GetRepairsSpendingsBigListResExcel(string pathTemplate, int emp_id, DateTime start, DateTime end)
        {
            var tmpl = new FileInfo(pathTemplate);
            using (ExcelPackage pck = new ExcelPackage(tmpl, true))
            {
                ExcelWorksheet wsh = pck.Workbook.Worksheets[1];
                wsh.Name = "Учет заработной платы";
                var list = spareDb.GetRepairsSpendingsBigListResExcel(emp_id, start, end);

                wsh.Cells["A2"].Value = "С: " + start.ToString("dd.MM.yyyy");
                wsh.Cells["B2"].Value = "По: " + end.ToString("dd.MM.yyyy");

                uint k = 4;
                double sum = 0;
                for (int i = 0; i < list.Count; i++)
                {
                    wsh.Cells["A" + k.ToString()].Value = list[i].ActNumber;
                    wsh.Cells["B" + k.ToString()].Value = list[i].Date1.Value.ToString("dd.MM.yyyy");
                    wsh.Cells["C" + k.ToString()].Value = list[i].EmpName;
                    wsh.Cells["D" + k.ToString()].Value = list[i].Trakt;
                    wsh.Cells["E" + k.ToString()].Value = list[i].Agr;
                    wsh.Cells["F" + k.ToString()].Value = list[i].TaskName;
                    wsh.Cells["G" + k.ToString()].Value = list[i].SumCost ?? 0;
                    sum += (double)(list[i].SumCost ?? 0);
                    k++;
                }
                if (list.Count > 0)
                    setBorder(wsh.Cells["A4:G" + (k - 1).ToString()].Style.Border);
                if (list.Count > 1)
                {
                    wsh.Cells["F" + k.ToString()].Value = "Итого сумма:";
                    wsh.Cells["G" + k.ToString()].Value = sum;
                    setBorder(wsh.Cells["F" + k.ToString() + ":G" + k.ToString()].Style.Border);
                    SetCellStyle(wsh.Cells["F" + k.ToString() + ":G" + k.ToString()], Color.Aqua);
                }
                var r = new ReportFile
                {
                    data = pck.GetAsByteArray(),
                    fileName = "Учет заработной платы.xlsx"
                };
                return r;
            }
        }

        // затраты на ремонт
        public ReportFile GetRepairsSparePartsSpendingsListResExcel(string pathTemplate, int tech_id, int equi_id, DateTime start, DateTime end)
        {
            var tmpl = new FileInfo(pathTemplate);
            using (ExcelPackage pck = new ExcelPackage(tmpl, true))
            {
                ExcelWorksheet wsh = pck.Workbook.Worksheets[1];
                wsh.Name = "Затраты на ремонт";
                List<SparePartsModel> list = spareDb.GetSparePartsRepairsSpendings(tech_id, equi_id, start, end);
                if (tech_id == -1)
                    wsh.Cells["B2"].Value = "Все";
                else
                    wsh.Cells["B2"].Value = db.Traktors.Where(s => s.trakt_id == tech_id).Select(s => s.name + " - " + s.model + " (" + s.reg_num + ")").FirstOrDefault();
                if (equi_id == -1)
                    wsh.Cells["B2"].Value = "Все";
                else
                    wsh.Cells["B2"].Value = db.Equipments.Where(s => s.equi_id == equi_id).Select(s => s.name).FirstOrDefault();
                wsh.Cells["B3"].Value = start.ToString("dd.MM.yyyy");
                wsh.Cells["D3"].Value = end.ToString("dd.MM.yyyy");

                uint k = 5;
                double sum = 0;
                for (int i = 0; i < list.Count; i++)
                {
                    string techName = "";
                    if (list[i].TechName != null && list[i].TechName != "")
                    {
                        techName = list[i].TechName;
                        if (list[i].EqName != null && list[i].EqName != "")
                            techName += " / " + list[i].EqName;
                    }
                    else
                        if (list[i].EqName != null && list[i].EqName != "")
                            techName = list[i].EqName;
                    wsh.Cells["A" + k.ToString()].Value = techName;
                    wsh.Cells["B" + k.ToString()].Value = list[i].Name;
                    wsh.Cells["C" + k.ToString()].Value = list[i].VendorCode1;
                    wsh.Cells["D" + k.ToString()].Value = list[i].Count;
                    wsh.Cells["E" + k.ToString()].Value = list[i].Price;
                    wsh.Cells["F" + k.ToString()].Value = list[i].Price * list[i].Count;
                    sum += list[i].Price.Value * list[i].Count.Value;
                    k++;
                }
                if (list.Count > 0)
                    setBorder(wsh.Cells["A5:F" + (k - 1).ToString()].Style.Border);
                wsh.Cells["E" + k.ToString()].Value = "Итого сумма:";
                wsh.Cells["F" + k.ToString()].Value = sum;
                if (list.Count > 0)
                    setBorder(wsh.Cells["E" + k.ToString() + ":F" + k.ToString()].Style.Border);

                var r = new ReportFile
                {
                    data = pck.GetAsByteArray(),
                    fileName = "Затраты_на_ремонт.xlsx"
                };
                return r;
            }
        }

        // реестр документов
        public ReportFile GetSpareRegistryResExcel(string pathTemplate, DateTime start, DateTime end)
        {
            var tmpl = new FileInfo(pathTemplate);
            using (ExcelPackage pck = new ExcelPackage(tmpl, true))
            {
                //реестр приходных накладных
                ExcelWorksheet wsh = pck.Workbook.Worksheets[1];
                wsh.Name = "Реестр приходных накладных";
                List<SparePartsItem> list = spareDb.GetInvoiceActsRegistry(start, end);

                wsh.Cells["B2"].Value = start.ToString("dd.MM.yyyy");
                wsh.Cells["D2"].Value = end.ToString("dd.MM.yyyy");

                uint k = 4;
                double sum = 0;
                for (int i = 0; i < list.Count; i++)
                {
                    wsh.Cells["A" + k.ToString()].Value = list[i].DateBegin;
                    wsh.Cells["B" + k.ToString()].Value = list[i].Number;
                    wsh.Cells["C" + k.ToString()].Value = list[i].Contractor.Name;
                    wsh.Cells["D" + k.ToString()].Value = list[i].Cost;
                    sum += list[i].Cost ?? 0;
                    k++;
                }
                if (list.Count > 0)
                    setBorder(wsh.Cells["A4:D" + (k - 1).ToString()].Style.Border);
                wsh.Cells["C" + k.ToString()].Value = "Итого сумма:";
                wsh.Cells["D" + k.ToString()].Value = sum;
                if (list.Count > 0)
                    setBorder(wsh.Cells["C" + k.ToString() + ":D" + k.ToString()].Style.Border);

                //реестр расходных накладных
                ExcelWorksheet wsh1 = pck.Workbook.Worksheets[2];
                wsh1.Name = "Реестр расходных накладных";
                var list2 = spareDb.GetSpendingActsRegistry(start, end);

                wsh1.Cells["B2"].Value = start.ToString("dd.MM.yyyy");
                wsh1.Cells["D2"].Value = end.ToString("dd.MM.yyyy");

                k = 4;
                sum = 0;
                for (int i = 0; i < list2.Count; i++)
                {
                    wsh1.Cells["A" + k.ToString()].Value = list2[i].DateBegin.Value.ToString("dd.MM.yyyy");
                    wsh1.Cells["B" + k.ToString()].Value = list2[i].Act_number;
                    wsh1.Cells["C" + k.ToString()].Value = list2[i].TechName;
                    wsh1.Cells["D" + k.ToString()].Value = list2[i].Cost3;
                    sum += (double)list2[i].Cost3;
                    k++;
                }
                if (list2.Count > 0)
                    setBorder(wsh1.Cells["A4:D" + (k - 1).ToString()].Style.Border);
                wsh1.Cells["C" + k.ToString()].Value = "Итого сумма:";
                wsh1.Cells["D" + k.ToString()].Value = sum;
                if (list2.Count > 0)
                    setBorder(wsh1.Cells["C" + k.ToString() + ":D" + k.ToString()].Style.Border);

                // реестр нарядов 
                ExcelWorksheet wsh2 = pck.Workbook.Worksheets[3];
                wsh2.Name = "Реестр нарядов";
                List<RepairsItem> list1 = spareDb.GetRepairsActsRegistry(start, end);

                wsh2.Cells["B2"].Value = start.ToString("dd.MM.yyyy");
                wsh2.Cells["D2"].Value = end.ToString("dd.MM.yyyy");

                k = 4;
                sum = 0;
                for (int i = 0; i < list1.Count; i++)
                {
                    wsh2.Cells["A" + k.ToString()].Value = list1[i].DateOpen;
                    wsh2.Cells["B" + k.ToString()].Value = list1[i].DateBegin;
                    wsh2.Cells["C" + k.ToString()].Value = list1[i].DateEnd;
                    wsh2.Cells["D" + k.ToString()].Value = list1[i].Number;
                    wsh2.Cells["E" + k.ToString()].Value = list1[i].CarsName;
                    wsh2.Cells["F" + k.ToString()].Value = list1[i].Cost;
                    wsh2.Cells["G" + k.ToString()].Value = list1[i].Status;
                    sum += list1[i].Cost;
                    k++;
                }
                if (list1.Count > 0)
                    setBorder(wsh2.Cells["A4:G" + (k - 1).ToString()].Style.Border);
                wsh2.Cells["E" + k.ToString()].Value = "Итого сумма:";
                wsh2.Cells["F" + k.ToString()].Value = sum;
                if (list1.Count > 0)
                    setBorder(wsh2.Cells["E" + k.ToString() + ":F" + k.ToString()].Style.Border);

                var r = new ReportFile
                {
                    data = pck.GetAsByteArray(),
                    fileName = "Реестр_документов.xlsx"
                };
                return r;
            }
        }

        //План-график ремонтных работ
        public ReportFile GetPlanChartTechResExcel(string pathTemplate, int? techId, int? eqId, int? year)
        {
            var tmpl = new FileInfo(pathTemplate);
            using (ExcelPackage pck = new ExcelPackage(tmpl, true))
            {
                ExcelWorksheet wsh = pck.Workbook.Worksheets[1];
                wsh.Name = "Техника";
                wsh.Cells["A1"].Value = "План-графики ремонтных работ за " + year + " год.";
                ExcelWorksheet wsh1 = pck.Workbook.Worksheets[2];
                wsh1.Name = "Оборудование";
                wsh1.Cells["A1"].Value = "План-графики ремонтных работ за " + year + " год.";

                var TraktList = db.TC_operation_to_Tech.Where(w => w.TC_operation.date_start.Value.Year == year.Value
                                                            && ((w.trakt_trakt_id.HasValue && w.trakt_trakt_id != null && techId != 0 && techId != null) ? w.Traktors.tptrak_tptrak_id == techId.Value : (w.trakt_trakt_id.HasValue && w.trakt_trakt_id != null))
                                                            ).OrderBy(o => o.TC_operation.date_start).GroupBy(
                                                                g => g.trakt_trakt_id,
                                                                g1 => new
                                                                {
                                                                    Id = g1.TC_operation.id,
                                                                    Name = g1.TC_operation.TC.Plants.name + ": " + g1.TC_operation.Type_tasks.name,
                                                                    Date1 = g1.TC_operation.date_start,
                                                                    Date2 = g1.TC_operation.date_finish
                                                                }
                                                            ).Select(s => new
                                                            {
                                                                Id = s.Key,
                                                                Name = db.Traktors.Where(w => w.trakt_id == s.Key).Select(
                                                                        s1 => s1.name + " (" + s1.reg_num + ")").FirstOrDefault().ToString(),
                                                                Operations = s.ToList(),
                                                                Repairs = db.Repairs_acts.Where(
                                                                        w => w.trakt_trakt_id == s.Key && w.is_require != 1).Select(
                                                                        s2 => new { DateStart = s2.date_start, DateEnd = s2.date_end, DateOpen = s2.date_open }).ToList()
                                                            }).ToList();

                var EquiList = db.TC_operation_to_Tech.Where(w => w.TC_operation.date_start.Value.Year == year.Value
                                            && ((w.equi_equi_id.HasValue && w.equi_equi_id != null && eqId != 0 && eqId != null  ) ? w.Equipments.tpequi_tpequi_id == eqId.Value : (w.equi_equi_id.HasValue && w.equi_equi_id != null))
                                            ).OrderBy(o => o.TC_operation.date_start).GroupBy(
                                                g => g.equi_equi_id,
                                                g1 => new
                                                {
                                                    Id = g1.TC_operation.id,
                                                    Name = g1.TC_operation.TC.Plants.name + ": " + g1.TC_operation.Type_tasks.name,
                                                    Date1 = g1.TC_operation.date_start,
                                                    Date2 = g1.TC_operation.date_finish
                                                }
                                            ).Select(s => new
                                            {
                                                Id = s.Key,
                                                Name = db.Equipments.Where(
                                                        w => w.equi_id == s.Key).Select(s1 => s1.name).FirstOrDefault().ToString(),
                                                Operations = s.ToList(),
                                                Repairs = db.Repairs_acts.Where(
                                                        w => w.equi_equi_id == s.Key && w.is_require != 1).Select(
                                                        s2 => new { DateStart = s2.date_start, DateEnd = s2.date_end, DateOpen = s2.date_open }).ToList()
                                            }).ToList();

                //закраска по месяцам
                string colorMain = "#777777";
                string colorWork = "#8DB4E2";
                string colorRepairs = "#CD5C5C";

                //столбцы для всех дней;
                var ls = new List<string>(); ls.Add("");
                char h = 'A'; var h1 = 'A';
                for (int j = 0; j < DateTime.Now.DayOfYear; j++)
                {
                    if (j == 0) { h = 'B'; } h1 = 'A';
                    for (int l = 0; l < 26; l++)
                    {
                        if (j == 0) { ls.Add(h.ToString()); h++; }
                        if (j > 0) { ls.Add(h.ToString() + h1.ToString()); }
                        h1++;
                        if (l == 24 && j == 0) { h = 'A'; break; }
                    }
                    if (j > 0) { h++; }
                }

                //ТЕХНИКА
                int k = 6;
                int tmp = 0;
                foreach (var t in TraktList)
                {
                    // вывод строки техники
                    tmp = k;
                    wsh.Cells["A" + k.ToString()].Value = t.Name;
                    wsh.Cells["A" + k.ToString()].Style.Font.Bold = true;
                    setColor(wsh, "A" + (k).ToString() + ":NB" + (k).ToString(), "#AFEEEE");
                    //границы недель
                    SetWeekBorders(ls, wsh, k);
                    k++;
                    for (int i = 1; i <= t.Operations.Count; i++)
                    {
                        //границы недель
                        SetWeekBorders(ls, wsh, i + tmp);
                        wsh.Cells["A" + k.ToString()].Value = t.Operations[i - 1].Name;
                        var date_start = (DateTime)t.Operations[i - 1].Date1;
                        var date_finish = (DateTime)t.Operations[i - 1].Date2;

                        // вывод подстрок работ
                        if (date_start.Year < date_finish.Year)
                        {
                            var lastDay =  new DateTime(date_start.Year, 12, 31);
                            setColor(wsh, ls[date_start.DayOfYear] + (i + tmp).ToString() + ":" + ls[lastDay.DayOfYear] + (i + tmp).ToString(), colorWork);
                            // заполнение общей строки
                            setColor(wsh, ls[date_start.DayOfYear] + (tmp).ToString() + ":" + ls[lastDay.DayOfYear] + (tmp).ToString(), colorMain);
                        }
                        else
                        {
                            setColor(wsh, ls[date_start.DayOfYear] + (i + tmp).ToString() + ":" + ls[date_finish.DayOfYear] + (i + tmp).ToString(), colorWork);
                            // заполнение общей строки
                            setColor(wsh, ls[date_start.DayOfYear] + (tmp).ToString() + ":" + ls[date_finish.DayOfYear] + (tmp).ToString(), colorMain);
                        }

                        k++;
                    }
                    // заполнение ремонтных интервалов - техника
                    for (int i = 0; i < t.Repairs.Count; i++)
                    {
                        var lastDay = new DateTime(t.Repairs[i].DateStart.Year, 12, 31);
                        var firstDay = new DateTime(year.Value, 1, 1);
                        if (t.Repairs[i].DateOpen.HasValue && t.Repairs[i].DateEnd.HasValue)
                        {
                            //проверка года
                            if (t.Repairs[i].DateOpen.Value.Year < t.Repairs[i].DateEnd.Value.Year && t.Repairs[i].DateOpen.Value.Year == year)
                            {
                                setColor(wsh, ls[t.Repairs[i].DateOpen.Value.DayOfYear] + (tmp).ToString() + ":" + ls[lastDay.DayOfYear] + (tmp).ToString(), colorRepairs);
                            }
                            else
                            {
                                //c первого дня по сегодня
                                if (t.Repairs[i].DateOpen.Value.Year < t.Repairs[i].DateEnd.Value.Year && t.Repairs[i].DateEnd.Value.Year == year)
                                {
                                    setColor(wsh, ls[firstDay.DayOfYear] + (tmp).ToString() + ":" + ls[t.Repairs[i].DateEnd.Value.DayOfYear] + (tmp).ToString(), colorRepairs);
                                }
                                else
                                {
                                    if (t.Repairs[i].DateOpen.Value.Year == year)
                                    {
                                        setColor(wsh, ls[t.Repairs[i].DateOpen.Value.DayOfYear] + (tmp).ToString() + ":" + ls[t.Repairs[i].DateEnd.Value.DayOfYear] + (tmp).ToString(), colorRepairs);
                                    }
                                }
                            }

                        }
                        if (!t.Repairs[i].DateOpen.HasValue || !t.Repairs[i].DateEnd.HasValue)
                        {  
                            //проверка года
                            if (t.Repairs[i].DateStart.Year < DateTime.Now.Year && t.Repairs[i].DateStart.Year == year)
                            {
                                setColor(wsh, ls[t.Repairs[i].DateStart.DayOfYear] + (tmp).ToString() + ":" + ls[lastDay.DayOfYear] + (tmp).ToString(), colorRepairs);
                            } else {
                                     if (t.Repairs[i].DateStart.Year < DateTime.Now.Year && DateTime.Now.Year == year) {
                                     setColor(wsh, ls[firstDay.DayOfYear] + (tmp).ToString() + ":" + ls[DateTime.Now.DayOfYear] + (tmp).ToString(), colorRepairs);   
                                  }
                            else
                            {
                                setColor(wsh, ls[t.Repairs[i].DateStart.DayOfYear] + (tmp).ToString() + ":" + ls[DateTime.Now.DayOfYear] + (tmp).ToString(), colorRepairs);
                            }
                            }
                        }


                    }
                }

                //рамка для всех 
                if (k > 6)
                {
                    setBorder(wsh.Cells["a6:a" + (k - 1).ToString()].Style.Border);
                    setBorderTopAndBottom(wsh.Cells["b6:nb" + (k - 1).ToString()].Style.Border);
                }

                //обозначения
                wsh.Cells["A" + (k + 2).ToString()].Value = "Общая занятость техники";
                setColor(wsh, "A" + (k + 2).ToString(), colorMain);
                wsh.Cells["A" + (k + 3).ToString()].Value = "Занятость техники в конкретной работе";
                setColor(wsh, "A" + (k + 3).ToString(), colorWork);
                wsh.Cells["A" + (k + 4).ToString()].Value = "Техника на ремонте";
                setColor(wsh, "A" + (k + 4).ToString(), colorRepairs);
                setBorder(wsh.Cells["a" + (k + 2).ToString() + ":" + "a" + (k + 4).ToString()].Style.Border);

                //ОБОРУДОВАНИЕ
                k = 6;
                tmp = 0;
                foreach (var t in EquiList)
                {
                    // вывод строки оборудования
                    tmp = k;
                    wsh1.Cells["A" + k.ToString()].Value = t.Name;
                    wsh1.Cells["A" + k.ToString()].Style.Font.Bold = true;
                    setColor(wsh1, "A" + (k).ToString() + ":NB" + (k).ToString(), "#AFEEEE");
                    //границы недель
                    SetWeekBorders(ls, wsh1, k);
                    k++;
                    for (int i = 1; i <= t.Operations.Count; i++)
                    {
                        //границы недель
                        SetWeekBorders(ls, wsh1, i + tmp);
                        wsh1.Cells["A" + k.ToString()].Value = t.Operations[i - 1].Name;
                        var date_start = (DateTime)t.Operations[i - 1].Date1;
                        var date_finish = (DateTime)t.Operations[i - 1].Date2;
                        //
                        // вывод подстрок работ
                        if (date_start.Year < date_finish.Year)
                        {
                            var lastDay = new DateTime(date_start.Year, 12, 31);
                            setColor(wsh1, ls[date_start.DayOfYear] + (i + tmp).ToString() + ":" + ls[lastDay.DayOfYear] + (i + tmp).ToString(), colorWork);
                            // заполнение общей строки
                            setColor(wsh1, ls[date_start.DayOfYear] + (tmp).ToString() + ":" + ls[lastDay.DayOfYear] + (tmp).ToString(), colorMain);
                        }
                        else
                        {
                            setColor(wsh1, ls[date_start.DayOfYear] + (i + tmp).ToString() + ":" + ls[date_finish.DayOfYear] + (i + tmp).ToString(), colorWork);
                            // заполнение общей строки
                            setColor(wsh1, ls[date_start.DayOfYear] + (tmp).ToString() + ":" + ls[date_finish.DayOfYear] + (tmp).ToString(), colorMain);
                        }
                        //// вывод подстрок работ
                        //setColor(wsh1, ls[date_start.DayOfYear] + (i + tmp).ToString() + ":" + ls[date_finish.DayOfYear] + (i + tmp).ToString(), colorWork);
                        //// заполнение общей строки
                        //setColor(wsh1, ls[date_start.DayOfYear] + (tmp).ToString() + ":" + ls[date_finish.DayOfYear] + (tmp).ToString(), colorMain);

                        k++;
                    }
                    // заполнение ремонтных интервалов - оборудование
                    for (int i = 0; i < t.Repairs.Count; i++)
                    {
                        var lastDay = new DateTime(t.Repairs[i].DateStart.Year, 12, 31);
                        var firstDay = new DateTime(year.Value, 1, 1);
                        //
                        if (t.Repairs[i].DateOpen.HasValue && t.Repairs[i].DateEnd.HasValue)
                        {
                            //проверка года
                            if (t.Repairs[i].DateOpen.Value.Year < t.Repairs[i].DateEnd.Value.Year && t.Repairs[i].DateOpen.Value.Year == year)
                            {
                                setColor(wsh1, ls[t.Repairs[i].DateOpen.Value.DayOfYear] + (tmp).ToString() + ":" + ls[lastDay.DayOfYear] + (tmp).ToString(), colorRepairs);
                            }
                            else
                            {
                                //c первого дня по сегодня
                                if (t.Repairs[i].DateOpen.Value.Year < t.Repairs[i].DateEnd.Value.Year && t.Repairs[i].DateEnd.Value.Year == year)
                                {
                                    setColor(wsh1, ls[firstDay.DayOfYear] + (tmp).ToString() + ":" + ls[t.Repairs[i].DateEnd.Value.DayOfYear] + (tmp).ToString(), colorRepairs);
                                }
                                else
                                {
                                    if (t.Repairs[i].DateOpen.Value.Year == year)
                                    {
                                        setColor(wsh1, ls[t.Repairs[i].DateOpen.Value.DayOfYear] + (tmp).ToString() + ":" + ls[t.Repairs[i].DateEnd.Value.DayOfYear] + (tmp).ToString(), colorRepairs);
                                    }
                                }
                            }

                        }
                        if (!t.Repairs[i].DateOpen.HasValue || !t.Repairs[i].DateEnd.HasValue)
                        {
                            //проверка года
                            if (t.Repairs[i].DateStart.Year < DateTime.Now.Year && t.Repairs[i].DateStart.Year == year)
                            {
                                setColor(wsh1, ls[t.Repairs[i].DateStart.DayOfYear] + (tmp).ToString() + ":" + ls[lastDay.DayOfYear] + (tmp).ToString(), colorRepairs);
                            }
                            else
                            {
                                if (t.Repairs[i].DateStart.Year < DateTime.Now.Year && DateTime.Now.Year == year)
                                {
                                    setColor(wsh1, ls[firstDay.DayOfYear] + (tmp).ToString() + ":" + ls[DateTime.Now.DayOfYear] + (tmp).ToString(), colorRepairs);
                                }
                                else
                                {
                                    setColor(wsh1, ls[t.Repairs[i].DateStart.DayOfYear] + (tmp).ToString() + ":" + ls[DateTime.Now.DayOfYear] + (tmp).ToString(), colorRepairs);
                                }
                            }
                        }
                    }
                }

                //рамка для всех 
                if (k > 6)
                {
                    setBorder(wsh1.Cells["a6:a" + (k - 1).ToString()].Style.Border);
                    setBorderTopAndBottom(wsh1.Cells["b6:nb" + (k - 1).ToString()].Style.Border);
                }

                //обозначения
                wsh1.Cells["A" + (k + 2).ToString()].Value = "Общая занятость оборудования";
                setColor(wsh1, "A" + (k + 2).ToString(), colorMain);
                wsh1.Cells["A" + (k + 3).ToString()].Value = "Занятость оборудования в конкретной работе";
                setColor(wsh1, "A" + (k + 3).ToString(), colorWork);
                wsh1.Cells["A" + (k + 4).ToString()].Value = "Оборудование на ремонте";
                setColor(wsh1, "A" + (k + 4).ToString(), colorRepairs);
                setBorder(wsh1.Cells["a" + (k + 2).ToString() + ":" + "a" + (k + 4).ToString()].Style.Border);

                var r = new ReportFile
                {
                    data = pck.GetAsByteArray(),
                    fileName = "План-график ремонтных работ.xlsx"
                };
                return r;
            }
        }

        // границы недель в отчете План-график
        public void SetWeekBorders(List<string> ls, ExcelWorksheet wsh, int line)
        {
            int sb = 7, con = 0, con1 = 0;
            for (int l = 0; l < 52; l++)
            {
                con++; con1++;
                if (l != 0)
                {
                    if (con1 == 16 || con1 == 24 || con1 == 36 || con1 == 44)
                    {
                        sb = sb + 9; con = 0;
                    }
                    else
                        if (con % 4 == 0 && con != 0 && con1 != 8)
                        {
                            sb = sb + 10; con = 0;
                        }
                        else
                        {
                            sb = sb + 7;
                        }
                }
                string zz = ls[sb] + (line).ToString();
                setBorderRight(wsh.Cells[zz].Style.Border);
            }
        }

        //1c - excel
        public ReportFile Get1CSparePartsExcel(String pathTemplate, DateTime start, DateTime end) {
            var tmpl = new FileInfo(pathTemplate);
            using (ExcelPackage pck = new ExcelPackage(tmpl, true))
            {
                ExcelWorksheet wsh = pck.Workbook.Worksheets[1];
                ExcelWorksheet wsh2 = pck.Workbook.Worksheets[2];
                wsh.Name = "реестр списаний 1С";
                wsh2.Name = "реестр поступлений 1С";
                wsh.Cells["F1"].Value = DateTime.Now.ToString();
                wsh.Cells["d3"].Value = "C " + start.ToString("dd.MM.yyyy");
                wsh.Cells["e3"].Value = "По " + end.ToString("dd.MM.yyyy");
                wsh2.Cells["F1"].Value = DateTime.Now.ToString();
                wsh2.Cells["d3"].Value = "C " + start.ToString("dd.MM.yyyy");
                wsh2.Cells["e3"].Value = "По " + end.ToString("dd.MM.yyyy"); 
                var list = Get1CSparePartsData(start, end);  //writeOff
                var invoice = Get1CSparePartsInvoice(start, end);
                uint k = 7; uint k1 = 7;
                for (int i = 0; i < list.Count; i++)
                {
                    wsh.Cells["a" + k.ToString()].Value = i + 1;
                    wsh.Cells["b" + k.ToString()].Value = list[i].Date.Value.ToString("dd.MM.yyyy");
                    wsh.Cells["c" + k.ToString()].Value = list[i].Act_number;
                    wsh.Cells["d" + k.ToString()].Value = list[i].CarsName + list[i].CarsEquip;
                    wsh.Cells["e" + k.ToString()].Value = list[i].CarsNum;
                    wsh.Cells["f" + k.ToString()].Value = list[i].CountPosition;
                    wsh.Cells["g" + k.ToString()].Value = list[i].CountSpare;
                    wsh.Cells["h" + k.ToString()].Value = Math.Round((decimal)(list[i].SumCost ?? 0), 2);
                    k++;
                }

                for (int i = 0; i < invoice.Count; i++)
                {
                    wsh2.Cells["a" + k1.ToString()].Value = i + 1;
                    wsh2.Cells["b" + k1.ToString()].Value = invoice[i].Date.Value.ToString("dd.MM.yyyy");
                    wsh2.Cells["c" + k1.ToString()].Value = invoice[i].Act_number;
                    wsh2.Cells["d" + k1.ToString()].Value = invoice[i].CarsName;
                    wsh2.Cells["e" + k1.ToString()].Value = invoice[i].CountPosition;
                    wsh2.Cells["f" + k1.ToString()].Value = invoice[i].CountSpare;
                    wsh2.Cells["g" + k1.ToString()].Value = Math.Round((decimal)(invoice[i].SumCost ?? 0), 2);
                    k1++;
                }

                if (k != 7)
                {
                    setBorder(wsh.Cells["A7:h" + (k - 1).ToString()].Style.Border);
                }
                if (k1 != 7)
                {
                    setBorder(wsh2.Cells["A7:g" + (k1 - 1).ToString()].Style.Border);
                }
                    wsh.Cells["g" + k.ToString()].Value = "ИТОГО";
                    wsh.Cells["h" + k.ToString()].Value = Math.Round((decimal)list.Sum(t => t.SumCost), 2);
                    setBorder(wsh.Cells["g" + k.ToString() + ":h" + k.ToString()].Style.Border);

                    wsh2.Cells["f" + k1.ToString()].Value = "ИТОГО";
                    wsh2.Cells["g" + k1.ToString()].Value = Math.Round((decimal)invoice.Sum(t => t.SumCost), 2);
                    setBorder(wsh2.Cells["f" + k1.ToString() + ":g" + k1.ToString()].Style.Border);
                var r = new ReportFile
                {
                    data = pck.GetAsByteArray(),
                    fileName = "Реестр1С.xlsx"
                };
                return r;
            }
        
        }









        //1C
        public string GetSparePartsXMLData(DateTime start, DateTime end)
        {
            // получение данных, формирование xml
            List<SparePartsDetails> income = new List<SparePartsDetails>(); // GEN - id, #, date, cont-inn, cont-kpp, 
                                                                            // ROWS - name, count, sum-no-nds, nds
            List<WriteOffActsDetails> writeoff = new List<WriteOffActsDetails>(); // GEN - id, #, date, tech-num, tech-model, tech-name, otvetstv, 
                                                                                  // ROWS - name, count, sum-no-nds
            List<DailyRepairsModel> daily = new List<DailyRepairsModel>(); // GEN - id, #, date, tech(reg_num, model, name), otvetstv; 
                                                                           // ROWS - date, emp_name, emp_code, type_task_code, type_task_name, tarif, rascenka, hours, sum  
            List<WriteOffActsDetails> demandAct = new List<WriteOffActsDetails>();

            // запросы 
            income = GetSpareIncomeXML(start, end,true);
            writeoff = GetSpareWriteoffXML(start, end);
            daily = GetSpareDailyXML(start, end);
            demandAct = GetDemandInvoiceXML(start, end);
            // формирование

            MemoryStream mStream = new MemoryStream();
            XmlTextWriter writer = new XmlTextWriter(mStream, Encoding.UTF8);

            writer.Formatting = Formatting.Indented;
            writer.WriteStartDocument();

            writer.WriteStartElement("Root"); // root

                foreach (var m in income) // поступление ЗП
                {
                    var df = m.Date.Value.ToString("yyyyMMddHHmmss");
                    writer.WriteStartElement("DocumentObject"); //DocumentObject
                    writer.WriteElementString("ВидДокумента", "1");
                    writer.WriteElementString("ФлагУслуги", m.ServiceXml.ToString());
                    writer.WriteElementString("ID", m.ActId.Value.ToString());
                    writer.WriteElementString("ПометкаУдал", "false");
                    writer.WriteElementString("НомерДокумента", m.Number);
                    writer.WriteElementString("ДатаДокумента", df);
                    writer.WriteElementString("КонтрагентИНН",  m.Contractor != null ? m.Contractor.INN :"");
                    writer.WriteElementString("КонтрагентКПП", m.Contractor != null ? m.Contractor.KPP : "");
                    writer.WriteElementString("Ответственный", "");
                    writer.WriteElementString("Комментарий", "");
                    writer.WriteStartElement("Товары"); // Товары
                    foreach (var ml in m.SparePartsList) // поступление ЗП
                    {
                        writer.WriteStartElement("Row"); // Row
                        writer.WriteElementString("Наименование", ml.SpareParts.Name);
                        writer.WriteElementString("Количество", ml.Count.HasValue ? ((int)ml.Count.Value).ToString() : "");
                        writer.WriteElementString("СуммаБезНДС", ml.Cost1.HasValue ? ml.Cost1.Value.ToString() : "");
                        writer.WriteElementString("СтавкаНДС", ml.Cost2.HasValue ? ml.Cost2.Value.ToString() : "");
                        writer.WriteEndElement(); // Row
                    }
                    writer.WriteEndElement(); // Товары
                    writer.WriteEndElement(); //DocumentObject
                }

            foreach (var m in writeoff) // списание ЗП
            {
                if (m.SparePartsList.Count == 0) { continue; }
                writer.WriteStartElement("DocumentObject"); //DocumentObject
                var df = m.Date.Value.ToString("yyyyMMddHHmmss");
                    writer.WriteElementString("ВидДокумента", "2");
                    writer.WriteElementString("ID", m.Id.Value.ToString());
                    writer.WriteElementString("ТипТехники", m.TypeTech);
                    writer.WriteElementString("ПометкаУдал", "false");
                    writer.WriteElementString("НомерДокумента", m.Act_number);
                    writer.WriteElementString("ДатаДокумента", df);
                    writer.WriteElementString("ТехникаГосНомер", m.CarsNum);
                    writer.WriteElementString("ТехникаМодель",  m.CarsModel);
                    writer.WriteElementString("ТехникаНаименование", m.CarsName);
                    writer.WriteElementString("ОборудованиеНаименование", m.CarsEquip);
                    writer.WriteElementString("Ответственный", m.OtvName);
                    writer.WriteElementString("Комментарий", m.Comment);
                    writer.WriteStartElement("Товары"); // Товары
                    foreach (var ml in m.SparePartsList) // поступление ЗП
                    {
                        writer.WriteStartElement("Row"); // Row
                            writer.WriteElementString("Наименование", ml.SpareParts.Name);
                            writer.WriteElementString("Количество", ml.Count.HasValue ? ((int)ml.Count.Value).ToString() : "");
                            writer.WriteElementString("СуммаБезНДС", ml.Cost1.HasValue ? ml.Cost1.Value.ToString() : "");
                        writer.WriteEndElement(); // Row
                    }
                    writer.WriteEndElement(); // Товары
                    writer.WriteEndElement(); //DocumentObject
            }

            foreach (var m in daily) // ежедневные наряды
            {
                writer.WriteStartElement("DocumentObject"); //DocumentObject
                var df = m.Main.Date.ToString("yyyyMMddHHmmss");
                    writer.WriteElementString("ВидДокумента", "3");
                    writer.WriteElementString("ID", m.Main.Id.ToString());
                    writer.WriteElementString("ТипТехники", m.Main.TypeTech);
                    writer.WriteElementString("ПометкаУдал", "false");
                    writer.WriteElementString("НомерДокумента", m.Main.DailyNum);
                    writer.WriteElementString("ДатаДокумента", df);
                    writer.WriteElementString("ТехникаГосНомер",  m.Main.CarsNum);
                    writer.WriteElementString("ТехникаМодель",  m.Main.CarsModel);
                    writer.WriteElementString("ТехникаНаименование", m.Main.CarsName);
                    writer.WriteElementString("Ответственный", m.Main.OtvName);
                    writer.WriteElementString("Комментарий", "");
                    writer.WriteStartElement("Товары"); // Товары
                    foreach (var ml in m.Spendings) // поступление ЗП
                    {
                        writer.WriteStartElement("Row"); // Row
                        var df1 = ml.Date1.Value.ToString("yyyyMMddHHmmss");
                            writer.WriteElementString("ДатаРабот", df1);
                            writer.WriteElementString("СотрудникФИО", ml.Employees.Name);
                            writer.WriteElementString("СотрудникКод", ml.Employees.Code);
                            writer.WriteElementString("ВидРаботКод", ml.Tasks.Code);
                            writer.WriteElementString("ВидРаботНаименование", ml.Tasks.Name);
                            writer.WriteElementString("Тариф", ml.RateShift != null ? ml.RateShift.ToString() : "");
                            writer.WriteElementString("Расценка", ml.RatePiecework != null ? ml.RatePiecework.ToString() : "");
                            writer.WriteElementString("ОтработаноЧасов", ml.Hours != null ? ml.Hours.ToString() : "");
                            writer.WriteElementString("ИтогоНачислено", ml.SumCost != null ? ml.SumCost.ToString() : "");
                        writer.WriteEndElement(); // Row
                    }
                    writer.WriteEndElement(); // Товары
                writer.WriteEndElement(); //DocumentObject
            }
            //=================требования-накладные
            foreach (var m in demandAct) // ежедневные наряды
            {
                if (m.SparePartsList.Count == 0) { continue; }
                writer.WriteStartElement("DocumentObject"); //DocumentObject
                var date = m.Date.Value.ToString("yyyyMMddHHmmss");
                writer.WriteElementString("ВидДокумента", "4");
                writer.WriteElementString("ID", m.Id.ToString());
                writer.WriteElementString("ТипТехники", m.TypeTech);
                writer.WriteElementString("ПометкаУдал", "false");
                writer.WriteElementString("НомерДокумента", m.Act_number);
                writer.WriteElementString("ДатаДокумента", date);
                writer.WriteElementString("ТехникаГосНомер", m.CarsNum);
                writer.WriteElementString("ТехникаМодель", m.CarsModel);
                writer.WriteElementString("ТехникаНаименование", m.CarsName);
                writer.WriteElementString("ОборудованиеНаименование", m.CarsEquip);
                writer.WriteElementString("Ответственный", m.OtvName);
                writer.WriteElementString("Комментарий", m.Comment);
                writer.WriteStartElement("Товары"); // Товары
                foreach (var ml in m.SparePartsList) 
                {
                    writer.WriteStartElement("Row"); // Row
                    writer.WriteElementString("Наименование", ml.SpareParts.Name);
                    writer.WriteElementString("Количество", ml.Count.HasValue ? ((int)ml.Count.Value).ToString() : "");
                    writer.WriteElementString("СуммаБезНДС", ml.Cost1.HasValue ? ml.Cost1.Value.ToString() : "");
                    writer.WriteEndElement(); // Row
                }
                writer.WriteEndElement(); // Товары
                writer.WriteEndElement(); //DocumentObject
            }


            writer.WriteEndElement(); // root

            writer.Flush();
            mStream.Flush();
            mStream.Position = 0;
            StreamReader sReader = new StreamReader(mStream);
            String FormattedXML = sReader.ReadToEnd();

            return FormattedXML;
        }

        // поступления запчастей 1с
        public List<SparePartsDetails> GetSpareIncomeXML(DateTime start, DateTime end, Boolean checkCash)
        {
            var result = db.Spare_parts_Acts.Where(s => DbFunctions.TruncateTime(s.date) >= DbFunctions.TruncateTime(start) && (checkCash ? s.is_cash != 1 : true)
                                            && DbFunctions.TruncateTime(s.date) <= DbFunctions.TruncateTime(end)).Select(g => new SparePartsDetails
            {
                Id = g.spss_id,
                ActId = g.spss_id,
                Date = g.date,
                ServiceXml = g.type_act,
                Number = g.act_number,
                ContractorsId = g.contr_id,
                Contractor = db.Contractor.Where(co => co.id == g.contr_id).Select(sco => new ContractorInfo { Code = sco.code, INN = sco.inn, KPP = sco.kpp }).FirstOrDefault(),
                StorageId = g.store_store_id,
                Torg12 = (g.torg_id == null || g.torg_id == 0) ? false : true, 
                SparePartsList = db.Spare_parts_Records.Where(p => p.spss_spss_id == g.spss_id && p.store_from_id == 0 && p.store_to_id != 0 //&& p.spare_spare_id==67 //test
                ).Select(m => new SparePartsModel
                {
                    SpareParts = new DictionaryItemsDTO
                    {
                        Id = (int)m.spare_spare_id,
                        Name = db.Spare_parts.Where(v => v.spare_id == m.spare_spare_id).Select(v => v.name).FirstOrDefault()
                    },
                    VendorCode = new DictionaryItemsDTO
                    {
                        Id = (int)m.spare_spare_id,
                        Name = db.Spare_parts.Where(v => v.spare_id == m.spare_spare_id).Select(v => v.vendor_code).FirstOrDefault()
                    },
                    Manufacturer = db.Spare_parts.Where(v => v.spare_id == m.spare_spare_id).Select(v => v.manufacturer).FirstOrDefault(),
                    Count = m.count,
                    Price = m.price,
                    Cost1 = Math.Round(m.price * m.count, 2),
                    Cost2 = m.nds,
                    Cost3 = Math.Round(m.price * m.count + (float)((m.price * m.count) * (m.nds / 100)), 2),
                    Id = m.sps_id,
                    Store_id = m.store_to_id,
                    Pk = 1,
                }).ToList(),
            }).ToList();
            return result;
        }

        // списание запчастей 1с
        public List<WriteOffActsDetails> GetSpareWriteoffXML(DateTime start, DateTime end)
        {
            var result = db.Repairs_acts.Where(s => DbFunctions.TruncateTime(s.date_end) >= DbFunctions.TruncateTime(start)
                                            && DbFunctions.TruncateTime(s.date_end) <= DbFunctions.TruncateTime(end) && s.status == 3).Select(g => new WriteOffActsDetails
            {
                Id = g.rep_id,
                Date = g.date_end,
                Act_number = g.number_act,
                Comment = g.comment,
                CarsName = g.trakt_trakt_id != null ? db.Traktors.Where(l => l.trakt_id == g.trakt_trakt_id).Select(l => l.name).FirstOrDefault() : "", //техника
                CarsNum = g.trakt_trakt_id != null ? db.Traktors.Where(l => l.trakt_id == g.trakt_trakt_id).Select(l => l.reg_num).FirstOrDefault() : "", //техника
                CarsModel = g.trakt_trakt_id != null ? db.Traktors.Where(l => l.trakt_id == g.trakt_trakt_id).Select(l => l.model).FirstOrDefault() : "", //техника
                TypeTech = g.trakt_trakt_id != null ? db.Traktors.Where(l => l.trakt_id == g.trakt_trakt_id).Select(l => l.id_type_way_list).FirstOrDefault().ToString() : "",
                CarsEquip = g.equi_equi_id != null ? db.Equipments.Where(p => p.equi_id == g.equi_equi_id).Select(p => p.name).FirstOrDefault() : "",
                OtvName = db.Employees.Where(ew => ew.emp_id == g.emp_emp_id).Select(se => se.short_name).FirstOrDefault(),
                SparePartsList = db.Spare_parts_Records.Where(p => p.spss_spss_id == g.rep_id && p.store_from_id != 0 && p.store_to_id == 0).Select(m => new SparePartsModel
                {
                    SpareParts = new DictionaryItemsDTO
                    {
                        Id = (int)m.spare_spare_id,
                        Name = db.Spare_parts.Where(v => v.spare_id == m.spare_spare_id).Select(v => v.name).FirstOrDefault()
                    },
                    VendorCode = new DictionaryItemsDTO
                    {
                        Id = (int)m.spare_spare_id,
                        Name = db.Spare_parts.Where(v => v.spare_id == m.spare_spare_id).Select(v => v.vendor_code).FirstOrDefault()
                    },
                    Storage1 = db.Storages.Where(v => v.store_id == m.store_from_id).Select(v => v.name).FirstOrDefault(),
                    Manufacturer = db.Spare_parts.Where(v => v.spare_id == m.spare_spare_id).Select(v => v.manufacturer).FirstOrDefault(),
                    Count = m.count,
                    Price = (float?)Math.Round(m.price, 2),
                    Cost1 = Math.Round(m.price * m.count, 2),
                    Id = m.sps_id,
                    Store_id = (int) m.store_from_id,
                    Pk = 1,
                }).ToList(),
            }).ToList();
            return result;
        }

        //============ 1c запчасти - списание
        public List<WriteOff1cSpare> Get1CSparePartsData(DateTime start, DateTime end)
        {
            var result = db.Repairs_acts.Where(s => DbFunctions.TruncateTime(s.date_end) >= DbFunctions.TruncateTime(start)
                                            && DbFunctions.TruncateTime(s.date_end) <= DbFunctions.TruncateTime(end) && s.status == 3)
                                            .Select(g => new WriteOff1cSpare
                                            {
                                                Id = g.rep_id,
                                                Date = g.date_end,
                                                Act_number = g.number_act,
                                                CarsName = g.trakt_trakt_id != null ? db.Traktors.Where(l => l.trakt_id == g.trakt_trakt_id).Select(l => l.name).FirstOrDefault() : "", //техника
                                                CarsNum = g.trakt_trakt_id != null ? db.Traktors.Where(l => l.trakt_id == g.trakt_trakt_id).Select(l => l.reg_num).FirstOrDefault() : "", //техника
                                                CarsEquip = g.equi_equi_id != null ? db.Equipments.Where(p => p.equi_id == g.equi_equi_id).Select(p => p.name).FirstOrDefault() : "",
                                                CountPosition = db.Repairs_Records.Where(t => t.rep_rep_id == g.rep_id).Count(),
                                                CountSpare = db.Repairs_Records.Where(t => t.rep_rep_id == g.rep_id).Sum(t => t.count) ?? 0,
                                                SumCost = db.Spare_parts_Records.Where(p => p.spss_spss_id == g.rep_id && p.store_from_id != 0 
                                                && p.store_to_id == 0).Select(t => t.count * t.price).Sum(),
                                            }).OrderBy(t=>t.Date).ToList();
            return result;
        }

        //поступление
        public List<WriteOff1cSpare> Get1CSparePartsInvoice(DateTime start, DateTime end)
        {
            var result = db.Spare_parts_Acts.Where(s => DbFunctions.TruncateTime(s.date) >= DbFunctions.TruncateTime(start)
                                            && DbFunctions.TruncateTime(s.date) <= DbFunctions.TruncateTime(end))
                                            .Select(g => new WriteOff1cSpare
                                            {
                                                Id = g.spss_id,
                                                Date = g.date,
                                                Act_number = g.act_number,
                                                CarsName = g.contr_id != null ? db.Contractor.Where(l => l.id == g.contr_id).Select(l => l.name).FirstOrDefault() : "",
                                                CountPosition = db.Spare_parts_Records.Where(p => p.spss_spss_id == g.spss_id && p.store_from_id == 0
                                                && p.store_to_id != 0).Count(),
                                                CountSpare = db.Spare_parts_Records.Where(p => p.spss_spss_id == g.spss_id && p.store_from_id == 0
                                                && p.store_to_id != 0).Select(t => t.count).Sum(),
                                                SumCost = db.Spare_parts_Records.Where(p => p.spss_spss_id == g.spss_id && p.store_from_id == 0
                                                && p.store_to_id != 0).Select(t => t.count * t.price + (t.count * t.price*(t.nds/100))).Sum(),
                                            }).OrderBy(t => t.Date).ToList();
            return result;
        }


        //




        //требования-накладные 1c
        public List<WriteOffActsDetails> GetDemandInvoiceXML(DateTime start, DateTime end)
        {
        var res = (from s in db.Repairs_Records
                   join k3 in db.Repairs_acts on s.rep_rep_id equals k3.rep_id into left3
                   from t3 in left3.DefaultIfEmpty()
                   where DbFunctions.TruncateTime(s.date.Value) >= DbFunctions.TruncateTime(start) && DbFunctions.TruncateTime(s.date.Value) <= DbFunctions.TruncateTime(end)
                   && t3.rep_id != null && s.count != 0
                   select new WriteOffActsDetails
                   {
                       Id = t3.rep_id,
                       Status = t3.status.Value,
                       Date = s.date,
                       Comment = t3.comment,
                       Act_number = t3.number_act,
                       CarsName = t3.trakt_trakt_id != null ? db.Traktors.Where(l => l.trakt_id == t3.trakt_trakt_id).Select(l => l.name).FirstOrDefault() : "", //техника
                       CarsNum = t3.trakt_trakt_id != null ? db.Traktors.Where(l => l.trakt_id == t3.trakt_trakt_id).Select(l => l.reg_num).FirstOrDefault() : "", //техника
                       CarsModel = t3.trakt_trakt_id != null ? db.Traktors.Where(l => l.trakt_id == t3.trakt_trakt_id).Select(l => l.model).FirstOrDefault() : "", //техника
                       TypeTech = t3.trakt_trakt_id != null ? db.Traktors.Where(l => l.trakt_id == t3.trakt_trakt_id).Select(l => l.id_type_way_list).FirstOrDefault().ToString() : "",
                       CarsEquip = t3.equi_equi_id != null ? db.Equipments.Where(p => p.equi_id == t3.equi_equi_id).Select(p => p.name).FirstOrDefault() : "",
                       OtvName = db.Employees.Where(ew => ew.emp_id == t3.emp_emp_id).Select(se => se.short_name).FirstOrDefault(),
                       SparePartsList = db.Repairs_Records.Where(p => p.rep_rep_id == t3.rep_id && DbFunctions.TruncateTime(p.date) == DbFunctions.TruncateTime(s.date))
                .Select(m => new SparePartsModel
                {
                    SpareParts = new DictionaryItemsDTO
                    {
                        Id = (int)m.spare_spare_id,
                        Name = db.Spare_parts.Where(v => v.spare_id == m.spare_spare_id).Select(v => v.name).FirstOrDefault()
                    },
                    VendorCode = new DictionaryItemsDTO
                    {
                        Id = (int)m.spare_spare_id,
                        Name = db.Spare_parts.Where(v => v.spare_id == m.spare_spare_id).Select(v => v.vendor_code).FirstOrDefault()
                    },
                    Storage1 = db.Storages.Where(v => v.store_id == m.store_store_id).Select(v => v.name).FirstOrDefault(),
                    Manufacturer = db.Spare_parts.Where(v => v.spare_id == m.spare_spare_id).Select(v => v.manufacturer).FirstOrDefault(),
                    Count = m.count,
                    Price = db.Spare_parts_Current.Where(v => v.spare_spare_id == m.spare_spare_id && v.store_store_id == m.store_store_id)
                    .Select(v => v.price).FirstOrDefault(),
                    Id = m.repair_id,
                }).ToList(),
                   }).OrderBy(t => t.Date).ToList();
            for (int i = 0; i < res.Count; i++)
            {
                res[i].Date1 = res[i].Date.Value.ToString("dd.MM.yyyy");
            }

            for (int i = 0; i < res.Count; i++)
            {
                if (i != 0 && res[i].Act_number.Equals(res[i - 1].Act_number)) { res.RemoveAt(i); i--; }
            }
            //создаем номера
            for (int i = 0; i < res.Count; i++)
            {
                var id = res[i].Id;
                var countRepairs = db.Repairs_Records.Where(t => t.rep_rep_id == id).Select(t => DbFunctions.TruncateTime(t.date)).Distinct().ToList();

                for (int j = 0; j < countRepairs.Count; j++)
                {
                    if (res[i].Date.Value.Date == countRepairs[j].Value.Date) { res[i].Act_number = res[i].Act_number + "/" + (j + 1); break; }
                }
            }

            res = res.OrderBy(t => t.Date).ToList();
            //
            for (int l = 0; l < res.Count; l++)
            {
                if (res[l].Status != 3)
                {
                    for (int i = 0; i < res[l].SparePartsList.Count; i++)
                    {
                        res[l].SparePartsList[i].Price = (float)Math.Round((decimal)(res[l].SparePartsList[i].Price ?? 0), 2);
                        res[l].SparePartsList[i].Cost1 = (double)Math.Round((decimal)((res[l].SparePartsList[i].Price ?? 0) * (res[l].SparePartsList[i].Count ?? 0)), 2);
                    }
                }
                else
                {
                    for (int i = 0; i < res[l].SparePartsList.Count; i++)
                    {
                        var spareId = res[l].SparePartsList[i].SpareParts.Id;
                        var storeId = res[l].SparePartsList[i].Store_id;
                        var id = res[l].Id;
                        res[l].SparePartsList[i].Price = db.Spare_parts_Records.Where(t => t.store_from_id != 0
                        && t.store_to_id == 0 && t.spare_spare_id == spareId && t.store_from_id == storeId && t.spss_spss_id == id)
                        .Select(t => t.price).FirstOrDefault();
                        res[l].SparePartsList[i].Price = (float)Math.Round((decimal)(res[l].SparePartsList[i].Price ?? 0), 2);
                        res[l].SparePartsList[i].Cost1 = (double)Math.Round((decimal)((res[l].SparePartsList[i].Price ?? 0) * (res[l].SparePartsList[i].Count ?? 0)), 2);
                    }
                }
            }

            return res;
        }



        // ежедневные наряды 1с
        public List<DailyRepairsModel> GetSpareDailyXML(DateTime start, DateTime end)
        {
            List<DailyRepairsModel> result = new List<DailyRepairsModel>();

            result = db.Daily_acts.Where(s => DbFunctions.TruncateTime(s.date1) >= DbFunctions.TruncateTime(start)
                                            && DbFunctions.TruncateTime(s.date1) <= DbFunctions.TruncateTime(end)).Select(p => new DailyRepairsModel
            {
                Main = new GeneralInfoDaily
                {
                    Id = p.dai_id,
                    DailyNum = p.number_dai_act,
                    RepairsActs = p.rep_id,
                    Date = p.date1,
                    timeStart = p.time_start,
                    timeEnd = p.time_end,
                //  CarsNum = g.trakt_trakt_id != null ? db.Traktors.Where(l => l.trakt_id == g.trakt_trakt_id).Select(l => l.reg_num).FirstOrDefault() : "", //техника
                    CarsName = db.Traktors.Where(wt => wt.trakt_id == db.Repairs_acts.Where(wr => wr.rep_id == p.rep_id).Select(sr => sr.trakt_trakt_id).FirstOrDefault()).Select(l => l.name).FirstOrDefault(),
                    CarsNum = db.Traktors.Where(wt => wt.trakt_id == db.Repairs_acts.Where(wr => wr.rep_id == p.rep_id).Select(sr => sr.trakt_trakt_id).FirstOrDefault()).Select(l => l.reg_num).FirstOrDefault(),
                    CarsModel = db.Traktors.Where(wt => wt.trakt_id == db.Repairs_acts.Where(wr => wr.rep_id == p.rep_id).Select(sr => sr.trakt_trakt_id).FirstOrDefault()).Select(l => l.model).FirstOrDefault(),
                    TypeTech = db.Traktors.Where(wt => wt.trakt_id == db.Repairs_acts.Where(wr => wr.rep_id == p.rep_id).Select(sr => sr.trakt_trakt_id).FirstOrDefault()).Select(l => l.id_type_way_list).FirstOrDefault().ToString()
                },

                Spendings = db.Repairs_Tasks_Spendings.Where(s => s.dai_dai_id == p.dai_id).Select(s => new TableDataDaily
                {
                    Id = s.sp_id,
                    Date1 = s.date1.Value,
                    Employees = new EmpTaskInfo
                    {
                        Id = s.emp_emp_id.Value,
                        Name = db.Employees.Where(e => e.emp_id == s.emp_emp_id).Select(r => r.short_name).FirstOrDefault(),
                        Code = db.Employees.Where(e => e.emp_id == s.emp_emp_id).Select(r => r.code).FirstOrDefault()
                    },
                    Tasks = new EmpTaskInfo
                    {
                        Id = s.tptas_tptas_id.Value,
                        Name = db.Type_tasks.Where(t => t.tptas_id == s.tptas_tptas_id).Select(r => r.name).FirstOrDefault(),
                        Code = db.Type_tasks.Where(t => t.tptas_id == s.tptas_tptas_id).Select(r => r.in_code).FirstOrDefault()
                    },
                    RateShift = s.rate_shift,
                    RatePiecework = s.rate_piecework,
                    Hours = s.hours,
                    SumCost = (s.rate_piecework ?? 0) * (s.hours ?? 0) + (s.rate_shift ?? 0)
                }).ToList()
            }).ToList();

            return result;
        }

        /// Aleksey
        // Журнал учета мин.уд и пестицидов
        public ReportFile GetJournalExcel(string pathTemplate, DateTime startDate, DateTime endDate, int? employeerId, int? fieldId, int? plantId, int? materialId)
        {
            var tmpl = new FileInfo(pathTemplate);
            using (ExcelPackage pck = new ExcelPackage(tmpl, true))
            {
                //реестр приходных накладных
                ExcelWorksheet wsh = pck.Workbook.Worksheets[2];
                switch (materialId)
                {
                    case 2: wsh.Name = "Журнал учета мин.удобрений"; break;
                    case 1: wsh.Name = "Журнал учета пестецидов"; break;
                }

                var startYear = 0; var endYear = 0;
                var id = db.Fields_to_plants.Where(t => t.fielplan_id == plantId).Select(t => t.plant_plant_id).FirstOrDefault();
                if (plantId != -1)
                {
                    startYear = db.Fields_to_plants.Where(t => t.fielplan_id == plantId).Select(t => t.date_ingathering.Year).FirstOrDefault();
                    endYear = db.Fields_to_plants.Where(t => t.fielplan_id == plantId).Select(t => t.date_ingathering.Year).FirstOrDefault();
                }
                else
                {
                    startYear = startDate.Year;
                    endYear = endDate.Year;
                }
                var listJournal = db.TMC_Records.Where(v => v.emp_to_id == 0 && v.emp_from_id != 0 && (employeerId != -1 ? v.TMC_Acts.emp_utv_id == employeerId : true)
                    && (materialId != -1 ? v.Materials.mat_type_id == materialId : true)
                    && (fieldId != -1 ? v.Fields_to_plants.fiel_fiel_id == fieldId : true) && (((plantId != -1 && fieldId != -1 && plantId != null) ? v.Fields_to_plants.fielplan_id == plantId : true))
                    && (((fieldId == -1 && plantId != -1 && plantId != null) ? v.Fields_to_plants.plant_plant_id == id : true))
                  && ((v.Fields_to_plants.Fields.fiel_id != null ? v.Fields_to_plants.date_ingathering.Year >= startYear : true) && (v.Fields_to_plants.Fields.fiel_id != null ? v.Fields_to_plants.date_ingathering.Year <= endYear : true))
                     && (DbFunctions.TruncateTime(v.TMC_Acts.act_date) >= DbFunctions.TruncateTime(startDate) && DbFunctions.TruncateTime(v.TMC_Acts.act_date) <= DbFunctions.TruncateTime(endDate))
                   )
                   .Select(v =>
                         new GetJournal
                         {
                             Id = v.Fields_to_plants.fiel_fiel_id, //номер поля
                             RecipientName = v.Employees1.short_name,
                             DateT = v.TMC_Acts.act_date.Value,
                             MaterialName = v.Materials.name,
                             MaterialId = v.Materials.mat_id,
                             MaterialType = materialId,
                             PlantName = v.Fields_to_plants != null ? v.Fields_to_plants.Plants.name : null,
                             FieldName = v.Fields_to_plants != null ? v.Fields_to_plants.Fields.name : null,
                             PlantSortId = -1,
                             plantId = v.Fields_to_plants.fielplan_id,
                             plantId1 = v.Fields_to_plants.plant_plant_id,
                             Area = v.area,
                             Count1 = v.count,
                             ConsumptionRate = v.area != 0 ? v.count / v.area : 0,
                             Cost1 = v.count * v.price,
                             TypeTaskId = -1,
                             NameTask = "",
                             NameTraktor = "",
                             //Number = v.TMC_Acts.act_num,
                             //Price1 = v.price,
                             //Organization = v.Employees1.Organizations.name,
                         }).OrderBy(v => v.DateT).ToList();

                //// Нахождение сорта
                var name = "Нет сорта";
                var sortlist = db.Sorts_plants_data.Where(f => f.fielplan_fielplan_id == f.Fields_to_plants.fielplan_id && f.Fields_to_plants.date_ingathering.Year >= startYear
                    // && f.Type_sorts_plants.name != name
                    && f.Fields_to_plants.date_ingathering.Year <= endYear).Select(f =>
                    new GetJournal
                    {
                        PlantSort = f.Type_sorts_plants.name != name ? f.Type_sorts_plants.name : "",
                        plantId = f.Fields_to_plants.fielplan_id,
                        Id = f.Fields_to_plants.fiel_fiel_id,
                        PlantSortId = f.Type_sorts_plants.tpsort_id,
                    }).ToList();

                /// Дата снятия - уборки урожая
                var datelist = db.TC_operation.Where(o => o.id_tc == o.TC.id && o.tptas_tptas_id == o.Type_tasks.tptas_id && o.Type_tasks.type_fuel_id == 3
                    && (DbFunctions.TruncateTime(o.date_start) >= DbFunctions.TruncateTime(startDate) && DbFunctions.TruncateTime(o.date_start) <= DbFunctions.TruncateTime(endDate))
                    ).Select(o => new GetJournal
                    {
                        TypeTaskId = o.tptas_tptas_id,
                        NameTask = o.Type_tasks.name,
                        PlantSortId = o.TC.tpsort_tpsort_id != null ? o.TC.tpsort_tpsort_id : -1,
                        plantId1 = o.TC.plant_plant_id,
                        Date = o.date_start.Value,
                        Id = o.TC.fiel_fiel_id != null ? o.TC.fiel_fiel_id : -1,
                    }).OrderBy(o => o.Date).ToList();

                /// список работ по ТК
                var list1 = db.TC_param_value.Where(o => o.id_tc == o.TC.id && o.TC_operation.id == o.id_tc_operation && o.Materials.mat_type_id == materialId &&
                    (DbFunctions.TruncateTime(o.TC_operation.date_start) >= DbFunctions.TruncateTime(startDate) && DbFunctions.TruncateTime(o.TC_operation.date_start) <= DbFunctions.TruncateTime(endDate))
                    ).Select(o => new GetJournal
                    {

                        TypeTaskId = o.TC_operation.tptas_tptas_id,
                        NameTask = o.TC_operation.Type_tasks.name,
                        PlantSortId = o.TC.tpsort_tpsort_id != null ? o.TC.tpsort_tpsort_id : -1,
                        plantId1 = o.TC.plant_plant_id,
                        Id = o.TC.fiel_fiel_id != null ? o.TC.fiel_fiel_id : -1,
                        MaterialId = o.mat_mat_id,
                        MaterialType = o.Materials.mat_type_id,
                        Date = o.TC_operation.date_start.Value,
                    }).ToList();

                // Марка машины
                var machinelist = db.Tasks.Where(s => s.shtr_shtr_id == s.Sheet_tracks.shtr_id &&
                    (DbFunctions.TruncateTime(s.Sheet_tracks.date_begin) >= DbFunctions.TruncateTime(startDate) && DbFunctions.TruncateTime(s.Sheet_tracks.date_begin) <= DbFunctions.TruncateTime(endDate))
                    && s.Sheet_tracks.shtr_type_id == 1).Select(s => new GetJournal
                    {
                        Id = s.fiel_fiel_id,
                        TypeTaskId = s.tptas_tptas_id,
                        plantId = s.plant_plant_id,
                        NameTraktor = s.Sheet_tracks.Traktors.model + ", " + s.Sheet_tracks.Equipments.name,
                        DateT1 = s.Sheet_tracks.date_begin,
                    }).ToList();

                for (int i = 0; i < listJournal.Count; i++)
                {
                    var sort = sortlist.Where(t => listJournal[i].Id == t.Id && t.plantId == listJournal[i].plantId).ToList();

                    for (int t = 0; t < sort.Count; t++)
                    {
                        if (listJournal[i].PlantSortId == -1)
                        {
                            listJournal[i].PlantName = sort[t].PlantSort != "" ? listJournal[i].PlantName + ", " + sort[t].PlantSort : listJournal[i].PlantName + " ";
                            listJournal[i].PlantSortId = sort[t].PlantSortId;
                        }
                        else
                        {
                            listJournal[i].PlantName = listJournal[i].PlantName + " / " + sort[t].PlantSort;
                            listJournal[i].SortID = sort[t].PlantSortId;
                        }
                    }

                    for (int s = 0; s < datelist.Count; s++)
                    {
                        if (datelist[s].PlantSortId == -1 && datelist[s].Id == -1)
                        {
                            var date = datelist.Where(d => d.plantId1 == listJournal[i].plantId1).ToList();
                            for (int l = 0; l < date.Count; l++)
                            {
                                if (listJournal[i].Date.Year == 1)
                                { listJournal[i].Date = date[l].Date; }
                            }
                        }
                        if (datelist[s].PlantSortId != -1)
                        {
                            var date = datelist.Where(d => d.plantId1 == listJournal[i].plantId1 && (d.PlantSortId == listJournal[i].PlantSortId ||
                                d.PlantSortId == listJournal[i].SortID)).ToList();
                            for (int l = 0; l < date.Count; l++)
                            {
                                if (listJournal[i].Date.Year == 1)
                                { listJournal[i].Date = date[l].Date; }
                            }
                        }
                        if (datelist[s].Id != -1)
                        {
                            var date = datelist.Where(d => d.plantId1 == listJournal[i].plantId1 && d.Id == listJournal[i].Id).ToList();
                            for (int l = 0; l < date.Count; l++)
                            {
                                if (listJournal[i].Date.Year == 1)
                                { listJournal[i].Date = date[l].Date; }
                            }
                        }

                    }

                    var nametrak = machinelist.Where(m => m.plantId == listJournal[i].plantId && m.Id == listJournal[i].Id).ToList();
                    var work = list1.Where(w => w.plantId1 == listJournal[i].plantId1).ToList();

                    int min = 32;
                    for (int j = 0; j < nametrak.Count; j++)
                    {
                        for (int f = 0; f < list1.Count; f++)  //type work
                        {
                            if (list1[f].Id != -1 && nametrak[j].TypeTaskId == list1[f].TypeTaskId && list1[f].Id == nametrak[j].Id)
                            {

                                var checkdate = Math.Abs((listJournal[i].DateT - nametrak[j].DateT1).Days);
                                if (min >= checkdate)
                                {
                                    min = checkdate;
                                    listJournal[i].DateT1 = nametrak[j].DateT1;
                                    listJournal[i].NameTraktor = nametrak[j].NameTraktor;
                                    listJournal[i].NameTask = list1[f].NameTask;
                                }

                            }
                            if (list1[f].Id == -1 && nametrak[j].TypeTaskId == list1[f].TypeTaskId && list1[f].plantId1 == listJournal[i].plantId1)
                            {

                                var checkdate = Math.Abs((listJournal[i].DateT - nametrak[j].DateT1).Days);
                                if (min >= checkdate)
                                {
                                    min = checkdate;
                                    listJournal[i].DateT1 = nametrak[j].DateT1;
                                    listJournal[i].NameTraktor = nametrak[j].NameTraktor;
                                    listJournal[i].NameTask = list1[f].NameTask;
                                }

                            }
                        }
                    }
                }

                listJournal.OrderBy(c => c.DateT1);
                uint k = 3;

                for (int c = 0; c < listJournal.Count; c++)
                {
                    wsh.Cells["A" + k.ToString()].Value = k - 2;
                    wsh.Cells["A" + k.ToString()].Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);
                    wsh.Cells["B" + k.ToString()].Value = listJournal[c].PlantName;
                    wsh.Cells["B" + k.ToString()].Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);
                    wsh.Cells["C" + k.ToString()].Value = listJournal[c].FieldName;
                    wsh.Cells["C" + k.ToString()].Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);
                    wsh.Cells["D" + k.ToString()].Value = listJournal[c].Area;
                    wsh.Cells["D" + k.ToString()].Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);
                    // wsh.Cells["E" + k.ToString()].Value = listJournal[i].;
                    wsh.Cells["E" + k.ToString()].Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);
                    wsh.Cells["F" + k.ToString()].Value = listJournal[c].MaterialName;
                    wsh.Cells["F" + k.ToString()].Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);
                    wsh.Cells["G" + k.ToString()].Value = listJournal[c].NameTraktor;
                    wsh.Cells["G" + k.ToString()].Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);
                    wsh.Cells["H" + k.ToString()].Value = listJournal[c].DateT1.ToString("dd.MM.yyyy") != "01.01.0001" ? listJournal[c].DateT1.ToString("dd.MM.yyyy") : "";
                    wsh.Cells["H" + k.ToString()].Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);
                    wsh.Cells["I" + k.ToString()].Value = listJournal[c].ConsumptionRate;
                    wsh.Cells["I" + k.ToString()].Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);
                    wsh.Cells["J" + k.ToString()].Value = listJournal[c].Count1;
                    wsh.Cells["J" + k.ToString()].Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);

                    if (materialId == 1)
                    {
                        wsh.Cells["K" + k.ToString()].Value = listJournal[c].Date.ToString("dd.MM.yyyy") != "01.01.0001" ? listJournal[c].Date.ToString("MMMM") : "";
                        wsh.Cells["K" + k.ToString()].Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);
                        wsh.Cells["L" + k.ToString()].Value = listJournal[c].RecipientName;
                        wsh.Cells["L" + k.ToString()].Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);
                    }
                    else
                    {
                        wsh.Cells["K" + k.ToString()].Value = listJournal[c].RecipientName;
                        wsh.Cells["K" + k.ToString()].Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);
                    }

                    k++;
                }

                ExcelWorksheet wsh2 = pck.Workbook.Worksheets[1];
                wsh2.Cells["N27"].Value = '\u00ab' + startDate.ToString("dd") + '\u00bb' + startDate.ToString(" MMMM yyyy") + " год";
                wsh2.Cells["N28"].Value = '\u00ab' + endDate.ToString("dd") + '\u00bb' + endDate.ToString(" MMMM yyyy") + " год";
                var idOrg = Int32.Parse(db.Parameters.Where(t => t.name.Equals("NameOrg")).Select(t => t.value).FirstOrDefault());
                var org_name = db.Organizations.Where(t => t.org_id == idOrg).Select(t => t.name).FirstOrDefault();
                wsh2.Cells["A11"].Value = org_name;
                wsh2.Cells["A12"].Value = db.Organizations.Where(t => t.org_id == idOrg).Select(t => t.address).FirstOrDefault();
                var r = new ReportFile
                {
                    data = pck.GetAsByteArray(),
                    fileName = wsh.Name + ".xlsx"
                };
                return r;
            }
        }

        public void setValueForPlanFact(string ch, string pl, string ft, string dv, int i, decimal? valueplan, decimal? valuefact, ExcelWorksheet wsh)
        {
            wsh.Cells[ch + pl.ToString()].Value = valueplan;
            wsh.Cells[ch + ft.ToString()].Value = valuefact;
            wsh.Cells[ch + dv.ToString()].Value = (valuefact ?? 0) - (valueplan ?? 0);
        }

        private void setBorder(Border modelTable)
        {
            modelTable.Top.Style = ExcelBorderStyle.Thin;
            modelTable.Left.Style = ExcelBorderStyle.Thin;
            modelTable.Right.Style = ExcelBorderStyle.Thin;
            modelTable.Bottom.Style = ExcelBorderStyle.Thin;
        }
        private void setBorderTopAndBottom(Border modelTable)
        {
            modelTable.Top.Style = ExcelBorderStyle.Thin;
            modelTable.Bottom.Style = ExcelBorderStyle.Thin;
        }
        private void setBorderBoldTop(Border modelTable)
        {
            modelTable.Top.Style = ExcelBorderStyle.Medium;
        }
        private void setBorderRight(Border modelTable)
        {
          modelTable.Right.Style = ExcelBorderStyle.Thin;
        }
        private void setColor(ExcelWorksheet wsht, string rangePath, string color)
        {
            using (ExcelRange rng = wsht.Cells[rangePath])
            {
                Color colFromHex = System.Drawing.ColorTranslator.FromHtml(color);
                SetCellStyle(rng, colFromHex);
            }
        }
        private void SetCellStyle(ExcelRange cell, Color color)
        {
            cell.Style.Fill.PatternType = ExcelFillStyle.Solid;
            cell.Style.Fill.BackgroundColor.SetColor(color);
        }





    }
}