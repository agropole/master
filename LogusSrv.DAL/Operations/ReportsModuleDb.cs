﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using LogusSrv.DAL.DBModel;
using LogusSrv.DAL.Enums;
using LogusSrv.DAL.Entities.DTO.ExportExcel;
using LogusSrv.DAL.Entities.DTO;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using LogusSrv.DAL.Entities.Res;
using System.Data;
using LogusSrv.DAL.Entities.DTO.ActsSzr;
using LogusSrv.DAL.Entities.SQL;

namespace LogusSrv.DAL.Operations
{
             
    public class ReportsModuleDb : ExportToExcel
    {
        protected readonly LogusDBEntities db = new LogusDBEntities();
        protected readonly SparePartsDb spareDb = new SparePartsDb();
        private List<int> OldShtrId = new List<int>();
        private List<int> OldShtrId1 = new List<int>();
        private List<int> OldShtrId2 = new List<int>();
        public List<int> ListOfPlantId = new List<int>();
        List<PlanRequestDto> totalplan1 = new List<PlanRequestDto>();
        public string str = null;
        public string str1 = null;
        public Cell cellDat;
        IQueryable<Employees> listEmployees;
        List<BalanceMaterials> balanceList = new List<BalanceMaterials>();
        List<ExportDocumentsList> respList = new List<ExportDocumentsList>();
        List<ExpensesRequestDto> tFieldId = new List<ExpensesRequestDto>();
        List<TaskNZPCountModel> nzp = new List<TaskNZPCountModel>();
        List<TaskNZPCountModel> nzp3 = new List<TaskNZPCountModel>();
        List<TaskNZPCountModel> nzp4 = new List<TaskNZPCountModel>();
        List<TaskNZPCountModel> nzp5 = new List<TaskNZPCountModel>();
        List<TaskNZPCountModel> nzp6 = new List<TaskNZPCountModel>();
        public GetReportsInfoDto GetReportsInfo(RespReq req)
        {
            var startDate = req.StartDate.Value.AddHours(3);
            var endDate = req.EndDate.Value;
            var result = new GetReportsInfoDto();
            result.TraktorsList = db.Traktors.Where(t => t.deleted != true).
                                   Where(t => (new[] {1, 2, 3, 4, 5, 6, 9}).Contains(t.tptrak_tptrak_id)).
                                   Select(t => new DictionaryItemsTraktors
                                   {
                                       Name = t.name,
                                       Id = t.trakt_id,
                                       RegNum = t.reg_num,
                                   }).OrderBy(g => g.Name).ToList();
            result.TraktorsList.Insert(0, new DictionaryItemsTraktors
                                   {
                                       Name = "Все",
                                       Id = -1,
                                       RegNum = " "
                                   });
            result.OrganizationsList = db.Garages.Where(o => o.deleted != true).Select(o => new DictionaryItemsDTO
                                        {
                                            Id = o.gar_id,
                                            Name = o.name,
                                        }).ToList();
            result.employeerList = db.Employees.Where(o => o.deleted != true && o.Posts.pst_id == 7).Select(o => new DictionaryItemsDTO
            {
                Id = o.emp_id,
                Name = o.short_name,
            }).ToList();
            result.TmcList = db.Type_materials.Where(o => o.deleted != true ).Select(o => new DictionaryItemsDTO
            {
                Id = o.type_id,
                Name = o.name,
            }).ToList();
            result.FieldsAndPlants = (from f in db.Fields_to_plants
                                      where f.status == (int)StatusFtp.Approved && f.date_ingathering.Year >= startDate.Year && f.date_ingathering.Year<=endDate.Year
                                      group new DictionaryPlantItemsDTO
                                      {
                                          plantId = f.plant_plant_id,
                                          Name = f.Plants.name  + " (" + f.date_ingathering.Year + ")",
                                          Id = f.fielplan_id
                                      }
                                      by new
                                      {
                                          f.fiel_fiel_id,
                                          f.Fields.name,
                                      } into g
                                      select new DictionaryItemForFields
                                      {
                                          Id = g.Key.fiel_fiel_id,
                                          Name = g.Key.name,
                                          Values = g.ToList(),
                                      }).OrderByDescending(g => g.Name).ToList();
            result.AllPlants = db.Fields_to_plants
                               .Where(f => f.status == (int)StatusFtp.Approved && f.date_ingathering.Year == startDate.Year ).
                               Select(t => new DictionaryItemForFields
                               {
                                   Name = t.Plants.name + " (" + t.date_ingathering.Year + ")",
                                   Id = t.plant_plant_id,
                               }).OrderBy(t => t.Name).Distinct().ToList();

            result.AllPlantsWriteOff = db.Fields_to_plants
                              .Where(f => f.status == (int)StatusFtp.Approved && f.date_ingathering.Year >= startDate.Year && f.date_ingathering.Year <= endDate.Year).
                              Select(t => new DictionaryItemForFields
                              {
                                  Name = t.Plants.name + " (" + t.date_ingathering.Year + ")",
                                  Id = t.fielplan_id,
                              }).OrderBy(t => t.Name).Distinct().ToList();
            for (int i = 0; i < result.AllPlantsWriteOff.Count; i++)
            {
                for (int j = 0; j < result.AllPlantsWriteOff.Count; j++)
                {
                    if (i != j && result.AllPlantsWriteOff[i].Name.Equals(result.AllPlantsWriteOff[j].Name))
                    {
                        result.AllPlantsWriteOff.RemoveAt(j);  j--;
                    }
                }
            }

            var all = new DictionaryItemsDTO{Id = -1 , Name = "Все"};
            var all2 = new DictionaryPlantItemsDTO {plantId = -1, Id = -1, Name = "Все" };
            var all3 = new DictionaryItemForFields { Id = -1, Name = "Все" };
            result.OrganizationsList.Insert(0, all);
            result.AllPlants.Insert(0, all3);
            result.AllPlantsWriteOff.Insert(0, all3);
            result.employeerList.Insert(0, all);
            result.TmcList.Insert(0, all);
            var valuesPlants = new List<DictionaryPlantItemsDTO>();
            valuesPlants.AddRange(result.AllPlantsWriteOff);
            for (int i = 0; i < result.FieldsAndPlants.Count; i++)
            {
                if (result.FieldsAndPlants[i].Values.Count > 1)
                {
                    result.FieldsAndPlants[i].Values.Insert(0, all2);
                }
            }
            var allFields = new DictionaryItemForFields { Id = -1, Name = "Все", Values = valuesPlants }; 
            result.FieldsAndPlants.Insert(0, allFields);
            return result;
        }
        //получаем сдельщиков из интервала
        public List<Resp> GetResp(RespReq req)
        {
            var list = db.Database.SqlQuery<DictionaryItemsDTO>(
   string.Format(@"SELECT DISTINCT salemp_id as Id, short_name as Name from Salary_details,Salaries_employees where salemp_id=salemp_salemp_id and sum is not null and
   convert(varchar(10),date,120) >= '" + req.StartDate.Value.ToString("yyyy-MM-dd") + "' " + "  and convert(varchar(10),date,120) <= '" + req.EndDate.Value.ToString("yyyy-MM-dd") + "'")).ToList();
            if (list.Count != 0) {

            list.Insert(0, new DictionaryItemsDTO
                       {Id = 0,Name = "Все" });

            }
            var res = new Resp
            {
              ResponsibleList = list
            };
            return new List<Resp> { res };
        }

        public List<Cult> GetCult(CultReq req)
        {
            var list = db.Database.SqlQuery<DictionaryItemsDTO>(
   string.Format(@"  select p.name+' ('+s.name+')' as Name, id as Id from Plants p, TC t,Type_sorts_plants s where p.plant_id=t.plant_plant_id and 
 s.tpsort_id=t.tpsort_tpsort_id and year=" + req.Year)).ToList();

            var list1 = db.Database.SqlQuery<DictionaryItemsDTO>(
string.Format(@"select p.name as Name, id as Id from Plants p, TC t where p.plant_id=t.plant_plant_id and year=" + req.Year)).ToList();
       
            for (int i = 0; i < list.Count; i++)
            {
                for (int j = 0; j < list1.Count; j++)
                {
                    if (list[i].Id == list1[j].Id)
                    {
                        list1.RemoveAt(j); j--; break;
                    }
                }
            }
            for (int j = 0; j < list1.Count; j++)
            {
                list.Add(list1[j]);
            }
            list.OrderBy(n => n.Name).ToList();


            list.Insert(0, new DictionaryItemsDTO  {Id = 0, Name = "Все"});
            var res = new Cult
            {
                CultList = list
            };
            
            return new List<Cult> { res };
        }
        //по себестоимости
        public List<Cult> GetPlanFactCult(CultReq req)
        {
            var list = (from t in db.TC
                        where t.year == req.Year
                        select new DictionaryItemsDTO
                        {
                           Id = t.Plants.plant_id,
                           Name = t.Plants.name
                        }).OrderBy(t=>t.Name).ToList();

            for (int i = 1; i < list.Count; i++)
            {
                if (list[i].Id == list[i - 1].Id) { list.RemoveAt(i); i--; }
            }
            list.Insert(0, new DictionaryItemsDTO { Id = 0, Name = "Все" });
            var res = new Cult
            {
                CultList = list
            };

            return new List<Cult> { res };
        }

        public TCPlanFactModel GetPlanFactTC(int? year, int? plant_id, int? check)
        {
            var planList = (from v in db.TC_param_value
                            where (plant_id.HasValue ? v.TC.plant_plant_id == plant_id : true) && 
             (v.id_tc_param == 4 || v.id_tc_param == 10 || v.id_tc_param == 16 || v.id_tc_param == 25 || v.id_tc_param == 22 || v.id_tc_param == 19
            || v.id_tc_param == 3 || v.id_tc_param == 1 || v.id_tc_param == 28 || v.id_tc_param == 61)
                                    && !v.id_tc_operation.HasValue
                                    && v.TC.year == year
                            select new TCParamValue
                            {
                                value = (float)v.value,
                                id_tc_param = v.id_tc_param
                            }).ToList();
            var plan = new CostsModel();
            plan.cost = 0;
            plan.cost_salary = 0;
            plan.cost_fuel = 0;
            plan.cost_szr = 0;
            plan.cost_min = 0;
            plan.cost_seed = 0;
            plan.gross_harvest = 0;
            plan.area = 0;
            plan.service_total = 0;
            plan.dop_material_fact = 0;
            foreach (TCParamValue l in planList)
            {
                switch (l.id_tc_param)
                {
                    case 4:
                        plan.cost = plan.cost + (l.value != null ? (float)l.value : 0);
                        break;
                    case 10:
                        plan.cost_salary = plan.cost_salary + (l.value != null ? (float)l.value : 0);
                        break;
                    case 16:
                        plan.cost_fuel = plan.cost_fuel + (l.value != null ? (float)l.value : 0);
                        break;
                    case 25:
                        plan.cost_szr = plan.cost_szr + (l.value != null ? (float)l.value : 0);
                        break;
                    case 22:
                        plan.cost_min = plan.cost_min + (l.value != null ? (float)l.value : 0);
                        break;
                    case 19:
                        plan.cost_seed = plan.cost_seed + (l.value != null ? (float)l.value : 0);
                        break;
                    case 3:
                        plan.gross_harvest = plan.gross_harvest + (l.value != null ? (float)l.value : 0);
                        break;
                    case 1:
                        plan.area = plan.area + (l.value != null ? (float)l.value : 0);
                        break;
                    case 28:
                        plan.service_total = plan.service_total + (l.value != null ? (float)l.value : 0);
                        break;
                    case 61:
                        plan.dop_material_fact = plan.dop_material_fact + (l.value != null ? (float)l.value : 0);
                        break;
                    default:
                        Console.WriteLine("Default case");
                        break;
                }
            }
            var result = new TCPlanFactModel();

            if (check != -1)
            {
                var res = new List<ExpensesRequestDto>();
                res = GetCostsByCulture((int)year, plant_id);
                var fact = new CostsModel();
                fact.cost_salary = (float)res[res.Count - 1].salary_total;
                fact.cost_fuel = (float)res[res.Count - 1].razx_top;
                fact.cost_szr = (float)res[res.Count - 1].szr_fact;
                fact.cost_min = (float)res[res.Count - 1].minud_fact;
                fact.cost_seed = (float)res[res.Count - 1].posevmat_fact;
                fact.gross_harvest = (float)res[res.Count - 1].gross_harvest;
                fact.area = (float)res[res.Count - 1].area;
                fact.service_total = (float)res[res.Count - 1].service_total;
                fact.dop_material_fact = (float)res[res.Count - 1].dop_material_fact;
                var costfact = res[res.Count - 1].total_fact;
                fact.cost = costfact.HasValue ? (float)costfact : 0;
                result.fact = fact;
            }
            result.plan = plan;
            return result;
        }
        //отчет - механизаторы и водители
        public byte[] GetWayListExportExcel(string pathTemplate, int? orgId, DateTime? startDate, DateTime? endDate, bool mechanik)
        {
            endDate = endDate.Value.AddDays(1);
            byte[] byteArray = File.ReadAllBytes(pathTemplate);
            using (MemoryStream stream = new MemoryStream())
            {
                stream.Write(byteArray, 0, (int)byteArray.Length);
                using (var document = SpreadsheetDocument.Open(stream, true))
                {
                    string SheetName;
                    if (mechanik) SheetName = "Путевые листы механизатора";
                    else SheetName = "Путевые листы водителей";
                    // 1 лист отчета 
                    Sheet sheet;
                    try
                    {
                        sheet = document.WorkbookPart.Workbook.GetFirstChild<Sheets>().Elements<Sheet>().SingleOrDefault(s => s.Name == SheetName);
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(String.Format("Возможно в документе существует два листа с названием \"{0}\"!\n", SheetName), ex);
                    }

                    if (sheet == null)
                    {
                        throw new Exception(String.Format("В шаблоне не найден \"{0}\"!\n", SheetName));
                    }


                    var worksheetPart = (WorksheetPart)document.WorkbookPart.GetPartById(sheet.Id.Value);
                    var sheetData = worksheetPart.Worksheet.GetFirstChild<SheetData>();
                    var list = new List<PrintWayListDto>();
                
                    
                    if (mechanik)
                    {
                        list = GetWayListInfo(orgId,(DateTime)startDate,(DateTime)endDate);
                    }
                    else
                    {
                        list = GetDriverWaysListInfo(orgId, (DateTime)startDate, (DateTime)endDate);
                    }
                   // var nameOrg = db.Garages.Where(g => g.gar_id == orgId).Select(g=>g.name).FirstOrDefault();
                    uint i = 3;

                    Cell cellDat = GetCell(worksheetPart.Worksheet,"m", 1);
                    cellDat.CellValue = new CellValue(startDate.Value.ToString("dd.MM.yyyy"));
                    cellDat.DataType = new EnumValue<CellValues>(CellValues.String);

                    Cell cellDat2 = GetCell(worksheetPart.Worksheet,"p", 1);
                    cellDat2.CellValue = new CellValue(endDate.Value.AddDays(-1).ToString("dd.MM.yyyy"));
                    cellDat2.DataType = new EnumValue<CellValues>(CellValues.String);

                    Cell cellDat3 = GetCell(worksheetPart.Worksheet, "E", 1);
                    cellDat3.CellValue = new CellValue(mechanik ? "Механизаторам" : "Водителям");
                    cellDat3.DataType = new EnumValue<CellValues>(CellValues.String);

                    OldShtrId = new List<int>();
                 
                    if (list.Any())
                    {
                        OldShtrId.Add(list[0].ShtrId);
                        list[0].Ind = i - 2;
                        Row refRowC = sheetData.Elements<Row>().Where(r => 3 == r.RowIndex).First();
                        if (mechanik)
                        {
                            SetCellsValueForWayList(list[0], refRowC, i);
                        }
                        else {
                            SetCellsValueForWayListDriver(list[0], refRowC, i);
                        }

                        list.Remove(list[0]);
                        i++;
                    }
                    foreach (var item in list)
                    {
                        OldShtrId.Add(item.ShtrId);
                        item.Ind = i - 2;
                        Row refRowC = sheetData.Elements<Row>().Where(r => 3 == r.RowIndex).First();
                        var row = new Row();
                        row = (Row)refRowC.Clone();
                        row.RowIndex = new UInt32Value(i);

                        foreach (var cell in row.Elements<Cell>())
                        {
                        cell.CellReference = new StringValue(cell.CellReference.Value.Replace(refRowC.RowIndex.Value.ToString(), row.RowIndex.ToString()));
                        }
                        if (mechanik)
                        {
                            SetCellsValueForWayList(item, row, i);
                        }
                        else {
                            SetCellsValueForWayListDriver(item, row, i);
                        }
                        sheetData.Append(row);
                        i++;
                    }
                   
                    worksheetPart.Worksheet.Save();
                    document.Close();
                }
                return stream.ToArray();
            }
        }


        public byte[] GetRefuilngsExportExcel(string pathTemplate, int? traktId, DateTime? startDate, DateTime? endDate)
        {
            endDate = endDate.Value.AddDays(1);
            byte[] byteArray = File.ReadAllBytes(pathTemplate);
            using (MemoryStream stream = new MemoryStream())
            {
                stream.Write(byteArray, 0, (int)byteArray.Length);
                using (var document = SpreadsheetDocument.Open(stream, true))
                {

                    string SheetName = "Топливный оборот";
                    // 1 лист отчета 
                    Sheet sheet;
                    try
                    {
                        sheet = document.WorkbookPart.Workbook.GetFirstChild<Sheets>().Elements<Sheet>().SingleOrDefault(s => s.Name == SheetName);
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(String.Format("Возможно в документе существует два листа с названием \"{0}\"!\n", SheetName), ex);
                    }

                    if (sheet == null)
                    {
                        throw new Exception(String.Format("В шаблоне не найден \"{0}\"!\n", SheetName));
                    }


                    var worksheetPart = (WorksheetPart)document.WorkbookPart.GetPartById(sheet.Id.Value);
                    var sheetData = worksheetPart.Worksheet.GetFirstChild<SheetData>();
                    var list = new PrintRefueilngsDto();

                    list = GetRefuilngsInfo((int?)traktId, (DateTime)startDate, (DateTime)endDate);


                    var traktorInfo = db.Traktors.Where(g => g.trakt_id == traktId).FirstOrDefault();
                    uint i = 12;

                    Cell cellDat = GetCell(worksheetPart.Worksheet, "D", 3);
                    cellDat.CellValue = new CellValue(startDate.Value.ToString("dd.MM.yyyy"));
                    cellDat.DataType = new EnumValue<CellValues>(CellValues.String);

                    Cell cellDat2 = GetCell(worksheetPart.Worksheet, "F", 3);
                    cellDat2.CellValue = new CellValue(endDate.Value.AddDays(-1).ToString("dd.MM.yyyy"));
                    cellDat2.DataType = new EnumValue<CellValues>(CellValues.String);

                    Cell cellDat3 = GetCell(worksheetPart.Worksheet, "D", 5);
                    cellDat3.CellValue = new CellValue(traktorInfo != null ? traktorInfo.name : null);
                    cellDat3.DataType = new EnumValue<CellValues>(CellValues.String);

                    Cell cellDat4 = GetCell(worksheetPart.Worksheet, "D", 6);
                    cellDat4.CellValue = new CellValue(traktorInfo != null ? traktorInfo.reg_num : null);
                    cellDat4.DataType = new EnumValue<CellValues>(CellValues.String);

                    Cell cellDat5 = GetCell(worksheetPart.Worksheet, "L", 4);
                    cellDat5.CellValue = new CellValue(list.TotalCopylov.ToString());
                    cellDat5.DataType = new EnumValue<CellValues>(CellValues.String);

                    Cell cellDat6 = GetCell(worksheetPart.Worksheet, "L", 5);
                    cellDat6.CellValue = new CellValue(list.TotalVinokurova.ToString());
                    cellDat6.DataType = new EnumValue<CellValues>(CellValues.String);

                    Cell cellDat7 = GetCell(worksheetPart.Worksheet, "L", 6);
                    cellDat7.CellValue = new CellValue(list.TotalSlepokurov.ToString());
                    cellDat7.DataType = new EnumValue<CellValues>(CellValues.String);

                    Cell cellDat8 = GetCell(worksheetPart.Worksheet, "L", 7);
                    cellDat8.CellValue = new CellValue(list.TotalAzs.ToString());
                    cellDat8.DataType = new EnumValue<CellValues>(CellValues.String);

                    Cell cellDat9 = GetCell(worksheetPart.Worksheet, "F", 11);
                    cellDat9.CellValue = new CellValue(list.TotalFuelIssued.ToString());
                    cellDat9.DataType = new EnumValue<CellValues>(CellValues.String);

                    Cell cellDat10 = GetCell(worksheetPart.Worksheet, "I", 11);
                    cellDat10.CellValue = new CellValue(list.TotalConsumptionFact.ToString());
                    cellDat10.DataType = new EnumValue<CellValues>(CellValues.String);

                    Cell cellDat12 = GetCell(worksheetPart.Worksheet, "M", 11);
                    cellDat12.CellValue = new CellValue(list.TotalMotoWork.ToString());
                    cellDat12.DataType = new EnumValue<CellValues>(CellValues.String);

                    OldShtrId = new List<int>();

                    if (list.items.Any())
                    {
                        list.items[0].Ind = i - 11;
                        Row refRowC = sheetData.Elements<Row>().Where(r => 12 == r.RowIndex).First();
                        SetCellsValueForRefuelings(list.items[0], refRowC, i);
                        list.items.Remove(list.items[0]);
                        i++;
                    }
                    foreach (var item in list.items)
                    {
                        OldShtrId.Add(item.ShtrId);

                        item.Ind = i - 11;
                        Row refRowC = sheetData.Elements<Row>().Where(r => 12 == r.RowIndex).First();
                        var row = new Row();
                        row = (Row)refRowC.Clone();
                        row.RowIndex = new UInt32Value(i);

                        foreach (var cell in row.Elements<Cell>())
                        {
                            cell.CellReference = new StringValue(cell.CellReference.Value.Replace(refRowC.RowIndex.Value.ToString(), row.RowIndex.ToString()));
                        }
                        SetCellsValueForRefuelings(item, row, i);
                        sheetData.Append(row);
                        i++;
                    }

                    worksheetPart.Worksheet.Save();
                    document.Close();
                }
                return stream.ToArray();
            }
        }
      
        public byte[] GetDocResExcel(DateTime startDate, DateTime endDate, string pathTemplate, int? empId)
        {
            byte[] byteArray = File.ReadAllBytes(pathTemplate);
            using (MemoryStream stream = new MemoryStream())
            {
                stream.Write(byteArray, 0, (int)byteArray.Length);
                using (var document = SpreadsheetDocument.Open(stream, true))
                {
                    string SheetName;
                    SheetName = "Начисление ЗП";
                    // 1 лист отчета 
                    Sheet sheet;
                    try
                    {
                        sheet = document.WorkbookPart.Workbook.GetFirstChild<Sheets>().Elements<Sheet>().SingleOrDefault(s => s.Name == SheetName);
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(String.Format("Возможно в документе существует два листа с названием \"{0}\"!\n", SheetName), ex);
                    }

                    if (sheet == null)
                    {
                        throw new Exception(String.Format("В шаблоне не найден \"{0}\"!\n", SheetName));
                    }
                    var worksheetPart = (WorksheetPart)document.WorkbookPart.GetPartById(sheet.Id.Value);
                    var sheetData = worksheetPart.Worksheet.GetFirstChild<SheetData>();
                    if (empId == 0) {
                        respList = db.Database.SqlQuery<ExportDocumentsList>(
            string.Format(@"SELECT sum(sum) as sum, short_name as name from Salary_details,Salaries_employees where salemp_id=salemp_salemp_id and
 sum is not null and convert(varchar(10),date,120) >= '" + startDate.ToString("yyyy-MM-dd") + "' " + "  and convert(varchar(10),date,120) <= '" + endDate.ToString("yyyy-MM-dd") + "' group by short_name")).ToList();
                        ;}
                    else
                    {
                        respList = db.Database.SqlQuery<ExportDocumentsList>(
               string.Format(@"SELECT sum(sum) as sum, short_name as name from Salary_details,Salaries_employees where salemp_id=salemp_salemp_id and
 sum is not null and salemp_id=" + empId + " and convert(varchar(10),date,120) >= '" + startDate.ToString("yyyy-MM-dd") + "' " + "  and convert(varchar(10),date,120) <= '" + endDate.ToString("yyyy-MM-dd") + "' group by short_name")).ToList();
                    }
                    Cell cellDat = GetCell(worksheetPart.Worksheet, "B", 1);
                    cellDat.CellValue = new CellValue("C:  "+startDate.ToString("dd.MM.yyyy"));
                    cellDat.DataType = new EnumValue<CellValues>(CellValues.String);

                    cellDat = GetCell(worksheetPart.Worksheet, "C", 1);
                    cellDat.CellValue = new CellValue("По:  "+endDate.ToString("dd.MM.yyyy"));
                    cellDat.DataType = new EnumValue<CellValues>(CellValues.String);


                    for (int j = 0; j < respList.Count; j++)
                    {
                        ListOfPlantId.Add(respList[j].Id);
                    }

                    uint i = 3;
                    OldShtrId = new List<int>();
                    if (respList.Any())
                    {
                        OldShtrId.Add(respList[0].Id);
                        respList[0].Id1 = i - 2; 
                        Row refRowC = sheetData.Elements<Row>().Where(r => 3 == r.RowIndex).First();
                        SetCellsValueForDocList(respList[0], refRowC, i);
                        respList.Remove(respList[0]);
                        i++;
                    }
                    foreach (var item in respList)
                    {
                        OldShtrId.Add(item.Id);
                        item.Id1 = i - 2;
                        Row refRowC = sheetData.Elements<Row>().Where(r => 3 == r.RowIndex).First();
                        var row = new Row();
                        row = (Row)refRowC.Clone();
                        row.RowIndex = new UInt32Value(i);

                        foreach (var cell in row.Elements<Cell>())
                        {
                            cell.CellReference = new StringValue(cell.CellReference.Value.Replace(refRowC.RowIndex.Value.ToString(), row.RowIndex.ToString()));
                        }
                        SetCellsValueForDocList(item, row, i);
                        sheetData.Append(row);
                        i++;
                    }
                    worksheetPart.Worksheet.Save();
                    document.Close();
                }
                return stream.ToArray();
            }

        }
         public void SetValueForDecimalCell(string st, uint i, decimal? val,Worksheet Worksheet)
         {
          cellDat = GetCell(Worksheet, st, i);
          cellDat.CellValue = new CellValue(val == null ? "" : val.Value.ToString().Replace(',', '.'));
          cellDat.DataType = new EnumValue<CellValues>(CellValues.Number);
         }


         public void SetValueForDecimalCellDrawTotal(string st, uint i, decimal? val, Worksheet Worksheet,
         SpreadsheetDocument document, List<PlanRequestDto> totalplan, string pathTemplate, int j3)
         {
             cellDat = GetCell(Worksheet, st, i);
             cellDat.CellValue = new CellValue(val == null ? "" : val.Value.ToString().Replace(',', '.'));
             cellDat.DataType = new EnumValue<CellValues>(CellValues.Number);
             if (totalplan.Count - 1 == j3)
             {
                 setCellFormat(pathTemplate, totalplan, Worksheet, document);
             }
         }
    
         public void SetValueForStringCellDrawTotal(string st, uint i, string val, Worksheet Worksheet,
        SpreadsheetDocument document, List<PlanRequestDto> totalplan, string pathTemplate, int j3)
         {
             cellDat = GetCell(Worksheet, st, i);
             cellDat.CellValue = new CellValue(val);
             cellDat.DataType = new EnumValue<CellValues>(CellValues.String);

             if (totalplan.Count - 1 == j3)
             {
                 setCellFormat(pathTemplate, totalplan, Worksheet, document);
             }
         }
         public void SetValueForStringCell(string st, uint i, string val, Worksheet Worksheet)
         {
             cellDat = GetCell(Worksheet, st, i);
             cellDat.CellValue = new CellValue(val);
             cellDat.DataType = new EnumValue<CellValues>(CellValues.String);


         }
        //==================================наряд на сдельную работу
         public byte[] GetPieceWorkResExcel(DateTime startDate, DateTime endDate, int empId, string pathTemplate)
         {
             byte[] byteArray = File.ReadAllBytes(pathTemplate);
             using (MemoryStream stream = new MemoryStream())
             {
                 stream.Write(byteArray, 0, (int)byteArray.Length);
                 using (var document = SpreadsheetDocument.Open(stream, true))
                 {
                     var data = spareDb.GetRepairsSpendingsBigListResExcel(empId, startDate, endDate);
                     for (var i = 0; i < data.Count; i++)
                     {
                         for (var j = 0; j < data.Count; j++)
                         {
                             if (i != j && data[i].Date1.Value.ToString("dd.MM.yyyy").Equals(data[j].Date1.Value.ToString("dd.MM.yyyy")) &&
                              data[i].TraktId == data[j].TraktId && data[i].AgrId == data[j].AgrId && data[i].TaskId == data[j].TaskId &&
                              data[i].EmpId == data[j].EmpId)
                             {
                                 data[i].SumCost = (data[i].SumCost ?? 0) + (data[j].SumCost ?? 0);
                                 data[i].Hours = ( data[i].Hours ?? 0) + (data[j].Hours ?? 0);
                                 data.RemoveAt(j); j--;
                             }
                         }
                     }

                 string SheetName; string SheetName2;
                 SheetName = "стр1";   SheetName2 = "стр2";
                 var   sheet = document.WorkbookPart.Workbook.GetFirstChild<Sheets>().Elements<Sheet>().SingleOrDefault(s => s.Name == SheetName);
                 var   worksheetPart = (WorksheetPart)document.WorkbookPart.GetPartById(sheet.Id.Value);
                 var   sheetData = worksheetPart.Worksheet.GetFirstChild<SheetData>();
                 var   sheet2 = document.WorkbookPart.Workbook.GetFirstChild<Sheets>().Elements<Sheet>().SingleOrDefault(s => s.Name == SheetName2);
                 var   worksheetPart2 = (WorksheetPart)document.WorkbookPart.GetPartById(sheet2.Id.Value);
                 var   sheetData2 = worksheetPart.Worksheet.GetFirstChild<SheetData>();

                  sheet.Name = "1 " + data[0].EmpName;  sheet2.Name = "2 " + data[0].EmpName;
                  var nameFirstList = data[0].EmpName;
                  var nameSecondList = data[0].EmpName;
                  int numberFirstList = 2; 
                     //календарь
                  char  ch1 = 'b', ch2 = 'p'; var rows = new List<string>();
                     for (int i = 0; i < 16; i++) {
                         if (ch2 == '{') { ch2 = 'a'; ch1++; }
                         rows.Add(ch1.ToString() + ch2.ToString());
                         ch2++;
                     }

                         //СОЗДАЕМ И ЗАПОЛНЯЕМ листы
                         var empIds = data.Select(t => t.EmpId).Distinct().ToList();
                         for (int i = 0; i < empIds.Count; i++)
                         {
                             var curId = empIds[i];
                             var list = data.Where(t => t.EmpId == curId).OrderBy(t=>t.Date1).ToList();
                             uint marker = 20;
                             if (i != 0)
                             {
                                 CopySheet(pathTemplate, "1 " + nameFirstList, numberFirstList + " " + list[0].EmpName, document);
                                 SheetName = numberFirstList + " " + list[0].EmpName;
                                 sheet = document.WorkbookPart.Workbook.GetFirstChild<Sheets>().Elements<Sheet>().SingleOrDefault(s => s.Name == SheetName);
                                 worksheetPart = (WorksheetPart)document.WorkbookPart.GetPartById(sheet.Id.Value);
                                 sheetData = worksheetPart.Worksheet.GetFirstChild<SheetData>();
                             }
                             //лист1

                             for (int j = 0; j < list.Count; j++)
                             {
                                 if (marker == 29)
                                 {
                                     marker = 20; numberFirstList++;
                                     CopySheet(pathTemplate, "1 " + nameFirstList, numberFirstList + " " + list[0].EmpName, document);
                                     SheetName = numberFirstList + " " + list[0].EmpName;
                                     sheet = document.WorkbookPart.Workbook.GetFirstChild<Sheets>().Elements<Sheet>().SingleOrDefault(s => s.Name == SheetName);
                                     worksheetPart = (WorksheetPart)document.WorkbookPart.GetPartById(sheet.Id.Value);
                                     sheetData = worksheetPart.Worksheet.GetFirstChild<SheetData>();

                                 }

                                 SetValueForStringCell("be", 35, list[j].EmpName, worksheetPart.Worksheet);  //сотрудник
                                 SetValueForStringCell("be", 33, list[j].OtvName, worksheetPart.Worksheet);  //ответв-ный
                                 SetValueForStringCell("o", 14, list[j].OtvName, worksheetPart.Worksheet);  //ответв-ный

                                 var idOrg = Int32.Parse(db.Parameters.Where(t => t.name.Equals("NameOrg")).Select(t => t.value).FirstOrDefault());
                                 var org_name = db.Organizations.Where(t => t.org_id == idOrg).Select(t => t.name).FirstOrDefault();
                                 SetValueForStringCell("m", 9, org_name, worksheetPart.Worksheet); 


                                 SetValueForStringCell("bz", 8, list[0].Date1.Value.Month.ToString(), worksheetPart.Worksheet); //месяц
                                 SetValueForStringCell("ci", 8, list[0].Date1.Value.Year.ToString(), worksheetPart.Worksheet);   //год

                                 SetValueForStringCell("a", marker, list[j].Date1.Value.ToString("dd.MM.yyyy"), worksheetPart.Worksheet);
                                 SetValueForStringCell("g", marker, list[j].TaskName + " " + list[j].Trakt + " " + list[j].Agr, worksheetPart.Worksheet);
                                 SetValueForDecimalCell("db", marker, (decimal?)list[j].RatePiecework, worksheetPart.Worksheet); //расценка
                                 SetValueForDecimalCell("ct", marker, (decimal?)list[j].Hours, worksheetPart.Worksheet); //часы
                                 SetValueForDecimalCell("en", marker, (decimal?)list[j].SumCost, worksheetPart.Worksheet); //summa
                                 marker++;
                             }

                             //лист2
                             if (i != 0)
                             {
                                 numberFirstList++;
                                 CopySheet(pathTemplate, "2 " + nameSecondList, numberFirstList + " " + list[0].EmpName, document);
                                 SheetName2 = numberFirstList + " " + list[0].EmpName;
                                 sheet2 = document.WorkbookPart.Workbook.GetFirstChild<Sheets>().Elements<Sheet>().SingleOrDefault(s => s.Name == SheetName2);
                                 worksheetPart2 = (WorksheetPart)document.WorkbookPart.GetPartById(sheet2.Id.Value);
                                 sheetData2 = worksheetPart.Worksheet.GetFirstChild<SheetData>();
                             }
                     
                             SetValueForStringCell("a", 9, list[0].EmpName, worksheetPart2.Worksheet);
                             SetValueForStringCell("bq", 30, list[0].OtvName, worksheetPart2.Worksheet);  //ответв-ный
                             var days = 0;
                             for (int j = 0; j < list.Count; j++)
                             {
                                 var dayId = list[j].Date1.Value.Day;
                                 var dayHours = list.Where(t => t.Date1.Value.Day == dayId).Sum(t => t.Hours);
                                 if (dayId <= 15)
                                 {
                                    SetValueForDecimalCell(rows[dayId - 1], 9, (decimal?)dayHours, worksheetPart2.Worksheet); //часы
                                    days++;
                                 }
                                 else
                                 {
                                    SetValueForDecimalCell(rows[dayId - 16], 10, (decimal?)dayHours, worksheetPart2.Worksheet); //часы
                                    days++;
                                 }
                             }
                             SetValueForDecimalCell("cf", 9, (decimal?)list.Sum(t=>t.Hours), worksheetPart2.Worksheet); //часы summa
                             SetValueForDecimalCell("cl", 9, (decimal?)days, worksheetPart2.Worksheet);  //дней
                             SetValueForDecimalCell("cr", 9, (decimal?)list.Sum(t => t.SumCost), worksheetPart2.Worksheet); //сумма
                             SetValueForDecimalCell("cf", 27, (decimal?)list.Sum(t => t.Hours), worksheetPart2.Worksheet); //часы summa
                             SetValueForDecimalCell("cl", 27, (decimal?)days, worksheetPart2.Worksheet);  //дней
                             SetValueForDecimalCell("cr", 27, (decimal?)list.Sum(t => t.SumCost), worksheetPart2.Worksheet); //сумма
                        //     numberFirstList=1; 
                         }

                     worksheetPart.Worksheet.Save();
                     worksheetPart2.Worksheet.Save();
                     document.Close();
                 }
                 return stream.ToArray();
             }
         }

        //Планируемые затраты
        public byte[] GetСultResExcel(int cultId, int year, string pathTemplate)
        {
            byte[] byteArray = File.ReadAllBytes(pathTemplate);
            using (MemoryStream stream = new MemoryStream())
            {
                stream.Write(byteArray, 0, (int)byteArray.Length);
                using (var document = SpreadsheetDocument.Open(stream, true))
                {
                    string SheetName;
                  
                    SheetName = "Свод затрат за период";
                    // 1 лист отчета 
                    Sheet sheet;
                    try
                    {
                        sheet = document.WorkbookPart.Workbook.GetFirstChild<Sheets>().Elements<Sheet>().SingleOrDefault(s => s.Name == SheetName);
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(String.Format("Возможно в документе существует два листа с названием \"{0}\"!\n", SheetName), ex);
                    }

                    if (sheet == null)
                    {
                        throw new Exception(String.Format("В шаблоне не найден \"{0}\"!\n", SheetName));
                    }
                    var worksheetPart = (WorksheetPart)document.WorkbookPart.GetPartById(sheet.Id.Value);
                    var sheetData = worksheetPart.Worksheet.GetFirstChild<SheetData>();

                    SetValueForStringCell("A", 2, "Планируемые затраты культур за период в " + year + " году.", worksheetPart.Worksheet);
                    SetValueForStringCell("E", 4, "НЗП на " + year + " год", worksheetPart.Worksheet);
                    //
                    var totalplan = (from tc in db.TC_param_value
                                     where tc.TC.year == year && (cultId != 0 ? tc.id_tc == cultId : true) &&
                                     (tc.id_tc_param==1 ||  tc.id_tc_param==2 || tc.id_tc_param==3 || tc.id_tc_param==7 || tc.id_tc_param==53
                                     || tc.id_tc_param==54 || tc.id_tc_param==55 || tc.id_tc_param==56 || tc.id_tc_param==57
                                     || tc.id_tc_param==60 || tc.id_tc_param==61 || tc.id_tc_param==35 || tc.id_tc_param==38
                                     || tc.id_tc_param==39 || tc.id_tc_param==59 || tc.id_tc_param==19 || tc.id_tc_param==25
                                     || tc.id_tc_param==22 || tc.id_tc_param==33)
                                     group new
                                     {
                                       
                                     }
                                         by new
                                         {
                                            plant_name = (tc.TC.Type_sorts_plants.name == null || (tc.TC.Type_sorts_plants.name.Length + tc.TC.Plants.name.Length > 27)) ? tc.TC.Plants.name : tc.TC.Plants.name + " (" + tc.TC.Type_sorts_plants.name + ")",
                                            id_tc = tc.id_tc,
                                            id_tc_param = tc.id_tc_param,
                                            value = tc.value,
                                            id = tc.id,
                                            grtask_id = tc.TC_operation.Type_tasks.auxiliary_rate,
                                         } into g
                                     select new PlanRequestDto
                                     {
                                         plant_name = g.Key.plant_name,
                                         id_tc = g.Key.id_tc,
                                         id_tc_param1 = g.Key.id_tc_param,
                                         value = (float?)g.Key.value,
                                         id = g.Key.id,
                                         grtask_id = g.Key.grtask_id,
                                     }).OrderBy(l=>l.plant_name).ToList();

                    int j3 = 0;
                    var totplan = totalplan.Select(s => s.id_tc).Distinct().ToList();
                    for ( j3 = 0; j3 < totplan.Count; j3++)
                    {
                         int id = totplan[j3];
                        var area = totalplan.Where(s => s.id_tc == totplan[j3] && s.id_tc_param1==1).Select(s => s.value).FirstOrDefault();
                        var gross_harvest = totalplan.Where(s => s.id_tc == totplan[j3] && s.id_tc_param1 == 3).Select(s => s.value).FirstOrDefault();
                        var dop_material = totalplan.Where(s => s.id_tc == totplan[j3] && s.id_tc_param1 == 61).Select(s => s.value).FirstOrDefault();
                         var dop_material_ga = area == 0 ? 0 : (dop_material == null ? 0 : dop_material) / area;
                        var salary = totalplan.Where(s => s.id_tc == totplan[j3] && s.id_tc_param1 == 35 && s.grtask_id==1).Select(s => s.value).Sum();
                        var dopsalary = totalplan.Where(s => s.id_tc == totplan[j3] && s.id_tc_param1 == 38 && s.grtask_id == 1).Select(s => s.value).Sum();
                         var  deduction = (salary == null ? 0 : salary * 0.321) + (dopsalary == null ? 0 : dopsalary * 0.321);
                         //орошение
                         var salary_watering = totalplan.Where(s => s.id_tc == totplan[j3] && s.id_tc_param1 == 35 && s.grtask_id == 2).Select(s => s.value).Sum();
                         var dopsalary_watering = totalplan.Where(s => s.id_tc == totplan[j3] && s.id_tc_param1 == 38 && s.grtask_id == 2).Select(s => s.value).Sum();
                         var deduction_watering = (salary_watering == null ? 0 : salary_watering * 0.321) + (dopsalary_watering == null ? 0 : dopsalary_watering * 0.321);
                        //
                        var total_salary = (salary == null ? 0 : salary) + (dopsalary == null ? 0 : dopsalary) + (deduction == null ? 0 : deduction)+
                           (salary_watering == null ? 0 : salary_watering) + (dopsalary_watering == null ? 0 : dopsalary_watering)
                           + (deduction_watering == null ? 0 : deduction_watering);
                        var workload = totalplan.Where(s => s.id_tc == totplan[j3] && s.id_tc_param1 == 33 && s.grtask_id == 1).Select(s => s.value).ToList();
                        var workload_watering = totalplan.Where(s => s.id_tc == totplan[j3] && s.id_tc_param1 == 33 && s.grtask_id == 2).Select(s => s.value).ToList();
                        var fuelConsumption = totalplan.Where(s => s.id_tc == totplan[j3] && s.id_tc_param1 == 59 && s.grtask_id == 1).Select(s => s.value).ToList();
                        float? consumption = 0;
                        for (int b = 0; b < fuelConsumption.Count; b++)
                        {
                             consumption = consumption + fuelConsumption[b] * workload[b];
                        }
                        var fuel_cost = totalplan.Where(s => s.id_tc == totplan[j3] && s.id_tc_param1 == 39 && s.grtask_id == 1).Select(s => s.value).Sum();
                     var fuel_cost_ga = area == 0 ? 0 : (consumption == null ? 0 : consumption) / area;

                        //топливо орошение
                     var fuelConsumption_watering = totalplan.Where(s => s.id_tc == totplan[j3] && s.id_tc_param1 == 59 && s.grtask_id == 2).Select(s => s.value).ToList();
                     float? consumption_watering = 0;
                     for (int b = 0; b < fuelConsumption_watering.Count; b++)
                     {
                         consumption_watering = consumption_watering + fuelConsumption_watering[b] * workload_watering[b];
                     }
                     var fuel_cost_watering = totalplan.Where(s => s.id_tc == totplan[j3] && s.id_tc_param1 == 39 && s.grtask_id == 2).Select(s => s.value).Sum();
                     var fuel_cost_ga_watering = area == 0 ? 0 : (consumption_watering == null ? 0 : consumption_watering) / area;
                        //
                     var total_consumption = consumption + consumption_watering;
                      var chemicalFertilizers = totalplan.Where(s => s.id_tc == totplan[j3] && s.id_tc_param1 == 22).Select(s => s.value).FirstOrDefault();
                      var  szr = totalplan.Where(s => s.id_tc == totplan[j3] && s.id_tc_param1 == 25).Select(s => s.value).FirstOrDefault();
                      var seed = totalplan.Where(s => s.id_tc == totplan[j3] && s.id_tc_param1 == 19).Select(s => s.value).FirstOrDefault();
                      var total_cost = (fuel_cost ?? 0) + (fuel_cost_watering ?? 0);  //итого по топливу

                       var nzp = totalplan.Where(s => s.id_tc == totplan[j3] && s.id_tc_param1 == 7).Select(s => s.value).FirstOrDefault();
                       var   combine = db.TC_to_service.Where(t => t.TC.year == year && t.id_service == 1 && t.id_tc == id).Select(v => (float?)v.cost ).Sum();
                       var   loader = db.TC_to_service.Where(t => t.TC.year == year && t.id_service == 2 && t.id_tc == id).Select(v => (float?)v.cost).Sum();
                       var autotransport = db.TC_to_service.Where(t => t.TC.year == year && t.id_service == 3 && t.id_tc == id).Select(v => (float?)v.cost).Sum();
                      var  desiccation = db.TC_to_service.Where(t => t.TC.year == year && t.id_service == 4 && t.id_tc == id).Select(v => (float?)v.cost).Sum();
                      var analyzes = db.TC_to_service.Where(t => t.TC.year == year && t.id_service == 5 && t.id_tc == id).Select(v => (float?)v.cost).Sum();
                      var total_service = db.TC_to_service.Where(t => t.TC.year == year && t.id_tc == id).Select(v => (float?)v.cost).Sum();
                      var total = (nzp == null ? 0 : nzp) + (total_salary == null ? 0 : total_salary) + (total_cost == null ? 0 : total_cost)
                       + (seed == null ? 0 : seed) + (szr == null ? 0 : szr) + (chemicalFertilizers == null ? 0 : chemicalFertilizers)
                       + (total_service == null ? 0 : total_service) + (dop_material == null ? 0 : dop_material);
                        totalplan1.Add(new PlanRequestDto
                        {
                        id_tc = id,
                        plant_name = totalplan.Where(s => s.id_tc == totplan[j3]).Select(s => s.plant_name).FirstOrDefault(),
                        area = (decimal?)area,
                        productivity = totalplan.Where(s => s.id_tc == totplan[j3] && s.id_tc_param1 == 2).Select(s => s.value).FirstOrDefault(),
                        gross_harvest = gross_harvest,
                        nzp = (decimal?) nzp,
                        nzp_zp = totalplan.Where(s => s.id_tc == totplan[j3] && s.id_tc_param1 == 53).Select(s => s.value).FirstOrDefault(),
                        nzp_fuel = totalplan.Where(s => s.id_tc == totplan[j3] && s.id_tc_param1 == 54).Select(s => s.value).FirstOrDefault(),
                        nzp_szr = totalplan.Where(s => s.id_tc == totplan[j3] && s.id_tc_param1 == 55).Select(s => s.value).FirstOrDefault(),
                        nzp_chemicalFertilizers = totalplan.Where(s => s.id_tc == totplan[j3] && s.id_tc_param1 == 56).Select(s => s.value).FirstOrDefault(),
                        nzp_seed = totalplan.Where(s => s.id_tc == totplan[j3] && s.id_tc_param1 == 57).Select(s => s.value).FirstOrDefault(),
                        nzp_dop_material = totalplan.Where(s => s.id_tc == totplan[j3] && s.id_tc_param1 == 60).Select(s => s.value).FirstOrDefault(),
                        dop_material = (decimal?)dop_material,
                        dop_material_ga = dop_material_ga,
                        salary = (decimal?)salary,
                        dopsalary = (decimal?)dopsalary,
                        salary_watering = (decimal?)salary_watering,
                        dopsalary_watering = (decimal?)dopsalary_watering,
                        deduction_watering = (decimal?)deduction_watering,
                        fuel_cost = (decimal?)fuel_cost,
                        fuel_cost_watering = (decimal?)fuel_cost_watering,
                        deduction = (decimal?)deduction,
                        total_salary = (decimal?)total_salary,
                        consumption = (decimal?)consumption,
                        consumption_watering = (decimal?)consumption_watering,
                        fuel_cost_ga = fuel_cost_ga,
                        fuel_cost_ga_watering = fuel_cost_ga_watering,
                        total_consumption = (decimal?)total_consumption,
                        total_cost = (decimal?)total_cost,  //итого по топливу
                        seed = (decimal?)seed,
                        szr = (decimal?)szr,
                        szr_ga = area == 0 ? 0 : (szr == null ? 0 : szr) / area,
                        chemicalFertilizers = (decimal?)chemicalFertilizers,
                        chemicalFertilizers_ga = area == 0 ? 0 : (chemicalFertilizers == null ? 0 : chemicalFertilizers) / area,
                        combine = (decimal?)combine,
                        loader = (decimal?)loader,
                        autotransport = (decimal?)autotransport,
                        desiccation = (decimal?)desiccation,
                        analyzes = (decimal?)analyzes,
                        total_service = (decimal?)total_service,
                        total = (decimal?)total,
                        total_ga = area == 0 ? 0 : total / area,
                        total_t = gross_harvest == 0 ? 0 : total / gross_harvest,
                        });     
                    }

                    totalplan1.Add(new PlanRequestDto
                    {
                     plant_name = "Итого",
                     area = totalplan1.Sum(l => l.area),
                     nzp = totalplan1.Sum(l => Math.Round((l.nzp ?? 0),2)),
                            salary = totalplan1.Sum(l => l.salary),
                            dopsalary = totalplan1.Sum(l => l.dopsalary),
                            deduction = totalplan1.Sum(l => l.deduction),
                            salary_watering = totalplan1.Sum(l => l.salary_watering),
                            dopsalary_watering = totalplan1.Sum(l => l.dopsalary_watering),
                            deduction_watering = totalplan1.Sum(l => l.deduction_watering),
                            total_salary = totalplan1.Sum(l => l.total_salary),
                            fuel_cost = totalplan1.Sum(l => l.fuel_cost),
                            consumption = totalplan1.Sum(l => l.consumption),
                            fuel_cost_watering = totalplan1.Sum(l => l.fuel_cost_watering),
                            consumption_watering = totalplan1.Sum(l => l.consumption_watering),
                            total_consumption = totalplan1.Sum(l => l.total_consumption),
                            dop_material = totalplan1.Sum(l => l.dop_material),
                            total_cost = totalplan1.Sum(l => l.total_cost),
                            seed = totalplan1.Sum(l => l.seed),
                            szr = totalplan1.Sum(l => l.szr),
                            chemicalFertilizers = totalplan1.Sum(l => l.chemicalFertilizers),
                            combine = totalplan1.Sum(l => l.combine),
                            loader = totalplan1.Sum(l => l.loader),
                            autotransport = totalplan1.Sum(l => l.autotransport),
                            desiccation = totalplan1.Sum(l => l.desiccation),
                            analyzes = totalplan1.Sum(l => l.analyzes),
                            total_service = totalplan1.Sum(l => l.total_service),
                            total = totalplan1.Sum(l => l.total),
                    });



                    j3 = 0;
                    for (uint i3 = 6; j3 < totalplan1.Count; i3++)
                    {
                        SetValueForStringCell("A", i3, totalplan1[j3].plant_name, worksheetPart.Worksheet);
                        if (totalplan1.Count - 1 == j3) {
                            setCellFormat(pathTemplate, totalplan1, worksheetPart.Worksheet, document);
                        
                        
                        }
                        SetValueForStringCell("A", (uint)(totalplan1.Count + 11 + j3), totalplan1[j3].plant_name, worksheetPart.Worksheet);
                        if (totalplan1.Count - 1 == j3)
                        {
                            setCellFormat(pathTemplate, totalplan1, worksheetPart.Worksheet, document);       
                        }

                        SetValueForDecimalCellDrawTotal("B", i3, (decimal?)totalplan1[j3].area, worksheetPart.Worksheet, document, totalplan1, pathTemplate, j3);
                        SetValueForDecimalCellDrawTotal("C", i3, (decimal?)totalplan1[j3].productivity, worksheetPart.Worksheet, document, totalplan1, pathTemplate, j3);
                        SetValueForDecimalCellDrawTotal("D", i3, (decimal?)totalplan1[j3].gross_harvest, worksheetPart.Worksheet, document, totalplan1, pathTemplate, j3); ;
                        SetValueForDecimalCellDrawTotal("E", i3, (decimal?)totalplan1[j3].nzp, worksheetPart.Worksheet, document, totalplan1, pathTemplate, j3);
                        SetValueForDecimalCellDrawTotal("F", i3, (decimal?)totalplan1[j3].salary, worksheetPart.Worksheet, document, totalplan1, pathTemplate, j3);
                        SetValueForDecimalCellDrawTotal("G", i3, (decimal?)totalplan1[j3].dopsalary, worksheetPart.Worksheet, document, totalplan1, pathTemplate, j3);
                        SetValueForDecimalCellDrawTotal("H", i3, (decimal?)totalplan1[j3].deduction, worksheetPart.Worksheet, document, totalplan1, pathTemplate, j3);
                        //орошение
                        SetValueForDecimalCellDrawTotal("I", i3, ((decimal?)totalplan1[j3].salary_watering ?? 0) + ((decimal?)totalplan1[j3].dopsalary_watering ?? 0), worksheetPart.Worksheet, document, totalplan1, pathTemplate, j3);
                        SetValueForDecimalCellDrawTotal("J", i3, (decimal?)totalplan1[j3].deduction_watering, worksheetPart.Worksheet, document, totalplan1, pathTemplate, j3);
                        //
                        SetValueForStringCellDrawTotal("U", i3, "", worksheetPart.Worksheet, document, totalplan1, pathTemplate, j3);
                        SetValueForStringCellDrawTotal("V", i3, "", worksheetPart.Worksheet, document, totalplan1, pathTemplate, j3);

                        for (char c = 'A'; c <= 'F';c++ )
                        {
                            SetValueForStringCellDrawTotal("A" + c, i3, "", worksheetPart.Worksheet, document, totalplan1, pathTemplate, j3);
                        }
                        for (char c = 'B'; c <= 'Z'; c++)
                        {
                            SetValueForStringCellDrawTotal(c.ToString(), (uint)(totalplan1.Count + 11 + j3), "", worksheetPart.Worksheet, document, totalplan1, pathTemplate, j3);
                        }
                        for (char c = 'A'; c <= 'O'; c++)
                        {
                            SetValueForStringCellDrawTotal("A" + c, (uint)(totalplan1.Count + 11 + j3), "", worksheetPart.Worksheet, document, totalplan1, pathTemplate, j3);
                        }
                        SetValueForDecimalCellDrawTotal("K", i3, (decimal?)totalplan1[j3].total_salary, worksheetPart.Worksheet, document, totalplan1, pathTemplate, j3);
                        SetValueForDecimalCellDrawTotal("L", i3, (decimal?)totalplan1[j3].fuel_cost, worksheetPart.Worksheet, document, totalplan1, pathTemplate, j3);
                        SetValueForDecimalCellDrawTotal("M", i3, (decimal?)totalplan1[j3].consumption, worksheetPart.Worksheet, document, totalplan1, pathTemplate, j3);
                        SetValueForDecimalCellDrawTotal("N", i3, (decimal?)totalplan1[j3].fuel_cost_ga, worksheetPart.Worksheet, document, totalplan1, pathTemplate, j3);

                        SetValueForDecimalCellDrawTotal("o", i3, (decimal?)totalplan1[j3].fuel_cost_watering, worksheetPart.Worksheet, document, totalplan1, pathTemplate, j3);
                        SetValueForDecimalCellDrawTotal("p", i3, (decimal?)totalplan1[j3].consumption_watering, worksheetPart.Worksheet, document, totalplan1, pathTemplate, j3);
                        SetValueForDecimalCellDrawTotal("q", i3, (decimal?)totalplan1[j3].fuel_cost_ga_watering, worksheetPart.Worksheet, document, totalplan1, pathTemplate, j3);

                        SetValueForDecimalCellDrawTotal("R", i3, (decimal?)totalplan1[j3].total_consumption, worksheetPart.Worksheet, document, totalplan1, pathTemplate, j3);
                        SetValueForDecimalCellDrawTotal("S", i3, (decimal?)totalplan1[j3].total_cost, worksheetPart.Worksheet, document, totalplan1, pathTemplate, j3);
                        SetValueForDecimalCellDrawTotal("T", i3, (decimal?)totalplan1[j3].seed, worksheetPart.Worksheet, document, totalplan1, pathTemplate, j3);
                        SetValueForDecimalCellDrawTotal("W", i3, (decimal?)totalplan1[j3].szr, worksheetPart.Worksheet, document, totalplan1, pathTemplate, j3);
                        SetValueForDecimalCellDrawTotal("X", i3, (decimal?)totalplan1[j3].szr_ga, worksheetPart.Worksheet, document, totalplan1, pathTemplate, j3);
                        SetValueForDecimalCellDrawTotal("Y", i3, (decimal?)totalplan1[j3].chemicalFertilizers, worksheetPart.Worksheet, document, totalplan1, pathTemplate, j3);
                        SetValueForDecimalCellDrawTotal("Z", i3, (decimal?)totalplan1[j3].chemicalFertilizers_ga, worksheetPart.Worksheet, document, totalplan1, pathTemplate, j3);
                        SetValueForDecimalCellDrawTotal("AC", i3, (decimal?)totalplan1[j3].dop_material, worksheetPart.Worksheet, document, totalplan1, pathTemplate, j3);
                        SetValueForDecimalCellDrawTotal("AD", i3, (decimal?)totalplan1[j3].dop_material_ga, worksheetPart.Worksheet, document, totalplan1, pathTemplate, j3);
                        SetValueForDecimalCellDrawTotal("AG", i3, (decimal?)totalplan1[j3].combine, worksheetPart.Worksheet, document, totalplan1, pathTemplate, j3);
                        SetValueForDecimalCellDrawTotal("AH", i3, (decimal?)totalplan1[j3].loader, worksheetPart.Worksheet, document, totalplan1, pathTemplate, j3);
                        SetValueForDecimalCellDrawTotal("AI", i3, (decimal?)totalplan1[j3].autotransport, worksheetPart.Worksheet, document, totalplan1, pathTemplate, j3); ;
                        SetValueForDecimalCellDrawTotal("AJ", i3, (decimal?)totalplan1[j3].desiccation, worksheetPart.Worksheet, document, totalplan1, pathTemplate, j3);
                        SetValueForDecimalCellDrawTotal("AK", i3, (decimal?)totalplan1[j3].analyzes, worksheetPart.Worksheet, document, totalplan1, pathTemplate, j3);
                        SetValueForDecimalCellDrawTotal("AL", i3, (decimal?)totalplan1[j3].total_service, worksheetPart.Worksheet, document, totalplan1, pathTemplate, j3);
                        SetValueForDecimalCellDrawTotal("AM", i3, (decimal?)totalplan1[j3].total, worksheetPart.Worksheet, document, totalplan1, pathTemplate, j3);
                        SetValueForDecimalCellDrawTotal("AN", i3, (decimal?)totalplan1[j3].total_ga, worksheetPart.Worksheet, document, totalplan1, pathTemplate, j3);
                        SetValueForDecimalCellDrawTotal("AO", i3, (decimal?)totalplan1[j3].total_t, worksheetPart.Worksheet, document, totalplan1, pathTemplate, j3);
                        j3++;
                    }

                    SetValueForStringCell("A", (uint)totplan.Count + 7, "Инвестиционные расходы на " + year + " год.", worksheetPart.Worksheet);
                    ///
                    for (char c = 'A'; c <= 'Z'; c++)
                    {
                        setHeader(pathTemplate, totalplan1, worksheetPart.Worksheet, document, c.ToString(), "4", 9);
                        setHeader(pathTemplate, totalplan1, worksheetPart.Worksheet, document, c.ToString(), "5", 10);
                    }
                    for (char c = 'A'; c <= 'O'; c++)
                    {
                        setHeader(pathTemplate, totalplan1, worksheetPart.Worksheet, document, "A" + c.ToString(), "4", 9);
                        setHeader(pathTemplate, totalplan1, worksheetPart.Worksheet, document, "A" + c.ToString(), "5", 10);
                    }
                    SetValueForStringCell("E", (uint)totplan.Count + 10, "НЗП на " + year + " год", worksheetPart.Worksheet);
               //
                    var months = (from tc in db.TC_param_value 
                                  where tc.TC_operation.date_start.Value.Year == year && (tc.id_tc_param == 35
                                  || tc.id_tc_param == 39 || tc.id_tc_param == 42 || tc.id_tc_param == 38) &&
                                  (cultId != 0 ? tc.id_tc == cultId : true)
                                  group new {}
                                      by new PlanMonthRequestDto
                                      {
                                          avg = (float?)tc.value / (DbFunctions.DiffDays(tc.TC_operation.date_start, tc.TC_operation.date_finish) + 1),
                                          date_start = tc.TC_operation.date_start,
                                          date_finish = tc.TC_operation.date_finish,
                                          kol = DbFunctions.DiffDays(tc.TC_operation.date_start, tc.TC_operation.date_finish) + 1,
                                          id_tc_param = tc.id_tc_param,
                                          mat_type_id = tc.Materials.mat_type_id,
                                          id = tc.id,
                                          id_tc = tc.id_tc
                                      } into g
                                  select new PlanMonthRequestDto
                                  {
                                      avg = g.Key.avg,
                                      date_start = g.Key.date_start,
                                      date_finish = g.Key.date_finish,
                                      kol = g.Key.kol,
                                      id_tc_param = g.Key.id_tc_param,
                                      mat_type_id = g.Key.mat_type_id,
                                      id = g.Key.id,
                                      id_tc = g.Key.id_tc
                                  }).ToList();
                    //

                    //Детализация затрат по культуре
                    SheetName = "Детализация по культуре";
                    sheet = document.WorkbookPart.Workbook.GetFirstChild<Sheets>().Elements<Sheet>().SingleOrDefault(s => s.Name == SheetName);
                    worksheetPart = (WorksheetPart)document.WorkbookPart.GetPartById(sheet.Id.Value);
                    sheetData = worksheetPart.Worksheet.GetFirstChild<SheetData>();
                    sheet.Name = "1 "+ totalplan1[0].plant_name;
                    //////////////////////////



                    //СОЗДАЕМ И ЗАПОЛНЯЕМ ЛИСТЫ
                    ///
                    for (int i = 0; i < totalplan1.Count - 1; i++)
                    {
                        if (i != 0)
                        {
                           CopySheet(pathTemplate, "1 " + totalplan1[0].plant_name, i + 1 + " " + totalplan1[i].plant_name, document);
                           SheetName = i + 1 + " " + totalplan1[i].plant_name;
                           sheet = document.WorkbookPart.Workbook.GetFirstChild<Sheets>().Elements<Sheet>().SingleOrDefault(s => s.Name == SheetName);
                           worksheetPart = (WorksheetPart)document.WorkbookPart.GetPartById(sheet.Id.Value);
                           sheetData = worksheetPart.Worksheet.GetFirstChild<SheetData>();
                        }

                        SetValueForStringCell("A", 1, "Планируемые затраты на " + totalplan1[i].plant_name + " за " + year + " год", worksheetPart.Worksheet);
                        SetValueForDecimalCell("B", 4, (decimal?)totalplan1[i].nzp_zp, worksheetPart.Worksheet);
                        SetValueForDecimalCell("B", 5, (decimal?)(totalplan1[i].nzp_zp * 0.13), worksheetPart.Worksheet);
                        SetValueForDecimalCell("B", 6, (decimal?)(totalplan1[i].nzp_zp * 0.321), worksheetPart.Worksheet);
                        SetValueForDecimalCell("B", 7, (decimal?)totalplan1[i].nzp_fuel, worksheetPart.Worksheet);
                        SetValueForDecimalCell("B", 8, (decimal?)totalplan1[i].nzp_seed, worksheetPart.Worksheet);
                        SetValueForDecimalCell("B", 9, (decimal?)totalplan1[i].nzp_szr, worksheetPart.Worksheet);
                        SetValueForDecimalCell("B", 10, (decimal?)totalplan1[i].nzp_chemicalFertilizers, worksheetPart.Worksheet);
                        SetValueForDecimalCell("B", 15, (decimal?)totalplan1[i].nzp_dop_material, worksheetPart.Worksheet);
                        SetValueForDecimalCell("B", 28, ((decimal?)(totalplan1[i].nzp_zp ?? 0) + ((decimal?)(totalplan1[i].nzp_zp * 0.321 ?? 0)) + (decimal?)(totalplan1[i].nzp_fuel ?? 0) +
                        (decimal?)(totalplan1[i].nzp_seed ?? 0) + (decimal?)(totalplan1[i].nzp_szr ?? 0) + (decimal?)(totalplan1[i].nzp_chemicalFertilizers ?? 0)
                        + (decimal?)(totalplan1[i].nzp_dop_material ?? 0)), worksheetPart.Worksheet);

                        SetValueForDecimalCell("C", 4, (decimal?)((totalplan1[i].salary ?? 0) + (totalplan1[i].dopsalary ?? 0) + (totalplan1[i].salary_watering ?? 0) + (totalplan1[i].dopsalary_watering ?? 0)), worksheetPart.Worksheet);
                        SetValueForDecimalCell("C", 5, (decimal?)((double?)((totalplan1[i].salary ?? 0) + (totalplan1[i].dopsalary ?? 0)) * 0.13 + (double?)((totalplan1[i].salary_watering ?? 0) + (totalplan1[i].dopsalary_watering ?? 0)) * 0.13), worksheetPart.Worksheet);
                        SetValueForDecimalCell("C", 6, (decimal?)((double?)((totalplan1[i].salary ?? 0) + (totalplan1[i].dopsalary ?? 0)) * 0.321 + (double?)((totalplan1[i].salary_watering ?? 0) + (totalplan1[i].dopsalary_watering ?? 0)) * 0.321), worksheetPart.Worksheet);
                        SetValueForDecimalCell("C", 7, (decimal?)(totalplan1[i].fuel_cost ?? 0) + (totalplan1[i].fuel_cost_watering ?? 0), worksheetPart.Worksheet);
                        SetValueForDecimalCell("C", 8, (decimal?)totalplan1[i].seed, worksheetPart.Worksheet);
                        SetValueForDecimalCell("C", 9, (decimal?)totalplan1[i].szr, worksheetPart.Worksheet);
                        SetValueForDecimalCell("C", 10, (decimal?)totalplan1[i].chemicalFertilizers, worksheetPart.Worksheet);
                        SetValueForDecimalCell("C", 15, (decimal?)totalplan1[i].dop_material, worksheetPart.Worksheet);
                        SetValueForDecimalCell("C", 18, (decimal?)totalplan1[i].total_service, worksheetPart.Worksheet);
                        SetValueForDecimalCell("C", 28, ((decimal?)(totalplan1[i].salary ?? 0) + (decimal?)(totalplan1[i].dopsalary ?? 0) +
                         (totalplan1[i].salary_watering ?? 0) + (decimal?)(totalplan1[i].dopsalary_watering ?? 0)
                          + (decimal?)((double?)(totalplan1[i].salary ?? 0) * 0.321) + (decimal?)((double?)(totalplan1[i].dopsalary ?? 0) * 0.321) +
                          +(decimal?)((double?)(totalplan1[i].salary_watering ?? 0) * 0.321) + (decimal?)((double?)(totalplan1[i].dopsalary_watering ?? 0) * 0.321) +
                            (decimal?)(totalplan1[i].fuel_cost_watering ?? 0) +
                        (decimal?)(totalplan1[i].fuel_cost ?? 0) + (decimal?)(totalplan1[i].seed ?? 0) + (decimal?)(totalplan1[i].szr ?? 0) + (decimal?)(totalplan1[i].chemicalFertilizers ?? 0) +
                       (decimal?)(totalplan1[i].dop_material ?? 0) + (decimal?)(totalplan1[i].total_service ?? 0)), worksheetPart.Worksheet);

                        //Разбивка по месяцам
                        char m = 'D';
                        for (int j = 1; j <= 12; j++)
                        {
                            var mon = (decimal?)getValueForMonth(j, months.Where(l => l.id_tc == totalplan1[i].id_tc).ToList(), year, null, 35, 38);
                            var f = (decimal?)getValueForMonth(j, months.Where(l => l.id_tc == totalplan1[i].id_tc).ToList(), year, null, 39, null);

                            SetValueForDecimalCell(m.ToString(), 4, (mon), worksheetPart.Worksheet);
                            SetValueForDecimalCell(m.ToString(), 5, ((decimal?)((double?)mon * 0.13)), worksheetPart.Worksheet);
                            SetValueForDecimalCell(m.ToString(), 6, ((decimal?)((double?)mon * 0.321)), worksheetPart.Worksheet);
                            SetValueForDecimalCell(m.ToString(), 7, (f), worksheetPart.Worksheet);
                            var s = (decimal?)getValueForMonth(j, months.Where(l => l.id_tc == totalplan1[i].id_tc).ToList(), year, 4, 42, null);
                            SetValueForDecimalCell(m.ToString(), 8, (s), worksheetPart.Worksheet);
                            var szr = (decimal?)getValueForMonth(j, months.Where(l => l.id_tc == totalplan1[i].id_tc).ToList(), year, 1, 42, null);
                            SetValueForDecimalCell(m.ToString(), 9, (szr), worksheetPart.Worksheet);
                            var ud = (decimal?)getValueForMonth(j, months.Where(l => l.id_tc == totalplan1[i].id_tc).ToList(), year, 2, 42, null);
                            SetValueForDecimalCell(m.ToString(), 10, (ud), worksheetPart.Worksheet);
                            var pr = (decimal?)getValueForMonth(j, months.Where(l => l.id_tc == totalplan1[i].id_tc).ToList(), year, 5, 42, null);
                            SetValueForDecimalCell(m.ToString(), 15, (pr), worksheetPart.Worksheet);
                            SetValueForDecimalCell(m.ToString(), 28, (mon + (decimal?)((double?)mon * 0.321) + f + s + szr + ud + pr), worksheetPart.Worksheet);
                            m++;
                        }
                    }
                
                    worksheetPart.Worksheet.Save();
                    document.Close();
                }
                return stream.ToArray();
            }

        }
      //
        /// себестоимость по культурам
        public byte[] GetPlanFactResExcel(DateTime? startDate,DateTime? endDate, int? plantId, string pathTemplate,Boolean detal,
            Boolean plan, Boolean fact, string SelectDate)
        {
            byte[] byteArray = File.ReadAllBytes(pathTemplate);
            using (MemoryStream stream = new MemoryStream())
            {
                stream.Write(byteArray, 0, (int)byteArray.Length);
                using (var document = SpreadsheetDocument.Open(stream, true))
                {
                    string SheetName;

                    SheetName = "Свод затрат за период";
               //     if (plan && fact) { SheetName = "план затрат на 2017 год"; }

                    // 1 лист отчета 
                    Sheet sheet;
                    try
                    {
                        sheet = document.WorkbookPart.Workbook.GetFirstChild<Sheets>().Elements<Sheet>().SingleOrDefault(s => s.Name == SheetName);
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(String.Format("Возможно в документе существует два листа с названием \"{0}\"!\n", SheetName), ex);
                    }

                    if (sheet == null)
                    {
                        throw new Exception(String.Format("В шаблоне не найден \"{0}\"!\n", SheetName));
                    }
                    var worksheetPart = (WorksheetPart)document.WorkbookPart.GetPartById(sheet.Id.Value);
                    var sheetData = worksheetPart.Worksheet.GetFirstChild<SheetData>();

                    if (plan && !fact)
                    {
                        SetValueForStringCell("A", 2, "Планируемые затраты культур за период c " + startDate.Value.ToString("dd.MM.yyyy") + " по " + endDate.Value.ToString("dd.MM.yyyy"), worksheetPart.Worksheet);
                        SetValueForStringCell("E", 4, "НЗП на " + startDate.Value.Year + " год", worksheetPart.Worksheet);
                    }

                    if (!plan && fact)
                    {
                SetValueForStringCell("A", 2, "Фактические затраты культур за период c " + startDate.Value.ToString("dd.MM.yyyy") + " по " + endDate.Value.ToString("dd.MM.yyyy"), worksheetPart.Worksheet);
                SetValueForStringCell("E", 4, "НЗП на " + startDate.Value.Year + " год", worksheetPart.Worksheet);
                    }
                    if (plan && fact)
                    {
                        SetValueForStringCell("A", 2, "План-факт затраты культур за период c " + startDate.Value.ToString("dd.MM.yyyy") + " по " + endDate.Value.ToString("dd.MM.yyyy"), worksheetPart.Worksheet);  
                    }
                  
                    //
                    //ЗА ПЕРИОД
                    //
                    int j3 = 0; 

                    //НЗП для всех
                    var sevoborot = getTotalNZP(startDate);
                    //
                    if (plan && !fact)  ////////////////////ТОЛЬКО ПЛАН
                    {
                        if (SelectDate.Equals("-1"))
                        {
                            totalplan1 = getTotalPlan(startDate, endDate, plantId, sevoborot);
                        }
                        else {
                            var nDate = Convert.ToDateTime(SelectDate);
                            totalplan1 = getTotalPlanSectionTechcard(startDate, endDate, plantId, sevoborot, nDate);
                        }

                    }
                    if (!plan && fact) ////////////////ТОЛЬКО ФАКТ
                    {
                        totalplan1 = getTotalFact(startDate, endDate, plantId, sevoborot);
                    
                    }
                
                    if (!(plan && fact)) {
                   totalplan1 = sumTotalPlanFact(totalplan1);
                 
                    //Вывод первого листа
                    j3 = 0;
                    for (uint i3 = 6; j3 < totalplan1.Count; i3++)
                    {
                        SetValueForStringCell("A", i3, totalplan1[j3].plant_name, worksheetPart.Worksheet);
                        if (totalplan1.Count - 1 == j3)
                        {
                            setCellFormat(pathTemplate, totalplan1, worksheetPart.Worksheet, document);
                        }
                        SetValueForStringCell("A", (uint)(totalplan1.Count + 11 + j3), totalplan1[j3].plant_name, worksheetPart.Worksheet);
                        if (totalplan1.Count - 1 == j3)
                        {
                            setCellFormat(pathTemplate, totalplan1, worksheetPart.Worksheet, document);
                        }
                        SetValueForDecimalCellDrawTotal("B", i3, (decimal?)totalplan1[j3].area, worksheetPart.Worksheet, document, totalplan1, pathTemplate, j3);
                        SetValueForDecimalCellDrawTotal("C", i3, (decimal?)totalplan1[j3].productivity, worksheetPart.Worksheet, document, totalplan1, pathTemplate, j3);
                        SetValueForDecimalCellDrawTotal("D", i3, (decimal?)totalplan1[j3].gross_harvest, worksheetPart.Worksheet, document, totalplan1, pathTemplate, j3); ;
                        SetValueForDecimalCellDrawTotal("E", i3, (decimal?)totalplan1[j3].nzp, worksheetPart.Worksheet, document, totalplan1, pathTemplate, j3);
                        SetValueForDecimalCellDrawTotal("F", i3, (decimal?)totalplan1[j3].salary, worksheetPart.Worksheet, document, totalplan1, pathTemplate, j3);
                        SetValueForDecimalCellDrawTotal("G", i3, (decimal?)totalplan1[j3].dopsalary, worksheetPart.Worksheet, document, totalplan1, pathTemplate, j3);
                        SetValueForDecimalCellDrawTotal("H", i3, (decimal?)totalplan1[j3].deduction, worksheetPart.Worksheet, document, totalplan1, pathTemplate, j3);
                        //орошение
                        SetValueForDecimalCellDrawTotal("I", i3, ((decimal?)totalplan1[j3].salary_watering ?? 0) + ((decimal?)totalplan1[j3].dopsalary_watering ?? 0), worksheetPart.Worksheet, document, totalplan1, pathTemplate, j3);
                        SetValueForDecimalCellDrawTotal("J", i3, (decimal?)totalplan1[j3].deduction_watering, worksheetPart.Worksheet, document, totalplan1, pathTemplate, j3);
                        //
                        SetValueForStringCellDrawTotal("U", i3, "", worksheetPart.Worksheet, document, totalplan1, pathTemplate, j3);
                        SetValueForStringCellDrawTotal("V", i3, "", worksheetPart.Worksheet, document, totalplan1, pathTemplate, j3);

                        for (char c = 'A'; c <= 'F'; c++)
                        {
                            SetValueForStringCellDrawTotal("A" + c, i3, "", worksheetPart.Worksheet, document, totalplan1, pathTemplate, j3);
                        }
                        for (char c = 'B'; c <= 'Z'; c++)
                        {
                            SetValueForStringCellDrawTotal(c.ToString(), (uint)(totalplan1.Count + 11 + j3), "", worksheetPart.Worksheet, document, totalplan1, pathTemplate, j3);
                        }
                        for (char c = 'A'; c <= 'O'; c++)
                        {
                            SetValueForStringCellDrawTotal("A" + c, (uint)(totalplan1.Count + 11 + j3), "", worksheetPart.Worksheet, document, totalplan1, pathTemplate, j3);
                        }
                        SetValueForDecimalCellDrawTotal("K", i3, (decimal?)totalplan1[j3].total_salary, worksheetPart.Worksheet, document, totalplan1, pathTemplate, j3);
                        SetValueForDecimalCellDrawTotal("L", i3, (decimal?)totalplan1[j3].fuel_cost, worksheetPart.Worksheet, document, totalplan1, pathTemplate, j3);
                        SetValueForDecimalCellDrawTotal("M", i3, (decimal?)totalplan1[j3].consumption, worksheetPart.Worksheet, document, totalplan1, pathTemplate, j3);
                        SetValueForDecimalCellDrawTotal("N", i3, (decimal?)totalplan1[j3].fuel_cost_ga, worksheetPart.Worksheet, document, totalplan1, pathTemplate, j3);
                        SetValueForDecimalCellDrawTotal("o", i3, (decimal?)totalplan1[j3].fuel_cost_watering, worksheetPart.Worksheet, document, totalplan1, pathTemplate, j3);
                        SetValueForDecimalCellDrawTotal("p", i3, (decimal?)totalplan1[j3].consumption_watering, worksheetPart.Worksheet, document, totalplan1, pathTemplate, j3);
                        SetValueForDecimalCellDrawTotal("q", i3, (decimal?)totalplan1[j3].fuel_cost_ga_watering, worksheetPart.Worksheet, document, totalplan1, pathTemplate, j3);
                        SetValueForDecimalCellDrawTotal("R", i3, (decimal?)totalplan1[j3].total_consumption, worksheetPart.Worksheet, document, totalplan1, pathTemplate, j3);
                        SetValueForDecimalCellDrawTotal("S", i3, (decimal?)totalplan1[j3].total_cost, worksheetPart.Worksheet, document, totalplan1, pathTemplate, j3);
                        SetValueForDecimalCellDrawTotal("T", i3, (decimal?)totalplan1[j3].seed, worksheetPart.Worksheet, document, totalplan1, pathTemplate, j3);
                        SetValueForDecimalCellDrawTotal("W", i3, (decimal?)totalplan1[j3].szr, worksheetPart.Worksheet, document, totalplan1, pathTemplate, j3);
                        SetValueForDecimalCellDrawTotal("X", i3, (decimal?)totalplan1[j3].szr_ga, worksheetPart.Worksheet, document, totalplan1, pathTemplate, j3);
                        SetValueForDecimalCellDrawTotal("Y", i3, (decimal?)totalplan1[j3].chemicalFertilizers, worksheetPart.Worksheet, document, totalplan1, pathTemplate, j3);
                        SetValueForDecimalCellDrawTotal("Z", i3, (decimal?)totalplan1[j3].chemicalFertilizers_ga, worksheetPart.Worksheet, document, totalplan1, pathTemplate, j3);
                        SetValueForDecimalCellDrawTotal("AC", i3, (decimal?)totalplan1[j3].dop_material, worksheetPart.Worksheet, document, totalplan1, pathTemplate, j3);
                        SetValueForDecimalCellDrawTotal("AD", i3, (decimal?)totalplan1[j3].dop_material_ga, worksheetPart.Worksheet, document, totalplan1, pathTemplate, j3);
                        SetValueForDecimalCellDrawTotal("AG", i3, (decimal?)totalplan1[j3].combine, worksheetPart.Worksheet, document, totalplan1, pathTemplate, j3);
                        SetValueForDecimalCellDrawTotal("AH", i3, (decimal?)totalplan1[j3].loader, worksheetPart.Worksheet, document, totalplan1, pathTemplate, j3);
                        SetValueForDecimalCellDrawTotal("AI", i3, (decimal?)totalplan1[j3].autotransport, worksheetPart.Worksheet, document, totalplan1, pathTemplate, j3); ;
                        SetValueForDecimalCellDrawTotal("AJ", i3, (decimal?)totalplan1[j3].desiccation, worksheetPart.Worksheet, document, totalplan1, pathTemplate, j3);
                        SetValueForDecimalCellDrawTotal("AK", i3, (decimal?)totalplan1[j3].analyzes, worksheetPart.Worksheet, document, totalplan1, pathTemplate, j3);
                        SetValueForDecimalCellDrawTotal("AL", i3, (decimal?)totalplan1[j3].total_service, worksheetPart.Worksheet, document, totalplan1, pathTemplate, j3);
                        SetValueForDecimalCellDrawTotal("AM", i3, (decimal?)totalplan1[j3].total, worksheetPart.Worksheet, document, totalplan1, pathTemplate, j3);
                        SetValueForDecimalCellDrawTotal("AN", i3, (decimal?)totalplan1[j3].total_ga, worksheetPart.Worksheet, document, totalplan1, pathTemplate, j3);
                        SetValueForDecimalCellDrawTotal("AO", i3, (decimal?)totalplan1[j3].total_t, worksheetPart.Worksheet, document, totalplan1, pathTemplate, j3);
                        j3++;
                    }
                    SetValueForStringCell("A", (uint)totalplan1.Count + 6, "Инвестиционные расходы на " + startDate.Value.Year + " год.", worksheetPart.Worksheet);
                    ///копирование заголовков
                    for (char c = 'A'; c <= 'Z'; c++)
                    {
                        setHeader(pathTemplate, totalplan1, worksheetPart.Worksheet, document, c.ToString(), "4", 9);
                        setHeader(pathTemplate, totalplan1, worksheetPart.Worksheet, document, c.ToString(), "5", 10);
                    }
                    for (char c = 'A'; c <= 'O'; c++)
                    {
                        setHeader(pathTemplate, totalplan1, worksheetPart.Worksheet, document, "A" + c.ToString(), "4", 9);
                        setHeader(pathTemplate, totalplan1, worksheetPart.Worksheet, document, "A" + c.ToString(), "5", 10);
                    }
                    SetValueForStringCell("E", (uint)totalplan1.Count + 9, "НЗП на " + startDate.Value.Year + " год", worksheetPart.Worksheet);
                }


                    //без детализации
                    if ((!detal) && !(plan & fact)) {
                        SheetName = "Детализация по культуре";
                        sheet = document.WorkbookPart.Workbook.GetFirstChild<Sheets>().Elements<Sheet>().SingleOrDefault(s => s.Name == SheetName);
                        worksheetPart = (WorksheetPart)document.WorkbookPart.GetPartById(sheet.Id.Value);
                        sheet.Remove();
                    }
                    //c детализацией
                    if (detal)
                    {
                        var months = new List<PlanMonthRequestDto>();
                        if (plan && !fact)
                        {
                            if (SelectDate.Equals("-1"))
                            {
                                months = (from tc in db.TC_param_value
                                          where tc.TC_operation.date_start.Value.Year == startDate.Value.Year && (tc.id_tc_param == 35
                                          || tc.id_tc_param == 39 || tc.id_tc_param == 42 || tc.id_tc_param == 38) &&
                                           (plantId != 0 ? tc.TC.plant_plant_id == plantId : true)
                                          group new { }
                                              by new PlanMonthRequestDto
                                              {
                                                  avg = (float?)tc.value / (DbFunctions.DiffDays(tc.TC_operation.date_start, tc.TC_operation.date_finish) + 1),
                                                  date_start = tc.TC_operation.date_start,
                                                  date_finish = tc.TC_operation.date_finish,
                                                  kol = DbFunctions.DiffDays(tc.TC_operation.date_start, tc.TC_operation.date_finish) + 1,
                                                  id_tc_param = tc.id_tc_param,
                                                  mat_type_id = tc.Materials.mat_type_id,
                                                  id = tc.id,
                                                  id_tc = tc.TC.plant_plant_id
                                              } into g
                                          select new PlanMonthRequestDto
                                          {
                                              avg = g.Key.avg,
                                              date_start = g.Key.date_start,
                                              date_finish = g.Key.date_finish,
                                              kol = g.Key.kol,
                                              id_tc_param = g.Key.id_tc_param,
                                              mat_type_id = g.Key.mat_type_id,
                                              id = g.Key.id,
                                              id_tc = g.Key.id_tc,
                                          }).ToList();
                            }
                                //from temp
                            else {
                                months = (from tc in db.TC_param_value_temp
                                          where tc.TC_operation_temp.date_start.Value.Year == startDate.Value.Year && (tc.id_tc_param_temp == 35
                                          || tc.id_tc_param_temp == 39 || tc.id_tc_param_temp == 42 || tc.id_tc_param_temp == 38) &&
                                           (plantId != 0 ? tc.TC_temp.plant_plant_id == plantId : true)
                                          group new { }
                                              by new PlanMonthRequestDto
                                              {
                                                  avg = tc.value / (DbFunctions.DiffDays(tc.TC_operation_temp.date_start, tc.TC_operation_temp.date_finish) + 1),
                                                  date_start = tc.TC_operation_temp.date_start,
                                                  date_finish = tc.TC_operation_temp.date_finish,
                                                  kol = DbFunctions.DiffDays(tc.TC_operation_temp.date_start, tc.TC_operation_temp.date_finish) + 1,
                                                  id_tc_param = tc.id_tc_param_temp,
                                                  mat_type_id = tc.Materials.mat_type_id,
                                                  id = tc.id,
                                                  id_tc = tc.TC_temp.plant_plant_id
                                              } into g
                                          select new PlanMonthRequestDto
                                          {
                                              avg = g.Key.avg,
                                              date_start = g.Key.date_start,
                                              date_finish = g.Key.date_finish,
                                              kol = g.Key.kol,
                                              id_tc_param = g.Key.id_tc_param,
                                              mat_type_id = g.Key.mat_type_id,
                                              id = g.Key.id,
                                              id_tc = g.Key.id_tc,
                                          }).ToList();
                            
                            
                            }
                        }
                        //
                        //Детализация затрат по культуре
                        SheetName = "Детализация по культуре";
                        sheet = document.WorkbookPart.Workbook.GetFirstChild<Sheets>().Elements<Sheet>().SingleOrDefault(s => s.Name == SheetName);
                        worksheetPart = (WorksheetPart)document.WorkbookPart.GetPartById(sheet.Id.Value);
                        sheetData = worksheetPart.Worksheet.GetFirstChild<SheetData>();
                  //      sheet.Name = "1 Детализация по культуре " + totalplan1[0].plant_name;
                        sheet.Name = "1 " + totalplan1[0].plant_name;
                        //////////////////////////
                        //СОЗДАЕМ И ЗАПОЛНЯЕМ ЛИСТЫ
                        ///
                        for (int i = 0; i < totalplan1.Count - 1; i++)
                        {
                            if (i != 0)
                            {
                          //      CopySheet(pathTemplate, "1 Детализация по культуре " + totalplan1[0].plant_name, i + 1 + " Детализация по культуре " + totalplan1[i].plant_name, document);
                          //      SheetName = i + 1 + " Детализация по культуре " + totalplan1[i].plant_name;
                                CopySheet(pathTemplate, "1 " + totalplan1[0].plant_name, i + 1 + " " + totalplan1[i].plant_name, document);
                                SheetName = i + 1 + " " + totalplan1[i].plant_name;
                                sheet = document.WorkbookPart.Workbook.GetFirstChild<Sheets>().Elements<Sheet>().SingleOrDefault(s => s.Name == SheetName);
                                worksheetPart = (WorksheetPart)document.WorkbookPart.GetPartById(sheet.Id.Value);
                                sheetData = worksheetPart.Worksheet.GetFirstChild<SheetData>();
                            }
                            if (plan && !fact)
                            {
                                SetValueForStringCell("A", 1, "Планируемые затраты на " + totalplan1[i].plant_name + " за " + startDate.Value.Year + " год", worksheetPart.Worksheet);
                            }
                            if (!plan && fact)
                            {
                                SetValueForStringCell("A", 1, "Фактические затраты на " + totalplan1[i].plant_name + " за " + startDate.Value.Year + " год", worksheetPart.Worksheet);
                            }

                            SetValueForDecimalCell("B", 4, (decimal?)totalplan1[i].nzp_zp, worksheetPart.Worksheet);
                            SetValueForDecimalCell("B", 5, (decimal?)(totalplan1[i].nzp_zp * 0.13), worksheetPart.Worksheet);
                            SetValueForDecimalCell("B", 6, (decimal?)(totalplan1[i].nzp_zp * 0.321), worksheetPart.Worksheet);
                            SetValueForDecimalCell("B", 7, (decimal?)totalplan1[i].nzp_fuel, worksheetPart.Worksheet);
                            SetValueForDecimalCell("B", 8, (decimal?)totalplan1[i].nzp_seed, worksheetPart.Worksheet);
                            SetValueForDecimalCell("B", 9, (decimal?)totalplan1[i].nzp_szr, worksheetPart.Worksheet);
                            SetValueForDecimalCell("B", 10, (decimal?)totalplan1[i].nzp_chemicalFertilizers, worksheetPart.Worksheet);
                            SetValueForDecimalCell("B", 15, (decimal?)totalplan1[i].nzp_dop_material, worksheetPart.Worksheet);
                            SetValueForDecimalCell("B", 28, ((decimal?)(totalplan1[i].nzp_zp ?? 0) + ((decimal?)(totalplan1[i].nzp_zp * 0.321 ?? 0)) + (decimal?)(totalplan1[i].nzp_fuel ?? 0) +
                            (decimal?)(totalplan1[i].nzp_seed ?? 0) + (decimal?)(totalplan1[i].nzp_szr ?? 0) + (decimal?)(totalplan1[i].nzp_chemicalFertilizers ?? 0)
                            + (decimal?)(totalplan1[i].nzp_dop_material ?? 0)), worksheetPart.Worksheet);

                            SetValueForDecimalCell("C", 4, (decimal?)((totalplan1[i].salary ?? 0) + (totalplan1[i].dopsalary ?? 0) + (totalplan1[i].salary_watering ?? 0) + (totalplan1[i].dopsalary_watering ?? 0)), worksheetPart.Worksheet);
                            SetValueForDecimalCell("C", 5, (decimal?)((double?)((totalplan1[i].salary ?? 0) + (totalplan1[i].dopsalary ?? 0)) * 0.13 + (double?)((totalplan1[i].salary_watering ?? 0) + (totalplan1[i].dopsalary_watering ?? 0)) * 0.13), worksheetPart.Worksheet);
                            SetValueForDecimalCell("C", 6, (decimal?)((double?)((totalplan1[i].salary ?? 0) + (totalplan1[i].dopsalary ?? 0)) * 0.321 + (double?)((totalplan1[i].salary_watering ?? 0) + (totalplan1[i].dopsalary_watering ?? 0)) * 0.321), worksheetPart.Worksheet);
                            SetValueForDecimalCell("C", 7, (decimal?)(totalplan1[i].fuel_cost ?? 0) + (totalplan1[i].fuel_cost_watering ?? 0), worksheetPart.Worksheet);
                            SetValueForDecimalCell("C", 8, (decimal?)totalplan1[i].seed, worksheetPart.Worksheet);
                            SetValueForDecimalCell("C", 9, (decimal?)totalplan1[i].szr, worksheetPart.Worksheet);
                            SetValueForDecimalCell("C", 10, (decimal?)totalplan1[i].chemicalFertilizers, worksheetPart.Worksheet);
                            SetValueForDecimalCell("C", 15, (decimal?)totalplan1[i].dop_material, worksheetPart.Worksheet);
                            SetValueForDecimalCell("C", 18, (decimal?)totalplan1[i].total_service, worksheetPart.Worksheet);
                            SetValueForDecimalCell("C", 28, ((decimal?)(totalplan1[i].salary ?? 0) + (decimal?)(totalplan1[i].dopsalary ?? 0) +
                             (totalplan1[i].salary_watering ?? 0) + (decimal?)(totalplan1[i].dopsalary_watering ?? 0)
                              + (decimal?)((double?)(totalplan1[i].salary ?? 0) * 0.321) + (decimal?)((double?)(totalplan1[i].dopsalary ?? 0) * 0.321) +
                              +(decimal?)((double?)(totalplan1[i].salary_watering ?? 0) * 0.321) + (decimal?)((double?)(totalplan1[i].dopsalary_watering ?? 0) * 0.321) +
                                (decimal?)(totalplan1[i].fuel_cost_watering ?? 0) +
                            (decimal?)(totalplan1[i].fuel_cost ?? 0) + (decimal?)(totalplan1[i].seed ?? 0) + (decimal?)(totalplan1[i].szr ?? 0) + (decimal?)(totalplan1[i].chemicalFertilizers ?? 0) +
                           (decimal?)(totalplan1[i].dop_material ?? 0) + (decimal?)(totalplan1[i].total_service ?? 0)), worksheetPart.Worksheet);

                            if (plan && !fact)
                            {
                                //Разбивка по месяцам - план
                                char m = 'D';
                                for (int j = 1; j <= 12; j++)
                                {
                                    var mon = (decimal?)getValueForMonth(j, months.Where(l => l.id_tc == totalplan1[i].id_tc).ToList(), startDate.Value.Year, null, 35, 38);
                                    var f = (decimal?)getValueForMonth(j, months.Where(l => l.id_tc == totalplan1[i].id_tc).ToList(), startDate.Value.Year, null, 39, null);
                                    SetValueForDecimalCell(m.ToString(), 4, (mon), worksheetPart.Worksheet);
                                    SetValueForDecimalCell(m.ToString(), 5, ((decimal?)((double?)mon * 0.13)), worksheetPart.Worksheet);
                                    SetValueForDecimalCell(m.ToString(), 6, ((decimal?)((double?)mon * 0.321)), worksheetPart.Worksheet);
                                    SetValueForDecimalCell(m.ToString(), 7, (f), worksheetPart.Worksheet);
                                    var s = (decimal?)getValueForMonth(j, months.Where(l => l.id_tc == totalplan1[i].id_tc).ToList(), startDate.Value.Year, 4, 42, null);
                                    SetValueForDecimalCell(m.ToString(), 8, (s), worksheetPart.Worksheet);
                                    var szr = (decimal?)getValueForMonth(j, months.Where(l => l.id_tc == totalplan1[i].id_tc).ToList(), startDate.Value.Year, 1, 42, null);
                                    SetValueForDecimalCell(m.ToString(), 9, (szr), worksheetPart.Worksheet);
                                    var ud = (decimal?)getValueForMonth(j, months.Where(l => l.id_tc == totalplan1[i].id_tc).ToList(), startDate.Value.Year, 2, 42, null);
                                    SetValueForDecimalCell(m.ToString(), 10, (ud), worksheetPart.Worksheet);
                                    var pr = (decimal?)getValueForMonth(j, months.Where(l => l.id_tc == totalplan1[i].id_tc).ToList(), startDate.Value.Year, 5, 42, null);
                                    SetValueForDecimalCell(m.ToString(), 15, (pr), worksheetPart.Worksheet);
                                    SetValueForDecimalCell(m.ToString(), 28, (mon + (decimal?)((double?)mon * 0.321) + f + s + szr + ud + pr), worksheetPart.Worksheet);
                                    m++;
                                }
                            }
                            // по месяцам - факт
                            if (!plan && fact)
                            {
                                //Разбивка по месяцам - факт
                                char m = 'D';
                                for (int j = 1; j <= 12; j++)
                                {
                                    DateTime date = new DateTime(startDate.Value.Year, j, 1);
                                    var firstDayOfMonth = new DateTime(date.Year, date.Month, 1);
                                    var lastDayOfMonth = firstDayOfMonth.AddMonths(1);
                                    var id = totalplan1[i].id_tc;
                                    decimal? salary = db.Tasks.Where(t => t.Sheet_tracks.date_begin >= firstDayOfMonth && t.Sheet_tracks.date_begin <= lastDayOfMonth &&
                                   t.Fields_to_plants.plant_plant_id == id).Select(t => t.total_salary).Sum();
                                    
                              decimal? fuel = db.Tasks.Where(t => t.Sheet_tracks.date_begin >= firstDayOfMonth && t.Sheet_tracks.date_begin <= lastDayOfMonth &&
                             t.Fields_to_plants.plant_plant_id == id).Select(t => t.fuel_cost).Sum();

                              var seed = db.TMC_Records.Where(t => t.TMC_Acts.act_date >= firstDayOfMonth && t.TMC_Acts.act_date <= lastDayOfMonth &&
                             t.Fields_to_plants.plant_plant_id == id && t.Materials.mat_type_id==4).Select(t => (float?)t.count*t.price).Sum();

                              var szr = db.TMC_Records.Where(t => t.TMC_Acts.act_date >= firstDayOfMonth && t.TMC_Acts.act_date <= lastDayOfMonth &&
                             t.Fields_to_plants.plant_plant_id == id && t.Materials.mat_type_id == 1).Select(t => (float?)t.count * t.price).Sum();

                              var chemicalFertilizers = db.TMC_Records.Where(t => t.TMC_Acts.act_date >= firstDayOfMonth && t.TMC_Acts.act_date <= lastDayOfMonth &&
                            t.Fields_to_plants.plant_plant_id == id && t.Materials.mat_type_id == 2).Select(t => (float?)t.count * t.price).Sum();

                              var dop_material = db.TMC_Records.Where(t => t.TMC_Acts.act_date >= firstDayOfMonth && t.TMC_Acts.act_date <= lastDayOfMonth &&
                            t.Fields_to_plants.plant_plant_id == id && t.Materials.mat_type_id == 5).Select(t => (float?)t.count * t.price).Sum();

                              var dop_salary = (float?)db.Salary_details.Where(t => t.date >= firstDayOfMonth && t.date <= lastDayOfMonth &&
                                  t.Fields_to_plants.plant_plant_id == id).Select(t => t.sum).Sum();
                             salary = (salary ?? 0) + (decimal)(dop_salary ?? 0);
                                    SetValueForDecimalCell(m.ToString(), 4, (salary), worksheetPart.Worksheet);
                                    SetValueForDecimalCell(m.ToString(), 4, (salary), worksheetPart.Worksheet);
                                    SetValueForDecimalCell(m.ToString(), 5, ((decimal?)((double?)(salary) * 0.13)), worksheetPart.Worksheet);
                                    SetValueForDecimalCell(m.ToString(), 6, ((decimal?)((double?)(salary) * 0.321)), worksheetPart.Worksheet);
                                    SetValueForDecimalCell(m.ToString(), 7, (decimal?)(fuel), worksheetPart.Worksheet);
                                    SetValueForDecimalCell(m.ToString(), 8, (decimal?)(seed), worksheetPart.Worksheet);
                                    SetValueForDecimalCell(m.ToString(), 9, (decimal?)(szr), worksheetPart.Worksheet);
                                    SetValueForDecimalCell(m.ToString(), 10, (decimal?)(chemicalFertilizers), worksheetPart.Worksheet);
                                    SetValueForDecimalCell(m.ToString(), 15, (decimal?)(dop_material), worksheetPart.Worksheet);
                                    SetValueForDecimalCell(m.ToString(), 28, (salary + (decimal?)((double?)salary * 0.321) + (decimal?)(fuel ?? 0) +
                                  (decimal?)(seed ?? 0) + (decimal?)(szr ?? 0) + (decimal?)(chemicalFertilizers ?? 0) + (decimal?)(dop_material ?? 0)), worksheetPart.Worksheet);
                                  
                                    m++;
                                }
                            }

                        }
                    }

                    //////////////////////////
                    worksheetPart.Worksheet.Save();
                    document.Close();
                }
                return stream.ToArray();
            }

        }
        public List<PlanRequestDto> sumTotalPlanFact(List<PlanRequestDto> totalplan1)
        {
            totalplan1.Add(new PlanRequestDto
            {
                id_tc=-1,
                plant_name = "Итого",
                area = totalplan1.Sum(l => l.area),
                nzp = totalplan1.Sum(l => Math.Round((l.nzp != null ? l.nzp.Value : 0), 2)),
                salary = totalplan1.Sum(l => l.salary),
                dopsalary = totalplan1.Sum(l => l.dopsalary),
                deduction = totalplan1.Sum(l => l.deduction),
                salary_watering = totalplan1.Sum(l => l.salary_watering),
                dopsalary_watering = totalplan1.Sum(l => l.dopsalary_watering),
                deduction_watering = totalplan1.Sum(l => l.deduction_watering),
                total_salary = totalplan1.Sum(l => l.total_salary),
                fuel_cost = totalplan1.Sum(l => l.fuel_cost),
                consumption = totalplan1.Sum(l => l.consumption),
                fuel_cost_watering = totalplan1.Sum(l => l.fuel_cost_watering),
                consumption_watering = totalplan1.Sum(l => l.consumption_watering),
                total_consumption = totalplan1.Sum(l => l.total_consumption),
                dop_material = totalplan1.Sum(l => l.dop_material),
                total_cost = totalplan1.Sum(l => l.total_cost),
                seed = totalplan1.Sum(l => l.seed),
                szr = totalplan1.Sum(l => l.szr),
                chemicalFertilizers = totalplan1.Sum(l => l.chemicalFertilizers),
                combine = totalplan1.Sum(l => l.combine),
                loader = totalplan1.Sum(l => l.loader),
                autotransport = totalplan1.Sum(l => l.autotransport),
                desiccation = totalplan1.Sum(l => l.desiccation),
                analyzes = totalplan1.Sum(l => l.analyzes),
                total_service = totalplan1.Sum(l => l.total_service),
                total = totalplan1.Sum(l => l.total),
            });
            return totalplan1;
        }



        //TOTAL НЗП
        public List<SevooborotModel> getTotalNZP(DateTime? startDate)
        { 
          //НЗП для всех
                    var sevoborot = (from ftp in db.Fields_to_plants
                                     where ftp.date_ingathering.Year == startDate.Value.Year && ftp.Fields.type == 1 && ftp.plant_plant_id != 36 && ftp.area_plant != 0
                                     let sum_area = (float?)db.Fields_to_plants.Where(f => f.fiel_fiel_id == ftp.fiel_fiel_id && f.date_ingathering.Year == startDate.Value.Year &&
                                     f.area_plant != 0 && ftp.plant_plant_id != 36).Select(f => f.area_plant).Sum()

                                     let total_salary = (float?)db.Tasks.Where(t => t.plant_plant_id == t.Fields_to_plants.fielplan_id
                                     && t.Fields_to_plants.date_ingathering.Year == startDate.Value.Year && t.Fields_to_plants.fiel_fiel_id == ftp.fiel_fiel_id && t.Fields_to_plants.plant_plant_id == 36).Select(t => t.total_salary).Sum()

                                     let consumption = (float?)db.Tasks.Where(t => t.plant_plant_id == t.Fields_to_plants.fielplan_id
                                     && t.Fields_to_plants.date_ingathering.Year == startDate.Value.Year && t.Fields_to_plants.fiel_fiel_id == ftp.fiel_fiel_id && t.Fields_to_plants.plant_plant_id == 36).Select(t => t.fuel_cost).Sum()

                                     let dop_salary = (float?)db.Salary_details.Where(t => t.plant_plant_id == t.Fields_to_plants.fielplan_id
                                     && t.Fields_to_plants.date_ingathering.Year == startDate.Value.Year && t.Fields_to_plants.fiel_fiel_id == ftp.fiel_fiel_id && t.Fields_to_plants.plant_plant_id == 36).Select(t => t.sum).Sum()

                                     let szr = (float?)db.TMC_Records.Where(t => t.fielplan_fielplan_id == t.Fields_to_plants.fielplan_id && t.emp_to_id == 0 && t.emp_from_id != 0
                                     && t.Materials.mat_id == t.mat_mat_id && t.Materials.mat_type_id == 1
                                     && t.Fields_to_plants.date_ingathering.Year == startDate.Value.Year && t.Fields_to_plants.fiel_fiel_id == ftp.fiel_fiel_id && t.Fields_to_plants.plant_plant_id == 36).Select(t => t.count * t.price).Sum()

                                     let chemicalFertilizers = (float?)db.TMC_Records.Where(t => t.fielplan_fielplan_id == t.Fields_to_plants.fielplan_id && t.emp_to_id == 0 && t.emp_from_id != 0
                                     && t.Materials.mat_id == t.mat_mat_id && t.Materials.mat_type_id == 2
                                     && t.Fields_to_plants.date_ingathering.Year == startDate.Value.Year && t.Fields_to_plants.fiel_fiel_id == ftp.fiel_fiel_id && t.Fields_to_plants.plant_plant_id == 36).Select(t => t.count * t.price).Sum()

                                     let seed = (float?)db.TMC_Records.Where(t => t.fielplan_fielplan_id == t.Fields_to_plants.fielplan_id && t.emp_to_id == 0 && t.emp_from_id != 0
                                     && t.Materials.mat_id == t.mat_mat_id && t.Materials.mat_type_id == 4
                                     && t.Fields_to_plants.date_ingathering.Year == startDate.Value.Year && t.Fields_to_plants.fiel_fiel_id == ftp.fiel_fiel_id && t.Fields_to_plants.plant_plant_id == 36).Select(t => t.count * t.price).Sum()

                                     let dopmaterial = (float?)db.TMC_Records.Where(t => t.fielplan_fielplan_id == t.Fields_to_plants.fielplan_id && t.emp_to_id == 0 && t.emp_from_id != 0
                                  && t.Materials.mat_id == t.mat_mat_id && t.Materials.mat_type_id == 5
                                  && t.Fields_to_plants.date_ingathering.Year == startDate.Value.Year && t.Fields_to_plants.fiel_fiel_id == ftp.fiel_fiel_id && t.Fields_to_plants.plant_plant_id == 36).Select(t => t.count * t.price).Sum()

                                     let deduction = ((dop_salary ?? 0) + (total_salary ?? 0)) * 0.321
                                     group new
                                     {
                                         plant_id = ftp.plant_plant_id,
                                         field_id = ftp.fiel_fiel_id,
                                         sum_area = sum_area ?? 1,
                                         total_salary = total_salary ?? 0,
                                         consumption = consumption ?? 0,
                                         dop_salary = dop_salary ?? 0,
                                         deduction = deduction,
                                         szr = szr ?? 0,
                                         chemicalFertilizers = chemicalFertilizers ?? 0,
                                         seed = seed ?? 0,
                                         dopmaterial = dopmaterial ?? 0
                                     }
                                         by new
                                         {
                                             field_id = ftp.fiel_fiel_id,
                                             plant_id = ftp.plant_plant_id,
                                             field_name = ftp.Fields.name,
                                             plant_name = ftp.Plants.name,
                                             area_plant = ftp.area_plant,
                                             sum_area = sum_area,
                                             total_salary = total_salary,
                                             consumption = consumption,
                                             dop_salary = dop_salary,
                                             szr = szr,
                                             chemicalFertilizers = chemicalFertilizers,
                                             seed = seed,
                                             dopmaterial = dopmaterial,
                                             deduction = deduction,

                                         } into g
                                     select new SevooborotModel
                                     {
                                         consumption = (float?)g.Key.consumption * ((float)g.Key.area_plant / (float)g.Key.sum_area) ?? 0,
                                         total_salary = (float?)g.Key.total_salary * ((float)g.Key.area_plant / (float)g.Key.sum_area) ?? 0,
                                         dop_salary = (float?)g.Key.dop_salary * ((float)g.Key.area_plant / (float)g.Key.sum_area) ?? 0,
                                         szr = g.Key.szr * ((float?)g.Key.area_plant / (float)g.Key.sum_area) ?? 0,
                                         chemicalFertilizers = (float?)g.Key.chemicalFertilizers * ((float)g.Key.area_plant / (float)g.Key.sum_area) ?? 0,
                                         seed = g.Key.seed * ((float?)g.Key.area_plant / (float)g.Key.sum_area) ?? 0,
                                         dopmaterial = g.Key.dopmaterial * ((float?)g.Key.area_plant / (float)g.Key.sum_area) ?? 0,
                                         field_id = g.Key.field_id,
                                         plant_id = g.Key.plant_id,
                                         field_name = g.Key.field_name,
                                         plant_name = g.Key.plant_name,
                                         sum_area = g.Key.sum_area,
                                         area_plant = g.Key.area_plant,
                                         total_sal1 = g.Key.total_salary,
                                         deduction = (float?)g.Key.deduction * ((float)g.Key.area_plant / (float)g.Key.sum_area) ?? 0,
                                     }).ToList();
                    return sevoborot;
        }



        //////////TOTAL PLAN
        public List<PlanRequestDto> getTotalPlan(DateTime? startDate, DateTime? endDate, int? plantId, List<SevooborotModel> sevoborot)
        {
        var  period = (from tc in db.TC_param_value
                                      where ((tc.TC_operation.date_start <= startDate && tc.TC_operation.date_finish >= startDate && tc.TC_operation.date_finish <= endDate)
                                      || (tc.TC_operation.date_start >= startDate && tc.TC_operation.date_finish <= endDate)
                                      || (tc.TC_operation.date_start >= startDate && tc.TC_operation.date_start <= endDate && tc.TC_operation.date_finish >= endDate)
                                      || (tc.TC_operation.date_start < startDate && tc.TC_operation.date_finish > endDate))
                                                           && (tc.id_tc_param == 1 || tc.id_tc_param == 2 || tc.id_tc_param == 3 || tc.id_tc_param == 7 || tc.id_tc_param == 53
                                                          || tc.id_tc_param == 54 || tc.id_tc_param == 55 || tc.id_tc_param == 56 || tc.id_tc_param == 57
                                                          || tc.id_tc_param == 60 || tc.id_tc_param == 61 || tc.id_tc_param == 35 || tc.id_tc_param == 38
                                                          || tc.id_tc_param == 39 || tc.id_tc_param == 59 || tc.id_tc_param == 19 || tc.id_tc_param == 25
                                                          || tc.id_tc_param == 22 || tc.id_tc_param == 33 || tc.id_tc_param == 42)
                                                        && (plantId != 0 ? tc.TC.plant_plant_id == plantId : true)
                                      group new
                                      {

                                      }
                                          by new PlanMonthRequestDto
                                          {
                                              avg = (float?)tc.value / (DbFunctions.DiffDays(tc.TC_operation.date_start, tc.TC_operation.date_finish) + 1),
                                              date_start = tc.TC_operation.date_start,
                                              date_finish = tc.TC_operation.date_finish,
                                              kol = DbFunctions.DiffDays(tc.TC_operation.date_start, tc.TC_operation.date_finish) + 1,
                                              id_tc_param = tc.id_tc_param,
                                              mat_type_id = tc.Materials.mat_type_id,
                                              id = tc.id,
                                              id_tc_operation = tc.id_tc_operation,
                                              id_tc = tc.TC.plant_plant_id,
                                              grtask_id = tc.TC_operation.Type_tasks.auxiliary_rate,
                                              val = (float?)tc.value,
                                          } into g
                                      select new PlanMonthRequestDto
                                      {
                                          avg = g.Key.avg,
                                          date_start = g.Key.date_start,
                                          date_finish = g.Key.date_finish,
                                          kol = g.Key.kol,
                                          id_tc_param = g.Key.id_tc_param,
                                          mat_type_id = g.Key.mat_type_id,
                                          id = g.Key.id,
                                          id_tc_operation = g.Key.id_tc_operation,
                                          id_tc = g.Key.id_tc,
                                          grtask_id = g.Key.grtask_id,
                                          val = g.Key.val,
                                      }).ToList();
                        //
                        var totalplan = (from tc in db.TC_param_value
                                         where tc.TC.year == startDate.Value.Year &&
                                         (tc.id_tc_param == 1 || tc.id_tc_param == 2 || tc.id_tc_param == 3 || tc.id_tc_param == 7 || tc.id_tc_param == 53
                                         || tc.id_tc_param == 54 || tc.id_tc_param == 55 || tc.id_tc_param == 56 || tc.id_tc_param == 57
                                         || tc.id_tc_param == 60 || tc.id_tc_param == 61 || tc.id_tc_param == 35 || tc.id_tc_param == 38
                                         || tc.id_tc_param == 39 || tc.id_tc_param == 59 || tc.id_tc_param == 19 || tc.id_tc_param == 25
                                         || tc.id_tc_param == 22 || tc.id_tc_param == 33)
                                       && (plantId != 0 ? tc.TC.plant_plant_id == plantId : true)
                                         group new
                                         {}
                                             by new
                                             {
                                                 plant_name = tc.TC.Plants.name,
                                                 plant_id = tc.TC.plant_plant_id,
                                                 id_tc = tc.TC.plant_plant_id,
                                                 id_tc_param = tc.id_tc_param,
                                                 value = tc.value,
                                                 id = tc.id,
                                                 grtask_id = tc.TC_operation.Type_tasks.auxiliary_rate,
                                             } into g
                                         select new PlanRequestDto
                                         {
                                             plant_name = g.Key.plant_name,
                                             plant_id = (int)(g.Key.plant_id ?? 0),
                                             id_tc = (int)(g.Key.plant_id ?? 0),
                                             id_tc_param1 = g.Key.id_tc_param,
                                             value = (float?)g.Key.value,
                                             id = g.Key.id,
                                             grtask_id = g.Key.grtask_id
                                         }).OrderBy(l => l.plant_name).ToList();
                        int j3 = 0;
                     var  totplan = totalplan.Select(s => s.id_tc).Distinct().ToList();
                        for (j3 = 0; j3 < totplan.Count; j3++)
                        {
                            int id = (int)totplan[j3];
                            var area = totalplan.Where(s => s.id_tc == totplan[j3] && s.id_tc_param1 == 1).Select(s => s.value).Sum();
                            var gross_harvest = totalplan.Where(s => s.id_tc == totplan[j3] && s.id_tc_param1 == 3).Select(s => s.value).Sum();
                            var val = getValueFromPeriod(period, j3, startDate, endDate, id, 42, null, 5);
                            var dop_material = (float?)val;
                            var dop_material_ga = area == 0 ? 0 : (dop_material == null ? 0 : dop_material) / area;
                            val = getValueFromPeriod(period, j3, startDate, endDate, id, 35, 1, null);
                            var salary = (float?)val;
                            val = getValueFromPeriod(period, j3, startDate, endDate, id, 38, 1, null);
                            var dopsalary = (float?)val;
                            var deduction = (salary == null ? 0 : salary * 0.321) + (dopsalary == null ? 0 : dopsalary * 0.321);
                            //орошение
                            val = getValueFromPeriod(period, j3, startDate, endDate, id, 35, 2, null);
                            var salary_watering = (float?)val;
                            val = getValueFromPeriod(period, j3, startDate, endDate, id, 38, 2, null);
                            var dopsalary_watering = (float?)val;
                            var deduction_watering = (salary_watering == null ? 0 : salary_watering * 0.321) + (dopsalary_watering == null ? 0 : dopsalary_watering * 0.321);
                            //
                            var total_salary = (salary == null ? 0 : salary) + (dopsalary == null ? 0 : dopsalary) + (deduction == null ? 0 : deduction) +
                               (salary_watering == null ? 0 : salary_watering) + (dopsalary_watering == null ? 0 : dopsalary_watering)
                               + (deduction_watering == null ? 0 : deduction_watering);
                            float? consumption = getValueFromPeriodForFuel(period, j3, startDate, endDate, id, 33, 1, null);
                            val = getValueFromPeriod(period, j3, startDate, endDate, id, 39, 1, null);
                            var fuel_cost = (float?)val;
                            var fuel_cost_ga = area == 0 ? 0 : (consumption == null ? 0 : consumption) / area;
                            //топливо орошение
                            float? consumption_watering = getValueFromPeriodForFuel(period, j3, startDate, endDate, id, 33, 2, null);
                            val = getValueFromPeriod(period, j3, startDate, endDate, id, 39, 2, null);
                            var fuel_cost_watering = (float?)val;
                            var fuel_cost_ga_watering = area == 0 ? 0 : (consumption_watering == null ? 0 : consumption_watering) / area;
                            //
                            var total_consumption = consumption + consumption_watering;
                            val = getValueFromPeriod(period, j3, startDate, endDate, id, 42, null, 4);
                            var seed = (float?)val;
                            val = getValueFromPeriod(period, j3, startDate, endDate, id, 42, null, 2);
                            var chemicalFertilizers = (float?)val;
                            val = getValueFromPeriod(period, j3, startDate, endDate, id, 42, null, 1);
                            var szr = (float?)val;
                            var total_cost = (fuel_cost ?? 0) + (fuel_cost_watering ?? 0);  //итого по топливу
                            var combine = db.TC_to_service.Where(t => t.TC.year == startDate.Value.Year && t.id_service == 1 && t.TC.plant_plant_id == id).Select(v => (float?)v.cost).Sum();
                            var loader = db.TC_to_service.Where(t => t.TC.year == startDate.Value.Year && t.id_service == 2 && t.TC.plant_plant_id == id).Select(v => (float?)v.cost).Sum();
                            var autotransport = db.TC_to_service.Where(t => t.TC.year == startDate.Value.Year && t.id_service == 3 && t.TC.plant_plant_id == id).Select(v => (float?)v.cost).Sum();
                            var desiccation = db.TC_to_service.Where(t => t.TC.year == startDate.Value.Year && t.id_service == 4 && t.TC.plant_plant_id == id).Select(v => (float?)v.cost).Sum();
                            var analyzes = db.TC_to_service.Where(t => t.TC.year == startDate.Value.Year && t.id_service == 5 && t.TC.plant_plant_id == id).Select(v => (float?)v.cost).Sum();
                            var total_service = db.TC_to_service.Where(t => t.TC.year == startDate.Value.Year && t.TC.plant_plant_id == id).Select(v => (float?)v.cost).Sum();

                            totalplan1.Add(new PlanRequestDto
                            {
                                id_tc = id,
                                plant_name = totalplan.Where(s => s.id_tc == totplan[j3]).Select(s => s.plant_name).FirstOrDefault(),
                                area = (decimal?)area,
                                productivity = totalplan.Where(s => s.id_tc == totplan[j3] && s.id_tc_param1 == 2).Select(s => s.value).Sum(),
                                gross_harvest = gross_harvest,
                                nzp = 0,
                                nzp_zp = 0,
                                nzp_fuel = 0,
                                nzp_szr = 0,
                                nzp_chemicalFertilizers = 0,
                                nzp_seed = 0,
                                nzp_dop_material = 0,
                                dop_material = (decimal?)dop_material,
                                dop_material_ga = dop_material_ga,
                                salary = (decimal?)salary,
                                dopsalary = (decimal?)dopsalary,
                                salary_watering = (decimal?)salary_watering,
                                dopsalary_watering = (decimal?)dopsalary_watering,
                                deduction_watering = (decimal?)deduction_watering,
                                fuel_cost = (decimal?)fuel_cost,
                                fuel_cost_watering = (decimal?)fuel_cost_watering,
                                deduction = (decimal?)deduction,
                                total_salary = (decimal?)total_salary,
                                consumption = (decimal?)consumption,
                                consumption_watering = (decimal?)consumption_watering,
                                fuel_cost_ga = fuel_cost_ga,
                                fuel_cost_ga_watering = fuel_cost_ga_watering,
                                total_consumption = (decimal?)total_consumption,
                                total_cost = (decimal?)total_cost,  //итого по топливу
                                seed = (decimal?)seed,
                                szr = (decimal?)szr,
                                szr_ga = area == 0 ? 0 : (szr == null ? 0 : szr) / area,
                                chemicalFertilizers = (decimal?)chemicalFertilizers,
                                chemicalFertilizers_ga = area == 0 ? 0 : (chemicalFertilizers == null ? 0 : chemicalFertilizers) / area,
                                combine = (decimal?)combine,
                                loader = (decimal?)loader,
                                autotransport = (decimal?)autotransport,
                                desiccation = (decimal?)desiccation,
                                analyzes = (decimal?)analyzes,
                                total_service = (decimal?)total_service,
                                total = 0,
                                total_ga = 0,
                                total_t = 0,
                            });
                        }
              //////добавляем к итого нзп -план
                        for (int i = 0; i < sevoborot.Count; i++)
                        {
                            for (int j = 0; j < totalplan1.Count; j++)
                            {
                                if (totalplan1[j].id_tc == sevoborot[i].plant_id)
                                {
                                    //нзп всего
                                    totalplan1[j].nzp = (totalplan1[j].nzp ?? 0) + (decimal)sevoborot[i].total_salary + (decimal)sevoborot[i].consumption +
                                    (decimal)sevoborot[i].szr + (decimal)sevoborot[i].chemicalFertilizers + (decimal)sevoborot[i].dop_salary +
                                    (decimal)sevoborot[i].seed + (decimal)sevoborot[i].dopmaterial + (decimal)sevoborot[i].deduction;

                                    totalplan1[j].nzp_zp = (totalplan1[j].nzp_zp ?? 0) + (sevoborot[i].total_salary ?? 0) + (sevoborot[i].dop_salary ?? 0); //детализация
                                    totalplan1[j].nzp_fuel = (totalplan1[j].nzp_fuel ?? 0) + (sevoborot[i].consumption ?? 0);
                                    totalplan1[j].nzp_seed = (totalplan1[j].nzp_seed ?? 0) + (sevoborot[i].seed ?? 0);
                                    totalplan1[j].nzp_szr = (totalplan1[j].nzp_szr ?? 0) + (sevoborot[i].szr ?? 0);
                                    totalplan1[j].nzp_chemicalFertilizers = (totalplan1[j].nzp_chemicalFertilizers ?? 0) + (sevoborot[i].chemicalFertilizers ?? 0);
                                    totalplan1[j].nzp_dop_material = (totalplan1[j].nzp_dop_material ?? 0) + (sevoborot[i].dopmaterial ?? 0);

                                    //ИТОГО
                                    totalplan1[j].total = (totalplan1[j].nzp == null ? 0 : totalplan1[j].nzp) +
                                    (totalplan1[j].total_salary == null ? 0 : totalplan1[j].total_salary) +
                                    (totalplan1[j].total_cost == null ? 0 : totalplan1[j].total_cost)
                                     + (totalplan1[j].seed == null ? 0 : totalplan1[j].seed) + (totalplan1[j].szr == null ? 0 : totalplan1[j].szr) +
                                     (totalplan1[j].chemicalFertilizers == null ? 0 : totalplan1[j].chemicalFertilizers)
                                     + (totalplan1[j].total_service == null ? 0 : totalplan1[j].total_service) +
                                     (totalplan1[j].dop_material == null ? 0 : totalplan1[j].dop_material);

                                    totalplan1[j].total_ga = (double?)totalplan1[j].area == 0 ? 0 : (double?)(totalplan1[j].total / totalplan1[j].area);
                                    totalplan1[j].total_t = (double?)totalplan1[j].gross_harvest == 0 ? 0 : (double?)(totalplan1[j].total / (decimal?)totalplan1[j].gross_harvest);
                                }
                            }
                        }

                        /////
           return totalplan1;
        }

        //TOTAl PLAN SECTION TECHCARD
        public List<PlanRequestDto> getTotalPlanSectionTechcard(DateTime? startDate, DateTime? endDate, int? plantId, List<SevooborotModel> sevoborot, DateTime dateTechs)
        {
            //добавить дату тех. карт
            var period = (from tc in db.TC_param_value_temp
                          where ((tc.TC_operation_temp.date_start <= startDate && tc.TC_operation_temp.date_finish >= startDate && tc.TC_operation_temp.date_finish <= endDate)
                          || (tc.TC_operation_temp.date_start >= startDate && tc.TC_operation_temp.date_finish <= endDate)
                          || (tc.TC_operation_temp.date_start >= startDate && tc.TC_operation_temp.date_start <= endDate && tc.TC_operation_temp.date_finish >= endDate)
                          || (tc.TC_operation_temp.date_start < startDate && tc.TC_operation_temp.date_finish > endDate))
                                               && (tc.id_tc_param_temp == 1 || tc.id_tc_param_temp == 2 || tc.id_tc_param_temp == 3 || tc.id_tc_param_temp == 7 || tc.id_tc_param_temp == 53
                                              || tc.id_tc_param_temp == 54 || tc.id_tc_param_temp == 55 || tc.id_tc_param_temp == 56 || tc.id_tc_param_temp == 57
                                              || tc.id_tc_param_temp == 60 || tc.id_tc_param_temp == 61 || tc.id_tc_param_temp == 35 || tc.id_tc_param_temp == 38
                                              || tc.id_tc_param_temp == 39 || tc.id_tc_param_temp == 59 || tc.id_tc_param_temp == 19 || tc.id_tc_param_temp == 25
                                              || tc.id_tc_param_temp == 22 || tc.id_tc_param_temp == 33 || tc.id_tc_param_temp == 42)
                                            && (plantId != 0 ? tc.TC_temp.plant_plant_id == plantId : true) && (tc.TC_temp.date == dateTechs)
                          group new {}
                              by new PlanMonthRequestDto
                              {
                                  avg = tc.value / (DbFunctions.DiffDays(tc.TC_operation_temp.date_start, tc.TC_operation_temp.date_finish) + 1),
                                  date_start = tc.TC_operation_temp.date_start,
                                  date_finish = tc.TC_operation_temp.date_finish,
                                  kol = DbFunctions.DiffDays(tc.TC_operation_temp.date_start, tc.TC_operation_temp.date_finish) + 1,
                                  id_tc_param = tc.id_tc_param_temp,
                                  mat_type_id = tc.Materials.mat_type_id,
                                  id = tc.id,
                                  id_tc_operation = tc.id_tc_operation_temp,
                                  id_tc = tc.TC_temp.plant_plant_id,
                                  grtask_id = tc.TC_operation_temp.Type_tasks.auxiliary_rate,
                                  val = tc.value,
                              } into g
                          select new PlanMonthRequestDto
                          {
                              avg = g.Key.avg,
                              date_start = g.Key.date_start,
                              date_finish = g.Key.date_finish,
                              kol = g.Key.kol,
                              id_tc_param = g.Key.id_tc_param,
                              mat_type_id = g.Key.mat_type_id,
                              id = g.Key.id,
                              id_tc_operation = g.Key.id_tc_operation,
                              id_tc = g.Key.id_tc,
                              grtask_id = g.Key.grtask_id,
                              val = g.Key.val,
                          }).ToList();
            //
            var totalplan = (from tc in db.TC_param_value_temp
                             where (tc.TC_temp.date == dateTechs) &&
                             (tc.id_tc_param_temp == 1 || tc.id_tc_param_temp == 2 || tc.id_tc_param_temp == 3 || tc.id_tc_param_temp == 7 || tc.id_tc_param_temp == 53
                             || tc.id_tc_param_temp == 54 || tc.id_tc_param_temp == 55 || tc.id_tc_param_temp == 56 || tc.id_tc_param_temp == 57
                             || tc.id_tc_param_temp == 60 || tc.id_tc_param_temp == 61 || tc.id_tc_param_temp == 35 || tc.id_tc_param_temp == 38
                             || tc.id_tc_param_temp == 39 || tc.id_tc_param_temp == 59 || tc.id_tc_param_temp == 19 || tc.id_tc_param_temp == 25
                             || tc.id_tc_param_temp == 22 || tc.id_tc_param_temp == 33)
                           && (plantId != 0 ? tc.TC_temp.plant_plant_id == plantId : true)
                             group new { }
                                 by new
                                 {
                                     plant_name = tc.TC_temp.Plants.name,
                                     plant_id = tc.TC_temp.plant_plant_id,
                                     id_tc = tc.TC_temp.plant_plant_id,
                                     id_tc_param = tc.id_tc_param_temp,
                                     value = tc.value,
                                     id = tc.id,
                                     grtask_id = tc.TC_operation_temp.Type_tasks.auxiliary_rate,
                                 } into g
                             select new PlanRequestDto
                             {
                                 plant_name = g.Key.plant_name,
                                 plant_id = (int)(g.Key.plant_id ?? 0),
                                 id_tc = (int)(g.Key.plant_id ?? 0),
                                 id_tc_param1 = g.Key.id_tc_param,
                                 value = g.Key.value,
                                 id = g.Key.id,
                                 grtask_id = g.Key.grtask_id
                             }).OrderBy(l => l.plant_name).ToList();
            int j3 = 0;
            var totplan = totalplan.Select(s => s.id_tc).Distinct().ToList();
            for (j3 = 0; j3 < totplan.Count; j3++)
            {
                int id = (int)totplan[j3];
                var area = totalplan.Where(s => s.id_tc == totplan[j3] && s.id_tc_param1 == 1).Select(s => s.value).Sum();
                var gross_harvest = totalplan.Where(s => s.id_tc == totplan[j3] && s.id_tc_param1 == 3).Select(s => s.value).Sum();
                var val = getValueFromPeriod(period, j3, startDate, endDate, id, 42, null, 5);
                var dop_material = (float?)val;
                var dop_material_ga = area == 0 ? 0 : (dop_material == null ? 0 : dop_material) / area;
                val = getValueFromPeriod(period, j3, startDate, endDate, id, 35, 1, null);
                var salary = (float?)val;
                val = getValueFromPeriod(period, j3, startDate, endDate, id, 38, 1, null);
                var dopsalary = (float?)val;
                var deduction = (salary == null ? 0 : salary * 0.321) + (dopsalary == null ? 0 : dopsalary * 0.321);
                //орошение
                val = getValueFromPeriod(period, j3, startDate, endDate, id, 35, 2, null);
                var salary_watering = (float?)val;
                val = getValueFromPeriod(period, j3, startDate, endDate, id, 38, 2, null);
                var dopsalary_watering = (float?)val;
                var deduction_watering = (salary_watering == null ? 0 : salary_watering * 0.321) + (dopsalary_watering == null ? 0 : dopsalary_watering * 0.321);
                //
                var total_salary = (salary == null ? 0 : salary) + (dopsalary == null ? 0 : dopsalary) + (deduction == null ? 0 : deduction) +
                   (salary_watering == null ? 0 : salary_watering) + (dopsalary_watering == null ? 0 : dopsalary_watering)
                   + (deduction_watering == null ? 0 : deduction_watering);
                float? consumption = getValueFromPeriodForFuel(period, j3, startDate, endDate, id, 33, 1, null);
                val = getValueFromPeriod(period, j3, startDate, endDate, id, 39, 1, null);
                var fuel_cost = (float?)val;
                var fuel_cost_ga = area == 0 ? 0 : (consumption == null ? 0 : consumption) / area;
                //топливо орошение
                float? consumption_watering = getValueFromPeriodForFuel(period, j3, startDate, endDate, id, 33, 2, null);
                val = getValueFromPeriod(period, j3, startDate, endDate, id, 39, 2, null);
                var fuel_cost_watering = (float?)val;
                var fuel_cost_ga_watering = area == 0 ? 0 : (consumption_watering == null ? 0 : consumption_watering) / area;
                //
                var total_consumption = consumption + consumption_watering;
                val = getValueFromPeriod(period, j3, startDate, endDate, id, 42, null, 4);
                var seed = (float?)val;
                val = getValueFromPeriod(period, j3, startDate, endDate, id, 42, null, 2);
                var chemicalFertilizers = (float?)val;
                val = getValueFromPeriod(period, j3, startDate, endDate, id, 42, null, 1);
                var szr = (float?)val;
                var total_cost = (fuel_cost ?? 0) + (fuel_cost_watering ?? 0);  //итого по топливу
                var combine = db.TC_to_service_temp.Where(t => t.TC_temp.date == dateTechs && t.id_service == 1 && t.TC_temp.plant_plant_id == id).Select(v => (float?)v.cost).Sum();
                var loader = db.TC_to_service_temp.Where(t => t.TC_temp.date == dateTechs && t.id_service == 2 && t.TC_temp.plant_plant_id == id).Select(v => (float?)v.cost).Sum();
                var autotransport = db.TC_to_service_temp.Where(t => t.TC_temp.date == dateTechs && t.id_service == 3 && t.TC_temp.plant_plant_id == id).Select(v => (float?)v.cost).Sum();
                var desiccation = db.TC_to_service_temp.Where(t => t.TC_temp.date == dateTechs && t.id_service == 4 && t.TC_temp.plant_plant_id == id).Select(v => (float?)v.cost).Sum();
                var analyzes = db.TC_to_service_temp.Where(t => t.TC_temp.date == dateTechs && t.id_service == 5 && t.TC_temp.plant_plant_id == id).Select(v => (float?)v.cost).Sum();
                var total_service = db.TC_to_service_temp.Where(t => t.TC_temp.date == dateTechs && t.TC_temp.plant_plant_id == id).Select(v => (float?)v.cost).Sum();

                totalplan1.Add(new PlanRequestDto
                {
                    id_tc = id,
                    plant_name = totalplan.Where(s => s.id_tc == totplan[j3]).Select(s => s.plant_name).FirstOrDefault(),
                    area = (decimal?)area,
                    productivity = totalplan.Where(s => s.id_tc == totplan[j3] && s.id_tc_param1 == 2).Select(s => s.value).Sum(),
                    gross_harvest = gross_harvest,
                    nzp = 0,
                    nzp_zp = 0,
                    nzp_fuel = 0,
                    nzp_szr = 0,
                    nzp_chemicalFertilizers = 0,
                    nzp_seed = 0,
                    nzp_dop_material = 0,
                    dop_material = (decimal?)dop_material,
                    dop_material_ga = dop_material_ga,
                    salary = (decimal?)salary,
                    dopsalary = (decimal?)dopsalary,
                    salary_watering = (decimal?)salary_watering,
                    dopsalary_watering = (decimal?)dopsalary_watering,
                    deduction_watering = (decimal?)deduction_watering,
                    fuel_cost = (decimal?)fuel_cost,
                    fuel_cost_watering = (decimal?)fuel_cost_watering,
                    deduction = (decimal?)deduction,
                    total_salary = (decimal?)total_salary,
                    consumption = (decimal?)consumption,
                    consumption_watering = (decimal?)consumption_watering,
                    fuel_cost_ga = fuel_cost_ga,
                    fuel_cost_ga_watering = fuel_cost_ga_watering,
                    total_consumption = (decimal?)total_consumption,
                    total_cost = (decimal?)total_cost,  //итого по топливу
                    seed = (decimal?)seed,
                    szr = (decimal?)szr,
                    szr_ga = area == 0 ? 0 : (szr == null ? 0 : szr) / area,
                    chemicalFertilizers = (decimal?)chemicalFertilizers,
                    chemicalFertilizers_ga = area == 0 ? 0 : (chemicalFertilizers == null ? 0 : chemicalFertilizers) / area,
                    combine = (decimal?)combine,
                    loader = (decimal?)loader,
                    autotransport = (decimal?)autotransport,
                    desiccation = (decimal?)desiccation,
                    analyzes = (decimal?)analyzes,
                    total_service = (decimal?)total_service,
                    total = 0,
                    total_ga = 0,
                    total_t = 0,
                });
            }
            //////добавляем к итого нзп -план
            for (int i = 0; i < sevoborot.Count; i++)
            {
                for (int j = 0; j < totalplan1.Count; j++)
                {
                    if (totalplan1[j].id_tc == sevoborot[i].plant_id)
                    {
                        //нзп всего
                        totalplan1[j].nzp = (totalplan1[j].nzp ?? 0) + (decimal)sevoborot[i].total_salary + (decimal)sevoborot[i].consumption +
                        (decimal)sevoborot[i].szr + (decimal)sevoborot[i].chemicalFertilizers + (decimal)sevoborot[i].dop_salary +
                        (decimal)sevoborot[i].seed + (decimal)sevoborot[i].dopmaterial + (decimal)sevoborot[i].deduction;

                        totalplan1[j].nzp_zp = (totalplan1[j].nzp_zp ?? 0) + (sevoborot[i].total_salary ?? 0) + (sevoborot[i].dop_salary ?? 0); //детализация
                        totalplan1[j].nzp_fuel = (totalplan1[j].nzp_fuel ?? 0) + (sevoborot[i].consumption ?? 0);
                        totalplan1[j].nzp_seed = (totalplan1[j].nzp_seed ?? 0) + (sevoborot[i].seed ?? 0);
                        totalplan1[j].nzp_szr = (totalplan1[j].nzp_szr ?? 0) + (sevoborot[i].szr ?? 0);
                        totalplan1[j].nzp_chemicalFertilizers = (totalplan1[j].nzp_chemicalFertilizers ?? 0) + (sevoborot[i].chemicalFertilizers ?? 0);
                        totalplan1[j].nzp_dop_material = (totalplan1[j].nzp_dop_material ?? 0) + (sevoborot[i].dopmaterial ?? 0);

                        //ИТОГО
                        totalplan1[j].total = (totalplan1[j].nzp == null ? 0 : totalplan1[j].nzp) +
                        (totalplan1[j].total_salary == null ? 0 : totalplan1[j].total_salary) +
                        (totalplan1[j].total_cost == null ? 0 : totalplan1[j].total_cost)
                         + (totalplan1[j].seed == null ? 0 : totalplan1[j].seed) + (totalplan1[j].szr == null ? 0 : totalplan1[j].szr) +
                         (totalplan1[j].chemicalFertilizers == null ? 0 : totalplan1[j].chemicalFertilizers)
                         + (totalplan1[j].total_service == null ? 0 : totalplan1[j].total_service) +
                         (totalplan1[j].dop_material == null ? 0 : totalplan1[j].dop_material);

                        totalplan1[j].total_ga = (double?)totalplan1[j].area == 0 ? 0 : (double?)(totalplan1[j].total / totalplan1[j].area);
                        totalplan1[j].total_t = (double?)totalplan1[j].gross_harvest == 0 ? 0 : (double?)(totalplan1[j].total / (decimal?)totalplan1[j].gross_harvest);
                    }
                }
            }
            return totalplan1;
        }
        //


      
        /////TOTAL FACT
        public List<PlanRequestDto> getTotalFact(DateTime? startDate, DateTime? endDate, int? plantId, List<SevooborotModel> sevoborot)
        {
            endDate = endDate.Value.AddDays(1);
            //топливо и ЗП из путевых листов
            var list = (from ftp in db.Fields_to_plants
                        where ftp.Fields.type == 1 && (plantId != 0 ? ftp.plant_plant_id == plantId : true)
                        join task1 in db.Tasks on ftp.fielplan_id equals task1.plant_plant_id into leftTask1
                        from t1 in leftTask1.DefaultIfEmpty()
                        where t1.Sheet_tracks.date_begin >= startDate && t1.Sheet_tracks.date_begin <= endDate
                        group new
                        {
                            consumption = t1.fuel_cost ?? 0,
                            total_salary = t1.total_salary ?? 0,
                            consumption_fact = t1.consumption_fact ?? 0,
                        }
                            by new
                            {
                                name = ftp.Plants.name,
                                id = ftp.plant_plant_id,
                            } into g
                        select new
                        {
                            id = g.Key.id,
                            name = g.Key.name,
                            consumption = (float?)g.Sum(p => p.consumption),
                            total_salary = (float?)g.Sum(p => p.total_salary),
                            consumption_fact = (float?)g.Sum(p => p.consumption_fact)
                        }).ToList();

            //сдельная ЗП полевые
            var dopSalary1 = (from ftp in db.Fields_to_plants
                              where ftp.Fields.type == 1 && (plantId != 0 ? ftp.plant_plant_id == plantId : true)
                              join sal_detail in db.Salary_details on ftp.fielplan_id equals sal_detail.plant_plant_id into leftDetail
                              from t2 in leftDetail.DefaultIfEmpty()
                              where t2.date >= startDate && t2.date <= endDate && t2.Type_tasks.auxiliary_rate == 1

                              group new
                              {
                                  sum1 = t2.sum ?? 0,
                              }
                                  by new
                                  {
                                      name = ftp.Plants.name,
                                      plant_plant_id = ftp.plant_plant_id,
                                  } into g
                              select new
                              {
                                  plant_plant_id = g.Key.plant_plant_id,
                                  dopSalary1 = g.Sum(p => p.sum1),
                              }).ToList();

            //сдельная ЗП орошение
            var dopSalary2 = (from ftp in db.Fields_to_plants
                              where ftp.Fields.type == 1 && (plantId != 0 ? ftp.plant_plant_id == plantId : true)

                              join sal_detail1 in db.Salary_details on ftp.fielplan_id equals sal_detail1.plant_plant_id into leftDetail1
                              from t3 in leftDetail1.DefaultIfEmpty()
                              where t3.date >= startDate && t3.date <= endDate && t3.Type_tasks.auxiliary_rate == 2
                              group new
                              {
                                  sum2 = t3.sum ?? 0,
                              }
                                  by new
                                  {
                                      name = ftp.Plants.name,
                                      plant_plant_id = ftp.plant_plant_id,
                                  } into g
                              select new
                              {
                                  plant_plant_id = g.Key.plant_plant_id,
                                  dopSalary2 = g.Sum(p => p.sum2),
                              }).ToList();

            //услуги
            var totalService = (from s in db.Service_records
                                where s.date >= startDate && s.date <= endDate
                                && (plantId != 0 ? s.plant_plant_id == plantId : true)
                                group new
                                {
                                    s
                                }
                                    by new
                                    {
                                        plant_plant_id = s.plant_plant_id,
                                    } into g
                                select new
                                {
                                    plant_plant_id = g.Key.plant_plant_id,
                                    Values = (
                                  from m in g.Where(ga => ga.s != null)
                                  group new
                                  {
                                      cost = m.s.cost
                                  } by new
                                  {
                                      m.s.service_id
                                  } into g2
                                  select new
                                  {
                                      type_id = g2.Key.service_id,
                                      cost = g2.Sum(m1 => m1.cost)
                                  }).ToList()
                                }).ToList();
            //сзр, удобрения, семена, прочие материалы
            var acts = (from ftp in db.Fields_to_plants
                        where ftp.Fields.type == 1 && (plantId != 0 ? ftp.plant_plant_id == plantId : true)
                        join act in db.TMC_Records on ftp.fielplan_id equals act.fielplan_fielplan_id into leftAct1
                        from a in leftAct1.DefaultIfEmpty()
                        where a.emp_from_id != 0 && a.emp_to_id == 0 && a.TMC_Acts.act_date >= startDate && a.TMC_Acts.act_date <= endDate
                        group new { ftp, a } by new { ftp.plant_plant_id, ftp.Plants.name } into g
                        select new
                        {
                            name = g.Key.name,
                            id = g.Key.plant_plant_id,
                            Values = (
                                from m in g.Where(ga => ga.a != null)
                                group new
                                {
                                    cost = (m.a.count * m.a.price),
                                } by new
                                {
                                    m.a.Materials.mat_type_id
                                } into g2
                                select new
                                {
                                    type_id = g2.Key.mat_type_id,
                                    cost = g2.Sum(mat => mat.cost)
                                }
                            ).ToList()
                        }).ToList();
            //валовый сбор
            var dopGross = (from gh in db.Gross_harvest
                            where gh.Fields_to_plants.date_ingathering.Year == startDate.Value.Year &&
                            (plantId != 0 ? gh.Fields_to_plants.plant_plant_id == plantId : true)
                            group gh by new { gh.Fields_to_plants.plant_plant_id } into g
                            select new
                            {
                                plant_plant_id = g.Key.plant_plant_id,
                                gross_harvest = g.Sum(g1 => g1.count / 1000)
                            });
            //площадь
            var dopArea = (from ftp in db.Fields_to_plants
                           where ftp.date_ingathering.Year == startDate.Value.Year
                           group ftp.area_plant by new { ftp.plant_plant_id } into g
                           select new
                           {
                               plant_plant_id = g.Key.plant_plant_id,
                               area = (double)g.Sum()
                           });
            var dopArea2 = (from ftp in db.Fields_to_plants
                           where ftp.date_ingathering.Year == startDate.Value.Year
                           group ftp.area_plant by new { ftp.plant_plant_id } into g
                           select new
                           {
                               plant_plant_id = g.Key.plant_plant_id,
                               area = (double)g.Sum()
                           }).ToList();

          var  totalfact = (from l in list
                          join dG in dopGross on l.id equals dG.plant_plant_id
                              into jGross
                          from gross in jGross.DefaultIfEmpty()
                          join dA in dopArea on l.id equals dA.plant_plant_id
                              into jArea
                          from area in jArea.DefaultIfEmpty()
                          join dS in dopSalary1 on l.id equals dS.plant_plant_id
                          into jSalary
                          from salary in jSalary.DefaultIfEmpty()

                          join dS1 in dopSalary2 on l.id equals dS1.plant_plant_id
                         into jSalary1
                          from salary1 in jSalary1.DefaultIfEmpty()

                          select new PlanRequestDto
                          {
                              plant_name = l.name,
                              area1 = area != null ? area.area : 0,
                              productivity = 0,
                              gross_harvest = (gross != null && gross.gross_harvest.HasValue) ? (float?)gross.gross_harvest : 0,
                              id_tc = l.id,
                              salary1 = (l.total_salary == null ? 0 : l.total_salary),
                              dopsalary = (decimal?)(salary == null ? 0 : salary.dopSalary1),       //сдельная - полевые
                              deduction = 0,
                              salary_watering = (decimal?)(salary1 == null ? 0 : salary1.dopSalary2),//сдельная - орошение
                              deduction_watering = 0,
                              total_salary = 0,
                              fuel_cost1 = (l.consumption == null ? 0 : l.consumption),
                              consumption1 = (l.consumption_fact == null ? 0 : l.consumption_fact),
                              fuel_cost_ga = 0,
                              total_consumption = 0,
                              total_cost = 0,
                              seed = 0,
                              szr = 0,
                              szr_ga = 0,
                              chemicalFertilizers = 0,
                              chemicalFertilizers_ga = 0,
                              dop_material = 0,
                              dop_material_ga = 0,
                              combine = 0,
                              loader = 0,
                              autotransport = 0,
                              desiccation = 0,
                              analyzes = 0,
                              total = 0,
                              total_ga = 0,
                              total_t = 0,
                              nzp_zp = 0,
                              nzp_chemicalFertilizers = 0,
                              nzp_dop_material = 0,
                              nzp_fuel = 0,
                              nzp_seed = 0,
                              nzp_szr = 0,
                              nzp = 0,
                          }).OrderBy(p => p.plant_name).ToList();
            ////
            //////добавляем к итого нзп -факт
            for (int i = 0; i < sevoborot.Count; i++)
            {
                for (int j = 0; j < totalfact.Count; j++)
                {
                    if (totalfact[j].id_tc == sevoborot[i].plant_id)
                    {
                        //ФАКТ
                        totalfact[j].nzp = totalfact[j].nzp + (decimal)sevoborot[i].total_salary + (decimal)sevoborot[i].consumption +
                        (decimal)sevoborot[i].szr + (decimal)sevoborot[i].chemicalFertilizers + (decimal)sevoborot[i].dop_salary +
                        (decimal)sevoborot[i].seed + (decimal)sevoborot[i].dopmaterial + (decimal)sevoborot[i].deduction;
                        //детализация
                        totalfact[j].nzp_zp = (totalfact[j].nzp_zp ?? 0) + (double?)(sevoborot[i].total_salary ?? 0) + (sevoborot[i].dop_salary ?? 0);
                        totalfact[j].nzp_fuel = (totalfact[j].nzp_fuel ?? 0) + (double?)(sevoborot[i].consumption ?? 0);
                        totalfact[j].nzp_szr = (totalfact[j].nzp_szr ?? 0) + (double?)(sevoborot[i].szr ?? 0);
                        totalfact[j].nzp_seed = (totalfact[j].nzp_seed ?? 0) + (double?)(sevoborot[i].seed ?? 0);
                        totalfact[j].nzp_chemicalFertilizers = (totalfact[j].nzp_chemicalFertilizers ?? 0) + (double?)(sevoborot[i].chemicalFertilizers ?? 0);
                        totalfact[j].nzp_dop_material = (totalfact[j].nzp_dop_material ?? 0) + (double?)(sevoborot[i].dopmaterial ?? 0);
                    }
                }
            }

            ///убираем повторяющиеся культуры и суммируем факт
            for (int j = 0; j < totalfact.Count; j++)
            {
                totalfact[j].area = (decimal?)totalfact[j].area1;
                totalfact[j].salary = (decimal?)totalfact[j].salary1;
                totalfact[j].consumption = (decimal?)totalfact[j].consumption1;
                totalfact[j].seed = (decimal?)totalfact[j].seed1;
                if (totalfact[j].area != null && totalfact[j].area != 0) { totalfact[j].fuel_cost_ga = (float?)((totalfact[j].consumption ?? 0) / (totalfact[j].area)); }

                totalfact[j].fuel_cost = (decimal?)totalfact[j].fuel_cost1;
                if (totalfact[j].area != 0 && totalfact[j].area != null)
                { totalfact[j].productivity = (totalfact[j].gross_harvest ?? 0) / (float)(totalfact[j].area); }
                totalfact[j].deduction = (decimal?)((totalfact[j].salary == null ? 0 : (double)totalfact[j].salary * 0.321) +
                (totalfact[j].dopsalary == null ? 0 : (double)totalfact[j].dopsalary * 0.321)); //отчисления
                totalfact[j].deduction_watering = (decimal?)((totalfact[j].salary_watering == null ? 0 : (double?)totalfact[j].salary_watering * 0.321)
                    + (totalfact[j].dopsalary_watering == null ? 0 : (double?)totalfact[j].dopsalary_watering * 0.321)); //отчисление орошение

                totalfact[j].total_salary = (totalfact[j].salary == null ? 0 : totalfact[j].salary) + (totalfact[j].dopsalary == null ? 0 : totalfact[j].dopsalary) +
               (totalfact[j].deduction == null ? 0 : totalfact[j].deduction) + (totalfact[j].salary_watering == null ? 0 : totalfact[j].salary_watering) +
              (totalfact[j].dopsalary_watering == null ? 0 : totalfact[j].dopsalary_watering) +
              (totalfact[j].deduction_watering == null ? 0 : totalfact[j].deduction_watering);

                totalfact[j].total_consumption = (totalfact[j].consumption ?? 0) + (totalfact[j].consumption_watering ?? 0);

                totalfact[j].total_cost = (totalfact[j].fuel_cost ?? 0) + (totalfact[j].fuel_cost_watering ?? 0);
                //ТМЦ
                for (int l = 0; l < acts.Count; l++)
                {
                    if (totalfact[j].id_tc == acts[l].id)
                    {
                        totalfact[j].seed = (decimal?)acts[l].Values.Where(t => t.type_id == 4).Select(t => t.cost).Sum();
                        totalfact[j].szr = (decimal?)acts[l].Values.Where(t => t.type_id == 1).Select(t => t.cost).Sum();
                        totalfact[j].chemicalFertilizers = (decimal?)acts[l].Values.Where(t => t.type_id == 2).Select(t => t.cost).Sum();
                        totalfact[j].dop_material = (decimal?)acts[l].Values.Where(t => t.type_id == 5).Select(t => t.cost).Sum();
                        if (totalfact[j].area != null && totalfact[j].area != 0)
                        {
                            totalfact[j].szr_ga = (float?)((totalfact[j].szr ?? 0) / (totalfact[j].area));
                            totalfact[j].chemicalFertilizers_ga = (float?)((totalfact[j].chemicalFertilizers ?? 0) / (totalfact[j].area));
                            totalfact[j].dop_material_ga = (float?)((totalfact[j].dop_material ?? 0) / (totalfact[j].area));
                        }
                    }
                }
                //услуги
                for (int l = 0; l < totalService.Count; l++)
                {
                    if (totalfact[j].id_tc == totalService[l].plant_plant_id)
                    {
                        totalfact[j].combine = totalfact[j].combine + (decimal?)(totalService[l].Values.Where(t => t.type_id == 1).Select(t => t.cost).Sum() ?? 0);
                        totalfact[j].loader = totalfact[j].loader + (decimal?)(totalService[l].Values.Where(t => t.type_id == 2).Select(t => t.cost).Sum() ?? 0);
                        totalfact[j].autotransport = totalfact[j].autotransport + (decimal?)(totalService[l].Values.Where(t => t.type_id == 3).Select(t => t.cost).Sum() ?? 0);
                        totalfact[j].desiccation = totalfact[j].desiccation + (decimal?)(totalService[l].Values.Where(t => t.type_id == 4).Select(t => t.cost).Sum() ?? 0);
                        totalfact[j].analyzes = totalfact[j].analyzes + (decimal?)(totalService[l].Values.Where(t => t.type_id == 5).Select(t => t.cost).Sum() ?? 0);
                        totalfact[j].total_service = (decimal?)(totalService[l].Values.Select(t => t.cost).Sum() ?? 0);
                    }
                }
                //ИТОГО
                totalfact[j].total = (totalfact[j].nzp == null ? 0 : totalfact[j].nzp) +
                (totalfact[j].total_salary == null ? 0 : totalfact[j].total_salary) +
                (totalfact[j].total_cost == null ? 0 : totalfact[j].total_cost)
                 + (totalfact[j].seed == null ? 0 : totalfact[j].seed) + (totalfact[j].szr == null ? 0 : totalfact[j].szr) +
                 (totalfact[j].chemicalFertilizers == null ? 0 : totalfact[j].chemicalFertilizers)
                 + (totalfact[j].total_service == null ? 0 : totalfact[j].total_service) +
                 (totalfact[j].dop_material == null ? 0 : totalfact[j].dop_material);

                totalfact[j].total_ga = (double?)totalfact[j].area == 0 ? 0 : (double?)(totalfact[j].total / totalfact[j].area);
                totalfact[j].total_t = (double?)totalfact[j].gross_harvest == 0 ? 0 : (double?)(totalfact[j].total / (decimal?)totalfact[j].gross_harvest);

                if (totalfact[j].id_tc == 36) { totalfact.RemoveAt(j); j--; } //удаляем строку урожай
                if (j > 0)
                {
                    if (totalfact[j].id_tc == totalfact[j - 1].id_tc)
                    {
                        totalfact[j].area = totalfact[j].area + totalfact[j - 1].area;
                        totalfact.RemoveAt(j - 1);
                        j--;
                    }
                }
            }
            return totalfact;
        }

    /////

        public decimal? getValueFromPeriod(List<PlanMonthRequestDto> period, int j3, DateTime? startDate, DateTime? endDate, int id, int id_tc_param, int? grtask_id, int? mat_type_id)
        {
            //за период
            var m1 = period.Where(t => t.id_tc == id && (mat_type_id != null ? t.mat_type_id == mat_type_id : true) && (grtask_id != null ? t.grtask_id == grtask_id : true) && t.id_tc_param == id_tc_param && (t.date_start >= startDate && t.date_finish <= endDate)).Select(b => b.avg * b.kol).Sum(); //полностью входит 
            var m2 = period.Where(t => t.id_tc == id && (mat_type_id != null ? t.mat_type_id == mat_type_id : true) && (grtask_id != null ? t.grtask_id == grtask_id : true) && t.id_tc_param == id_tc_param && t.date_start < startDate && t.date_finish >= startDate && t.date_finish <= endDate).Select(b => b.avg * (b.date_finish.Value.DayOfYear - (startDate.Value.DayOfYear - 1))).Sum(); // меньше start и в середину 
            var m3 = period.Where(t => t.id_tc == id && (mat_type_id != null ? t.mat_type_id == mat_type_id : true) && (grtask_id != null ? t.grtask_id == grtask_id : true) && t.id_tc_param == id_tc_param && t.date_start >= startDate && t.date_start <= endDate && t.date_finish > endDate).Select(b => b.avg * (endDate.Value.DayOfYear - (b.date_start.Value.DayOfYear - 1))).Sum(); // c середины и больше end 
            var m4 = period.Where(t => t.id_tc == id && (mat_type_id != null ? t.mat_type_id == mat_type_id : true) && (grtask_id != null ? t.grtask_id == grtask_id : true) && t.id_tc_param == id_tc_param && t.date_start < startDate && t.date_finish > endDate).Select(b => b.avg * (endDate.Value.DayOfYear - (startDate.Value.DayOfYear - 1))).Sum(); // меньше start и больше end 
            //
            return (decimal)(m1 + m2 + m3 + m4);
        }
        // для топлива
        public float? getValueFromPeriodForFuel(List<PlanMonthRequestDto> period, int j3, DateTime? startDate, DateTime? endDate, int id, int id_tc_param, int? grtask_id, int? mat_type_id)
        {
            //за период
            var w1 = period.Where(t => t.id_tc == id && (mat_type_id != null ? t.mat_type_id == mat_type_id : true) && (grtask_id != null ? t.grtask_id == grtask_id : true) && t.id_tc_param == id_tc_param && (t.date_start >= startDate && t.date_finish <= endDate)).OrderBy(t=>t.id_tc_operation).Select(b => b.avg * b.kol).ToList(); //полностью входит 
         // var test = period.Where(t => t.id_tc == id && (mat_type_id != null ? t.mat_type_id == mat_type_id : true) && (grtask_id != null ? t.grtask_id == grtask_id : true) && t.id_tc_param == id_tc_param && (t.date_start >= startDate && t.date_finish <= endDate)).ToList();
            var w2 = period.Where(t => t.id_tc == id && (mat_type_id != null ? t.mat_type_id == mat_type_id : true) && (grtask_id != null ? t.grtask_id == grtask_id : true) && t.id_tc_param == id_tc_param && t.date_start < startDate && t.date_finish >= startDate && t.date_finish <= endDate).Select(b => b.avg * (b.date_finish.Value.DayOfYear - (startDate.Value.DayOfYear - 1))).ToList(); // меньше start и в середину 
            var w3 = period.Where(t => t.id_tc == id && (mat_type_id != null ? t.mat_type_id == mat_type_id : true) && (grtask_id != null ? t.grtask_id == grtask_id : true) && t.id_tc_param == id_tc_param && t.date_start >= startDate && t.date_start <= endDate && t.date_finish > endDate).Select(b => b.avg * (endDate.Value.DayOfYear - (b.date_start.Value.DayOfYear - 1))).ToList(); // c середины и больше end 
            var w4 = period.Where(t => t.id_tc == id && (mat_type_id != null ? t.mat_type_id == mat_type_id : true) && (grtask_id != null ? t.grtask_id == grtask_id : true) && t.id_tc_param == id_tc_param && t.date_start < startDate && t.date_finish > endDate).Select(b => b.avg * (endDate.Value.DayOfYear - (startDate.Value.DayOfYear - 1))).ToList(); // меньше start и больше end 
            //
            var f1 = period.Where(t => t.id_tc == id && (mat_type_id != null ? t.mat_type_id == mat_type_id : true) && (grtask_id != null ? t.grtask_id == grtask_id : true) && t.id_tc_param == 59 && (t.date_start >= startDate && t.date_finish <= endDate)).OrderBy(t=>t.id_tc_operation).Select(b => b.val).ToList(); //полностью входит 
          //  var test2 = period.Where(t => t.id_tc == id && (mat_type_id != null ? t.mat_type_id == mat_type_id : true) && (grtask_id != null ? t.grtask_id == grtask_id : true) && t.id_tc_param == 59 && (t.date_start >= startDate && t.date_finish <= endDate)).ToList(); //полностью входит 
            var f2 = period.Where(t => t.id_tc == id && (mat_type_id != null ? t.mat_type_id == mat_type_id : true) && (grtask_id != null ? t.grtask_id == grtask_id : true) && t.id_tc_param == 59 && t.date_start < startDate && t.date_finish >= startDate && t.date_finish <= endDate).Select(b => b.val).ToList(); // меньше start и в середину 
            var f3 = period.Where(t => t.id_tc == id && (mat_type_id != null ? t.mat_type_id == mat_type_id : true) && (grtask_id != null ? t.grtask_id == grtask_id : true) && t.id_tc_param == 59 && t.date_start >= startDate && t.date_start <= endDate && t.date_finish > endDate).Select(b => b.val).ToList(); // c середины и больше end 
            var f4 = period.Where(t => t.id_tc == id && (mat_type_id != null ? t.mat_type_id == mat_type_id : true) && (grtask_id != null ? t.grtask_id == grtask_id : true) && t.id_tc_param == 59 && t.date_start < startDate && t.date_finish > endDate).Select(b => b.val).ToList(); // меньше start и больше end 
            var s = getSumFuel(w1, f1) + getSumFuel(w2, f2) + getSumFuel(w3, f3) + getSumFuel(w4, f4);
                return (float?)s;
        }

        public decimal? getSumFuel(List<float?> w,List<float?> f)
        {
            var sum = 0.0;
            for (int i = 0; i < w.Count; i++)
            {
                sum = sum + (w[i] ?? 0)  * (f[i] ?? 0);
            }
            return (decimal)sum;
        }
        static WorksheetPart GetWorkSheetPart(WorkbookPart workbookPart, string sheetName)
        {

            string relId = workbookPart.Workbook.Descendants<Sheet>()

            .Where(s => s.Name.Value.Equals(sheetName)).First().Id;

            return (WorksheetPart)workbookPart.GetPartById(relId);
        }
        //
        static void CopySheet(string filename, string sheetName, string clonedSheetName, SpreadsheetDocument document)
       {
               WorksheetPart sourceSheetPart = GetWorkSheetPart(document.WorkbookPart, sheetName);

               SpreadsheetDocument tempSheet = SpreadsheetDocument.Create(new MemoryStream(), document.DocumentType);

               WorkbookPart tempWorkbookPart = tempSheet.AddWorkbookPart();

               WorksheetPart tempWorksheetPart = tempWorkbookPart.AddPart<WorksheetPart>(sourceSheetPart);

               WorksheetPart clonedSheet = document.WorkbookPart.AddPart<WorksheetPart>(tempWorksheetPart);
               int numTableDefParts = sourceSheetPart.GetPartsCountOfType<TableDefinitionPart>();

               var tableId = numTableDefParts;

               if (numTableDefParts != 0)

                   FixupTableParts(clonedSheet, numTableDefParts);

               CleanView(clonedSheet);
               Sheets sheets = document.WorkbookPart.Workbook.GetFirstChild<Sheets>();

               Sheet copiedSheet = new Sheet();

               copiedSheet.Name = clonedSheetName;

               copiedSheet.Id = document.WorkbookPart.GetIdOfPart(clonedSheet);

               copiedSheet.SheetId = (uint)sheets.ChildElements.Count + 2;

               sheets.Append(copiedSheet);
               document.WorkbookPart.Workbook.Save();               
}
        static void CleanView(WorksheetPart worksheetPart)
        {

            SheetViews views = worksheetPart.Worksheet.GetFirstChild<SheetViews>();

            if (views != null)
            {

                views.Remove();

               worksheetPart.Worksheet.Save();

            }

        }
        static void FixupTableParts(WorksheetPart worksheetPart, int numTableDefParts)

        {
        var   tableId = numTableDefParts;

       foreach (TableDefinitionPart tableDefPart in worksheetPart.TableDefinitionParts)

       {

     tableId++;

     tableDefPart.Table.Id = (uint)tableId;

     tableDefPart.Table.DisplayName = "CopiedTable" + tableId;

     tableDefPart.Table.Name = "CopiedTable" + tableId;

     tableDefPart.Table.Save();
     }

     }
        //

        public decimal? getValueForMonth(int i, List<PlanMonthRequestDto> months, int year, int? mat_type_id, int id_tc_param, int? id_tc_param1)
        {
            var s =(decimal?) months.Where(v => v.date_start.Value.Month == i && v.date_finish.Value.Month == i && (id_tc_param1 != null ? (v.id_tc_param == id_tc_param || v.id_tc_param == id_tc_param1) : (v.id_tc_param == id_tc_param)) && (mat_type_id != null ? v.mat_type_id == mat_type_id : true)).Select(v => v.avg * v.kol).Sum();
            var s1 = (decimal?)months.Where(v => v.date_start.Value.Month == i && v.date_finish.Value.Month != i && (id_tc_param1 != null ? (v.id_tc_param == id_tc_param || v.id_tc_param == id_tc_param1) : (v.id_tc_param == id_tc_param)) && (mat_type_id != null ? v.mat_type_id == mat_type_id : true)).Select(v => v.avg * ((DateTime.DaysInMonth(year, i) + 1) - v.date_start.Value.Day)).Sum();
            var s2 = (decimal?)months.Where(v => v.date_start.Value.Month != i && v.date_finish.Value.Month == i && (id_tc_param1 != null ? (v.id_tc_param == id_tc_param || v.id_tc_param == id_tc_param1) : (v.id_tc_param == id_tc_param)) && (mat_type_id != null ? v.mat_type_id == mat_type_id : true)).Select(v => v.avg * (v.date_finish.Value.Day)).Sum();
            var s3 = (decimal?)months.Where(v => v.date_start.Value.Month != i && v.date_finish.Value.Month != i &&
          v.date_start.Value.Month < i && v.date_finish.Value.Month > i && (id_tc_param1 != null ? (v.id_tc_param == id_tc_param || v.id_tc_param == id_tc_param1) : (v.id_tc_param == id_tc_param))
          && (mat_type_id != null ? v.mat_type_id == mat_type_id : true)).Select(v => v.avg * (DateTime.DaysInMonth(year, i))).Sum();
            return (decimal?)(s + s1 + s2 + s3);
        }
        //
        public void setHeader(string pathTemplate, List<PlanRequestDto> totalplan, Worksheet Worksheet, SpreadsheetDocument document,string letter, string num, int k)
        {
            string s = letter + num;
            string val = GetCellValue(pathTemplate, "Свод затрат за период", s);

            cellDat = GetCell(Worksheet, letter, (uint)(totalplan.Count + k));
            cellDat.CellValue = new CellValue(val);
            cellDat.DataType = new EnumValue<CellValues>(CellValues.String);

            Fills fs = AddFormatPlan(document.WorkbookPart.WorkbookStylesPart.Stylesheet.Fills,
           document.WorkbookPart.WorkbookStylesPart.Stylesheet.Fonts, document.WorkbookPart.WorkbookStylesPart.Stylesheet.Borders);
            AddCellFormatPlan(document.WorkbookPart.WorkbookStylesPart.Stylesheet.CellFormats, document.WorkbookPart.WorkbookStylesPart.Stylesheet.Fills,
              document.WorkbookPart.WorkbookStylesPart.Stylesheet.Fonts, document.WorkbookPart.WorkbookStylesPart.Stylesheet.Borders
             );
            cellDat.StyleIndex = (UInt32)(document.WorkbookPart.WorkbookStylesPart.Stylesheet.CellFormats.Elements<CellFormat>().Count() - 1);

          
        }
        public void setCellFormat(string pathTemplate, List<PlanRequestDto> totalplan, Worksheet Worksheet, SpreadsheetDocument document)
        {
            Fills fs = AddFormatPlan(document.WorkbookPart.WorkbookStylesPart.Stylesheet.Fills,
           document.WorkbookPart.WorkbookStylesPart.Stylesheet.Fonts, document.WorkbookPart.WorkbookStylesPart.Stylesheet.Borders);
            AddCellFormatPlan(document.WorkbookPart.WorkbookStylesPart.Stylesheet.CellFormats, document.WorkbookPart.WorkbookStylesPart.Stylesheet.Fills,
              document.WorkbookPart.WorkbookStylesPart.Stylesheet.Fonts, document.WorkbookPart.WorkbookStylesPart.Stylesheet.Borders
            );
            cellDat.StyleIndex = (UInt32)(document.WorkbookPart.WorkbookStylesPart.Stylesheet.CellFormats.Elements<CellFormat>().Count() - 1);  
        }


        //получение значения ячейки
       public static string GetCellValue(string fileName,
      string sheetName,
      string addressName)
        {
            string value = null;
            using (SpreadsheetDocument document =
                SpreadsheetDocument.Open(fileName, false))
            {
                WorkbookPart wbPart = document.WorkbookPart;
                Sheet theSheet = wbPart.Workbook.Descendants<Sheet>().
                  Where(s => s.Name == sheetName).FirstOrDefault();
                if (theSheet == null)
                {
                    throw new ArgumentException("sheetName");
                }
                WorksheetPart wsPart =
                    (WorksheetPart)(wbPart.GetPartById(theSheet.Id));
                Cell theCell = wsPart.Worksheet.Descendants<Cell>().
                  Where(c => c.CellReference == addressName).FirstOrDefault();
                if (theCell != null)
                {
                    value = theCell.InnerText;
                    if (theCell.DataType != null)
                    {
                        switch (theCell.DataType.Value)
                        {
                            case CellValues.SharedString:
                                var stringTable =
                                    wbPart.GetPartsOfType<SharedStringTablePart>()
                                    .FirstOrDefault();
                                if (stringTable != null)
                                {
                                    value =
                                        stringTable.SharedStringTable
                                        .ElementAt(int.Parse(value)).InnerText;
                                }
                                break;
                            case CellValues.Boolean:
                                switch (value)
                                {
                                    case "0":
                                        value = "FALSE";
                                        break;
                                    default:
                                        value = "TRUE";
                                        break;
                                }
                          break;
                        }}}}
            return value;
        }

        //
        //---------- ПОЛНЫЙ ОТЧЕТ (СДЕЛЬЩИКИ)
        //GetFullDocResExcel
        public byte[] GetFullDocResExcel(DateTime startDate, DateTime endDate, string pathTemplate, int? empId)
        {
            byte[] byteArray = File.ReadAllBytes(pathTemplate);
            using (MemoryStream stream = new MemoryStream())
            {
                stream.Write(byteArray, 0, (int)byteArray.Length);
                using (var document = SpreadsheetDocument.Open(stream, true))
                {
                    string SheetName;
                    SheetName = "Начисление ЗП";
                    // 1 лист отчета 
                    Sheet sheet;
                    try
                    {
                        sheet = document.WorkbookPart.Workbook.GetFirstChild<Sheets>().Elements<Sheet>().SingleOrDefault(s => s.Name == SheetName);
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(String.Format("Возможно в документе существует два листа с названием \"{0}\"!\n", SheetName), ex);
                    }

                    if (sheet == null)
                    {
                        throw new Exception(String.Format("В шаблоне не найден \"{0}\"!\n", SheetName));
                    }
                    var worksheetPart = (WorksheetPart)document.WorkbookPart.GetPartById(sheet.Id.Value);
                    var sheetData = worksheetPart.Worksheet.GetFirstChild<SheetData>();
                    if (empId == 0)
                    {
                   respList = db.Database.SqlQuery<ExportDocumentsList>(
            string.Format(@"SELECT DISTINCT sum as sum, sl.short_name as name, date as date, p.name as fiel_name,  f1.name as fiel_code, t.name as task_code
            from Salary_details s,Salaries_employees sl, Fields_to_plants f, Plants p,Type_tasks t,
            Fields f1, Type_tasks t1 where sl.salemp_id=s.salemp_salemp_id and s.plant_plant_id=f.fielplan_id and p.plant_id=f.plant_plant_id
            and f1.fiel_id=f.fiel_fiel_id and t.tptas_id=s.tptas_tptas_id and  sum is not null and convert(varchar(10),date,120) >= '" + startDate.ToString("yyyy-MM-dd") + "' " + 
            "and convert(varchar(10),date,120) <= '" + endDate.ToString("yyyy-MM-dd") + "'")).ToList();
                    }
                    else
                    {
                 respList = db.Database.SqlQuery<ExportDocumentsList>(
               string.Format(@"SELECT DISTINCT sum as sum, sl.short_name as name, date as date, p.name as fiel_name,  f1.name as fiel_code, t.name as task_code
            from Salary_details s,Salaries_employees sl, Fields_to_plants f, Plants p,Type_tasks t,
            Fields f1, Type_tasks t1 where sl.salemp_id=s.salemp_salemp_id and s.plant_plant_id=f.fielplan_id and p.plant_id=f.plant_plant_id
            and f1.fiel_id=f.fiel_fiel_id and t.tptas_id=s.tptas_tptas_id and  sum is not null and convert(varchar(10),date,120) >= '" + startDate.ToString("yyyy-MM-dd") + "' " +
           "and convert(varchar(10),date,120) <= '" + endDate.ToString("yyyy-MM-dd") + "'" + " and  sl.salemp_id=" + empId)).ToList();
                    }
                    Cell cellDat = GetCell(worksheetPart.Worksheet, "B", 1);
                    cellDat.CellValue = new CellValue("C:  " + startDate.ToString("dd.MM.yyyy"));
                    cellDat.DataType = new EnumValue<CellValues>(CellValues.String);

                    cellDat = GetCell(worksheetPart.Worksheet, "C", 1);
                    cellDat.CellValue = new CellValue("По:  " + endDate.ToString("dd.MM.yyyy"));
                    cellDat.DataType = new EnumValue<CellValues>(CellValues.String);


                    for (int j = 0; j < respList.Count; j++)
                    {
                        ListOfPlantId.Add(respList[j].Id);
                    }

                    uint i = 3;
                    OldShtrId = new List<int>();
                    if (respList.Any())
                    {
                        OldShtrId.Add(respList[0].Id);
                        respList[0].Id1 = i - 2;
                        Row refRowC = sheetData.Elements<Row>().Where(r => 3 == r.RowIndex).First();
                        SetCellsValueForFullDocList(respList[0], refRowC, i);
                        respList.Remove(respList[0]);
                        i++;
                    }
                    foreach (var item in respList)
                    {
                        OldShtrId.Add(item.Id);
                        item.Id1 = i - 2;
                        Row refRowC = sheetData.Elements<Row>().Where(r => 3 == r.RowIndex).First();
                        var row = new Row();
                        row = (Row)refRowC.Clone();
                        row.RowIndex = new UInt32Value(i);

                        foreach (var cell in row.Elements<Cell>())
                        {
                            cell.CellReference = new StringValue(cell.CellReference.Value.Replace(refRowC.RowIndex.Value.ToString(), row.RowIndex.ToString()));
                        }
                        SetCellsValueForFullDocList(item, row, i);
                        sheetData.Append(row);
                        i++;
                    }
                    worksheetPart.Worksheet.Save();
                    document.Close();
                }
                return stream.ToArray();
            }

        }

        //--функция списания ТМЦ
        public byte[] GetWriteOffExportExcel(DateTime startDate,DateTime endDate,string pathTemplate, int? employeerId, int? fieldId, int? plantId, int? materialId)
        {
            byte[] byteArray = File.ReadAllBytes(pathTemplate);
            using (MemoryStream stream = new MemoryStream())
            {
                stream.Write(byteArray, 0, (int)byteArray.Length);
                using (var document = SpreadsheetDocument.Open(stream, true))
                {
                    string SheetName;
                    SheetName = "Списание ТМЦ";
                    // 1 лист отчета 
                    Sheet sheet;
                    try
                    {
                        sheet = document.WorkbookPart.Workbook.GetFirstChild<Sheets>().Elements<Sheet>().SingleOrDefault(s => s.Name == SheetName);
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(String.Format("Возможно в документе существует два листа с названием \"{0}\"!\n", SheetName), ex);
                    }

                    if (sheet == null)
                    {
                        throw new Exception(String.Format("В шаблоне не найден \"{0}\"!\n", SheetName));
                    }
                    var worksheetPart = (WorksheetPart)document.WorkbookPart.GetPartById(sheet.Id.Value);
                    var sheetData = worksheetPart.Worksheet.GetFirstChild<SheetData>();
                    //выбраны все поля
                    var startYear = 0; var endYear = 0;
                    var id = db.Fields_to_plants.Where(t => t.fielplan_id == plantId).Select(t => t.plant_plant_id).FirstOrDefault();
                    if (plantId != -1)
                    {
                        startYear = db.Fields_to_plants.Where(t => t.fielplan_id == plantId).Select(t => t.date_ingathering.Year).FirstOrDefault();
                        endYear = db.Fields_to_plants.Where(t => t.fielplan_id == plantId).Select(t => t.date_ingathering.Year).FirstOrDefault();
                    }
                    else {
                        startYear = startDate.Year;
                        endYear = endDate.Year;
                    }

                    var   listActsSzr = db.TMC_Records.Where(v => v.emp_to_id == 0 && v.emp_from_id != 0 && (employeerId != -1 ? v.TMC_Acts.emp_utv_id == employeerId : true)
                    && (materialId != -1 ? v.Materials.mat_type_id == materialId : true)
                    && (fieldId != -1 ? v.Fields_to_plants.fiel_fiel_id == fieldId : true) && (((plantId != -1 && fieldId != -1 && plantId != null) ? v.Fields_to_plants.fielplan_id == plantId : true))
                    && (((fieldId == -1 && plantId != -1 && plantId != null) ? v.Fields_to_plants.plant_plant_id == id : true))
                    && ((v.cont_cont_id != -1 ?  v.Fields_to_plants.date_ingathering.Year >= startYear : true )   
                    &&  (v.cont_cont_id != -1 ? v.Fields_to_plants.date_ingathering.Year <= endYear : true))
                    && (DbFunctions.TruncateTime(v.TMC_Acts.act_date) >= DbFunctions.TruncateTime(startDate)
                    && DbFunctions.TruncateTime(v.TMC_Acts.act_date) <= DbFunctions.TruncateTime(endDate)))
                    .Select(v =>
                          new GetActsSzr
                          {
                              Id = v.TMC_Acts.tma_id,
                              RecipientName = v.Employees1.short_name,
                              Number = v.TMC_Acts.act_num,
                              DateT = v.TMC_Acts.act_date.Value,
                              MaterialName = v.Materials.name,
                              PlantName = v.cont_cont_id != -1 ? (  v.Fields_to_plants != null ? v.Fields_to_plants.Plants.name : null) : "Культура",
                              FieldName = v.cont_cont_id != -1 ? (  v.Fields_to_plants != null ? v.Fields_to_plants.Fields.name : null)
                              : db.Fields.Where( t=> t.fiel_id == v.fielplan_fielplan_id).Select(t => t.name).FirstOrDefault(),
                              Area = v.area,
                              Count1 = v.count,
                              ConsumptionRate = v.area != 0 ? v.count/v.area : 0,
                              Price1 = v.price,
                              Cost1 = v.count*v.price,
                          }).ToList();

                    for (int j = 0; j < listActsSzr.Count; j++)
                    {
                        ListOfPlantId.Add(listActsSzr[j].Id);
                    }
                 
                    uint i = 3;
                    OldShtrId = new List<int>();
                    if (listActsSzr.Any())
                    {
                        OldShtrId.Add(listActsSzr[0].Id);
                        listActsSzr[0].Id1 = i - 2;
                        Row refRowC = sheetData.Elements<Row>().Where(r => 3 == r.RowIndex).First();
                        SetCellsValueForWriteOffList(listActsSzr[0], refRowC, i);
                        listActsSzr.Remove(listActsSzr[0]);
                        i++;
                    }
                    foreach (var item in listActsSzr)
                    {
                        OldShtrId.Add(item.Id);
                        item.Id1 = i - 2;
                        Row refRowC = sheetData.Elements<Row>().Where(r => 3 == r.RowIndex).First();
                        var row = new Row();
                        row = (Row)refRowC.Clone();
                        row.RowIndex = new UInt32Value(i);

                        foreach (var cell in row.Elements<Cell>())
                        {
                            cell.CellReference = new StringValue(cell.CellReference.Value.Replace(refRowC.RowIndex.Value.ToString(), row.RowIndex.ToString()));
                        }
                        SetCellsValueForWriteOffList(item, row, i);
                        sheetData.Append(row);
                        i++;
                    }
                    worksheetPart.Worksheet.Save();
                    document.Close();
                }
                return stream.ToArray();
            }
           
        }

        
        public byte[] GetExpensesExportExcel(string pathTemplate, int year)
        {
            byte[] byteArray = File.ReadAllBytes(pathTemplate);
            using (MemoryStream stream = new MemoryStream())
            {
                stream.Write(byteArray, 0, (int)byteArray.Length);
                using (var document = SpreadsheetDocument.Open(stream, true))
                {
                    
                    string SheetName = "Фактические затраты";
                    // 1 лист отчета 
                    Sheet sheet;
                    try
                    {
                        sheet = document.WorkbookPart.Workbook.GetFirstChild<Sheets>().Elements<Sheet>().SingleOrDefault(s => s.Name == SheetName);
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(String.Format("Возможно в документе существует два листа с названием \"{0}\"!\n", SheetName), ex);
                    }

                    if (sheet == null)
                    {
                        throw new Exception(String.Format("В шаблоне не найден \"{0}\"!\n", SheetName));
                    }


                    var worksheetPart = (WorksheetPart)document.WorkbookPart.GetPartById(sheet.Id.Value);
                    var sheetData = worksheetPart.Worksheet.GetFirstChild<SheetData>();

                    var list = GetExpensesList(year);


                    Cell cellDat1 = GetCell(worksheetPart.Worksheet, "C", 2);
                    cellDat1.CellValue = new CellValue(year.ToString());
                    cellDat1.DataType = new EnumValue<CellValues>(CellValues.String);

                    Cell cellDat2 = GetCell(worksheetPart.Worksheet, "D", 7);
                    cellDat2.CellValue = new CellValue(list.Sum(l => l.minud_fact).ToString());
                    cellDat2.DataType = new EnumValue<CellValues>(CellValues.String);

                    Cell cellDat3 = GetCell(worksheetPart.Worksheet, "E", 7);
                    cellDat3.CellValue = new CellValue(list.Sum(l => l.minud_time).ToString());
                    cellDat3.DataType = new EnumValue<CellValues>(CellValues.String);

                    Cell cellDat4 = GetCell(worksheetPart.Worksheet, "F", 7);
                    cellDat4.CellValue = new CellValue(list.Sum(l => l.szr_fact).ToString());
                    cellDat4.DataType = new EnumValue<CellValues>(CellValues.String);

                    Cell cellDat5 = GetCell(worksheetPart.Worksheet, "G", 7);
                    cellDat5.CellValue = new CellValue(list.Sum(l => l.szr_time).ToString());
                    cellDat5.DataType = new EnumValue<CellValues>(CellValues.String);

                    Cell cellDat6 = GetCell(worksheetPart.Worksheet, "H", 7);
                    cellDat6.CellValue = new CellValue(list.Sum(l => l.posevmat_fact).ToString());
                    cellDat6.DataType = new EnumValue<CellValues>(CellValues.String);

                    Cell cellDat7 = GetCell(worksheetPart.Worksheet, "I", 7);
                    cellDat7.CellValue = new CellValue(list.Sum(l => l.vyvozprod_fact).ToString());
                    cellDat7.DataType = new EnumValue<CellValues>(CellValues.String);

                    Cell cellDat8 = GetCell(worksheetPart.Worksheet, "J", 7);
                    cellDat8.CellValue = new CellValue(list.Sum(l => l.razx_top).ToString());
                    cellDat8.DataType = new EnumValue<CellValues>(CellValues.String);

                    Cell cellDat9 = GetCell(worksheetPart.Worksheet, "K", 7);
                    cellDat9.CellValue = new CellValue(list.Sum(l => l.sx_tex).ToString());
                    cellDat9.DataType = new EnumValue<CellValues>(CellValues.String);

                     Cell cellDat10 = GetCell(worksheetPart.Worksheet, "L", 7);
                    cellDat10.CellValue = new CellValue(list.Sum(l => l.gruz_tex).ToString());
                    cellDat10.DataType = new EnumValue<CellValues>(CellValues.String);

                     Cell cellDat11 = GetCell(worksheetPart.Worksheet, "M", 7);
                    cellDat11.CellValue = new CellValue(list.Sum(l => l.salary_main).ToString());
                    cellDat11.DataType = new EnumValue<CellValues>(CellValues.String);

                     Cell cellDat12 = GetCell(worksheetPart.Worksheet, "N", 7);
                    cellDat12.CellValue = new CellValue(list.Sum(l => l.salary_dop).ToString());
                    cellDat12.DataType = new EnumValue<CellValues>(CellValues.String);

                    uint i =8;
                    if (list.Any())
                    {
                        list[0].Id = i - 7;
                        Row refRowC = sheetData.Elements<Row>().Where(r => 8 == r.RowIndex).First();
                        SetCellsValueForExpensessList(list[0], refRowC, i);
                        list.Remove(list[0]);
                        i++;
                    }
                    foreach (var item in list)
                    {
                        item.Id = i - 7;
                        Row refRowC = sheetData.Elements<Row>().Where(r => 8 == r.RowIndex).First();
                        var row = new Row();
                        row = (Row)refRowC.Clone();
                        row.RowIndex = new UInt32Value(i);

                        foreach (var cell in row.Elements<Cell>())
                        {
                            cell.CellReference = new StringValue(cell.CellReference.Value.Replace(refRowC.RowIndex.Value.ToString(), row.RowIndex.ToString()));
                        }
                        SetCellsValueForExpensessList(item, row, i);
                        sheetData.Append(row);
                        i++;
                    }
            
                }
                return stream.ToArray();
            }
        }
        // code color
        public Cell get_CellByRowAndColumn(WorksheetPart worksheetPart, int rowIndex, int columnIndex)
        {
            Cell cell = null;
            Worksheet worksheet = worksheetPart.Worksheet;
            SheetData sheetData = worksheet.GetFirstChild<SheetData>();
            Row row = sheetData.Elements<Row>().ElementAt(rowIndex - 1);
            cell = row.Elements<Cell>().ElementAt(columnIndex - 1);

            return cell;
        }
        static Row getRow(uint rowIndex, WorksheetPart worksheetPart)
        {
            Worksheet worksheet = worksheetPart.Worksheet;
            SheetData sheetData = worksheet.GetFirstChild<SheetData>();
            Row row;
            if (sheetData.Elements<Row>().Where(r => r.RowIndex == rowIndex).Count() != 0)
            {
                row = sheetData.Elements<Row>().Where(r => r.RowIndex == rowIndex).First();
            }
            else
            {
                row = new Row() { RowIndex = rowIndex };
                sheetData.Append(row);
            }
            return row;
        }
        static void AddbackgroundFormat(SpreadsheetDocument document, Cell c)
        {
            Fills fs = AddFill(document.WorkbookPart.WorkbookStylesPart.Stylesheet.Fills);
            AddCellFormat(document.WorkbookPart.WorkbookStylesPart.Stylesheet.CellFormats, document.WorkbookPart.WorkbookStylesPart.Stylesheet.Fills);
            c.StyleIndex = (UInt32)(document.WorkbookPart.WorkbookStylesPart.Stylesheet.CellFormats.Elements<CellFormat>().Count() - 1);
        }
        static Fills AddFill(Fills fills1)
        {
            Fill fill1 = new Fill();

            PatternFill patternFill1 = new PatternFill() { PatternType = PatternValues.Solid };
            ForegroundColor foregroundColor1 = new ForegroundColor() { Rgb = "FFFF0000" };  //FFFFFF00
            BackgroundColor backgroundColor1 = new BackgroundColor() { Indexed = (UInt32Value)64U };

            patternFill1.Append(foregroundColor1);
            patternFill1.Append(backgroundColor1);

            fill1.Append(patternFill1);
            fills1.Append(fill1);
            return fills1;
        }
        static void AddCellFormat(CellFormats cf, Fills fs)
        {
            CellFormat cellFormat2 = new CellFormat() { NumberFormatId = 0, FontId = 0, FillId = (UInt32)(fs.Elements<Fill>().Count() - 1), BorderId = 0, FormatId = 0, ApplyFill = true };
            cf.Append(cellFormat2);
        }
        //настройки шапки
        static Fills AddFormatPlan(Fills fills1, Fonts fonts, Borders forders)
        {

       

            Font font1 = new Font(
                new Bold(),
                new FontSize() { Val = 12.5 },
                new Color() { Rgb = new HexBinaryValue() { Value = "000000" } },
                new FontName() { Val = "Times New Roman" }
             );
             fonts.Append(font1);

             Border border1 = new Border(
                new LeftBorder() { Style = BorderStyleValues.Medium },
                    new RightBorder() { Style = BorderStyleValues.Medium },
                    new TopBorder() { Style = BorderStyleValues.Medium },
                    new BottomBorder() { Style = BorderStyleValues.Medium },
                    new DiagonalBorder() { Style = BorderStyleValues.Medium }
                );
             forders.Append(border1);

            
    
            


            Fill fill1 = new Fill();
            PatternFill patternFill1 = new PatternFill() { PatternType = PatternValues.Solid };
            ForegroundColor foregroundColor1 = new ForegroundColor() { Rgb = "D9D9D9" };  //#d9d9d9
            BackgroundColor backgroundColor1 = new BackgroundColor() { Indexed = (UInt32Value)64U };

            patternFill1.Append(foregroundColor1);
            patternFill1.Append(backgroundColor1);

            fill1.Append(patternFill1);
            fills1.Append(fill1);
            return fills1;
        }
        static void AddCellFormatPlan(CellFormats cf, Fills fs, Fonts f, Borders b)
        {
            CellFormat cellFormat2 = new CellFormat(new Alignment() { WrapText = true, Horizontal = HorizontalAlignmentValues.Center, Vertical = VerticalAlignmentValues.Center })
            {
                FillId = (UInt32)(fs.Elements<Fill>().Count() - 1),
              FontId = Convert.ToUInt32(f.Elements<Font>().Count() - 1),
              BorderId = Convert.ToUInt32(b.Elements<Border>().Count() - 1),
          NumberFormatId = (UInt32Value)2U,
                FormatId = (UInt32Value)0U,
                                                       ApplyFill = true,
                                                       ApplyFont = true,
                                                       ApplyBorder = true,
                                                       ApplyNumberFormat =true,};
            cf.Append(cellFormat2);
        }
       
        public List<ExpensesRequestDto> GetCostsByCulture(int year, int? plant_id)
        {
            //топливо и ЗП из путевых листов
            var list = (from ftp in db.Fields_to_plants
                        where ftp.date_ingathering.Year == year && ftp.Fields.type == 1 && (plant_id.HasValue ? ftp.plant_plant_id == plant_id : true)

                        join task1 in db.Tasks on ftp.fielplan_id equals task1.plant_plant_id into leftTask1
                        from t1 in leftTask1.DefaultIfEmpty()

                        group new
                            {
                                consumption = t1.fuel_cost ?? 0,
                                total_salary = t1.total_salary ?? 0,
                            }
                            by new
                            {
                                name = ftp.Plants.name,
                                id = ftp.plant_plant_id,
                            } into g
                            select new
                            {
                                id= g.Key.id,
                                name = g.Key.name,
                                consumption = (float)g.Sum(p => p.consumption),
                                total_salary = (float?)g.Sum(p => p.total_salary) 
                            });
            //сдельная ЗП
            var dopSalary = (from ftp in db.Fields_to_plants
                             where ftp.date_ingathering.Year == year && ftp.Fields.type == 1 && (plant_id.HasValue ? ftp.plant_plant_id == plant_id : true)

                             join sal_detail in db.Salary_details on ftp.fielplan_id equals sal_detail.plant_plant_id into leftDetail
                             from t2 in leftDetail.DefaultIfEmpty()
                             group new
                             {
                                 sum = t2.sum ?? 0
                             }
                                 by new
                                 {
                                     name = ftp.Plants.name,
                                     plant_plant_id = ftp.plant_plant_id,
                                 } into g
                             select new
                             {
                                 plant_plant_id = g.Key.plant_plant_id,
                                 dopSalary = g.Sum(p => p.sum)
                             });
            //услуги
            var totalService = (from s in db.Service_records
                             where s.date.Value.Year==year && (plant_id.HasValue ? s.plant_plant_id == plant_id : true)
                             group new
                             {
                                 sum = s.cost ?? 0
                             }
                                 by new
                                 {
                                     plant_plant_id = s.plant_plant_id,
                                 } into g
                             select new
                             {
                                 plant_plant_id = g.Key.plant_plant_id,
                                 service = (double)g.Sum(p => p.sum)
                             });
            //сзр, удобрения, семена, прочие материалы
            var acts = (from ftp in db.Fields_to_plants
                        where ftp.date_ingathering.Year == year && ftp.Fields.type == 1 && (plant_id.HasValue ? ftp.plant_plant_id == plant_id : true)
                        join act in db.TMC_Records on ftp.fielplan_id equals act.fielplan_fielplan_id into leftAct1
                        from a in leftAct1.DefaultIfEmpty()
                        where a.emp_from_id != 0 && a.emp_to_id == 0 
                        group new { ftp, a } by new { ftp.plant_plant_id, ftp.Plants.name } into g
                        select new
                        {
                            name = g.Key.name,
                            id = g.Key.plant_plant_id,
                            Values = (
                                from m in g.Where(ga => ga.a != null)
                                group new
                                {
                                    cost = (m.a.count * m.a.price),
                                } by new
                                {
                                    m.a.Materials.mat_type_id
                                } into g2
                                select new
                                {
                                    type_id = g2.Key.mat_type_id,
                                    cost = g2.Sum(mat => mat.cost)
                                }

                            ).ToList()
                        }
                        );
            //валовый сбор
            var dopGross = (from gh in db.Gross_harvest
                            where gh.Fields_to_plants.date_ingathering.Year == year && (plant_id.HasValue ? gh.Fields_to_plants.plant_plant_id == plant_id : true)
                            group gh by new {gh.Fields_to_plants.plant_plant_id} into g
                            select new {
                                plant_plant_id = g.Key.plant_plant_id,
                                gross_harvest = g.Sum(g1 => g1.count / 1000)
                            });
            //ПЛАН
            var dopPlan = (from tc in db.TC_param_value.Where(x => x.id_tc_param == 4 && x.TC.year == year)

                           join tc1 in db.TC_param_value.Where(x => x.id_tc_param == 1 && x.TC.year == year) on tc.id_tc equals tc1.id_tc
                              into tcLj1
                           from a in tcLj1.DefaultIfEmpty()

                           join tc2 in db.TC_param_value.Where(x => x.id_tc_param == 3 && x.TC.year == year) on tc.id_tc equals tc2.id_tc
                              into tcLj2
                           from b in tcLj2.DefaultIfEmpty()
                           //зп
                           join tc3 in db.TC_param_value.Where(x => x.id_tc_param == 10 && x.TC.year == year) on tc.id_tc equals tc3.id_tc
                              into tcLj3
                           from d in tcLj3.DefaultIfEmpty()
                            //топливо
                           join tc4 in db.TC_param_value.Where(x => x.id_tc_param == 16 && x.TC.year == year) on tc.id_tc equals tc4.id_tc
                             into tcLj4
                           from d1 in tcLj4.DefaultIfEmpty()

                           join tc5 in db.TC_param_value.Where(x => x.id_tc_param == 22 && x.TC.year == year) on tc.id_tc equals tc5.id_tc
                             into tcLj5
                           from d2 in tcLj5.DefaultIfEmpty()
                           //szr
                           join tc6 in db.TC_param_value.Where(x => x.id_tc_param == 25 && x.TC.year == year) on tc.id_tc equals tc6.id_tc
                            into tcLj6
                           from d3 in tcLj6.DefaultIfEmpty()
                           //semena
                           join tc7 in db.TC_param_value.Where(x => x.id_tc_param == 19 && x.TC.year == year) on tc.id_tc equals tc7.id_tc
                            into tcLj7
                           from d4 in tcLj7.DefaultIfEmpty()
                           //dop
                           join tc8 in db.TC_param_value.Where(x => x.id_tc_param == 61 && x.TC.year == year) on tc.id_tc equals tc8.id_tc
                            into tcLj8
                           from d5 in tcLj8.DefaultIfEmpty()

                           join tc9 in db.TC_param_value.Where(x => x.id_tc_param == 28 && x.TC.year == year) on tc.id_tc equals tc9.id_tc
                           into tcLj9
                           from d6 in tcLj9.DefaultIfEmpty()


                           select new {
                                plant_plant_id =  tc.TC.plant_plant_id,
                                total_plan = tc.value,
                                area_plan = a.value,
                                gross_harvest_plant = b.value,
                                total_salary = d.value,
                                fuel = d1.value,
                                minud = d2.value,
                                szr = d3.value,
                                semena = d4.value,
                                dop_mat = d5.value,
                                service = d6.value,
                           });
           //площадь
            var dopArea = (from ftp in db.Fields_to_plants
                           where ftp.date_ingathering.Year == year
                           group ftp.area_plant by new { ftp.plant_plant_id } into g
                           select new
                           {
                               plant_plant_id = g.Key.plant_plant_id,
                               area = (double)g.Sum()
                           });

            var result = (from l in list
                          join act in acts on l.id equals act.id
                            into actLj from a in actLj.DefaultIfEmpty()
                          join dG in dopGross on l.id equals dG.plant_plant_id
                              into jGross from gross in jGross.DefaultIfEmpty()
                          join dP in dopPlan on l.id equals dP.plant_plant_id
                              into jPlant from plan in jPlant.DefaultIfEmpty()
                          join dA in dopArea on l.id equals dA.plant_plant_id
                              into jArea from area in jArea.DefaultIfEmpty()
                          join dS in dopSalary on l.id equals dS.plant_plant_id
                          into jSalary
                          from salary in jSalary.DefaultIfEmpty()

                          join dV in totalService on l.id equals dV.plant_plant_id
                      into jService
                          from service in jService.DefaultIfEmpty()

                          let a_type1 = a.Values.Where(t => t.type_id == 1).FirstOrDefault()
                          let a_type2 = a.Values.Where(t => t.type_id == 2).FirstOrDefault()
                          let a_type4 = a.Values.Where(t => t.type_id == 4).FirstOrDefault()
                          let a_type5 = a.Values.Where(t => t.type_id == 5).FirstOrDefault()

                          let a_total_fact = ((l.total_salary == null ? 0 : l.total_salary) + l.consumption + (a_type1 == null ? 0 : a_type1.cost) + (a_type2 == null ? 0 : a_type2.cost) +
                          (a_type4 == null ? 0 : a_type4.cost) + (salary.dopSalary == null ? 0 : salary.dopSalary) + (service.service == null ? 0 : service.service) + (a_type5 == null ? 0 : a_type5.cost))
                          let a_cost_ga_fact = area.area != 0 ? Math.Round((double)a_total_fact / area.area, 2) : 0
                          let a_cost_ga_plan = (plan.total_plan.HasValue && plan.area_plan.HasValue && plan.total_plan != 0 && plan.area_plan != 0) ? plan.total_plan / plan.area_plan : 0
                          let a_cost_t_fact = gross.gross_harvest.HasValue && gross.gross_harvest != 0 ? (float?)a_total_fact / (float?)gross.gross_harvest : 0
                          let a_cost_t_plan = (plan.total_plan.HasValue && plan.gross_harvest_plant.HasValue && plan.total_plan !=0 &&
                          plan.gross_harvest_plant != 0) ? plan.total_plan / plan.gross_harvest_plant : 0

                              select new ExpensesRequestDto
                              {
                                  plant_id = l.id,
                                  plant_name = l.name,
                                  salary_total = (l.total_salary == null ? 0: l.total_salary) + (salary.dopSalary==null ? 0: salary.dopSalary),
                                  razx_top = l.consumption,
                                  szr_fact = a_type1 == null ? 0 : a_type1.cost,
                                  minud_fact = a_type2 == null ? 0 : a_type2.cost,
                                  posevmat_fact = a_type4 == null ? 0 : a_type4.cost,
                                  dop_material_fact = a_type5 == null ? 0 : a_type5.cost,
                                  service_total = (service.service == null ? 0 : service.service),
                                  total_fact = a_total_fact,
                                  nzp = 0,
                                  total_plan = (float?)(plan.total_plan.HasValue ? plan.total_plan.Value : 0),
                                  total_delta = (float?)((plan.total_plan.HasValue ? plan.total_plan.Value : 0) - (float?)a_total_fact),
                                  area = area.area,
                                  gross_harvest = gross.gross_harvest.HasValue ? (float?)gross.gross_harvest : 0,
                                  cost_ga_fact = a_cost_ga_fact,
                                  cost_ga_plan = a_cost_ga_plan.HasValue ? Math.Round(a_cost_ga_plan.Value, 2) : 0,
                                  cost_ga_delta = Math.Round((a_cost_ga_plan.HasValue ? a_cost_ga_plan.Value : 0) - (float)a_cost_ga_fact, 2),
                                  cost_t_fact = a_cost_t_fact.HasValue ? Math.Round(a_cost_t_fact.Value, 2): 0,
                                  cost_t_plan = a_cost_t_plan.HasValue ? Math.Round(a_cost_t_plan.Value, 2) : 0,
                                  cost_t_delta = Math.Round(a_cost_t_plan.Value - a_cost_t_fact.Value, 2),
                                  plan_salary_total = plan.total_salary ?? 0,
                                  plan_razx_top = plan.fuel ?? 0,
                                  plan_minud = (float?)plan.minud ?? 0,
                                  plan_posevmat = (float?)plan.semena ?? 0 ,
                                  plan_szr = (float?)plan.szr ?? 0,
                                  plan_dop_material = (float?)plan.dop_mat ?? 0,
                                  plan_service_total = plan.service ?? 0,
                              }).OrderBy(p => p.plant_name).ToList();
            ////////----------------
            ///////////////////////////////севооборот НЗП
            var sevoborot = (from ftp in db.Fields_to_plants
                             where ftp.date_ingathering.Year == year && ftp.Fields.type == 1 && ftp.plant_plant_id != 36 && ftp.area_plant != 0
                             let sum_area = (float?)db.Fields_to_plants.Where(f => f.fiel_fiel_id == ftp.fiel_fiel_id && f.date_ingathering.Year == year &&
                             f.area_plant != 0 && ftp.plant_plant_id != 36).Select(f => f.area_plant).Sum()

                             let total_salary = (float?)db.Tasks.Where(t => t.plant_plant_id == t.Fields_to_plants.fielplan_id
                             && t.Fields_to_plants.date_ingathering.Year == year && t.Fields_to_plants.fiel_fiel_id == ftp.fiel_fiel_id && t.Fields_to_plants.plant_plant_id == 36).Select(t => t.total_salary).Sum()

                             let consumption = (float?)db.Tasks.Where(t => t.plant_plant_id == t.Fields_to_plants.fielplan_id
                             && t.Fields_to_plants.date_ingathering.Year == year && t.Fields_to_plants.fiel_fiel_id == ftp.fiel_fiel_id && t.Fields_to_plants.plant_plant_id == 36).Select(t => t.fuel_cost).Sum()

                             let dop_salary = (float?)db.Salary_details.Where(t => t.plant_plant_id == t.Fields_to_plants.fielplan_id
                             && t.Fields_to_plants.date_ingathering.Year == year && t.Fields_to_plants.fiel_fiel_id == ftp.fiel_fiel_id && t.Fields_to_plants.plant_plant_id == 36).Select(t => t.sum).Sum()

                             let szr = (float?)db.TMC_Records.Where(t => t.fielplan_fielplan_id == t.Fields_to_plants.fielplan_id && t.emp_to_id == 0 && t.emp_from_id != 0
                             && t.Materials.mat_id == t.mat_mat_id && t.Materials.mat_type_id == 1
                             && t.Fields_to_plants.date_ingathering.Year == year && t.Fields_to_plants.fiel_fiel_id == ftp.fiel_fiel_id && t.Fields_to_plants.plant_plant_id == 36).Select(t => t.count * t.price).Sum()

                             let chemicalFertilizers = (float?)db.TMC_Records.Where(t => t.fielplan_fielplan_id == t.Fields_to_plants.fielplan_id && t.emp_to_id == 0 && t.emp_from_id != 0
                             && t.Materials.mat_id == t.mat_mat_id && t.Materials.mat_type_id == 2
                             && t.Fields_to_plants.date_ingathering.Year == year && t.Fields_to_plants.fiel_fiel_id == ftp.fiel_fiel_id && t.Fields_to_plants.plant_plant_id == 36).Select(t => t.count * t.price).Sum()

                             let seed = (float?)db.TMC_Records.Where(t => t.fielplan_fielplan_id == t.Fields_to_plants.fielplan_id && t.emp_to_id == 0 && t.emp_from_id != 0
                             && t.Materials.mat_id == t.mat_mat_id && t.Materials.mat_type_id == 4
                             && t.Fields_to_plants.date_ingathering.Year == year && t.Fields_to_plants.fiel_fiel_id == ftp.fiel_fiel_id && t.Fields_to_plants.plant_plant_id == 36).Select(t => t.count * t.price).Sum()

                             let dopmaterial = (float?)db.TMC_Records.Where(t => t.fielplan_fielplan_id == t.Fields_to_plants.fielplan_id && t.emp_to_id == 0 && t.emp_from_id != 0
                          && t.Materials.mat_id == t.mat_mat_id && t.Materials.mat_type_id == 5
                          && t.Fields_to_plants.date_ingathering.Year == year && t.Fields_to_plants.fiel_fiel_id == ftp.fiel_fiel_id && t.Fields_to_plants.plant_plant_id == 36).Select(t => t.count * t.price).Sum()

                             let deduction = ((dop_salary ?? 0) + (total_salary ?? 0)) * 0.321
                             group new
                             {
                                 plant_id = ftp.plant_plant_id,
                                 field_id = ftp.fiel_fiel_id,
                                 sum_area = sum_area ?? 1,
                                 total_salary = total_salary ?? 0,
                                 consumption = consumption ?? 0,
                                 dop_salary = dop_salary ?? 0,
                                 deduction = deduction,
                                 szr = szr ?? 0,
                                 chemicalFertilizers = chemicalFertilizers ?? 0,
                                 seed = seed ?? 0,
                                 dopmaterial = dopmaterial ?? 0

                             }
                                 by new
                                 {
                                     field_id = ftp.fiel_fiel_id,
                                     plant_id = ftp.plant_plant_id,
                                     area_plant = ftp.area_plant,
                                     sum_area = sum_area,
                                     total_salary = total_salary,
                                     consumption = consumption,
                                     dop_salary = dop_salary,
                                     szr = szr,
                                     chemicalFertilizers = chemicalFertilizers,
                                     seed = seed,
                                     dopmaterial = dopmaterial,
                                     deduction = deduction,
                                  
                                 } into g
                             select new
                             {
                                 consumption = (float?)g.Key.consumption * ((float)g.Key.area_plant / (float)g.Key.sum_area) ?? 0,
                                 total_salary = (float?)g.Key.total_salary * ((float)g.Key.area_plant / (float)g.Key.sum_area) ?? 0,
                                 dop_salary = (float?)g.Key.dop_salary * ((float)g.Key.area_plant / (float)g.Key.sum_area) ?? 0,
                                 szr = g.Key.szr * ((float?)g.Key.area_plant / (float)g.Key.sum_area) ?? 0,
                                 chemicalFertilizers = (float?)g.Key.chemicalFertilizers * ((float)g.Key.area_plant / (float)g.Key.sum_area) ?? 0,
                                 seed = g.Key.seed * ((float?)g.Key.area_plant / (float)g.Key.sum_area) ?? 0,
                                 dopmaterial = g.Key.dopmaterial * ((float?)g.Key.area_plant / (float)g.Key.sum_area) ?? 0,
                                 field_id = g.Key.field_id,
                                 plant_id = g.Key.plant_id,
                                 sum_area = g.Key.sum_area,
                                 area_plant = g.Key.area_plant,
                                 total_sal1 = g.Key.total_salary,
                                 deduction = (float?)g.Key.deduction * ((float)g.Key.area_plant / (float)g.Key.sum_area) ?? 0,
                             }).ToList();
            //
            ////добавляем к итого
            for (int i = 0; i < sevoborot.Count; i++)
            {
                for (int j = 0; j < result.Count; j++)
                {
                    if (result[j].plant_id == sevoborot[i].plant_id)
                    {
                        result[j].salary_total = result[j].salary_total + (double)sevoborot[i].total_salary + (double)sevoborot[i].dop_salary+ (double)sevoborot[i].deduction;  //зп
                        result[j].razx_top = result[j].razx_top + (double)sevoborot[i].consumption; //топливо
                        result[j].szr_fact = result[j].szr_fact + (float)sevoborot[i].szr;  // cзр
                        result[j].minud_fact = result[j].minud_fact + (float)sevoborot[i].chemicalFertilizers;  // удобрения
                        result[j].posevmat_fact = result[j].posevmat_fact + (float)sevoborot[i].seed;  // семена
                        result[j].dop_material_fact = result[j].dop_material_fact + (float)sevoborot[i].dopmaterial;  // доп материалы
                        result[j].total_fact = result[j].total_fact + (double)sevoborot[i].total_salary + (double)sevoborot[i].consumption +
                        (double)sevoborot[i].szr + (double)sevoborot[i].chemicalFertilizers + (double)sevoborot[i].dop_salary +
                        (float)sevoborot[i].seed + (float)sevoborot[i].dopmaterial + (double)sevoborot[i].deduction; //итого(факт)

                        result[j].nzp = (result[j].nzp ?? 0) + (double)sevoborot[i].total_salary + (double)sevoborot[i].consumption +
                        (double)sevoborot[i].szr + (double)sevoborot[i].chemicalFertilizers + (double)sevoborot[i].dop_salary +
                        (float)sevoborot[i].seed + (float)sevoborot[i].dopmaterial + (double)sevoborot[i].deduction;
                    }
                }
            }

            /////////////

            ///убираем повторяющиеся культуры и суммируем план
            for (int j = 0; j < result.Count; j++)
            {
                if (result[j].plant_id == 36) { result.RemoveAt(j); j--; } //удаляем строку урожай
                if (j > 0)
                {
                    if (result[j].plant_id == result[j - 1].plant_id)
                    {
                        result[j].total_plan = result[j].total_plan + result[j - 1].total_plan;  //итого план 
                        result.RemoveAt(j - 1);
                        j--;
                    }
                }
            }
 
            //  считаем для га и т для всех записей
            for (int i = 0; i < result.Count; i++)
            {
                result[i].total_delta = result[i].total_plan - (float)result[i].total_fact;
                if (result[i].gross_harvest != 0) { result[i].cost_ga_plan = result[i].total_plan / (float)result[i].gross_harvest; }
                else { result[i].cost_ga_plan = 0; }//План (т)
                if (result[i].gross_harvest != 0) { result[i].cost_ga_fact = result[i].total_fact / result[i].gross_harvest; }
                else { result[i].cost_ga_fact = 0; } // Факт (т)
                result[i].cost_ga_delta = result[i].cost_ga_plan - (double)result[i].cost_ga_fact; //отклонение (т)

                if (result[i].area != 0) { result[i].cost_t_plan = result[i].total_plan / (float)result[i].area; }
                else { result[i].cost_t_plan = 0; } //План (га)
                if (result[i].area != 0) { result[i].cost_t_fact = (float)result[i].total_fact / (float)result[i].area; }
                else { result[i].cost_t_fact = 0; }//Факт (га)
                result[i].cost_t_delta = result[i].cost_t_plan - (float)result[i].cost_t_fact; // отклонение  га                 
            }
            //
            result.Add(new ExpensesRequestDto
                {
                    plant_id = 0,
                    plant_name = "Всего",
                    salary_total = result.Sum(l => l.salary_total),
                    razx_top = result.Sum(l => l.razx_top),
                    szr_fact = result.Sum(l => l.szr_fact),
                    minud_fact = result.Sum(l => l.minud_fact),
                    posevmat_fact = result.Sum(l => l.posevmat_fact),
                    dop_material_fact = result.Sum(l => l.dop_material_fact),
                    service_total = result.Sum(l => l.service_total),
                    total_fact = result.Sum(l => l.total_fact),
                    total_plan = result.Sum(l => l.total_plan),
                    area = result.Sum(l => l.area),
                    gross_harvest = result.Sum(l => l.gross_harvest),
                    nzp = result.Sum(l => l.nzp),
                });
            return result;
        }

        public byte[] GetResponsablesExcel(string templatePath, int? employeerId)
        {
            byte[] byteArray = File.ReadAllBytes(templatePath);
            using (MemoryStream stream = new MemoryStream())
            {
                stream.Write(byteArray, 0, (int)byteArray.Length);
                using (var document = SpreadsheetDocument.Open(stream, true))
                {
                    string SheetName;
                    SheetName = "Остаток по фильтрам";
                    // 1 лист отчета 
                    Sheet sheet;
                    try
                    {
                        sheet = document.WorkbookPart.Workbook.GetFirstChild<Sheets>().Elements<Sheet>().SingleOrDefault(s => s.Name == SheetName);
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(String.Format("Возможно в документе существует два листа с названием \"{0}\"!\n", SheetName), ex);
                    }

                    if (sheet == null)
                    {
                        throw new Exception(String.Format("В шаблоне не найден \"{0}\"!\n", SheetName));
                    }
                    var worksheetPart = (WorksheetPart)document.WorkbookPart.GetPartById(sheet.Id.Value);
                    var sheetData = worksheetPart.Worksheet.GetFirstChild<SheetData>();

                    if (employeerId != -1)
                    {
                        balanceList = db.Database.SqlQuery<BalanceMaterials>(
                       string.Format(@"select Materials.name AS Name,  Employees.short_name as Employees ,Materials.description AS Description, 
                     TMC_Current.count AS Balance ,TMC_Current.price AS Current_price, Employees.emp_id as IdEmployees, Materials.mat_id as IdMaterial
                     from Employees,TMC_Current,Materials  where Employees.emp_id=TMC_Current.emp_emp_id and mat_id=mat_mat_id and Employees.emp_id=" + employeerId + "")).ToList();

                        for (int i = 0; i < balanceList.Count; i++)
                        {
                            balanceList[i].Current_price = Convert.ToSingle(Math.Round(balanceList[i].Current_price, 2));
                            balanceList[i].Summa = balanceList[i].Current_price * balanceList[i].Balance;
                        }

                    }

                    if (employeerId == -1)
                    {
                        balanceList = db.Database.SqlQuery<BalanceMaterials>(
                       string.Format(@"select Materials.name AS Name,  Employees.short_name as Employees ,Materials.description AS Description, 
                     TMC_Current.count AS Balance ,TMC_Current.price AS Current_price, Employees.emp_id as IdEmployees, Materials.mat_id as IdMaterial
                     from Employees,TMC_Current,Materials  where Employees.emp_id=TMC_Current.emp_emp_id and mat_id=mat_mat_id ")).ToList();

                        for (int i = 0; i < balanceList.Count; i++)
                        {
                            balanceList[i].Current_price = Convert.ToSingle(Math.Round(balanceList[i].Current_price, 2));
                            balanceList[i].Summa = balanceList[i].Current_price * balanceList[i].Balance;
                        }
                    }
                    var list = balanceList;
                    uint a = 5;
                    OldShtrId = new List<int>();
                    if (list.Any())
                    {
                        OldShtrId.Add(list[0].Id1);
                        list[0].Id1 = (int)a - 4;
                        Row refRowC = sheetData.Elements<Row>().Where(r => 5 == r.RowIndex).First();
                        SetCellsValueForResponsables(list[0], refRowC, a);
                        list.Remove(list[0]);
                        a++;
                    }
                    foreach (var item in list)
                    {
                        OldShtrId.Add((int)item.Id1);
                        item.Id1 = (int)a - 4;
                        Row refRowC = sheetData.Elements<Row>().Where(r => 5 == r.RowIndex).First();
                        var row = new Row();
                        row = (Row)refRowC.Clone();
                        row.RowIndex = new UInt32Value(a);

                        foreach (var cell in row.Elements<Cell>())
                        {
                            cell.CellReference = new StringValue(cell.CellReference.Value.Replace(refRowC.RowIndex.Value.ToString(), row.RowIndex.ToString()));
                        }
                        SetCellsValueForResponsables(item, row, a);
                        sheetData.Append(row);
                        a++;
                    }
                    worksheetPart.Worksheet.Save();
                    document.Close();
                }
                return stream.ToArray();
            }

        }
        private void SetCellsValueForResponsables(BalanceMaterials item, Row row, uint i)
        {
            SetValueForCellByRow(row, "A", i, item.Name);
            SetValueForCellByRow(row, "B", i, item.Description);
            SetValueForCellByRow(row, "C", i, item.Balance);
            SetValueForCellByRow(row, "D", i, item.Current_price);
            SetValueForCellByRow(row, "E", i, item.Summa);
            SetValueForCellByRow(row, "F", i, item.Employees);
        }
        ////////////////Детализация по культурам и по полям
        public List<ExpensesRequestDto> GetCostsByCultureByField(int year, int? plant_id = null)
        {
            //зарплата и топливо
            var list = (from ftp in db.Fields_to_plants
                        where ftp.date_ingathering.Year == year && ftp.Fields.type == 1 && (plant_id != null ? ftp.plant_plant_id == plant_id : true)

                        join task1 in db.Tasks on ftp.fielplan_id equals task1.plant_plant_id into leftTask1
                        from t1 in leftTask1.DefaultIfEmpty()

                        group new
                        {
                            consumption = t1.fuel_cost ?? 0,
                            total_salary = t1.total_salary ?? 0
                        }
                            by new
                            {
                                plant_name = ftp.Plants.name,
                                name = ftp.Fields.name,
                                area = ftp.area_plant,
                                id = ftp.fiel_fiel_id
                            } into g
                        select new
                        {
                            id = g.Key.id,
                            name = g.Key.name,
                            area = g.Key.area,
                            plant_name = g.Key.plant_name,
                            consumption = g.Sum(p => p.consumption),
                            total_salary = g.Sum(p => p.total_salary),
                        }).ToList();
            //сзр, удобрения, семена, прочие материалы
            var acts = (from ftp in db.Fields_to_plants
                        where  ftp.Fields.type == 1 && (plant_id != null ? ftp.plant_plant_id == plant_id : true)
                        && ftp.date_ingathering.Year == year
                        join act in db.TMC_Records on ftp.fielplan_id equals act.fielplan_fielplan_id into leftAct1
                        from a in leftAct1.DefaultIfEmpty()
                        group new { ftp, a } by new { ftp.fiel_fiel_id, ftp.Fields.name } into g
                        select new
                        {
                            name = g.Key.name,
                            id = g.Key.fiel_fiel_id,
                            Values = (
                                from m in g.Where(ga => ga.a != null)
                                group new
                                {
                                    cost = (m.a.count * m.a.price==null) ? 0 : (m.a.count * m.a.price),
                                } by new
                                {
                                    m.a.Materials.mat_type_id
                                } into g2
                                select new
                                {
                                    type_id = g2.Key.mat_type_id,
                                    cost = g2.Sum(mat => mat.cost)
                                }

                            ).ToList()
                        }
                        ).ToList();
           ///////////////////////////////////////////////////////сдельная ЗП
            var dopSalary = (from ftp in db.Fields_to_plants
                             where ftp.Fields.type == 1 && (plant_id.HasValue ? ftp.plant_plant_id == plant_id : true)
                              && ftp.date_ingathering.Year == year
                             join sal_detail in db.Salary_details on ftp.fielplan_id equals sal_detail.plant_plant_id into leftDetail
                             from t2 in leftDetail.DefaultIfEmpty() 
                             group new
                             {
                                 sum = t2.sum ?? 0
                             }
                                 by new
                                 {
                                     name = ftp.Fields.name,
                                     fiel_fiel_id = ftp.fiel_fiel_id,
                                 } into g
                             select new
                             {
                                 id = g.Key.fiel_fiel_id,
                                 dopSalary = (double?)g.Sum(p => p.sum)
                             });



            //урожай
            var gross = (from ftp in db.Fields_to_plants
                         where ftp.date_ingathering.Year == year && ftp.Fields.type == 1 && (plant_id != null ? ftp.plant_plant_id == plant_id : true)

                         join grossharvest in db.Gross_harvest on ftp.fielplan_id equals grossharvest.ftp_ftp_id into leftGross
                         from gh in leftGross.DefaultIfEmpty()

                         group new
                         {
                             count = gh.count ?? 0,
                         }
                             by new
                             {
                                 name = ftp.Fields.name,
                                 id = ftp.fiel_fiel_id
                             } into g
                         select new
                         {
                             id = g.Key.id,
                             name = g.Key.name,
                             count = g.Sum(p => p.count),
                         }).ToList();

            var result = (from l in list
                          join act in acts on l.id equals act.id into actLj
                          from a in actLj.DefaultIfEmpty()
                          join gh in gross on l.id equals gh.id into ghLj
                          from g in ghLj.DefaultIfEmpty()

                          join dS in dopSalary on l.id equals dS.id into jSalary
                          from salary in jSalary.DefaultIfEmpty()


                          let a_type1 = a.Values.Where(t => t.type_id == 1).FirstOrDefault()
                          let a_type2 = a.Values.Where(t => t.type_id == 2).FirstOrDefault()
                          let a_type4 = a.Values.Where(t => t.type_id == 4).FirstOrDefault()
                          let a_type5 = a.Values.Where(t => t.type_id == 5).FirstOrDefault()

                          select new ExpensesRequestDto
                          {
                              Id = l.id,
                              fields_name = l.name,
                              plant_name = l.plant_name,
                              area =(double) l.area,
                              salary_total = (double)l.total_salary + ((salary == null) ? 0 : salary.dopSalary),
                              razx_top = (double)l.consumption,
                              szr_fact = a_type1 == null ? 0 : (float?)a_type1.cost,
                              minud_fact = a_type2 == null ? 0 : (float?)a_type2.cost,
                              posevmat_fact = a_type4 == null ? 0 : (float?)a_type4.cost,
                              dop_material_fact = a_type5 == null ? 0 : (float?)a_type5.cost,
                              gross_harvest = (float)Math.Round((decimal)g.count,2)
                          }).OrderBy(l => -(l.salary_total + l.minud_fact + l.szr_fact + l.posevmat_fact + l.razx_top+l.dop_material_fact)).ToList();
                return result;
        }

       ///////////////
        public List<ExpensesRequestDto> GetExpensesList(int year)
        {
            var list = (from ftp in db.Fields_to_plants
                        where ftp.date_ingathering.Year == year && ftp.Fields.type == 1

                        join task1 in db.Tasks on ftp.fielplan_id equals task1.plant_plant_id into leftTask1
                        from t1 in leftTask1.DefaultIfEmpty()

                        group new
                        {
                            consumption = t1.fuel_cost ?? 0,
                            total_salary = t1.total_salary ?? 0
                        }
                            by new
                            {
                                plant_name = ftp.Plants.name,
                                name = ftp.Fields.name,
                                area = ftp.area_plant,
                                id = ftp.fiel_fiel_id
                            } into g
                        select new
                        {
                            id = g.Key.id,
                            name = g.Key.name,
                            area = g.Key.area,
                            plant_name = g.Key.plant_name,
                            consumption = g.Sum(p => p.consumption),
                            total_salary = g.Sum(p => p.total_salary),
                        }).ToList();
            var acts = (from ftp in db.Fields_to_plants
                        where ftp.date_ingathering.Year == year && ftp.Fields.type == 1
                        join act in db.Materials_to_ftp on ftp.fielplan_id equals act.ftp_ftp_id into leftAct1
                        from a in leftAct1.DefaultIfEmpty()

                        group new { ftp, a } by new { ftp.fiel_fiel_id, ftp.Fields.name } into g
                        select new
                        {
                            name = g.Key.name,
                            id = g.Key.fiel_fiel_id,
                            Values = (
                                from m in g.Where(ga => ga.a != null)
                                group new
                                {
                                    cost = m.a.summ ?? 0,
                                } by new
                                {
                                    m.a.Materials.mat_type_id
                                } into g2
                                select new
                                {
                                    type_id = g2.Key.mat_type_id,
                                    cost = g2.Sum(mat => mat.cost)
                                }

                            ).ToList()
                        }
                        ).ToList();

            var result = (from l in list
                          join act in acts on l.id equals act.id
                              into actLj
                          from a in actLj.DefaultIfEmpty()

                          let a_type1 = a.Values.Where(t => t.type_id == 1).FirstOrDefault()
                          let a_type2 = a.Values.Where(t => t.type_id == 2).FirstOrDefault()
                          let a_type4 = a.Values.Where(t => t.type_id == 4).FirstOrDefault()
                          let a_type5 = a.Values.Where(t => t.type_id == 5).FirstOrDefault()
                          select new ExpensesRequestDto
                          {
                              Id = l.id,
                              fields_name = l.name,
                              plant_name = l.plant_name,
                              area = (double)l.area,
                              salary_total = (double)l.total_salary,
                              razx_top = (double)l.consumption,
                              szr_fact = a_type1 == null ? 0 : (float?)a_type1.cost,
                              minud_fact = a_type2 == null ? 0 : (float?)a_type2.cost,
                              posevmat_fact = a_type4 == null ? 0 : (float?)a_type4.cost,
                              dop_material_fact = a_type5 == null ? 0 : (float?)a_type5.cost
                          }).ToList();
            return result;
          }

        
        private void GetMainAndDopSalary( ExpensesDto item )
        {

            if (item.type_price != null)
            {
                switch ((TypePrice)((int)item.type_price))
                {
                    case TypePrice.DayFiled:
                        item.salary_main = item.work_days * item.salary;
                        break;

                    case TypePrice.Ga:
                         item.salary_main = item.area * item.salary;
                        break;
                    case TypePrice.Tonn:
                        item.salary_main = item.volume_fact * item.salary;
                        break;
                    case TypePrice.Hours:
                        item.salary_main = item.work_time * item.salary;
                        break;
                    default:
                        item.salary_main = 0;
                        break;
                }
            }
            else
            {
                item.salary_main = 0;
            }
            if ((item.price_add != null && item.volume_fact != null) || (item.price_add != null && item.area != null))
            {
                if (item.volume_fact != 0)
                    item.salary_dop = item.price_add * item.volume_fact;
                else
                {
                    item.salary_dop = item.price_add * item.area;
                }
                if (item.price_add != null && item.area != null && item.volume_fact == null)
                    item.salary_dop = item.price_add * item.area;

            }
            item.salary_dop = item.salary_dop ?? 0;
         
        }

        private void SetCellsValueForRefuelings(PrintRefuilItemDto item, Row row, uint i)
        {
            SetValueForCellByRow(row, "A", i, item.Ind);
            SetValueForCellByRow(row, "B", i, item.NumList);
            SetValueForCellByRow(row, "C", i, item.Date.ToString("dd.MM.yyyy"));
            SetValueForCellByRow(row, "D", i, item.TechName);
            SetValueForCellByRow(row, "E", i, item.DriverName);
            SetValueForCellByRow(row, "F", i, item.RefuelerName);
            SetValueForDecimalCellByRow(row, "G", i, item.FuelIssued);
            SetValueForDecimalCellByRow(row, "H", i, item.FuelLeft);
            SetValueForDecimalCellByRow(row, "I", i, item.FuelRemain);
            SetValueForDecimalCellByRow(row, "K", i, item.ConsumptionFact);  
            SetValueForDecimalCellByRow(row, "L", i, item.MotoStart);
            SetValueForDecimalCellByRow(row, "M", i, item.MotoEnd);
            SetValueForDecimalCellByRow(row, "N", i, item.MotoWork);
            SetValueForCellByRow(row, "O", i, item.TypeTask);

        }
        //механизаторы 
        private void SetCellsValueForWayList( PrintWayListDto item, Row row , uint i)
        {
          
            SetValueForCellByRow(row, "A", i, item.NumList);
            SetValueForCellByRow(row, "B", i, item.ShiftNum);
            SetValueForCellByRow(row, "C", i, item.Date.ToString("dd.MM.yyyy"));
            SetValueForCellByRow(row, "D", i, item.DriverName);
            SetValueForCellByRow(row, "E", i, item.PlantName);
            SetValueForCellByRow(row, "F", i, item.FieldName);
            SetValueForCellByRow(row, "G", i, item.TypeTask);
            SetValueForCellByRow(row, "H", i, item.TypePriceName);
            SetValueForDecimalCellByRow(row, "I", i, item.WorkDay);
            SetValueForDecimalCellByRow(row, "J", i, item.Volumefact);
            SetValueForDecimalCellByRow(row, "K", i, item.Area);
            SetValueForDecimalCellByRow(row, "L", i, item.AreaAuto);
            SetValueForDecimalCellByRow(row, "n", i, item.Salary);
            SetValueForDecimalCellByRow(row, "m", i, item.Worktime);
            SetValueForDecimalCellByRow(row, "o", i, item.Salary);
            SetValueForDecimalCellByRow(row, "p", i, item.PriceAdd);
            SetValueForDecimalCellByRow(row, "q", i, item.PriceTotal);
            SetValueForCellByRow(row, "r", i, item.TracktorName);
            SetValueForCellByRow(row, "s", i, item.EquipmentName);
            SetValueForDecimalCellByRow(row, "t", i, item.MileageTotal);
            SetValueForDecimalCellByRow(row, "u", i, item.MileageCargo);
            SetValueForDecimalCellByRow(row, "v", i, item.ConsumptionFact);
            SetValueForDecimalCellByRow(row, "w", i, item.ConsumptionAuto);


            SetValueForDecimalCellByRow(row, "x", i, (item.Area != null && item.Area != 0 && item.ConsumptionRate != null ? item.Area * item.ConsumptionRate :    
           item.Volumefact!=null &&  item.ConsumptionRate!=null ?  item.Volumefact*item.ConsumptionRate : 0)); 
            SetValueForCellByRow(row, "y", i, item.TypeFuel.Name); 
            SetValueForDecimalCellByRow(row, "z", i, item.MotoHours);
            if (OldShtrId.Where(s => s == item.ShtrId).Count() == 1) 
            {
                SetValueForDecimalCellByRow(row, "aa", i, item.FuelIssued);
            }
            else
            {
                SetValueForDecimalCellByRow(row, "aa", i, new decimal?());
            }
           
            SetValueForCellByRow(row, "ab", i, item.Comment);
            SetValueForCellByRow(row, "ac", i, item.ResponsibleName);

        }
        private void SetCellsValueForWayListDriver(PrintWayListDto item, Row row, uint i)
        {
            SetValueForCellByRow(row, "A", i, item.NumList);
            SetValueForCellByRow(row, "B", i, item.ShiftNum);
            SetValueForCellByRow(row, "C", i, item.Date.ToString("dd.MM.yyyy"));
            SetValueForCellByRow(row, "D", i, item.DriverName);
            SetValueForCellByRow(row, "E", i, item.PlantName);
            SetValueForCellByRow(row, "F", i, item.FieldName);
            SetValueForCellByRow(row, "G", i, item.TypeTask);
            SetValueForCellByRow(row, "H", i, item.TypePriceName);
            SetValueForDecimalCellByRow(row, "I", i, item.WorkDay); //полевой день? 
            SetValueForDecimalCellByRow(row, "J", i, item.Volumefact);
            SetValueForDecimalCellByRow(row, "K", i, item.Area);
            SetValueForDecimalCellByRow(row, "L", i, item.Worktime);
            SetValueForDecimalCellByRow(row, "M", i, item.Salary);
            SetValueForDecimalCellByRow(row, "O", i, item.PriceAdd);
            SetValueForDecimalCellByRow(row, "P", i, item.PriceTotal);
            SetValueForCellByRow(row, "Q", i, item.TracktorName);
            SetValueForCellByRow(row, "R", i, item.EquipmentName);
            SetValueForDecimalCellByRow(row, "S", i, item.MileageTotal);
            SetValueForDecimalCellByRow(row, "T", i, item.MileageCargo);
            SetValueForDecimalCellByRow(row, "U", i, item.ConsumptionFact);
            SetValueForCellByRow(row, "V", i, item.TypeFuel.Name); //ТИП ТОПЛИВА 
            SetValueForDecimalCellByRow(row, "W", i, item.MotoHours);
            if (OldShtrId.Where(s => s == item.ShtrId).Count() == 1)
            {
                SetValueForDecimalCellByRow(row, "X", i, item.FuelIssued);
            }
            else
            {
                SetValueForDecimalCellByRow(row, "X", i, new decimal?());
            }

            SetValueForCellByRow(row, "Y", i, item.Comment);
            SetValueForCellByRow(row, "Z", i, item.ResponsibleName);
        }
    
      
        private void SetCellsValueForExpensessList(ExpensesRequestDto item, Row row, uint i)
        {
            SetValueForCellByRow(row, "A", i, item.Id);
            SetValueForCellByRow(row, "B", i, item.plant_name);
            SetValueForCellByRow(row, "C", i, item.fields_name);
            SetValueForCellByRow(row, "D", i, item.minud_fact.ToString());
            SetValueForCellByRow(row, "E", i, item.minud_time.ToString());
            SetValueForCellByRow(row, "F", i, item.szr_fact.ToString());
            SetValueForCellByRow(row, "G", i, item.szr_time.ToString());
            SetValueForCellByRow(row, "H", i, item.posevmat_fact.ToString());
            SetValueForCellByRow(row, "I", i, item.vyvozprod_fact.ToString());
            SetValueForCellByRow(row, "J", i, item.razx_top.ToString());
            SetValueForCellByRow(row, "K", i, item.sx_tex.ToString());
            SetValueForCellByRow(row, "L", i, item.gruz_tex.ToString());
            SetValueForCellByRow(row, "M", i, item.salary_main.ToString());
            SetValueForCellByRow(row, "N", i, item.salary_dop.ToString());
        }
        //ПФА
        private void SetCellsValueForCostList(ExpensesRequestDto item, Row row, uint i)
        {
            SetValueForCellByRow(row, "A", i, item.plant_name);
            SetValueForDecimalCellByRow(row, "B", i, (decimal?)item.area);
            SetValueForDecimalCellByRow(row, "C", i, (decimal?)item.gross_harvest);
            SetValueForDecimalCellByRow(row, "D", i, (decimal?)item.salary_total);
            SetValueForDecimalCellByRow(row, "E", i, (decimal?)item.razx_top);
            SetValueForDecimalCellByRow(row, "F", i, (decimal?)item.minud_fact);  
            SetValueForDecimalCellByRow(row, "G", i, (decimal?)item.szr_fact);
            SetValueForDecimalCellByRow(row, "H", i, (decimal?)item.posevmat_fact);
            SetValueForDecimalCellByRow(row, "I", i, (decimal?)item.dop_material_fact);
            SetValueForDecimalCellByRow(row, "J", i, (decimal?)item.service_total); 
            SetValueForDecimalCellByRow(row, "K", i, (decimal?)item.nzp);             
            SetValueForDecimalCellByRow(row, "L", i, (decimal?)item.total_plan);
            SetValueForDecimalCellByRow(row, "M", i, (decimal?)item.total_fact); 
            SetValueForDecimalCellByRow(row, "O", i, (decimal?)item.total_delta);     
            SetValueForDecimalCellByRow(row, "P", i, (decimal?)item.cost_ga_plan);
            SetValueForDecimalCellByRow(row, "Q", i, (decimal?)item.cost_ga_fact);
            SetValueForDecimalCellByRow(row, "R", i, (decimal?)item.cost_ga_delta);
            SetValueForDecimalCellByRow(row, "S", i, (decimal?)item.cost_t_plan);
            SetValueForDecimalCellByRow(row, "T", i, (decimal?)item.cost_t_fact);
            SetValueForDecimalCellByRow(row, "U", i, (decimal?)item.cost_t_delta);
        }

            //cписание ТМЦ
            private void SetCellsValueForWriteOffList(GetActsSzr item, Row row, uint i)
        {
            SetValueForCellByRow(row, "A", i, item.Number);
            SetValueForCellByRow(row, "B", i, item.DateT.ToString("dd.MM.yyyy"));
            SetValueForCellByRow(row, "C", i, item.MaterialName);
            SetValueForCellByRow(row, "D", i, item.PlantName);
            SetValueForCellByRow(row, "E", i, item.FieldName);
            SetValueForDecimalCellByRow(row, "F", i, (decimal)item.Area);
            SetValueForDecimalCellByRow(row, "G", i, (decimal)item.Count1);
            SetValueForDecimalCellByRow(row, "H", i, (decimal)item.ConsumptionRate);
            SetValueForDecimalCellByRow(row, "I", i, (decimal)item.Price1);
            SetValueForDecimalCellByRow(row, "J", i, (decimal)item.Cost1);
            SetValueForCellByRow(row, "K", i, item.RecipientName);
           
        }
        //сдельная ЗП
        private void SetCellsValueForDocList(ExportDocumentsList item, Row row, uint i)
        {
            SetValueForCellByRow(row, "A", i, item.Name);
            SetValueForDecimalCellByRow(row, "B", i, item.sum);
        }
        //Полная сдельная ЗП
        private void SetCellsValueForFullDocList(ExportDocumentsList item, Row row, uint i)
        {
            SetValueForCellByRow(row, "A", i, item.date.ToString("dd-MM-yyyy"));
            SetValueForCellByRow(row, "B", i, item.Name);
            SetValueForCellByRow(row, "C", i, item.fiel_name);
            SetValueForCellByRow(row, "D", i, item.fiel_code);
            SetValueForCellByRow(row, "E", i, item.task_code);
            SetValueForDecimalCellByRow(row, "F", i, (decimal)item.sum);
        }
        private void SetCellsValueForCostList1(ExpensesRequestDto item1, Row row1, uint i1)
        {
            SetValueForCellByRow(row1, "A", i1, item1.plant_name);
            SetValueForDecimalCellByRow(row1, "B", i1, (decimal)item1.salary_total);
            SetValueForDecimalCellByRow(row1, "C", i1, (decimal)item1.razx_top);
            SetValueForDecimalCellByRow(row1, "D", i1, (decimal)item1.minud_fact);
            SetValueForDecimalCellByRow(row1, "E", i1, (decimal)item1.szr_fact);
            SetValueForDecimalCellByRow(row1, "F", i1, (decimal)item1.posevmat_fact);
            SetValueForDecimalCellByRow(row1, "G", i1, (decimal)item1.dop_material_fact);
            SetValueForDecimalCellByRow(row1, "H", i1, (decimal)item1.service_total);
        }


        private PrintRefueilngsDto GetRefuilngsInfo(int? traktId, DateTime startDate, DateTime endDate)
        {
            var result = new PrintRefueilngsDto();
            var list = (from shtr in db.Sheet_tracks
                        where shtr.date_begin >= startDate
                        && shtr.date_begin <= endDate && (traktId != -1 ? shtr.Traktors.trakt_id == traktId : true)

                        join task in db.Tasks on shtr.shtr_id equals task.shtr_shtr_id into leftTask
                        from t in leftTask.DefaultIfEmpty()

                        group new
                        {
                            t.consumption_fact,
                            t.tptas_tptas_id,
                            t.Type_tasks.name,
                            t.moto_hours,
                            t.mileage_total,
                        } by shtr into g1
                        select new PrintRefuilItemDto
                        {
                            ShtrId = g1.Key.shtr_id,
                            Date = g1.Key.date_begin,
                            NumList = g1.Key.num_sheet,
                            DriverName = g1.Key.Employees.short_name,
                            FuelLeft = g1.Key.left_fuel,
                            FuelRemain = g1.Key.remain_fuel,
                            ShtrType = g1.Key.shtr_type_id,
                            MotoStart = g1.Key.moto_start,
                            MotoEnd = g1.Key.moto_end,
                            MotoWork = g1.Key.shtr_type_id == 1 ? g1.Sum(t => t.moto_hours ?? 0) : g1.Sum(t => t.mileage_total ?? 0),
                            FuelIssued = db.Refuelings.Where(r => r.shtr_shtr_id == g1.Key.shtr_id).Sum(r => r.volume),
                            TypeTaskList = g1.GroupBy(t => new { t.tptas_tptas_id, t.name }).Select(gpr => gpr.Key.name).ToList(),
                            ConsumptionFact = g1.Sum(t => t.consumption_fact ?? 0),
                        }).OrderBy(l => l.Date).ThenBy(l => l.NumList).ToList();

            foreach (var e in list)
            {
                e.TypeTask = string.Join(", ", e.TypeTaskList.Select(p => p));
                e.TechName = db.Sheet_tracks.Where(t => t.shtr_id == e.ShtrId).Select(t => t.Traktors.name + "(" + t.Traktors.reg_num + ")").FirstOrDefault();
            }
            var list2 = new List<PrintRefuilItemDto>();
            //заправщики
            var refuelList = db.Refuelings.Where(r => r.shtr_shtr_id == r.Sheet_tracks.shtr_id && r.Sheet_tracks.date_begin >= startDate
                              && r.Sheet_tracks.date_begin <= endDate && r.Sheet_tracks.Traktors.trakt_id == traktId).
                              Select(r => new PrintRefuilItemDto
                              {
                                  ShtrId = r.shtr_shtr_id,
                                  RefuelerName = r.Employees.short_name,
                                  FuelIssued = r.volume != null ? r.volume : 0,
                              }).ToList();

            for (int i = 0; i < list.Count; i++)
            {
                var id = list[i].ShtrId;
                var data = refuelList.Where(t => t.ShtrId == id).FirstOrDefault();
                if (data != null)
                {
                   data.Date = list[i].Date;
                   data.NumList = list[i].NumList;
                   data.DriverName = list[i].DriverName;
                   data.FuelLeft = list[i].FuelLeft;
                   data.FuelRemain = list[i].FuelRemain;
                   data.ShtrType = list[i].ShtrType;
                   data.MotoStart = list[i].MotoStart;
                   data.MotoEnd = list[i].MotoEnd;
                   data.MotoWork = list[i].MotoWork;
                   data.TypeTaskList = list[i].TypeTaskList;
                   data.ConsumptionFact = list[i].ConsumptionFact;  //
                   data.TypeTask = list[i].TypeTask;
                }
                else {
                    list2.Add(list.Where(t => t.ShtrId == id).FirstOrDefault());
                }
            }

            refuelList.AddRange(list2);
            result.items = new List<PrintRefuilItemDto>();
            result.items = refuelList.Where(t => t.DriverName != null).OrderBy(t => t.Date).ThenBy(t => t.NumList).ToList();
            result.TotalFuelIssued = refuelList.GroupBy(l => new { l.ShtrId, l.FuelIssued }).Sum(g => g.Key.FuelIssued);
            result.TotalConsumptionFact = refuelList.Sum(l => l.ConsumptionFact);
            result.TotalMotoWork = refuelList.Sum(l => l.MotoWork);
            var idWayList = refuelList.Select(l => l.ShtrId).ToList();
            var refueling = db.Refuelings.Where(r => idWayList.Contains(r.shtr_shtr_id));
            var TotalAzs = refueling.Where(r => r.emp_emp_id == null);
            var TotalCopylov = refueling.Where(r => r.emp_emp_id == 89);
            result.TotalAzs = TotalAzs.Any() ? TotalAzs.Sum(r => r.volume) : 0;
            result.TotalCopylov = TotalCopylov.Any() ? TotalCopylov.Sum(r => r.volume) : 0;
            return result;
        }

        private List<PrintWayListDto> GetDriverWaysListInfo(int? orgId, DateTime startDate, DateTime endDate)
        {
            var list = (from shtr in db.Sheet_tracks

                        where shtr.shtr_type_id == 2 && shtr.date_begin >= startDate
                        && shtr.date_begin <= endDate && (orgId == -1 || shtr.Traktors.gar_gar_id == orgId)

                        join task in db.Tasks on shtr.shtr_id equals task.shtr_shtr_id into leftTask
                        from t in leftTask.DefaultIfEmpty()

                        select new PrintWayListDto
                        {
                            ShtrId = shtr.shtr_id,
                            Date = shtr.date_begin,
                            NumList = shtr.num_sheet,
                            DriverName = shtr.Employees.short_name,
                            TypeTask = t.Type_tasks.name,
                            FieldName = t.Fields.name,
                            ResponsibleName = t.Employees.short_name,
                            PlantName = t.plant_plant_id != -1 ? t.Fields_to_plants.Plants.name + " (" + t.Fields_to_plants.date_ingathering.Year + ")" : "Культура",
                            ShiftNum = shtr.date_begin.Hour < shtr.date_end.Hour ? 1 : 2,
                            NumRuns = t.num_runs,
                            TracktorName = shtr.Traktors.reg_num == null ? shtr.Traktors.name : shtr.Traktors.name + " (" + shtr.Traktors.reg_num + ") ",
                            EquipmentName = t.Equipments.name,
                            ConsumptionFact = t.consumption_fact,
                            MotoHours = t.moto_hours,
                            ValumeFact = t.volume_fact,
                            MileageTotal = t.mileage_total,
                            MileageCargo = t.mileage_cargo,
                            MileageOnTrack = t.mileage_on_track,
                            MileageOnGround = t.mileage_on_ground,
                            NumRefuel = t.num_refuel,
                            Area = t.area,
                            NumLifts = t.num_lifts,
                            Salary = t.salary,
                            Worktime = t.worktime,
                            WorkDay = t.workdays,
                            Comment = t.description,
                            TypePrice = t.type_price,
                            Volumefact = t.volume_fact,
                            TypeFuel = new DictionaryItemsDTO
                            {
                                Id = t.Sheet_tracks.id_type_fuel,
                                Name = t.Sheet_tracks.Type_fuel.name,
                            },
                            PriceAdd = t.price_add, //t.total_salary - t.salary * ((t.workdays == 0) ? 1 : t.workdays),
                            PriceTotal = t.total_salary,
                            FuelIssued = db.Refuelings.Where(r => r.shtr_shtr_id == shtr.shtr_id).Sum(r => r.volume),
                        }).OrderBy(l => l.Date).ThenBy(l => l.NumList).ToList();

            /*
            foreach (var task in list)
            {
                decimal? k;
                if (task.TypePrice != null)
                {
                    switch ((TypePrice)((int)task.TypePrice))
                    {
                        case TypePrice.DayFiled:
                            k = task.WorkDay * task.Salary;
                            task.TypePriceName = "Пол. день";

                            break;

                        case TypePrice.Hours:
                            k = task.Worktime * task.Salary;
                            task.TypePriceName = "Часы";
                            break;
                        case TypePrice.Kilometers:
                            k = task.MileageTotal * task.Salary;
                            task.TypePriceName = "Километры";
                            break;
                        default:
                            k = 0;
                            break;
                    }
                }
                else
                {
                    k = 0;
                    task.TypePriceName = "";
                }
                if (task.PriceAdd != null && task.Volumefact != null)
                {
                    task.PriceTotal = k + (task.PriceAdd * task.Volumefact);
                }
                else
                {
                    task.PriceTotal = k;
                }
                task.PriceTotal = task.PriceTotal ?? 0;
                task.PriceTotal = Math.Round((decimal)task.PriceTotal, 2);
            }
            */

            return list;
        }


        private  List<PrintWayListDto> GetWayListInfo(int? orgId, DateTime startDate, DateTime endDate)
        {
             var   list = (from shtr in db.Sheet_tracks
                        where shtr.shtr_type_id == 1 && shtr.date_begin >= startDate
                        && shtr.date_begin <= endDate && (orgId == -1 || shtr.Traktors.gar_gar_id == orgId)

                        join task in db.Tasks on shtr.shtr_id equals task.shtr_shtr_id into leftTask
                        from t in leftTask.DefaultIfEmpty()

                        select new PrintWayListDto
                        {
                            ShtrId = shtr.shtr_id,
                            TracktorName = shtr.Traktors.reg_num == null ? shtr.Traktors.name : shtr.Traktors.name + " (" + shtr.Traktors.reg_num + ") ",
                            EquipmentName = t.Equipments.name,
                            ConsumptionFact = t.consumption_fact,
                            Date = shtr.date_begin,
                            NumList = shtr.num_sheet,
                            DriverName = shtr.Employees.short_name,
                            TypeTask = t.Type_tasks.name,
                            FieldName = t.Fields.name,
                            PlantName =  t.plant_plant_id != -1  ?  t.Fields_to_plants.Plants.name + " (" + t.Fields_to_plants.date_ingathering.Year + ")" : "Культура",
                            ResponsibleName = t.Employees.short_name,
                            ShiftNum = shtr.date_begin.Hour < shtr.date_end.Hour ? 1 : 2,
                            TypePrice = t.type_price,
                            Worktime = t.worktime,
                            MotoHours = t.moto_hours,
                            Area = t.area,
                            AreaAuto = t.area_auto,
                            Volumefact = t.volume_fact,
                            PriceAdd = t.price_add,
                            WorkDay = t.workdays,
                            Salary = t.salary,
                            PriceTotal = t.total_salary,
                            Comment = t.description,
                            ConsumptionRate = t.consumption_rate,
                            ConsumptionAuto = t.consumption_auto,
                            TypeFuel = new DictionaryItemsDTO
                            {
                                Id = t.Sheet_tracks.id_type_fuel,
                                Name = t.Sheet_tracks.Type_fuel.name,
                            },
                            FuelIssued = db.Refuelings.Where(r => r.shtr_shtr_id == shtr.shtr_id).Sum(r => r.volume),
                        }).OrderBy(l => l.Date).ThenBy(l => l.NumList).ToList();
          
                return list;
            
        }
   
        public byte[] GetReportTrucksExcel(string pathTemplate, DateTime startDate, DateTime endDate)
        {
            byte[] byteArray = File.ReadAllBytes(pathTemplate);
            using (MemoryStream stream = new MemoryStream())
            {
                stream.Write(byteArray, 0, (int)byteArray.Length);
                using (var document = SpreadsheetDocument.Open(stream, true))
                {
                    var SheetName = "Грузовая техника";
                    // 1 лист отчета 
                    Sheet sheet;
                    try
                    {
                        sheet = document.WorkbookPart.Workbook.GetFirstChild<Sheets>().Elements<Sheet>().SingleOrDefault();
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(String.Format("Возможно в документе существует два листа с названием \"{0}\"!\n", SheetName), ex);
                    }

                    if (sheet == null)
                    {
                        throw new Exception(String.Format("В шаблоне не найден \"{0}\"!\n", SheetName));
                    }

                    var worksheetPart = (WorksheetPart)document.WorkbookPart.GetPartById(sheet.Id.Value);
                    var sheetData = worksheetPart.Worksheet.GetFirstChild<SheetData>();
                    var list = new List<PrintWayListDto>();

                    var tasks = (from t in db.Tasks
                                 where t.Sheet_tracks.Traktors.truck == true && t.Sheet_tracks.date_end > startDate && t.Sheet_tracks.date_begin < endDate
                                 select t).ToList();
                    var res = new
                    {
                        NumRuns = tasks.Sum(p => p.num_runs),
                        VolumeFact = tasks.Sum(p => p.volume_fact),
                        MileageTotal = tasks.Sum(p => p.mileage_total),
                        MileageCargo = tasks.Sum(p => p.mileage_cargo)
                    };

                    Cell cellDat = GetCell(worksheetPart.Worksheet, "C", 2);
                    cellDat.CellValue = new CellValue(startDate.ToString("dd.MM.yyyy"));
                    cellDat.DataType = new EnumValue<CellValues>(CellValues.String);

                    Cell cellDat2 = GetCell(worksheetPart.Worksheet, "E", 2);
                    cellDat2.CellValue = new CellValue(endDate.ToString("dd.MM.yyyy"));
                    cellDat2.DataType = new EnumValue<CellValues>(CellValues.String);

                    Cell cellNumRuns = GetCell(worksheetPart.Worksheet, "C", 4);
                    cellNumRuns.CellValue = new CellValue(res.NumRuns.ToString());
                    cellNumRuns.DataType = new EnumValue<CellValues>(CellValues.String);
                    Cell cellMileageTotal = GetCell(worksheetPart.Worksheet, "C", 5);
                    cellMileageTotal.CellValue = new CellValue(res.MileageTotal.ToString());
                    cellMileageTotal.DataType = new EnumValue<CellValues>(CellValues.String);
                    Cell cellMileageCargo = GetCell(worksheetPart.Worksheet, "C", 6);
                    cellMileageCargo.CellValue = new CellValue(res.MileageCargo.ToString());
                    cellMileageCargo.DataType = new EnumValue<CellValues>(CellValues.String);
                    Cell cellVolumeFact = GetCell(worksheetPart.Worksheet, "C", 7);
                    cellVolumeFact.CellValue = new CellValue(res.VolumeFact.ToString());
                    cellVolumeFact.DataType = new EnumValue<CellValues>(CellValues.String);

                    worksheetPart.Worksheet.Save();
                    document.Close();
                }
                return stream.ToArray();
            }
        }
    }
}
