﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LogusSrv.DAL.DBModel;
using LogusSrv.DAL.Entities.DTO;
using LogusSrv.DAL.Entities.Req;
using LogusSrv.DAL.Entities.Res;

namespace LogusSrv.DAL.Operations
{
    public class KPIDb
    {
        protected readonly LogusDBEntities db = new LogusDBEntities();

        public KPIInfoRes GetKPIInfo(int year)
        {
            var info = new KPIInfoRes();

            var yearList = new List<DictionaryItemsDTO>();
            var endYear = DateTime.Now.Year + 30;
            for (var i = 2000; i <= endYear; i++)
            {
                var item = new DictionaryItemsDTO { Id = i, Name = i.ToString() };
                yearList.Add(item);
            }
            info.YearList = yearList.OrderBy(i => i.Id).ToList();

            info.TCList = (from p in db.TC
                           where p.year == year &&
                                      p.Plants.deleted != true &&
                                      p.Fields.deleted != true  //фильтр культуры
                              select new DictionaryItemsDTO 
                              {
                                  Id = p.id,
                                  Name = p.Plants.name + (p.tpsort_tpsort_id.HasValue ? "(" + p.Type_sorts_plants.name + ")" : "") + (p.fiel_fiel_id.HasValue ? "(" + p.Fields.name + ")" : "")
                              }).ToList();
            return info;
        }

        public KPIResModel GetWorksWithKPI(int year, int id_tc)
        {
            var res = new KPIResModel();

            var tc = db.TC.Where(p => p.id == id_tc).First();

            if (tc.fiel_fiel_id.HasValue)
            {
                res.Fields = db.Fields_to_plants.Where(f => f.fiel_fiel_id == tc.fiel_fiel_id && f.plant_plant_id == tc.plant_plant_id).Where(p => p.Fields.deleted != true)
                    .Select(p => new FieldDictionary { Id = p.fiel_fiel_id, Name = p.Fields.name, Area = p.area_plant}).ToList();
            }
            else if (tc.tpsort_tpsort_id.HasValue)
            {
                res.Fields = db.Sorts_plants_data.Where(f => f.tpsort_tpsort_id.Value == tc.tpsort_tpsort_id.Value && f.Fields_to_plants.plant_plant_id == tc.plant_plant_id).Where(p => p.Fields_to_plants.Fields.deleted != true)
                    .Select(p => new FieldDictionary { Id = p.Fields_to_plants.fiel_fiel_id, Name = p.Fields_to_plants.Fields.name, Area = p.Fields_to_plants.area_plant }).ToList();
            }
            else
            {
                res.Fields = (from ftp in db.Fields_to_plants
                              where ftp.plant_plant_id == tc.plant_plant_id
                                    && ftp.date_ingathering.Year == year && ftp.Fields.deleted != true
                              group ftp by new { ftp.fiel_fiel_id, ftp.Fields.name } into g
                              select new FieldDictionary
                              {
                                  Id = g.Key.fiel_fiel_id,
                                  Name = g.Key.name,
                                  Area = g.Sum(f => f.area_plant)
                              }).ToList();
            }

            List<int> ids = res.Fields.Select(p => p.Id.Value).ToList();

            var tasksDict = db.Tasks.Where(tsk => tsk.Fields_to_plants.date_ingathering.Year == year && tsk.Fields_to_plants.plant_plant_id == tc.plant_plant_id
                        && ids.Contains(tsk.fiel_fiel_id)).Where(t => t.Type_tasks.deleted != true)
                .Select(tsk => new KPITaskResModel
                        {
                            Id = tsk.tas_id,
                            Field = tsk.Fields.name,
                            Date1 = tsk.date_begin != null ? tsk.date_begin.Value : tsk.Sheet_tracks.date_begin,
                            Name = tsk.Type_tasks.name,
                            Kpi = tsk.kpi.HasValue ? tsk.kpi.Value : 100,
                            IdTypeTask = tsk.tptas_tptas_id,
                            IdTypeTaskForGroup = tsk.id_tc_operation.HasValue ? tsk.TC_operation.tptas_tptas_id : tsk.tptas_tptas_id
                        }).GroupBy(tsk => tsk.IdTypeTaskForGroup).ToDictionary(g => g.Key, gr => gr.ToList());

            res.Works = (from t in db.TC_operation
                         where t.id_tc == id_tc && t.Type_tasks.deleted != true

                         select new KPIWorkResModel
                         {
                             Id = t.id,
                             Name = t.Type_tasks.name,
                             IdTypeTask = t.tptas_tptas_id,
                             Date1 = t.date_start,
                             Date2 = t.date_finish
                         }).OrderBy(p => p.Date1).ToList();

            for (var i = res.Works.Count - 1; i >= 0 ; i--)
            {
                var w = res.Works[i];
                if (tasksDict.ContainsKey(w.IdTypeTask))
                {
                    var tsks = tasksDict[w.IdTypeTask];
                    decimal s = 0;
                    for (var j = 0; j < tsks.Count; j++)
                    {
                        tsks[j].Date = tsks[j].Date1.ToString("dd.MM.yyyy");
                        s += tsks[j].Kpi.HasValue ? tsks[j].Kpi.Value : 100;
                    }
                    w.TotalKPI = new TotalKPI
                    {
                        total = tsks.Count * 100,
                        sum = s
                    };

                    w.TotalKPI.rate = Math.Round(s / w.TotalKPI.total * 100, 2);
                    w.tasks = tsks.OrderBy(p => p.Date1).ToList();
                }
            }
            return res;
        }

        public List<DictionaryItemsDTO> GetWorksListByPlant(KPIReqModel req)
        {
            var tc = db.TC.Where(p => p.id == req.id_tc).First();

            var tasks = (from t in db.Tasks
                            where t.Fields_to_plants.date_ingathering.Year == req.year
                                && t.Fields_to_plants.plant_plant_id == tc.plant_plant_id
                                && (tc.fiel_fiel_id.HasValue ? t.fiel_fiel_id == tc.fiel_fiel_id : true)
                                && (t.id_tc_operation == req.id_tc_operation || t.id_tc_operation == null)
                                && (t.Type_tasks.deleted != true)
                            select new 
                           { 
                               Id = t.tas_id,
                               Date =  t.date_begin != null ? t.date_begin.Value : t.Sheet_tracks.date_begin,
                               Name = " " + t.Fields.name + " " + t.Type_tasks.name
                           }).OrderBy(p => p.Name).ToList();

            var res = new List<DictionaryItemsDTO>();
            for (var i=0; i < tasks.Count; i++) {
                var t = tasks[i];
                res.Add(new DictionaryItemsDTO 
                {
                    Id = t.Id,
                    Name = t.Date.ToString("dd.MM.yyyy") + t.Name
                });
            }
            return res;
        }

        public KPIResModel AddTasksToWork(TasksToUpdate req)
        {
            using (var trans = db.Database.BeginTransaction())
            {
                var ids = new List<int>();
                var tasks = req.tasks;
                for (var i = 0; i < tasks.Count; i++)
                {
                    ids.Add(tasks[i].Id.Value);
                }
                var tasForUpdate = db.Tasks.Where(p => ids.Contains(p.tas_id)).ToList();
                for (var i = 0; i < tasForUpdate.Count; i++)
                {
                    tasForUpdate[i].id_tc_operation = req.req.id_tc_operation;
                }

                db.SaveChanges();
                trans.Commit();
            }
            return GetWorksWithKPI(req.req.year, req.req.id_tc);
        }

        public KPIForTaskResModel GetKPI(int tasId)
        {
            var selectedList = (from k in db.KPI_value
                                where k.tas_tas_id == tasId
                                select new KPIValueResModel
                                {
                                    Id = k.id,
                                    IdKPItoPlants = k.id_kpi_to_plants,
                                    IdKpi = k.KPI_to_plants.id_kpi,
                                    IdTypeTask = k.KPI_to_plants.tptas_tptas_id,
                                    IdPlant = k.KPI_to_plants.plant_plant_id,
                                    Name = k.KPI_to_plants.KPI.name,
                                    Value = k.value,
                                    StandartValue = k.KPI_to_plants.standard_value.HasValue ? k.KPI_to_plants.standard_value.Value : k.KPI_to_plants.KPI.standard_value,
                                    DevLimit = k.KPI_to_plants.KPI.deviation_limit,
                                    DevLimit2 = k.KPI_to_plants.KPI.deviation_limit2,
                                    DevLimit3 = k.KPI_to_plants.KPI.deviation_limit3,
                                    Rate = k.KPI_to_plants.KPI.rate,
                                    Rate2 = k.KPI_to_plants.KPI.rate2,
                                    Rate3 = k.KPI_to_plants.KPI.rate3
                                }).ToList();
            var task = db.Tasks.Where(p => p.tas_id == tasId).First();
            if (selectedList.Count == 0)
            {
                selectedList = (from k in db.KPI_to_plants
                                where k.tptas_tptas_id == task.tptas_tptas_id
                                select new KPIValueResModel
                                {
                                    IdKPItoPlants = k.id,
                                    IdKpi = k.id_kpi,
                                    IdTypeTask = k.tptas_tptas_id,
                                    IdPlant = k.plant_plant_id,
                                    Name = k.KPI.name,
                                    StandartValue = k.standard_value.HasValue ? k.standard_value.Value : k.KPI.standard_value,
                                    DevLimit = k.KPI.deviation_limit,
                                    DevLimit2 = k.KPI.deviation_limit2,
                                    DevLimit3 = k.KPI.deviation_limit3,
                                    Rate = k.KPI.rate,
                                    Rate2 = k.KPI.rate2,
                                    Rate3 = k.KPI.rate3
                                }).ToList();
            }

            var kpiids = new List<int>();
            for (var i = 0; i < selectedList.Count; i++)
            {
                kpiids.Add(selectedList[i].IdKpi);
            }

            var otherList = db.KPI.Where(p => !kpiids.Contains(p.id)).Select(p => new KPIValueResModel
                                {
                                    IdKpi = p.id,
                                    IdTypeTask = task.tptas_tptas_id,
                                    IdPlant = task.Fields_to_plants.plant_plant_id,
                                    Name = p.name,
                                    StandartValue = p.standard_value,
                                    DevLimit = p.deviation_limit,
                                    DevLimit2 = p.deviation_limit2,
                                    DevLimit3 = p.deviation_limit3,
                                    Rate = p.rate,
                                    Rate2 = p.rate2,
                                    Rate3 = p.rate3
                                }).ToList();
            var kpis = new KPIForTaskResModel
            {
                ListSelected = selectedList,
                ListOther = otherList
            };
            return kpis;
        }

        public string AddKPIValue(int TaskId, int TotalRate, List<KPIValueResModel> list)
        {
            using (var trans = db.Database.BeginTransaction())
            {
                try {
                    var task = db.Tasks.Where(p => p.tas_id == TaskId).First();
                    task.kpi = TotalRate;

                    var del = db.KPI_value.Where(p => p.tas_tas_id == TaskId).ToList();
                    db.KPI_value.RemoveRange(del);

                    var id = db.KPI_to_plants.Max(p => p.id) + 1;
                    var kpis = new List<KPI_value>();
                    var kpi_to_plants = new List<KPI_to_plants>();
                    for (var i = 0; i < list.Count; i++)
                    {
                        var l = list[i];
                        var kpi_to_plant = db.KPI_to_plants.Where(p => p.plant_plant_id == task.Fields_to_plants.plant_plant_id 
                                                                       && p.tptas_tptas_id == task.tptas_tptas_id 
                                                                       && p.id_kpi == l.IdKpi).FirstOrDefault();
                        if (kpi_to_plant != null) {
                            kpi_to_plant.standard_value = l.StandartValue;
                        } else {
                             kpi_to_plant = new KPI_to_plants
                             {
                                 id = id,
                                 id_kpi = l.IdKpi,
                                 tptas_tptas_id = task.tptas_tptas_id,
                                 plant_plant_id = task.Fields_to_plants.plant_plant_id,
                                 standard_value = l.StandartValue
                             };
                            id++;
                            kpi_to_plants.Add(kpi_to_plant);
                        }
                        var k = new KPI_value
                        {
                            id_kpi_to_plants = kpi_to_plant.id,
                            tas_tas_id = TaskId,
                            value = l.Value
                        };
                        kpis.Add(k);
                    }

                    db.KPI_to_plants.AddRange(kpi_to_plants);
                    db.KPI_value.AddRange(kpis);
                    db.SaveChanges();
                    trans.Commit();
                }
                catch (Exception)
                {
                    trans.Rollback();
                    throw;
                }
            }
            return "success" ;
        }
        public string DeleteTasksFromWork(int tasId)
        {
            using (var trans = db.Database.BeginTransaction())
            {
                var tasForDelete = db.Tasks.Where(p => p.tas_id == tasId).First();
                tasForDelete.id_tc_operation = null;

                var kpis = db.KPI_value.Where(p => p.tas_tas_id == tasId).ToList();
                db.KPI_value.RemoveRange(kpis);

                db.SaveChanges();
                trans.Commit();
                return "success";
            }
        }
    }
}
