﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LogusSrv.DAL.DBModel;
using LogusSrv.DAL.Entities.DTO;
using LogusSrv.DAL.Entities.Res;
using System.Data.SqlClient;
using LogusSrv.CORE.Exceptions;
using System.Data.Entity.Validation;
using System.Data.Entity.Infrastructure;

namespace LogusSrv.DAL.Operations
{
    public class RateFuelDb
    {
        protected readonly LogusDBEntities db = new LogusDBEntities();

        public List<RateFuelList> GetWorks(TypeTech req)
        {
            int? track_id = null; Sheet_tracks sheet = null; Boolean? checkSeason = null;
            if (req.Sheet_id != null) {
            var startSummer  = new DateTime(DateTime.Now.Year, 04, 01);
            var endSummer  = new DateTime(DateTime.Now.Year, 10, 30);
            track_id = db.Sheet_tracks.Where(t => t.shtr_id == req.Sheet_id).Select(t => t.Traktors.trakt_id).FirstOrDefault();
            sheet =  db.Sheet_tracks.Where(t => t.shtr_id == req.Sheet_id).FirstOrDefault();
            checkSeason  = (sheet.date_begin.Date >= startSummer && sheet.date_begin.Date < endSummer) ? true : false;
            }
            var allRate = db.Rate_fuel;
            return db.Traktors.Where(p => p.deleted != true && p.id_type_way_list != null && ((!req.Car && req.Tech) ? p.id_type_way_list == 1 : true )
                && ((req.Car && !req.Tech) ? p.id_type_way_list == 2 : true) && (track_id != null ? p.trakt_id == track_id : true)
                )
                .Select(p => new RateFuelList
            {
                Id = p.trakt_id,
                index = (int)p.id_type_way_list,
                Name = p.name + " "+ p.reg_num,
                Summer_track = allRate.Where(t => t.trakt_trakt_id == p.trakt_id).Select(t => t.summer_track).FirstOrDefault(),
                Summer_city = allRate.Where(t => t.trakt_trakt_id == p.trakt_id).Select(t => t.summer_city).FirstOrDefault(),
                Summer_field = allRate.Where(t => t.trakt_trakt_id == p.trakt_id).Select(t => t.summer_field).FirstOrDefault(),
                Winter_track = allRate.Where(t => t.trakt_trakt_id == p.trakt_id).Select(t => t.winter_track).FirstOrDefault(),
                Winter_city = allRate.Where(t => t.trakt_trakt_id == p.trakt_id).Select(t => t.winter_city).FirstOrDefault(),
                Winter_field = allRate.Where(t => t.trakt_trakt_id == p.trakt_id).Select(t => t.winter_field).FirstOrDefault(),
                Coef = allRate.Where(t => t.trakt_trakt_id == p.trakt_id).Select(t => t.coef).FirstOrDefault(),
                Engine_heating = allRate.Where(t => t.trakt_trakt_id == p.trakt_id).Select(t => t.engine_heating).FirstOrDefault(),
                Icebox = allRate.Where(t => t.trakt_trakt_id == p.trakt_id).Select(t => t.icebox).FirstOrDefault(),
                Icebox_per = allRate.Where(t => t.trakt_trakt_id == p.trakt_id).Select(t => t.icebox_per).FirstOrDefault(),
                Body_lifting = allRate.Where(t => t.trakt_trakt_id == p.trakt_id).Select(t => t.body_lifting).FirstOrDefault(),
                Heating = allRate.Where(t => t.trakt_trakt_id == p.trakt_id).Select(t => t.heating).FirstOrDefault(),
                Trailer = allRate.Where(t => t.trakt_trakt_id == p.trakt_id).Select(t => t.trailer).FirstOrDefault(),
                Sink = allRate.Where(t => t.trakt_trakt_id == p.trakt_id).Select(t => t.sink).FirstOrDefault(),
                Refill = allRate.Where(t => t.trakt_trakt_id == p.trakt_id).Select(t => t.refill).FirstOrDefault(),
                CheckSeason = checkSeason,
                Setting = allRate.Where(t => t.trakt_trakt_id == p.trakt_id).Select(t => t.setting).FirstOrDefault(),
            }).OrderByDescending(p=>p.index).ThenBy(p => p.Name).ToList();
        }

        public string UpdateRate(List<RateFuelList> req)
        {
            using (var trans = db.Database.BeginTransaction())
            {
                try
                {
                    var allRate = new List<Rate_fuel>();
                    if (req[0].Car && req[0].Tech)
                    {
                         allRate = db.Rate_fuel.ToList();
                    }
                    if (!req[0].Car && req[0].Tech)
                    {
                        var Ids = db.Traktors.Where(t => t.id_type_way_list == 1).Select(t => t.trakt_id);
                        allRate = db.Rate_fuel.Where(t=>Ids.Contains (t.trakt_trakt_id)).ToList();
                    }
                    if (req[0].Car && !req[0].Tech)
                    {
                        var Ids2 = db.Traktors.Where(t => t.id_type_way_list == 2).Select(t => t.trakt_id);
                        allRate = db.Rate_fuel.Where(t => Ids2.Contains(t.trakt_trakt_id)).ToList();
                    } 

                    db.Rate_fuel.RemoveRange(allRate);
                    db.SaveChanges();
                    var list = new List<Rate_fuel>();
                    req = req.Where(t => t.Summer_city != null || t.Summer_field != null || t.Summer_track != null || t.Coef != null ||
                    t.Engine_heating != null || t.Icebox != null || t.Winter_city != null || t.Winter_field != null || t.Winter_track != null ||
                    t.Body_lifting != null || t.Heating != null || t.Icebox_per != null || t.Trailer != null || t.Sink != null
                    || t.Refill != null).ToList();
                    int nId = 1;
                    var count =  db.Rate_fuel.Count();
                    if (count != 0)
                    {
                         nId = db.Rate_fuel.Max(t => t.id) + 1;
                    }
                    for (int i = 0; i < req.Count; i++) {
                        var dic = new Rate_fuel
                        {
                            id = nId,
                            trakt_trakt_id = (int)req[i].Id,
                            summer_track = req[i].Summer_track,
                            summer_city = req[i].Summer_city,
                            summer_field = req[i].Summer_field,
                            coef = req[i].Coef,
                            engine_heating = req[i].Engine_heating,
                            icebox = req[i].Icebox,
                            icebox_per = req[i].Icebox_per,
                            winter_city = req[i].Winter_city,
                            winter_field = req[i].Winter_field,
                            winter_track = req[i].Winter_track,
                            body_lifting = req[i].Body_lifting,
                            heating = req[i].Heating,
                            sink = req[i].Sink,
                            trailer = req[i].Trailer,
                            refill = req[i].Refill,
                            setting = req[i].Setting,
                        };
                        list.Add(dic);
                        nId++;
                    }
                    db.Rate_fuel.AddRange(list);

                    try
                    {
                        db.SaveChanges();
                    }
                    catch (DbUpdateException dbu)
                    {
                   //     throw new BllException(15, "Норма расхода используется.Удаление невозможно.");
                    }

                    trans.Commit();
                }
                catch (Exception)
                {
                    trans.Rollback();
                    throw;
                }
            }
            return "success";
        }

        public string DeleteRate(int req)
        {
            using (var trans = db.Database.BeginTransaction())
            {
                try
                {
                    var dist = db.Rate_fuel.Where(t => t.trakt_trakt_id == req).FirstOrDefault();
                    db.Rate_fuel.Remove(dist);
                    try
                    {
                        db.SaveChanges();
                    }
                    catch (DbUpdateException dbu)
                    { }

                    trans.Commit();
                }
                catch (Exception)
                {
                    trans.Rollback();
                    throw;
                }
            }
            return "success";
        }
    }

    public class TypeTech
    {
        public Boolean Car { get; set; }
        public Boolean Tech { get; set; }
        public int? Sheet_id { get; set; }

    }

    public class RateFuelList : DictionaryItemsDTO
    {
    public float? Summer_track { get; set; }
    public float? Winter_track { get; set; }
    public float? Summer_city { get; set; }
    public float? Winter_city { get; set; }
    public float? Summer_field { get; set; }
    public float? Winter_field { get; set; }
    public float? Coef { get; set; }
    public float? Body_lifting { get; set; }
    public float? Engine_heating { get; set; }
    public float? Heating { get; set; }
    public float? Setting { get; set; }
    public float? Icebox { get; set; }
    public float? Icebox_per { get; set; }
    public float? Trailer { get; set; }
    public float? Sink { get; set; }
    public float? Refill { get; set; }
    public Boolean Car { get; set; }
    public Boolean Tech { get; set; }
    public Boolean? CheckSeason { get; set; }

    }

    public class ModelUpdateFuelRate 
    {
        public RateFuelList Document { get; set; }
    }
}
