﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity.Spatial;
using System.Globalization;
using System.Timers;
using LogusSrv.DAL.Utils;
using LogusSrv.DAL.DBModel;
using System.IO;
using System.Net.Sockets;

namespace LogusSrv.DAL.Operations
{
    public class GPSSocketDb
    {

        protected readonly LogusDBEntities db = new LogusDBEntities();
        private static Timer aTimer;
        public Boolean StartTimer = false;
        public Boolean canBegin = true;
        public GPSSocketDb()
        {
        }

        public string StartGPSTimer(){
            if (!StartTimer)
            {
              StartTimer = true;
                aTimer = new System.Timers.Timer(60000);
                // Hook up the Elapsed event for the timer. 
                aTimer.Elapsed += OnTimedEvent;
                aTimer.Enabled = true;
            }
            return "start gps timer";
        }

        public string StopGPSTimer()
        {
            aTimer.Enabled = false;
            StartTimer = false;
            return "stop gps timer";
        }

        public List<GPS> GetCoordinatesByTraktorId(int idTrack)
        {
            List<GPS> list = new List<GPS>();
            using (var trans = db.Database.BeginTransaction())
            {
                list = db.GPS.Where(t => t.track_id == idTrack).ToList();
            }
            return list;
        }
        private void OnTimedEvent(Object source, ElapsedEventArgs e)
        {
            if (canBegin)
                using (var trans = db.Database.BeginTransaction())
                {
                    canBegin = false;
                    List<GPS> listGps = new List<GPS>();
                    var traktorsList = db.Traktors.Where(t => t.track_id != null).Select(t => t.track_id).ToList();
                    foreach (var traktor in traktorsList)
                    {
                        listGps.Add(getGpsValues((int)traktor));
                    }
                    //
                    int id = CommonFunction.GetNextId(db,"GPS");
                    var list = listGps.Where(g => g.track_id != null && g.track_id != 0);
                    foreach (var item in list)
                    {
                        var doubleTime = db.GPS.Where(t => t.track_id == item.track_id && t.date == item.date).ToList();
                        if (!doubleTime.Any())
                        {
                            id++;
                            item.gps_id = id;
                            db.GPS.Add(item);
                        }

                    }
                    db.SaveChanges();
                    trans.Commit();
                    canBegin = true;
                }
        }

        public GPS getGpsValues(int idTrack)
        {
            GPS gps = new GPS();
            try
            {
                TcpClient client = new TcpClient();
                client.Connect("89.18.136.230", 30001);

                String m_login = "logus.test";
                uint login_type = 0xDEAD1000;
                int login_size = m_login.Length;

                NetworkStream Stream = client.GetStream();
                BinaryWriter sw = new BinaryWriter(Stream);
                BinaryReader sr = new BinaryReader(Stream);

                sw.Write(GetBytes(login_type));
                sw.Write(int2byte(login_size));
                sw.Write(GetBytes(m_login));
                sw.Flush();

                byte[] m_buff = new byte[4];
                sr.Read(m_buff, 0, 4);
                uint status_ok = BitConverter.ToUInt32(m_buff, 0);

                sr.Read(m_buff, 0, 4);
                int status_size = BitConverter.ToInt32(m_buff, 0);

                //Send password
                String m_password = "qwerty"; //"logus3674";
                uint passw_type = 0xDEAD1001;
                int passw_size = m_password.Length;

                sw.Write(GetBytes(passw_type));
                sw.Write(int2byte(passw_size));
                sw.Write(GetBytes(m_password));
                sw.Flush();

                sr.Read(m_buff, 0, 4);
                status_ok = BitConverter.ToUInt32(m_buff, 0);

                sr.Read(m_buff, 0, 4);
                status_size = BitConverter.ToInt32(m_buff, 0);

                uint get_queue_type = 0xDEAD5000;
                int id = idTrack; //ID нужного объекта
                sw.Write(GetBytes(get_queue_type));
                sw.Write(int2byte(id));
                sw.Flush();
                sr.Read(m_buff, 0, 4);
                uint type = BitConverter.ToUInt32(m_buff, 0);

                if (type == 0xDEAD6000)
                {
                    sr.Read(m_buff, 0, 4);
                    var flags = System.BitConverter.ToSingle(m_buff, 0);
                    gps.track_id = idTrack;
                    sr.Read(m_buff, 0, 4);
                    var date = System.BitConverter.ToInt32(m_buff, 0);//время в UTC
                    DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
                    gps.date = dtDateTime.AddSeconds((long)date);

                    sr.Read(m_buff, 0, 4);
                    var lat = System.BitConverter.ToSingle(m_buff, 0); //широта

                    sr.Read(m_buff, 0, 4);
                    var lon = System.BitConverter.ToSingle(m_buff, 0); //долгота
                    var point = string.Format(CultureInfo.InvariantCulture.NumberFormat, "POINT({1} {0})", lat, lon);
                    gps.coord = DbGeography.PointFromText(point, 4326);
                    sr.Read(m_buff, 0, 4);
                    gps.speed = (decimal)System.BitConverter.ToSingle(m_buff, 0);//скорость

                    sr.Read(m_buff, 0, 4);
                    gps.direction = (decimal)System.BitConverter.ToSingle(m_buff, 0);//направление

                    sr.Read(m_buff, 0, 4);
                    gps.mileage = (decimal)System.BitConverter.ToSingle(m_buff, 0);//пробег
                }
                else if (type == 0xDEAD6001)
                {//нет пакета
                    Console.WriteLine("No pakage");
                }

                uint log_out_type = 0xDEAD1004;
                sw.Write(GetBytes(log_out_type));
                sw.Write(int2byte(0));
                sw.Close();
                sr.Close();
                client.Close();

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }

            return gps;

        }


        public byte[] int2byte(int i)
        {
            return BitConverter.GetBytes(i);
        }

        public static byte[] GetBytes(uint value)
        {
            return ReverseAsNeeded(BitConverter.GetBytes(value));
        }

        private static byte[] ReverseAsNeeded(byte[] bytes)
        {
            if (BitConverter.IsLittleEndian)
                return bytes;
            else
                return (byte[])bytes.Reverse().ToArray();
        }

        static byte[] GetBytes(string str)
        {
            byte[] bytes = System.Text.Encoding.ASCII.GetBytes(str);

            return bytes;
        }
        public int Byte2int(byte[] m_buff)
        {
            if (BitConverter.IsLittleEndian)
                Array.Reverse(m_buff);

            int i = BitConverter.ToInt32(m_buff, 0);
            Console.WriteLine("int: {0}", i);
            return i;
        } 
    }
}
