﻿using System;
using System.Data.Entity;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LogusSrv.CORE.Exceptions;
using LogusSrv.DAL.Entities.DTO.ActsSzr;
using LogusSrv.DAL.Entities.DTO;
using LogusSrv.DAL.Entities.Req;
using LogusSrv.DAL.DBModel;
using LogusSrv.DAL.Utils;
using LogusSrv.DAL.Enums;
using LogusSrv.DAL.Entities.Res;
using System.Xml.Serialization;


namespace LogusSrv.DAL.Operations
{
    [Serializable]
    [XmlRoot("Организация")]
    public class MaterialXML
    {
        [XmlElement("ПОСТУПЛЕНИЕ")]
        public OperationsIn Income { get; set; }

        [XmlElement("СПИСАНИЕ")]
        public  OperationsOut Outcome { get; set; }
    }

    public class OperationsOut
    {
        [XmlElement("Операция")]
        public List<OperationOutcome> Operation { get; set; }
    }
    public class OperationsIn
    {
        [XmlElement("Операция")]
        public List<OperationIncome> Operation { get; set; }
    }

    public class OperationIncome: Operation
    {
        [XmlElement("КорСубконто1")]
        public Contracotr Contractor { get; set; }

        [XmlElement("Субконто1")]
        public Contracotr Material { get; set; }
    }

    public class Contracotr
    {
        [XmlAttribute("Ссылка")]
        public string Link { get; set; }

        [XmlAttribute("Наименование")]
        public string Name { get; set; }
    }

    public class OperationOutcome : Operation
    {
        [XmlElement("Подразделение")]
        public Contracotr Field { get; set; }

        [XmlElement("Субконто1")]
        public Contracotr Ftp { get; set; }

        [XmlElement("КорСубконто1")]
        public Contracotr Material { get; set; }
    }

    public class Operation
    {
        [XmlElement("Дата")]
        public string Date { get; set; }

        [XmlElement("Время")]
        public string Time { get; set; }

        [XmlElement("Количество")]
        public string Count { get; set; }

        [XmlElement("Сумма")]
        public string Sum { get; set; }
    }
        
}
