﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using LogusSrv.DAL.DBModel;
using LogusSrv.DAL.Entities.DTO;
using LogusSrv.DAL.Entities.DTO.ExportExcel;
using System.IO;
using LogusSrv.DAL.Entities.Res;
using LogusSrv.DAL.Entities.DTO.FileArchive;
using System.Xml.Serialization;
using System.Xml;
using System.Diagnostics;
using System.Globalization;
using System.Data.Entity.Validation;

namespace LogusSrv.DAL.Operations
{
    public class ParseXMLGrossHarvest
    {
        protected readonly LogusDBEntities db = new LogusDBEntities();
        private List<GrossHarvestOperationIncome> lost = new List<GrossHarvestOperationIncome>();
        //======================================================================================================================================
        //======================FUEL_COST=======================================================================================================
        public string ParseGrossHarvest(FileUploadDto file)
        {
            using (var ms = new MemoryStream(file.contents))
            {
                ms.Seek(0, SeekOrigin.Begin);

                using (var xr = XmlReader.Create(ms))
                {
                    var serializer = new XmlSerializer(typeof(GrossHarvestXML));

                    var obj = (GrossHarvestXML)serializer.Deserialize(xr);
                    using (var trans = db.Database.BeginTransaction())
                    {
                        insertAndRecountGrossHarvest(obj.Income.Operation);

                        var grouped = (from out1 in lost
                                       group out1 by new
                                       {
                                           linkPlant = out1.Ftp.Link,
                                           namePlant = out1.Ftp.Name,
                                           linkField = out1.Field.Link,
                                           nameField = out1.Field.Name
                                       } into k
                                       select new
                                       {
                                           LinkPlant = k.Key.linkPlant,
                                           NamePlant = k.Key.namePlant,
                                           LinkField = k.Key.linkField,
                                           NameField = k.Key.nameField,
                                           Outcome = k.ToList()
                                       }).ToList();

                        for (var i = 0; i < grouped.Count; i++)
                        {
                            Debug.WriteLine("------------------------");
                            Debug.WriteLine(grouped[i].NamePlant + " " + grouped[i].LinkPlant);
                            Debug.WriteLine(grouped[i].NameField + " " + grouped[i].LinkField);
                        }
                        db.SaveChanges();
                        trans.Commit();
                        return obj.Income.Operation.Count.ToString();
                    }
                }
            }
        }

        private void insertAndRecountGrossHarvest(List<GrossHarvestOperationIncome> income)
        {
            var newContrId = (db.Contractor.Max(t => (int?)t.id) ?? 0) + 1;
            for (var i = 0; i < income.Count; i++)
            {
                float count, cost;

                float.TryParse(income[i].Count.Replace(',', '.').Replace(" ", String.Empty), NumberStyles.Any, new CultureInfo("en-US"), out count);
                float.TryParse(income[i].Sum.Replace(',', '.').Replace(" ", String.Empty), NumberStyles.Any, new CultureInfo("en-US"), out cost);
                float price = cost / count;

                var dateParsed = DateTime.ParseExact(income[i].Date, "dd.MM.yyyy", new CultureInfo("en-US"));

                var ftpid = getFtp(income[i].Ftp, income[i].Field);
                if (ftpid != null) {
                    var incomeItem = new Gross_harvest
                    {
                        count = (long) count,
                        cost = (long) cost,
                        price = (long) price,
                        date = dateParsed,
                        ftp_ftp_id = ftpid.fielplan_id
                    };

                    var prod = getProduct(ftpid.plant_plant_id);
                    var prodcurcost = prod.balance * prod.current_price;
                    prod.balance += (long)count;
                    var newCost = prodcurcost + (decimal)cost;
                    var newCurPrice = newCost / prod.balance;
                    prod.current_price = (long)newCurPrice;

                    db.Gross_harvest.Add(incomeItem);
                }
                else
                {
                    lost.Add(income[i]);
                }
            };
        }

        private Fields_to_plants getFtp(Contracotr ftp, Contracotr field)
        {
            Fields_to_plants ftpsearch;
            try
            {
                ftpsearch = (from m in db.Fields_to_plants
                             where m.in_code == ftp.Link && m.Fields.code == field.Link
                            select m).First();
                return ftpsearch;
            }
            catch (Exception)
            {
               /* Debug.WriteLine("----------------------------------------------------");
                Debug.WriteLine(ftp.Link + " " + ftp.Name);
                Debug.WriteLine(field.Link + " " + field.Name);*/
                return null;
            }
        }

        private Product getProduct(int plant_plant_id)
        {
            var prod = (from p in db.Product
                        where p.plant_plant_id == plant_plant_id
                        select p).FirstOrDefault();
            if (prod != null)
            {
                return prod;
            }
            else
            {
                prod = new Product
                {
                    plant_plant_id = plant_plant_id,
                    balance = 0,
                    current_price = 0
                };
                db.Product.Add(prod);
                db.SaveChanges();
                return prod;
            }
        }
    }
}


