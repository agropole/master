﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using LogusSrv.DAL.DBModel;
using LogusSrv.DAL.Entities.DTO;
using LogusSrv.DAL.Entities.DTO.ExportExcel;
using System.IO;
using LogusSrv.DAL.Entities.Res;
using LogusSrv.DAL.Entities.DTO.FileArchive;
using System.Xml.Serialization;
using System.Xml;
using System.Diagnostics;
using System.Globalization;
using System.Data.Entity.Validation;

namespace LogusSrv.DAL.Operations
{
    public class ParseXMLFuel
    {
        protected readonly LogusDBEntities db = new LogusDBEntities();
        private decimal TotalLostSum;
        private List<OperationOutcome> LostOperation;

        public string ParseFuel(FileUploadDto file)
        {
            using (var ms = new MemoryStream(file.contents))
            {
                ms.Seek(0, SeekOrigin.Begin);

                using (var xr = XmlReader.Create(ms))
                {
                    var serializer = new XmlSerializer(typeof(MaterialXML));

                    var obj = (MaterialXML)serializer.Deserialize(xr);
                    using (var trans = db.Database.BeginTransaction())
                    {
                        var outcome = obj.Outcome.Operation;

                        var newId = (db.Type_fuel.Max(t => (int?)t.id) ?? 0) + 1;
                        newId = insertAndRecountFuelIn(obj.Income.Operation, newId);

                        LostOperation = new List<OperationOutcome>();
                        TotalLostSum = 0;
                        newId = insertAndRecountFuelOut(obj.Outcome.Operation, newId);


                        var grouped = (from out1 in LostOperation
                                       group out1 by new
                                       {
                                           linkPlant = out1.Ftp.Link,
                                           namePlant = out1.Ftp.Name,
                                           linkField = out1.Field.Link,
                                           nameField = out1.Field.Name
                                       } into k
                                       select new
                                       {
                                           LinkPlant = k.Key.linkPlant,
                                           NamePlant = k.Key.namePlant,
                                           LinkField = k.Key.linkField,
                                           NameField = k.Key.nameField,
                                           Outcome = k.ToList()
                                       }).ToList();

                        for (var i = 0; i < grouped.Count; i++)
                        {
                            Debug.WriteLine(grouped[i].NamePlant + " " + grouped[i].NameField + " " + grouped[i].Outcome.Count);
                        }
                        Debug.WriteLine("total " + TotalLostSum);
                        db.SaveChanges();
                        trans.Commit();
                        return obj.Income.Operation.Count + " " + obj.Outcome.Operation.Count;
                    }
                }
            }
        }

        private int insertAndRecountFuelIn(List<OperationIncome> income, int newId){
            var newContrId = (db.Contractor.Max(t => (int?)t.id) ?? 0) + 1;
            for (var i = 0; i < income.Count; i++)
            {
                float count, cost;

                float.TryParse(income[i].Count.Replace(',', '.').Replace(" ", String.Empty), NumberStyles.Any, new CultureInfo("en-US"), out count);
                float.TryParse(income[i].Sum.Replace(',', '.').Replace(" ", String.Empty), NumberStyles.Any, new CultureInfo("en-US"), out cost);
                float price = cost / count;

                var dateParsed = DateTime.ParseExact(income[i].Date, "dd.MM.yyyy", new CultureInfo("en-US"));

                var fueltype = getFuel(income[i].Material, newId);
                if (newId == fueltype.id)
                {
                    newId++;
                }

                var contracotr = getContractor(income[i].Contractor, newContrId);
                if (contracotr.id == newContrId)
                {
                    newContrId++;
                }
                var incomeItem = new Acts_fuel_income
                {
                    count = count,
                    cost = cost,
                    price = price,
                    date = dateParsed,
                    id_type_fuel = fueltype.id,
                    id_contractor = contracotr.id,
                };

                var matcurcost = fueltype.balance * fueltype.current_price;
                fueltype.balance += (float)count;
                var newCost = matcurcost + cost;
                var newCurPrice = newCost / fueltype.balance;
                fueltype.current_price = newCurPrice;

                db.Acts_fuel_income.Add(incomeItem);
            };
            return newId;
        }

        private Type_fuel getFuel(Contracotr fuellink, int newId)
        {
            Type_fuel mat;
            try
            {
                mat = (from m in db.Type_fuel
                       where m.code == fuellink.Link
                       select m).First();
                return mat;
            }
            catch (Exception)
            {
                try
                {
                    mat = new Type_fuel
                    {
                        id = newId,
                        name = fuellink.Name,
                        code = fuellink.Link,
                        balance = 0,
                        current_price = 0,
                        tpsal_tpsal_id = 9
                    };
                    db.Type_fuel.Add(mat);
                    db.SaveChanges();
                    return mat;
                }
                catch (DbEntityValidationException e)
                {
                    foreach (var eve in e.EntityValidationErrors)
                    {
                        Debug.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                            eve.Entry.Entity.GetType().Name, eve.Entry.State);
                        foreach (var ve in eve.ValidationErrors)
                        {
                            Debug.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                                ve.PropertyName, ve.ErrorMessage);
                        }
                    }
                    throw;
                }
            }
        }

        private int insertAndRecountFuelOut(List<OperationOutcome> outcome, int newId)
        {
            var groupedOut = (from o in outcome
                              group o by new { o.Material.Link, o.Material.Name} into k
                              select new
                              {
                                  Link = k.Key.Name,
                                  Name = k.Key.Link,
                                  Outcome = k.ToList()
                              }
                ).ToList();
            for (var i = 0; i < groupedOut.Count; i++)
            {
                var fueltype = getFuel(new Contracotr { Link = groupedOut[i].Link, Name = groupedOut[i].Name}, newId);
                if (newId == fueltype.id)
                {
                    newId++;
                }

                var shtr = new Sheet_tracks
                {
                    org_org_id = 1,
                    num_sheet = "INTEGR_FUEL" + i,
                    emp_emp_id = 168,
                    user_user_id = 6,
                    date_begin = new DateTime(2015, 5, 10, 9, 0, 0),
                    date_end = new DateTime(2015, 5, 10, 21, 0, 0),
                    status = 4,
                    danger_cargo = false,
                    id_type_fuel = fueltype.id
                };
                db.Sheet_tracks.Add(shtr);
                db.SaveChanges();

                var outcomeForTask = groupedOut[i].Outcome;
                for (var j = 0; j < outcomeForTask.Count; j++)
                {
                    decimal count, cost;
                    decimal.TryParse(outcomeForTask[j].Count.Replace(',', '.').Replace(" ", String.Empty), NumberStyles.Any, new CultureInfo("en-US"), out count);
                    decimal.TryParse(outcomeForTask[j].Sum.Replace(',', '.').Replace(" ", String.Empty), NumberStyles.Any, new CultureInfo("en-US"), out cost);

                    var field = getField(outcomeForTask[j].Field.Link);
                    var ftp = getFtp(outcomeForTask[j].Ftp.Link, outcomeForTask[j].Field.Link);
                    if (field.HasValue)
                    {
                        var task = new Tasks
                        {
                            fiel_fiel_id = (int)field,
                            shtr_shtr_id = shtr.shtr_id,
                            plant_plant_id = ftp,
                            tptas_tptas_id = 1,
                            consumption = count,
                            fuel_cost = cost
                        };
                        db.Tasks.Add(task);
                    }
                    else
                    {
                        TotalLostSum += cost;

                        Debug.WriteLine(i + " " + outcomeForTask[j].Field.Name + " " + outcomeForTask[j].Field.Link);
                        Debug.WriteLine(i + " " + outcomeForTask[j].Ftp.Name + " " + outcomeForTask[j].Ftp.Link);
                    }
                }
            }
            
            return 0;
        }

        private int? getField(string link)
        {
            try
            {
                var field = (from m in db.Fields
                         where m.code == link
                         select m).First();
                return field.fiel_id;
            }
            catch (Exception)
            {
                return null;
            }
        }

        private int? getFtp(string link, string fieldlink)
        {
            try
            {
                var ftp = (from m in db.Fields_to_plants
                             where m.in_code == link && m.Fields.code == fieldlink
                             select m).First();
                return ftp.fielplan_id;
            }
            catch (Exception)
            {
                return null;
            }
        }

        private Contractor getContractor(Contracotr contractor, int newContrId)
        {
            Contractor contr;
            try
            {
                contr = (from m in db.Contractor
                         where m.code == contractor.Link
                         select m).First();
            }
            catch (Exception)
            {

                contr = new Contractor
                {
                    id = newContrId,
                    name = contractor.Name,
                    code = contractor.Link
                };
                db.Contractor.Add(contr);
            }
            return contr;
        }
    }
}


