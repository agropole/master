﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using LogusSrv.DAL.DBModel;
using LogusSrv.DAL.Entities.DTO;
using LogusSrv.DAL.Entities.DTO.ExportExcel;
using System.IO;
using LogusSrv.DAL.Entities.Res;
using LogusSrv.DAL.Entities.DTO.FileArchive;
using System.Xml.Serialization;
using System.Xml;
using System.Diagnostics;
using System.Globalization;
using System.Data.Entity.Validation;

namespace LogusSrv.DAL.Operations
{
    public class ParseXML
    {
        protected readonly LogusDBEntities db = new LogusDBEntities();
        private decimal TotalLostSum;
        private List<OperationOutcome> LostOperation;

        public string ParseMaterial(FileUploadDto file)
        {
            using (var ms = new MemoryStream(file.contents))
            {
                ms.Seek(0, SeekOrigin.Begin);

                using (var xr = XmlReader.Create(ms))
                {
                    var serializer = new XmlSerializer(typeof(MaterialXML));

                    var obj = (MaterialXML) serializer.Deserialize(xr);
                    using (var trans = db.Database.BeginTransaction())
                    {
                        var outcome = obj.Outcome;
                        var income = obj.Income;

                        var newId = (db.Materials.Max(t => (int?)t.mat_id) ?? 0) + 1;
                        if (income != null)
                        {
                            newId = insertAndRecountIncome(income.Operation, newId);
                        }

                        LostOperation = new List<OperationOutcome>();
                        TotalLostSum = 0;
                        if (outcome != null)
                        {
                            newId = insertAndRecountOutcome(outcome.Operation, newId);
                        }
                        

                        var grouped = (from out1 in LostOperation
                                       group out1 by new { 
                                           linkPlant = out1.Ftp.Link, namePlant = out1.Ftp.Name, linkField = out1.Field.Link, nameField = out1.Field.Name} into k
                                       select new
                                       {
                                           LinkPlant = k.Key.linkPlant,
                                           NamePlant = k.Key.namePlant,
                                           LinkField = k.Key.linkField,
                                           NameField = k.Key.nameField,
                                           Outcome = k.ToList()
                                       }).ToList();

                        for (var i = 0; i < grouped.Count; i++)
                        {
                            Debug.WriteLine(grouped[i].NamePlant + " " + grouped[i].NameField + " " + grouped[i].Outcome.Count);
                        }
                        db.SaveChanges();
                        trans.Commit();
                        return obj.Income.Operation.Count + " " + obj.Outcome.Operation.Count;
                    }
                }
            }
        }


        public int insertAndRecountIncome(List<OperationIncome> income, int newId)
        {
            var newContrId = (db.Contractor.Max(t => (int?)t.id) ?? 0) + 1;
            for (var i = 0; i < income.Count; i++)
            {
                decimal count, cost;

                decimal.TryParse(income[i].Count.Replace(',', '.').Replace(" ", String.Empty), NumberStyles.Any, new CultureInfo("en-US"), out count);
                decimal.TryParse(income[i].Sum.Replace(',', '.').Replace(" ", String.Empty), NumberStyles.Any, new CultureInfo("en-US"), out cost);
                decimal price = cost / count;

                var dateParsed = DateTime.ParseExact(income[i].Date, "dd.MM.yyyy", new CultureInfo("en-US"));

                var mat = getMaterial(income[i].Material, newId);
                if (newId == mat.mat_id)
                {
                    newId++;
                }
                
                var contracotr = getContractor(income[i].Contractor, newContrId);
                if (contracotr.id == newContrId)
                {
                    newContrId++;
                }
                var incomeItem = new Acts_szr_income
                {
                    count = count,
                    cost = cost,
                    price = price,
                    date = dateParsed,
                    mat_id = mat.mat_id,
                    type_mat_id = (int)mat.mat_type_id,
                    id_contractor = contracotr.id,
                };

                var matcurcost = mat.balance * mat.current_price;
                mat.balance += count;
                var newCost = matcurcost + cost;
                var newCurPrice = newCost / mat.balance;
                mat.current_price = newCurPrice;

                db.Acts_szr_income.Add(incomeItem);
            };
            return newId;
        }

        private int insertAndRecountOutcome(List<OperationOutcome> outcome, int newId)
        {
            var act = new Acts_szr
            {
                res_emp_id = 34,
                rec_emp_id = 34,
                org_org_id = 1,
                year = 2015,
                date = new DateTime()
            };
            db.Acts_szr.Add(act);
            db.SaveChanges();

            for (var i = 0; i < outcome.Count; i++)
            {
                var mat = getMaterial(outcome[i].Material, newId);
                if (newId == mat.mat_id)
                {
                    newId++;
                }

                decimal count, cost;
                decimal.TryParse(outcome[i].Count.Replace(',', '.').Replace(" ", String.Empty), NumberStyles.Any, new CultureInfo("en-US"), out count);
                decimal.TryParse(outcome[i].Sum.Replace(',', '.').Replace(" ", String.Empty), NumberStyles.Any, new CultureInfo("en-US"), out cost);

                var fLink = outcome[i].Field.Link;
                var fLink2 = outcome[i].Ftp.Link;
                var ftp = (from f in db.Fields_to_plants
                            where f.Fields.code == fLink && f.in_code == fLink2
                            select f).FirstOrDefault();
                if (ftp != null)
                {
                    var mat_to_ftp = new Materials_to_ftp
                    {
                        mat_mat_id = mat.mat_id,
                        summ = cost,
                        count = count,
                        price = cost / count,
                        szr_szr_id = act.szr_id,
                        consumption_rate = 0,
                        area = 0,
                        ftp_ftp_id = ftp.fielplan_id
                    };
                    db.Materials_to_ftp.Add(mat_to_ftp);
                }
                else
                {
                    TotalLostSum += cost;
                    LostOperation.Add(outcome[i]);
                    Debug.WriteLine(i + " " + outcome[i].Material.Name + " " + outcome[i].Material.Link);
                    Debug.WriteLine(outcome[i].Field.Name + " " + outcome[i].Field.Link);
                    Debug.WriteLine("================================================================");
                }
            }
            return newId;
        }

        private Materials getMaterial(Contracotr materiallink, int newId)
        {
            Materials mat;
            try
            {
                mat = (from m in db.Materials
                       where m.code == materiallink.Link
                       select m).First();
            }
            catch (Exception)
            {

                mat = new Materials
                {
                    mat_id = newId,
                    name = materiallink.Name,
                    description = "СЗР",
                    mat_type_id = 1,
                    cost = 0,
                    code = materiallink.Link,
                    balance = 0,
                    current_price = 0
                };
                db.Materials.Add(mat);
                db.SaveChanges();
            }
            return mat;
        }

        private Contractor getContractor(Contracotr contractor, int newContrId)
        {
            Contractor contr;
            try
            {
                contr = (from m in db.Contractor
                         where m.code == contractor.Link
                        select m).First();
            }
            catch (Exception)
            {

                contr = new Contractor
                {
                    id = newContrId,
                    name = contractor.Name,
                    code = contractor.Link
                };
                db.Contractor.Add(contr);
            }
            return contr;
        }
    }
}


