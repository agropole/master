﻿using System;
using System.Data.Entity;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LogusSrv.CORE.Exceptions;
using LogusSrv.DAL.Entities.DTO.ActsSzr;
using LogusSrv.DAL.Entities.DTO;
using LogusSrv.DAL.Entities.Req;
using LogusSrv.DAL.DBModel;
using LogusSrv.DAL.Utils;
using LogusSrv.DAL.Enums;
using LogusSrv.DAL.Entities.Res;
using System.Xml.Serialization;


namespace LogusSrv.DAL.Operations
{
    [Serializable]
    [XmlRoot("Организация")]
    public class GrossHarvestXML
    {
        [XmlElement("ПОСТУПЛЕНИЕ")]
        public GrossHarvestIn Income { get; set; }
    }

    public class GrossHarvestIn
    {
        [XmlElement("Операция")]
        public List<GrossHarvestOperationIncome> Operation { get; set; }
    }

    public class GrossHarvestOperationIncome: Operation
    {
        [XmlElement("Подразделение")]
        public Contracotr Field { get; set; }
        [XmlElement("КорСубконто1")]
        public Contracotr Ftp { get; set; }
    }
        
}
