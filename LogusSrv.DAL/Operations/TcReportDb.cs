﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using LogusSrv.DAL.DBModel;
using LogusSrv.DAL.Entities.Res;
using OfficeOpenXml;
using OfficeOpenXml.Style;

namespace LogusSrv.DAL.Operations
{
    public class TcReportDb
    {
        protected readonly LogusDBEntities db = new LogusDBEntities();

        public ReportFile DrawTcReport(string templatePath, TCDetailModel res)
        {
            var tmpl = new FileInfo(templatePath);
            using (ExcelPackage pck = new ExcelPackage(tmpl, true))
            {
                ExcelWorksheet wsh = pck.Workbook.Worksheets[1];
                var tc = db.TC.Where(p => p.id == res.TC.Id).FirstOrDefault();
                var SheetName = tc.Plants.name + " " + res.TC.YearId.ToString();
                if (res.TC.SortId != null)
                {
                    SheetName += '(' + tc.Type_sorts_plants.name + ')';
                }
                if (res.TC.FieldId != null)
                {
                    SheetName += '(' + tc.Fields.name + ')';
                }
                wsh.Name = SheetName;

                drawHeader(wsh, res, tc);
                if (res.NZP != null)
                {
                    drawNZP(wsh, res.NZP);
                }
                drawWorks(wsh, res.Works, res.SupportStuff);
                drawSZR(wsh, res.DataSZR);
                drawFertilizer(wsh, res.DataChemicalFertilizers);
                drawSeed(wsh, res.DataSeed);
                drawDopMaterial(wsh, res.DataDopMaterial);
                drawService(wsh, res.DataServices);
                if (res.DataServices != null && res.DataServices.Count > 0) { wsh.Cells["AW16"].Value = res.DataServices.Sum(p => p.Cost); }

                wsh.Cells["AW9"].Calculate();
                wsh.Cells["V5"].Calculate();
                wsh.Cells["AW16"].Calculate();

                wsh.Cells["AW9:AY16"].Style.Numberformat.Format = "#,##0.00";
                wsh.Cells["R5:T6"].Style.Numberformat.Format = "#,##0.00";
                wsh.Cells["V5:X5"].Style.Numberformat.Format = "#,##0.00";

                wsh.Cells.AutoFitColumns();
                wsh.Column(22).Width = 20;

                var r = new ReportFile
                {
                    data = pck.GetAsByteArray(),
                    fileName = "Отчет ТК " + SheetName + ".xlsx"
                };
                return r;
            }
        }

        private void drawNZP(ExcelWorksheet wsh, NZPModel nZPModel)
        {
            float s = 0;
            if (nZPModel.Salary.HasValue)
            {
                wsh.Cells["S16"].Value = nZPModel.Salary.Value;
                s += nZPModel.Salary.Value;
            }
            if (nZPModel.Energy.HasValue)
            {
                wsh.Cells["AA16"].Value = nZPModel.Energy.Value;
                s += nZPModel.Energy.Value;
            }
            if (nZPModel.SZR.HasValue)
            {
                wsh.Cells["AG16"].Value = nZPModel.SZR.Value;
                s += nZPModel.SZR.Value;
            }
            if (nZPModel.ChemicalFertilizers.HasValue)
            {
                wsh.Cells["AL16"].Value = nZPModel.ChemicalFertilizers.Value;
                s += nZPModel.ChemicalFertilizers.Value;
            }
            if (nZPModel.Seed.HasValue)
            {
                wsh.Cells["AQ16"].Value = nZPModel.Seed.Value;
                s += nZPModel.Seed.Value;
            }
            if (nZPModel.DopMaterial.HasValue)
            {
                wsh.Cells["AV16"].Value = nZPModel.DopMaterial.Value;
                s += nZPModel.DopMaterial.Value;
            }
            wsh.Cells["BA10"].Value = s;
        }

        private void drawHeader(ExcelWorksheet wsht, TCDetailModel res, TC tc)
        {
            wsht.Cells["K3"].Value = tc.Plants.name;
            if (res.TC.SortId != null)
            {
                wsht.Cells["K4"].Value = tc.Type_sorts_plants.name;
            }
            if (res.TC.FieldId != null)
            {
                wsht.Cells["D3"].Value = tc.Fields.name;
            }
            wsht.Cells["K5"].Value = res.TC.Area;
            wsht.Cells["S5"].Value = res.TC.Productivity;
            wsht.Cells["T5"].Value = res.TC.GrossHarvest;

            wsht.Cells["C15"].Value = res.TC.YearId - 1;
            wsht.Cells["C17"].Value = res.TC.YearId;
        }

        private void drawWorks(ExcelWorksheet wsht, List<WorkModel> works, List<SupportStaffModel> supportStaff)
        {
            var startRowInd = 18;
            float s1 = 0;
            float s2 = 0;
            float s3 = 0;
            for (var i = 0; i < works.Count; i++)
            {
                var w = works[i];
                var ind = i + startRowInd;

                wsht.Cells['B' + ind.ToString()].Value = (i + 1);
                wsht.Cells['C' + ind.ToString()].Value = w.Name.Name;
                wsht.Cells['D' + ind.ToString()].Value = 0;
                wsht.Cells['E' + ind.ToString()].Value = w.Count;

                wsht.Cells['F' + ind.ToString()].Value = (w.UnicFlag == 0 ? "га" : "ед");
                wsht.Cells['G' + ind.ToString()].Value = w.Workload;

                wsht.Cells['H' + ind.ToString()].Value = w.DateStart.Value;
                wsht.Cells['I' + ind.ToString()].Value = w.DateEnd.Value;
                wsht.Cells['J' + ind.ToString()].Value = (w.DateEnd.Value - w.DateStart.Value).TotalDays + 1;

                string equi = "", traktors = "";

                for (var j = 0; j < w.Equipment.Count; j++)
                {
                    equi += w.Equipment[j].Name + " ";
                }
                wsht.Cells['K' + ind.ToString()].Value = equi;
                for (var j = 0; j < w.Tech.Count; j++)
                {
                    traktors += w.Tech[j].Name + " ";
                }
                wsht.Cells['L' + ind.ToString()].Value = traktors;

                var compos = w.Name.Name;
                if (equi != "")
                {
                    compos += " " + equi;
                    if (traktors != "")
                    {
                        compos += "+" + traktors;
                    }
                }
                else
                {
                    compos += " " + traktors;
                }
                wsht.Cells['N' + ind.ToString()].Value = compos;

                wsht.Cells['O' + ind.ToString()].Value = w.ShiftRate.HasValue ? w.ShiftRate.Value : 0;
                wsht.Cells['P' + ind.ToString()].Value = w.ShiftCount.HasValue ? w.ShiftCount.Value : 0;
                wsht.Cells['Q' + ind.ToString()].Value = 1;
                wsht.Cells['R' + ind.ToString()].Value = w.EmpPrice.HasValue ? w.EmpPrice.Value : 0;
                wsht.Cells['S' + ind.ToString()].Value = w.EmpCost.HasValue ? w.EmpCost.Value : 0;
                s1 += w.EmpCost.Value;

                var support = supportStaff.Where(p => p.index == i+1).FirstOrDefault();
                var rows = w.EmpCost.Value + w.FuelCost.Value;

                if (support != null)
                {
                    wsht.Cells['T' + ind.ToString()].Value = support.Count.Value;
                    wsht.Cells['U' + ind.ToString()].Value = support.Price.Value;
                    wsht.Cells['V' + ind.ToString()].Value = support.Cost.Value;
                    s2 += support.Cost.Value;
                    rows += support.Cost.Value;
                }
                else
                {
                    wsht.Cells['V' + ind.ToString()].Value = 0;
                }

                wsht.Cells['W' + ind.ToString()].Value = w.Consumption.Value;
                if (w.FuelType != null)
                {
                    wsht.Cells['X' + ind.ToString()].Value = w.FuelType.Name;
                }
                wsht.Cells['Y' + ind.ToString()].Value = w.Workload.Value * w.Consumption.Value;
                if (w.FuelType != null)
                {
                    wsht.Cells['Z' + ind.ToString()].Value = w.FuelType.Price.Value;
                }
                wsht.Cells["AA" + ind.ToString()].Value = w.FuelCost.Value;


                wsht.Cells["AB" + ind.ToString()].Value = rows;
                s3 += w.FuelCost.Value;

            }
            wsht.Cells["S17"].Value = s1;
            wsht.Cells["S17"].Style.Numberformat.Format = "#,##0.00";
            wsht.Cells["V17"].Value = s2;
            wsht.Cells["V17"].Style.Numberformat.Format = "#,##0.00";
            wsht.Cells["AA17"].Value = s3;
            wsht.Cells["AA17"].Style.Numberformat.Format = "#,##0.00";
            wsht.Cells["AB17"].Value = s1 + s2 + s3;
            wsht.Cells["AB17"].Style.Numberformat.Format = "#,##0.00";

            wsht.Cells["AW11"].Value = s1 + s2;
            wsht.Cells["AW12"].Value = s3;

            wsht.Cells["R17:S" + (startRowInd + works.Count - 1)].Style.Numberformat.Format = "#,##0.00";
            wsht.Cells["U17:V" + (startRowInd + works.Count - 1)].Style.Numberformat.Format = "#,##0.00";
            wsht.Cells["Z17:AB" + (startRowInd + works.Count - 1)].Style.Numberformat.Format = "#,##0.00";

            if (works.Count != 0)
            {
                setColor(wsht, "B" + startRowInd.ToString() + ":G" + (startRowInd + works.Count - 1).ToString(), "#DDEBF7");
                setColor(wsht, "H" + startRowInd.ToString() + ":J" + (startRowInd + works.Count - 1).ToString(), "#BDD7EE");
                setColor(wsht, "K" + startRowInd.ToString() + ":N" + (startRowInd + works.Count - 1).ToString(), "#9BC2E6");
                setColor(wsht, "O" + startRowInd.ToString() + ":V" + (startRowInd + works.Count - 1).ToString(), "#FFF2CC");
                setColor(wsht, "W" + startRowInd.ToString() + ":AA" + (startRowInd + works.Count - 1).ToString(), "#FCE4D6");
                setBorder(wsht.Cells["B" + startRowInd + ":AS" + (startRowInd + works.Count - 1)].Style.Border);
            }
        }

        private void drawSZR(ExcelWorksheet wsh, List<SZRModel> list)
        {
            var colors = new List<string> { "#A9E2F3", "#81DAF5", "#58D3F7", "#2ECCFA" };
            var startRowInd = 18;
            int workid = 0;
            var startPaintInt = 18;
            WorkModel w = new WorkModel();
            float s = 0;
            for (var i = 0; i < list.Count; i++)
            {
                if (workid != list[i].NameSelectedWorkForMaterial.Id.Value)
                {
                    if (i != 0)
                    {
                        setColor(wsh, "AC" + startPaintInt.ToString() + ":AG" + (startRowInd - 1).ToString(), colors[0]);
                        colors.Add(colors[0]);
                        colors.RemoveAt(0);
                    }
                    startPaintInt = startRowInd;
                    workid = list[i].NameSelectedWorkForMaterial.Id.Value;
                    wsh.Cells["AC" + startRowInd.ToString()].Value = list[i].NameSelectedWorkForMaterial.Name;
                    wsh.Cells["AC" + startRowInd.ToString()].Style.Font.Bold = true;
                    startRowInd++;
                }
                var m = list[i];
                wsh.Cells["AC" + startRowInd.ToString()].Value = m.NameSZR.Name;
                wsh.Cells["AD" + startRowInd.ToString()].Value = m.ConsumptionRate;
                wsh.Cells["AE" + startRowInd.ToString()].Value = m.MaterialArea;
                wsh.Cells["AF" + startRowInd.ToString()].Value = m.Price;
                wsh.Cells["AG" + startRowInd.ToString()].Value = m.Cost.Value;
                s += m.Cost.Value;
                startRowInd++;
            }
            wsh.Cells["AG17"].Value = s;
            if (startRowInd > startPaintInt)
            {
                setColor(wsh, "AC" + startPaintInt.ToString() + ":AG" + (startRowInd - 1).ToString(), colors[0]);
                colors.Add(colors[0]);
                colors.RemoveAt(0);

              //  wsh.Cells["AW13"].Value = s;
                wsh.Cells["AF17:AG" + (startRowInd + list.Count - 1)].Style.Numberformat.Format = "#,##0.00";
                setBorder(wsh.Cells["B17:AX" + (startRowInd - 1)].Style.Border);
            }
        }

        private void drawFertilizer(ExcelWorksheet wsh, List<FertilizerModel> list)
        {
            var colors = new List<string> { "#F3E2A9", "#F5DA81", "#F7D358", "#FACC2E" };
            var startRowInd = 18;
            int workid = 0;
            WorkModel w = new WorkModel();
            var startPaintInt = 18;
            float s = 0;
            for (var i = 0; i < list.Count; i++)
            {
                if (workid != list[i].NameSelectedWorkForMaterial.Id.Value)
                {
                    if (i != 0)
                    {
                        setColor(wsh, "AH" + startPaintInt.ToString() + ":AL" + (startRowInd - 1).ToString(), colors[0]);
                        colors.Add(colors[0]);
                        colors.RemoveAt(0);
                    }
                    workid = list[i].NameSelectedWorkForMaterial.Id.Value;
                    wsh.Cells["AH" + startRowInd.ToString()].Value = list[i].NameSelectedWorkForMaterial.Name;
                    wsh.Cells["AH" + startRowInd.ToString()].Style.Font.Bold = true;
                    startRowInd++;
                }
                var m = list[i];
                wsh.Cells["AH" + startRowInd.ToString()].Value = m.NameFertilizer.Name;
                wsh.Cells["AI" + startRowInd.ToString()].Value = m.ConsumptionRate;
                wsh.Cells["AJ" + startRowInd.ToString()].Value = m.MaterialArea;
                wsh.Cells["AK" + startRowInd.ToString()].Value = m.Price;
                wsh.Cells["AL" + startRowInd.ToString()].Value = m.Cost;
                s += m.Cost.Value;
                startRowInd++;
            }
            wsh.Cells["AL17"].Value = s;
            if (startRowInd > startPaintInt)
            {
                setColor(wsh, "AH" + startPaintInt.ToString() + ":AL" + (startRowInd - 1).ToString(), colors[0]);
                colors.Add(colors[0]);
                colors.RemoveAt(0);

           //     wsh.Cells["AW14"].Value = s;
                wsh.Cells["AK17:AL" + (startRowInd + list.Count - 1)].Style.Numberformat.Format = "#,##0.00";
                setBorder(wsh.Cells["B17:AX" + (startRowInd - 1)].Style.Border);
            }
        }

        private void drawSeed(ExcelWorksheet wsh, List<SeedModel> list)
        {
            var colors = new List<string> { "#F6CED8", "#F5A9BC", "#F7819F", "#FA5882" };
            var startRowInd = 18;
            int workid = 0;
            WorkModel w = new WorkModel();
            var startPaintInt = 18;
            float s = 0;
            for (var i = 0; i < list.Count; i++)
            {
                if (workid != list[i].NameSelectedWorkForMaterial.Id.Value)
                {
                    if (i != 0)
                    {
                        setColor(wsh, "AM" + startPaintInt.ToString() + ":AQ" + (startRowInd - 1).ToString(), colors[0]);
                        colors.Add(colors[0]);
                        colors.RemoveAt(0);
                    }
                    workid = list[i].NameSelectedWorkForMaterial.Id.Value;
                    wsh.Cells["AM" + startRowInd.ToString()].Value = list[i].NameSelectedWorkForMaterial.Name;
                    wsh.Cells["AM" + startRowInd.ToString()].Style.Font.Bold = true;
                    startRowInd++;
                }
                var m = list[i];
                wsh.Cells["AM" + startRowInd.ToString()].Value = m.NameSeed.Name;
                wsh.Cells["AN" + startRowInd.ToString()].Value = m.ConsumptionRate;
                wsh.Cells["AO" + startRowInd.ToString()].Value = m.MaterialArea;
                wsh.Cells["AP" + startRowInd.ToString()].Value = m.Price;
                wsh.Cells["AQ" + startRowInd.ToString()].Value = m.Cost.Value;
                s += m.Cost.Value;
                startRowInd++;
            }
            wsh.Cells["AQ17"].Value = s;
            if (startRowInd > startPaintInt)
            {
                setColor(wsh, "AM" + startPaintInt.ToString() + ":AQ" + (startRowInd - 1).ToString(), colors[0]);
                colors.Add(colors[0]);
                colors.RemoveAt(0);
                wsh.Cells["AP17:AQ" + (startRowInd + list.Count - 1)].Style.Numberformat.Format = "#,##0.00";
              //  wsh.Cells["AW15"].Value = s;
                setBorder(wsh.Cells["B17:AX" + (startRowInd - 1)].Style.Border);
            }
        }
        //
        private void drawDopMaterial(ExcelWorksheet wsh, List<DopMaterialsModel> list)
        {
            var colors = new List<string> { "#F6CED8", "#F5A9BC", "#F7819F", "#FA5882" };
            var startRowInd = 18;
            int workid = 0;
            WorkModel w = new WorkModel();
            var startPaintInt = 18;
            float s = 0;
            for (var i = 0; i < list.Count; i++)
            {
                if (workid != list[i].NameSelectedWorkForMaterial.Id.Value)
                {
                    if (i != 0)
                    {
                        setColor(wsh, "AR" + startPaintInt.ToString() + ":AV" + (startRowInd - 1).ToString(), colors[0]);
                        colors.Add(colors[0]);
                        colors.RemoveAt(0);
                    }
                    workid = list[i].NameSelectedWorkForMaterial.Id.Value;
                    wsh.Cells["AR" + startRowInd.ToString()].Value = list[i].NameSelectedWorkForMaterial.Name;
                    wsh.Cells["AR" + startRowInd.ToString()].Style.Font.Bold = true;
                    startRowInd++;
                }
                var m = list[i];
                wsh.Cells["AR" + startRowInd.ToString()].Value = m.NameDopMaterial.Name;
                wsh.Cells["AS" + startRowInd.ToString()].Value = m.ConsumptionRate;
                wsh.Cells["AT" + startRowInd.ToString()].Value = m.MaterialArea;
                wsh.Cells["AU" + startRowInd.ToString()].Value = m.Price;
                wsh.Cells["AV" + startRowInd.ToString()].Value = m.Cost.Value;
                s += m.Cost.Value;
                startRowInd++;
            }
            wsh.Cells["AV17"].Value = s;
            if (startRowInd > startPaintInt)
            {
                setColor(wsh, "AR" + startPaintInt.ToString() + ":AV" + (startRowInd - 1).ToString(), colors[0]);
                colors.Add(colors[0]);
                colors.RemoveAt(0);
                wsh.Cells["AU17:AV" + (startRowInd + list.Count - 1)].Style.Numberformat.Format = "#,##0.00";
            //    wsh.Cells["AW16"].Value = s;
                setBorder(wsh.Cells["B17:AX" + (startRowInd - 1)].Style.Border);
            }
        }
        //
        private void drawService(ExcelWorksheet wsh, List<DataService> list)
        {
            var colors = new List<string> { "#F6CED8", "#F5A9BC", "#F7819F", "#FA5882" };
            var startRowInd = 18;
            int workid = 0;
            WorkModel w = new WorkModel();
            var startPaintInt = 18;
            float s = 0;
            for (var i = 0; i < list.Count; i++)
            {
                if (workid != list[i].Id)
                {
                    if (i != 0)
                    {
                        setColor(wsh, "AW" + startPaintInt.ToString() + ":AX" + (startRowInd - 1).ToString(), colors[0]);
                        colors.Add(colors[0]);
                        colors.RemoveAt(0);
                    }
                    workid = (int)list[i].Id;
                }
                var m = list[i];
                wsh.Cells["AW" + startRowInd.ToString()].Value = m.NameService.Name;
                wsh.Cells["AX" + startRowInd.ToString()].Value = m.Cost;
                s += m.Cost.Value;
                startRowInd++;
            }
            wsh.Cells["AX17"].Value = s;
            if (startRowInd > startPaintInt)
            {
                setColor(wsh, "AW" + startPaintInt.ToString() + ":AX" + (startRowInd - 1).ToString(), colors[0]);
                colors.Add(colors[0]);
                colors.RemoveAt(0);
                wsh.Cells["AU17:AV" + (startRowInd + list.Count - 1)].Style.Numberformat.Format = "#,##0.00";
               // wsh.Cells["AW16"].Value = s;
                setBorder(wsh.Cells["B17:AX" + (startRowInd - 1)].Style.Border);
            }
        }
        //
     


        ///
        private void setColor(ExcelWorksheet wsht, string rangePath, string color)
        {
            using (ExcelRange rng = wsht.Cells[rangePath])
            {
                Color colFromHex = System.Drawing.ColorTranslator.FromHtml(color);
                SetCellStyle(rng, colFromHex);
            }
        }

        private void SetCellStyle(ExcelRange cell, Color color)
        {
            cell.Style.Fill.PatternType = ExcelFillStyle.Solid;
            cell.Style.Fill.BackgroundColor.SetColor(color);
        }

        public byte[] DrawTcTotalReport(string templatePath, int year)
        {
            var tmpl = new FileInfo(templatePath);
            using (ExcelPackage pck = new ExcelPackage(tmpl, true))
            {

                var count = db.TC.Where(t => t.year == year).Count();
                if (count != 0)
                {
                    drawTotalList(year, pck.Workbook.Worksheets[1]);
                    drawMaterial(year, pck.Workbook.Worksheets[2], 1);
                    drawMaterial(year, pck.Workbook.Worksheets[3], 2);
                    drawMaterial(year, pck.Workbook.Worksheets[4], 4);
                    drawMaterial(year, pck.Workbook.Worksheets[5], 5);
                }

                return pck.GetAsByteArray();
            }
        }

        private void drawTotalList(int year, ExcelWorksheet wsh)
        {
            var res = new List<TCTotalItem>();
            res = (from t in db.TC
                   where t.year == year

                   let tcparValue = t.TC_param_value.Where(x => x.id_tc_param == 4).Select(x => x.value).FirstOrDefault()
                   let tcArea = t.TC_param_value.Where(x => x.id_tc_param == 1).Select(x => x.value).FirstOrDefault()
                   let tcGH = t.TC_param_value.Where(x => x.id_tc_param == 3).Select(x => x.value).FirstOrDefault()

                   orderby t.Plants.name

                   select new TCTotalItem
                   {
                       Id = t.id,
                       Plant = t.Plants.name,
                       Sort = t.Type_sorts_plants.name,
                       Field = t.Fields.name,
                       Cost = tcparValue ?? 0,
                       Area = (float?)tcArea ?? 0,
                       GrossHarvest = (float?)tcGH ?? 0
                   }).ToList();

            wsh.Cells["F3"].Value = year;
            var startRow = 8;
            double s = 0;
            for (var i = 0; i < res.Count; i++)
            {
                var ind = startRow + i;
                var tc = res[i];
                wsh.Cells["A" + ind.ToString()].Value = (i + 1);
                wsh.Cells["B" + ind.ToString()].Value = tc.Plant;
                wsh.Cells["C" + ind.ToString()].Value = tc.Sort;
                wsh.Cells["D" + ind.ToString()].Value = tc.Field;
                wsh.Cells["E" + ind.ToString()].Value = tc.Area;
                wsh.Cells["F" + ind.ToString()].Value = tc.GrossHarvest;
                wsh.Cells["G" + ind.ToString()].Value = tc.Cost.Value;
                s += tc.Cost.Value;
            }
            wsh.Cells["G" + (startRow - 1)].Value = s;
            wsh.Cells["G" + startRow].Style.Numberformat.Format = "#,##0.00";
            wsh.Cells["B" + startRow + ":G" + (startRow + res.Count - 1)].Style.Numberformat.Format = "#,##0.00";
            setBorder(wsh.Cells["A" + startRow + ":G" + (startRow + res.Count - 1)].Style.Border);
            wsh.Cells.AutoFitColumns();
        }

        private void setBorder(Border modelTable)
        {
            modelTable.Top.Style = ExcelBorderStyle.Thin;
            modelTable.Left.Style = ExcelBorderStyle.Thin;
            modelTable.Right.Style = ExcelBorderStyle.Thin;
            modelTable.Bottom.Style = ExcelBorderStyle.Thin;
        }

        public void drawMaterial(int year, ExcelWorksheet wsh, int type)
        {
            wsh.Cells["E3"].Value = year;

            var res1 = (from p in db.TC_param_value
                        where (p.id_tc_param == 42 || p.id_tc_param == 44 || p.id_tc_param == 49 || p.id_tc_param == 48) && p.Materials.mat_type_id == type && p.TC.year == year
                        && p.TC.plant_plant_id == p.TC.Plants.plant_id

                        group new { p.value, p.id_tc_param, p.TC.Plants.name } by new { p.mat_mat_id, p.Materials.name, p.id_tc_operation, name1 = p.TC.Plants.name } into g1

                        orderby g1.Key.name

                        select new
                        {
                            Name = g1.Key.name,
                            Price = g1.Where(v => v.id_tc_param == 44).Sum(v => v.value) / g1.Where(v => v.id_tc_param == 44).Count(),
                            Area = g1.Where(v => v.id_tc_param == 49).Sum(v => v.value),
                            Consumprion = g1.Where(v => v.id_tc_param == 48).Sum(v => v.value),
                            Cost = g1.Where(v => v.id_tc_param == 42).Sum(v => v.value),
                            Culture = g1.Key.name1,

                        }).ToList();

            var startRow = 8;
            double? s = 0;
            for (var i = 0; i < res1.Count; i++)
            {
                var ind = startRow + i;
                var m = res1[i];
                wsh.Cells["A" + ind.ToString()].Value = (i + 1);
                wsh.Cells["B" + ind.ToString()].Value = m.Name;
                wsh.Cells["C" + ind.ToString()].Value = m.Culture;
                wsh.Cells["D" + ind.ToString()].Value = m.Area * m.Consumprion;
                wsh.Cells["E" + ind.ToString()].Value = m.Price;
                wsh.Cells["F" + ind.ToString()].Value = m.Cost;
                s += m.Cost;
            }

            wsh.Cells["G" + startRow].Style.Numberformat.Format = "#,##0.00";
            if (res1.Count != 0)
            {
                wsh.Cells["A" + startRow + ":A" + (startRow + res1.Count - 1)].Style.Numberformat.Format = "#,##0";
                wsh.Cells["B" + startRow + ":F" + (startRow + res1.Count - 1)].Style.Numberformat.Format = "#,##0.00";
                setBorder(wsh.Cells["A" + startRow + ":F" + (startRow + res1.Count - 1)].Style.Border);
            }
            wsh.Cells["F" + (startRow - 1)].Value = s;
            wsh.Cells["F" + (startRow - 1)].Style.Numberformat.Format = "#,##0.00";
            wsh.Cells.AutoFitColumns();
        }

        /////////////////////////////////////111111111111111111111111
        ///////////////////--------------11111111111111111111111111111
        public ReportFile DrawTcReport1(string templatePath, TCDetailModel res)
        {
            var tmpl = new FileInfo(templatePath);
            using (ExcelPackage pck = new ExcelPackage(tmpl, true))
            {
                ExcelWorksheet wsh = pck.Workbook.Worksheets[1];
                var tc = db.TC.Where(p => p.id == res.TC.Id).FirstOrDefault();
                var SheetName = tc.Plants.name + " " + res.TC.YearId.ToString();
                if (res.TC.SortId != null)
                {
                    SheetName += '(' + tc.Type_sorts_plants.name + ')';
                }
                if (res.TC.FieldId != null)
                {
                    SheetName += '(' + tc.Fields.name + ')';
                }
                wsh.Name = SheetName;

                drawHeader1(wsh, res, tc);
                drawWorks1(wsh, res.Works, res.SupportStuff);
                drawSZR1(wsh, res.DataSZR);
                drawFertilizer1(wsh, res.DataChemicalFertilizers);
                drawSeed1(wsh, res.DataSeed);
                drawDopMaterial1(wsh, res.DataDopMaterial);
                drawService1(wsh, res.DataServices);
                if (res.DataServices != null && res.DataServices.Count > 0) { wsh.Cells["AW16"].Value = res.DataServices.Sum(p => p.Cost); }

                wsh.Cells["AW9"].Calculate();
                wsh.Cells["V5"].Calculate();
                wsh.Cells["AW16"].Calculate();

             //   wsh.Cells["AW9:AY16"].Style.Numberformat.Format = "#,##0.00";
                wsh.Cells["R5:T6"].Style.Numberformat.Format = "#,##0.00";
                wsh.Cells["V5:X5"].Style.Numberformat.Format = "#,##0.00";

                wsh.Cells.AutoFitColumns();
                wsh.Column(22).Width = 20;

                var r = new ReportFile
                {
                    data = pck.GetAsByteArray(),
                    fileName = "Отчет ТК " + SheetName + ".xlsx"
                };
                return r;
            }
        }

    

        private void drawHeader1(ExcelWorksheet wsht, TCDetailModel res, TC tc)
        {
            wsht.Cells["b3"].Value = tc.Plants.name;
            if (res.TC.SortId != null)
            {
                wsht.Cells["c3"].Value = tc.Type_sorts_plants.name;
            }
            //if (res.TC.FieldId != null)
            //{
            //    wsht.Cells["D3"].Value = tc.Fields.name;
            //}
        //    wsht.Cells["K5"].Value = res.TC.Area;
          //  wsht.Cells["S5"].Value = res.TC.Productivity;
        //    wsht.Cells["M5"].Value = res.TC.GrossHarvest;

         //   wsht.Cells["C15"].Value = res.TC.YearId - 1;
         //   wsht.Cells["C17"].Value = res.TC.YearId;
        }

        private void drawWorks1(ExcelWorksheet wsht, List<WorkModel> works, List<SupportStaffModel> supportStaff)
        {
            var startRowInd = 9;
            float s1 = 0;
            float s2 = 0;
            for (var i = 0; i < works.Count; i++)
            {
                var w = works[i];
                var ind = i + startRowInd;

                wsht.Cells['B' + ind.ToString()].Value = (i + 1);
                wsht.Cells['C' + ind.ToString()].Value = w.Name.Name;

                string str = null;
                if (w.UnicFlag == 0) { str = "По площади"; }
                if (w.UnicFlag == 1) { str = "Штучные(по сменам)"; }
                if (w.UnicFlag == 2) { str = "Штучные(по объему)"; }
                wsht.Cells['D' + ind.ToString()].Value = str;
                wsht.Cells['E' + ind.ToString()].Value = w.Count;
                wsht.Cells['F' + ind.ToString()].Value = w.Factor;
                if (w.UnicFlag != 0)
                {
                    wsht.Cells['G' + ind.ToString()].Value = w.Workload;
                }
                wsht.Cells['H' + ind.ToString()].Value = w.DateStart.Value;
                wsht.Cells['I' + ind.ToString()].Value = w.DateEnd.Value;
                wsht.Cells['J' + ind.ToString()].Value = (w.DateEnd.Value - w.DateStart.Value).TotalDays + 1;

                string equi = "", traktors = "";

                for (var j = 0; j < w.Equipment.Count; j++)
                {
                    equi += w.Equipment[j].Name + " ";
                }
                wsht.Cells['J' + ind.ToString()].Value = equi;
                for (var j = 0; j < w.Tech.Count; j++)
                {
                    traktors += w.Tech[j].Name + " ";
                }
                wsht.Cells['K' + ind.ToString()].Value = traktors;

           
                s1 += w.EmpCost.Value;

                var support = supportStaff.Where(p => p.index == i + 1).FirstOrDefault();
                var rows = w.EmpCost.Value + w.FuelCost.Value;

                if (support != null)
                {
                    wsht.Cells['L' + ind.ToString()].Value = support.Count.Value;
                    wsht.Cells['M' + ind.ToString()].Value = support.Price.Value;
                    s2 += support.Cost.Value;
                    rows += support.Cost.Value;
                }
            


            }
      
        }

        private void drawSZR1(ExcelWorksheet wsh, List<SZRModel> list)
        {
            var colors = new List<string> { "#A9E2F3", "#81DAF5", "#58D3F7", "#2ECCFA" };
            var startRowInd = 9;
            int workid = 0;
            var startPaintInt = 9;
            WorkModel w = new WorkModel();
            float s = 0;
            for (var i = 0; i < list.Count; i++)
            {
                if (workid != list[i].NameSelectedWorkForMaterial.Id.Value)
                {
                    if (i != 0)
                    {
                        setColor1(wsh, "n" + startPaintInt.ToString() + ":o" + (startRowInd - 1).ToString(), colors[0]);
                        colors.Add(colors[0]);
                        colors.RemoveAt(0);
                    }
                    startPaintInt = startRowInd;
                    workid = list[i].NameSelectedWorkForMaterial.Id.Value;
                    wsh.Cells["n" + startRowInd.ToString()].Value = list[i].NameSelectedWorkForMaterial.Name;
                    wsh.Cells["n" + startRowInd.ToString()].Style.Font.Bold = true;
                    startRowInd++;
                }
                var m = list[i];
                wsh.Cells["n" + startRowInd.ToString()].Value = m.NameSZR.Name;
                wsh.Cells["o" + startRowInd.ToString()].Value = m.ConsumptionRate;
              //  wsh.Cells["p" + startRowInd.ToString()].Value = m.MaterialArea;
              //  wsh.Cells["AF" + startRowInd.ToString()].Value = m.Price;
              //  wsh.Cells["AG" + startRowInd.ToString()].Value = m.Cost.Value;
           //     s += m.Cost.Value;
                startRowInd++;
            }
          //  wsh.Cells["AG17"].Value = s;
            if (startRowInd > startPaintInt)
            {
                //setColor1(wsh, "n" + startPaintInt.ToString() + ":o" + (startRowInd - 1).ToString(), colors[0]);
                //colors.Add(colors[0]);
                //colors.RemoveAt(0);

             //   wsh.Cells["AW13"].Value = s;
               wsh.Cells["c9:g" + (startRowInd + list.Count - 1)].Style.Numberformat.Format = "#,##0.00";
               setBorder1(wsh.Cells["B9:w" + (startRowInd - 1)].Style.Border);
            }
        }

        private void drawFertilizer1(ExcelWorksheet wsh, List<FertilizerModel> list)
        {
            var colors = new List<string> { "#A9E2F3", "#81DAF5", "#58D3F7", "#2ECCFA" };
            var startRowInd = 9;
            int workid = 0;
            WorkModel w = new WorkModel();
            var startPaintInt = 9;
            float s = 0;
            for (var i = 0; i < list.Count; i++)
            {
                if (workid != list[i].NameSelectedWorkForMaterial.Id.Value)
                {
                    if (i != 0)
                    {
                        setColor1(wsh, "p" + startPaintInt.ToString() + ":q" + (startRowInd - 1).ToString(), colors[0]);
                        colors.Add(colors[0]);
                        colors.RemoveAt(0);
                    }
                    workid = list[i].NameSelectedWorkForMaterial.Id.Value;
                    wsh.Cells["p" + startRowInd.ToString()].Value = list[i].NameSelectedWorkForMaterial.Name;
                    wsh.Cells["p" + startRowInd.ToString()].Style.Font.Bold = true;
                    startRowInd++;
                }
                var m = list[i];
                wsh.Cells["p" + startRowInd.ToString()].Value = m.NameFertilizer.Name;
                wsh.Cells["q" + startRowInd.ToString()].Value = m.ConsumptionRate;
             //   wsh.Cells["s" + startRowInd.ToString()].Value = m.MaterialArea;
             //   wsh.Cells["AK" + startRowInd.ToString()].Value = m.Price;
             //   wsh.Cells["AL" + startRowInd.ToString()].Value = m.Cost;
          //      s += m.Cost.Value;
                startRowInd++;
            }
         //   wsh.Cells["AL17"].Value = s;
            if (startRowInd > startPaintInt)
            {
                //setColor1(wsh, "p" + startPaintInt.ToString() + ":q" + (startRowInd - 1).ToString(), colors[0]);
                //colors.Add(colors[0]);
                //colors.RemoveAt(0);

                //     wsh.Cells["AW14"].Value = s;
                wsh.Cells["j9:w" + (startRowInd + list.Count - 1)].Style.Numberformat.Format = "#,##0.00";
                setBorder1(wsh.Cells["B9:w" + (startRowInd - 1)].Style.Border);
            }
        }

        private void drawSeed1(ExcelWorksheet wsh, List<SeedModel> list)
        {
            var colors = new List<string> { "#A9E2F3", "#81DAF5", "#58D3F7", "#2ECCFA" };
            var startRowInd = 9;
            int workid = 0;
            WorkModel w = new WorkModel();
            var startPaintInt = 9;
            float s = 0;
            for (var i = 0; i < list.Count; i++)
            {
                if (workid != list[i].NameSelectedWorkForMaterial.Id.Value)
                {
                    if (i != 0)
                    {
                        //setColor1(wsh, "r" + startPaintInt.ToString() + ":s" + (startRowInd - 1).ToString(), colors[0]);
                        //colors.Add(colors[0]);
                        //colors.RemoveAt(0);
                    }
                    workid = list[i].NameSelectedWorkForMaterial.Id.Value;
                    wsh.Cells["r" + startRowInd.ToString()].Value = list[i].NameSelectedWorkForMaterial.Name;
                    wsh.Cells["r" + startRowInd.ToString()].Style.Font.Bold = true;
                    startRowInd++;
                }
                var m = list[i];
                wsh.Cells["r" + startRowInd.ToString()].Value = m.NameSeed.Name;
                wsh.Cells["s" + startRowInd.ToString()].Value = m.ConsumptionRate;
             //   wsh.Cells["w" + startRowInd.ToString()].Value = m.MaterialArea;
            //    wsh.Cells["AP" + startRowInd.ToString()].Value = m.Price;
            //    wsh.Cells["AQ" + startRowInd.ToString()].Value = m.Cost.Value;
                s += m.Cost.Value;
                startRowInd++;
            }
        //    wsh.Cells["AQ17"].Value = s;
            if (startRowInd > startPaintInt)
            {
                //setColor(wsh, "r" + startPaintInt.ToString() + ":s" + (startRowInd - 1).ToString(), colors[0]);
                //colors.Add(colors[0]);
                //colors.RemoveAt(0);
                wsh.Cells["j9:w" + (startRowInd + list.Count - 1)].Style.Numberformat.Format = "#,##0.00";
                //  wsh.Cells["AW15"].Value = s;
                setBorder(wsh.Cells["B9:w" + (startRowInd - 1)].Style.Border);
            }
        }
        //
        private void drawDopMaterial1(ExcelWorksheet wsh, List<DopMaterialsModel> list)
        {
            var colors = new List<string> { "#A9E2F3", "#81DAF5", "#58D3F7", "#2ECCFA" };
            var startRowInd = 9;
            int workid = 0;
            WorkModel w = new WorkModel();
            var startPaintInt = 9;
            float s = 0;
            for (var i = 0; i < list.Count; i++)
            {
                if (workid != list[i].NameSelectedWorkForMaterial.Id.Value)
                {
                    if (i != 0)
                    {
                        //setColor(wsh, "t" + startPaintInt.ToString() + ":u" + (startRowInd - 1).ToString(), colors[0]);
                        //colors.Add(colors[0]);
                        //colors.RemoveAt(0);
                    }
                    workid = list[i].NameSelectedWorkForMaterial.Id.Value;
                    wsh.Cells["t" + startRowInd.ToString()].Value = list[i].NameSelectedWorkForMaterial.Name;
                    wsh.Cells["t" + startRowInd.ToString()].Style.Font.Bold = true;
                    startRowInd++;
                }
                var m = list[i];
                wsh.Cells["t" + startRowInd.ToString()].Value = m.NameDopMaterial.Name;
                wsh.Cells["u" + startRowInd.ToString()].Value = m.ConsumptionRate;
              //  wsh.Cells["M" + startRowInd.ToString()].Value = m.MaterialArea;
             //   wsh.Cells["AU" + startRowInd.ToString()].Value = m.Price;
             //   wsh.Cells["AV" + startRowInd.ToString()].Value = m.Cost.Value;
                s += m.Cost.Value;
                startRowInd++;
            }
         //   wsh.Cells["AV17"].Value = s;
            if (startRowInd > startPaintInt)
            {
                //setColor(wsh, "t" + startPaintInt.ToString() + ":u" + (startRowInd - 1).ToString(), colors[0]);
                //colors.Add(colors[0]);
                //colors.RemoveAt(0);
                wsh.Cells["j9:w" + (startRowInd + list.Count - 1)].Style.Numberformat.Format = "#,##0.00";
                //    wsh.Cells["AW16"].Value = s;
                setBorder(wsh.Cells["B9:w" + (startRowInd - 1)].Style.Border);
            }
        }
        //
        private void drawService1(ExcelWorksheet wsh, List<DataService> list)
        {
            var colors = new List<string> { "#A9E2F3", "#81DAF5", "#58D3F7", "#2ECCFA" };
            var startRowInd = 9;
            int workid = 0;
            WorkModel w = new WorkModel();
            var startPaintInt = 9;
            float s = 0;
            for (var i = 0; i < list.Count; i++)
            {
                if (workid != list[i].Id)
                {
                    if (i != 0)
                    {
                        //setColor1(wsh, "v" + startPaintInt.ToString() + ":w" + (startRowInd - 1).ToString(), colors[0]);
                        //colors.Add(colors[0]);
                        //colors.RemoveAt(0);
                    }
                    workid = (int)list[i].Id;
                }
                var m = list[i];
                wsh.Cells["V" + startRowInd.ToString()].Value = m.NameService.Name;
                wsh.Cells["W" + startRowInd.ToString()].Value = m.Cost;
                s += m.Cost.Value;
                startRowInd++;
            }
          //  wsh.Cells["AX17"].Value = s;
            if (startRowInd > startPaintInt)
            {
                //setColor1(wsh, "v" + startPaintInt.ToString() + ":w" + (startRowInd - 1).ToString(), colors[0]);
                //colors.Add(colors[0]);
                //colors.RemoveAt(0);
                wsh.Cells["j9:w" + (startRowInd + list.Count - 1)].Style.Numberformat.Format = "#,##0.00";
                // wsh.Cells["AW16"].Value = s;
                setBorder1(wsh.Cells["B9:w" + (startRowInd - 1)].Style.Border);
            }
        }
        //



        ///
        private void setColor1(ExcelWorksheet wsht, string rangePath, string color)
        {
            using (ExcelRange rng = wsht.Cells[rangePath])
            {
                Color colFromHex = System.Drawing.ColorTranslator.FromHtml(color);
                SetCellStyle1(rng, colFromHex);
            }
        }

        private void SetCellStyle1(ExcelRange cell, Color color)
        {
            cell.Style.Fill.PatternType = ExcelFillStyle.Solid;
            cell.Style.Fill.BackgroundColor.SetColor(color);
        }

        public byte[] DrawTcTotalReport1(string templatePath, int year)
        {
            var tmpl = new FileInfo(templatePath);
            using (ExcelPackage pck = new ExcelPackage(tmpl, true))
            {
                drawTotalList1(year, pck.Workbook.Worksheets[1]);
                drawMaterial1(year, pck.Workbook.Worksheets[2], 1);
                drawMaterial1(year, pck.Workbook.Worksheets[3], 2);
                drawMaterial1(year, pck.Workbook.Worksheets[4], 4);
                drawMaterial1(year, pck.Workbook.Worksheets[5], 5);
                return pck.GetAsByteArray();
            }
        }

        private void drawTotalList1(int year, ExcelWorksheet wsh)
        {
            var res = new List<TCTotalItem>();
            res = (from t in db.TC
                   where t.year == year

                   let tcparValue = t.TC_param_value.Where(x => x.id_tc_param == 4).Select(x => x.value).FirstOrDefault()
                   let tcArea = t.TC_param_value.Where(x => x.id_tc_param == 1).Select(x => x.value).FirstOrDefault()
                   let tcGH = t.TC_param_value.Where(x => x.id_tc_param == 3).Select(x => x.value).FirstOrDefault()

                   orderby t.Plants.name

                   select new TCTotalItem
                   {
                       Id = t.id,
                       Plant = t.Plants.name,
                       Sort = t.Type_sorts_plants.name,
                       Field = t.Fields.name,
                       Cost = tcparValue ?? 0,
                       Area = (float?)tcArea ?? 0,
                       GrossHarvest = (float?)tcGH ?? 0
                   }).ToList();

            wsh.Cells["F3"].Value = year;
            var startRow = 8;
            double s = 0;
            for (var i = 0; i < res.Count; i++)
            {
                var ind = startRow + i;
                var tc = res[i];
                wsh.Cells["A" + ind.ToString()].Value = (i + 1);
                wsh.Cells["B" + ind.ToString()].Value = tc.Plant;
                wsh.Cells["C" + ind.ToString()].Value = tc.Sort;
                wsh.Cells["D" + ind.ToString()].Value = tc.Field;
                wsh.Cells["E" + ind.ToString()].Value = tc.Area;
                wsh.Cells["F" + ind.ToString()].Value = tc.GrossHarvest;
                wsh.Cells["G" + ind.ToString()].Value = tc.Cost.Value;
                s += tc.Cost.Value;
            }
            wsh.Cells["G" + (startRow - 1)].Value = s;
            wsh.Cells["G" + startRow].Style.Numberformat.Format = "#,##0.00";
          //  wsh.Cells["B" + startRow + ":G" + (startRow + res.Count - 1)].Style.Numberformat.Format = "#,##0.00";
            setBorder1(wsh.Cells["A" + startRow + ":G" + (startRow + res.Count - 1)].Style.Border);
            wsh.Cells.AutoFitColumns();
        }

        private void setBorder1(Border modelTable)
        {
            modelTable.Top.Style = ExcelBorderStyle.Thin;
            modelTable.Left.Style = ExcelBorderStyle.Thin;
            modelTable.Right.Style = ExcelBorderStyle.Thin;
            modelTable.Bottom.Style = ExcelBorderStyle.Thin;
        }

        public void drawMaterial1(int year, ExcelWorksheet wsh, int type)
        {
            wsh.Cells["E3"].Value = year;

            var res1 = (from p in db.TC_param_value
                        where (p.id_tc_param == 42 || p.id_tc_param == 44 || p.id_tc_param == 49 || p.id_tc_param == 48) && p.Materials.mat_type_id == type && p.TC.year == year
                        && p.TC.plant_plant_id == p.TC.Plants.plant_id

                        group new { p.value, p.id_tc_param, p.TC.Plants.name } by new { p.mat_mat_id, p.Materials.name, p.id_tc_operation, name1 = p.TC.Plants.name } into g1

                        orderby g1.Key.name

                        select new
                        {
                            Name = g1.Key.name,
                            Price = g1.Where(v => v.id_tc_param == 44).Sum(v => v.value) / g1.Where(v => v.id_tc_param == 44).Count(),
                            Area = g1.Where(v => v.id_tc_param == 49).Sum(v => v.value),
                            Consumprion = g1.Where(v => v.id_tc_param == 48).Sum(v => v.value),
                            Cost = g1.Where(v => v.id_tc_param == 42).Sum(v => v.value),
                            Culture = g1.Key.name1,

                        }).ToList();

            var startRow = 8;
            double? s = 0;
            for (var i = 0; i < res1.Count; i++)
            {
                var ind = startRow + i;
                var m = res1[i];
                wsh.Cells["A" + ind.ToString()].Value = (i + 1);
                wsh.Cells["B" + ind.ToString()].Value = m.Name;
                wsh.Cells["C" + ind.ToString()].Value = m.Culture;
                wsh.Cells["D" + ind.ToString()].Value = m.Area * m.Consumprion;
                wsh.Cells["E" + ind.ToString()].Value = m.Price;
                wsh.Cells["F" + ind.ToString()].Value = m.Cost;
                s += m.Cost;
            }

            wsh.Cells["J" + startRow].Style.Numberformat.Format = "#,##0.00";
            if (res1.Count != 0)
            {
                wsh.Cells["A" + startRow + ":A" + (startRow + res1.Count - 1)].Style.Numberformat.Format = "#,##0";
                wsh.Cells["B" + startRow + ":F" + (startRow + res1.Count - 1)].Style.Numberformat.Format = "#,##0.00";
                setBorder(wsh.Cells["A" + startRow + ":F" + (startRow + res1.Count - 1)].Style.Border);
            }
            wsh.Cells["F" + (startRow - 1)].Value = s;
            wsh.Cells["F" + (startRow - 1)].Style.Numberformat.Format = "#,##0.00";
            wsh.Cells.AutoFitColumns();
        }

    }
}
