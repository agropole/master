﻿using System;
using System.Data.Entity;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LogusSrv.CORE.Exceptions;
using LogusSrv.DAL.Entities.DTO.ActsSzr;
using LogusSrv.DAL.Entities.DTO;
using LogusSrv.DAL.Entities.Req;
using LogusSrv.DAL.DBModel;
using LogusSrv.DAL.Utils;
using LogusSrv.DAL.Enums;
using LogusSrv.DAL.Entities.Res;
using LogusSrv.DAL.Operations;
using LogusSrv.DAL.Operations.DictionaryOperations;
using System.IO;
using System.Xml;
using System.Globalization;
using System.Xml.Linq;
using System.Data.Entity.Spatial;

namespace LogusSrv.DAL.Operations
{//
    public class DictionaryDb
    {
        protected readonly LogusDBEntities db = new LogusDBEntities();
        protected readonly FieldsDict fields_db = new FieldsDict();
        public List<LogusDictionary> GetDictionaries()
        {
            var res = (from d in db.Dictionary
                     //  where (roleid==11 ? (d.id!29 &&))
                       select new LogusDictionary
                           {
                               Id = d.id,
                               Name = d.name,
                               TableName = d.table_name,
                           }).OrderBy(p => p.Name).ToList();
            return res;
        }

        public LogusDictionaryData GetDictionaryById (int id)
        {
            var res = new LogusDictionaryData();
            var dict = db.Dictionary.Where(o => o.id == id).FirstOrDefault();
            res.Request = dict.request;
            res.RequestUpdate = dict.request_update;
            res.RequestDetail = dict.request_detail;
            res.index_column = dict.index_column;
            res.ColumnDefs = (from c in db.Dictionary_column
                              where c.id_dictionary == id
                              select new LogusDictionaryColumn { 
                                  id = c.id,
                                  field = c.field,
                                  displayName = c.displayName,
                                  width = c.width,
                                  cellTemplate = c.Dictionary_column_type.cellTemplate,
                                  filterParams = new FilterParamModel {apply = true, newRowsAction = "keep"},
                                  filter = "text"
                              }).ToList();
            return res;
        }

        public List<ModelContractor> GetContractor() {
            var contractor = new ContractorDict();
            return contractor.GetContractors(db);
        }
        public string UpdateContractor(List<UpdateContractor> updateObject)
        {
            var contractor = new ContractorDict();
            return contractor.UpdateContractor(updateObject, db);
        }

        public List<ModelType_salary_doc> GetType_salary_doc()
        {
            var type_salary_doc = new Type_salary_docDict();
            return type_salary_doc.GetType_salary_doc(db);
        }
        public string Updateype_salary_doc(List<UpdateType_salary_doc> updateObject)
        {
            var type_salary_doc = new Type_salary_docDict();
            return type_salary_doc.Updateype_salary_doc(updateObject, db);
        }

        public List<ModelVat> GetVAT()
        {
            var vat = new VATDict();
            return vat.GetVAT(db);
        }
        public string UpdateVAT(List<UpdateVat> updateObject)
        {
            var vat = new VATDict();
            return vat.UpdateVAT(updateObject, db);
        }

        public List<ModelTypeFuel> GetType_fuel()
        {
            var typeFuel = new Type_fuelDict();
            return typeFuel.GetType_fuel(db);
        }
        public string UpdateType_fuel(List<UpdateTypeFuel> updateObject)
        {
            var typeFuel = new Type_fuelDict();
            return typeFuel.UpdateType_fuel(updateObject, db);
        }
        public DetailTypeFuel DetailType_fuel()
        {
            var typeFuel = new Type_fuelDict();
            return typeFuel.DetailType_fuel(db);
        }


        public List<ModelTypeMaterials> GetType_materials()
        {
            var typeMaterials = new Type_materialsDict();
            return typeMaterials.GetType_materials(db);
        }
        public string UpdateType_materials(List<UpdateTypeMaterials> updateObject)
        {
            var typeMaterials = new Type_materialsDict();
            return typeMaterials.UpdateType_materials(updateObject, db);
        }

        public List<ModelMaterials> GetMaterials(int? t)
        {
            var materials = new MaterialsDict();
            return materials.GetMaterialsDict(db, t);
        }

        public string UpdateMaterialsDict(List<ModelUpdateMaterials> list)
        {
            var materials = new MaterialsDict();
            return materials.UpdateMaterialsDict(list, db);
        }

        public DetailMaterials DetailMaterials()
        {
            var materials = new MaterialsDict();
            return materials.GetDetailMaterials(db);
        }

        public List<ModelPlants> GetPlants()
        {
            var plants = new PlantsDict();
            return plants.GetPlantsDict(db);
        }

        public string UpdatePlantsDict(List<ModelUpdatePlants> list)
        {
            var plants = new PlantsDict();
            return plants.UpdatePlantsDict(list, db);
        }

        public DetailPlantsModel DetailPlants()
        {
            var plants = new PlantsDict();
            return plants.GetDetailPlants(db);
        }

        public List<ModelFields> GetFields()
        {
            var fields = new FieldsDict();
            return fields.GetFieldsDict(db);
        }

        public string UpdateFieldsDict(List<ModelUpdateFields> list)
        {
            var fields = new FieldsDict();
            return fields.UpdateFieldsDict(list,db);
        }

        public DetailFieldsModel DetailFields()
        {
            var fields = new FieldsDict();
            return fields.GetDetailFields(db);
        }
        //==
        public string UpdateOrganizationsDict(List<ModelUpdateOrganizations> list)
        {
            var gars = new OrganizationsDict();
            return gars.UpdateOrganizationsDict(list, db);
        }

        public DetailOrganizationsModel DetailOrganizations()
        {
            var gars = new OrganizationsDict();
            return gars.GetDetailOrganizations(db);
        }

        public List<OrganizationsDictModel> GetOrganizations()
        {
            var gars = new OrganizationsDict();
            return gars.GetOrganizations(db);
        }
        //===
        public string UpdateGaragesDict(List<ModelUpdateGarages> list)
        {
            var gars = new GaragesDict();
            return gars.UpdateGaragesDict(list, db);
        }

        public DetailGaragesModel DetailGarages()
        {
            var gars = new GaragesDict();
            return gars.GetDetailGarages(db);
        }

        public List<GaragesDictModel> GetGarages()
        {
            var gars = new GaragesDict();
            return gars.GetGarages(db);
        }
        //Модули-автогараж
        public List<ModulesDictModel> GetModules()
        {
            var gars = new ModulesDict();
            return gars.GetModules(db);
        }
        public string UpdateModulesDict(List<ModelUpdateModules> list)
        {
            var mod = new ModulesDict();
            return mod.UpdateModulesDict(list, db);
        }
        //
        public List<Type_traktorsDictModel> GetGroup_traktors()
        {
            var tptrak = new Type_traktorsDict();
            return tptrak.GetType_traktorsDict(db);
        }
   
        public string UpdateType_traktors(List<ModelUpdateType_traktors> list)
        {
            var gars = new Type_traktorsDict();
            return gars.UpdateType_traktorsDict(list, db);
        }

        //группы работ
        public List<Group_tasksDictModel> GetGroup_tasks()
        {
            var tptrak = new Group_tasksDict();
            return tptrak.GetGroup_tasksDict(db);
        }

        public string UpdateGroup_tasks(List<ModelUpdateGroup_tasks> list)
        {
            var gars = new Group_tasksDict();
            return gars.UpdateGroup_tasksDict(list, db);
        }
        public DetailGroup_tasksModel DetailGroup_tasks()
        {
            var traktors = new Type_tasksDict();
            return traktors.GetDetailGroup_tasks(db);
        }
        //этапы выращивания
        public List<Stages_cultivationDictModel> GetStages_cultivation()
        {
            var stagcult = new Stages_cultivationDict();
            return stagcult.GetStages_cultivationDict(db);
        }
        public string UpdateStages_cultivation(List<ModelUpdateStages_cultivation> list)
        {
            var gars = new Stages_cultivationDict();
            return gars.UpdateStages_cultivationDict(list, db);
        }
        //склады
        public List<StoragesDictModel> GetStorages()
        {
            var store = new StoragesDict();
            return store.GetStoragesDict(db);
        }

        public string UpdateStorages(List<ModelUpdateStorages> list)
        {
            var gars = new StoragesDict();
            return gars.UpdateStoragesDict(list, db);
        }
        //Запчасти
        public List<Spare_partsDictModel> GetSpare_parts()
        {
            var spare = new Spare_partsDict();
            return spare.GetSpare_partsDict(db);
        }

        public string UpdateSpare_parts(List<ModelUpdateSpare_parts> list)
        {
            var spare = new Spare_partsDict();
            return spare.UpdateSpare_partsDict(list, db);
        }
        public DetailSpare_partsModel DetailSpare_parts()
        {
            var spare = new Spare_partsDict();
            return spare.GetDetailSpare_parts(db);
        }


        //
        public List<ModelTraktorsDict> GetTraktors()
        {
            var traktors = new TraktorsDict();
            return traktors.GetTraktorsDict(db);
        }
        public string UpdateTraktorsDict(List<ModelUpdateTraktors> list)
        {
            var traktors = new TraktorsDict();
            return traktors.UpdateTraktorsDict(list, db);
        }

        public DetailTraktorsModel DetailTraktors()
        {
            var traktors = new TraktorsDict();
            return traktors.GetDetailTraktors(db);
        }

        public List<ModelType_equipmentsDict> GetType_equipments()
        {
            var equi = new Type_equipmentsDict();
            return equi.GetEquiDict(db);
        }

        public string UpdateType_equipments(List<ModelUpdateType_equipmentsDict> list)
        {
            var equi = new Type_equipmentsDict();
            return equi.UpdateEquiDict(list,db);
        }

        public List<ModelEquipmentDict> GetEquipments()
        {
            var eq = new EquipmentsDict();
            return eq.GetEquipmentsDict(db);
        }
        public string UpdateEquipmentsDict(List<ModelUpdateEquipments> list)
        {
            var eq = new EquipmentsDict();
            return eq.UpdateEquipmentsDict(list,db);
        }

        public DetailEquipmentsModel DetailEquipments()
        {
            var eq = new EquipmentsDict();
            return eq.GetDetailEquipments(db);
        }

        public List<ModelType_tasksDict> GetType_tasks()
        {
            var tptas = new Type_tasksDict();
            return tptas.GetType_tasksDict(db);
        }

        public string UpdateType_tasks(List<ModelUpdateType_tasks> list)
        {
            var tptas = new Type_tasksDict();
            return tptas.UpdateType_tasksDict(list, db);
        }

        public List<ModelType_sorts_plantsDict> GetType_sorts_plants()
        {
            var tpsort = new Type_sorts_plantsDict();
            return tpsort.GetType_sorts_plantsDict(db);
        }
        public string UpdateType_sorts_plantsDict(List<ModelUpdateType_sorts_plants> list)
        {
            var tpsort = new Type_sorts_plantsDict();
            return tpsort.UpdateType_sorts_plantsDict(list, db);
        }

        public DetailType_sorts_plantsModel DetailType_sorts_plants()
        {
            var tpsort = new Type_sorts_plantsDict();
            return tpsort.GetDetailType_sorts_plants(db);
        }

        public List<ModelEmployeeDict> GetEmployees()
        {
            var emp = new EmployeeDict();
            return emp.GetEmployeeDict(db);
        }

        public string UpdateEmployees(List<ModelUpdateEmployeeDict> list)
        {
            var emp = new EmployeeDict();
            return emp.UpdateFieldsDict(list,db);
        }

        public DetailEmployeeDict DetailEmployees()
        {
            var emp = new EmployeeDict();
            return emp.GetEmployeeInfoDict(db);
        }
        //Сдельщики 
        public List<ModelSalEmployeesDict> GetSalaries_employees()
        {
            var salemp = new Salaries_employeesDict();
            return salemp.GetSalaries_employeesDict(db);
        }
        //обновление
        public string UpdateSalaries_employees(List<ModelUpdateSalEmployeesDict> list)
        {
            var salemp = new Salaries_employeesDict();
            return salemp.UpdateSalDict(list, db);
        }
        //удаление


        //
        public string UpdateServiceDict(List<ModelUpdateServiceDict> list)
        {
            var service = new ServiceDict();
            return service.UpdateFieldsDict(list, db);
        }

        public List<ModelServiceDict> GetServices()
        {
            var service = new ServiceDict();
            return service.GetServiceDict(db);
        }


        public List<ModelAggregateDict> GetAggregates()
        {
            var aggregate = new AggregateDict();
            return aggregate.GetAggregateDict(db);
        }

        public string UpdateAggregatesDict(List<ModelUpdateAggregateDict> list)
        {
            var aggregate = new AggregateDict();
            return aggregate.UpdateFieldsDict(list, db);
        }

        public List<ModelTraktors1cDict> GetTech1c()
        {
            var tech = new TraktorsDict1c();
            return tech.GetTraktors1сDict(db);
        }

        public string UpdateTech1c(List<ModelUpdateTraktors1с> req)
        {
            var tech = new TraktorsDict1c();
            return tech.UpdateTraktors1сDict(req, db);
        }

        public List<ModelKPI> GetKPI()
        {
            var kpi = new KPIDict();
            return kpi.Get(db);
        }

        public DetailKPIModel DetailKPI()
        {
            var kpi = new KPIDict();
            return kpi.Detail(db);
        }

        public string UpdateKPI(List<ModelUpdateKPI> req)
        {
            var kpi = new KPIDict();
            return kpi.Update(req, db);
        }

        public List<AgroreqModel> GetAgtoreq()
        {
            var tech = new AgroreqDict();
            return tech.GetAgroreq(db);
        }

        public string UpdateAgroreq(List<ModelUpdateAgroreq> req)
        {
            var tech = new AgroreqDict();
            return tech.UpdateAgroreq(req, db);
        }
        public List<TypeSparePartsDictModel> GetTypeSpareParts()
        {
            var tptrak = new TypeSparePartsDict();
            return tptrak.GetTypeSpareParts(db);
        }

        public string UpdateTypeSpareParts(List<ModelUpdateTypeSpareParts> list)
        {
            var gars = new TypeSparePartsDict();
            return gars.UpdateTypeSpareParts(list, db);
        }
        public LogusDictionaryData GetDictionaryField(int p)
        {
            int id = 8;
            var res = new LogusDictionaryData();
            var dict = db.Dictionary.Where(o => o.id == id).FirstOrDefault();
            res.Request = dict.request;
            res.RequestUpdate = dict.request_update;
            res.RequestDetail = dict.request_detail;
            res.index_column = dict.index_column;
            res.ColumnDefs = (from c in db.Dictionary_column
                              where c.id_dictionary == id
                              select new LogusDictionaryColumn
                              {
                                  id = c.id,
                                  field = c.field,
                                  displayName = c.displayName,
                                  width = c.width,
                                  cellTemplate = c.Dictionary_column_type.cellTemplate,
                                  filterParams = new FilterParamModel { apply = true, newRowsAction = "keep" },
                                  filter = "text"
                              }).ToList();
            return res;
        }
        public List<ModelFields> GetFieldsByType(int? type)
        {
            var fields = new FieldsDict();
            if (type != 0) { return fields.GetFieldsDictByType(type, db); }
            else { return fields.GetFieldsDict(db); }
        }
        public DetailFieldsModel DetailFieldsByType(int? type)
        {
            var fields = new FieldsDict();
            return fields.GetDetailFieldsByType(type, db);
        }

        /// Выгрузка кординат полей в txt
        public ReportFile GetFieldCoordTxt(string pathTemplate)
        {
            DirectoryInfo d = new DirectoryInfo(pathTemplate);
            var list = (from f in db.Fields
                        where f.type == 1
                        select new
                        {
                            name = f.name,
                            coord = f.coord.AsText(),
                            center = f.center,
                        }).ToList();

            var text = d.ToString();
            StreamWriter sw = new StreamWriter(text);
            for (int i = 0; i < list.Count; i++)
            {
                sw.WriteLine(list[i].name + " // " + list[i].coord + " / " + list[i].center + Environment.NewLine);
            }
            sw.Close();

            byte[] buff = null;

            FileStream fs = new FileStream(text, FileMode.Open, FileAccess.Read);
            BinaryReader br = new BinaryReader(fs);
            long numBytes = new FileInfo(pathTemplate).Length;
            buff = br.ReadBytes((int)numBytes);

            var r = new ReportFile
            {
                data = buff,
                fileName = "Координаты полей.txt"
            };
            fs.Close();
            br.Close();
            return r;
        }
        ///Получение данных по полям из файла
        public List<String> GetCoordFromTXT(int type, string templatePath)
        {
            var fields = new FieldsDict();
            var report = fields.GetCoordFromTXT(type, templatePath, db);
            return report;
        }

        //KML
        public string GetFieldCoordKmlData()
        {
            var list = (from f in db.Fields
                        where f.type == 1 && f.deleted == false
                        select new FieldArmDTO
                        {
                            name = f.name,
                            coord = f.coord.AsText(),
                            coordG = f.coord,
                            center = f.center.AsText(),
                        }).ToList();

            MemoryStream mStream = new MemoryStream();
            XmlTextWriter writer = new XmlTextWriter(mStream, Encoding.UTF8);
            NumberFormatInfo nfi = new NumberFormatInfo();
            nfi.NumberDecimalSeparator = ".";
            writer.Formatting = Formatting.Indented;
            writer.WriteStartDocument();
        //    writer.WriteStartElement("kml", "http://www.opengis.net/kml/2.2");
            writer.WriteStartElement("Document"); //Document
            var coords = "";
            foreach (var m in list) // поступление ЗП
            {
                if (m.coordG == null) { continue; }
                writer.WriteStartElement("Placemark"); //Placemark
                writer.WriteElementString("name", m.name);
                writer.WriteStartElement("Polygon");
                writer.WriteElementString("extrude", "1");
                writer.WriteElementString("altitudeMode", "relativeToGround");

                writer.WriteStartElement("outerBoundaryIs");
                writer.WriteStartElement("LinearRing");
                for (int i = 1; i < m.coordG.PointCount; i++)
                {
                    var p = m.coordG.PointAt(i);
                    coords = coords + p.Longitude.Value.ToString(nfi) + "," + p.Latitude.Value.ToString(nfi) + ",0. ";
                }
                writer.WriteElementString("coordinates", coords);
                coords = "";
                writer.WriteEndElement();  //Polygon
                writer.WriteEndElement();  //outerBoundaryIs
                writer.WriteEndElement();  //LinearRing
                writer.WriteEndElement(); 
            } 
        //    writer.WriteEndElement(); // kml
            writer.WriteEndElement();  //document
            writer.Flush();
            mStream.Flush();
            mStream.Position = 0;
            StreamReader sReader = new StreamReader(mStream);
            String FormattedXML = sReader.ReadToEnd();
            return FormattedXML;
        }

        //load from KML
        public List<String> saveFieldsFromKmlFile(Stream stream, int type)
        {
            int records = 0;
            int insert = 0;
            var result = new List<String>();
            XDocument xDoc = XDocument.Load(stream);
            foreach (var m in xDoc.Element("Document").Elements("Placemark"))
            {
                var sb = new StringBuilder(); sb.Append(@"POLYGON((");
                String name = m.Element("name").Value;           //название поля
                String coords = m.Element("Polygon").Element("outerBoundaryIs").Element("LinearRing").Element("coordinates").Value.Replace("\n", "");

                var parseCoord = coords.Split(' ');
                for (int i = 0; i < parseCoord.Length; i++)
                {
                    if (parseCoord[i].Equals("")) { continue; }
                    var parseCoord2 = parseCoord[i].Split(',');
                    sb.Append(parseCoord2[0] + " " + parseCoord2[1] + ",");
                    //закольцевать
                    if (parseCoord.Length == i + 2) {
                        parseCoord2 = parseCoord[0].Split(',');
                        sb.Append(parseCoord2[0] + " " + parseCoord2[1]);
                    }
                }
                sb.Append(@"))");
                var Coord = DbGeography.PolygonFromText(sb.ToString(), 4326);     //coord
                var Center = fields_db.GetCenter(Coord);
                double? Area = Coord.Area.Value / 10000 * 10;
                Area = (Math.Round((double)Area) / 10) - 0.1;

                using (var trans = db.Database.BeginTransaction())
                {
                    try
                    {
                        var newId = (db.Fields.Max(t => (int?)t.fiel_id) ?? 0) + 1;
                            var updateList = db.Fields.Where(f => f.type == type && f.name == name && f.deleted != true).FirstOrDefault();
                            if (updateList != null)
                            {
                                updateList.coord = Coord;
                                updateList.area = Area != null ? Convert.ToDecimal(Area) : (decimal)1.0;
                                updateList.center = Center;
                                records++;
                            }
                            else
                            {
                                var newDic = new Fields
                                {
                                    fiel_id = newId,
                                    name = name,
                                    org_org_id = 1, // по умолчанию Логус
                                    type = type,
                                    deleted = false,
                                    coord = Coord,
                                    area = Area != null ? Convert.ToDecimal(Area) : (decimal)0,
                                    center = Center,
                                };
                                newId++;
                               insert++;
                                db.Fields.Add(newDic);
                            }

                        db.SaveChanges();
                        trans.Commit();

                    }
                    catch (Exception ex)
                    {
                        trans.Rollback();
                       result.Add(ex.Message);
                       return result;
                        throw;

                    }
                }
            }
            result.Add(Convert.ToString(records));
            result.Add(Convert.ToString(insert));
            return result;
        }




    }
}
