﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogusSrv.DAL.Entities.SQL
{
    public class ExportTMCOutSet
    {
        public DateTime record_act_date { get; set; }
        public string fiel_name { get; set; }
        public string fiel_code { get; set; }
        public string plant_name { get; set; }
        public string fps_code { get; set; }
        public string mat_name { get; set; }
        public string mat_code { get; set; }
        public string fullname { get; set; }
        public string emp_code { get; set; }
        public Single count { get; set; }
        public Single price { get; set; }
        public DateTime act_date { get; set; }
        public string act_num { get; set; }
    }

    public class ExportDocumentsList
    {
        public int Id { get; set; }
        public uint Id1 { get; set; }
        public string doc_num { get; set; } //номер документа
        public DateTime doc_date { get; set; } // дата документа
        public DateTime date { get; set; } // дата документа
        public Int64 sum { get; set; }
        public Int64 sum1 { get; set; }
        public Int64 summaDoc { get; set; }
        public Int64 price { get; set; } //расценка
        public Int64 value { get; set; } //значение
        public string emp_code { get; set; }
        public string Name { get; set; }
        public string task_code { get; set; }
        public string task_code1 { get; set; }
        public string description { get; set; } //вид работ
        public string fiel_code { get; set; }
        public string fiel_name { get; set; } //подразделение затрат

    }

}

