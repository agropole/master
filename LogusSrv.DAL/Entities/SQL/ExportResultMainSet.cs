﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogusSrv.DAL.Entities.SQL
{
    public class ExportResultMainSet
    {
        public int shtr_id { get; set; }
        public string dbegin { get; set; }
        public string trk_code { get; set; }
        public string trk_name { get; set; }
        public string trk_num { get; set; }
        public string trk_im_code { get; set; }
        public string trk_skld { get; set; }
        public string eq_code { get; set; }
        public string os_code { get; set; }
        public string eq_name { get; set; }
        public string mech_code { get; set; }
        public string short_name_mech { get; set; }
        public string dend { get; set; }
        public string num_sheet { get; set; }
        public string otv { get; set; }
        public Decimal sm { get; set; }
        public int exported { get; set; }
        public string fuelcod { get; set; }

    }
}
