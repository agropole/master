﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogusSrv.DAL.Entities.SQL
{
    public class ExportResultWorkSet
    {
        public string in_code { get; set; }
        public string task_name { get; set; }
        public string subk { get; set; }
        public string dbegin { get; set; }
        public string dend { get; set; }
        public Decimal price_days { get; set; }
        public Decimal worktime { get; set; }
        public string porg { get; set; }
        public string porg_name { get; set; }
        public int? plant_plant_id { get; set; }
        public Decimal cons { get; set; }
        public int? type_price { get; set; }
        public Decimal workdays { get; set; }
        public Decimal salary { get; set; }
        public Decimal area { get; set; }
        public Decimal volume_fact { get; set; }
        public Decimal price_add { get; set; }
        public Decimal mileage_total { get; set; }
        public Decimal total_salary { get; set; }
    }
}
