﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogusSrv.DAL.Entities.SQL
{
    public class ExportRefuellingSet
    {
        public Int32 emp_emp_id { get; set; }
        public Int32 order_num { get; set; }
        public Decimal volume { get; set; }
    }
}
