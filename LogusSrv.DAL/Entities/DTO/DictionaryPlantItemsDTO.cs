﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace LogusSrv.DAL.Entities.DTO
{
    public class DictionaryPlantItemsDTO : DictionaryItemsDTO
    {
        public int? plantId { get; set; }
        public int? fieldId { get; set; }
    }
}