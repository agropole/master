﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogusSrv.DAL.Entities.DTO
{
    public class BalanceTraktorsDto
    {
        public string name { get; set; }

        public int id { get; set; }
        public int count { get; set; }
    }
}
