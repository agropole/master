﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogusSrv.DAL.Entities.DTO.DriverArm
{
    public class CreateTask
    {
        public int way_list_id { get; set; }
        public DateTime? date_degin { get; set; }
        public DateTime? date_end { get; set; }
        public int field_id { get; set; }
        public int task_type_id { get; set; }
    
    }
}
