﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogusSrv.DAL.Entities.DTO.DriverArm
{
    public class GeoTraktorPropDto
    {
        public int id { get; set; }
        public int? track_id { get; set; }
        public string model { get; set; }
        public string reg_num { get; set; }
        public string type_name { get; set; }
        public int type_id { get; set; }
        public string icon_path { get; set; }
        public DateTime? date { get; set; }
        public decimal? speed { get; set; }
        public decimal? mileage { get; set; }
        public decimal? direction { get; set; }
        public string  name  { get; set; }
        public int? grtrak_id { get; set; }
    }
}
