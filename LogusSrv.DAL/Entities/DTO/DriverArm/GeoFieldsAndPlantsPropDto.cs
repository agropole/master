﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogusSrv.DAL.Entities.DTO.DriverArm
{
    public class GeoFieldsAndPlantsPropDto
    {
        public string color { get; set; }
        public string name { get; set; }
        public string plant { get; set; }
        public int id { get; set; }

        public decimal? area { get; set; }
        public List<PlantsDTO> plants { get; set; }
    }
}
