﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogusSrv.DAL.Entities.DTO.DriverArm
{
    public class GeoFieldsAndPlantsMapDto
    {
        public string type { get; set; }
        public List<GeoFieldAndPlantsFeaturesDto> features { get; set; }
    }
}
