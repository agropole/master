﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogusSrv.DAL.Entities.DTO.DriverArm
{
    public class ProgressMap
    {
        public DateTime DateT { get; set; }
        public DateTime DateT1 { get; set; }
        public DateTime Date { get; set; }
        public string datePlan { get; set; }
        public string dateFact { get; set; }
        public string TaskName { get; set; }
        public int? TaskId { get; set; }
        public int? groupId { get; set; }
        public string colorgroup { get; set; }

    }
    public class ProgressInfoField
    {        
        public string PlantName { get; set; }
        public string FieldName { get; set; }
        public int? PlantId { get; set; }
    }
    public class ChackTimeStage
    {
        public int? taskID { get; set; }
        public bool back { get; set; }
        public bool equal { get; set; }
        public bool forvard { get; set; }
        public DateTime DataChack { get; set; }
        
    }
}
