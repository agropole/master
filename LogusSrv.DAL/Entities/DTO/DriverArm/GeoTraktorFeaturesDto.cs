﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogusSrv.DAL.Entities.DTO.DriverArm
{
    public class GeoTraktorFeaturesDto
    {
        public string type { get; set; }
        public CloseGeometryInfoDTO geometry { get; set; }
        public GeoTraktorPropDto properties { get; set; }
    }
}
