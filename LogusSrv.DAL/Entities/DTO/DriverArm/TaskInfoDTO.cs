﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogusSrv.DAL.Entities.DTO.DriverArm
{
    public class TaskInfoDTO
    {
        public int? task_id { get; set; }
        public string field_name { get; set; }

        public string plant_name { get; set; }

        public string task_name { get; set; }
        public string equip_name { get; set; }

        public decimal? work_time { get; set; }
        public decimal? work_day { get; set; }
        public decimal? price_days { get; set; }
        public int? moto_hours { get; set; }
        public decimal? volume_fact { get; set; }
        public decimal? area { get; set; }
        public decimal? salary { get; set; }
        public decimal? consumption_rate { get; set; }
        public decimal? consumption { get; set; }
        public decimal? consumption_fact { get; set; }

        public string available_name { get; set; }

        public decimal? task_num { get; set; }

        public string driver_name { get; set; }
        public string traktor_brand { get; set; }
             //   traktor_id = t.Sheet_tracks.Traktors.trakt_id,
        public string traktor_number { get; set; }
        public string list_num { get; set; }

        public DateTime date_begin { get; set; }

        public string equipment_name { get; set; }
    }
}
