﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogusSrv.DAL.Entities.DTO
{
    public  class CloseWayListMapDTO
    {
        public string type { get; set; }
        public List<CloseMapInfoDTO> features { get; set; }
    }
}
