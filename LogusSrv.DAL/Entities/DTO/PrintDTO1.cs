﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogusSrv.DAL.Entities.DTO
{
    public class PrintDTO1
    {
        public int shtr_id { get; set; }
        public string q1_num { get; set; }
        public int q1_year { get; set; }
        public int q1_month { get; set; }
        public int q1_day { get; set; }
        public int q1_db_hh { get; set; }
        public int q1_de_hh { get; set; }
        public int q1_db_mi { get; set; }
        public int q1_de_mi { get; set; }
        public DateTime q1_date_end { get; set; }
        public string q1_garage_name { get; set; }
        public string q1_m_name { get; set; }
        public string q1_traktor_name { get; set; }
        public string q1_traktor_num { get; set; }
        public string org_name { get; set; }
        public string ogrn { get; set; }
        public decimal? q1_left_fuel { get; set; }
        public decimal? q1_issued_fuel { get; set; }
        public decimal? q1_remain_fuel { get; set; }
        public decimal? q1_moto_start { get; set; }
        public decimal? q1_moto_end { get; set; }

    }
}
