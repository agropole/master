﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LogusSrv.DAL.Enums;

namespace LogusSrv.DAL.Entities.DTO
{
    public class GetTaskDTO
    {
        public int TaskId { get; set; }
        public string FieldName { get; set; }
        public string PlantName { get; set; }
        public string TaskName { get; set; }
        public string EquipmentName { get; set; }
        public decimal? WorkTime { get; set; }
        public decimal? WorkDay { get; set; }
        public decimal? PriceDays { get; set; }
        public int? MotoHours { get; set; }
        public decimal? Volumefact { get; set; }
        public decimal? Area { get; set; }
        public decimal? Salary { get; set; }
        public decimal? ConsumptionRate { get; set; }
        public decimal? Consumption { get; set; }
        public decimal? ConsumptionFact { get; set; }
        public string AvailableName { get; set; }
        public decimal? TaskNum { get; set; }
        public decimal? PrimaryPayment { get; set; }
        public decimal? TypePrice { get; set; }
        public decimal? PriceAdd { get; set; }
        public decimal? PriceTotal { get; set; }
    }
}
