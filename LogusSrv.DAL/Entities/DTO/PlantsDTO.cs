﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace LogusSrv.DAL.Entities.DTO
{
    public class PlantsDTO 
    {
        public int plant_id { get; set; }
        public string plant_name { get; set; }
        public string color { get; set; }
        public string icon_path { get; set; }
        public string description { get; set; }
        public decimal? area { get; set; }
        public DateTime? date_sowing { get; set; }
        public DateTime? date_ingathering { get; set; }
     
    }
}