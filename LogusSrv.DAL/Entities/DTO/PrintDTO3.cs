﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogusSrv.DAL.Entities.DTO
{
    public class PrintDTO3
    {
        public decimal? q3_workdays { get; set; }

        public decimal? q3_worktime { get; set; }
        public decimal? q3_salary { get; set; }

        public decimal? q3_consumption_fact { get; set; }
    }
}
