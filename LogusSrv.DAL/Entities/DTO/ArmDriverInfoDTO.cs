﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogusSrv.DAL.Entities.DTO
{
    public class ArmDriverInfoDTO
    {
        public decimal driver_id { get; set; }
        public string first_name { get; set; }

        public string middle_name { get; set; }
        public string second_name { get; set; }

        public string post { get; set; }
        public string Organization { get; set; }
    }
}
