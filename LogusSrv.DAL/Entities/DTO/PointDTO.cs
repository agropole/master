﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogusSrv.DAL.Entities.DTO
{
    public class PointDTO
    {
        public double lon { get; set; }
        public double lat { get; set; }
    }
}
