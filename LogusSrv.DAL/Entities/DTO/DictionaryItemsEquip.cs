﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogusSrv.DAL.Entities.DTO
{
    public class DictionaryItemsEquip : DictionaryItemsTraktors
    {
        public decimal? Width { get; set; }
    }
}
