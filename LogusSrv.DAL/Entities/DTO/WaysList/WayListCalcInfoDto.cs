﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LogusSrv.DAL.DBModel;

namespace LogusSrv.DAL.Entities.DTO.WaysList
{
    public class WayListCalcInfoDto
    {
        public Sheet_tracks s { get; set; }
        public decimal? reful { get; set; }
        public decimal? ConsumptionFact { get; set; }
        public decimal? MotoHour { get; set; }
        public decimal? MileageTotal { get; set; }
    }
}
