﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogusSrv.DAL.Entities.DTO
{
    public class FieldsWithPlantsDTO : DictionaryItemsDTO
    {
        public List<DictionaryItemsDTO> PlantsList { get; set; }
    }
}
