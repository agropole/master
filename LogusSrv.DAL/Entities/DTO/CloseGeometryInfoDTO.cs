﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogusSrv.DAL.Entities.DTO
{
    public class CloseGeometryInfoDTO
    {
        public string type { get; set; }
        public Object coordinates { get; set; }
        
    }
}
