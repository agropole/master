﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogusSrv.DAL.Entities.DTO
{
    public class PrintShiftDTO
    {
        public string TitleColumn { get; set; }
        public string FieldName { get; set; }
        public string PlantName { get; set; }
        public string EquipmentName { get; set; }
        public string Employees { get; set; }
        public string TraktorName { get; set; }
        public string TraktorNum { get; set; }
        public string Available { get; set; }
        public decimal? Shift { get; set; }
        public string TaskPlan { get; set; }
        public string MainComment { get; set; }
        public string Comment { get; set; }
        public string AgroreqName { get; set; }
    }
    public class BalanceDaytasksDTO
    {
        public List<DictionaryItemsDTO> Employees { get; set; }
        public List<DictionaryItemsDTO> unsedEmployees { get; set; }
        public List<DictionaryItemsDTO> Traktors { get; set; }
        public List<DictionaryItemsDTO> unsedTraktors { get; set; }
        public List<DictionaryItemsDTO> TypeTasks { get; set; }
        public List<DictionaryItemsDTO> ChiefsList { get; set; }
        public List<DictionaryItemsTraktors> EquipmnetsList { get; set; }
        public List<DictionaryItemForFields> FieldsList { get; set; }
        public List<DictionaryItemForFields> AllFields { get; set; }
        public List<DictionaryItemForFields> FieldsAndPlants { get; set; }
    }
    public class BalanceDaytasksTraktorDTO
    {
        public List<DictionaryItemsTraktors> Traktors { get; set; }
    }


    public class PrintShiftEmployees
    {
        public string Name { get; set; }
        public string Func { get; set; }
    }
}

