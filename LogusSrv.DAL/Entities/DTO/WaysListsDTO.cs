﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogusSrv.DAL.Entities.DTO
{
    public class WaysListsDTO
    {
        public int WaysListId { get;set; }
        public string WaysListNum { get; set; }
        public string DateBegin { get; set; }

        public DateTime Date { get; set; }
        public string FioDriver { get; set; }
        public string CarsNum { get; set; }
        public string CarsName { get; set; }
        public decimal? Status { get; set; }
        public string NameEq { get; set; }

        public int ShiftNum { get; set;}
        public Boolean? IsRepair { get; set; }
    }
}
