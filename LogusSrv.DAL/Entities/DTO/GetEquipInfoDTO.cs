﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;


namespace LogusSrv.DAL.Entities.DTO
{
    public class GetEquipInfoDTO 
    {
        public ElemntInfoDTO TracktorInfo { get; set; }
        public ElemntInfoDTO EquipInfo { get; set; }
    }
}