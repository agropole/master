﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Data.Entity.Spatial;

namespace LogusSrv.DAL.Entities.DTO
{
    public class CoordGeoJsonDTO
    {
        [JsonConverter(typeof(DbGeographyGeoJsonConverter))]
        public DbGeography Kk { get; set; }
    }
}
