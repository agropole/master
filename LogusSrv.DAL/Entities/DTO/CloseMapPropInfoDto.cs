﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogusSrv.DAL.Entities.DTO
{
    public class CloseMapPropInfoDTO
    {

        public string color { get; set; }
        public string name { get; set; }
        public string plant { get; set; }
    }
}
