﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogusSrv.DAL.Entities.DTO
{
    public class ElemntInfoDTO
    {
        public int? Id { get; set; }
        public string Name { get; set; }
        public string RepairStatus { get; set; }

    }
}
