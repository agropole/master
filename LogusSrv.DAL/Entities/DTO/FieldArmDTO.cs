﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Spatial;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogusSrv.DAL.Entities.DTO
{
    public class FieldArmDTO
    {

        public int id { get; set; }

        public string coord { get; set; }
        public DbGeography coordG { get; set; }

        public string center { get; set; }

        public string name { get; set; }

        public decimal? area { get; set; }

        public List<PlantsDTO> plants { get; set; }
    }
}
