﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogusSrv.DAL.Entities.DTO.DriverWayBills
{
    public class CloseDriverWayBillTaskInListDto
    {
        public int TaskId { get; set; }
        public decimal? TaskNum { get; set; }
        public string FieldName { get; set; }
        public string PlantName { get; set; }
        public string TaskName { get; set; }
        public string EquipmentName { get; set; }
        public string AvailableName { get; set; }
        public decimal? Salary { get; set; }
        public decimal? Volumefact { get; set; }
        public decimal? TypePrice { get; set; }
        public decimal? NumRuns { get; set; }
        public decimal? MileageTotal { get; set; }
        public decimal? MileageCargo { get; set; }
        public decimal? MileageNoCargo { get; set; }
        public decimal? MileageOnTrack { get; set; }
        public decimal? MileageOnGround { get; set; }
        public decimal? NumLifts { get; set; }
        public decimal? TrailerFact { get; set; }
        public decimal? RateTrailer { get; set; }
        public decimal? NumRefuel  { get; set; }
        public decimal? ConsumptionRate  { get; set; }
        public decimal? ConsumptionFact { get; set; }

        public decimal? PriceAdd { get; set; }
        public decimal? WorkTime { get; set; }
        public decimal? WorkDay { get; set; }
        public decimal? PriceTotal { get; set; }
        public decimal? Area { get; set; }
    }
}