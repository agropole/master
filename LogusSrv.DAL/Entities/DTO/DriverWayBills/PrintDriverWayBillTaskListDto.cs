﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LogusSrv.DAL.DBModel;

namespace LogusSrv.DAL.Entities.DTO.DriverWayBills
{
    public class PrintDriverWayBillTaskListDto
    {
        public int TaskId { get; set; }
        public string TypeTask { get; set; }
        public string TaskAndPlant { get; set; }
        public string Available {get;set;}
        public string Customer {get;set;}
        public string Equip {get;set;}
        public string EquipNum {get;set;}
        public decimal TypeField { get; set; }
        public string FieldName { get; set; }
        public string Description { get; set; }
        public int FiledId { get; set; }
        public decimal ? Salary {get;set;}
        public decimal ? NumRuns {get;set;}
        public decimal ? MileageTotal {get;set;}
        public decimal ? MileageCargo {get;set;}
        public decimal ? MileageNoCargo {get;set;}
        public decimal? TotalSalary { get; set; }
        public decimal ? MileageOnTrack {get;set;}
        public decimal ? MileageOnGround {get;set;}
        public decimal ? NumLifts {get;set;}
        public decimal? Volume { get; set; }
        public decimal?  NumRefuel { get; set; }
        public decimal? ConsumptionRate { get; set; }
        public decimal? ConsumptionFact { get; set; }
        public decimal? WorkTime { get; set; }
        public string Ogranization { get; set; }
        public string OGRN { get; set; }
    }
}
