﻿using System.Collections.Generic;
using LogusSrv.DAL.Entities.Res;

namespace LogusSrv.DAL.Entities.DTO.DriverWayBills
{
    public class InfoForDriverWayBill
    {
        public List<DictionaryItemsDTO> ChiefsList { get; set; }
        public List<DictionaryItemsDTO> EmployeesList { get; set; }
        public List<DictionaryItemsTraktors> TraktorsList { get; set; }
        public List<DictionaryItemsDTO> TasksList { get; set; }
        public List<DictionaryItemsDTO> FieldsList { get; set; }
        public List<DictionaryItemForFields> FieldsAndPlants { get; set; }
        public List<DictionaryItemsTraktors> EquipmnetsList { get; set; }
        public List<DictionaryItemsDTO> TypePriceList { get; set; }
        public List<DictionaryItemsDTO> AgroreqList { get; set; }

        public List<DictionaryItemsDTO> TypeFuel { get; set; }
    }
}
