﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogusSrv.DAL.Entities.DTO.DriverWayBills
{
    public class PrintDriverWayBillInfoDto
    {
          public string WayBillNum {get;set;}
          public string OrgName  {get;set;}
          public string OGRN { get; set; }
          public string StartMonth  {get;set;}
          public string StartYear { get; set; }
          public string Model  {get;set;}
          public string ModelNum  {get;set;}
          public string Driver {get;set;}
          public string Certificate  {get;set;}
          public string StartDD { get; set; }
          public string StartMM { get; set; }
          public string Starthh { get; set; }
          public string Startmm { get; set; }
          public string EndDD { get; set; }
          public string EndMM { get; set; }
          public string Endhh { get; set; }
          public string Endmm { get; set; }

          public decimal? IssuedFuel { get; set; }
          public decimal? RemainFuel { get; set; }
          public decimal? leftFuel { get; set; }
          public string StartFactTime { get; set; }
          public string EndFactTime { get; set; }


          public decimal? MotoStart { get; set; }
          public decimal? MotoEnd { get; set; }

          public DateTime StartDate { get; set; }
          public DateTime EndDate { get; set; }
          public DateTime? StartFactDate { get; set; }
          public DateTime? EndFactDate { get; set; }

          public decimal? SalaryTotal { get; set; }
          public decimal? WorkTime { get; set; }
          public decimal? NumRunsTotal { get; set; }
          public decimal? MileageTotal { get; set; }
          public decimal? MileageCargoTotal { get; set; }

          public decimal? MileageNoCargoTotal { get; set; }
          public decimal? MileageOnTrackTotal { get; set; }

          public decimal? MileageOnGroundTotal { get; set; }
          public decimal? NumLiftsTotal { get; set; }

          public decimal? VolumeTotal { get; set; }
          public decimal? DoToVolumeTotal { get; set; }
          public decimal? TotalSalary { get; set; } 

          public string MileageTotalStr { get; set; }
          public string MileageCargoTotalStr { get; set; }
          public string MileageNoCargoTotalStr { get; set; }
          public string MileageOnTrackTotalStr { get; set; }
          public string MileageOnGroundTotalStr { get; set; }
          public string NumLiftsTotalStr { get; set; }
          public decimal? ConsumptionFact { get; set; }
          public List<PrintDriverWayBillTaskListDto> TaskList { get;set; }
    }
}
