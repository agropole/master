﻿using LogusSrv.DAL.Entities.DTO.DriverWayBillTaskExt;

namespace LogusSrv.DAL.Entities.DTO.DriverWayBills
{
    public class DriverWayBillItemTaskDto
    {
        public int? TaskId { get; set; }
        public DrvWbTaskFieldInfoDto FieldInfo { get; set; }
        public DrvWbTaskPlantInfoDto PlantInfo { get; set; }
        public DrvWbTaskTypeInfoDto TypeTaskInfo { get; set; }
        public DrvWbTaskEquipmentInfoDto EquipInfo { get; set; }
        public DrvWbTaskAvailableInfoDto AvailableInfo { get; set; }
        public string Comment { get; set; }
    }
}
