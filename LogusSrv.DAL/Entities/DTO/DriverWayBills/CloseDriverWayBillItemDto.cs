﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LogusSrv.DAL.DBModel;

namespace LogusSrv.DAL.Entities.DTO.DriverWayBills
{
    public class CloseDriverWayBillItemDto
    {
        public int? WayBillId { get; set; }

        public string DriverName { get; set; }
        public string TraktorName { get; set; }

        public int? TraktorId { get; set; }

        public Boolean? IsRepair { get; set; }

        public DateTime? DateBegin { get; set; }

        public DateTime? DateEnd { get; set; }

      //-  [RegularExpression(@"^\d*(\,\d{1,2})?$", ErrorMessage = "Значение в поле \"Спидометр (начало)\" не должно иметь больше двух знаков после запятой")]
        [Range(0, 9999999999.99, ErrorMessage = "Значение в поле \"Выдано всего горючего\" должно находиться в диапазоне от 0 до 9999999999.99")]
        public decimal? MotoStart { get; set; }
        public decimal? MotoEnd { get; set; }

        [Required(ErrorMessage = "Не введено значение в поле \"Начало работы\"")]
        [RegularExpression(@"^(0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$", ErrorMessage = "Значение в поле \"Начало работы\" должно находиться в диапазоне 00:00 - 23:59")]
        public string timeStartFact { get; set; }

        [Required(ErrorMessage = "Не введено значение в поле \"Окончание работы\"")]
        [RegularExpression(@"^(0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$", ErrorMessage = "Значение в поле \"Окончание работы\" должно находиться в диапазоне 00:00 - 23:59")]
        public string timeEndFact { get; set; }

        //-   [RegularExpression(@"^\d*(\,\d{1,2})?$", ErrorMessage = "Значение в поле \"Было горючего\" не должно иметь больше двух знаков после запятой")]
        [Range(0, 99999.99, ErrorMessage = "Значение в поле \"Было горючего\" должно находиться в диапазоне от 0 до 99999.99")]
        public decimal? leftFuel { get; set; }

        //-   [RegularExpression(@"^\d*(\,\d{1,2})?$", ErrorMessage = "Значение в поле \"Выдано всего горючего\" не должно иметь больше двух знаков после запятой")]
        [Range(0, 99999.99, ErrorMessage = "Значение в поле \"Выдано всего горючего\" должно находиться в диапазоне от 0 до 99999.99")]
        public decimal? IssuedFuel { get; set; }

        public decimal? RemainFuel { get; set; }

        public string WaysListNum { get; set; }
        public DateTime? DateBeginFact { get; set; }
        public DateTime? DateEndFact { get; set; }
        public bool? AllResource { get; set; }
        public int OrgId { get; set; }
        public List<CloseDriverWayBillTaskInListDto> Tasks { get; set; }
    }
}
