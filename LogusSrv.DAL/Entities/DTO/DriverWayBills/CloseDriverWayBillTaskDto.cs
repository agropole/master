﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LogusSrv.DAL.Entities.Res;

namespace LogusSrv.DAL.Entities.DTO.DriverWayBills
{
    public class CloseDriverWayBillTaskDto
    {
        public int? TaskId { get; set; }
        
        public int WayBillId { get; set; }

        [Required(ErrorMessage = "Не выбрано значение в поле \"Поле\"")]
        public int? FieldId { get; set; }

        public int? PlantId { get; set; }
        public int? AvailableId { get; set; }

        [Required(ErrorMessage = "Не выбрано значение в поле \"Задание\"")]
        public int? TaskTypeId { get; set; }
        
        public int? EquipmentId { get; set; }

        public decimal? Area { get; set; }

        [Required(ErrorMessage = "Не введено значение в поле \"Количество рейсов\"")]
        [RegularExpression(@"^\d*$", ErrorMessage = "Значение в поле \"Количество рейсов\" должно бьть числовым")]
        [Range(0, 999, ErrorMessage = "Значение в поле \"Количество рейсов\" должно находиться в диапазоне от 0 до 999")]
        public decimal? NumRuns { get; set; }

   
        [Range(0, 99999.99, ErrorMessage = "Значение в поле \"Пробег общий, км.\" должно находиться в диапазоне от 0 до 99999.99")]
        public decimal? MileageTotal { get; set; }

        [Required(ErrorMessage = "Не введено значение в поле \"Пробег с грузом, км.\"")]
        [Range(0, 99999.99, ErrorMessage = "Значение в поле \"Пробег с грузом, км.\" должно находиться в диапазоне от 0 до 99999.99")]
        public decimal? MileageCargo { get; set; }
      
        [Range(0, 99999.99, ErrorMessage = "Значение в поле \"Пробег без груза, км.\" должно находиться в диапазоне от 0 до 99999.99")]
        public decimal? MileageNoCargo { get; set; }

        [Required(ErrorMessage = "Не введено значение в поле \"Пробег по трассе, км.\"")]
        [Range(0, 99999.99, ErrorMessage = "Значение в поле \"Пробег по трассе, км.\" должно находиться в диапазоне от 0 до 99999.99")]
        public decimal? MileageOnTrack { get; set; }

        [Range(0, 99999.99, ErrorMessage = "Значение в поле \"Пробег по грунту, км.\" должно находиться в диапазоне от 0 до 99999.99")]
        public decimal? MileageOnGround { get; set; }

        public decimal? Volumefact { get; set; }
        [RegularExpression(@"^\d*$", ErrorMessage = "Значение в поле \"Количество подъемов\" должно бьть числовым")]
        [Range(0, 999, ErrorMessage = "Значение в поле \"Количество подъемов\" должно находиться в диапазоне от 0 до 999")]
        public decimal? NumLifts { get; set; }

        [Required(ErrorMessage = "Не введено значение в поле \"Оплата труда, руб\"")]
    
        [Range(0, 9999.99, ErrorMessage = "Значение в поле \"Оплата труда, руб\" должно находиться в диапазоне от 0 до 9999.99")]
        public decimal? RateShift { get; set; }

      
        [Range(0, 999999.99, ErrorMessage = "Значение в поле \"Норма расхода горючего\" должно находиться в диапазоне от 0 до 999999.99")]
        public decimal? ConsumptionRate { get; set; }

        [Required(ErrorMessage = "Не введено значение в поле \"Расход горючего, факт.\"")]
        public decimal? ConsumptionFact { get; set; }

           [RegularExpression(@"^\d*$", ErrorMessage = "Значение в поле \"Количество заправок\" должно бьть числовым")]
        [Range(0, 999, ErrorMessage = "Значение в поле \"Количество заправок\" должно находиться в диапазоне от 0 до 999")]
        public decimal? NumRefuel { get; set; }
        public decimal? TaskNum { get; set; }

     //   [Range(0, 9999.99, ErrorMessage = "Значение в поле \"Доп оплата, руб\" должно находиться в диапазоне от 0 до 9999.99")]
        public decimal? RatePiecework { get; set; }

        [Range(0, 99.9, ErrorMessage = "Значение в поле \"Отработано часов\" должно находиться в диапазоне от 0 до 99.9")]
        public decimal? WorkTime { get; set; }

        [Required(ErrorMessage = "Не введено значение в поле \"Норма выработки\"")]
        [Range(0, 9.9, ErrorMessage = "Значение в поле \"Норма выработки\" должно находиться в диапазоне от 0 до 9.9")]
        public decimal? WorkDay { get; set; }

        public int? TypePrice { get; set; }
        public string Comment { get; set; }
        public DictionaryItemsDTO TypeFuel { get; set; }
        public decimal? FuelCost { get; set; }
        public int? TraktorId { get; set; }
        public int? IdRate { get; set; }
        public decimal? MileageCity { get; set; }
        public decimal? Body_lifting { get; set; }
        public decimal? Engine_heating { get; set; }
        public decimal? Engine_heating_hour { get; set; }
        public decimal? Heating { get; set; }
        public decimal? Heating_hour { get; set; }
        public decimal? Icebox { get; set; }
        public decimal? Icebox_hour { get; set; }
        public decimal? Icebox_per { get; set; }
        public decimal? Coef { get; set; }
        public decimal? CoefTrack { get; set; }
        public decimal? CoefField { get; set; }
        public decimal? CoefCity { get; set; }
        public decimal? CoefSetting { get; set; }
        public decimal? Setting { get; set; }
        public decimal? CheckNeedChange { get; set; }
        public int? DemIceBox { get; set; }
        public decimal? Trailerfact { get; set; }
        public decimal? NumFuel { get; set; }
        public decimal? NumSink { get; set; }
        public decimal? RateTrailer { get; set; }
        public decimal? MileageTrailer { get; set; }
        public decimal? CoefTrailer { get; set; }
        public decimal? CoefFuel { get; set; }
        public decimal? CoefSink { get; set; }
        public bool? AllResource { get; set; }
        public int? OrgId { get; set; }

    }
}
