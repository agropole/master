﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogusSrv.DAL.Entities.DTO.DriverWayBills
{
    public class PrintLightAvtoDto
    {
        public int FiledId { get; set;}
        public string NumWayBill { get; set; }
        public DateTime Date { get; set; }
        public string TraktorBrand { get; set; }
        public string TraktorNum { get; set; }
        public string DriverName { get; set; }
        public string DriverLicense { get; set; }
        public string Description { get; set; }
        public string FieldName { get; set; }
        public string Available { get; set; }
        public string Day { get; set; }
        public string Month { get; set; }
        public string Year { get; set; }
        public string StartTime { get; set; }
        public string OrgName { get; set; }
        public string OGRN { get; set; }
        public string Customer { get; set; }
    }
}
