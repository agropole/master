﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using LogusSrv.CORE.REST.Attributes;
using LogusSrv.DAL.Entities.Res;

namespace LogusSrv.DAL.Entities.DTO.DriverWayBills
{
    public class DriverWayBillItemDto
    {
        public int? WayBillId { get; set; }

        public bool DangerCargo { get; set; }

        [Required(ErrorMessage = "Не введено значение в поле \"№ путевого листа\"")]
        [RegularExpression(@"^.{1,20}$", ErrorMessage = "Значение в поле \"№ путевого листа\" не должно превышать 20 символов")]
        public string WayListNum { get; set; }

        [Required(ErrorMessage = "Не выбрано значение в поле \"Дата выдачи\"")]
        public DateTime? DateBegin { get; set; }

        public DateTime? DateEnd { get; set; }

        public int? ShiftTaskId { get; set; }

        [Required(ErrorMessage = "Не выбрано значение в поле \"Водитель\"")]
        public int? DriverId { get; set; }

        public Boolean? IsRepair { get; set; }

        //[Required(ErrorMessage = "Не выбрано значение в поле \"Техника\"")]
        public int? TraktorId { get; set; }

        [Required(ErrorMessage = "Не введено значение в поле \"Начало работы\"")]
        [RegularExpression(@"^(0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$", ErrorMessage = "Значение в поле \"Начало работы\" должно находиться в диапазоне 00:00 - 23:59")]
        public string timeStart { get; set; }

        [Required(ErrorMessage = "Не введено значение в поле \"Окончание работы\"")]
        [RegularExpression(@"^(0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$", ErrorMessage = "Значение в поле \"Окончание работы\" должно находиться в диапазоне 00:00 - 23:59")]
        public string timeEnd { get; set; }

        [Required(ErrorMessage = "Остсутствует информация о пользователе системы (Организация), перелогинтесь или обратитесь в службу поддержки")]
        public int? OrgId { get; set; }
        [Required(ErrorMessage = "Остсутствует информация о пользователе системы , перелогинтесь или обратитесь в службу поддержки")]
        public int? UserId { get; set; }
        public int? AgroreqId { get; set; }

        [NotEmptyList(ErrorMessage = "Не добавлено ни одного задания")] 
        public List<DriverWayBillItemTaskDto> TableTasksData { get; set; }

        [Required(ErrorMessage = "Не введено значение в поле \"Тип топлива\"")]
        public int? TypeFuel { get; set; }
        public bool? AllResource { get; set; }
    }
}