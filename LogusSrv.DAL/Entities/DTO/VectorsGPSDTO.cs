﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity.Spatial;

namespace LogusSrv.DAL.Entities.DTO
{
    public class VectorsGPSDTO
    {
        public DbGeography[] points { get; set; }
        public double length { get; set; }

        public double width { get; set; }

        public double area { get; set; }
        public double[] scalar { get; set; }

        public DateTime date { get; set; }

        public int id_traktor { get; set; }

        public PointDTO p1 { get; set; }
        public PointDTO p2 { get; set; }
        public PointDTO p3 { get; set; }
        public PointDTO p4 { get; set; }

    }
}
