﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogusSrv.DAL.Entities.DTO
{
    public class PrintDTO2
    {
        public int tas_id { get; set; }
        public string q2_field_name { get; set; }
        public int q2_field_id { get; set; }
        public string q2_tptas_name { get; set; }
        public string q2_equip_name { get; set; }
        public string q2_otv_name { get; set; }
        public string q2_agrotech_name { get; set; }
        public string q2_plant { get; set; }
        public decimal? q2_workdays { get; set; }
        public decimal? q2_worktime { get; set; }
        public decimal? q2_price_days { get; set; }
        public int? q2_moto_hours { get; set; }
        public decimal? q2_volume_fact { get; set; }
        public decimal? q2_area { get; set; }
        public decimal? q2_salary { get; set; }
        public string q2_comment { get; set; }
        public string org_name { get; set; }
        public string ogrn { get; set; }
        public string adress { get; set; }
        public decimal? q2_consumption_rate { get; set; }
        public decimal? q2_consumption { get; set; }
        public decimal? q2_consumption_fact { get; set; }
        public decimal? q2_primary_payment { get; set; }
        public decimal? q2_add_price { get; set; }
        public decimal? q2_total_salary { get; set; }
        public decimal? q2_total { get; set; }
        public decimal? q2_price_add { get; set; }

    }
}
