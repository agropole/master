﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LogusSrv.DAL.Entities.DTO;
using LogusSrv.DAL.Entities.Res;

namespace LogusSrv.DAL.Entities.DTO.ActsSzr
{
    public class DictionatysActSzr
    {
        public List<DictionaryItemsDTO> OrganzationsList { get; set; }
        public List<DictionaryItemsDTO> EmployeesList { get; set; }
        public List<DictionaryItemsDTO> EmployeesList1 { get; set; }
        public List<DictionaryItemsDTO> EmployeesList2 { get; set; }
        public List<DictionaryItemsDTO> MaterialsList { get; set; }
        public List<DictionaryItemsDTO> TypeSoilsList { get; set; }
        public List<DictionaryItemsDTO> YearList { get; set; }
        public List<DictionaryItemForFields> FieldsAndPlants { get; set; }
        public List<DictionaryItemsDTO> AllFields { get; set; }
        public List<DictionaryItemsTypeMaterials> TypeMaterials { get; set; }
        public List<DictionaryItemsDTO> Contractors { get; set; }
        public List<DictionaryItemsDTO> VATs { get; set; }
        public List<DictionaryItemsDTO> Type_salary_docs { get; set; }
    }
}
