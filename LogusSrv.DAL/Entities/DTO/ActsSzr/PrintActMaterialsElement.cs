﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogusSrv.DAL.Entities.DTO.ActsSzr
{
    public class PrintActMaterialsElement
    {
        public float? Area { get; set; }
        public float? Count { get; set; }
        public float? Summ { get; set; }
        public int? fielplanId { get; set; }
        public int? mat_mat_id { get; set; }
        public int? fielplan_id{ get; set; }
        public int? tmr_id { get; set; }
    }
}
