﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogusSrv.DAL.Entities.DTO.ActsSzr
{
    public class PrintActInfo
    {
        public int DateD { get; set; }
        public string DateM { get; set; }
        public int DateY { get; set; }
        public DateTime DateT { get; set; }
        public string Organization { get; set; }

        public decimal? Year { get; set; }

        public string SoilTypeName { get; set; }

        public string RecipientName { get; set; }
        public string ResponsibleName { get; set; }
        public string Number { get; set; }
        public List<string> PlantAndFields { get; set; }

        public List<PrintActMaterials> Materials { get; set; }
        public List<PrintActMaterials> Materials2 { get; set; }

    }
}
