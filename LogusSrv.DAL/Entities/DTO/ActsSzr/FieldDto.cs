﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace LogusSrv.DAL.Entities.DTO.ActsSzr
{
    public class FieldDto
    {
        [Required(ErrorMessage = "Не выбрано значение в поле \"Поле\"")]
        public int? Id { get; set; }
        public string Name { get; set; }
    }
}
