﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogusSrv.DAL.Entities.DTO.ActsSzr
{
    public class GetActsSzr
    {
        public int Id { get; set; }
        public Single Area { get; set; }
        public Single Count1 { get; set; }
        public Single ConsumptionRate { get; set; }
        public Single Price1 { get; set; }
        public Single Cost1 { get; set; }
        public uint Id1 { get; set; }
        public string DateBegin { get; set; }
        public string Organization { get; set; }
        public int? OrgId { get; set; }
        public int? TypeAct { get; set; }
        public Nullable<decimal> Year { get; set; }
        public DateTime DateT { get; set; }
        public string SoilTypeName {get;set;}

        public string RecipientName { get; set; }
        public string ResponsibleName { get; set; }
        public string Number { get; set; }
        public Nullable<decimal> count { get; set; }
        public Nullable<decimal> price { get; set; }
        public Nullable<decimal> cost { get; set; }
        public string MaterialName { get; set; }
        public string PlantName { get; set; }
        public string FieldName { get; set; }
        public int EmpId { get; set; }

    }

    public class GetTop10ActsSzr
    {
        public List<ActMaterials> tableData { get; set; }
        public int? Id { get; set; }
        public string DateBegin { get; set; }
        public string Organization { get; set; }
        public Nullable<decimal> Year { get; set; }
        public DateTime DateT { get; set; }
        public int? SoilTypeId { get; set; }
        public int? RecipientId { get; set; }
        public int? ResponsibleId { get; set; }
        public string Number { get; set; }
        public int? OrgId { get; set; }
    }
 
    public class GetJournal
    {
        public int? Id { get; set; }
        public Single Area { get; set; }
        public Single Count1 { get; set; }
        public Single ConsumptionRate { get; set; }
        public Single Cost1 { get; set; }
        public string Organization { get; set; }
        public Nullable<decimal> Year { get; set; }
        public DateTime DateT { get; set; }
        public DateTime DateT1 { get; set; }
        public DateTime Date { get; set; } 
        public string RecipientName { get; set; }
        public Nullable<decimal> count { get; set; }
        public Nullable<decimal> price { get; set; }
        public Nullable<decimal> cost { get; set; }
        public string MaterialName { get; set; }
        public int? MaterialId { get; set; }
        public int? MaterialType { get; set; }
        public int? plantId { get; set; }
        public int? plantId1 { get; set; }
        public string PlantName { get; set; }
        public string PlantSort { get; set; }
        public string FieldName { get; set; }
        public int? PlantSortId { get; set; }
        public int? SortID { get; set; } 
        public string NameTask { get; set; }
        public int TypeTaskId { get; set; }
        public string NameTraktor { get; set; }
    }
    public class GetTMCMao
    {
        public string Material { get; set; }
        public double Count { get; set; }
        public double Cost { get; set; }
        public int? Id { get; set; }
        public int? MaterialType { get; set; }
    }
 
}
