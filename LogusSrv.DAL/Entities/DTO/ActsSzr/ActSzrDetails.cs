﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LogusSrv.DAL.Entities.Res;

namespace LogusSrv.DAL.Entities.DTO.ActsSzr
{
    public class ActSzrDetails
    {
        public int? Id { get; set; }
        public string Name { get; set; }

        [Required(ErrorMessage = "Не выбрано значение в поле \"Дата\"")]
        public DateTime? Date { get; set; }
        public int? OrganizationId { get; set; }
        public decimal? Year { get; set; }
        public int? SoilTypeId { get; set; }
        public int? RecipientId { get; set; }
        [Required(ErrorMessage = "Не выбрано значение в поле \"Получатель\"")]
        public int? ResponsibleId { get; set; }
        public int? ContractorId { get; set; }
        [Required(ErrorMessage = "Не выбрано значение в поле \"Номер Акта\"")]
        public string Number { get; set; }
        public String Date1 { get; set; }
        public bool Income { get; set; }
        public List<ActMaterials> MaterialsList { get; set; }
        public List<Income> ValuesIncome { get; set; }
        public int? Pk { get; set; }
        public bool? AllResource { get; set; }
    }
}
