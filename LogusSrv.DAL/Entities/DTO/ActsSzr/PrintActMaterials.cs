﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogusSrv.DAL.Entities.DTO.ActsSzr
{
    public class PrintActMaterials
    {
        public string Name { get; set; }
        public List<PrintActMaterialsElement> PlantsValue { get; set; }
    }
}
