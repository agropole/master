﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LogusSrv.DAL.Entities.Res;


namespace LogusSrv.DAL.Entities.DTO.ActsSzr
{
    public class ActSzrDetails1
    {
        public List<ActMaterials1> MaterialsList { get; set; }
        public List<ActMaterials2> MaterialsList1 { get; set; }
        public List<ActEmployees> EmployeesList { get; set; }
        public int CountPages { get; set; }
        public int CountItems { get; set; }
        public DateTime? DateT { get; set; }
        public String DateBegin { get; set; }
        public int Pk { get; set; }
        public int? OrgId { get; set; }
        public int? ActId { get; set; }
    }


    public class DeleteRow1
    {
        public ModelDeleteRow1 Document { get; set; }
    }
    public class UpdateRow
    {
        public ModelUpdateRow Document { get; set; }
        public int? ResponsibleId { get; set; }
    }
    public class UpdateComing
    {
        public ActEmployees Document { get; set; }
    }

    public class Motion
    {
        public ActMaterials1 MaterialsList { get; set; }
        public ActEmployees EmployeesList { get; set; }
        public int? Pk { get; set; }
        public int? OrgId { get; set; }
    }
    public class SaveRow
    {
        public ModelSaveRow Document { get; set; }
        public string command { get; set; }
        public int? Id { get; set; }
        public int? Pk { get; set; }
        public string Name { get; set; }

        [Required(ErrorMessage = "Не выбрано значение в поле \"Дата\"")]
        public DateTime? Date { get; set; }
        public int? OrganizationId { get; set; }
        public decimal? Year { get; set; }
        public int? SoilTypeId { get; set; }
        public int? RecipientId { get; set; }
        [Required(ErrorMessage = "Не выбрано значение в поле \"Получатель\"")]
        public int? ResponsibleId { get; set; }
        public int? ContractorId { get; set; }
        [Required(ErrorMessage = "Не выбрано значение в поле \"Номер Акта\"")]
        public string Number { get; set; }

    }
    public class ModelSaveRow
    {
        public int Id { get; set; }
        public int Pk { get; set; }
        public MaterialDto Material { get; set; }
        public PlantDto Plant { get; set; }
        public FieldDto Field { get; set; }
        public Single? Area { get; set; }
        public Single Count { get; set; }
        public Single Cost { get; set; }
        public float ConsumptionRate { get; set; }
        public float? Price { get; set; }

    }
    public class ModelDeleteRow1
    {
        public int Pk { get; set; }
        public int Id { get; set; }
        public MaterialDto Material { get; set; }
        public MaterialDto Recipient { get; set; }
        public MaterialDto Responsible { get; set; }
        public String DateBegin { get; set; }
        public Single Kol { get; set; }
        public Single Price { get; set; }
    }
    public class ModelUpdateRow
    {
        public int id { get; set; }
        public int Id { get; set; }
        public MaterialDto Material { get; set; }
        public PlantDto Plant { get; set; }
        public FieldDto Field { get; set; }
        public Nullable<int> ftp_ftp_id { get; set; }
        public Single area { get; set; }
        public Nullable<decimal> summ { get; set; }
        public Single count { get; set; }
        public Nullable<int> szr_szr_id { get; set; }
        public string code { get; set; }
        public Nullable<decimal> consumption_rate { get; set; }
        public Single price { get; set; }
    }
}
