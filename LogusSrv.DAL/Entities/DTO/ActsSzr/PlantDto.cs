﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace LogusSrv.DAL.Entities.DTO.ActsSzr
{
    public class PlantDto
    {
        [Required(ErrorMessage = "Не выбрано значение в поле \"Культура\"")]
        public int? Id { get; set; }
        public string  Name { get; set; }
    }
}
