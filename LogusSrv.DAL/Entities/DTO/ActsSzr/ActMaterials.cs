﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogusSrv.DAL.Entities.DTO.ActsSzr
{
    public class ActMaterials
    {
        public int Id { get; set; }
        public MaterialDto Material { get; set; }      
        public PlantDto Plant { get; set; }
        public FieldDto Field { get; set; }
        public Single? Area { get; set; }
        public Single Count { get; set; }
        public Single Cost { get; set; }
        public float ConsumptionRate { get; set; }
        public float? Price { get; set; }
    }
    public class ActEmployees
    {
        public string DateBegin  { get; set; }
        public int? Act_Number { get; set; }
        public Single Kol { get; set; }
        public Single Price { get; set; }
        public MaterialDto Recipient { get; set; }
        public MaterialDto Responsible { get; set; }
        public MaterialDto Material { get; set; }
        public int? Pk { get; set; }
       
    }
    public class ActMaterials1
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "Не выбрано значение в поле \"Материал\"")]
        public int? Act_number { get; set; }
        [Required(ErrorMessage = "Не выбрано значение в поле \"Номер акта\"")]
        public MaterialDto Material { get; set; }
        [Required(ErrorMessage = "Не выбрано значение в поле \"Получатель\"")]
        public MaterialDto Recipient { get; set; }
        [Required(ErrorMessage = "Не выбрано значение в поле \"Ответственный\"")]
        public MaterialDto Responsible { get; set; }
        [Required(ErrorMessage = "Не выбрано значение в поле \"Дата\"")]
        [RegularExpression(@"([0-2]\d|3[01])\.(0\d|1[012])\.(\d{4})", ErrorMessage = "Неверный формат в поле \"Дата\"")]
        public String DateBegin { get; set; }
        [Required(ErrorMessage = "Не выбрано значение в поле \"Количество\"")]
        public Single? Kol { get; set; }
        public int Pk { get; set; }

        [Required(ErrorMessage = "Не выбрано значение в поле \"Цена\"")]
        public Single? Price { get; set; }
    }
    public class ActMaterials2
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "Не выбрано значение в поле \"Материал\"")]
        public MaterialDto Material { get; set; }

        [Required(ErrorMessage = "Не выбрано значение в поле \"Ответственный\"")]
        public MaterialDto Responsible { get; set; }
        [Required(ErrorMessage = "Не выбрано значение в поле \"Дата\"")]
        [RegularExpression(@"([0-2]\d|3[01])\.(0\d|1[012])\.(\d{4})", ErrorMessage = "Неверный формат в поле \"Дата\"")]
        public String DateBegin { get; set; }
        [Required(ErrorMessage = "Не выбрано значение в поле \"Количество\"")]
        public Single? Kol { get; set; }
        public int Pk { get; set; }
        public Single? Cost { get; set; }

        [Required(ErrorMessage = "Не выбрано значение в поле \"Цена\"")]
        public Single? Price { get; set; }
    }
}
