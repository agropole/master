﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LogusSrv.DAL.Entities.Req;

namespace LogusSrv.DAL.Entities.DTO.ActsSzr
{
    public class DeleteActSzrResponse : GridDataReq
    {
        public int Id { get; set; }
    }
}
