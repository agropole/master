﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace LogusSrv.DAL.Entities.DTO.ActsSzr
{
    public class MaterialDto
    {
        [Required(ErrorMessage = "Не выбрано значение в поле \"Материал\"")]
        public int? Id { get; set; }
        public string Name { get; set; }

        public decimal? Balance { get; set; }

        public decimal? Price { get; set; }
        public decimal? Sum { get; set; }
        public decimal? Current_price { get; set; }
    }
    public class EmployeerDto
    {
        public int? Id { get; set; }
        public string Name { get; set; }

    }
}
