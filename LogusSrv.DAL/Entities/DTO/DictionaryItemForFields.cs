﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogusSrv.DAL.Entities.DTO
{
    public class DictionaryItemForFields : DictionaryPlantItemsDTO
    {
        public List<DictionaryPlantItemsDTO> Values { get; set; }
    }
}
