﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using LogusSrv.DAL.Entities.Res;

namespace LogusSrv.DAL.Entities.DTO
{
    public class TaskFullDTO
    {
        public int? TaskId { get; set; }
        public int FieldId { get; set; }
        public int? PlantId { get; set; }
        public int? AgroreqId { get; set; }
        public int TaskTypeId { get; set; }
        public int? EquipmentId { get; set; }

        [RegularExpression(@"^\d+.\d{0,2}$", ErrorMessage = "Price must can't have more than 2 decimal places")]
        [Range(0.01, 100, ErrorMessage = "Price must be greater than 0.00")]
        public decimal? WorkTime { get; set; }
        public decimal? WorkDay { get; set; }
        public decimal? PriceDays { get; set; }
        public int? MotoHours { get; set; }
        public decimal? Volumefact { get; set; }
        public decimal? AreaAuto { get; set; }
        public decimal? Area { get; set; }
        public decimal? Salary { get; set; }
        public bool? AllResource { get; set; }
        public decimal? FuelConsumption { get; set; }
        public decimal? RateShift { get; set; }
        public decimal? RatePiecework { get; set; }
        public decimal? Consumption { get; set; }
        public decimal? ConsumptionFact { get; set; }
        public decimal? ConsumptionRate { get; set; }
        public decimal? WialonConsFact { get; set; }
        public int? AvailableId { get; set; }
        public decimal? TaskNum { get; set; }
        public decimal?  TypePrice { get; set; }
        public decimal? PriceAdd { get; set; }
        public decimal? EquipWidth { get; set; }
        public string Comment { get; set; }
        public DictionaryItemsDTO TypeFuel { get; set; }
        public DictionaryItemsDTO Traktor { get; set; }
        public decimal? FuelCost { get; set; }
    }
}
