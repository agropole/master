﻿namespace LogusSrv.DAL.Entities.DTO.DayTaskExt
{
    public class PlantInfoDTO
    {
        public int? Id { get; set; }
        public string Name { get; set; }
        public string RepairStatus { get; set; }
    }
}
