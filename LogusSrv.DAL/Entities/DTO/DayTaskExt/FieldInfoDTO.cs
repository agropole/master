﻿using System.ComponentModel.DataAnnotations;

namespace LogusSrv.DAL.Entities.DTO.DayTaskExt
{
    public class FieldInfoDTO
    {
     //   [Required(ErrorMessage = "Не выбрано значение в поле \"№ поля\"")]
        public int? Id { get; set; }
        public string Name { get; set; }
    }
}
