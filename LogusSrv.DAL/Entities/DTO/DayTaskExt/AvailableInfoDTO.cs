﻿using System.ComponentModel.DataAnnotations;

namespace LogusSrv.DAL.Entities.DTO.DayTaskExt
{
    public class AvailableInfoDTO
    {
      //  [Required(ErrorMessage = "Не выбрано значение в поле \"Ответственный\"")]
        public int? Id { get; set; }
        public string Name { get; set; }
    }
}
