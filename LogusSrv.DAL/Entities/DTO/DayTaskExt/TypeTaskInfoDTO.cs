﻿using System.ComponentModel.DataAnnotations;

namespace LogusSrv.DAL.Entities.DTO.DayTaskExt
{
    public class TypeTaskInfoDTO
    {
      //  [Required(ErrorMessage = "Не выбрано значение в поле \"Планируемые работы\"")]
        public int? Id { get; set; }
        public string Name { get; set; }
    }
}
