﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LogusSrv.DAL.Entities.DTO.DayTaskExt;

namespace LogusSrv.DAL.Entities.DTO
{
    public class DayTaskDTO
    {
        public PlantInfoDTO PlantInfo { get; set; }
        public FieldInfoDTO FieldInfo { get; set; }
        public TypeTaskInfoDTO TypeTaskInfo { get; set; }
        public GetEquipInfoDTO EquipmentsInfo { get; set; }
        public GetEmpInfoDTO EmployeesInfo { get; set; }
        public List<DictionaryItemsDTO> EmpList { get; set; }
        public AvailableInfoDTO AvailableInfo { get; set; }
        public DictionaryItemsDTO AgroreqInfo { get; set; }
        public int? DetailId { get; set; }
        public float? Hours { get; set; }
        public decimal? Summa { get; set; }
        public int Shift { get; set; }
        public decimal? Status { get; set; }
        public string CheckOrange { get; set; }
        public string Comment { get; set; }
        public string Number { get; set; }
        public string NumDailyId { get; set; }
        public string MainComment { get; set; }
    }

}

