﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogusSrv.DAL.Entities.DTO
{
    public class CloseMapInfoDTO
    {
        public string type { get; set; }
        public CloseGeometryInfoDTO geometry { get; set; }
        public CloseMapPropInfoDTO properties { get; set; }
    }
}
