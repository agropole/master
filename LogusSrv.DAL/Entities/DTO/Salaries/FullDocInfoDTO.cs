﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LogusSrv.DAL.Entities.Req.Salaries;

namespace LogusSrv.DAL.Entities.DTO.Salaries
{
    public class FullDocInfoDTO : SaveDocInfoReq
    {
        public List<DetailDocInfoDTO> DetailList { get; set; }
    }
}
