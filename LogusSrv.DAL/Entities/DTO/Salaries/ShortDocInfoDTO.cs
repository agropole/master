﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogusSrv.DAL.Entities.DTO.Salaries
{
    public class ShortDocInfoDTO
    {
        public int SalaryDocId { get; set; }
        public Int64? Summa { get; set; }
        public int? Type { get; set; }
        public DateTime? DateDoc { get; set; }
        public string Date { get; set; }
        public string Number {get;set;}
        public string Organization { get; set; }
        public string Responsible { get; set; }
        public string Comment { get; set; }
    }
}
