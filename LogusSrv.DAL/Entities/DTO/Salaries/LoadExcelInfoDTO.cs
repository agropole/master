﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogusSrv.DAL.Entities.DTO.Salaries
{
    public class LoadExcelInfoDTO
    {
        public string NameFile { get; set; }
        public byte[] Content { get; set; }
    }
}
