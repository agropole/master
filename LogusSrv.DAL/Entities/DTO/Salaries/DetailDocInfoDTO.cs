﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogusSrv.DAL.Entities.DTO.Salaries
{
    public class DetailDocInfoDTO
    {

        public int? detailDocId {get;set;}
        public DateTime? Date { get; set; }
        public string Date1 { get; set; }
        public DateTime? Date2 { get; set; }
        public ElemntInfoDTO Employee { get; set; }
        public ElemntInfoDTO Shift { get; set; }

        public ElemntInfoDTO TypeTask { get; set; }

        public ElemntInfoDTO TypeSalary { get; set; }

        public ElemntInfoDTO Field { get; set; }

        public ElemntInfoDTO Plant { get; set; }

    //    public decimal? FieldDay { get; set; }

    //    public decimal? WorkedTime { get; set; }

        public decimal? Value { get; set; }

        public decimal? Prices { get; set; }

        public decimal? Sum { get; set; }

        public string Comment { get; set; }

        public decimal? Proizv { get; set; }
    }
}
