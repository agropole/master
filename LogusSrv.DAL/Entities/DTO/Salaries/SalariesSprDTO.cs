﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogusSrv.DAL.Entities.DTO.Salaries
{
    public class SalariesSprDTO
    {
        public List<DictionaryItemsDTO> TypeDocList { get; set; }
        public List<DictionaryItemsDTO> ResponsibleList { get; set; }
        public List<DictionaryItemsDTO> ShiftList { get; set; }
        public List<DictionaryItemsDTO> OgranizationList { get; set; }
        public List<DictionaryItemsDTO> ObjectList { get; set; }
        public List<DictionaryItemsDTO> TasksList { get; set; }
        public List<DictionaryItemForFields> FieldsAndPlants { get; set; }
        public List<DictionaryItemsDTO> PlantsByYear { get; set; }

    }
}
