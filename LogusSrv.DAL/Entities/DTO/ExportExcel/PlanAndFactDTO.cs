﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogusSrv.DAL.Entities.DTO.ExportExcel
{
    public class PlanAndFactDTO : PrintShiftDTO
    {
        public int Id { get; set; }
        public string AreaPlant { get; set; }
        public List<FactDTO> Fact { get; set; }
    }
}
