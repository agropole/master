﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogusSrv.DAL.Entities.DTO.ExportExcel
{
    public class ExpensesResultDto
    {
        public string name { get; set; }
        public decimal? consumption { get; set; }
        public ExpensesDto t1 { get; set; }
        public ExpensesDto t2 { get; set; }
        public ExpensesDto t3 { get; set; }
        public ExpensesDto t4 { get; set; }

        public decimal? sh_tech { get; set; }

        public decimal? cargo_tech { get; set; }

        public int PlantId { get; set; }
    }
}
