﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogusSrv.DAL.Entities.DTO.ExportExcel
{
    public class PrintRefueilngsDto
    {
        public decimal? TotalFuelIssued { get; set; }
        public decimal? TotalConsumptionFact { get; set; }
        public decimal? TotalMotoWork { get; set; }

        public decimal? TotalAzs { get; set; }
        public decimal? TotalCopylov { get; set; }
        public decimal? TotalVinokurova { get; set; }
        public decimal? TotalSlepokurov { get; set; }

        public List<PrintRefuilItemDto> items { get; set; }
    }
}
