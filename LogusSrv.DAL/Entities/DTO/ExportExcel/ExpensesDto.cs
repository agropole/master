﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogusSrv.DAL.Entities.DTO.ExportExcel
{
    public class ExpensesDto
    {
        
         public decimal? shtr_type {get;set;}
         public decimal? volume_fact {get;set;}
         public decimal? work_time {get;set;}

         public decimal? area {get;set;}
         public decimal? work_days {get;set;}

         public decimal? type_price {get;set;}
         public decimal? price_add {get;set;}

         public decimal? salary {get;set;}

         public decimal? salary_main { get; set; }
         public decimal? salary_dop { get; set; }
         public decimal? salary_total { get; set; }
          
    }
}
