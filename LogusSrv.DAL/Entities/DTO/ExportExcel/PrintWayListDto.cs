﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogusSrv.DAL.Entities.DTO.ExportExcel
{
    public class PrintWayListDto
    {
          public int ShtrId { get; set; }
          public int TaskId { get; set; }
          public string TracktorName { get; set; }
          public int? TracktorId { get; set; }
          public string EquipmentName { get; set; }
          public int? EquipmentId { get; set; }
          public DateTime Date {get;set;}
          public string NumList {get;set;}
          public string TypeTask {get;set;}
          public string FieldName {get;set;}
          public int FieldId { get; set; }
          public string DriverName { get; set; }
          public string ResponsibleName { get; set; }
          public string PlantName {get;set;}
          public int ShiftNum {get;set;}
          public decimal? TypePrice { get; set; }
          public string TypePriceName { get; set; }
          public decimal? Worktime {get;set;}
          public decimal? Area {get;set;}
          public decimal? AreaAuto { get; set; }
          public decimal?  Volumefact {get;set;}
          public decimal?  PriceAdd {get;set;}
          public decimal? WorkDay {get;set;}
          public decimal?  Salary {get;set;}
          public decimal? PriceTotal { get; set; }
          public decimal? Ind { get; set; }
          public decimal? MileageTotal { get; set; }
          public decimal? MileageCargo { get; set; }
          public decimal? ConsumptionFact { get; set; }
          public decimal? ConsumptionAuto { get; set; }
          public decimal? ConsumptionRate { get; set; }
          public decimal? MotoHours { get; set; }
          public string Comment { get; set; }
          public decimal? FuelIssued { get; set; }
          public decimal? NumRuns { get; set; }
          public decimal? ValumeFact { get; set; }
          public decimal? MileageOnTrack { get; set; }
          public decimal? MileageOnGround { get; set; }
          public decimal? NumRefuel { get; set; }
          public decimal? NumLifts { get; set; }
          public DictionaryItemsDTO TypeFuel { get; set; }
    
    }
}
