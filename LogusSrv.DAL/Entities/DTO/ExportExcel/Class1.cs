﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogusSrv.DAL.Entities.DTO.ExportExcel
{
    public class GrpTaskForTimeSheet
    {
        public decimal? worktime { get; set; }

        public DateTime? date { get; set; }
        public decimal? status { get; set; }
        public string Employees { get; set; }
        public int? emp_emp_id { get; set; }
        public int? type_emp_id { get; set; }
    }
}
