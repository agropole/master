﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogusSrv.DAL.Entities.DTO.ExportExcel
{
    public class ExpensesRequestDto
    {
        public decimal cost_salary { get; set; }
        public decimal? cost_fuel { get; set; }
        public int plant_id { get; set; }
        public int fiel_fiel_id { get; set; }
        public decimal Id { get; set; }
        public double? area { get; set; }
        public decimal area1 { get; set; }
        public string plant_name { get; set; }
        public string fields_name { get; set; }
        public decimal? minud_time { get; set; }
        public float? minud_fact { get; set; }
        public float? plan_minud { get; set; }
        public decimal? szr_time { get; set; }
        public float? szr_fact { get; set; }
        public float? plan_szr { get; set; }
        public float? dop_material_fact { get; set; }
        public float? plan_dop_material { get; set; }
        public float? posevmat_fact { get; set; }
        public float? plan_posevmat { get; set; }
        public decimal? vyvozprod_fact { get; set; }
        public double? razx_top { get; set; }
        public double? plan_razx_top { get; set; }
        public decimal? sx_tex { get; set; }
        public decimal? gruz_tex { get; set; }
        public decimal? salary_main { get; set; }
        public decimal? salary_dop { get; set; }
        public double? salary_total { get; set; }
        public double? plan_salary_total { get; set; }
        public double? service_total { get; set; }
        public double? plan_service_total { get; set; }
        public decimal? salary_total1 { get; set; }
        public float? gross_harvest { get; set; }
        public double? total_fact { get; set; }
        public double? nzp { get; set; }
        public float? total_plan { get; set; }
        public float? total_delta { get; set; }
        public double? cost_ga_fact { get; set; }
        public double? cost_ga_plan { get; set; }
        public double? cost_ga_delta { get; set; }
        public double? cost_t_fact { get; set; }
        public double? cost_t_plan { get; set; }
        public double? cost_t_delta { get; set; }
        public double? szr { get; set; }
        public double? chemicalFertilizers { get; set; }
        public double? seed { get; set; }
        public decimal szr1 { get; set; }
        public decimal chemicalFertilizers1 { get; set; }
        public decimal seed1 { get; set; }
        public decimal dopmaterial { get; set; }
        public Int64 docSumma { get; set; }
        public decimal docSumma1 { get; set; }
    }
    public class PlanRequestDto
    {                             
        public string  plant_name { get; set; }
        public int  id { get; set; }
        public int plant_id { get; set; }
        public int? tpsort_id { get; set; }
        public int? field_id { get; set; }
        public int id_tc { get; set; }
        public float? id_tc_param1 { get; set; }
        public float? value { get; set; }
        public decimal?  area  { get; set; }
        public double? area1 { get; set; }
        public float?  productivity { get; set; }
        public float?  gross_harvest { get; set; }
        public decimal?  nzp { get; set; }
        public decimal? salary { get; set; }
        public float? salary1 { get; set; }
        public decimal? dopsalary { get; set; }
        public decimal? salary_watering { get; set; }
        public decimal? dopsalary_watering { get; set; }
        public decimal? deduction_watering { get; set; }
        public decimal? fuel_cost { get; set; }
        public float? fuel_cost1 { get; set; }
        public decimal? consumption { get; set; }
         public float? consumption1 { get; set; }
         public decimal? fuel_cost_watering { get; set; }
         public decimal? consumption_watering { get; set; }
         public decimal? deduction { get; set; }
         public decimal? total_salary { get; set; }
         public float? fuel_cost_ga { get; set; }
         public float? fuel_cost_ga_watering { get; set; }
         public decimal? total_consumption { get; set; }
         public decimal? total_cost { get; set; }
         public decimal? seed { get; set; }
         public float? seed1{ get; set; }
         public decimal? szr { get; set; }
         public float?  szr_ga { get; set; }
         public decimal? chemicalFertilizers { get; set; }
         public float?  chemicalFertilizers_ga { get; set; }
         public decimal? combine { get; set; }
         public decimal? loader { get; set; }
         public decimal? autotransport { get; set; }
         public decimal? desiccation { get; set; }
         public decimal? analyzes { get; set; }
         public decimal? total_service { get; set; }
         public decimal? total { get; set; }
         public double? total_ga { get; set; }
         public double? total_t { get; set; }
         public double? nzp_chemicalFertilizers { get; set; }
         public double? nzp_dop_material { get; set; }
         public double? nzp_fuel { get; set; }
         public double? nzp_seed { get; set; }
         public double? nzp_szr { get; set; }
         public double? nzp_zp { get; set; }
         public decimal? dop_material { get; set; }
         public double? dop_material_ga { get; set; }
         public decimal? grtask_id { get; set; }
    }

    public class PlanMonthRequestDto
    {
        public DateTime? date_start { get; set; }
        public DateTime? date_finish { get; set; }
        public float? avg  { get; set; }
        public float? val { get; set; }
        public int? kol { get; set; }
        public int? id { get; set; }
        public int? id_tc_operation { get; set; }
        public int? id_tc { get; set; }
        public decimal? grtask_id { get; set; }
       public int id_tc_param { get; set; }
       public int? mat_type_id { get; set; }
    }
    public class ExpensesRequestModel
    {
        public int? year { get; set; }
        public int? plant_id { get; set; }
    }

    public class PlanFactTmcModel
    {
        public int id { get; set; }
        public int id_tc_param { get; set; }
        public float? value { get; set; }
        public int? field_id { get; set; }
        public int? material_id { get; set; }
        public int plant_id { get; set; }
        public string material_name_plan { get; set; }
        public string plant_name_plan { get; set; }
        public string field_name_plan { get; set; }
        public decimal? area_plant_plan { get; set; }
        public decimal? summa_kol_plan { get; set; }
        public decimal? kol_plan { get; set; }
        public decimal? total_sum_plan { get; set; }
        public decimal? summa_ga_plan { get; set; }
        public decimal? consumption { get; set; }
    }
    public class FactTmcModel //к
    {
        public int id { get; set; }
        public int? material_id { get; set; }
        public int plant_id { get; set; }
        public string material_name { get; set; }
        public string plant_name { get; set; }
        public decimal? area_plant { get; set; }
        public decimal? summa_kol { get; set; }
        public decimal? kol { get; set; }
        public decimal? total_sum { get; set; }
        public decimal? summa_ga { get; set; }
        public string field_name { get; set; }
        public int field_id { get; set; }
        public decimal? consumption { get; set; }
    }
    public class SevooborotModel
    {
        public int id { get; set; }
        public float? consumption { get; set; }
        public float? total_salary { get; set; }
        public float? dop_salary { get; set; }
        public decimal? consumption_fact { get; set; }
        public float? szr { get; set; }
        public float? chemicalFertilizers { get; set; }
        public float? seed { get; set; }
        public float? dopmaterial { get; set; }
        public int? field_id { get; set; }
        public int? plant_id { get; set; }
        public float? sum_area { get; set; }
        public decimal? dop_area { get; set; }
        public decimal full_area { get; set; }
        public decimal? area_plant { get; set; }
        public float? total_sal1 { get; set; }
        public float? deduction { get; set; }
        public string field_name { get; set; }
        public string plant_name { get; set; }
        public int month { get; set; }
        public int? type_material { get; set; }
        public int? tptas_id { get; set; }
        public string name_task { get; set; }
        public List<TasksSevModel> tasksList { get; set; }
    
    }
    public class SevModelMonth : SevooborotModel
    {
        public List<SevooborotModel> total_salary_list { get; set; }
        public List<SevooborotModel> dop_salary_list { get; set; }
        public List<SevooborotModel> tmc_list { get; set; }
    }

    public class TasksSevModel : SevooborotModel
    { 

    }

    public class ExpensesNZPModel
    {
        public List<ExpensesRequestDto> fuel_salary { get; set; }
    }
}
