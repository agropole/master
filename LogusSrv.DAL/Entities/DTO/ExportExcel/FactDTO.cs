﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LogusSrv.DAL.Entities.DTO.ExportExcel
{
    public class FactDTO : PrintShiftDTO
    {
        public decimal? AreaPlant { get; set; }
        public decimal? WorkTime { get; set; }

        public decimal? Salary { get; set; }

        public decimal? ConsumptionFact { get; set; }
        public decimal? Area { get; set; }
     
    }
}