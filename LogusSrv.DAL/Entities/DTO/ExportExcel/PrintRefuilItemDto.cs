﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogusSrv.DAL.Entities.DTO.ExportExcel
{
    public class PrintRefuilItemDto
    {
          public decimal? Ind { get; set; }
          public int ShtrId {get;set;}
          public DateTime Date {get;set;}
          public string  NumList {get;set;}
          public string  DriverName {get;set;}
          public string TechName { get; set; }
          public string RefuelerName { get; set; }
          public List<RefuelerItemDto> RefuelerList { get; set; }
          public decimal? FuelLeft {get;set;}
          public decimal? FuelRemain {get;set;}
          public decimal? ConsumptionFact {get;set;}
          public decimal? MotoStart {get;set;}
          public decimal? MotoEnd {get;set;}
          public decimal? MotoWork  {get;set;}
          public decimal? FuelIssued {get;set;}
          public string  TypeTask {get;set;}
          public List<string> TypeTaskList { get; set; }
          public int? ShtrType { get; set; }
      
    }
    public class RefuelerItemDto : PrintRefuilItemDto
    { 
    
    }
}
