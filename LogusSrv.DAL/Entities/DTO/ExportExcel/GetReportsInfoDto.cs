﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LogusSrv.DAL.Entities.DTO;
using LogusSrv.DAL.Entities.SQL;

namespace LogusSrv.DAL.Entities.DTO.ExportExcel
{
    public class GetReportsInfoDto
    {
        public List<DictionaryItemsTraktors> TraktorsList { get; set; }

        public List<DictionaryItemsDTO> OrganizationsList { get; set; }
        public List<DictionaryItemsDTO> employeerList { get; set; }
        public List<DictionaryItemForFields> FieldsAndPlants { get; set; }
        public List<DictionaryItemForFields> PlantsToFields { get; set; }
        public List<DictionaryItemForFields> AllPlants { get; set; }
        public List<DictionaryItemForFields> AllPlantsWriteOff { get; set; }
        public List<DictionaryItemsDTO> TmcList { get; set; }
       
    }
    public class RespReq
    {
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
    }
    public class CultReq
    {
        public int? Year { get; set; }
    }
    public class Resp
    {
        public List<DictionaryItemsDTO> ResponsibleList { get; set; }
    }
    public class Cult
    {
        public List<DictionaryItemsDTO> CultList { get; set; }
    }

}
