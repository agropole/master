﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity.Spatial;

namespace LogusSrv.DAL.Entities.DTO
{
    public class AutogenerateTask
    {
        public int? tas_id { get; set; }
        public int? fiel_fiel_id { get; set; }
        public int? plant_plant_id { get; set; }
        public int? shtr_shtr_id { get; set; }
        public int? tpas_tpas_id { get; set; }
        public int? emp_emp_id { get; set; }
        public DateTime date_begin { get; set; }
        public DateTime date_end { get; set; }
        public decimal? width { get; set; }
        public decimal? status { get; set; }
        public double area { get; set; }
        public int? track_id { get; set; }
        public int? trakt_id { get; set; }
        public int? equi_equi_id { get; set; }
    }

    public class FieldsForAutogenerate
    {
        public int fiel_id { get; set; }
        public int count { get; set; }
    }

}
