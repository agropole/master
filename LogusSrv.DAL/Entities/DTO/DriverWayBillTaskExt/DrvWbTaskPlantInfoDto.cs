﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogusSrv.DAL.Entities.DTO.DriverWayBillTaskExt
{
    public class DrvWbTaskPlantInfoDto
    {
        public int? Id { get; set; }
        public string Name { get; set; }
        public string RepairStatus { get; set; }
    }
}
