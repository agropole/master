﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogusSrv.DAL.Entities.DTO.DriverWayBillTaskExt
{
    public class DrvWbTaskAvailableInfoDto
    {
      //  [Required(ErrorMessage = "Не выбрано значение в поле \"Ответственный\"")]
        public int? Id { get; set; }
        public string Name { get; set; }
    }
}
