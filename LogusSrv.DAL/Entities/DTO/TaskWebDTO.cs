﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace LogusSrv.DAL.Entities.DTO
{
    public class TaskWebDTO
    {
        [Required]
        public  int FieldId { get; set; }
        public decimal? SatusId { get; set; }
        [Required]
        public int TaskType { get; set; }
        [Required]
        public int AvailableId { get; set; }

        public int? TaskId { get; set; }
    }
}
