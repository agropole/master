﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogusSrv.DAL.Entities.DTO.FileArchive
{
    public class FileUploadDto
    {
        public Guid? id { get; set; }
        public string file_name { get; set; }
        public byte[] contents { get; set; }
    }
}
