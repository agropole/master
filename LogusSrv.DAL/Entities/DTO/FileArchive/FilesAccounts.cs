﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogusSrv.DAL.Entities.DTO.FileArchive
{
    public class FilesAccounts
    {
        public Guid file_main_id { get; set; }
        public byte[] file_main_content {get; set;}
        public string file_main_name { get; set; } 
        public Guid file_patient_id {get; set;}
        public byte[] file_patient_content { get; set; }
        public Guid period_id { get; set; }
        public Guid area_id { get; set; }
        public string error_text { get; set; }
    }
}
