﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogusSrv.DAL.Entities.DTO.FileArchive
{
    public class FilesLoadStatus
    {
        public string name { get; set; }
        public string status { get; set; }
        public string erorr_text { get; set; }
    }
}
