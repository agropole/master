﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogusSrv.DAL.Entities.DTO.ExportExcel
{
    public class ExpensesRequestDto
    {
        public decimal cost_salary { get; set; }
        public decimal cost_fuel { get; set; }
        public int plant_id { get; set; }
        public int fiel_fiel_id { get; set; }
        public decimal Id { get; set; }
        public double? area { get; set; }
        public decimal area1 { get; set; }
        public string plant_name { get; set; }
        public string fields_name { get; set; }
        public decimal? minud_time { get; set; }
        public float? minud_fact { get; set; }
        public decimal? szr_time { get; set; }
        public float? szr_fact { get; set; }

        public float? posevmat_fact { get; set; }
        public decimal? vyvozprod_fact { get; set; }
        public double? razx_top { get; set; }
        public decimal? sx_tex { get; set; }
        public decimal? gruz_tex { get; set; }

        public decimal? salary_main { get; set; }
        public decimal? salary_dop { get; set; }
        public double? salary_total { get; set; }
        public decimal salary_total1 { get; set; }
        public float? gross_harvest { get; set; }
        public double? total_fact { get; set; }
        public float? total_plan { get; set; }
        public float? total_delta { get; set; }

        public double? cost_ga_fact { get; set; }
        public double? cost_ga_plan { get; set; }
        public double? cost_ga_delta { get; set; }

        public double? cost_t_fact { get; set; }
        public double? cost_t_plan { get; set; }
        public double? cost_t_delta { get; set; }
        public double? szr { get; set; }
        public double? chemicalFertilizers { get; set; }
        public double? seed { get; set; }
        public decimal szr1 { get; set; }
        public decimal chemicalFertilizers1 { get; set; }
        public decimal seed1 { get; set; }
        public Int64 docSumma { get; set; }
        public decimal docSumma1 { get; set; }
    }

    public class ExpensesRequestModel
    {
        public int? year { get; set; }
        public int? plant_id { get; set; }
    }

    public class ExpensesNZPModel
    {
        public List<ExpensesRequestDto> fuel_salary { get; set; }
    }
}
