﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogusSrv.DAL.Entities.DTO
{
    public class RefuelingDTO
    {
        public int? RefuelId { get; set; }

      //  [RegularExpression(@"^\d{1,5}(\,\d{1,2})?$", ErrorMessage = "Значение в поле \"Заправка\" не должно превышать 99999.99 и иметь больше двух знаков после запятой")]
        public decimal? Volume { get; set; }

        public int? NameId { get; set; }
        public int OrderNum { get; set; }
    }
}
