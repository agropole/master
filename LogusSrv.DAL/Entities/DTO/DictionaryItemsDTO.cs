﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogusSrv.DAL.Entities.DTO
{
    public class DictionaryItemsDTO
    {
        public string Name { get; set; }
        public int? Id { get; set; }
        public bool? Bold { get; set; }
        public float? Price { get; set; }
        public float? Balance { get; set; }
        public decimal? Current_price { get; set; }
        public string Description { get; set; }
        public string Manufacturer { get; set; }
        public decimal Area_plant { get; set; }
        public decimal Area { get; set; }
        public int? index { get; set; }
        public int? emp_emp_id { get; set; }
        public string RepairStatus { get; set; }
          
    }

    public class DictionaryMaterialDTO
    {
        public string Name { get; set; }
        public int? Id { get; set; }
        public bool? Bold { get; set; }
        public int? Mat_type_id { get; set; }
        public decimal Price { get; set; }
        public decimal? Balance { get; set; }

    }


    public class TechDictionaryItemsDTO : DictionaryItemsDTO
    {
        public int? IdGroup { get; set; }
    }
}
