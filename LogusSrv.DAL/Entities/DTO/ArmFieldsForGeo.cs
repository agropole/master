﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity.Spatial;

namespace LogusSrv.DAL.Entities.DTO
{
    public class ArmFieldsForGeo
    {
        public int id { get; set; }
        public int? status { get; set; }

        public string coord { get; set; }

        public DbGeography center { get; set; }

        public string name { get; set; }

        public decimal? area { get; set; }

        public List<PlantsDTO> plants { get; set; }
    }
}
