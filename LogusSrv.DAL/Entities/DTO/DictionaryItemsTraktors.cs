﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogusSrv.DAL.Entities.DTO
{
    public class DictionaryItemsTraktors : DictionaryItemsDTO
    {
        public string RegNum { get; set; }
        public string TypeTraktor { get; set; }
        public DictionaryItemsDTO Name_fuel { get; set; }
        public decimal Width { get; set; }
        public int? TrackId { get; set; }
        public int? TypeId { get; set; }
        public string Model { get; set; }

    }
}
