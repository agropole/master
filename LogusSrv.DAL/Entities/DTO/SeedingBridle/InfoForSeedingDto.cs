﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LogusSrv.DAL.Entities.DTO;

namespace LogusSrv.DAL.Entities.DTO.SeedingBridle
{
    public class InfoForSeedingDto
    {
        public List<DictionaryItemsDTO> OrgList { get; set; }
        public List<DictionaryItemsDTO> PlantsList { get; set; }
        public List<DictionaryItemsDTO> FieldList { get; set; }
        public List<DictionaryItemsDTO> yearList { get; set; }

        public List<DictionaryItemsDTO> IndicatorsList { get; set; }
    }
}
