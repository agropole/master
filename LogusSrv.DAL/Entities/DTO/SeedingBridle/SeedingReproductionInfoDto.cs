﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogusSrv.DAL.Entities.DTO.SeedingBridle
{
    public class SeedingReproductionInfoDto
    {
        [Required(ErrorMessage = "Не выбрано значение в поле \"Репродукция\"")]
        public int? Id { get; set; }
        public string Name { get; set; }
    }
}
