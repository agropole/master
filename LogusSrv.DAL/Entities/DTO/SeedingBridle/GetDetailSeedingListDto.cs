﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LogusSrv.DAL.Entities.DTO;


namespace LogusSrv.DAL.Entities.DTO.SeedingBridle
{
    public class GetDetailSeedingListDto
    {
         //public int? FieldPlantId { get; set; }
   
         public SeedingSortInfoDto Sort { get; set; }
         public SeedingReproductionInfoDto Reproduction { get; set; }
         public SeedingIndicatorInfoDto Area { get; set; }
         public SeedingIndicatorInfoDto Harvest { get; set; }
         public SeedingIndicatorInfoDto CostTonn { get; set; }
         public SeedingIndicatorInfoDto CostGA { get; set; }
                       
    }
}
