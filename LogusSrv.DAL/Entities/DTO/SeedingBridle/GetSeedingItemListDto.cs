﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogusSrv.DAL.Entities.DTO.SeedingBridle
{
    public class GetSeedingItemListDto
    {
        public string PlantName { get; set; }
        public decimal? Fact { get; set; }

        public decimal? Plan { get; set; }

        public int? Status { get; set; } 
    }
}
