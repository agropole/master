﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LogusSrv.DAL.Entities.DTO.DayTaskExt;

namespace LogusSrv.DAL.Entities.DTO.SeedingBridle
{
    public  class GetSeedingListDto
    {

        public string FieldName { get; set; }
        public  FieldInfoDTO Field { get; set; }

        public List<GetSeedingItemListDto> year1 { get; set; }
        public List<GetSeedingItemListDto> year2 { get; set; }
        public List<GetSeedingItemListDto> year3 { get; set; }
        public List<GetSeedingItemListDto> year4 { get; set; }
        public List<GetSeedingItemListDto> year5 { get; set; }
    //    public List<GetSeedingItemListDto> year6 { get; set; }
     //   public List<GetSeedingItemListDto> year7 { get; set; }
    }
}
