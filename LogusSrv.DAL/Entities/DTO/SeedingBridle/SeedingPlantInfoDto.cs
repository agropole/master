﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogusSrv.DAL.Entities.DTO.SeedingBridle
{
    public class SeedingPlantInfoDto
    {
        [Required(ErrorMessage = "Не выбрано значение в поле \"Культура\"")]
        public int? Id { get; set; }
        public string Name { get; set; }
    }
}
