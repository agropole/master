﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogusSrv.DAL.Entities.DTO.SeedingBridle
{
    public class SeedingIndicatorInfoDto
    {
       // [Required(ErrorMessage = "Не выбрано значение в поле \"Поле\"")]

       // [RegularExpression(@"^\d*(\.\d{1,2})?$", ErrorMessage = "Значение в поле \"Площадь\" или \"Урожайность\" или \"Затраты, руб./га\" или \"Затраты, руб./тн\" не должно иметь больше двух знаков после запятой")]
        [Range(0, 9999999999.99, ErrorMessage = "Значение в поле \"Площадь\" или \"Урожайность\" или \"Затраты, руб./га\" или \"Затраты, руб./тн\"  не должно находиться в диапазоне от 0 до 9999999999.99")]
        public decimal? Fact { get; set; }

      //  [RegularExpression(@"^\d*(\.\d{1,2})?$", ErrorMessage = "Значение в поле \"Площадь\" или \"Урожайность\" или \"Затраты, руб./га\" или \"Затраты, руб./тн\" не должно иметь больше двух знаков после запятой")]
        [Range(0, 9999999999.99, ErrorMessage = "Значение в поле \"Площадь\" или \"Урожайность\" или \"Затраты, руб./га\" или \"Затраты, руб./тн\"  не должно находиться в диапазоне от 0 до 9999999999.99")]
        public decimal? Plan { get; set; }
    }
}
