﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LogusSrv.DAL.Entities.Res;

namespace LogusSrv.DAL.Entities.DTO.SeedingBridle
{
    public class DictionaryItemForPlants : DictionaryItemsDTO
    {
        public List<DictionaryItemsDTO> Values { get; set; }
    }
}
