﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LogusSrv.DAL.Entities.DTO;

namespace LogusSrv.DAL.Entities.DTO.SeedingBridle
{
    public class DictionaryItemFields : DictionaryItemsDTO
    {
        public decimal Area { get; set; } 
    }
}
