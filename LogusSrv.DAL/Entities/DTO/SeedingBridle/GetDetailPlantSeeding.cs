﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogusSrv.DAL.Entities.DTO.SeedingBridle
{
    public class GetDetailPlantSeeding
    {
        public int? FieldPlantId { get; set; }
        public int? Status { get; set; }
        public SeedingPlantInfoDto Plant { get; set; }
        public List<GetDetailSeedingListDto> Sorts { get; set; }
    }
}
