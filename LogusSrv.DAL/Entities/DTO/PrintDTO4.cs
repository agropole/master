﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogusSrv.DAL.Entities.DTO
{
    public class PrintDTO4
    {
        public decimal? q4_volume { get; set; }
        public string q4_name { get; set; }
    }
}
