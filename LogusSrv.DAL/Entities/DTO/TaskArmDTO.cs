﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace LogusSrv.DAL.Entities.DTO
{
    public class TaskArmDTO 
    {

       public int field_id { get; set; }
       public int type_task_id { get; set; }
       public int? emp_emp_id { get; set; }
       public string empName { get; set; }
       public int shiftNum { get; set; }
       public int? equipment_id { get; set; }
       public int? task_id { get; set; }
       public string field_name {get;set;}
       public string plant_name {get;set;}
       public string plant_icon_path { get; set; }
       public string task_name{get;set;}
       public int? status { get; set; }
       public DateTime? date_begin {get;set;}
       public decimal width { get; set; }
       public string equipment_name { get; set; }
       public DateTime? date_end { get; set; }
       public int? way_list_id { get; set; }
    }
}