﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogusSrv.DAL.Entities.DTO
{
    public class GPSDTO
    {
        public int track_id { get; set; }
        public decimal lat { get; set; }
        public decimal lon { get; set; }
        public decimal time { get; set; }
        public decimal? direction { get; set; }
        public decimal? speed { get; set; }
        public decimal? mileage { get; set; }
    }

    public class GPSDTOfromDriver
    {
        public string req_id { get; set; }
        public string time { get; set; }
        public int task_id { get; set; }
        public int track_id { get; set; }
        public decimal lat { get; set; }
        public decimal lon { get; set; }
        public decimal? direction { get; set; }
        public decimal? speed { get; set; }
        public decimal? mileage { get; set; }
    }
}
