﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace LogusSrv.DAL.Entities.Req.CloseWaysList
{
    public class NewFieldsReq
    {
        [Required(ErrorMessage = "Не введено значение в поле \"Имя поля\"")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Не введено значение в поле \"Организация\"")]
        public int? OrgId { get; set; }
        [Required(ErrorMessage = "Не введено значение в поле \"Культура\"")]
        public int? PlantId { get; set; }

        [Required(ErrorMessage = "Не введено значение в поле \"Плошадь\"")]
        public decimal? Area { get; set; }

        [Required(ErrorMessage = "Не выбрано значение в поле \"Координаты поля\"")]
        public List<string> Coordinates { get; set; }

        [Required(ErrorMessage = "Не выбрано значение в поле \"Центр поля\"")]
        public string Center { get; set; }

        [Required(ErrorMessage = "Не выбрано значение в поле \"Дата\"")]
        public DateTime Date { get; set; }
    }
}
