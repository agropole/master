﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LogusSrv.DAL.Entities.DTO;

namespace LogusSrv.DAL.Entities.Req
{
    public class UpdateDayTaskReq
    {
        public DateTime Date { get; set; }
        public DayTaskDTO ShiftInfo { get; set; }
        public int OrgId { get; set; }
        public int? FilterOrg { get; set; }
        public bool? Traktors { get; set; }
        public bool? Fields_to_plants { get; set; }
        public bool? Equipments { get; set; }
        public bool? Employees { get; set; }
        public bool? Type_tasks { get; set; }
        public bool? Fields { get; set; }
        public bool? Plants { get; set; }
        public bool? Agrotech_requirment { get; set; }
    }
}
