﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace LogusSrv.DAL.Entities.Req
{
    public class UserReq
    {
        [Required]
        public int OrgId { get; set; }
    }
}
