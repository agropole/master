﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogusSrv.DAL.Entities.Req
{
    public class Top100GPSReq
    {
        public int track_id { get; set; }
        public int take_num { get; set; }
        public string start_time { get; set; }
        public string end_time { get; set; }
        public bool? stop { get; set; }
        public bool? parking { get; set; }
    }
}
