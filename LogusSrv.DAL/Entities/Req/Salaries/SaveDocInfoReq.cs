﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogusSrv.DAL.Entities.Req.Salaries
{
    public class SaveDocInfoReq
    {
        public int? SalaryDocId { get; set; }
        public int? TypeId { get; set; }
        public string Date { get; set; }
        public DateTime? DateBegin { get; set; }
        public int? ResponsibleId { get; set; }
        public int? OrgId { get; set; }
        public string Number { get; set; }
        public string Comment { get; set; }
        public string ResponsibleName { get; set; }

    }
}

