﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LogusSrv.DAL.Entities.DTO;
using LogusSrv.DAL.Entities.Res;

namespace LogusSrv.DAL.Entities.Req
{
    public class KPIReqModel
    {
        public int year { get; set; }
        public int id_tc { get; set; }
        public int? id_tc_operation { get; set; }
        public int? id_type_task { get; set; }
    }

    public class TasksToUpdate
    {
        public List<DictionaryItemsDTO> tasks { get; set; }
        public KPIReqModel req { get; set; }
    }

    public class SaveKPIReqModel
    {
        public int TaskId { get; set; }
        public int TotalRate { get; set; }
        public List<KPIValueResModel> ListSeleted { get; set; }
    }
}
