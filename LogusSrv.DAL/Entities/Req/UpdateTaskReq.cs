﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LogusSrv.DAL.Entities.DTO;
using System.ComponentModel.DataAnnotations;

namespace LogusSrv.DAL.Entities.Req
{
    public class UpdateTaskReq : GetWaysReq
    {

        public int? TaskId { get; set; }

        [Required(ErrorMessage = "Не выбрано значение в поле \"Поле\"")]
        public int? FieldId { get; set; }

        public int? TypePrice { get; set; }

        public int? PlantId { get; set; }

        public int? AvailableId { get; set; }

        [Required(ErrorMessage = "Не выбрано значение в поле \"Задание\"")]
        public int? TaskTypeId { get; set; }

        public int? EquipmentId { get; set; }

        [Required(ErrorMessage = "Не введено значение в поле \"Отработано часов\"")]

        [Range(0, 99.9, ErrorMessage = "Значение в поле \"Отработано часов\" должно находиться в диапазоне от 0 до 99.9")]
        public decimal? WorkTime { get; set; }

        [Required(ErrorMessage = "Не введено значение в поле \"Норма выработки\"")]
       
        [Range(0, 9.9, ErrorMessage = "Значение в поле \"Норма выработки\" должно находиться в диапазоне от 0 до 9.9")]
        public decimal? WorkDay { get; set; }

        //[RegularExpression(@"^\d*,\d{0,2}$", ErrorMessage = "Значение 'Расценка' не может иметь более чем 2 знака после запятой ")]
        //[Range(0, 999.99, ErrorMessage = "Диапазон значения 'Расценка' от 0 до 999.99")]
        public decimal? PriceDays { get; set; }

        [Required(ErrorMessage = "Не введено значение в поле \"Отработано моточасов\"")]
        [RegularExpression(@"^\d*$", ErrorMessage = "Значение в поле \"Отработано моточасов\" должно бьть числовым")]
        [Range(0, 99, ErrorMessage = "Значение в поле \"Отработано моточасов\" должно находиться в диапазоне от 0 до 99")]
        public int? MotoHours { get; set; }

        //-   [RegularExpression(@"^\d*.\d{0,2}$", ErrorMessage = "Значение в поле \"Факт объем. тонн\" не должно иметь больше двух знаков после запятой")]
        [Range(0, 99999.99, ErrorMessage = "Значение в поле \"Факт объем. тонн\" должно находиться в диапазоне от 0 до 99999.99")]
        public decimal? Volumefact { get; set; }


        //-  [RegularExpression(@"^\d*.\d{0,1}$", ErrorMessage = "Значение в поле \"В переводе на условные га\" не должно иметь больше одного знака после запятой")]
        [Range(0, 9999.9, ErrorMessage = "Значение в поле \"В переводе на условные га\" должно находиться в диапазоне от 0 до 9999.9")]
        public decimal? Area { get; set; }

        [Required(ErrorMessage = "Не введено значение в поле \"Расценка\"")]
        //-  [RegularExpression(@"^\d*.\d{0,2}$", ErrorMessage = "Значение в поле \"Расценка\" не должно иметь больше двух знаков после запятой")]
        [Range(0, 9999999.99, ErrorMessage = "Значение в поле \"Расценка\" должно находиться в диапазоне от 0 до 9999999.99")]
        public decimal? RateShift { get; set; }

       // [Required(ErrorMessage = "Не введено значение в поле \"Расход горючего на ед. работы\"")]
        //-  [RegularExpression(@"^\d*.\d{0,2}$", ErrorMessage = "Значение в поле \"Расход горючего на ед. работы\" не должно иметь больше двух знаков после запятой")]
        [Range(0, 99999.99, ErrorMessage = "Значение в поле \"Расход горючего на ед. работы\" должно находиться в диапазоне от 0 до 99999.99")]
        public decimal? ConsumptionRate { get; set; }

        [Required(ErrorMessage = "Не введено значение в поле \"Расход горючего, факт.\"")]
        //- [RegularExpression(@"^\d*.\d{0,2}$", ErrorMessage = "Значение в поле \"Расход горючего, факт.\" не должно иметь больше двух знаков после запятой")]
        [Range(0, 99999.99, ErrorMessage = "Значение в поле \"Расход горючего, факт.\" должно находиться в диапазоне от 0 до 99999.99")]
        public decimal? ConsumptionFact { get; set; }

        //[RegularExpression(@"^\d*,\d{0,2}$", ErrorMessage = "Значение 'Всего расход горючего ' не может иметь более чем 2 знака после запятой ")]
        //[Range(0, 999.99, ErrorMessage = "Диапазон значения 'Всего расход горючего ' от 0 до 999.99")]
        public decimal? Consumption { get; set; }

        public string Comment { get; set; }
        
        public decimal? TaskNum { get; set; }

        //-  [RegularExpression(@"^\d*.\d{0,2}$", ErrorMessage = "Значение в поле \"Доп. оплата\" не должно иметь больше двух знаков после запятой")]
     //   [Range(0, 999.99, ErrorMessage = "Значение в поле \"Доп. оплата\" должно находиться в диапазоне от 0 до 999.99")]
        public decimal? RatePiecework { get; set; }

        //-   [RegularExpression(@"^\d*.\d{0,2}$", ErrorMessage = "Значение в поле \"Ширина захвата оборудования.\" не должно иметь больше двух знаков после запятой")]
        [Range(0, 99.99, ErrorMessage = "Значение в поле \"Ширина захвата оборудования.\" должно находиться в диапазоне от 0 до 99.99")]
        public decimal? EquipWidth { get; set; }

        public decimal? CostFuel { get; set; }
        public int? AgroreqId { get; set; }

        public int? IdRate { get; set; }

        public decimal? WialonConsFact { get; set; }
    }
}