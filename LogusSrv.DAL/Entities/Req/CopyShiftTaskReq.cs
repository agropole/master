﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogusSrv.DAL.Entities.Req
{
    public  class CopyShiftTaskReq
    {
        public DateTime Date { get; set; }
        public int Shift { get; set; }
        public int FilterOrg { get; set; }
        public int OrgId { get; set; }
    }
}
