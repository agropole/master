﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace LogusSrv.DAL.Entities.Req
{
    public class DayTaskDetailReq
    {
        public int? DetailId { get; set; }

        public int? Driver1Id { get; set; }

        public int? Driver2Id { get; set; }

        public int? TraktorId { get; set; }

        public int? TrailerId { get; set; }
        public int? AvailableId { get; set; }
        public int? AgroreqId { get; set; }

        [DisplayAttribute(Name = " 'Вид задания'")]
        [Required(ErrorMessage = "Вы не указали вид задания")]
        public int? TaskTypeId { get; set; }

        [DisplayAttribute(Name = " 'Поле'")]
        [Required(ErrorMessage = "Вы не указали Поле")]
        public int? FieldId { get; set; }

        [Required(ErrorMessage = "Вы не указали Культуру")]
        public int? PlantId { get; set; }

        [DisplayAttribute(Name = " 'Дата'")]
        [Required(ErrorMessage = "Вы не указали Дату")]
        public DateTime? Date { get; set; }

        public int? ShiftNum { get; set; }

        public string Comment { get; set; }
        
    }
}
