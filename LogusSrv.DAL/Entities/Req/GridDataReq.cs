﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LogusSrv.DAL.Enums;
using System.ComponentModel.DataAnnotations;
using LogusSrv.DAL.Entities.Res;

namespace LogusSrv.DAL.Entities.Req
{
    public class GridDataReq : SparePartsDetails
    {
        public int PageSize { get; set; }
        public int PageNum { get; set; }
        public string SortField { get; set; }
        public int? MatTypeId { get; set; }
        public DateTime? DateStart { get; set; }
        public DateTime? DateEnd { get; set; }
        public int? ContractorsId { get; set; }
        public SortDirections SortOrder { get; set; }
        public DateTime? DateFilter { get; set; }
        public string CheckSearch { get; set; }
        public int? OrgId { get; set; }
        public int? EmpId { get; set; }

    }
}
