﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogusSrv.DAL.Entities.Req
{
    public class Top5TaskReq
    {
        public int id { get; set; }
    }

    public class TMCMapReq
    {
        public int fieldId { get; set; }
        public int year { get; set; }
    }
}
