﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using LogusSrv.DAL.Entities.DTO;
using LogusSrv.DAL.Entities.Res;

namespace LogusSrv.DAL.Entities.Req
{
    public class CreateWaysListReq
    {
        [Required(ErrorMessage = "Не введено значение в поле \"№ путевого листа\"")]
        [RegularExpression(@"^.{1,20}$", ErrorMessage = "Значение в поле \"№ путевого листа\" не должно превышать 20 символов")]
        public string WayListNum { get; set; }
        public int? WaysListId { get; set; }

        [Required(ErrorMessage = "Не выбрано значение в поле \"Водитель\"")]
        public int? DriverId { get; set; }

       // [Required(ErrorMessage = "Не выбрано значение в поле \"Техника\"")]
        public int? TraktorId { get; set; }
        public int? TrailerId { get; set; }

        [Required(ErrorMessage = "Не выбрано значение в поле \"Дата выдачи\"")]
        public DateTime? DateBegin {get;set; }
        public DateTime? DateEnd { get; set; }

        [Required(ErrorMessage = "Не введено значение в поле \"Начало работы\"")]
        [RegularExpression(@"^(0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$", ErrorMessage = "Значение в поле \"Начало работы\" должно находиться в диапазоне 00:00 - 23:59")]
        public string timeStart { get; set; }

        [Required(ErrorMessage = "Не введено значение в поле \"Окончание работы\"")]
        [RegularExpression(@"^(0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$", ErrorMessage = "Значение в поле \"Окончание работы\" должно находиться в диапазоне 00:00 - 23:59")]
        public string timeEnd { get; set; }

        [Required(ErrorMessage = "Остсутствует информация о пользователе системы (Организация), перелогинтесь или обратитесь в службу поддержки")]
        public int? OrgId { get; set; }
        [Required(ErrorMessage = "Остсутствует информация о пользователе системы , перелогинтесь или обратитесь в службу поддержки")]
        public int? UserId { get; set;}

        [Required(ErrorMessage = "Не выбрано значение в поле \"Поле\"")]
        public int? FieldId { get; set; }
        public decimal? SatusId { get; set; }
        [Required(ErrorMessage = "Не выбрано значение в поле \"Задание\"")]
        public int? TaskTypeId { get; set; }
        //[Required(ErrorMessage = "Не выбрано значение в поле \"Ответственный\"")]
        public int? AvailableId { get; set; }
        public int? AgroreqId { get; set; }
        public int? TaskId { get; set; }
        public Boolean? IsRepair { get; set; }
        public int? PlantId { get; set; }
        public bool? AllResource { get; set; }
        public int? DetailId { get; set; }
        public string Comment { get; set; }

      //  [RegularExpression(@"^\d*(\,\d{1,2})?$", ErrorMessage = "Значение в поле \"Ширина захвата оборудования.\" не должно иметь больше двух знаков после запятой")]
        [Range(0, 99.99, ErrorMessage = "Значение в поле \"Ширина захвата оборудования.\" должно находиться в диапазоне от 0 до 99.99")]
        public decimal? EquipWidth { get; set; }

        [Required(ErrorMessage = "Не выбрано значение в поле \"Тип топлива\"")]
        public int? TypeFuel { get; set; }
    }
}
