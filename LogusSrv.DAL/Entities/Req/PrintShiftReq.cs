﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogusSrv.DAL.Entities.Req
{
    public class PrintShiftReq
    {
        public int DayTaskId { get; set; }
    }
}
