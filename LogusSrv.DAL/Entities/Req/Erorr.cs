﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogusSrv.DAL.Entities.Req
{
    public class Erorr
    {
        public int? ErorrCode { get; set; }
        public List<string> ErorrText { get; set; }
    }
}
