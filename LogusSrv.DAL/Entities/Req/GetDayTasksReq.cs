﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogusSrv.DAL.Entities.Req
{
    public class GetDayTasksReq
    {
        public DateTime? DateFilter { get; set; }
        public int Shift { get; set; }
        public int? DetailId { get; set; }
        public string Comment { get; set; }
        public int? OrgId { get; set; }
        public int? FilterOrg { get; set; }
        public bool? checkRes { get; set; }
    }
}
