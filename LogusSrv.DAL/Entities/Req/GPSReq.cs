﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LogusSrv.DAL.Entities.DTO;

namespace LogusSrv.DAL.Entities.Req
{
    public class GPSReq
    {
        public List<GPSDTO> GPSArray { get; set; }
    }
}
