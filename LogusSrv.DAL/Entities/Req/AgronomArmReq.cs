﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LogusSrv.DAL.Entities.Res;

namespace LogusSrv.DAL.Entities.Req
{
    public class AgronomUpdateField
    {
        public string name { get; set; }
        public int? fiel_id { get; set; }
        public string coord { get; set; }
    }

    public class AgronomUpdateFieldModel : UpdateDictModel
    {
        public AgronomUpdateField Document { get; set; }
    }
}
