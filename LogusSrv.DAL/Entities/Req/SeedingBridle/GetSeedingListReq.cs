﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogusSrv.DAL.Entities.Req.SeedingBridle
{
    public class GetSeedingListReq : GridDataReq
    {
        public int FilterPlantId { get; set; }
        public int FilterFieldId { get; set; }
        public int FilterYearId { get; set; }
        public int FilterOrgId { get; set; }
        public int? FilterIndicatorId { get; set; }
    }
}
