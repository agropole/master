﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogusSrv.DAL.Entities.Req.SeedingBridle
{
    public class GetDetailSeedingListReq
    {
        public int YearId {get;set;}
        public int FieldId { get; set; } 
    }
}
