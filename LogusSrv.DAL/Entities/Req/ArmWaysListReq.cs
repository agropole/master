﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogusSrv.DAL.Entities.Req
{
    public class ArmWaysListReq
    {
        public decimal? traktor_id {get;set;}
        public decimal? ways_list_id { get; set; }
        public decimal? status { get; set; }
        public decimal? task_id {get; set;}        
        public string time_end_str { get; set; }
        public string time_begin_str { get; set; }
        public string xml { get; set; }
        public string date { get; set; }
    }
}
