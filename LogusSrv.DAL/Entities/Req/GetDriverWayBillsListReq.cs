﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogusSrv.DAL.Entities.Req
{
    public class GetDriverWayBillsListReq
    {
        public int? OrgId { get; set; }
        public DateTime? DateFilter { get; set; }
        public DateTime? DateFilterEnd { get; set; }
        public string RegNum { get; set; }
        public int? TporgId { get; set; }
        public int? WaysListId { get; set; }
        public int? DayTaskId { get; set; }
        public int? TaskId { get; set; }
        public bool? AllResource { get; set; }
    }
}
