﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LogusSrv.DAL.Entities.DTO;

namespace LogusSrv.DAL.Entities.Req
{
    public class UpdateShiftTasksReq
    {
        public string Comment { get; set; }
        public DateTime Date { get; set; }
        public List<DayTaskDTO> ShiftInfo { get; set; }
    }
}
