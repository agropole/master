﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LogusSrv.DAL.Entities.DTO;

namespace LogusSrv.DAL.Entities.Req
{
    public class UpdateRefuelingsReq
    {
        public int WaysListId { get; set; }
        public List<RefuelingDTO> Refuelings { get; set; }
    }
}
