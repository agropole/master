﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogusSrv.DAL.Entities.Req
{
    public class RateReq
    {
        public int IdTptas { get; set; }
        public int? IdTrakt { get; set; }
        public int? IdEqui { get; set; }
        public int? IdPlant { get; set; }
        public int? IdAgroreq { get; set; }
        public int? TypeSheet { get; set; }
        public int? SheetId { get; set; }
    }
}
