﻿/*
 * created by ofirtych 2015-08-21
 * 
 * */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LogusSrv.DAL.Entities.DTO;
using System.ComponentModel.DataAnnotations;

namespace LogusSrv.DAL.Entities.Res
{
    public class DashboardResModel
    {

        public decimal? Spenttime { get; set; }//затраты времени
        public decimal? LaborCosts { get; set; }//затраты на оплату труда
        public decimal? MaterialCosts { get; set; }//материальные затраты
        public decimal? OverheadCosts { get; set; }//накрадные расходы
        public decimal? GrossHarvesr { get; set; }//валовый сбор

        public decimal? Area { get; set; }//общая площадь посевов

    }

    public class PlantResModel
    {
        public string Name { get; set; }
        public int Id { get; set; }
    }
}
