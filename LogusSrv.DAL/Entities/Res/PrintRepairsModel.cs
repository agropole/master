﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogusSrv.DAL.Entities.DTO
{
    public class PrintRepairsModel
    {
        public List<PrintRepairsDTO1> query1 { get; set; }
        public List<PrintRepairsDTO2> query2 { get; set; }
        public List<PrintRepairsDTO3> query3 { get; set; }
        public List<PrintRepairsDTO4> query4 { get; set; }
    }
}
