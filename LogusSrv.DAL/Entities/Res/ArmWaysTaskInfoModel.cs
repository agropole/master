﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LogusSrv.DAL.Entities.DTO;


namespace LogusSrv.DAL.Entities.Res
{
    public class ArmWaysTaskInfoModel 
    {
        public int driver_id { get; set; }

        public string driver_name { get; set; }

        public int traktor_id { get; set; }

        public string traktor_brand{ get; set; }
        public string traktor_number { get; set; }
        public string equipment_name { get; set; }
        public int ways_list_id { get; set; }

        public DateTime date_begin { get; set; }
        public int? status { get; set; }


        public List<TaskArmDTO> tasks_info { get; set; }
    }
}