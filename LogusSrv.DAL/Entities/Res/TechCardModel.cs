﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity.Spatial;
using LogusSrv.DAL.DBModel;
using LogusSrv.DAL.Entities.DTO;
using LogusSrv.DAL.Entities.DTO.SeedingBridle;
using System.ComponentModel.DataAnnotations;
using LogusSrv.DAL.Entities.DTO.ActsSzr;

namespace LogusSrv.DAL.Entities.Res
{
    public class TechCardModel
    {
        public int id { get; set; }
        public int? id_tc { get; set; }
        public int? plant_plant_id { get; set; }
        public string plant_name { get; set; }
        public int? tpsort_tpsort_id { get; set; }
        public string tpsort_name { get; set; }
        public List<Field> fields { get; set; }
    }

    public class Field
    {
        public int id { get; set; }
        public string name { get; set; }
        public decimal? area { get; set; }
        public string plant { get; set; }

    }

    public class TCTask
    {
        public int id { get; set; }
        public int? plant_id { get; set; }
        public string plant_name { get; set; }
        public int field_id { get; set; }
        public string field_name { get; set; }
        public int? id_tc { get; set; }
        public string trakt { get; set; }
        public DateTime date { get; set; }
        public bool add_value { get; set; }
        public decimal? cons_fuel { get; set; }
        public decimal? total_salary { get; set; }
    }

    public class TCPlanFactModel
    {
        public CostsModel plan;
        public CostsModel fact;
    }

    public class CostsModel
    {
        public float cost { get; set; }
        public float cost_salary { get; set; }
        public float cost_fuel { get; set; }
        public double? service_total { get; set; }
        public float? cost_szr { get; set; }
        public float? cost_min { get; set; }
        public float? cost_seed { get; set; }
        public float? dop_material_fact { get; set; }
        public float? gross_harvest { get; set; }
        public float? area { get; set; }
    }

    public class TCParamValue
    {
        public float? value { get; set; }
        public int id_tc_param { get; set; }

    }

    public class TCCostModel
    {
        public float? Cost { get; set; }
        public int Id { get; set; }

    }

    public class TCCostByMaterialType
    {
        public int? Id { get; set; }
        public string Name { get; set; }
        public List<TCCostModel> Costs { get; set; }

        public float SumCost { get; set; }
    }

    public class InfoForTCDetail
    {
        public List<DictionaryItemsDTO> YearList { get; set; }
        public List<WorkDictionary> WorkList { get; set; }
        public List<DictionaryItemsDTO> EquipmentList { get; set; }
        public List<DictionaryItemsDTO> TechList { get; set; }
        public List<DictionaryMaterialDTO> SzrList { get; set; }
        public List<DictionaryMaterialDTO> FertilizerList { get; set; }
        public List<DictionaryMaterialDTO> SeedList { get; set; }
        public List<DictionaryMaterialDTO> DopMaterialList { get; set; }
        public List<DictionaryItemsDTO> TypeFuelList { get; set; }
        public List<DictionaryItemsDTO> ServiceList { get; set; }
    }

    public class TCFuelDictionary : DictionaryItemsDTO
    {
    }

    public class PlantSortFieldDictionary : DictionaryItemsDTO
    {
        public List<SortFieldDictionary> Values { get; set; }
        public List<FieldDictionary> Fields { get; set; }
    }

    public class SortFieldDictionary : DictionaryItemsDTO
    {
        public List<FieldDictionary> Fields { get; set; }
    }

    public class FieldDictionary : DictionaryItemsDTO
    {
        public decimal? Area { get; set; }
        public DictionaryItemsDTO Sorts { get; set; }
        public string Coord { get; set; }
        public string Center { get; set; }
        public decimal? typeField { get; set; }
    }

    public class WorkDictionary : DictionaryItemsDTO
    {
        public DictionaryItemsDTO FuelType { get; set; }

        public decimal? Consumption { get; set; }
        public decimal? ShiftRate { get; set; }
        public decimal? EmpPrice { get; set; }
    }

    public class TCDetailModel
    {
        public Object Clone()
        {
            return this.MemberwiseClone();
        }
        public TCModel TC { get; set; }
        public NZPModel NZP { get; set; }
        public List<WorkModel> Works { get; set; }
        public List<SupportStaffModel> SupportStuff { get; set; }
        public List<SZRModel> DataSZR { get; set; }
        public List<FertilizerModel> DataChemicalFertilizers { get; set; }
        public List<SeedModel> DataSeed { get; set; }
        public List<DopMaterialsModel> DataDopMaterial { get; set; }
        public List<DataTotalModel> DataTotal { get; set; }
        public List<DataService> DataServices { get; set; }
    }

    public class DataService
    {
        public int? Id { get; set; }
        [Required(ErrorMessage = "Не выбран тип услуг")]
        public DictionaryItemsDTO NameService { get; set; }
        [Required(ErrorMessage = "Не указана стоимость услуг")]
        public float? Cost { get; set; }
    }

    public class TCModel
    {
        public Object Clone()
        {
            return this.MemberwiseClone();
        }
        public int? Id { get; set; }
        [Required(ErrorMessage = "Не выбрана культура")]
        public int? PlantId { get; set; }
        public int? SortId { get; set; }
        public int? FieldId { get; set; }
        [Required(ErrorMessage = "Не выбран год")]
        public int YearId { get; set; }
        public float? Area { get; set; }
        [Required(ErrorMessage = "Не заполнено поле Урожайность")]
        public float? Productivity { get; set; }
        public float? GrossHarvest { get; set; }
        public int? TemplateStatus { get; set; }
        public int? CheckTemp { get; set; }
        public int? CheckСhange { get; set; }
        public int TypeModuleId { get; set; }
    }

    public class NZPModel
    {
        public float? Energy { get; set; }
        public float? Salary { get; set; }
        public float? SZR { get; set; }
        public float? ChemicalFertilizers { get; set; }
        public float? DopMaterial { get; set; }
        public float? Seed { get; set; }
        public float? Total { get; set; }
    }

    public class WorkModel
    {
        public int? Id { get; set; }
        [Required(ErrorMessage = "Не выбрано название работы")]
        public DictionaryItemsDTO Name { get; set; }
        [Required(ErrorMessage = "Не заполнено поле Кратность работ")]
        public decimal? Count { get; set; }
        [Required(ErrorMessage = "Не заполнено поле Коэффициент работ")]
        public decimal? Factor { get; set; }
        [Required(ErrorMessage = "Не заполнено поле Объем работ")]
        public float? Workload { get; set; }
        public DateTime? DateStart { get; set; }
        public DateTime? DateEnd { get; set; }
        public int DayCount { get; set; }
        public List<DictionaryItemsDTO> Equipment { get; set; }
        public List<DictionaryItemsDTO> Tech { get; set; }
        public string Comment { get; set; }
        [Required(ErrorMessage = "Не заполнено поле Расход топлива")]
        public float? Consumption { get; set; }
      //  [Required(ErrorMessage = "Не заполнено поле Тип топлива")]
        public DictionaryItemsDTO FuelType { get; set; }
        public float? FuelCost { get; set; }
        [Required(ErrorMessage = "Не заполнено поле Сменная норма выработки")]
        public float? ShiftRate { get; set; }
        public float? ShiftCount { get; set; }
        [Required(ErrorMessage = "Не заполнено поле Расценка")]
        public float? EmpPrice { get; set; }
        public float? EmpCost { get; set; }
        public float? UnicFlag { get; set; }
        public float? Rate { get; set; }
        public int? FuelId { get; set; }
        public float? FuelCount { get; set; }
        public int index { get; set; }
        public int? Id1 { get; set; }
     
    }

    public class SupportStaffModel
    {
        [Required(ErrorMessage = "Не заполнено поле Наименование работы для Вспомогательного персонала")]
        public DictionaryItemsDTO NameSelected { get; set; }
        [Required(ErrorMessage = "Не заполнено поле Количество для Вспомогательного персонала")]
        public int? Count { get; set; }
        [Required(ErrorMessage = "Не заполнено поле Расценка для Вспомогательного персонала")]
        public float? Price { get; set; }
        public float? Cost { get; set; }
        public int? Id_tc_operation { get; set; }
        public float? UnicFlag { get; set; }
        public int? index { get; set; }
        public int? id_tc_oper { get; set; }
    }

    public class MaterialModel
    {
        [Required(ErrorMessage = "Не заполнено поле Наименование работы для материалов")]
        public DictionaryItemsDTO NameSelectedWorkForMaterial { get; set; }
        [Required(ErrorMessage = "Не заполнено поле Цена для материалов")]
        public float? Price { get; set; }
        public float? Cost { get; set; }
        [Required(ErrorMessage = "Не заполнено поле Расход для материалов")]
        public float? ConsumptionRate { get; set; }
        [Required(ErrorMessage = "Не заполнено поле Площадь внесения для материалов")]
        public float? MaterialArea { get; set; }
        public int Type { get; set; }
        public int? index { get; set; }
        public int? id_tc_oper { get; set; }
    }

    public class SZRModel : MaterialModel
    {
        public DictionaryItemsDTO NameSZR { get; set; }


    }

    public class FertilizerModel : MaterialModel
    {
        public DictionaryItemsDTO NameFertilizer { get; set; }
    }

    public class SeedModel : MaterialModel
    {
        public DictionaryItemsDTO NameSeed { get; set; }
    }

     public class DopMaterialsModel : MaterialModel
    {
         public DictionaryItemsDTO NameDopMaterial { get; set; }
    }
    public class DataTotalModel
    {
        public string Name { get; set; }
        public double Total { get; set; }
        public float? PerGa { get; set; }
        public float? PerT { get; set; }
        public List<int> ParamIds { get; set; }
    }

    public class TCItem
    {
        public int Id { get; set; }
        public string Plant { get; set; }
        public string Sort { get; set; }
        public string Field { get; set; }
        public double? Cost { get; set; }
        public int Status { get; set; }
        public int Number { get; set; }
        public DateTime Changing_date { get; set; }
        public String Changing_date1 { get; set; }
        public int? TemplateStatus { get; set; }

    }

    public class TCTotalItem : TCItem
    {
        public float Area { get; set; }
        public float GrossHarvest { get; set; }
    }

    public class NZPReq
    {
        public int YearId { get; set; }
        public float Area { get; set; }
        public List<DictionaryItemsDTO> Fields { get; set; }

    }
    //
    public class RateTechReq
    {
        public int? tptasId { get; set; }
        public int? techId { get; set; }
        public int? equiId { get; set; }
        public int? plantId { get; set; }
        public int? operationId { get; set; }

    }
    //
    public class TaskNZPCountModel
    {
        public decimal? salary { get; set; }
        public decimal? fuel_cost { get; set; }
        public decimal? sum { get; set; }
        public Int64? summa { get; set; }
        public decimal? area { get; set; }
        public Single szr { get; set; }
        public Single chemicalFertilizers { get; set; }
        public Single dopMaterial { get; set; }
        public Single seed { get; set; }
        public int fiel_id { get; set; }
    }

    public class MaterialNZPCountModel
    {
        public int type_id { get; set; }
        public decimal? sum { get; set; }
    }

    public class NZPRes
    {
        public decimal? Energy { get; set; }
        public decimal? Salary { get; set; }
        public decimal? SZR { get; set; }
        public decimal? ChemicalFertilizers { get; set; }
        public decimal? DopMaterial { get; set; }
        public decimal? Seed { get; set; }
        public decimal? Total { get; set; }
    }

    public class FieldClosed
    {
        public int id { get; set; }
        public DateTime date { get; set; }
    }

    public class ReqGantt
    {
        public DateTime? DateStart { get; set; }
        public DateTime? DateEnd { get; set; }
        public int? TypeTraktorId { get; set; }
        public int? TypeEquiId { get; set; }
    }
    public class ReqTMC
    {
        public DateTime? DateStart { get; set; }
        public DateTime? DateEnd { get; set; }
        public int? TmcId { get; set; }
    }
    public class GantTech
    {
        public List<GantTaskModel> TraktList { get; set; }
        public List<GantTaskModel> EquiList { get; set; }
        public List<DictionaryItemsDTO> FreeTrakt { get; set; }
        public List<DictionaryItemsDTO> FreeEqui { get; set; }
    }
    public class GantTMC
    {
        public List<DHXGantModel> data { get; set; }
        public List<DHXGantModel> data2 { get; set; }
        public List<GantTaskModel> TmcList { get; set; }
       
    }
    public class GantTaskModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string BaselineStartDate { get; set; }
        public string BaselineEndDate { get; set; }
        public List<GantTaskModel> Children { get; set; }
        public string StartDate { get; set; }
        public int Duration { get; set; }
        public DateTime? Date1 { get; set; }
        public DateTime? Date2 { get; set; }
        public float? Cost { get; set; }
        public float? Consumption { get; set; }
    }

    //dhtmlx gantt
    public class DHXGantModel
    {
        public int id { get; set; }
        public int id2 { get; set; }
        public string text { get; set; }
        public string start_date { get; set; }
        public string end_date { get; set; }
        public string type { get; set; }
        public string color { get; set; }
        public int EquipOrTech { get; set; }
        public DateTime Date1 { get; set; }
        public DateTime Date2 { get; set; }
        public int duration { get; set; }
        public float progress { get; set; }
        public int? parent { get; set; }
        public float? consumption { get; set; }
        public float? cost { get; set; }
        public int id_tc_param { get; set; }
        public float? value { get; set; }
        public bool open { get; set; }
        public List<DHXGantModel1> Children { get; set; }
        

    }

    public class DHXGantModel1
    {
        public int id { get; set; }
        public string text { get; set; }
        public string start_date { get; set; }
        public string end_date { get; set; }
        public DateTime Date1 { get; set; }
        public DateTime Date2 { get; set; }
        public int duration { get; set; }
        public float progress { get; set; }
        public int? parent { get; set; }
        public float? consumption { get; set; }
        public float? cost { get; set; }
        public int id_tc_param { get; set; }
        public float? value { get; set; }
    }


    public class GanttInfo
    {
        public List<DictionaryItemsDTO> TypeTraktor { get; set; }
        public List<DictionaryItemsDTO> TypeEqui { get; set; }
    }
    public class TmcInfo
    {
        public List<MaterialDto> TmcList { get; set; }
    }
    public class TMCInfo
    {
        public List<DictionaryItemsDTO> TypeTraktor { get; set; }
     //   public List<DictionaryItemsDTO> TypeEqui { get; set; }
    }
    public class ReportFile
    {
        public byte[] data { get; set; }
        public string fileName { get; set; }
    }
}
