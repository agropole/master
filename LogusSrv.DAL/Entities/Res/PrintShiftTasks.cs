﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LogusSrv.DAL.Entities.DTO;

namespace LogusSrv.DAL.Entities.Res
{
    public class PrintShiftTasks
    {
        public string DateShifts { get; set; }

        public string Comment { get; set; }
        public DateTime DateBegin { get; set; }

        public List<PrintShiftDTO> ShiftInfo1 { get; set; }
        public List<PrintShiftDTO> ShiftInfo2 { get; set; }
    }

    //Антон----------------------------------------------------------------------------------------------------------------------------------------
    public class PrintShiftTasksEmployees
    {
        public string DateShifts { get; set; }

        public string Comment { get; set; }
        public DateTime DateBegin { get; set; }

        public List<PrintShiftEmployees> ShiftInfo1 { get; set; }
        public List<PrintShiftEmployees> ShiftInfo2 { get; set; }
    }
    //---------------------------------------------------------------------------------------------------------------------------------------------
}
