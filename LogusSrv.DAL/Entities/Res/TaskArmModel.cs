﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogusSrv.DAL.Models.ReturnModels
{
    public class TaskArmModel
    {
        public decimal task_id { get; set; }
        public string field_name { get; set; }
        public string plant_name { get; set; }
        public string task_name { get; set; }
        public decimal? status { get; set; }
        public double? date_begin { get; set; }

        public double? date_end { get; set; }
    }
}
