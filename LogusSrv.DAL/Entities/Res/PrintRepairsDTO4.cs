﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogusSrv.DAL.Entities.DTO
{
    public class PrintRepairsDTO4
    {
        public string SpareName { get; set; }
        public int SpareId { get; set; }
        public int Count { get; set; }
    }
}
