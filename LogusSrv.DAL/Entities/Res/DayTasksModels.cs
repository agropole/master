﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LogusSrv.DAL.Entities.DTO;
using System.Threading.Tasks;

namespace LogusSrv.DAL.Entities.Res
{
    public class DayTasksModels
    {
        public int DayTaskId { get; set; }
        public string Comment { get; set; }
        public List<DayTaskDTO> Shift1 { get; set; }
        public List<DayTaskDTO> Shift2 { get; set; }
        public List<DictionaryItemsDTO> FreeEmp { get; set; }
    }

    public class ParametersModel
    {
        public string Name { get; set; }
        public string Value { get; set; }
        public int? Id { get; set; }

    }

}
