﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace LogusSrv.DAL.Entities.Res
{
    public class Top5TasksInfoModel 
    {
        public int driver_id { get; set; }

        public string driver_name { get; set; }

        public int traktor_id { get; set; }

        public string traktor_brand { get; set; }
        public string traktor_number { get; set; }

        public string equipment_name { get; set; }

        public int task_id { get; set; }
        public string field_name { get; set; }
        public string plant_name { get; set; }
        public string task_name { get; set; }
        public int? status { get; set; }
        public DateTime? date_begin { get; set; }

        public DateTime? date_end { get; set; }
        public decimal? area { get; set; }



    }
}