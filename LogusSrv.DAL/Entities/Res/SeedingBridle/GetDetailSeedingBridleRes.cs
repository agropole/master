﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LogusSrv.DAL.Entities.DTO.SeedingBridle;
using LogusSrv.CORE.REST.Attributes;

namespace LogusSrv.DAL.Entities.Res.SeedingBridle
{
    public class GetDetailSeedingBridleRes : FieldEventModel
    {
        public decimal? area {get;set;}

        
        [NotEmptyList(ErrorMessage = "Не добавлено ни одной культуры")]
        public List<GetDetailPlantSeeding> Values { get; set; }
    }

    public class FieldEventModel
    {
        public int PlantId { get; set; }
        public int? FieldId { get; set; }
        public int? YearId { get; set; }
        public int? SortId { get; set; }
        public int? TempId { get; set; }
        public int? CheckTemp { get; set; }
    }

    public class CloseFieldModel : FieldEventModel
    {
        public DateTime DateClose { get; set; }
    }

}
