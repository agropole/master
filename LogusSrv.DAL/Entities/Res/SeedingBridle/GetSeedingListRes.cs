﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LogusSrv.DAL.Entities.DTO.SeedingBridle;

namespace LogusSrv.DAL.Entities.Res.SeedingBridle
{
    public class GetSeedingListRes 
    {
        public List<GetSeedingListDto> Values {get;set;}

        public int CountPages{get;set;}
        public int CountItems {get;set;}
    }
}
