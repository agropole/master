﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LogusSrv.DAL.Entities.DTO;
using LogusSrv.DAL.Entities.DTO.SeedingBridle;

namespace LogusSrv.DAL.Entities.Res.SeedingBridle
{
    public class GetInfoDetailSeeding
    {
        public List<DictionaryItemsDTO> YearsList { get; set; }
        public List<DictionaryItemFields> FieldsList { get; set; }
        public List<DictionaryItemsDTO> ReproductionList { get; set; }
        public List<DictionaryItemForPlants> PlantsAndSorts { get; set; } 
    }
}
