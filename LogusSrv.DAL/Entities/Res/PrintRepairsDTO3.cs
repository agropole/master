﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogusSrv.DAL.Entities.DTO
{
    public class PrintRepairsDTO3
    {
        public string DefectName { get; set; }
        public int DefectId { get; set; }
        public string TaskName { get; set; }
    }
}
