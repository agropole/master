﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity.Spatial;

namespace LogusSrv.DAL.Entities.Res
{
    public class GPSmodel
    {
        public int id { get; set; }
        public int id_track { get; set; }
        public float flags { get; set; }
        public DateTime date { get; set; }
        public DbGeography coord { get; set; }
        public decimal? speed { get; set; }
        public decimal? direction { get; set; }
        public decimal? miliage { get; set; }

        public decimal? width { get; set; }
        public int fiel_id { get; set; }
    }
}