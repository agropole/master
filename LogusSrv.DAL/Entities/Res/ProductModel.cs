﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LogusSrv.DAL.Entities.DTO;

namespace LogusSrv.DAL.Entities.Res
{
    public class ProductInfo
    {
        public List<DictionaryItemsDTO> YearList { get; set; }
        public List<DictionaryItemsDTO> OrganizationsList { get; set; }
    }

    public class CultureProduct
    {
        public int Id { get; set; }
        public DictionaryItemsDTO Plant { get; set; }
        public DictionaryItemsDTO Sort { get; set; }
        public decimal? GrossHarvest { get; set; }
        public decimal? Balance { get; set; }
        public decimal? Sales { get; set; }
        public decimal? ProfitPlan { get; set; }
        public decimal? Profit { get; set; }
        public decimal? CurrentPrice { get; set; }
        public decimal? Kol { get; set; }
        public int? Year { get; set; }
        
    }

    public class GrossInfo
    {
        public List<DictionaryItemsDTO> YearList { get; set; }
        public List<PlantSortFieldDictionary> Plants { get; set; }
        public List<DictionaryItemsDTO> OrganizationsList { get; set; }
    }

    public class GrossReq
    {
        public int Year { get; set; }
        public int? PlantId { get; set; }
        public int? OrgId { get; set; }

    }
    public class DelRow
    {
        public ModelDelRow rows { get; set; }
    }
    public class ModelDelRow
    {
        public int Id { get; set; }
        public DateTime Date { get; set; }
        public DictionaryItemsDTO Plant { get; set; }
        public DictionaryItemsDTO Sort { get; set; }
        public DictionaryItemsDTO Field { get; set; }
        public decimal? Count { get; set; }
        public decimal? Price { get; set; }
        public decimal? Cost { get; set; }
    }
    public class Gross
    {
        public int Id { get; set; }
        public DateTime Date { get; set; }
        public DictionaryItemsDTO Plant { get; set; }
        public DictionaryItemsDTO Sort { get; set; }
        public DictionaryItemsDTO Field { get; set; }
        public decimal? Count { get; set; }
        public decimal? Price { get; set; }
        public decimal? Cost { get; set; }
    }

    public class Sale
    {
        public int Id { get; set; }
        public DateTime Date { get; set; }
        public DictionaryItemsDTO Plant { get; set; }
        public DictionaryItemsDTO Sort { get; set; }
        public decimal? Count { get; set; }
        public decimal? Price { get; set; }
        public decimal? Cost { get; set; }
        public decimal? Balance { get; set; }
        public decimal? Balance2 { get; set; }
    }

    public class SaleModel
    {
        public List<Sale> res { get; set; }
        public decimal Balance { get; set; }
    }


}
