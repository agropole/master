﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity.Spatial;
using LogusSrv.DAL.DBModel;
using LogusSrv.DAL.Entities.DTO;
using LogusSrv.DAL.Entities.DTO.SeedingBridle;
using System.ComponentModel.DataAnnotations;
using OfficeOpenXml;
using LogusSrv.DAL.Entities.DTO.ExportExcel;

namespace LogusSrv.DAL.Entities.Res
{
    public class SparePartsModel 
    {
        public Object Clone()
        {
            return this.MemberwiseClone();
        }
        public int Id { get; set; }
        public int ActId { get; set; }
        public int? Spare_id { get; set; }
        public int Store_id { get; set; }
        public string Name { get; set; } 
        public string TechName { get; set; } 
        public string EqName { get; set; } 
        public float? Reserve { get; set; }
        public float Reserve2 { get; set; }
        public int Number { get; set; }
        public DictionaryItemsDTO SpareParts { get; set; }
        public DictionaryItemsDTO VendorCode { get; set; }
        public string VendorCode1 { get; set; }
        public DictionaryItemsDTO Storage { get; set; }
        public string Storage1 { get; set; }
        public float? Storage2 { get; set; }
        public string Contractor { get; set; }
        public string Manufacturer { get; set; }
        public DateTime? DateBegin { get; set; }
        public string DateBegin1 { get; set; }
        public string SpareParts1 { get; set; }
        public string Act_number { get; set; }
        public float? Price { get; set; }
        public float? Count { get; set; }
        public double? Cost1 { get; set; }
        public double? Cost2 { get; set; } // НДС
        public double? Cost3 { get; set; }
        public int? Pk { get; set; }
        public string NodeRepairs { get; set; } 
    }
    public class HistorySpareParts
    {
        public List<SparePartsModel> InvoiceList { get; set; }
        public List<SparePartsWriteOffModel> WriteOffList { get; set; }
    }

    public class MoveSpareParts
    {
        public List<SparePartsModel> InvoiceList { get; set; }
        public List<SparePartsModel> WriteOffList { get; set; }
        public List<SparePartsModel> BalanceList { get; set; }
    }
    public class InvoiceDetailsForRepair
    {
        public int Id { get; set; }
        public DictionaryItemsDTO SpareParts { get; set; }
        public DictionaryItemsDTO VendorCode { get; set; }
        public DictionaryItemsDTO Storage { get; set; }
        public float? Reserve { get; set; }
        public float? Reserve2 { get; set; }
        public float? Count { get; set; }
        public float? Balance { get; set; }
        public float? Available { get; set; }
        public float? Cost3 { get; set; }
    }


    public class SparePartsRepairs
    {
        public int? TraktorId { get; set; }
        public int? ModId { get; set; }
        public int? EquipId { get; set; }
    }
    public class CreateUpdateRepairs
    {
        public GeneralInfo GeneralInfo { get; set; }
        public List<TableInfo1> TableInfo1 { get; set; }
        public List<TableInfo2> TableInfo2 { get; set; }
        public List<TableInfo3> TableInfo3 { get; set; }
        public List<TableInfo4> TableInfo4 { get; set; }
        public List<TableInfo5> TableInfo5 { get; set; }
    }
    public class GeneralInfo
    {
        public int? ActId { get; set; }
        public string RepairNumber { get; set; }
        public DateTime dateStart { get; set; }
        public DateTime? dateEnd { get; set; }
        public DateTime? dateOpen { get; set; }
        public DateTime? dateEnd1 { get; set; }
        public DateTime? dateOpen1 { get; set; }
        public int? EmpId { get; set; }
        public int? TraktorId { get; set; }
        public Single? Run { get; set; }
        public Single? Moto { get; set; }
        public int? ModuleId { get; set; }
        public int? AgroreqId { get; set; }
        public Boolean Plan1 { get; set; }
        public Boolean Plan2 { get; set; }
        public Boolean Plan3 { get; set; }
        public Boolean Plan4 { get; set; }
        public Boolean WriteOff { get; set; }
        public int? CheckDailyAct { get; set; }
        public Boolean IsDaily { get; set; }
        public Boolean IsRequire { get; set; }
        public bool? AdminDates { get; set; }
        public int? Status { get; set; }
        public int? TypeSpareId { get; set; }
        public string Comment { get; set; }
        
    }
    public class TableInfo1
     {
        public int? Id { get; set; }
        public string  Malfunction { get; set; }
        public int? Number { get; set; }

    }
    public class TableInfo2
    {
        public int? Id { get; set; }
        public string Malfunction { get; set; }
        public DictionaryItemsDTO TypeTask { get; set; }
        public int? Number { get; set; }
    }
    public class TableInfo3
    {
        public int? Id { get; set; }
        public DictionaryItemsDTO SpareParts { get; set; }
        public DictionaryItemsDTO VendorCode { get; set; }
        public DictionaryItemsDTO Storage { get; set; }
        public float? Reserve { get; set; }
        public float? Reserve2 { get; set; }
        public float? Balance { get; set; }
        public float? Available { get; set; }
        public float? Count { get; set; }
        public float? Cost { get; set; }
        public int? Number { get; set; }
        public DateTime? Date { get; set; }
        public Boolean WriteOff { get; set; }

    }
    public class TableInfo4
    {
        public int? Id { get; set; }
        public string Number { get; set; }
        public int? Hours { get; set; }
        public decimal? SumCost { get; set; }
        public string Status { get; set; }
        public DateTime? Date { get; set; }
        public string Date1 { get; set; }
        public int? DailyAct { get; set; }
    }
    public class TableInfo5
    {
        public int? Id { get; set; }
        public int Number { get; set; }
        public string NameService { get; set; }
        public float? Count { get; set; }
        public float? Price { get; set; }
        public float? Cost1 { get; set; }
        public float? Cost2 { get; set; }
        public float? Cost3 { get; set; }
    }

    public class RepairTechEqiupModel
    {
        public int? Id { get; set; }
        public int? Rep_Id { get; set; }
        public int? Days { get; set; }
        public int? techOrEquip { get; set; }
        public float? SumSpare { get; set; }
        public float? ServiceSum { get; set; }
        public decimal? SumCost { get; set; }
        public string NameTech { get; set; }
    }

    public class SparePartsMovementModel
    {
        public int Id { get; set; }
        public  string Act_number { get; set; }
        public int? Act_number1 { get; set; }
        public string Date1 { get; set; }
        public DictionaryItemsDTO SpareParts { get; set; }
        public DictionaryItemsDTO VendorCode { get; set; }
        public DictionaryItemsDTO StorageWriteOff { get; set; }
        public DictionaryItemsDTO StorageComing { get; set; }
        public float? Price { get; set; }
        public DateTime Date { get; set; }
        public float? Count { get; set; }
        public double? Cost { get; set; }
    }
    public class SparePartsReserveModel
    {
        public DateTime? Date { get; set; }
        public DictionaryItemsDTO SpareParts { get; set; }
        public DictionaryItemsDTO VendorCode { get; set; }
        public String Act_number { get; set; }
        public float? Count { get; set; }
        public int Id { get; set; }
        public int Store_id { get; set; }
        public String CarsName { get; set; }
        public String CarsNum { get; set; }
    }
    public class SparePartsWriteOffModel
    {
        public DateTime? Date { get; set; }
        public String Date1 { get; set; }
        public DictionaryItemsDTO SpareParts { get; set; }
        public string SpareParts1 { get; set; }
        public string CheckWrite { get; set; }
        public DictionaryItemsDTO VendorCode { get; set; }
        public DictionaryItemsDTO Storage { get; set; }
        public string Tech { get; set; }
        public string CarNum { get; set; }
        public string Comment { get; set; }
        public String Act_number { get; set; }
        public float? Count { get; set; }
        public float? Price { get; set; }
        public float? Cost { get; set; }
        public int Id { get; set; }
        public int ActId { get; set; }
        public String CarsName { get; set; }
        public String CarsNum { get; set; }

    }
    public class SparePartsListResponse
    {
        public List<SparePartsModel> Values { get; set; }
        public int CountPages { get; set; }
        public int CountItems { get; set; }
    }
    public class SparePartsInvoiceListResponse
    {
        public List<SparePartsItem> Values { get; set; }
        public int CountPages { get; set; }
        public int CountItems { get; set; }
    }
    public class SparePartsRepairListResponse
    {
        public List<RepairsItem> Values { get; set; }
        public int CountPages { get; set; }
        public int CountItems { get; set; }
    }
    public class SparePartsReserveListResponse
    {
        public List<SparePartsReserveModel> Values { get; set; }
        public int CountPages { get; set; }
        public int CountItems { get; set; }
    }
    public class SparePartsWriteOffListResponse
    {
        public List<SparePartsWriteOffModel> Values { get; set; }
        public int CountPages { get; set; }
        public int CountItems { get; set; }
    }
    public class SparePartsMovementListResponse
    {
        public List<SparePartsMovementModel> Values { get; set; }
        public int CountPages { get; set; }
        public int CountItems { get; set; }
    }
    public class DictionarySpareParts
    {
        public List<DictionaryItemsDTO> StorageList { get; set; }
        public List<DictionaryItemsDTO> FullStorageList { get; set; }
        public List<DictionaryItemsDTO> ContractorsList { get; set; }
        public List<DictionaryItemsDTO> SparePartsList { get; set; }
        public List<DictionaryItemsDTO>  OrganzationsList { get; set; }
        public List<DictionaryItemsDTO> VendorCodeList { get; set; }
        public List<DictionaryItemsDTO> ModuleList { get; set; }
        public List<DictionaryItemsDTO> GroupTractorsList { get; set; }
        public List<DictionaryItemsDTO> Employees { get; set; }
        public List<DictionaryItemsDTO> RepairsActs { get; set; }
        public List<DictionaryItemsDTO> TaskTypes { get; set; }
        public List<DictionaryItemsTraktors> TraktorsList { get; set; }
        public List<DictionaryItemsTraktors> Agr { get; set; }
        public string NewDailyActs { get; set; }
        public int CountGeneralWorks { get; set; }

    }
    public class DictionaryRepairs
    {
        public List<DictionaryItemsDTO> EmployeesList { get; set; }
        public List<DictionaryItemsTraktors> TraktorsList { get; set; }
        public List<DictionaryItemsTraktors> TraktorsShiftList { get; set; }
        public List<DictionaryItemsDTO> TypeFuel { get; set; }
        public List<DictionaryItemsDTO> TypeSpareList { get; set; }
        public List<DictionaryItemsTraktors> EquipmnetsList { get; set; }
        public List<DictionaryItemsDTO> ModuleList { get; set; }
        public List<DictionaryItemsDTO> TypeTasksList { get; set; }
        public List<DictionaryItemsDTO> InvoiceActs { get; set; }
        public int? NewActNumber { get; set; }
        public int? NewRequireNumber { get; set; }
        public int? CheckDailyAct { get; set; }
    }
    public class InfoSpareParts
    {
        public List<DictionaryItemsDTO> SparePartsNamesList { get; set; }
        public List<DictionaryItemsDTO> SparePartsVendorList { get; set; }
        public List<DictionaryItemsDTO> StoragesList { get; set; }

    }
    public class SaveSpareParts
    {
        public SparePartsModel Document { get; set; }
    }
    public class SparePartsDetails
    {
        public Object Clone()
        {
            return this.MemberwiseClone();
        }
        public int? Id { get; set; }
        public int? ActId { get; set; }
        public int TypeAct { get; set; }
        public string Name { get; set; }
        public ContractorInfo Contractor { get; set; }
        public DateTime? Date { get; set; }
        public DateTime? repairStartDate { get; set; }
        public DateTime? repairEndDate { get; set; }
        public decimal? Year { get; set; }
        public int? ContractorsId { get; set; }
        public int? StorageId { get; set; }
        public string Number { get; set; }
        public bool Torg12 { get; set; }
        public bool Service { get; set; }
        public bool Cash { get; set; }
        public int? ServiceXml { get; set; }
        public int? Pk { get; set; }
        public List<SparePartsModel> SparePartsList { get; set; }
        public List<InvoiceDetailsForRepair> RepairsList { get; set; }
    }
    public class ContractorInfo
    {
        public string Code { get; set; }
        public string INN { get; set; }
        public string KPP { get; set; }
    }
    public class WriteOffActsDetails
    {
        public Object Clone()
        {
            return this.MemberwiseClone();
        }
        public int? Id { get; set; }
        public int Status { get; set; }
        public string TypeTech { get; set; }
        public string Act_number { get; set; }
        public string CarsName { get; set; }
        public string CarNum { get; set; }
        public string CarsEquip { get; set; }
        public DateTime? Date { get; set; }
        public string Date1 { get; set; }
        public string CarsNum { get; set; } 
        public string CarsModel { get; set; }
        public string OtvName { get; set; }
        public string ShortName { get; set; }
        public string NodeRepairs { get; set; } 
        public double? SumCount { get; set; }
        public string OrgName { get; set; } 
        public double? SumCost { get; set; }
        public string Comment { get; set; } 
        public List<SparePartsModel> SparePartsList { get; set; }
    }

    public class WriteOff1cSpare : WriteOffActsDetails
    {
        public int CountPosition { get; set; }
        public float? CountSpare { get; set; }
    }


    public class SaveRowSpareParts
    {
        public SparePartsModel Document { get; set; }
        public int? Id { get; set; }
        public int? Pk { get; set; }
        public string Name { get; set; }
        public DateTime? Date { get; set; }
        public int? ContractorsId { get; set; }
        public int? OrganizationId { get; set; }
        public int? StorageId { get; set; }
        public bool Torg12 { get; set; }
        public bool Service { get; set; }
        public bool Cash { get; set; }
        public string Number { get; set; }
       public int TypeAct { get; set; }
    }
    public class SparePartsItem
    {
        public int Id { get; set; }
        public string DateBegin { get; set; }
        public string Comment { get; set; }
        public DateTime DateBegin1 { get; set; }
        public string Number { get; set; }
        public DictionaryItemsDTO Contractor { get; set; }
        public DictionaryItemsDTO Storage { get; set; }
        public int? Torg12 { get; set; }
        public int? Service { get; set; }
        public float? Cost { get; set; }
    }
    public class RepairsItem
    {
        public int Id { get; set; }
        public string DateBegin { get; set; }
        public DateTime DateBegin1 { get; set; }
        public string DateOpen { get; set; } 
        public DateTime? DateOpen1 { get; set; }
        public string DateEnd { get; set; }
        public DateTime? DateEnd1 { get; set; }
        public string CarsName { get; set; }
        public string CarsNum { get; set; }
        public string Number { get; set; }
        public string Status { get; set; }
        public float Cost { get; set; }

    }
    public class SaveMoveSpareParts
    {
        public SparePartsMovementModel Document { get; set; }
    }
    public class DelMoveRow
    {
        public ModelDelMoveRow rows { get; set; }
    }
    public class ModelDelMoveRow
    {
        public int Id { get; set; }
    }
    public class RepairsActsInfo
    {
        public int rep_id { get; set; }
        public int? dail_id { get; set; }
        public string number_act { get; set; }
        public string typeoper { get; set; }
        public DateTime date_start { get; set; }
        public DateTime date_end { get; set; }
        public int? emp_emp_id { get; set; }
        public int? trakt_trakt_id { get; set; }
        public Single? mileage { get; set; }
        public Single? moto { get; set; }
        public int? mod_mod_id { get; set; }
        public int? equi_equi_id { get; set; }
        public int? type_repairs { get; set; }
    }
    public class RepairsReportsInfo
    {
        public List<DictionaryItemsDTO> Tech { get; set; }
        public List<DictionaryItemsDTO> Eq { get; set; }
    }
}
