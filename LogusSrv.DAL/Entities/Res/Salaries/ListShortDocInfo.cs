﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogusSrv.DAL.Entities.Res.Salaries
{
    public class ListShortDocInfo
    {
        public object Values { get; set; }
        public int CountPages { get; set; }
        public int CountItems { get; set; }
    }
}
