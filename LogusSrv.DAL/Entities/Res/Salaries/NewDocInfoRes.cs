﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogusSrv.DAL.Entities.Res.Salaries
{
    public class NewDocInfoRes
    {
        public string Date { get; set; }
        public string Number { get; set; }
    }
}
