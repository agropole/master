﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using LogusSrv.DAL.DBModel;
using LogusSrv.DAL.Entities.DTO;

namespace LogusSrv.DAL.Entities.Res
{
    public partial class DailyRepairsModel
    {
        public GeneralInfoDaily Main { get; set; }
        public List<TableDataDaily> Spendings { get; set; }
    }

    public partial class GeneralInfoDaily
    {
        public int? Id { get; set; }
        public int? Rep_id { get; set; }
        public string DailyNum { get; set; }
        public int RepairsActs { get; set; }
        public DateTime Date { get; set; }

        [Required(ErrorMessage = "Не введено значение в поле \"Начало работы\"")]
        [RegularExpression(@"^(0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$", ErrorMessage = "Значение в поле \"Начало работы\" должно находиться в диапазоне 00:00 - 23:59")]
        public string timeStart { get; set; }

        [Required(ErrorMessage = "Не введено значение в поле \"Окончание работы\"")]
        [RegularExpression(@"^(0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$", ErrorMessage = "Значение в поле \"Окончание работы\" должно находиться в диапазоне 00:00 - 23:59")]
        public string timeEnd { get; set; }

        public int? TraktorId { get; set; }
        public int? Agr { get; set; }
        public string CarsName { get; set; } 
        public string CarsNum { get; set; }  
        public string CarsModel { get; set; } 
        public string OtvName { get; set; }
        public string TypeTech { get; set; }
        public string TypeOper{ get; set; }
        public Boolean GeneralWorks { get; set; }
        public int CountGeneralWorks { get; set; }

    }

    public partial class TableDataDaily
    {
        public int Number1 { get; set; }
        public int Id { get; set; }
        public DateTime? Date1 { get; set; }
        public EmpTaskInfo Employees { get; set; }
        public EmpTaskInfo Tasks { get; set; }
        public decimal? RateShift { get; set; }
        public decimal? RatePiecework { get; set; }
        public int? Hours { get; set; }
        public decimal? SumCost { get; set; }
        public string ActNumber { get; set; }
        public string TaskName { get; set; }
        public int TaskId { get; set; }
        public string Trakt { get; set; }
        public string Agr { get; set; }
        public string EmpName { get; set; }
        public string OtvName { get; set; }
        public int EmpId { get; set; }
        public int? TraktId { get; set; }
        public int? AgrId { get; set; }
    }

    public partial class EmpTaskInfo
    {
        public int? Id { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
    }

    public partial class DailyRepairsModelList
    {
        public int Id { get; set; }
        public string Number { get; set; }
        public string Date { get; set; }
        public DateTime Date1 { get; set; }
        public string CarsName { get; set; }
        public string CarsNum { get; set; }
        public string Agr { get; set; }
        public string Status { get; set; }
    }
}
