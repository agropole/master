﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LogusSrv.DAL.Entities.DTO;
using LogusSrv.DAL.Entities.DTO.ExportExcel;

namespace LogusSrv.DAL.Entities.Res
{
    public class PlanFactModel
    {
        public string DateShifts { get; set; }

        public DateTime DateBegin { get; set; }

        public List<PlanAndFactDTO> ShiftInfo1 { get; set; }
        public List<PlanAndFactDTO> ShiftInfo2 { get; set; }
    }
}
