﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LogusSrv.DAL.Entities.DTO;
using System.ComponentModel.DataAnnotations;

namespace LogusSrv.DAL.Entities.Res
{
    public class ClosedWayListModel
    {
        public int? WaysListId { get; set; }

        public int? TraktorId { get; set; }
        public DateTime DateBegin { get; set; }
        public string DriverName { get; set; }
        public string TraktorName { get; set; }
        public decimal? MotoStart { get; set; }
        public decimal? MotoEnd { get; set; }
        public bool? AllResource { get; set; }
        public decimal? leftFuel { get; set; }
        public decimal? IssuedFuel { get; set; }
        public decimal? RemainFuel { get; set; }
        public decimal? SumWorkTime { get; set; }
        public decimal? SumWorkDay { get; set; }
        public int? OrgId { get; set; }
        public string WaysListNum { get; set; }
        public Boolean? IsRepair { get; set; }
        public List<GetTaskDTO> Tasks { get; set; }

    }
}
