﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity.Spatial;
using LogusSrv.DAL.DBModel;
using LogusSrv.DAL.Entities.DTO;
using LogusSrv.DAL.Entities.DTO.SeedingBridle;

namespace LogusSrv.DAL.Entities.Res
{
    public class LogusDictionary : DictionaryItemsDTO
    {
        public string TableName { get; set; }
    }

    public class LogusDictionaryData
    {
        public List<LogusDictionaryColumn> ColumnDefs { get; set; }
        public string Request { get; set; }
        public string RequestUpdate { get; set; }
        public string index_column { get; set; }
        public string RequestDetail { get; set; }
    }

    public class LogusDictionaryColumn
    {
        public int id { get; set;}
        public string field { get; set;}
        public string displayName {get; set;}
        public string cellTemplate {get; set;}
        public string width {get; set;}

        public FilterParamModel filterParams { get; set; }

        public string filter { get; set; }
    }

    public class FilterParamModel
    {

        public string newRowsAction { get; set; }

        public bool apply { get; set; }
    }


    
}