﻿/*
 * created by ofirtych 2015-08-21
 * 
 * */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LogusSrv.DAL.Entities.DTO;
using System.ComponentModel.DataAnnotations;

namespace LogusSrv.DAL.Entities.Res
{
    public class TaskDashboardModel
    {
      
        public decimal? Worktime { get; set; }
        public decimal? TotalSalary { get; set; }
     
    }
}
