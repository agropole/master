﻿using LogusSrv.DAL.Entities.DTO;
using LogusSrv.DAL.Entities.DTO.DriverWayBills;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogusSrv.DAL.Entities.Res
{
    public class Top100GPSmodel
    {
        public int? id { get; set; }
        public string coord { get; set; }
        public DateTime? date { get; set; }
        public DateTime? endDate { get; set; }
        public decimal? direction { get; set; }
        public decimal? speed { get; set; }
        public decimal? mileage { get; set; }
        public bool? stop { get; set; }
        public bool? parking { get; set; }
        public string massage { get; set; }
        public PrintShiftDTO sheet { get; set; }
    }
    public class GPSArmModel
    {
        public List<Top100GPSmodel> Coord { get; set; }
        public PrintShiftDTO Sheet { get; set; }
    }
}
