﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogusSrv.DAL.Entities.DTO
{
    public class PrintRepairsDTO1
    {
        public string NumberAct { get; set; }
        public string CarsName { get; set; }
        public string EqName { get; set; }
        public string ReqNum { get; set; }
        public float? MotoHours { get; set; }
        public float? Mileage { get; set; }
        public string Employee { get; set; }
        public string[] RepairsTypeMark { get; set; }

        public int RepairsType { get; set; }
    }
}
