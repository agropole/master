﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using LogusSrv.DAL.Entities.DTO;

namespace LogusSrv.DAL.Entities.Res
{
    public class GetWayListModel 
    {
        public decimal CountItems {get;set;}
        public decimal CountPages { get; set; }
        public List<WaysListsDTO> Values { get; set; }
    }
}