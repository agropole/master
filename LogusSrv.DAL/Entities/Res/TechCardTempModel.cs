﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity.Spatial;
using LogusSrv.DAL.DBModel;
using LogusSrv.DAL.Entities.DTO;
using LogusSrv.DAL.Entities.DTO.SeedingBridle;
using System.ComponentModel.DataAnnotations;
using LogusSrv.DAL.Entities.DTO.ActsSzr;

namespace LogusSrv.DAL.Entities.Res
{
    public class TechCardTempModel
    {
        public int id { get; set; }
        public int? id_tc { get; set; }
        public int? plant_plant_id { get; set; }
        public string plant_name { get; set; }
        public int? tpsort_tpsort_id { get; set; }
        public string tpsort_name { get; set; }
       // public List<Field> fields { get; set; }
    }

    public class TCTempTask
    {
        public int id { get; set; }
        public int? plant_id { get; set; }
        public string plant_name { get; set; }
        public int field_id { get; set; }
        public string field_name { get; set; }
        public int? id_tc { get; set; }
        public string trakt { get; set; }
        public DateTime date { get; set; }
        public bool add_value { get; set; }
        public decimal? cons_fuel { get; set; }
        public decimal? total_salary { get; set; }
    }

  



    public class TCParamValueTemp
    {
        public float? value { get; set; }
        public int id_tc_param { get; set; }

    }



    public class TCFuelDictionaryTemp : DictionaryItemsDTO
    {
    }

    public class PlantSortFieldDictionaryTemp : DictionaryItemsDTO
    {
        public List<SortFieldDictionary> Values { get; set; }
        public List<FieldDictionary> Fields { get; set; }
    }

    public class SortFieldDictionaryTemp : DictionaryItemsDTO
    {
        public List<FieldDictionary> Fields { get; set; }
    }

    public class FieldDictionaryTemp : DictionaryItemsDTO
    {
        public decimal? Area { get; set; }
        public DictionaryItemsDTO Sorts { get; set; }
        public string Coord { get; set; }
    }

    public class WorkDictionaryTemp : DictionaryItemsDTO
    {
        public DictionaryItemsDTO FuelType { get; set; }

        public decimal? Consumption { get; set; }
        public decimal? ShiftRate { get; set; }
        public decimal? EmpPrice { get; set; }
    }

    public class TCTempDetailModel
    {
        public TCTempModel TC { get; set; }
        public NZPModelTemp NZP { get; set; }
        public List<WorkModelTemp> Works { get; set; }
        public List<SupportStaffModelTemp> SupportStuff { get; set; }
        public List<SZRModelTemp> DataSZR { get; set; }
        public List<FertilizerModelTemp> DataChemicalFertilizers { get; set; }
        public List<SeedModelTemp> DataSeed { get; set; }
        public List<DopMaterialsModelTemp> DataDopMaterial { get; set; }
        public List<DataServiceTemp> DataServices { get; set; }
        public List<DataTotalModelTemp> DataTotal { get; set; }
    }

    public class DataServiceTemp
    {
        public int? Id { get; set; }
        [Required(ErrorMessage = "Не выбран тип услуг")]
        public DictionaryItemsDTO NameService { get; set; }
        [Required(ErrorMessage = "Не указана стоимость услуг")]
        public float? Cost { get; set; }
    }

    public class TCTempModel
    {
        public int? Id { get; set; }
        [Required(ErrorMessage = "Не выбрана культура")]
        public int? PlantId { get; set; }
        public int? SortId { get; set; }
        public float? Area { get; set; }
        public float? Productivity { get; set; }
        public float? GrossHarvest { get; set; }
        public int YearId { get; set; }
        public int TypeModuleId { get; set; }
    }

    public class NZPModelTemp
    {
        public float? Energy { get; set; }
        public float? Salary { get; set; }
        public float? SZR { get; set; }
        public float? ChemicalFertilizers { get; set; }
        public float? DopMaterial { get; set; }
        public float? Seed { get; set; }
        public float? Total { get; set; }
    }
    public class DataTotalModelTemp
    {
        public string Name { get; set; }
        public float Total { get; set; }
        public float? PerGa { get; set; }
        public float? PerT { get; set; }
        public List<int> ParamIds { get; set; }
    }

    public class WorkModelTemp
    {
        public int? Id { get; set; }
        [Required(ErrorMessage = "Не выбрано название работы")]
        public DictionaryItemsDTO Name { get; set; }
        [Required(ErrorMessage = "Не заполнено поле Кратность работ")]
        public decimal? Count { get; set; }
        [Required(ErrorMessage = "Не заполнено поле Коэффициент работ")]
        public decimal? Factor { get; set; }
        [Required(ErrorMessage = "Не заполнено поле Объем работ")]
        public float? Workload { get; set; }
        public DateTime? DateStart { get; set; }
        public DateTime? DateEnd { get; set; }
     //   public int DayCount { get; set; }
        public List<DictionaryItemsDTO> Equipment { get; set; }
        public List<DictionaryItemsDTO> Tech { get; set; }
    //    public string Comment { get; set; }
         public float? FuelCost { get; set; }
     //   [Required(ErrorMessage = "Не заполнено поле Сменная норма выработки")]
        public float? ShiftRate { get; set; }
        public float? ShiftCount { get; set; }
    //    [Required(ErrorMessage = "Не заполнено поле Расценка")]
        public float? EmpPrice { get; set; }
        public float? EmpCost { get; set; }
        public float? UnicFlag { get; set; }
        public float? Rate { get; set; }
        public int? FuelId { get; set; }
        public float? FuelCount { get; set; }
        public int index { get; set; }
        public DictionaryItemsDTO FuelType { get; set; }
        public float? Consumption { get; set; }
        public int? Id1 { get; set; }

    }

    public class SupportStaffModelTemp
    {
        [Required(ErrorMessage = "Не заполнено поле Наименование работы для Вспомогательного персонала")]
        public DictionaryItemsDTO NameSelected { get; set; }
        [Required(ErrorMessage = "Не заполнено поле Количество для Вспомогательного персонала")]
        public int? Count { get; set; }
        [Required(ErrorMessage = "Не заполнено поле Расценка для Вспомогательного персонала")]
        public float? Price { get; set; }
        public float? Cost { get; set; }
        public int? Id_tc_operation { get; set; }
        public float? UnicFlag { get; set; }
        public int? index { get; set; }
        public int? id_tc_oper { get; set; }
    }

    public class MaterialModelTemp
    {
        [Required(ErrorMessage = "Не заполнено поле Наименование работы для материалов")]
        public DictionaryItemsDTO NameSelectedWorkForMaterial { get; set; }
      //  [Required(ErrorMessage = "Не заполнено поле Цена для материалов")]
        public float? Price { get; set; }
        public float? MaterialArea { get; set; }
        public float? Cost { get; set; }
        [Required(ErrorMessage = "Не заполнено поле Расход для материалов")]
        public float? ConsumptionRate { get; set; }
        public int Type { get; set; }
        public int? index { get; set; }
        public int? id_tc_oper { get; set; }
    }

    public class SZRModelTemp : MaterialModelTemp
    {
        public DictionaryItemsDTO NameSZR { get; set; }


    }

    public class FertilizerModelTemp : MaterialModelTemp
    {
        public DictionaryItemsDTO NameFertilizer { get; set; }
    }

    public class SeedModelTemp : MaterialModelTemp
    {
        public DictionaryItemsDTO NameSeed { get; set; }
    }

    public class DopMaterialsModelTemp : MaterialModelTemp
    {
        public DictionaryItemsDTO NameDopMaterial { get; set; }
    }


    public class TCItemTemp
    {
        public int Id { get; set; }
        public string Plant { get; set; }
        public string Sort { get; set; }
        public string Field { get; set; }
        public float? Cost { get; set; }
        public int Status { get; set; }
        public int Number { get; set; }
        public int PlantId { get; set; }
        public int? SortId { get; set; }
    }

}
