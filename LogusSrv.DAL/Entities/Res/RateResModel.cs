﻿using LogusSrv.DAL.Entities.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogusSrv.DAL.Entities.Res
{
    public class RateResModel
    {
        public int Id { get; set; }

        public int TptasId { get; set; }

        public int? PlantId { get; set; }

        public int? AgroreqId { get; set; }

        public int? EquiId { get; set; }

        public int? TraktId { get; set; }
        public int? GroupTrId { get; set; }

        public decimal? ShiftOutput { get; set; }

        public decimal? RateShift { get; set; }

        public decimal? RatePiecework { get; set; }

        public decimal? FuelConsumption { get; set; }

        public int CountCheck { get; set; }
        public DictionaryItemsDTO Typefuel { get; set; }
    }
}
