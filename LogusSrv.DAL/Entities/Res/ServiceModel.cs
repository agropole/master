﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity.Spatial;
using LogusSrv.DAL.DBModel;
using LogusSrv.DAL.Entities.DTO;
using LogusSrv.DAL.Entities.DTO.SeedingBridle;
using System.ComponentModel.DataAnnotations;

namespace LogusSrv.DAL.Entities.Res
{
    public class ServiceModel
    {
        public int Id { get; set; }
        public int Id1 { get; set; }
        public DictionaryItemsDTO TypeSalary { get; set; }
        public DictionaryItemsDTO NameService { get; set; }
        public DictionaryItemsDTO NamePlant{ get; set; }
        public DateTime? Date { get; set; }
        public string Act_number { get; set; }
        public int? Act_number1 { get; set; }
        public string NameWork { get; set; }
        public decimal? Price { get; set; }
        public decimal? Count { get; set; }
        public decimal? Cost { get; set; }

    }
    public class ServiceListResponse
    {
        public List<ServiceModel> Values { get; set; }
        public int CountPages { get; set; }
        public int CountItems { get; set; }
    }

    public class InfoService
    {
        public List<DictionaryItemsDTO> ServiceList { get; set; }
        public List<DictionaryItemsDTO> TypeDocList { get; set; }
        public List<DictionaryItemsDTO> AllPlants { get; set; }

    }
    public class SaveService
    {
        public ServiceModel Document { get; set; }
    }
 
}
