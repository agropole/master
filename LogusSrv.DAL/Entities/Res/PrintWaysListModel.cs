﻿using System;
using System.Collections;
using System.Collections.Generic;
using LogusSrv.DAL.Entities.DTO;


namespace LogusSrv.DAL.Entities.Res
{
    public class PrintWaysListModel 
    {
       public List<PrintDTO1> query1 {get;set;}
       public List<PrintDTO2> query2 {get;set;}
       public List<PrintDTO3> query3 {get;set;}
       public List<PrintDTO4> query4 {get;set;}
       public decimal? sum1 { get; set; }
       public decimal? sum2 { get; set; }
       public decimal? sum3 { get; set; }
       public decimal? sum4 { get; set; }
       public decimal? sum5 { get; set; }
    }
}