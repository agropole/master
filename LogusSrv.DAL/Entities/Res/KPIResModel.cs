﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LogusSrv.DAL.Entities.DTO;

namespace LogusSrv.DAL.Entities.Res
{
    public class KPIResModel
    {
        public List<FieldDictionary> Fields { get; set; }
        public List<KPIWorkResModel> Works { get; set; }
    }

    public class KPIInfoRes
    {
        public List<DictionaryItemsDTO> YearList { get; set; }
        public List<DictionaryItemsDTO> TCList { get; set; }
    }

    public class KPITaskResModel
    {
        public int Id { get; set; }
        public string Field { get; set; }
        public DateTime Date1 { get; set; }
        public string Date { get; set; }
        public decimal? Kpi { get; set; }
        public string Name { get; set; }
        public int IdTypeTask { get; set; }
        public int IdTypeTaskForGroup { get; set; }
    }

    public class KPIWorkResModel
    {
        public int Id { get; set; }
        public int IdTcOperation { get; set; }
        public int IdTypeTask { get; set; }
        public string Name { get; set; }
        public DateTime? Date1 { get; set; }
        public DateTime? Date2 { get; set; }
        public string DateStart { get; set; }
        public string DateFinish { get; set; }
        public List<KPITaskResModel> tasks { get; set; }
        public TotalKPI TotalKPI { get; set; }
    }

    public class TotalKPI
    {
        public int total { get; set; }
        public decimal sum { get; set; }
        public decimal rate { get; set; }
    }

    public class KPIValueResModel
    {

        public int? Id { get; set; }
        public int? IdKPItoPlants { get; set; }
        public int IdTypeTask { get; set; }
        public int IdPlant { get; set; }
        public int IdTas { get; set; }
        public int IdKpi { get; set; }
        
        public string Name { get; set; }
        public float? Value { get; set; }

        public float StandartValue { get; set; }
        public float DevLimit { get; set; }
        public float? DevLimit2 { get; set; }
        public float? DevLimit3 { get; set; }

        public int Rate { get; set; }
        public int? Rate2 { get; set; }
        public int? Rate3 { get; set; }

    }

    public class KPIForTaskResModel
    {
        public List<KPIValueResModel> ListSelected { get; set; }
        public List<KPIValueResModel> ListOther { get; set; }
    }
}
