﻿using System.Collections.Generic;
using LogusSrv.DAL.Entities.DTO;

namespace LogusSrv.DAL.Entities.Res
{
    public class WaysListForCreateModel
    {
       public List<DictionaryItemsTraktors> TraktorsList { get; set; }
       public List<DictionaryItemsDTO> EmployeesList { get; set; }
       public List<DictionaryItemsTraktors> EquipmnetsList { get; set; }
       public List<DictionaryItemsDTO> ChiefsList { get; set; }
       public List<DictionaryItemsDTO> TasksList { get; set; }
       public List<DictionaryItemsDTO> OrganizationsList { get; set; }
       public List<FieldDictionary> FieldsList { get; set; }
       public List<DictionaryItemForFields> AllFields { get; set; }
       public List<DictionaryItemForFields> FieldsAndPlants { get; set; }
       public List<DictionaryItemsDTO> TypePriceList { get; set; }
       public List<DictionaryItemsDTO> TypeFuel { get; set; }
       public List<DictionaryItemsDTO> AgroreqList { get; set; }
       public List<DictionaryItemsDTO> PlantList { get; set; }
       public decimal NumWaysList { get; set; }
    }

}
