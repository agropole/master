﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LogusSrv.DAL.Entities.DTO;

namespace LogusSrv.DAL.Entities.Res
{
    public partial class SpareReportsInfo
    {
        public List<DictionaryItemsDTO> Storages { get; set; }
    }

    public partial class DateInterval 
    {  
        public DateTime start { get; set; }
        public DateTime end { get; set; }
    }
}
