﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LogusSrv.DAL.Entities.DTO;

namespace LogusSrv.DAL.Entities.Res
{
    public class UserModel
    {
        public int Id { get; set; }
        public int Id1 { get; set; }
        public int UserId { get; set; }
        public int EmpId { get; set; }
        public string UserLogin { get; set; }
        public string UserName { get; set; }
        public int? OrgId { get; set; }
        public string OrgName { get; set; }
        public List<string> SubsystemsList { get; set; }
        public int? TypeOrg { get; set; }
        public int? RoleId { get; set; }
        public string Password { get; set; }
        public string Note { get; set; }
        public int? PostId { get; set; }
        public DictionaryItemsDTO NameUser { get; set; }
        public DictionaryItemsDTO NameRole { get; set; }
    }

    public class RoleModel
    {
        public int? Id { get; set; }
        public List<EmpListModel> EmpList { get; set; }
        public string Name { get; set; }
    }

    public class EmpListModel
    {
        public int? Id { get; set; }
        public string Value { get; set; }
        public string Name { get; set; }
        public string IdBox { get; set; }
    }


    public class UserListResponse
    {
        public List<UserModel> Values { get; set; }
        public int CountPages { get; set; }
        public int CountItems { get; set; }

    }
    public class RoleListResponse
    {
        public List<RoleModel> Values { get; set; }
        public int CountPages { get; set; }
        public int CountItems { get; set; }

    }
    public class UserSelectedPlants : InfoService
    {
     public List<DictionaryItemsDTO> YearList { get; set; }
    }

    public class InfoUser
    {
        public List<DictionaryItemsDTO> UserList { get; set; }
        public List<DictionaryItemsDTO> RoleList { get; set; }

    }

    public class SaveUser
    {
        public UserModel Document { get; set; }
    }

    public class PlantModel
    {
        public int year { get; set; }
        public int plantId { get; set; }
        public int id { get; set; }
        public string name { get; set; }
    }

    public class ParamOperationModel
    {
               public int? OrgId { get; set; }
               public string INN { get; set; }
               public string KPP  { get; set; }
               public string OGRN { get; set; }
               public int? DirectorId { get; set; }
               public string Speed { get; set; }
               public string Stop { get; set; }
               public string Parking { get; set; }
               public string Key { get; set; }
               public string MaxParking { get; set; }
               public DateTime summer_date { get; set; }
               public DateTime winter_date { get; set; }
       
    }
    public class ParamOperDistModel
    {
        public List<DictionaryItemsDTO> EmployeeList { get; set; }
        public List<DictionaryItemsDTO> OrgList { get; set; }
    }


    enum TypeModule { 
         Template = 1,
         SectionTechcard = 2,
    }
}