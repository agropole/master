﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LogusSrv.DAL.Entities.DTO;


namespace LogusSrv.DAL.Entities.Res
{
    public class BasicInfoIncoming
    {
        public List<DictionaryItemsDTO> VATs { get; set; }
        public List<DictionaryItemsDTO> Type_salary_docs { get; set; }
        public List<DictionaryItemsDTO> Contractors { get; set; }
    }
    public class InfoIncomingModel : BasicInfoIncoming
    {
        public List<DictionaryItemsTypeMaterials> TypeMaterials { get; set; }
    }

    public class InfoIncomingFuelModel : BasicInfoIncoming
    {
        public List<FuelDictionary> Fuels { get; set; }
    }

    public class FuelDictionary: DictionaryItemsDTO
    {
        public DictionaryItemsDTO type_salary_doc { get; set; }
    }

    public class DictionaryItemsTypeMaterials
    {
        public int? Id { get; set; }
        public string Name { get; set; }
        public List<DictionaryItemsDTO> Materials { get; set; }
    }

    public class BasicIncome
    {
        public int? Id { get; set; }
        public float Price { get; set; }
        public float Count { get; set; }
        public float Cost { get; set; }
        public DateTime? Date { get; set; }
        public string Comment { get; set; }
        public DictionaryItemsDTO Contractor { get; set; }
        public DictionaryItemsDTO VAT { get; set; }
        public DictionaryItemsDTO Type_salary_doc { get; set; }
    }

    public class Income : BasicIncome
    {
        
        public int TypeMaterialId { get; set; }
        public DictionaryItemsTypeMaterials Type { get; set; }
        public int MaterialId { get; set; }
        public DictionaryItemsDTO Name { get; set; }
    }

    public class IncomeFuel : BasicIncome
    {

        public DictionaryItemsDTO Fuel { get; set; }
    }
    public class InfoBalanceMaterialsModel : BasicInfoIncoming
    {
        public List<DictionaryItemsDTO> TypeMaterials { get; set; }
        public List<DictionaryItemsDTO> OrganizationsList { get; set; }
        public List<DictionaryItemsDTO> EmpList { get; set; }
        public List<DictionaryItemsDTO> ContractorsList { get; set; }
    }
    public class BalanceMaterials
    {
        public int? OrgId { get; set; }
        public int? Id { get; set; }
        public int Id1 { get; set; }
        public string Name { get; set; }
        public string Material { get; set; }
        public Single Current_price { get; set; }
        public Single Summa { get; set; }
        public Single Balance { get; set; }
        public string Employees { get; set; }
        public int? MatId { get; set; }
        public string Description { get; set; }
        public int? IdMaterial { get; set; }
        public int? IdEmployees { get; set; }

    }

}