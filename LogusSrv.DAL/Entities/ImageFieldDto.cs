﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;


namespace LogusSrv.DAL.Entities
{
    public class ImageFieldDto
    {     
        public string note { get; set; }
        public string marker { get; set; }
        public DateTime? date { get; set; }
        public string date2 { get; set; }
        public int fiel_fiel_id { get; set; }
        public int id { get; set; }
        public string name_photo { get; set; }
    }
}
