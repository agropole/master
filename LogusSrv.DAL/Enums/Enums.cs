﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogusSrv.DAL.Enums
{
    public class Enums
    {
    }

    public enum SortDirections
    {
        OrderBy,
        OrderByDescending
    }

    public enum WayListStatus
    {
        Issued = 1,
        Completed = 3,
        Closed = 4,
    }

    public enum DayTaskStatus
    {
        Create = 0,
        Assigned = 1,
        Copy = 2,
        Issued = 3,
        Tablet = 4
    }

    public enum XmlFileStatus
    {
        Load = 1,
        Parse = 2,
        ErrorParse = 3,
    }

    public enum TypeOrganization
    {
        Section = 1,
        Company = 2,
    }

    public enum TypePrice
    {
        DayFiled = 1,
        Ga = 2,
        Tonn = 3,
        Kilometers = 4,
        Hours = 5
    }

    public enum TypeShift
    {
        First = 1,
        Second = 2,
    }

    public enum WayListTaskSatuts
    {
        Create = 1,
        InWork = 2,
        Complite  = 3,
    }

    public enum TypeFields
    {
        Fields = 1,
        Object = 2,
    }

    public enum StatusFtp
    {
        Approved = 1,
        NoApproved = 2,
        Closed = 3
    }

}
