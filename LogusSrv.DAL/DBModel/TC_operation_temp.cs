﻿namespace LogusSrv.DAL.DBModel
{
    using System;
    using System.Collections.Generic;

    public partial class TC_operation_temp
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public TC_operation_temp()
        {
            //this.Tasks = new HashSet<Tasks>();
            //this.TC_operation_to_Tech = new HashSet<TC_operation_to_Tech>();
            this.TC_param_value_temp = new HashSet<TC_param_value_temp>();
        }

        public int id { get; set; }
        public int id_temp { get; set; }
        public int tptas_tptas_id { get; set; }
        public Nullable<decimal> multiplicity { get; set; }
        public Nullable<System.DateTime> date_start { get; set; }
        public Nullable<System.DateTime> date_finish { get; set; }
        public Nullable<decimal> factor { get; set; }
        public string comment { get; set; }
        public Nullable<int> id_rate { get; set; }
        public Nullable<int> id_agroreq { get; set; }

        //public virtual Agrotech_requirment Agrotech_requirment { get; set; }
        //public virtual Task_rate Task_rate { get; set; }
        //[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        //public virtual ICollection<Tasks> Tasks { get; set; }
        public virtual TC_temp TC_temp { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TC_operation_to_Tech_temp> TC_operation_to_Tech_temp { get; set; }
        public virtual Type_tasks Type_tasks { get; set; }
        //[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TC_param_value_temp> TC_param_value_temp { get; set; }
    }
}
