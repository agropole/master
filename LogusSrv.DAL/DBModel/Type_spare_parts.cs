﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogusSrv.DAL.DBModel
{
    using System;
    using System.Collections.Generic;

    public partial class Type_spare_parts
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]

        public int tsp_id { get; set; }
        public string code { get; set; }
        public string name { get; set; }
        public string uniq_code { get; set; }
        public string description { get; set; }
        public Nullable<bool> deleted { get; set; }
    }
}
