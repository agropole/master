﻿namespace LogusSrv.DAL.DBModel
{
    using System;
    using System.Collections.Generic;

    public partial class TC_temp
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public TC_temp()
        {
            this.KPI_value = new HashSet<KPI_value>();
            this.Tasks = new HashSet<Tasks>();
            this.TC_operation_temp = new HashSet<TC_operation_temp>();
            this.TC_param_value_temp = new HashSet<TC_param_value_temp>();
            this.TC_to_service_temp = new HashSet<TC_to_service_temp>();
         //   this.TC_to_Sheet_track_temp = new HashSet<TC_to_Sheet_track>();
            this.TC1 = new HashSet<TC>();
        }

        public int id { get; set; }
        public Nullable<int> tpsort_tpsort_id { get; set; }
        public Nullable<int> plant_plant_id { get; set; }
        public Nullable<int> fiel_fiel_id { get; set; }
        public Nullable<DateTime> date { get; set; }
        public Nullable<int> type_module { get; set; }

        public virtual Fields Fields { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<KPI_value> KPI_value { get; set; }
        public virtual Plants Plants { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Tasks> Tasks { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TC_operation_temp> TC_operation_temp { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TC_param_value_temp> TC_param_value_temp { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TC_to_service_temp> TC_to_service_temp { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
     //   public virtual ICollection<TC_to_Sheet_track> TC_to_Sheet_track { get; set; }
     //   [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TC> TC1 { get; set; }
        public virtual TC TC2 { get; set; }
        public virtual Type_sorts_plants Type_sorts_plants { get; set; }
    }
}
