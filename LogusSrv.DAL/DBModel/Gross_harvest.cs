//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace LogusSrv.DAL.DBModel
{
    using System;
    using System.Collections.Generic;
    
    public partial class Gross_harvest
    {
        public int id { get; set; }
        public int ftp_ftp_id { get; set; }
        public Nullable<Decimal> count { get; set; }
        public Nullable<Decimal> price { get; set; }
        public Nullable<Decimal> cost { get; set; }
        public System.DateTime date { get; set; }
    
        public virtual Fields_to_plants Fields_to_plants { get; set; }
    }
}
