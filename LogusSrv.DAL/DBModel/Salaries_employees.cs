//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace LogusSrv.DAL.DBModel
{
    using System;
    using System.Collections.Generic;
    
    public partial class Salaries_employees
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Salaries_employees()
        {
            this.Salaries = new HashSet<Salaries>();
            this.Salary_details = new HashSet<Salary_details>();
        }
    
        public int salemp_id { get; set; }
        public string first_name { get; set; }
        public string second_name { get; set; }
        public string middle_name { get; set; }
        public string short_name { get; set; }
        public string in_code { get; set; }
        public bool deleted { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Salaries> Salaries { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Salary_details> Salary_details { get; set; }
    }
}
