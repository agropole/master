﻿using System;
namespace LogusSrv.DAL.DBModel
{
    using System;
    using System.Collections.Generic;

    public partial class Spare_parts_Current
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Spare_parts_Current()
        {

        }
        public int sp_id { get; set; }
        public int spare_spare_id { get; set; }
        public DateTime date { get; set; }
        public Single count { get; set; }
        public Single price { get; set; }
        public Single? nds { get; set; }
        public int store_store_id{ get; set; }
    }
}
