﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogusSrv.DAL.DBModel
{
    public partial class Defect_Records
    {
        public int def_id { get; set; }
        public string defect { get; set; }
        public int? tptas_tptas_id { get; set; }
        public int? rep_rep_id { get; set; }
        public int? defect_type { get; set; }
    }
}
