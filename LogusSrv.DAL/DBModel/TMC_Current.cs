﻿using System;
namespace LogusSrv.DAL.DBModel
{
    using System;
    using System.Collections.Generic;

    public partial class TMC_Current
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public TMC_Current()
        {
        

        }

        public int tmc_id { get; set; }
        public int? mat_mat_id { get; set; }
        public int cont_cont_id { get; set; }
        public Nullable<int> emp_emp_id { get; set; }
        public DateTime act_date { get; set; }
        public Single? count { get; set; }
        public Single price { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual Materials Materials { get; set; }
        public virtual Employees Employees { get; set; }

    }
}