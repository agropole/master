//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace LogusSrv.DAL.DBModel
{
    using System;
    using System.Collections.Generic;
    
    public partial class Prices
    {
        public int pris_id { get; set; }
        public int tptas_tptas_id { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public Nullable<decimal> price_day { get; set; }
        public Nullable<decimal> price_volume { get; set; }
        public Nullable<decimal> add_price_volume { get; set; }
        public Nullable<decimal> add_price_ha { get; set; }
        public Nullable<int> modtr_modtr_id { get; set; }
        public Nullable<int> modequi_modequi_id { get; set; }
        public string mu { get; set; }
        public Nullable<decimal> ind_min { get; set; }
        public Nullable<decimal> ind_max { get; set; }
        public int ind_ind_id { get; set; }
        public Nullable<decimal> consumption_rate { get; set; }
        public int plant_plant_id { get; set; }
    
        public virtual Indicators Indicators { get; set; }
        public virtual Model_Equipments Model_Equipments { get; set; }
        public virtual Model_Traktors Model_Traktors { get; set; }
        public virtual Plants Plants { get; set; }
        public virtual Type_tasks Type_tasks { get; set; }
    }
}
