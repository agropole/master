﻿using System;
namespace LogusSrv.DAL.DBModel
{
    using System;
    using System.Collections.Generic;

    public partial class Service_records
    {
        public int serv_id { get; set; }
        public int? tpsal_tpsal_id { get; set; }
        public int? service_id { get; set; }
        public int? plant_plant_id { get; set; }
        public Nullable<System.DateTime> date { get; set; }
        public string act_num { get; set; }
        public string name_service { get; set; }
        public Nullable<decimal> cost { get; set; }
        public Nullable<decimal> price { get; set; }
        public Nullable<decimal> count { get; set; }

    }
}