﻿
namespace LogusSrv.DAL.DBModel
{
    using System;
    using System.Collections.Generic;

    public partial class Day_repairs_detail
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Day_repairs_detail()
        {
        }

        public int drd_id { get; set; }
        public int dr_dr_id { get; set; }
        public Nullable<int> tptas_tptas_id { get; set; }
        public Nullable<int> trak_trak_id { get; set; }
        public Nullable<int> equi_equi_id { get; set; }
        public Nullable<int> otv_emp_id { get; set; }
        public string number { get; set; }
        public string comment { get; set; }
        public Nullable<int> status { get; set; }

    }
}
