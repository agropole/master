//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace LogusSrv.DAL.DBModel
{
    using System;
    using System.Collections.Generic;
    
    public partial class Sorts_plants_data
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Sorts_plants_data()
        {
            this.Ind_sorts_plants_data = new HashSet<Ind_sorts_plants_data>();
        }
    
        public int sortdat_id { get; set; }
        public Nullable<int> tpsort_tpsort_id { get; set; }
        public Nullable<int> fielplan_fielplan_id { get; set; }
        public Nullable<decimal> value_plan { get; set; }
        public Nullable<decimal> value_fact { get; set; }
        public Nullable<int> tprep_tprep_id { get; set; }
    
        public virtual Fields_to_plants Fields_to_plants { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Ind_sorts_plants_data> Ind_sorts_plants_data { get; set; }
        public virtual Type_reproduction Type_reproduction { get; set; }
        public virtual Type_sorts_plants Type_sorts_plants { get; set; }
    }
}
