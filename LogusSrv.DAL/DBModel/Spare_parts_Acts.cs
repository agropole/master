﻿using System;
namespace LogusSrv.DAL.DBModel
{
    using System;
    using System.Collections.Generic;

    public partial class Spare_parts_Acts
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Spare_parts_Acts()
        {

        }

        public int spss_id { get; set; }
        public DateTime date { get; set; }
        public string act_number { get; set; }
        public int? contr_id { get; set; }
        public int? torg_id { get; set; }
        public int? store_store_id { get; set; }
        public int? type_act { get; set; }  //услуги
        public int? is_cash { get; set; }

    }
}
