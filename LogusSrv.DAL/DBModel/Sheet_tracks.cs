//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace LogusSrv.DAL.DBModel
{
    using System;
    using System.Collections.Generic;
    
    public partial class Sheet_tracks
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Sheet_tracks()
        {
            this.Refuelings = new HashSet<Refuelings>();
            this.Salary_details = new HashSet<Salary_details>();
            this.Tasks = new HashSet<Tasks>();
            this.TC_to_Sheet_track = new HashSet<TC_to_Sheet_track>();
        }
    
        public int shtr_id { get; set; }
        public int org_org_id { get; set; }
        public Nullable<int> trakt_trakt_id { get; set; }
        public Nullable<int> equi_equi_id { get; set; }
        public int emp_emp_id { get; set; }
        public System.DateTime date_begin { get; set; }
        public System.DateTime date_end { get; set; }
        public Nullable<decimal> left_fuel { get; set; }
        public Nullable<decimal> issued_fuel { get; set; }
        public Nullable<decimal> remain_fuel { get; set; }
        public Nullable<decimal> mileage { get; set; }
        public string num_sheet { get; set; }
        public decimal status { get; set; }
        public int user_user_id { get; set; }
        public Nullable<int> dtd_dtd_id { get; set; }
        public Nullable<int> shtr_type_id { get; set; }
        public bool danger_cargo { get; set; }
        public Nullable<System.DateTime> date_begin_fact { get; set; }
        public Nullable<System.DateTime> date_end_fact { get; set; }
        public Nullable<decimal> moto_start { get; set; }
        public Nullable<decimal> moto_end { get; set; }
        public Nullable<int> exported { get; set; }
        public Nullable<bool> is_repair { get; set; }
        public Nullable<int> id_type_fuel { get; set; }
        public Nullable<decimal> fuel_price { get; set; }
        public Nullable<int> id_agroreq { get; set; }
        public virtual Agrotech_requirment Agrotech_requirment { get; set; }
        public virtual Day_task_detail Day_task_detail { get; set; }
        public virtual Employees Employees { get; set; }
        public virtual Equipments Equipments { get; set; }
        public virtual Organizations Organizations { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Refuelings> Refuelings { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Salary_details> Salary_details { get; set; }
        public virtual Type_fuel Type_fuel { get; set; }
        public virtual Traktors Traktors { get; set; }
        public virtual Users Users { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Tasks> Tasks { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TC_to_Sheet_track> TC_to_Sheet_track { get; set; }
        public virtual Type_sheet_tracks Type_sheet_tracks { get; set; }
    }
}
