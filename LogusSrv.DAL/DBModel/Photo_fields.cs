﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogusSrv.DAL.DBModel
{
    public partial class Photo_fields
    {
        public Photo_fields()
        { }

        public int photo_id { get; set; }
        public int fiel_fiel_id { get; set; }
        public string note { get; set; }
        public string marker { get; set; }
        public string path_photo { get; set; }
        public Nullable<DateTime> date { get; set; }
    }
}
