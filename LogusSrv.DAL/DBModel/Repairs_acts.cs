﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogusSrv.DAL.DBModel
{
    public partial class Repairs_acts
    {
        public int rep_id { get; set; }
        public string number_act { get; set; }
        public DateTime date_start { get; set; }
        public Nullable<DateTime> date_end { get; set; }
        public Nullable<DateTime> date_open { get; set; }
        public string comment { get; set; }
        public int? emp_emp_id { get; set; }
        public int? trakt_trakt_id { get; set; }
        public Single? mileage { get; set; }
        public Single? moto { get; set; }
        public int? mod_mod_id { get; set; }
        public int? equi_equi_id { get; set; }
        public int? type_repairs { get; set; }
        public int? status { get; set; }
        public int? type_rep_parts { get; set; }
        public int? is_daily { get; set; }
        public int? is_require { get; set; }
        public int? is_admin_dates { get; set; }
    }
}
