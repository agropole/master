﻿
namespace LogusSrv.DAL.DBModel
{
    using System;
    using System.Collections.Generic;

    public partial class Day_repairs
    {
       [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Day_repairs()
        {
        }

        public int dr_id { get; set; }
        public DateTime dr_date { get; set; }
        public int? status { get; set; }
        public int? dr_type { get; set; }
        public string discription { get; set; }
        public int? org_org_id { get; set; }

    }
}
