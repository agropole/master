﻿namespace LogusSrv.DAL.DBModel
{
    using System;
    using System.Collections.Generic;

    public partial class TC_param_value_temp
    {
        public int id { get; set; }
        public int id_temp { get; set; }
        public int id_tc_param_temp { get; set; }
        public Nullable<int> id_tc_operation_temp { get; set; }
        public Nullable<float> value { get; set; }
        public int plan { get; set; }
        public Nullable<int> mat_mat_id { get; set; }

        public virtual Materials Materials { get; set; }
        public virtual TC_temp TC_temp { get; set; }
        public virtual TC_operation_temp TC_operation_temp { get; set; }
        //public virtual TC_param TC_param { get; set; }
    }
}
