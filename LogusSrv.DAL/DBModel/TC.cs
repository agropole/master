//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace LogusSrv.DAL.DBModel
{
    using System;
    using System.Collections.Generic;
    
    public partial class TC
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public TC()
        {
            this.KPI_value = new HashSet<KPI_value>();
            this.Tasks = new HashSet<Tasks>();
            this.TC_operation = new HashSet<TC_operation>();
            this.TC_param_value = new HashSet<TC_param_value>();
            this.TC_to_service = new HashSet<TC_to_service>();
            this.TC_to_Sheet_track = new HashSet<TC_to_Sheet_track>();
            this.TC1 = new HashSet<TC>();
        }
    
        public int id { get; set; }
        public Nullable<int> tpsort_tpsort_id { get; set; }
        public Nullable<int> fiel_fiel_id { get; set; }
        public Nullable<int> plant_plant_id { get; set; }
        public Nullable<int> id_tc { get; set; }
        public Nullable<int> year { get; set; }
        public DateTime changing_date { get; set; }
    
        public virtual Fields Fields { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<KPI_value> KPI_value { get; set; }
        public virtual Plants Plants { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Tasks> Tasks { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TC_operation> TC_operation { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TC_param_value> TC_param_value { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TC_to_service> TC_to_service { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TC_to_Sheet_track> TC_to_Sheet_track { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TC> TC1 { get; set; }
        public virtual TC TC2 { get; set; }
        public virtual Type_sorts_plants Type_sorts_plants { get; set; }
    }
}
