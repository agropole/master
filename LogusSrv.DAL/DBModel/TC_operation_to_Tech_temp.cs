﻿namespace LogusSrv.DAL.DBModel
{
    using System;
    using System.Collections.Generic;

    public partial class TC_operation_to_Tech_temp
    {
        public int id { get; set; }
        public int it_tc_operation_temp { get; set; }
        public Nullable<int> equi_equi_id { get; set; }
        public Nullable<int> trakt_trakt_id { get; set; }

        public virtual Equipments Equipments { get; set; }
        public virtual TC_operation_temp TC_operation_temp { get; set; }
        public virtual Traktors Traktors { get; set; }
    }
}
