﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogusSrv.DAL.DBModel
{
    public partial class Selected_Plants
    {
        public int sel_id { get; set; }
        public int? plant_plant_id { get; set; }
        public int? year { get; set; }
        public int? type { get; set; }
    }
}
