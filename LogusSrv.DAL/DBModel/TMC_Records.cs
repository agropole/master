﻿using System;
namespace LogusSrv.DAL.DBModel
{
    using System;
    using System.Collections.Generic;

    public partial class TMC_Records
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public TMC_Records()
        {
 
        }

        public int tmr_id { get; set; }
        public int? tma_tma_id { get; set; }
        public int? mat_mat_id { get; set; }
        public int cont_cont_id { get; set; } //==OrgId
        public int? emp_from_id { get; set; }
        public int emp_to_id { get; set; }
        public int? fielplan_fielplan_id { get; set; }
        public int? act_number { get; set; }
        public DateTime act_date { get; set; }
        public Single count { get; set; }
        public Single price { get; set; }
        public Single? nds { get; set; }
        public Single area { get; set; }
        public bool deleted { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual Materials Materials { get; set; }
        public virtual Employees Employees { get; set; }
        public virtual Employees Employees1 { get; set; }
        public virtual Organizations Organizations { get; set; }
        public virtual TMC_Acts TMC_Acts { get; set; }
        public virtual Fields_to_plants Fields_to_plants { get; set; }

    }
}