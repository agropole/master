﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogusSrv.DAL.DBModel
{
    public partial class Repairs_Service
    {
        public int repserv_id { get; set; }
        public int rep_rep_id { get; set; }
        public string name_service { get; set; }
        public Nullable<Single> count { get; set; }
        public Nullable<Single> price { get; set; }
        public Nullable<Single> nds { get; set; }
    }
}
