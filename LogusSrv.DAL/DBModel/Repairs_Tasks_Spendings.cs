﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogusSrv.DAL.DBModel
{
    public partial class Repairs_Tasks_Spendings
    {
        public int sp_id { get; set; }
        public int dai_dai_id { get; set; }
        public DateTime? date1 { get; set; }
        public int? emp_emp_id { get; set; }
        public int? tptas_tptas_id { get; set; }
        public decimal? rate_shift { get; set; }
        public decimal? rate_piecework { get; set; }
        public int? hours { get; set; }
    }
}
