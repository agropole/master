﻿using System;
namespace LogusSrv.DAL.DBModel
{
    using System;
    using System.Collections.Generic;

    public partial class TMC_Acts
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public TMC_Acts()
        {
            this.TMC_Records = new HashSet<TMC_Records>();
        }
        public int tma_id { get; set; }
        public int? fielplan_fielplan_id { get; set; }  //org_id
        public DateTime? act_date { get; set; }
        public string act_num { get; set; }
        public string comment { get; set; }
        public int? emp_emp_id { get; set; }
        public int? type_act { get; set; }
        public int? torg_id { get; set; }
        public int? contr_id { get; set; }
        public int? soil_soil_id { get; set; }
        public int? emp_utv_id { get; set; }
        public Nullable<decimal> year { get; set; }
        public virtual ICollection<TMC_Records> TMC_Records { get; set; }
        public virtual ICollection<Materials_to_ftp> Materials_to_ftp { get; set; }
    }
}