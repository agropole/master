//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace LogusSrv.DAL.DBModel
{
    using System;
    using System.Collections.Generic;
    
    public partial class Indicators_to_tasks
    {
        public int id { get; set; }
        public int ind_ind_id { get; set; }
        public int tas_tas_id { get; set; }
        public string value { get; set; }
        public System.DateTime date { get; set; }
        public System.Data.Entity.Spatial.DbGeography coord { get; set; }
    
        public virtual Indicators Indicators { get; set; }
        public virtual Tasks Tasks { get; set; }
    }
}
