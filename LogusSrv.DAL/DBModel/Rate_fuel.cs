﻿using System;
namespace LogusSrv.DAL.DBModel
{
    using System;
    using System.Collections.Generic;

    public partial class Rate_fuel
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Rate_fuel()
        {
        }
        public int id { get; set; }
        public int trakt_trakt_id { get; set; }
        public Single? summer_track { get; set; }
        public Single? winter_track { get; set; }
        public Single? summer_city { get; set; }
        public Single? winter_city { get; set; }
        public Single? summer_field { get; set; }
        public Single? winter_field { get; set; }
        public Single? coef { get; set; }
        public Single? body_lifting { get; set; }
        public Single? engine_heating { get; set; }
        public Single? heating { get; set; }
        public Single? icebox { get; set; }
        public Single? setting { get; set; }
        public Single? icebox_per { get; set; }
        public Single? trailer { get; set; }
        public Single? sink { get; set; }
        public Single? refill { get; set; }

    
    }
}