﻿using System;
namespace LogusSrv.DAL.DBModel
{
    using System;
    using System.Collections.Generic;

    public partial class Roles_subsystems
    {
        public Roles_subsystems()
        {
        }
        public int rss_id { get; set; }
        public int? role_role_id { get; set; }
        public string subsystem_id { get; set; }

    }
}