﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogusSrv.DAL.DBModel
{
    public partial class Daily_acts
    {
        public int dai_id { get; set; }
        public string number_dai_act { get; set; }
        public int rep_id { get; set; }
        public DateTime date1 { get; set; }
        public string time_start { get; set; }
        public string time_end { get; set; }
        public int? status { get; set; }
        public int? general_works { get; set; }
        public int? equi_equi_id { get; set; }
    }
}
