﻿using System;
namespace LogusSrv.DAL.DBModel
{
    using System;
    using System.Collections.Generic;

    public partial class Spare_parts_Records
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Spare_parts_Records()
        {

        }

       public int sps_id { get; set; }
       public DateTime date { get; set; }
       public Single count { get; set; }
       public Single price { get; set; }
       public Nullable<Single> nds { get; set; }
       public int? spss_spss_id { get; set; }
       public int spare_spare_id { get; set; }
       public int? store_from_id { get; set; }
       public int store_to_id { get; set; }
       public int? act_number { get; set; }
    }
}
