﻿namespace LogusSrv.DAL.DBModel
{
    using System;
    using System.Collections.Generic;

    public partial class TC_to_service_temp
    {
        public int id { get; set; }
        public int id_temp { get; set; }
        public int id_service { get; set; }
        public float cost { get; set; }

        public virtual Service Service { get; set; }
        public virtual TC_temp TC_temp { get; set; }
    }
}
