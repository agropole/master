﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogusSrv.DAL.DBModel
{
    public partial class Repairs_Records
    {
        public int repair_id { get; set; }
        public int? spare_spare_id { get; set; }
        public int? store_store_id { get; set; }
        public int? rep_rep_id { get; set; }
        public float? count { get; set; }
        public DateTime? date { get; set; }
    }
}
