﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using LogusSrv.DAL.Enums;
using LogusSrv.DAL.Entities.Req;
using LogusSrv.DAL.Utils;

namespace LogusSrv.DAL.Utils
{
    public class GridHelpers<T>
    {
        public static IQueryable<T> GetGridData(IQueryable<T> source, GridDataReq request)
        {

            if (request.SortField != null)
            {
                source = OrderBy(source, request.SortField, request.SortOrder);
            }
            source = GetPagingData(source, request.PageNum, request.PageSize);

            return source;
        }

        /// <summary>
        /// Сортировка данных по полю
        /// </summary>
        /// <param name="source"> Запрос </param>
        /// <param name="ordering"> Номер колонки для сортировки </param>
        /// <param name="sortDirections"> Направление сортировки </param>
        /// <returns></returns>
        public static IQueryable<T> OrderBy(IQueryable<T> source, string ordering, SortDirections sortDirections)
        {
            var type = typeof(T);
            var property = type.GetProperty(ordering);
            var parameter = Expression.Parameter(type, "p");
            MemberExpression propertyAccess = null;
            try
            {
                propertyAccess = Expression.MakeMemberAccess(parameter, property);
            }
            catch (Exception ex)
            {
              throw ex;
            }
            var orderByExp = Expression.Lambda(propertyAccess, parameter);

            MethodCallExpression resultExp = Expression.Call(typeof(Queryable), sortDirections.ToString(), new Type[] { type, property.PropertyType }, source.Expression, Expression.Quote(orderByExp));
            return source.Provider.CreateQuery<T>(resultExp);
        }

       
        public static IQueryable<T> GetPagingData(IQueryable<T> source, int PageNum, int PageSize)
        {

            return source.Skip((PageNum - 1) * PageSize).Take(PageSize);
        }
    }
}
