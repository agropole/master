﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LogusSrv.DAL.DBModel;
using System.Data.Entity.Spatial;
using Newtonsoft.Json.Linq;

namespace LogusSrv.DAL.Utils
{
    public class CommonFunction
    {
        public static int GetNextId(LogusDBEntities db, string table)
        {
            return (int)db.Database.SqlQuery<Decimal>("SELECT IDENT_CURRENT( '" + table + "' )").FirstOrDefault() + 1;
        }

        public static JArray ParseCoord(DbGeography geographyValue)
        {

            BinaryReader lebr;
            BinaryReader bebr;
            int coordinateSystemId;
            var br = new BinaryReader(new MemoryStream(geographyValue.AsBinary()));
            lebr = BitConverter.IsLittleEndian ? br : new ReverseEndianBinaryReader(br.BaseStream);
            bebr = BitConverter.IsLittleEndian ? new ReverseEndianBinaryReader(br.BaseStream) : br;
            coordinateSystemId = geographyValue.CoordinateSystemId;
            var jsonObject =  DbGeographyGeoJsonConverter.WriteObject(lebr, bebr);
            return jsonObject;
           
        }
    }

    public class ReverseEndianBinaryReader : BinaryReader
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ReverseEndianBinaryReader"/> class.
        /// </summary>
        /// <param name="stream">
        /// The stream.
        /// </param>
        public ReverseEndianBinaryReader(Stream stream)
            : base(stream)
        {
        }
    }
}
