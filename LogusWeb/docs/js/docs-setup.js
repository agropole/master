NG_DOCS={
  "sections": {
    "api": "API Documentation"
  },
  "pages": [
    {
      "section": "api",
      "id": "app.controller:CatalogController",
      "shortName": "CatalogController",
      "type": "controller",
      "moduleName": "app",
      "shortDescription": "Справочник культур. Не используется",
      "keywords": "api app controller"
    },
    {
      "section": "api",
      "id": "app.controller:DictionaryCtrl",
      "shortName": "DictionaryCtrl",
      "type": "controller",
      "moduleName": "app",
      "shortDescription": "DictionaryCtrl",
      "keywords": "$cookiestore $http $location $rootscope $scope $stateparams addrowforupdate api app app-controller-dictionaryctrl-page app-controller-page cancel class controller create delrow dictionary dictionaryctrl editcoord function loadnewdic map modals modaltype preloader requests savedicchanges search selectdictionary submitform update updateselectedcell"
    },
    {
      "section": "api",
      "id": "app.controller:DoubleListCtrl",
      "shortName": "DoubleListCtrl",
      "type": "controller",
      "moduleName": "app",
      "shortDescription": "DoubleListCtrl",
      "keywords": "$scope api app app-controller-doublelistctrl-page app-controller-page class controller deletekpi delkpifromlist doublelist doublelistctrl evaluate gridoptions init recounttotal sc scope selectkpi"
    },
    {
      "section": "api",
      "id": "app.controller:KPICtrl",
      "shortName": "KPICtrl",
      "type": "controller",
      "moduleName": "app",
      "shortDescription": "IncomingListCtrl",
      "keywords": "$cookiestore $http $location $rootscope $scope afterselectionchange api app app-controller-kpictrl-page app-controller-page beforeselectionchange callback cell changecountprice class column comment commentclick config console contractor controller cost count createtask datechange dateincoming delrow displayname editcell event fuel function getdayincoming headers incomingfuellist incominglistctrl info linenums modals modaltype newincome null postincome prettyprint price requests row sc selectedval status type_salary_doc updateselectedcell var vat"
    },
    {
      "section": "api",
      "id": "app.controller:LeftPanelCtrl",
      "shortName": "LeftPanelCtrl",
      "type": "controller",
      "moduleName": "app",
      "shortDescription": "LeftPanelCtrl",
      "keywords": "$cookiestore $location $scope api app app-controller-leftpanelctrl-page app-controller-page class clickrole controller cultid fileinputchanged leftpanelctrl logusconfig preparefile sc scope searchdata sendtoserver"
    },
	{
      "section": "api",
      "id": "app.controller:RightPanelCtrl",
      "shortName": "RightPanelCtrl",
      "type": "controller",
      "moduleName": "app",
      "shortDescription": "RightPanelCtrl",
      "keywords": "$cookiestore $location $scope api app app-controller-rightpanelctrl-page app-controller-page class clickrole controller cultid fileinputchanged rightpanelctrl logusconfig preparefile sc scope searchdata sendtoserver"
    },
    {
      "section": "api",
      "id": "app.controller:LoginController",
      "shortName": "LoginController",
      "type": "controller",
      "moduleName": "app",
      "shortDescription": "LoginController",
      "keywords": "$cookiestore $location $rootscope $scope api app app-controller-logincontroller-page app-controller-page authenticationservice class controller login logincontroller"
    },
    {
      "section": "api",
      "id": "app.controller:MainCtrl",
      "shortName": "MainCtrl",
      "type": "controller",
      "moduleName": "app",
      "shortDescription": "MainCtrl",
      "keywords": "$cookiestore $location $scope api app app-controller-mainctrl-page app-controller-page class clickrole controller fileinputchanged logusconfig mainctrl preparefile sc scope sendtoserver settingsupload username"
    },
    {
      "section": "api",
      "id": "app.controller:RatesCtrl",
      "shortName": "RatesCtrl",
      "type": "controller",
      "moduleName": "app",
      "shortDescription": "RatesCtrl",
      "keywords": "$http $scope addforsave addrate api app app-controller-page app-controller-ratesctrl-page cancel changeprice class controller delrow infodata load modals modaltype openselect ratesctrl requests save saveinfo sc scope selectdialogname"
    },
    {
      "section": "api",
      "id": "app.controller:ReportsCtrl",
      "shortName": "ReportsCtrl",
      "type": "controller",
      "moduleName": "app",
      "shortDescription": "ReportsCtrl",
      "keywords": "$http $scope activetab api app app-controller-page app-controller-reportsctrl-page class controller currentyear formdata infodata logusconfig modals modaltype monthlist reportsctrl requests sc scope settab submitform traktorchange yearslist"
    },
    {
      "section": "api",
      "id": "app.factory:AuthenticationService",
      "shortName": "AuthenticationService",
      "type": "service",
      "moduleName": "app",
      "shortDescription": "Сервис авторизации",
      "keywords": "$cookiestore $http $rootscope $timeout api app base64 factory requests service"
    },
    {
      "section": "api",
      "id": "app.factory:Base64",
      "shortName": "Base64",
      "type": "service",
      "moduleName": "app",
      "shortDescription": "Кодировани и декодирование Base64для пароля",
      "keywords": "api app base64 factory service"
    },
    {
      "section": "api",
      "id": "app.factory:map",
      "shortName": "map",
      "type": "service",
      "moduleName": "app",
      "shortDescription": "map",
      "keywords": "$http $requests addinteraction addonoverlay allowsingledraw api app app-factory-map-page app-factory-page areainput callback- class clearall clearfield clearmap clearoverlay createfieldstyle factory featureoverlay fieldslayer formatarea getarea getcoord getfield getrgb hex initmap insertfieldcb map modals modaltype oldarea raster refreshfield rgb service setareainput setinsertfieldcb"
    },
    {
      "section": "api",
      "id": "app.factory:preloader",
      "shortName": "preloader",
      "type": "service",
      "moduleName": "app",
      "shortDescription": "map",
      "keywords": "$http api app factory hide map preloader service"
    },
    {
      "section": "api",
      "id": "app.factory:requests",
      "shortName": "requests",
      "type": "service",
      "moduleName": "app",
      "shortDescription": "map",
      "keywords": "$http api app factory logusconfig map modals modaltype post preloader servercall service"
    },
    {
      "section": "api",
      "id": "Documents.controller:DocumentsItemCtrl",
      "shortName": "DocumentsItemCtrl",
      "type": "controller",
      "moduleName": "Documents",
      "shortDescription": "DocumentsItemCtrl",
      "keywords": "$cookiestore $http $location $scope $stateparams api cancelclick class controller creatework data datechange deletework documents documents-controller-documentsitemctrl-page documents-controller-page documentsitemctrl editcell editdropcell formdata gridevdatalistener gridoptions item logusconfig modals modaltype myselections printbtn requests savework sc scope selectrowbyfield submitform tabledata typeoperation updateselectedcell"
    },
    {
      "section": "api",
      "id": "Documents.controller:DocumentsListCtrl",
      "shortName": "DocumentsListCtrl",
      "type": "controller",
      "moduleName": "Documents",
      "shortDescription": "DocumentsListCtrl",
      "keywords": "$cookiestore $http $location $scope $stateparams api class controller copybtnclick createbtnclick curuser data datechangedatedocuments deletebtnclick documents documents-controller-documentslistctrl-page documents-controller-page documentslistctrl editbtnclick getdocuments gridevdatalistener gridoptions modals modaltype myselections needselection nggrideventsorted pagingoptions requests sc scope sortoptions tabledata totalserveritems"
    },
    {
      "section": "api",
      "id": "DriverWayBill.controller:CloseDriverWayBill",
      "shortName": "CloseDriverWayBill",
      "type": "controller",
      "moduleName": "DriverWayBill",
      "shortDescription": "DriverWayBillItem",
      "keywords": "$cookiestore $http $location $scope $stateparams addtask api cancelclick cancelrefuelingmodal class clearclick clonetask closerefuelingmodal closewaybill controller curuser driverwaybill driverwaybill-controller-closedriverwaybill-page driverwaybill-controller-page driverwaybillitem edittask formdata infodata modals modaltype needselection refuelingclick refuilingsdata refuilingsformdata reloadclosedriverwaybilldata removetask requests sc scope setrefuilingsdatabyordernum submitform uigridoptions way_bill_id"
    },
    {
      "section": "api",
      "id": "DriverWayBill.controller:CloseDriverWayBillTaskCtrl",
      "shortName": "CloseDriverWayBillTaskCtrl",
      "type": "controller",
      "moduleName": "DriverWayBill",
      "shortDescription": "CloseDriverWayBillTaskCtrl",
      "keywords": "$cookiestore $http $location $scope $stateparams addcomment api cancelclick changeconsumptionfact class closedriverwaybilltaskctrl controller cookiedate curuser driverwaybill driverwaybill-controller-closedriverwaybilltaskctrl-page driverwaybill-controller-page formdata gettaskformdata gridoptions infodata modals modaltype refreshlayer requests rootscope sc scope selectedrow submitform task_id taskscolumndefinitions way_bill_id"
    },
    {
      "section": "api",
      "id": "DriverWayBill.controller:DriverWayBillItem",
      "shortName": "DriverWayBillItem",
      "type": "controller",
      "moduleName": "DriverWayBill",
      "shortDescription": "DriverWayBillItem",
      "keywords": "$cookiestore $http $location $scope $stateparams addtask api cancelwaybillclick class controller cookiedate curuser driverwaybill driverwaybill-controller-driverwaybillitem-page driverwaybill-controller-page driverwaybillitem editcell formdata gridoptions infodata itemid modals modaltype removetask requests rootscope sc scope selectedrow shifttaskid submitform taskscolumndefinitions traktorchange updateselectedcell"
    },
    {
      "section": "api",
      "id": "Monitor.contorller:CostCtrl",
      "shortName": "CostCtrl",
      "type": "controller",
      "moduleName": "Monitor",
      "shortDescription": "CostCtrl",
      "keywords": "$cookiestore $http $location $rootscope $scope api class contorller controller costctrl initmainpie loadplanfact loadreport logusconfig monitor monitor-contorller-costctrl-page monitor-contorller-page refreshcharts report requests sc scope setmainpiedata showplanfact"
    },
    {
      "section": "api",
      "id": "Monitor.contorller:CultureCtrl",
      "shortName": "CultureCtrl",
      "type": "controller",
      "moduleName": "Monitor",
      "shortDescription": "CultureCtrl",
      "keywords": "$http $scope api calcinvest class contorller controller culturectrl loadcosts monitor monitor-contorller-culturectrl-page monitor-contorller-page recounttable requests sc scope settabledata showplanfact"
    },
    {
      "section": "api",
      "id": "Monitor.contorller:MapCtrl",
      "shortName": "MapCtrl",
      "type": "controller",
      "moduleName": "Monitor",
      "shortDescription": "MapCtrl",
      "keywords": "$http $scope api class contorller controller createtech createworks feature infodata layersclick listfilter logusconfig mapctrl monitor monitor-contorller-mapctrl-page monitor-contorller-page openinfo requests sc scope searchclick statustranslate"
    },
    {
      "section": "api",
      "id": "Monitor.contorller:SchemeCtrl",
      "shortName": "SchemeCtrl",
      "type": "controller",
      "moduleName": "Monitor",
      "shortDescription": "SchemeCtrl",
      "keywords": "$scope api class contorller controller monitor monitor-contorller-page monitor-contorller-schemectrl-page sc schemectrl scope"
    },
    {
      "section": "api",
      "id": "Product.contorller:GrossCtrl",
      "shortName": "GrossCtrl",
      "type": "controller",
      "moduleName": "Product",
      "shortDescription": "GrossCtrl",
      "keywords": "$cookiestore $http $location $scope $stateparams add api class contorller controller culturechange getnewrows gridoptions grossctrl loadgross loadinfo modals modaltype product product-contorller-grossctrl-page product-contorller-page requests save saveerror saverows sc scope updateselectedcell"
    },
    {
      "section": "api",
      "id": "Product.contorller:ProductCtrl",
      "shortName": "ProductCtrl",
      "type": "controller",
      "moduleName": "Product",
      "shortDescription": "ProductCtrl",
      "keywords": "$http $location $scope api changeprice class contorller controller gridoptions infodata loadproductlist opengrossharvest opensales product product-contorller-page product-contorller-productctrl-page productctrl requests sc scope selectedrow updateselectedcell"
    },
    {
      "section": "api",
      "id": "Product.contorller:SalesCtrl",
      "shortName": "SalesCtrl",
      "type": "controller",
      "moduleName": "Product",
      "shortDescription": "SalesCtrl",
      "keywords": "$http $location $scope add api changeprice class contorller controller getnewrows gotoyear gridoptions infodata loadsales product product-contorller-page product-contorller-salesctrl-page refreshyearsales requests salesctrl save saveerror saverows sc scope selectedrow updateselectedcell"
    },
    {
      "section": "api",
      "id": "Seeding.contorller:SeedingItemCtrl",
      "shortName": "SeedingItemCtrl",
      "type": "controller",
      "moduleName": "Seeding",
      "shortDescription": "SeedingItemCtrl",
      "keywords": "$http $location $scope $stateparams addplant addsort api cancelclick class closefield closeflag contorller controller data finishfield firstload formdata getinfosedding gridevdatalistener gridoptionsplant gridoptionssort html modals modaltype myselectionsplant myselectionssort ratecellclick ratecelltemplate refreshtable removeitem requests saveform sc scope seeding seeding-contorller-page seeding-contorller-seedingitemctrl-page seedingitemctrl selectedplantid selectrowbyfield submitform tabledataplant tabledatasort template updateselectedcell"
    },
    {
      "section": "api",
      "id": "Seeding.contorller:SeedingListCtrl",
      "shortName": "SeedingListCtrl",
      "type": "controller",
      "moduleName": "Seeding",
      "shortDescription": "SeedingListCtrl",
      "keywords": "$cookiestore $http $location $scope $stateparams api class contorller controller currentplantid currentyearid editseedingitem fieldcelltemplate firstload formdata getplantstyle getseeding gridoptions gridoptionsplant gridoptionssort header headerdistemplate html modals modaltype mycolumndefs myselection myselections myselectionsplant myselectionssort pagingoptions refreshtable requests sc scope seeding seeding-contorller-page seeding-contorller-seedinglistctrl-page seedinglistctrl selectedplantid showmessage sortoptions tabledata tabledataplant tabledatasort template totalserveritems yearscellclick yearscelltemplate"
    },
    {
      "section": "api",
      "id": "ShiftTask.contorller:CreateShiftTaskCtrl",
      "shortName": "CreateShiftTaskCtrl",
      "type": "controller",
      "moduleName": "ShiftTask",
      "shortDescription": "CreateShiftTaskCtrl",
      "keywords": "$cookiestore $http $location $rootscope $scope $stateparams activetab api approvetask checktab class columndefinitions comment compare contorller controller copytaskrow copytasksfromyesterday createshifttaskctrl createtask createwaybill createwaybtnvis curuser data dataemployees datatraktors datechangeshifts daytaskid deleatetasks deletetask discreatewaybill editbtnvis editcell excel getclasscopycell getdaytasks getprintinfo gridevdatalistener gridfirstoptions gridsecondoptions keydowngrid keyupgrid maincommentclick modals modaltype multi needselection preloader printshiftinfo1 printshiftinfo2 printtask requests sc scope selectedrow selectfirstrow selectrowbyfield sendtask shifts shifttask shifttask-contorller-createshifttaskctrl-page shifttask-contorller-page tabledatafirst tabledatasecond updateselectedcell"
    },
    {
      "section": "api",
      "id": "ShiftTask.contorller:CreateTaskCtrl",
      "shortName": "CreateTaskCtrl",
      "type": "controller",
      "moduleName": "ShiftTask",
      "shortDescription": "CreateTaskCtrl",
      "keywords": "api contorller controller createtaskctrl shifttask"
    },
    {
      "section": "api",
      "id": "TC.controller:TCListCtrl",
      "shortName": "TCListCtrl",
      "type": "controller",
      "moduleName": "TC",
      "shortDescription": "TCListCtrl",
      "keywords": "$http $location $scope api class controller createtc delrow edittc gridoptions infodata loadtc logusconfig modals modaltype opentcreport opentotalreport refreshyear requests sc scope selectedrow tc tc-controller-page tc-controller-tclistctrl-page tclistctrl yearid"
    },
    {
      "section": "api",
      "id": "TC.controller:Techcard",
      "shortName": "Techcard",
      "type": "controller",
      "moduleName": "TC",
      "shortDescription": "Techcard",
      "keywords": "$http $location $scope api cancel changemaindata changenorm changework changeworkdata changeworkload class cleartcform commentclick controller countnzp createnewfertilizer createnewmaterial createnewseed createnewservices createnewsupportstaff createnewszr createtask datachemicalfertilizers datanzp dataseed dataservices datasupportstuff dataszr datatotal dataworks datechange delrowservice delrowsupportstaff delrowszr delrowwork editcell fieldid formdata getflagname getindex getmaterialrowindex getrowwork getstuffrowindex getvalueforinput gridoptions_chemicalfertilizers gridoptions_emp gridoptions_nzp gridoptions_seed gridoptions_services gridoptions_supportstuff gridoptions_szr gridoptions_tech gridoptions_works gridtotal grossharvest loadtcbyid logusconfig modals modaltype plantid productivity recountdata recountfuel recountrow recountrowmaterial recountrowsupportstaff recountservice recounttotal recounttotalone refreshfield refreshyear requests sc scope setcommongridoption sortid submitform tc tc-controller-page tc-controller-techcard-page techcard typesdrop updateselectedcell updatetype yearid"
    },
    {
      "section": "api",
      "id": "Warehouse.controlle:IncomingFuelListCtrl",
      "shortName": "IncomingFuelListCtrl",
      "type": "controller",
      "moduleName": "Warehouse",
      "shortDescription": "IncomingFuelListCtrl",
      "keywords": "$cookiestore $http $location $rootscope $scope afterselectionchange api beforeselectionchange callback cell changecountprice class column comment commentclick console contractor controlle controller cost count createtask datechange dateincoming delrow displayname editcell event fuel function getdayincoming incomingfuellist incomingfuellistctrl info linenums modals modaltype newincome null postincome prettyprint price requests row sc selectedval type_salary_doc updateselectedcell var vat warehouse warehouse-controlle-incomingfuellistctrl-page warehouse-controlle-page"
    },
    {
      "section": "api",
      "id": "Warehouse.controlle:IncomingListCtrl",
      "shortName": "IncomingListCtrl",
      "type": "controller",
      "moduleName": "Warehouse",
      "shortDescription": "IncomingListCtrl",
      "keywords": "$cookiestore $http $location $rootscope $scope afterselectionchange api beforeselectionchange callback cell changecountprice class column comment commentclick console contractor controlle controller cost count createtask datechange dateincoming delrow displayname editcell event fuel function getdayincoming incominglist incominglistctrl info linenums modals modaltype newincome null postincome prettyprint price requests row sc selectedval type_salary_doc updateselectedcell var vat warehouse warehouse-controlle-incominglistctrl-page warehouse-controlle-page"
    },
    {
      "section": "api",
      "id": "WayList.controller:CloseEditTaskCtrl",
      "shortName": "CloseEditTaskCtrl",
      "type": "controller",
      "moduleName": "WayList",
      "shortDescription": "CloseEditTaskCtrl",
      "keywords": "$cookiestore $http $location $scope $stateparams addcomment api callback cancelclick changeconsumptionfact changefield class closeedittaskctrl controller countarea curuser formdata getinfocallback initmapedit logusconfig map modals modaltype refreshlayer refreshrate requests sc scope submitform type waylist waylist-controller-closeedittaskctrl-page waylist-controller-page"
    },
    {
      "section": "api",
      "id": "WayList.controller:CloseWayBillCtrl",
      "shortName": "CloseWayBillCtrl",
      "type": "controller",
      "moduleName": "WayList",
      "shortDescription": "CloseWayBillCtrl",
      "keywords": "$cookiestore $http $location $scope $stateparams addtask api cancelclick cancelrefuelingmodal class clearclick clonetask closerefuelingmodal closewaybillctrl closewaylist controller curuser edittask formdata modals modaltype needselection refuelingclick refuilingsdata refuilingsformdata removetask requests sc scope selectedrow setrefuilingsdatabyordernum submitform uigridoptions waylist waylist-controller-closewaybillctrl-page waylist-controller-page"
    },
    {
      "section": "api",
      "id": "WayList.controller:WayBillEditCtrl",
      "shortName": "WayBillEditCtrl",
      "type": "controller",
      "moduleName": "WayList",
      "shortDescription": "WayBillEditCtrl",
      "keywords": "$cookiestore $http $location $scope $stateparams api cancelwaybillclick checkuserrole class controller curuser formdata initmap loadinfo requests sc scope setrepair submitcallback submitform submitpath traktorchange waybilleditctrl waylist waylist-controller-page waylist-controller-waybilleditctrl-page"
    },
    {
      "section": "api",
      "id": "WayList.controller:WayBillsIssuanceCtrl",
      "shortName": "WayBillsIssuanceCtrl",
      "type": "controller",
      "moduleName": "WayList",
      "shortDescription": "WayBillsIssuanceCtrl",
      "keywords": "$cookiestore $http $location $scope $stateparams api class closebtnclick controller createbtnclick curuser datechangewaybills deletebtnclick editbtnclick getwaybills gridoptions issuebtnclick modals modaltype myselections needselection requests sc scope selectrowbyfield tabledata waybillsissuancectrl waylist waylist-controller-page waylist-controller-waybillsissuancectrl-page"
    }
  ],
  "apis": {
    "api": true
  },
  "html5Mode": false,
  "editExample": true,
  "startPage": "/api",
  "scripts": [
    "angular.min.js"
  ]
};