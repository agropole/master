function initStack(cont_id, d, title) {
    $(cont_id).ejChart({
        titleRendering : "ontitlerender",
        pointRegionClick : "onpointclick",
        pointRegionMouseMove : "onpointmousemove",
        legendItemMouseMove : "onlegendmousemove",
        legendItemClick : "onlegendclicked",
        load : "loadTheme",
        primaryXAxis : {
            axisLine : {
                visible : true
            },
            font : {
                fontFamily : "DINPro"
            }
        },
        primaryYAxis : {
            visible : true,
            axisLine : {
                visible : true
            },
            font : {
                fontFamily : "DINPro"
            },
        },
        commonSeriesOptions:
        {
            labelPosition: 'outside',
            enableAnimation: true,
            tooltip: {
                visible: true,
                format: "#point.x# : #point.y#",
                font : {
                    fontFamily : "DINPro"
                }
            },
            marker:
            {
                dataLabel:
                {
                    shape: 'none',
                    visible: true,
                    textPosition: 'inside',
                    border: { width: 1},
                    connectorLine: { height: 70, stroke:"black" },
                    font : {
                        fontFamily : "DINPro"
                    }
                },
                format: "#point.x# : #point.y#%"
            },
            type: 'stackingcolumn'
        },
        title : {
            text : title,
            font : {
                fontFamily : "DINPro"
            }
        },
        legend : {
            visible : false
        },
        series: d
    });
}
/**
 * Created by ofirtych on 14/08/15.
 */
