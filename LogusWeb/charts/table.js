/**
 * Created by ofirtych on 10/06/15.
 */
function initTable(cont_id, res, template) {
	// executing query
    for (var i= 0,li=res.length; i<li; i++) {
        var info = res[i];
        info.deviation = (100 - Math.round( info.fact / info.plan * 100)) + "%";
    }
    $(cont_id).html($(template).render(res));
}
