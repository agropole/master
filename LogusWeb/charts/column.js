function initColumn() {
	$("#container").ejChart({
		canResize : true,
		primaryXAxis : {
			majorGridLines : {
				visible : false
			},
			labelIntersectAction : 'rotate90',
			font : {
				fontFamily : "DINPro",
				fontSize: 12
			}
		},
		primaryYAxis : {
			axisLine : {
				visible : false
			},
			font : {
				fontFamily : "DINPro"
			},
		},
		commonSeriesOptions:
		{
			type: 'stackingcolumn',
			enableAnimation: true,
			tooltip :
			{
				visible :true ,
				format: " #series.name#  <br/> #point.x# : #point.y# тыс. руб",
				font : {
					fontFamily : "DINPro"
				}
			}
		},
		legend : {
			visible : true,
			font : {
				fontFamily : "DINPro"
			}
		},
		pointRegionClick : 'columnclick'
	});
}

function setDataColumn(data, field) {
	var ps1 = [],
		ps2 = [],
		ps3 = [],
		ps4 = [],
		ps5 = [],
		ps6 = [],
		ps7 = [];
	var s1 = 0, s2= 0, s3= 0, s4= 0, s5=0;s6=0;s7=0;
	for (var i = 0,li = data.length; i < li; i++) {
		if (data[i].plant_id && data[i].plant_id !== 36) {
			ps1.push({
				x : getName(data[i][field]),
				y : data[i].salary_total
			});
			ps2.push({
				x : getName(data[i][field]),
				y : data[i].szr_fact
			});
			ps3.push({
				x : getName(data[i][field]),
				y : data[i].razx_top
			});
			ps4.push({
				x : getName(data[i][field]),
				y : data[i].minud_fact
			});
			ps5.push({
				x : getName(data[i][field]),
				y : data[i].posevmat_fact
			});
			ps6.push({
				x : getName(data[i][field]),
				y : data[i].service_total
			});
			ps7.push({
				x : getName(data[i][field]),
				y : data[i].dop_material_fact
			});
		}
		s1+=data[i].salary_total;
		s3+=data[i].razx_top;
		s2+=data[i].szr_fact;
		s4+=data[i].minud_fact;
		s5+=data[i].posevmat_fact;
		s6+=data[i].service_total;
		s7+=data[i].dop_material_fact;
	};

	$("#container").ejChart("option", {
		"drilldown" : {
			series : [
				{
					points : ps1,
					fill : "#69D2E7",
					name : 'Зар. плата'
				}, {
					points : ps3,
					fill : "#cc4646",
					name : 'Топливо',
					enableAnimation : true
				},{
					points : ps2,
					fill : "#c3c34c",
					name : 'СЗР',
					enableAnimation : true
				},{
					points : ps4,
					fill : "#8d5ab5",
					name : 'Мин. удобрения',
					enableAnimation : true
				},
				{
					points : ps5,
					fill : "#2d5276",
					name : 'Семена',
					enableAnimation : true
				},
				{
					points : ps6,
					fill : "#800000",
					name : 'Услуги',
					enableAnimation : true
				},
					{
					points : ps7,
					fill : "#842e2e",
					name : 'Пр. материалы',
					enableAnimation : true
				}
			]
		}
	});
}

function getName(plant_name) {
	if (plant_name.length > 10) return plant_name.substr(0,10) + '...';
	else return plant_name;
}


function setDataColumnField(data, field) {
	var ps1 = [],
		ps2 = [],
		ps3 = [],
		ps4 = [],
		ps5 = [],
		ps6 = [],
		ps7= [];
		 console.log(data);
	var s1 = 0, s2= 0, s3= 0, s4= 0, s5=0;s6=0;s7=0;
	for (var i = 0,li = data.length; i < li; i++) {
		if (data[i].plant_id !== 36) {
			ps1.push({
				x : getNameField(data[i][field]),
				y : data[i].salary_total
			});
			ps2.push({
				x : getNameField(data[i][field]),
				y : data[i].szr_fact
			});
			ps3.push({
				x : getNameField(data[i][field]),
				y : data[i].razx_top
			});
			ps4.push({
				x : getNameField(data[i][field]),
				y : data[i].minud_fact
			});
			ps5.push({
				x : getNameField(data[i][field]),
				y : data[i].posevmat_fact
			});
				ps6.push({
				x : getNameField(data[i][field]),
				y : data[i].service_total
			});
				ps7.push({
				x : getNameField(data[i][field]),
				y : data[i].dop_material_fact
			});
		}
		s1+=data[i].salary_total;
		s3+=data[i].razx_top;
		s2+=data[i].szr_fact;
		s4+=data[i].minud_fact;
		s5+=data[i].posevmat_fact;
		s6+=data[i].service_total;
		s7+=data[i].dop_material_fact;
	};

	$("#container").ejChart("option", {
		"drilldown" : {
			series : [
				{
					points : ps1,
					fill : "#69D2E7",
					name : 'Зар. плата'
				}, {
					points : ps3,
					fill : "#cc4646",
					name : 'Топливо',
					enableAnimation : true
				},{
					points : ps2,
					fill : "#c3c34c",
					name : 'СЗР',
					enableAnimation : true
				},{
					points : ps4,
					fill : "#8d5ab5",
					name : 'Мин. удобрения',
					enableAnimation : true
				},
				{
					points : ps5,
					fill : "#2d5276",
					name : 'Семена',
					enableAnimation : true
				},
					{
					points : ps6,
					fill : "#800000",
					name : 'Услуги',
					enableAnimation : true
				},
						{
					points : ps7,
					fill : "#842e2e",
					name : 'Пр. материалы',
					enableAnimation : true
				},
			]
		}
	});
}

function getNameField(fields_name) {
	if (fields_name.length > 10) return fields_name.substr(0,10) + '...';
	else return fields_name;
}


