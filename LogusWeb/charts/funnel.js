function initFunnel(cont_id, data, title) {
	for (var i=0,li=data.length;i<li;i++) {
		data[i].text = data[i].y.toString();
	}
	$(cont_id).ejChart({
		//Initializing Series
		series : [{
			enableAnimation : true,
			points : data,
			marker : {
				dataLabel : {
					visible : true,
					shape : 'none',
					font : {
						color : 'white',
						size : '12px',
						fontWeight : 'lighter',
						fontFamily : "DINPro"
					},
					format : "#point.x#: #point.y# ц"
				}
			},
			tooltip : {
				visible : true,
				format : "#point.x#: #point.y# ц <br/>",
				font : {
					fontFamily : "DINPro"
				}
			},
			name : 'WebSite',
			type : 'funnel',
			funnelHeight : "20%",
			funnelWidth : "15%"
		}],
		load : "loadTheme",
		canResize : true,
		title : {
			text : title,
			font : {
				fontFamily : "DINPro"
			}
		},
		legend : {
			visible : false
		}
	});
}