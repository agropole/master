/**
 * Created by ofirtych on 15/07/15.
 */

function initPieDetail() {
    $("#piedetail").ejChart({
        //Initializing Common Properties for all the series
        commonSeriesOptions:
        {
            enableAnimation : true,
            type: 'doughnut',
            labelPosition: 'inside',
            tooltip: {
                visible: true,
                format: "#point.x# : #point.y#",
                font : {
                    fontFamily : "DINPro"
                }
            },
            marker:
            {
                dataLabel:
                {
                    shape: 'none',
                    visible: true,
                    textPosition: 'top',
                    border: { width: 1},
                    connectorLine: { height: 70, stroke:"black" },
                    font : {
                        fontFamily : "DINPro",

                    }
                },
                format: "#point.x# : #point.y#%"
            }
        },
        //Enabling 3D Chart
        enable3D: false,
        enableRotation:false,
        depth: 30,
        tilt: -30,
        rotation: -30,
        perspectiveAngle: 90,
        canResize: true,
        load: "onchartload",
        legend: {
            visible: true,
            font : {
                fontFamily : "DINPro"
            }
        }
    });
}

function setPieDetailData(d) {
    console.log('setPieDetailData');
    if (d.cost_salary + d.cost_szr + d.cost_min + d.cost_seed + d.cost_fuel+d.service_total+d.dop_material_fact > 0) {
        var s = {
            series: [
                {
                    points: [{x: "Зар.плата", y: d.cost_salary, text: (d.cost_salary)},
                        {x: "СЗР", y: d.cost_szr, text: d.cost_szr},
                        {x: "Мин. удобрения", y: d.cost_min, text: d.cost_min},
                        {x: "Семена", y: d.cost_seed, text: d.cost_seed},
                        {x: "Топливо", y: d.cost_fuel, text: (d.cost_fuel)},
					    {x: "Услуги", y: d.service_total, text: (d.service_total)},
						{x: "Пр. материалы", y: d.dop_material_fact, text: (d.dop_material_fact)}
                    ],
                    explodeIndex: 0,
                    border: {width: 2, color: 'white'},
                    labelPosition: 'inside',
                    startAngle: 145
                }
            ]
        };

        $("#piedetail").ejChart("option", {
            "drilldown": s
        });
    }
}