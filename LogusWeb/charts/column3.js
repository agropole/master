function initColumn3(cont_id, d) {
    $(cont_id).ejChart({
        titleRendering : "ontitlerender",
        pointRegionClick : "onpointclick",
        pointRegionMouseMove : "onpointmousemove",
        legendItemMouseMove : "onlegendmousemove",
        legendItemClick : "onlegendclicked",
        canResize : true,
        load : "loadTheme",
        primaryXAxis : {
            axisLine : {
                visible : true
            },
            font : {
                fontFamily : "DINPro"
            }
        },
        primaryYAxis : {
            visible : true,
            axisLine : {
                visible : true
            },
            font : {
                fontFamily : "DINPro"
            },
        },
        commonSeriesOptions:
        {
            type: 'stackingcolumn',
            labelPosition: 'outside',
            enableAnimation: true,
            tooltip: {
                visible: true,
                format: "#point.x# : #point.y#",
                font : {
                    fontFamily : "DINPro"
                }
            },
            marker:
            {
                dataLabel:
                {
                    shape: 'none',
                    visible: true,
                    textPosition: 'inside',
                    border: { width: 1},
                    connectorLine: { height: 70, stroke:"black" },
                    labelIntersectAction : 'rotate90',
                    font : {
                        fontFamily : "DINPro"
                    }
                },
                format: "#point.x# : #point.y#%"
            }
        },
        canResize: true,
        size : {
            height : "360"
        },
        legend : {
            visible : false
        },
        series: d
    });
}


function initColumnSeria(cont_id, d, title, legendHide) {
    $(cont_id).ejChart({
        titleRendering : "ontitlerender",
        pointRegionClick : "onpointclick",
        pointRegionMouseMove : "onpointmousemove",
        legendItemMouseMove : "onlegendmousemove",
        legendItemClick : "onlegendclicked",
        canResize : true,
        load : "loadTheme",
        primaryXAxis : {
            axisLine : {
                visible : true
            },
            labelIntersectAction : 'rotate90',
            font : {
                fontFamily : "DINPro"
            }
        },
        primaryYAxis : {
            visible : true,
            axisLine : {
                visible : true
            },
            font : {
                fontFamily : "DINPro"
            },
        },
        commonSeriesOptions:
        {
            type: 'column',
            labelPosition: 'outside',
            enableAnimation: true,
            tooltip: {
                visible: true,
                format: "#point.x# : #point.y#",
                font : {
                    fontFamily : "DINPro"
                }
            },
            marker:
            {
                dataLabel:
                {
                    visible:true,
                    template:'template'
                }
            }
        },
        canResize: true,
        size : {
            height : "360"
        },
        legend : {
            visible : !legendHide,
            shape: 'circle',
            font : {
                fontFamily: "DINPro"
            },
        },
        title: {
            text : title,
            font : {
                fontFamily : "DINPro"
            },
            textAlignment : "center"
        },
        series: d
    });
}