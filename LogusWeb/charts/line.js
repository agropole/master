/**
 * Created by ofirtych on 14/08/15.
 */

function initLine(cont_id, d, title) {
    $(cont_id).ejChart(
    {
        //Initializing Primary X Axis
        primaryXAxis : {
            axisLine : {
                visible : true
            },
            font : {
                fontFamily : "DINPro"
            }
        },
        primaryYAxis : {
            visible : true,
            axisLine : {
                visible : true
            },
            font : {
                fontFamily : "DINPro"
            },
        },
        //Initializing Common Properties for all the series
        commonSeriesOptions:
        {
            type: 'line', enableAnimation: true,
            tooltip:{ visible :true, template:'Tooltip'},
            marker:
            {
                shape: 'circle',
                size:
                {
                    height: 10, width: 10
                },
                visible: true
            },
            border : {width: 2}
        },

        //Initializing Series
        series:d,
        canResize:true,
        load:"loadTheme",
        title :{
            text: title,
            font : {
                fontFamily : "DINPro"
            }
        },
        legend: { visible: false}
    });
}