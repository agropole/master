function initPie(cont_id, d, legendPos, type) {
	$(cont_id).ejChart({
		//Initializing Common Properties for all the series
		commonSeriesOptions:
		{
			type: type ? type: 'doughnut',
			labelPosition: 'outside',
			smartLabelEnabled : true,
			enableAnimation: true,
			tooltip: {
				visible: true,
				format: "#point.x# : #point.y#",
				font : {
					fontFamily : "DINPro"
				}
			},
			marker:
			{
				dataLabel:
				{
					shape: 'none',
					visible: true,
					textPosition: 'top',
					border: { width: 1},
					connectorLine: { height: 70, stroke:"black" },
					font : {
						fontFamily : "DINPro",
						color: '#696969'
					}
				},
				format: "#point.x# : #point.y#%"
			},
			smartLabelEnabled:true, startAngle:145
		},
		canResize: true,
		title:{
			text : '    ',
			font : {
				fontFamily : "DINPro"
			}
		},
		legend : {
			position: legendPos,
			visible: true,
			shape: 'circle',
			font : {
				fontFamily: "DINPro"
			},
			size: {
				width: '200'
			}
		},
		series:
			[
				{
					points: d
				}
			]
	});
}