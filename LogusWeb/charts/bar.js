/**
 * Created by ofirtych on 10/06/15.
 */
function initBar(cont_id, data, title) {
	var series = [];
	var colors = ['#cc4646','#3c5275'];
	for (var i=0,li=data.length;i<li;i++) {
		series.push({
			fill: colors[i],
			name: data[i].title
		});
	}
	$(cont_id).ejChart({
		//Initializing Primary X Axis
		primaryXAxis : {
			labelPosition : "inside",
			font : {
				color : '#696969',
				fontFamily : "DINPro"
			},
			majorGridLines : {
				opacity : 0
			}
		},

		//Initializing Primary Y Axis
		primaryYAxis : {
			range : {},
			labelFormat : "{value}",
			title : {
				text : 'тыс.руб',
				font : {
					color : '#fff',
					fontFamily : "DINPro"
				}
			},
			font : {
				color : '#696969',
				fontFamily : "DINPro"
			},
			majorGridLines : {
				opacity : 0
			}
		},

		//Initializing Common Properties for all the series
		commonSeriesOptions : {
			type : 'stackingbar',
			enableAnimation : true,
			tooltip : {
				visible : true,
				format : "#point.x#: #point.y#<br/>",
				font : {
					fontFamily : "DINPro"
				}
			},
		},
		load : "loadTheme",
		displayTextRendering : "OndataLabel",
		canResize : true,
		showTooltip : true,
		title : {

		},
		size : {
			height : "350"
		},
		legend : {
			visible : false,
		},
		series : data
	});
}

function OndataLabel(sender) {
	sender.data.location.x = sender.data.location.x + 20;
}
