/**
 * Created by ofirtych on 15/07/15.
 */
function initBarDetail() {
    $("#bardetail").ejChart({
        //Initializing Primary X Axis
        primaryXAxis : {
            labelIntersectAction : 'rotate90',
            font : {
                fontFamily : "DINPro"
            }
        },
        //Initializing Primary Y Axis
        primaryYAxis : {
            labelFormat : "{value}",
            title : {
                text : 'тыс.руб',
                font : {
                    fontFamily : "DINPro"
                }
            },
            font : {
                fontFamily : "DINPro"
            },
            majorGridLines : {
                opacity : 0
            }
        },
        //Initializing Common Properties for all the series
        commonSeriesOptions : {
            type : 'column',
            enableAnimation : true,
            tooltip : {
                visible : true,
                format : "#point.x# : #point.y#",
                font : {
                    fontFamily : "DINPro"
                }
            }
        },
        load : "loadTheme",
        legend: { visible: false},
        canResize : true,
        showTooltip : true
    });
}

function setBarDetailData(data) {
    console.log('setBarDetailData');
    var points = [];
    var total = 0, area = 0;
    for (var i= 0, li=data.length; i<li; i++) {
        var s = 0;
        if (data[i].salary_total) {
            s += data[i].salary_total
        };
        if (data[i].minud_fact) {
            s += data[i].minud_fact
        };
        if (data[i].posevmat_fact) {
            s += data[i].posevmat_fact
        };
        if (data[i].szr_fact) {
            s += data[i].szr_fact
        };
        if (data[i].razx_top) {
            s += data[i].razx_top
        };
        total += s;
        area += data[i].area;
        points.push({
            x: data[i].fields_name,
            y: Math.round(s)
        });
    }
    $("#total_cost").text(Math.round(total * 100) / 100 + " руб.");
    $("#ga_cost").text(Math.round(total / area * 100) / 100 + " руб.");
    $("#bardetail").ejChart("option", {
        "drilldown" : {
            series : [
                {
                    points : points,
                    fill : "#69D2E7"
                }
            ]
        }
    });
}
