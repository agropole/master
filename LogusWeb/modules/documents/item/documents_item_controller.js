/**
 *  @ngdoc controller
 *  @name Documents.controller:DocumentsItemCtrl
 *  @description
 *  # DocumentsItemCtrl
 *  Это контроллер, определеяющий поведение формы НАЧИСЛЕНИЯ ЗП
 *  @requires $scope
 *  @requires $http
 *  @requires requests
 *  @requires $location
 *  @requires $stateParams
 *  @requires $cookieStore
 *  @requires modals
 *  @requires modalType
 *  @requires LogusConfig

 *  @property {Object} sc: scope
 *  @property {Array} mySelections: выбранные строки таблицы
 *  @property {Array} sc.tableData: данные таблицы
 *  @property {Object} sc.gridOptions: настройки таблицы
 *  @property {Object} formData: данные формы
 
 *  @property {string} typeOperation: тип операции с item
 *  @property {function} selectRowByField: выделение строчки по определенному столбцу
 *  @property {function} gridEvDataListener: листнер на событие изменение data в таблице
 *  @property {function} sc.dateChange: после изменения даты запросить данные с сервера
 *  @property {function} sc.editDropCell: при редатировании ячейки с выпадающим списком
 *  @property {function} sc.editCell: вызывается при редактировании ячейки с комментарием
 *  @property {function} sc.updateSelectedCell: кнопка "Сохранить" - сохраняем таблицу
 *  @property {function} sc.createWork: инициализация функционала карты
 *  @property {function} sc.saveWork : если устанавливают флаг Ремонтный путевой лист - подставляем маску для номера и значения по уолчанию
 *  @property {function} sc.deleteWork: кнопка "Удалить" - удаляем строчку в таблице
 *  @property {function} sc.printBtn: кнопка "Печать"
 *  @property {function} sc.submitForm: кнопка "ОК" - сохранение формы
 *  @property {function} sc.cancelClick: кнопка "Отмена" - выход без сохранения
 */
var app = angular.module("DocumentsItem", ["ngGrid", "services", 'ngCookies', "modalWindows", "LogusConfig"]);
app.controller("DocumentsItemCtrl", ["$scope", "$http", "requests", "$location", '$cookieStore', "modals", "modalType", '$stateParams', "LogusConfig", function ($scope, $http, requests, $location, $cookieStore, modals, modalType, $stateParams, LogusConfig) {
    var sc = $scope;

    sc.mySelections = [];
    sc.tableData = [];
    sc.formData = {};
	sc.dateDocument=moment($cookieStore.get('dateDocumentsCalendar').date).format("MM.DD.YYYY")
    sc.formData.SalaryDocId = $stateParams.doc_id;
    //тип операции с item (передается по клику по кнопке в documets_list)
    var typeOperation = $stateParams.type;
     sc.count =
    // свойства таблицы
    sc.gridOptions = {
        data: 'tableData',
        enableColumnResize: true,
        multiSelect: false,
        enableRowSelection: true,
        showFooter: false,
        selectedItems: sc.mySelections,
        rowHeight: 49,
        headerRowHeight: 49,

        //useExternalSorting: true,
        columnDefs: [
            {
                field: "Date2",
                displayName: "Дата",
                width: "8%",
               cellTemplate:'<a tooltip="{{row.getProperty(col.field)}}" tooltip-popup-delay="1000" tooltip-class="my-custom-tooltip" tooltip-placement="top" tooltip-append-to-body="true">' +
                '<datepicktable class="logus-headers col-md-2 col-xs-4 col-sm-2 col-lg-2" style="padding-top: 6px;margin-left: -22px;font-size: 15px;" data="row.entity[col.field]" mask="\'99.99.9999\'" regex-name="\'date\'" date-change="dateChange(row.entity, row.entity[col.field])">' +
                '</datepicktable>' +
                '</a>'

            },
            {
                field: "Employee",
                displayName: "Сотрудник",
                width: "15%",
                cellTemplate: /*'<div class="ngCellText" ng-class="col.colIndex()">' +
                 '<dropsearchtable drop="infoData.ResponsibleList" data="row.entity[col.field].Id" ng-click="editDropCell(row.entity, col.field, row.entity[col.field])"></dropsearchtable>'+
                 '</div>'*/
                '<a tooltip="{{row.entity[col.field][\'Name\']}}" tooltip-popup-delay="1000" tooltip-class="my-custom-tooltip" tooltip-placement="top" tooltip-append-to-body="true">' +
                '<div class="ngCellText" ng-class="col.colIndex()">' +
                '<input value="{{row.entity[col.field][\'Name\']}}" placeholder="Сотрудник" style="border: none;"  data-ng-click="updateSelectedCell(col.field, row.entity[col.field][\'Id\'], \'Сотрудник\',row.entity,$event)" readonly>' +
                '</div>' +
                '</a>'
            },
            {
                field: "Shift", displayName: "Смена", width: "7%",
                cellTemplate: '<a tooltip="{{row.entity[col.field][\'Name\']}}" tooltip-popup-delay="1000" tooltip-class="my-custom-tooltip" tooltip-placement="top" tooltip-append-to-body="true">' +
                '<div class="ngCellText" ng-class="col.colIndex()">' +
                '<input value="{{row.entity[col.field][\'Name\']}}" placeholder="Смена" style="border: none;" data-ng-click="updateSelectedCell(col.field, row.entity[col.field][\'Id\'],\'Смена\',row.entity,$event)" readonly>' +
                '</div>' +
                '</a>'
            },
            {
                field: "TypeTask", displayName: "Наименование работ", width: "15%",
                cellTemplate: '<a tooltip="{{row.entity[col.field][\'Name\']}}" tooltip-popup-delay="1000" tooltip-class="my-custom-tooltip" tooltip-placement="top" tooltip-append-to-body="true">' +
                '<div class="ngCellText" ng-class="col.colIndex()">' +
                '<input value="{{row.entity[col.field][\'Name\']}}" placeholder="Наименование работ" style="border: none;" data-ng-click="updateSelectedCell(col.field, row.entity[col.field][\'Id\'],\'Наименование работ\',row.entity,$event)" readonly>' +
                '</div>' +
                '</a>'
            },
            {
                field: "Field",
                displayName: "Поле",
                width: "7%",
                cellTemplate:'<a tooltip="{{row.entity[col.field][\'Name\']}}" tooltip-popup-delay="1000" tooltip-class="my-custom-tooltip" tooltip-placement="top" tooltip-append-to-body="true">' +
                '<div class="ngCellText" ng-class="col.colIndex()">' +
                '<input value="{{row.entity[col.field][\'Name\']}}" placeholder="№ поля" style="border: none;"  data-ng-click="updateSelectedCell(col.field, row.entity[col.field][\'Id\'],\'Поле\',row.entity,$event)"  readonly>' +
                '</div>' +
                '</a>'
            },
            {
                field: "Plant",
                displayName: "Культура",
                width: "8%",
                cellTemplate:'<a tooltip="{{row.entity[col.field][\'Name\']}}" tooltip-popup-delay="1000" tooltip-class="my-custom-tooltip" tooltip-placement="top" tooltip-append-to-body="true">' +
                '<div class="ngCellText" ng-class="col.colIndex()">' +
                '<input value="{{row.entity[col.field][\'Name\']}}" placeholder="Культура" style="border: none;"  data-ng-click="updateSelectedCell(col.field, row.entity[col.field][\'Id\'], \'Культура\',row.entity,$event)" readonly>' +
                '</div>' +
                '</a>'
            },
            {
                field: "TypeSalary", displayName: "Тип", width: "10%",
                cellTemplate:'<a tooltip="{{row.entity[col.field][\'Name\']}}" tooltip-popup-delay="1000" tooltip-class="my-custom-tooltip" tooltip-placement="top" tooltip-append-to-body="true">' +
                '<div class="ngCellText" ng-class="col.colIndex()">' +
                '<input value="{{row.entity[col.field][\'Name\']}}" placeholder="Тип оплаты" style="border: none;" data-ng-click="updateSelectedCell(col.field, row.entity[col.field][\'Id\'],\'Тип оплаты\',row.entity,$event)" readonly>' +
                '</div>' +
                '</a>'

            },
            {
                field: "Value", displayName: "Значение", width: "10%",
                cellTemplate:'<a tooltip="{{row.getProperty(col.field)}}" tooltip-popup-delay="1000" tooltip-class="my-custom-tooltip" tooltip-placement="top" tooltip-append-to-body="true">' +
                '<div class="ngCellText" ng-class="col.colIndex()">' +
                '<input value="{{row.entity[col.field]}}" ng-model="row.entity[col.field]" style="border: none;" ng-blur="editInputCell(row.entity, col.field, row.entity[col.field])"' +
                '<!--input value="{{row.entity[col.field]}}" ng-model="row.entity[col.field]" style="height: 32px;"-->' +
                '</div>' +
                '</a>'
            },
            {
                field: "Prices", displayName: "Расценка", width: "8%",
                cellTemplate:'<a tooltip="{{row.getProperty(col.field)}}" tooltip-popup-delay="1000" tooltip-class="my-custom-tooltip" tooltip-placement="top" tooltip-append-to-body="true">' +
                '<div class="ngCellText" ng-class="col.colIndex()">' +
                '<input value="{{row.entity[col.field]}}" ng-model="row.entity[col.field]" style="border: none;"  ng-blur="editInputCell(row.entity, col.field, row.entity[col.field])"' +
                '<!--input value="{{row.entity[col.field]}}" ng-model="row.entity[col.field]" style="height: 32px;"-->' +
                '</div>' +
                '</a>'
            },
            {
                field: "Sum", displayName: "Сумма", width: "6%",
                cellTemplate:'<a tooltip="{{row.entity[col.field]}}" tooltip-popup-delay="1000" tooltip-class="my-custom-tooltip" tooltip-placement="top" tooltip-append-to-body="true">' +
                '<div class="ngCellText" ng-class="col.colIndex()">' +
                '<input value="{{row.entity[col.field]}}" ng-model="row.entity[col.field]" style="height: 32px;">' +
                '</div>' +
                '</a>'
            },
            {
                field: "Comment", displayName: " ", width: "7%",
                cellTemplate: 'templates/comment_shift_template.html'
            }
        ],
        afterSelectionChange: function (row) {
            sc.mySelections = [];
            sc.mySelections.push(row.entity);
			console.log(sc.mySelections);
        }
    }
    ;

    var _field, _val;
    // выделение строчки по определенному столбцу
    function selectRowByField(field, val) {
        _field = field;
        _val = val;
        sc.$on('ngGridEventData', gridEvDataListener);
    }

    function gridEvDataListener() {
        var offCallMeFn = $scope.$on("ngGridEventData", gridEvDataListener);
        offCallMeFn();
        if (_val == "first" && sc.tableData.length > 0) _val = sc.tableData[0][_field];
        sc.tableData.forEach(function (item, i, arr) {
            if (item[_field] == _val) {
                sc.gridOptions.selectRow(i, true);
                if (sc.gridOptions.ngGrid) sc.gridOptions.ngGrid.$viewport.scrollTop(sc.gridOptions.ngGrid.rowMap[i] * sc.gridOptions.ngGrid.config.rowHeight);
            } else {
                sc.gridOptions.selectRow(i, false);
            }
        });
        $(".ngViewport").focus();
    };

    // получение масива в выпадающие списки
    requests.serverCall.post($http, "SalariesCtrl/GetSalariesDictionary", true, function (data, status, headers, config) {
        console.info(data);
        sc.infoData = data;
        // получение номера и даты создания документа
        if (typeOperation == 'create' || typeOperation == 'copy') {
            //посылаем наш часовой пояс и присваиваем документу дату и название
            requests.serverCall.post($http, "SalariesCtrl/GetNewInfoSalariesDoc", (new Date().getTimezoneOffset() / 60) * (-1), function (data, status, headers, config) {
                console.info(data);
                sc.Date = data.Date;
                sc.Number = data.Number;
                if (typeOperation == 'copy') {
                    // получаем инфо по выделенному документу
                    requests.serverCall.post($http, "SalariesCtrl/getSalariesDocInfoById", sc.formData.SalaryDocId, function (data, status, headers, config) {
                        console.info(data);
                        sc.formData = data;
                        sc.tableData = sc.formData.DetailList;
                        selectRowByField("detailDocId", "first");
                        // при копировании убираем id документа
                        delete sc.formData.SalaryDocId;
                        console.info(sc.formData);
                    });
                }
            });
        } else if (typeOperation == 'edit') {
            requests.serverCall.post($http, "SalariesCtrl/getSalariesDocInfoById", sc.formData.SalaryDocId, function (data, status, headers, config) {
                console.info(data);
                sc.formData = data;
                sc.tableData = sc.formData.DetailList;
                sc.Date = data.Date;
                sc.Number = data.Number;
            });
        }
    });

    // при редатировании ячейки с выпадающим списком
    sc.editDropCell = function (row, column, selectedItem) {
        sc.tableData.forEach(function (item, d) {
            if (item == row) item[column] = selectedItem;
        })
    };

    // при редатировании ячейки с input
    sc.editInputCell = function (row, column, value) {
		console.log('сработало');
    row.Value=row.Value.toString().replace(",", "."); 
    row.Prices=row.Prices.toString().replace(",", ".");	
		row.Sum=(row.Prices*row.Value).toFixed(2);
        sc.tableData.forEach(function (item, d) {
            if (item == row) item[column] = value;
        })
    };

    // вызывается при редактировании ячейки с комментарием
    sc.editCell = function (row, cell, column) {
        modals.showWindow(modalType.WIN_COMMENT, {
            nameField: "Комментарий",
            comment: cell
        })
            .result.then(function (result) {
                console.info(result);
                sc.mySelections[0]['Comment'] = result;
            }, function () {
                console.info('Modal dismissed at: ' + new Date());
            });
    };

    // редактирование ячек с модальными окнами
    sc.updateSelectedCell = function (colField, selectedVal, displayName, row) {
        sc.colField = colField;
        var modalValues;
        if (sc.infoData) {
            switch (sc.colField) {
                case "Plant":
                    console.info(sc.mySelections[0]);
                    if (sc.mySelections[0]["Field"]) {
                        console.info(sc.mySelections[0]["Field"]);
                        if (sc.mySelections[0]["Field"]["Id"]) {
                            console.info(sc.mySelections[0]["Field"]["Id"]);
                            for (var i = 0, il = sc.infoData.FieldsAndPlants.length; i < il; i++) {
                                if (sc.infoData.FieldsAndPlants[i].Id == sc.mySelections[0]["Field"]["Id"]) {
                                    modalValues = sc.infoData.FieldsAndPlants[i].Values;
                                }
                            }
                        } else modalValues = sc.infoData.PlantsByYear;
                    } else modalValues = sc.infoData.PlantsByYear;
                    break;
                case "Field":
                    modalValues = sc.infoData.FieldsAndPlants;
                    break;
                case "Shift":
                    modalValues = sc.infoData.ShiftList;
                    break;
                case "TypeTask":
                    modalValues = sc.infoData.TasksList;
                    break;
                case "Employee":
                    modalValues = sc.infoData.ResponsibleList;
                    break;
                case "Shift":
                    modalValues = sc.infoData.ShiftList;
                    break;
                case "TypeSalary":
                    modalValues = sc.infoData.TypeDocList;
                    break;
            }


            if (sc.colField == 'Employee') {
                modals.showWindow(modalType.WIN_SELECT_PAGINATION, {
                    nameField: displayName,
                    values: modalValues,
                    selectedValue: selectedVal
                })
                    .result.then(function (result) {
                        console.info(result);
                        sc.mySelections[0][sc.colField] = result;
                        if (sc.colField == 'Field') sc.mySelections[0]['Plant'] = {Id: null, Name: null};
                    }, function () {
                        console.info('Modal dismissed at: ' + new Date());
                    });
            } else {
                modals.showWindow(modalType.WIN_SELECT, {
                    nameField: displayName,
                    values: modalValues,
                    selectedValue: selectedVal
                })
                    .result.then(function (result) {
                        console.info(result);
                        sc.mySelections[0][sc.colField] = result;
                        if (sc.colField == 'Field') sc.mySelections[0]['Plant'] = {Id: null, Name: null};
                    }, function () {
                        console.info('Modal dismissed at: ' + new Date());
                    });
            }


        } else {
            console.log('не пришла sc.infoData');
        }
    };


    //кнопка "Создать" - создаем строчку в таблице
    sc.createWork = function () {
        if (!sc.$$phase) {
            sc.$apply(function () {
            });
        }
	//	if (sc.tableData.length==0) {
     //   var strDate=null;
	//	} else {var strDate=sc.Date;}

		var newWork = {
            Date2: null,
            Employee: {Id: null, Name: null},
            Shift: {Id: 1, Name: "1"},
            TypeTask: {Id: null, Name: null},
            Field: {Id: null, Name: null},
            Plant: {Id: null, Name: null},
            TypeSalary: {Id: null, Name: null},
            Value: null,
            Prices: null,
            Sum: null,
            Comment: null
        };
	
        selectRowByField("detailDocId", "first");
        sc.tableData.splice(0, 0, newWork);
    };
     //скопировать
	sc.copyWork = function () {
		console.log(sc.mySelections[0]);
		 var newWork = {
            Date2: moment(sc.mySelections[0].Date2).format("MM.DD.YYYY"),
            Employee: {Id: sc.mySelections[0].Employee.Id, Name: sc.mySelections[0].Employee.Name},
            Shift: {Id: sc.mySelections[0].Shift.Id, Name: sc.mySelections[0].Shift.Name},
            TypeTask:{Id: sc.mySelections[0].TypeTask.Id, Name: sc.mySelections[0].TypeTask.Name},
            Field: {Id: sc.mySelections[0].Field.Id, Name: sc.mySelections[0].Field.Name},
            Plant:{Id: sc.mySelections[0].Plant.Id, Name: sc.mySelections[0].Plant.Name},
            TypeSalary: {Id: sc.mySelections[0].TypeSalary.Id, Name: sc.mySelections[0].TypeSalary.Name},
            Value: sc.mySelections[0].Value,
            Prices: sc.mySelections[0].Prices,
            Sum: sc.mySelections[0].Sum,
            Comment: sc.mySelections[0].Comment
        };
	   sc.tableData.push(newWork);
	    setTimeout(function() {
            var grid = sc.gridOptions.ngGrid;
            grid.$viewport.scrollTop(sc.tableData.length * grid.config.rowHeight + 100);
        }, 100);
	}
	
	
    //кнопка "Сохранить" - сохраняем таблицу
    sc.saveWork = function () {
        sc.formData.Date = moment(sc.dateDocument).format("DD.MM.YYYY");  
		console.log(sc.dateDocument);
        sc.formData.Number = sc.Number;
        if (typeOperation == 'copy') {
            var table = angular.copy(sc.tableData);
            table.forEach(function (item, d) {
                if (item["detailDocId"]) delete item["detailDocId"];
            });
            sc.formData.DetailList = table;
        } else  sc.formData.DetailList = sc.tableData;
		        for (var i = 0, il = sc.formData.DetailList.length; i < il; i++) {
					
				sc.formData.DetailList[i].Date2=moment(sc.formData.DetailList[i].Date2).format("MM.DD.YYYY");
					}
			console.log(sc.formData);
        requests.serverCall.post($http, "SalariesCtrl/UpdateSalariesMainDocInfo", sc.formData, function (data, status, headers, config) {
            console.info(data);
            sc.formData.SalaryDocId = data;
            requests.serverCall.post($http, "SalariesCtrl/getSalariesDocInfoById", sc.formData.SalaryDocId, function (data, status, headers, config) {
                console.info(data);
                sc.formData = data;
                sc.tableData = sc.formData.DetailList;
              //  $cookieStore.put('dateDocumentsCalendar', {date: moment(sc.Date).format("MM.DD.YYYY")});
                modals.showWindow(modalType.WIN_MESSAGE, {
                    title: "Сохранение документа",
                    message: "Документ успешно сохранен",
                    callback: function () {
                    }
                });
            });

        });
    };

    //кнопка "Удалить" - удаляем строчку в таблице
    sc.deleteWork = function () {
        if (sc.mySelections[0]) {
            modals.showWindow(modalType.WIN_DIALOG, {
                    title: "Удаление", message: "Удалить выбранный документ?", callback: function () {
                        if (sc.mySelections[0].detailDocId) {
                            requests.serverCall.post($http, "SalariesCtrl/DeleteEmployeeSalaryInfo", sc.mySelections[0]["detailDocId"], function (data, status, headers, config) {
                                console.info(data);
                                sc.tableData.forEach(function (item, d) {
                                    if (item["detailDocId"] == sc.mySelections[0].detailDocId) sc.tableData.splice(d, 1);
                                })
                            });
                        } else {
                            sc.tableData.forEach(function (item, d) {
                                if (item == sc.mySelections[0]) sc.tableData.splice(d, 1);
                            })
                        }
                        selectRowByField("detailDocId", "first");
                    }
                }
            );
        } else {
            modals.showWindow(modalType.WIN_MESSAGE, {
                title: "Удаление строки",
                message: "Для удаления необходимо выбрать строку",
                callback: function () {
                }
            });
        }
    };

    // кнопка Печать
    sc.printBtn = function () {
        if (sc.formData.SalaryDocId) {
            var popupWin = window.open(LogusConfig.serverUrl + 'SalariesCtrl/LoadReport?id=' + sc.formData.SalaryDocId, '_blank');
            requests.serverCall.get($http, 'SalariesCtrl/LoadReport?id=' + sc.formData.SalaryDocId, function (data, status, headers, config) {
                console.info(data);
            });
        } else {
            modals.showWindow(modalType.WIN_MESSAGE, {
                title: "Печать",
                message: "Для печати необходимо сохранить данные",
                callback: function () {
                }
            });
        }

    };

    //кнопка ОК
    sc.submitForm = function () {
        sc.formData.Date = moment(sc.dateDocument).format("DD.MM.YYYY");  
        sc.formData.Number = sc.Number;
        if (typeOperation == 'copy') {
            var table = angular.copy(sc.tableData);
            table.forEach(function (item, d) {
                if (item["detailDocId"]) delete item["detailDocId"];
            });
            sc.formData.DetailList = table;
        } else  sc.formData.DetailList = sc.tableData;
		  for (var i = 0, il = sc.formData.DetailList.length; i < il; i++) {
					
				sc.formData.DetailList[i].Date2=moment(sc.formData.DetailList[i].Date2).format("MM.DD.YYYY");
					}
        requests.serverCall.post($http, "SalariesCtrl/UpdateSalariesMainDocInfo", sc.formData, function (data, status, headers, config) {
            console.info(data);
            sc.formData.SalaryDocId = data;
           // $cookieStore.put('dateDocumentsCalendar', {date: moment(sc.Date).format("MM.DD.YYYY")});
            $cookieStore.put('selectedSalaryDoc', {id: data});
            $location.path('/documents_list');
        });
    };

    //кнопка Отмена
    sc.cancelClick = function () {
        if (sc.formData.SalaryDocId) $cookieStore.put('selectedSalaryDoc', {id: sc.formData.SalaryDocId});
        $location.path('/documents_list');
    };
}])
;