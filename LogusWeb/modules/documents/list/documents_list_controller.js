/**
 *  @ngdoc controller
 *  @name Documents.controller:DocumentsListCtrl
 *  @description
 *  # DocumentsListCtrl
 *  Это контроллер, определеяющий поведение формы списка ДОКУМЕНТОВ по зарплате
 *  @requires $scope
 *  @requires $http
 *  @requires requests
 *  @requires $location
 *  @requires $stateParams
 *  @requires $cookieStore
 *  @requires modals
 *  @requires modalType

 *  @property {Object} sc: scope
 *  @property {Array} curUser: данные о пользователе
 *  @property {Array} sc.tableData: данные таблицы
 *  @property {Array} sc.mySelections: выбранные строки таблицы
 *  @property {Object} sc.gridOptions: настройки таблицы
 *  @property {Object} sc.pagingOptions: пейджинг таблицы
 *  @property {Object} sc.sortOptions: сортировка таблицы
 *  @property {int} sc.totalServerItems: общее количество строк в таблицы
 
 *  @property {function} pagingOptions: смотрим за изменениям в пейджинге
 *  @property {function} ngGridEventSorted: событие на сортировку по столбцу
 *  @property {function} sc.dateChangeDateDocuments: изменение даты просмотра списка документов
 *  @property {function} sc.gridEvDataListener: листнер на событие изменение data в таблице
 *  @property {function} getDocuments: получение списка документов
 *  @property {function} needSelection: модальное окно с требованием выделить строку
 *  @property {function} sc.createBtnClick: создание документа
 *  @property {function} sc.editBtnClick: редактирование документа
 *  @property {function} sc.deleteBtnClick: удаление документа
 *  @property {function} sc.copyBtnClick: копирование документа
 */
var app = angular.module("DocumentsList", ["ngGrid", "services", 'ngCookies', "modalWindows","LogusConfig"]);
app.controller("DocumentsListCtrl", ["$scope", "$http", "requests", "$location", '$cookieStore', "modals", "modalType",'LogusConfig', function ($scope, $http, requests, $location, $cookieStore, modals, modalType, LogusConfig) {
    var sc = $scope;
    sc.formData = {};
    var curUser = $cookieStore.get('currentUser');
    //устанавливаем даты в период по умолчанию (начало - 1 день текущего месяца, окончание - сегодня)
    sc.formData.docStartDate = moment().startOf('month').utc().toString("MM.DD.YYYY");
	
   if ($cookieStore.get('dateDocumentsCalendar')) {
		    sc.dateDocuments = moment(sc.dateDocuments).format("MM.DD.YYYY");
	 if ($cookieStore.get('dateDocumentsCalendar').date) {
		 sc.dateDocuments = $cookieStore.get('dateDocumentsCalendar').date;}
    }

    sc.tableData = [];
    sc.mySelections = [];

    // пейджинг
    sc.totalServerItems = 0;
    sc.pagingOptions = {
        pageSizes: [10, 15, 20, 50, 100],
        pageSize: 10,
        currentPage: 1,
        totalServerItems: sc.totalServerItems
    };

    //сортировка по столбцам
    sc.sortOptions = {
        fields: "Date",
        directions: "OrderByDescending" //'OrderBy' или 'OrderByDescending'
    };

    // смотрим за изменениям в пейджинге
    sc.$watch('pagingOptions', function (newVal, oldVal) {
        if (newVal.currentPage != null && newVal !== oldVal && !isNaN(newVal.currentPage) && !isNaN(oldVal.currentPage)) {
            if (newVal.pageSize !== oldVal.pageSize) {
                sc.pagingOptions.currentPage = 1;
            }
            if (newVal.currentPage !== oldVal.currentPage) getDocuments();
        }
    }, true);


    // событие на сортировку по столбцу
    sc.$on("ngGridEventSorted", function (e, field) {
            sc.sortOptions.fields = field.field;
            sc.sortOptions.directions = field.sortDirection == "asc" ? 'OrderBy' : 'OrderByDescending';
            getDocuments();
        }
    );

    // свойства таблицы
    sc.gridOptions = {
        data: 'tableData',
        enableColumnResize: true,
        multiSelect: false,
        enableRowSelection: true,
        enableCellEdit: true,
        enablePaging: true,
        showFooter: true,
        pagingOptions: sc.pagingOptions,
        selectedItems: sc.mySelections,
        rowHeight: 49,
        headerRowHeight: 49,
        useExternalSorting: true,
        i18n: 'ru',
        //footerTemplate: '',
        columnDefs: [
            {field: "Date", displayName: "Дата", width: "15%",  cellTemplate:'<a tooltip="{{row.getProperty(col.field)}}" tooltip-popup-delay="1000" tooltip-class="my-custom-tooltip" tooltip-placement="top" tooltip-append-to-body="true"><div class="ngCellText">{{row.getProperty(col.field)}}</div></a>'},
            {field: "Number", displayName: "Номер", width: "10%", cellTemplate:'<a tooltip="{{row.getProperty(col.field)}}" tooltip-popup-delay="1000" tooltip-class="my-custom-tooltip" tooltip-placement="top" tooltip-append-to-body="true"><div class="ngCellText">{{row.getProperty(col.field)}}</div></a>'},
            {field: "Organization", displayName: "Отделение", width: "15%", cellTemplate:'<a tooltip="{{row.getProperty(col.field)}}" tooltip-popup-delay="1000" tooltip-class="my-custom-tooltip" tooltip-placement="top" tooltip-append-to-body="true"><div class="ngCellText">{{row.getProperty(col.field)}}</div></a>'},
            {field: "Responsible", displayName: "Ответственный", width: "25%", cellTemplate:'<a tooltip="{{row.getProperty(col.field)}}" tooltip-popup-delay="1000" tooltip-class="my-custom-tooltip" tooltip-placement="top" tooltip-append-to-body="true"><div class="ngCellText">{{row.getProperty(col.field)}}</div></a>'},
			{field: "Summa", displayName: "Итого начислено", width: "15%", cellTemplate:'<a tooltip="{{row.getProperty(col.field)}}" tooltip-popup-delay="1000" tooltip-class="my-custom-tooltip" tooltip-placement="top" tooltip-append-to-body="true"><div class="ngCellText">{{row.getProperty(col.field)}}</div></a>'},
            {field: "Comment", displayName: "Комментарий", width: "20%", cellTemplate:'<a tooltip="{{row.getProperty(col.field)}}" tooltip-popup-delay="1000" tooltip-class="my-custom-tooltip" tooltip-placement="top" tooltip-append-to-body="true"><div class="ngCellText">{{row.getProperty(col.field)}}</div></a>'}
        ],
        afterSelectionChange: function (row) {
            sc.mySelections = [];
            sc.mySelections.push(row.entity);
        }
    };

    $('a').tooltip({
        'delay': { show: 5000, hide: 3000 }
    });

    // изменение даты просмотра списка документов
    sc.dateChangeDateDocuments = function () {
        if (sc.dateDocuments) {
			console.log(sc.dateDocuments);
            $cookieStore.put('dateDocumentsCalendar', {date: sc.dateDocuments});
            getDocuments();
        } else {
            console.log('wrong date from datepick')
        }
    };

    var _field, _val;
    // выделение строчки по определенному столбцу
    function selectRowByField(field, val) {
        _field = field;
        _val = val;
        sc.$on('ngGridEventData', sc.gridEvDataListener);
    }

    // листнер на событие изменение data в таблице
    sc.gridEvDataListener = function () {
        var offCallMeFn = $scope.$on("ngGridEventData", sc.gridEvDataListener);
        offCallMeFn();
        if (_val == "first" && sc.tableData.length > 0) _val = sc.tableData[0][_field];
        sc.tableData.forEach(function (item, i, arr) {
            if (item[_field] == _val) {
                sc.gridOptions.selectRow(i, true);
                if (sc.gridOptions.ngGrid) sc.gridOptions.ngGrid.$viewport.scrollTop(sc.gridOptions.ngGrid.rowMap[i] * sc.gridOptions.ngGrid.config.rowHeight);
            }
        });
        $(".ngViewport").focus();
    };

    // получение списка документов
    function getDocuments() {
        sc.mySelections = [];
     console.log('найдено');
        requests.serverCall.post($http, "SalariesCtrl/GetSalariesDocumentList", {
           DateFilter: moment(sc.dateDocuments).format("MM.DD.YYYY"),
            OrgId: curUser.orgid,
            PageSize: sc.pagingOptions.pageSize,
            PageNum: sc.pagingOptions.currentPage,
            SortField: sc.sortOptions.fields,
            SortOrder: sc.sortOptions.directions
        }, function (data, status, headers, config) {
            console.info(data);
            //sc.pagingOptions.CountPages - всего страниц
            sc.totalServerItems = data.CountItems;
            sc.pagingOptions.totalServerItems = data.CountItems;
            sc.tableData = data.Values;
            if ($cookieStore.get('selectedSalaryDoc')) {
                selectRowByField("SalaryDocId", $cookieStore.get('selectedSalaryDoc').id);
                $cookieStore.remove('selectedSalaryDoc');
            } else selectRowByField("SalaryDocId", "first");
        }, function () {
        });
    }

    // модальное окно с требованием выделить строку
    function needSelection(title, message) {
        modals.showWindow(modalType.WIN_MESSAGE, {
            title: title,
            message: message,
            callback: function () {
            }
        });
    }

    // создание документа
    sc.createBtnClick = function () {
        $location.path('/document/create/');
    };

    //редактирование документа
    sc.editBtnClick = function () {
        if (sc.mySelections[0]) {
            // console.info('edit  ' + sc.mySelections[0].WaysListId);
            $location.path('/document/edit/' + sc.mySelections[0].SalaryDocId);
        } else needSelection('Редактирование документа', "Для редактирования необходимо выбрать документ");
    };

    //удаление документа
    sc.deleteBtnClick = function () {
        if (sc.mySelections[0]) {
            modals.showWindow(modalType.WIN_DIALOG, {
                    title: "Удаление", message: "Удалить выбранный документ?", callback: function () {
                        requests.serverCall.post($http, "SalariesCtrl/deleteSalaryDoc", sc.mySelections[0].SalaryDocId, function (data, status, headers, config) {
                            sc.tableData.forEach(function (item, d) {
                                if (item["SalaryDocId"] == sc.mySelections[0].SalaryDocId) sc.tableData.splice(d, 1);
                            });
                            console.info(data);
                        });
                    }
                }
            );
        } else needSelection('Удаление документа', "Для удаления необходимо выбрать документ");
    };
 //выгрузка XML
	sc.export = function ()  {
     var dateStart = moment(angular.copy(sc.formData.docStartDate)).format("MM.DD.YYYY");
	 var dateEnd = moment(angular.copy(sc.formData.docEndDate)).format("MM.DD.YYYY");
      console.log(dateStart);
	  console.log(dateEnd);
	  var popupWin = window.open(LogusConfig.serverUrl + 'TMCExport/ExpDocumentsList?startDate=' + dateStart
      + '&endDate=' + dateEnd, '_blank');
		
	}
	
    // копирование документа (открывается страница как на создание-редактирование, но уже предзаполненная инфо с сервера (по идее копия как выделенная, но на текущую дату)
    sc.copyBtnClick = function () {
        if (sc.mySelections[0]) {
            $location.path('/document/copy/' + sc.mySelections[0].SalaryDocId);
        } else needSelection('Копирование документа', "Для копирования необходимо выбрать документ");
    }
	//загрузка файла 
  sc.loadFile = function () {
	   modals.showWindow(modalType.WIN_UPLOAD_FILE, {
                title: "Загрузка файла",
            }).result.then(function (result) {
                    console.info(result);
                }, function () {
                    console.info('Modal dismissed at: ');
                }); 
	  
  }
}]);













