/**
 *  @ngdoc controller
 *  @name Seeding.contorller:SeedingItemCtrl
 *  @description
 *  # SeedingItemCtrl
 *  Это контроллер, определеяющий поведение формы редактирования севооборота на поле
 *  @requires $scope
 *  @requires $http
 *  @requires requests
 *  @requires modalType
 *  @requires modals
 *  @requires $location
 *  @requires $stateParams

 *  @property {Object} sc scope
 *  @property {Object} sc.formData данные формы
 *  @property {bool} closeFlag если севооборот закрыт, то блокируется редактирование за этот год. Сейчас не используется
 *  @property {bool} firstLoad обозначает первую загрузку формы. Блокирует загрузку ненужных запросов
 *  @property {string} rateCellTemplate html template ячеек
 *  @property {Object} sc.tableDataPlant данные таблицы
 *  @property {Object} sc.tableDataSort сортировка таблицы
 *  @property {Object} sc.mySelectionsPlant выбранная ячейка
 *  @property {Object} sc.mySelectionsSort выбранный сорт
 *  @property {Object} sc.gridOptionsPlant настройки таблицы Культур
 *  @property {Object} sc.gridOptionsSort настройки таблицы Сортов
 *  @property {int} selectedPlantId id культуры выбранной в прошлый раз
 

 *  @property {function} sc.updateSelectedCell открытие модальный окон справочников
 *  @property {function} sc.rateCellClick модальное окно с двумя полями - План и Факт
 *  @property {function} selectRowByField выделение строчки по определенному столбцу. НЕ ИСПОЛЬЗУЕТСЯ
 *  @property {function} sc.gridEvDataListener листнер на событие изменение data в таблице
 *  @property {function} sc.refreshTable происходит по изменению значения в списках лет и полей
 *  @property {function} getInfoSedding инфо по данному севообороту за выбранный год на выбранном поле
 *  @property {function} sc.addPlant добавление культуры на текущее поле
 *  @property {function} sc.addSort добавление сорта культуры
 *  @property {function} sc.removeItem удаление сорта или культуры
 *  @property {function} sc.submitForm сохранение формы с выходом на общую форму
 *  @property {function} sc.cancelClick выход без сохранения
 *  @property {function} sc.addPlant добавление культуры на текущее поле
 *  @property {function} sc.saveForm сохранить изменения без выхода
 *  @property {function} sc.finishField НЕ ИСПОЛЬЗУЕТСЯ
 *  @property {function} sc.closeField закрыть поле. НЕ ИСПОЛЬЗУЕТСЯ

 */
var app = angular.module("SeedingItem", ["ngGrid", "services", 'ngCookies', "modalWindows"]);
app.controller("SeedingItemCtrl", ["$scope", "$http", "requests", "$location", "modals", "modalType", '$stateParams', function ($scope, $http, requests, $location, modals, modalType, $stateParams) {
    var sc = $scope;
    sc.formData = {};
    sc.closeFlag = false;
 
    var metka=0;
    var firstLoad = true;
    var rateCellTemplate = '<div class="ngCellTextSeedding" style="padding: 15px;" ng-class="col.colIndex()" ng-click="rateCellClick(col.field, row.entity.Sort[\'Id\'], col.displayName, row.entity[col.field].Plan, row.entity[col.field].Fact, row)">' +
        '<span style="color:#d8a331;">{{row.entity[col.field].Plan || \'0\'}}</span><span style="color:#06aa9f;">/<span><span style="color:#06aa9f;">{{row.entity[col.field].Fact|| \'0\'}}</span>' +
        '</div>';

    sc.tableDataPlant = [];
    sc.tableDataSort = [];
    sc.mySelectionsPlant = [];
    sc.mySelectionsSort = [];
    

    // свойства таблицы
    sc.gridOptionsPlant = {
        data: 'tableDataPlant',
        enableColumnResize: true,
        multiSelect: false,
        enableRowSelection: true,
        showFooter: false,
        selectedItems: sc.mySelectionsPlant,
        rowHeight: 49,
        headerRowHeight: 49,
        columnDefs: [
            {
                field: "Plant", displayName: "Культура", width: "100%",
                cellTemplate: '<a tooltip="{{row.entity[col.field][\'Name\']}}" tooltip-popup-delay="1000" tooltip-class="my-custom-tooltip" tooltip-placement="top" tooltip-append-to-body="true">' +
                '<div class="ngCellText" ng-class="col.colIndex()" ng-click="editCell(row.entity, row.getProperty(col.field), col.field)">' +
                '<input value="{{row.entity[col.field][\'Name\']}}" placeholder="Культура" style="border: none;"  data-ng-click="updateSelectedCell(col.field, row.entity[col.field][\'Id\'],\'Культура\',row,$event)"  readonly>' +
                '</div>'
            },
            {field: 'Sorts', visible: false},
            {field: "FieldPlantId", visible: false}
        ],
        afterSelectionChange: function (row) {
            if (row.selected) {
                sc.mySelectionsPlant = [];
                sc.mySelectionsPlant.push(row.entity);
            }
        }
    };

    sc.$watch('mySelectionsPlant', function (data) {
        //console.info(data);
        if (data[0] != undefined) {
            sc.tableDataSort = data[0].Sorts;
			console.log(data[0].Plant);
            selectRowByField("Plant", data[0].Plant);
            //console.info(sc.mySelectionsSort[0]);
            selectRowByField("Sort", "first");// (sc.mySelectionsSort[0] != undefined) ? sc.mySelectionsSort[0].Sort : "first");
        }
    }, true);

    sc.gridOptionsSort = {
        data: 'tableDataSort',
        enableColumnResize: true,
        multiSelect: false,
        enableRowSelection: true,
        showFooter: false,
        selectedItems: sc.mySelectionsSort,
        rowHeight: 49,
        headerRowHeight: 49,
        columnDefs: [
            {
                field: "Sort", displayName: "Сорт", width: "40%",
                cellTemplate: '<a tooltip="{{row.entity[col.field][\'Name\']}}" tooltip-popup-delay="1000" tooltip-class="my-custom-tooltip" tooltip-placement="top" tooltip-append-to-body="true">' +
                '<div class="ngCellText" ng-class="col.colIndex()" ng-click="editCell(row)">' +
                '<input value="{{row.entity[col.field][\'Name\']}}" placeholder="Сорт" style="border: none;"  data-ng-click="updateSelectedCell(col.field, row.entity[col.field][\'Id\'],\'Сорт\',row,$event)"  readonly>' +
                '</div>'
            },
            {
                field: "Reproduction", displayName: "Репродукция", width: "40%",
                cellTemplate: '<a tooltip="{{row.entity[col.field][\'Name\']}}" tooltip-popup-delay="1000" tooltip-class="my-custom-tooltip" tooltip-placement="top" tooltip-append-to-body="true">' +
                '<div class="ngCellText" ng-class="col.colIndex()" ng-click="editCell(row)">' +
                '<input value="{{row.entity[col.field][\'Name\']}}" placeholder="Репродукция" style="border: none;"  data-ng-click="updateSelectedCell(col.field, row.entity[col.field][\'Id\'],\'Репродукция\',row,$event)"  readonly>' +
                '</div>'
            },
            {field: "Area", displayName: "Площадь", width: "20%", cellTemplate: rateCellTemplate},
        ],
        afterSelectionChange: function (row) {
            sc.mySelectionsSort = [];
            sc.mySelectionsSort.push(row.entity);
        }
    };

    requests.serverCall.post($http, "SeedingCtrl/GetInfoForDetailSeeding", {FieldId : $stateParams.field_id, YearId : $stateParams.year}, function (data, status, headers, config) {
        console.info(data);
        sc.infoData = data;
        sc.formData.YearId = $stateParams.year;
        sc.formData.FieldId = $stateParams.field_id;
        getInfoSedding();
    });

    var selectedPlantId;
    // редактирование ячек с модальными окнами
    sc.updateSelectedCell = function (colField, selectedVal, displayName, row) {
        sc.colField = colField;
        var modalValues;
        if (sc.infoData) {
            switch (sc.colField) {
                case "Plant":
				metka = 1;			
				if (selectedVal == 36) {
					$("#delButton").addClass("disabledbutton");
				} else {
				$("#delButton").removeClass("disabledbutton");	
				}
                    if (selectedPlantId === selectedVal && selectedVal != 36 ) {
                     modalValues = sc.infoData.PlantsAndSorts;
                    }
					console.log(selectedVal);
                    selectedPlantId = selectedVal;
                    break;
                case "Sort":
				metka = 1;
                    if (sc.mySelectionsPlant[0]["Plant"]) {
                        if (sc.mySelectionsPlant[0]["Plant"]["Id"]) {
                            for (var i = 0, il = sc.infoData.PlantsAndSorts.length; i < il; i++) {
                                if (sc.infoData.PlantsAndSorts[i].Id == sc.mySelectionsPlant[0]["Plant"]["Id"]) {
                                    modalValues = sc.infoData.PlantsAndSorts[i].Values;
                                }
                            }
                        } else modalValues = [];
                    }
                    break;
                case "Reproduction":
				metka=1;
                    modalValues = sc.infoData.ReproductionList;
                    break;
            }
        }
        if (modalValues) {
            modals.showWindow(modalType.WIN_SELECT, {
                nameField: displayName,
                values: modalValues,
                selectedValue: selectedVal
            })
                .result.then(function (result) {
                    console.info(result);
                    if (sc.colField == 'Plant') {
					if (selectedPlantId == null) {selectedPlantId = result.Id;}
                        if (sc.mySelectionsPlant[0][sc.colField].Id !== result.Id) {
                            sc.mySelectionsPlant[0][sc.colField] = result;
                            //очищение сортов при перевыборе культуры
                            sc.mySelectionsPlant[0].Sorts = [];
                            sc.tableDataSort = [];
                            //selectRowByField("Plant", sc.mySelectionsPlant[0].Plant);
                        }
                    } else {
                        sc.mySelectionsPlant[0].Sorts[sc.colField] = result;
                        sc.mySelectionsSort[0][sc.colField] = result;
                        selectRowByField("Sort", sc.mySelectionsPlant[0].Sort);
                    }

                }, function () {
                    console.info('Modal dismissed at: ' + new Date());
                });
        } else {
            console.log('не пришла sc.infoData');
        }
        //}
    };

    //модальное окно с двумя полями - План и Факт
    sc.rateCellClick = function (colField, selectedVal, displayName, planValue, factValue, row) {
        if (colField === 'Plant') selectedId = sc.mySelectionsPlant[0]["Plant"]["Id"];
        else selectedId = sc.mySelectionsSort[0]["Sort"]["Id"];
        if (selectedId === selectedVal) {
            sc.colField = colField;
            modals.showWindow(modalType.WIN_DOUBLE_VALUES, {
                title: displayName,
                firstvalue: planValue,
                secondvalue: factValue
            }).result.then(function (result) {
                    console.info(result);
                    sc.mySelectionsSort[0][sc.colField] = result;
					console.log(sc.mySelectionsSort[0][sc.colField]);
                }, function () {
                    console.info('Modal dismissed at: ' + new Date());
                });
        }
    };


    var _field, _val, _tableData, _gridOptions, _grid;
    // выделение строчки по определенному столбцу
    function selectRowByField(field, val) {
		
        //console.info('selectRowByField ' + field + '  ' + val);
        _field = field;
        _val = val;
        _tableData = (_field == "Plant" ) ? sc.tableDataPlant : sc.tableDataSort;
        _gridOptions = (_field == "Plant") ? sc.gridOptionsPlant : sc.gridOptionsSort;
        _grid = (_field == "Plant") ? sc.gridOptionsPlant.ngGrid : sc.gridOptionsSort.ngGrid;
        sc.$on('ngGridEventData', sc.gridEvDataListener);
		
    }

    // листнер на событие изменение data в таблице
    sc.gridEvDataListener = function () {
        //console.info('gridEvDataListener');
        var offCallMeFn = $scope.$on("ngGridEventData", sc.gridEvDataListener);
        offCallMeFn();
        // console.info(_val);
        if (_val == "first" && _tableData.length > 0) _val = _tableData[0][_field];
        // console.info(_val);
        _tableData.forEach(function (item, i, arr) {
            if (item[_field] == _val) {
                _gridOptions.selectRow(i, true);
                if (_gridOptions.ngGrid) _grid.$viewport.scrollTop(_grid.rowMap[i] * _grid.config.rowHeight);
            } else {
                _gridOptions.selectRow(i, false);
            }
        });
        //$(".ngViewport").focus();
    };

    //происходит по изменению значения в списках лет и полей
    sc.refreshTable = function () {
		console.log('====1');
        if (!firstLoad) getInfoSedding();
    };

    // инфо по данному севообороту за выбранный год на выбранном поле
    var getInfoSedding = function () {
        sc.closeFlag = false;
        requests.serverCall.post($http, "SeedingCtrl/GetDetailSeedingBridleList", {
            YearId: sc.formData.YearId,
            FieldId: sc.formData.FieldId
        }, function (data, status, headers, config) {
            console.info(data);
            sc.tableDataPlant = data.Values;
            for (var i = 0, li = data.Values.length; i < li; i++) {
                if (data.Values[i].Status === 3) {
                    sc.closeFlag = true;
                }
            }
            if (data.Values.length === 0) {
                sc.tableDataSort = [];
            }
       //   selectRowByField("Plant", "first");
			 if (!firstLoad) {
			requests.serverCall.post($http, "SeedingCtrl/GetInfoForDetailSeeding", 
			{FieldId : sc.formData.FieldId, YearId : sc.formData.YearId}, function (data, status, headers, config) {
            console.info(data);
            sc.infoData.PlantsAndSorts = data.PlantsAndSorts;
             }); 
		 }
            sc.formData.Area = data.area;
            firstLoad = false;

        });
    };

    sc.addPlant = function () {
        var newPlant = {
            Plant: {Id: null, Name: null},
            Sorts: [],
            FieldPlantId: null
        };
        sc.tableDataPlant.splice(0, 0, newPlant);
     //   selectRowByField("Plant", "first");
    };

    sc.addSort = function () {
		metka = 1;
        if (sc.mySelectionsPlant[0]) {
            var newSort = {
                Sort: {Id: null, Name: null},
                Reproduction: {Id: null, Name: null},
                Area: {Fact: null, Plan: null},
                Productivity: {Fact: null, Plan: null},
                СostMln: {Fact: null, Plan: null},
                СostTn: {Fact: null, Plan: null},
                СostGa: {Fact: null, Plan: null}
            };
            sc.mySelectionsPlant[0].Sorts.splice(0, 0, newSort);
            selectRowByField("Sort", "first");
        } else {
            modals.showWindow(modalType.WIN_MESSAGE, {
                title: "Добавление сорта",
                message: "Для добавления сорта необходимо выбрать или добавить культуру",
                callback: function () {
                }
            });
        }
    };

    sc.removeItem = function (type) {
        var selectionRow, message;
        if (type === 'Plant') {
            selectionRow = sc.mySelectionsPlant[0];
            message = "Удалить выбранную культуру?"
        } else if (type === 'Sort') {
            selectionRow = sc.mySelectionsSort[0];
            message = "Удалить выбранный сорт?"
        }
	//	console.log(sc.mySelectionsPlant[0]);
        if (selectionRow) {
            modals.showWindow(modalType.WIN_DIALOG, {
                title: "Удаление", message: message, callback: function () {
                    var delIndex;
                    if (type === 'Plant') {
						//проверка на затраты
						    requests.serverCall.post($http, "SeedingCtrl/CheckSeedingSpeendingCult", sc.mySelectionsPlant[0].FieldPlantId, function (data, status, headers, config) {
							 console.info(data);  
                             console.info(data.Id);    
						//
						if (data.Id==1) {
						console.log('удаление');
						metka = 1;
                        sc.tableDataPlant.forEach(function (item, d) {
                            if (item == sc.mySelectionsPlant[0]) {
                                sc.tableDataPlant.splice(d, 1);
                                delIndex = (d - 1) > 0 ? (d - 1) : 0;
                                sc.tableDataSort = [];
                            }
                        });
                        if (delIndex)selectRowByField("Plant", sc.tableDataPlant[delIndex]["Plant"]);
						} else {
							console.log('нельзя удалять');
							 modals.showWindow(modalType.WIN_MESSAGE, {
                             title: "Удаление",
                             message: "На данную культуру имеются затраты. Удаление невозможно",
                            callback: function () {
                            }
                            });
						}
						 });
						//===
                    } else if (type === 'Sort') {
						console.log('удаление');
						metka = 1;
                        sc.tableDataSort.forEach(function (item, d) {
                            if (item == sc.mySelectionsSort[0]) {
                                sc.tableDataSort.splice(d, 1);
                                console.info(d);
                                delIndex = (d - 1) > 0 ? (d - 1) : 0;
                            }
                        });
                        sc.mySelectionsPlant[0].Sorts = sc.tableDataSort;
                        //sc.mySelectionsSort = [];
                        //sc.mySelectionsSort.push(sc.tableDataSort[delIndex]);
                        if (delIndex)selectRowByField("Sort", sc.tableDataSort[delIndex]["Sort"]);

                    }
                }
            });
        } else {
            modals.showWindow(modalType.WIN_MESSAGE, {
                title: "Удаление",
                message: "Для удаления необходимо выбрать строку",
                callback: function () {
                }
            });
        }
    };
    sc.cancelClick = function () {
		if (metka==1) {
			var   checkModal = 0;
		$("#myModal").modal("show");	
		 } else {
     $location.path('/seeding_list');
		 }
		 
    };
      sc.yes = function () {
		 $("#myModal").modal("hide");
		 $("div.modal-backdrop").hide();
		  checkModal = 1;
		  sc.saveForm();
	//	  $location.path('/seeding_list');
	}
   sc.no = function () {
	$("#myModal").modal("hide");
	$("div.modal-backdrop").hide();
	$location.path('/seeding_list');	 
	}
      sc.cancel = function () {
		$("#myModal").modal("hide");    
	  }

    sc.saveForm = function () {
		console.log(selectedPlantId);
		var met = 0;var plan = 0;  var fact = 0;
		for (var i = 0; i < sc.tableDataPlant.length; i++)
		{ if (sc.formData.Area < plan || sc.formData.Area < fact) {
			break;
		}
			//var plan = 0;  var fact = 0;
			for (var j = 0; j < sc.tableDataPlant[i].Sorts.length; j++)
		{

			plan = plan + (isNaN(parseFloat(sc.tableDataPlant[i].Sorts[j].Area.Plan)) ? 0 : (parseFloat(sc.tableDataPlant[i].Sorts[j].Area.Plan)));
			fact = fact + (isNaN(parseFloat(sc.tableDataPlant[i].Sorts[j].Area.Fact)) ? 0 : (parseFloat(sc.tableDataPlant[i].Sorts[j].Area.Fact)));
			console.log(plan);
			console.log(fact);
		}
		if (sc.formData.Area < plan || sc.formData.Area < fact) {
			met = 1 ;
		 modals.showWindow(modalType.WIN_DIALOG, {
            title: "Предупреждение", message: "Введенное значение площади превышает площадь поля? Сохранить?", callback: function () {
          sc.save();			  
            }
        });	
		break;
		}
		}
		 if (met == 0) {sc.save();}

		 /*
		//шаблоны
		  requests.serverCall.post($http, "TechCardTemp/CheckSortPlantYear", {
            FieldId: sc.formData.FieldId,
            YearId: sc.formData.YearId,
            Values: sc.tableDataPlant
        }, function (data, status, headers, config) {
			if (data!="empty_message") {
           modals.showWindow(modalType.WIN_MESSAGE, {
                title: "Сообщение",
                message: data,
                callback: function () {
                }
            });
			}
        });
		*/
    };
	 sc.save = function () {
		   requests.serverCall.post($http, "SeedingCtrl/UpdateDetailSeeing", {
            FieldId: sc.formData.FieldId,
            YearId: sc.formData.YearId,
            Values: sc.tableDataPlant
        }, function (data, status, headers, config) {
            console.info(data);
			metka = 0;
			if (checkModal == 1) { $location.path('/seeding_list');}
			/*
		 requests.serverCall.post($http, "TechCardTemp/CheckSortPlantYear", {
            FieldId: sc.formData.FieldId,
            YearId: sc.formData.YearId,
            Values: sc.tableDataPlant
        }, function (data, status, headers, config) {
			if (data!="empty_message") {
           modals.showWindow(modalType.WIN_MESSAGE, {
                title: "Сообщение",
                message: data,
                callback: function () {
                }
            });
			}
        });
		*/
            sc.tableDataPlant = data.Values;
            modals.showWindow(modalType.WIN_MESSAGE, {
                title: "Сохранение",
                message: "Культура для поля сохранена",
                callback: function () {
                }
            });
			var selarr = [];
			//выделяем культуру
			for (var i = 0; i < sc.tableDataPlant.length; i++)
		{
			if (sc.tableDataPlant[i].Plant.Id == selectedPlantId) { 
            console.info('data');
			selectRowByField("Plant", sc.tableDataPlant[i].Plant); break;}
		}
        }); 	 
		 }
    sc.finishField = function () {

    };

    sc.closeField = function() {
        modals.showWindow(modalType.WIN_DIALOG, {
            title: "Закрытие", message: "Вы уверены, что хотите завершить работы на этом поле?", callback: function () {
                requests.serverCall.post($http, "SeedingCtrl/CloseSeedingBridle", {
                    FieldId: sc.formData.FieldId,
                    YearId: sc.formData.YearId,
                    DateClose: sc.dateClose
                }, function () {
                    sc.closeFlag = true;
                });
            }
        });
    }
}])
;