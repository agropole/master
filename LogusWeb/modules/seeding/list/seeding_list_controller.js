/**
 *  @ngdoc controller
 *  @name Seeding.contorller:SeedingListCtrl
 *  @description
 *  # SeedingListCtrl
 *  Это контроллер, определеяющий поведение формы таблицы Севооборота
 *  @requires $scope
 *  @requires $http
 *  @requires requests
 *  @requires modalType
 *  @requires modals
 *  @requires $location
 *  @requires $cookieStore
 *  @requires $stateParams

 *  @property {Object} sc scope
 *  @property {bool} firstLoad обозначает первую загрузку формы. Блокирует загрузку ненужных запросов
 *  @property {Object} sc.tableData данные таблицы
 *  @property {Object} sc.mySelections выбранная ячейка таблицы
 *  @property {Object} sc.formData данные формы
 *  @property {int} sc.currentPlantId текущая культура
 *  @property {int} sc.currentYearId текущий год
 *  @property {string} headerDisTemplate template header нетекущий год
 *  @property {string} fieldCellTemplate template поле
 *  @property {string} yearsCellTemplate template культуры и показатели по годам

 *  @property {Object} sc.myColumnDefs html template ячеек
 *  @property {int} sc.totalServerItems количество записей в таблице для пейджинга
 *  @property {Object} sc.pagingOptions настройки пейджинга
 *  @property {Object} sc.mySelection выделенная ячейка
 *  @property {Object} sc.sortOptions сортировка по столбцам
 *  @property {Object} sc.gridOptions настройки таблицы

 *  @property {Object} sc.tableDataPlant данные таблицы
 *  @property {Object} sc.tableDataSort сортировка таблицы
 *  @property {Object} sc.mySelectionsPlant выбранная ячейка
 *  @property {Object} sc.mySelectionsSort выбранный сорт
 *  @property {Object} sc.gridOptionsPlant настройки таблицы Культур
 *  @property {Object} sc.gridOptionsSort настройки таблицы Сортов
 *  @property {int} selectedPlantId id культуры выбранной в прошлый раз
 

 *  @property {function} sc.getPlantStyle определение стиля ячейки в зависимости от статуса поля
 *  @property {function} sc.refreshTable обновление таблицы
 *  @property {function} getSeeding запрос на данные таблицы Севообороте
 *  @property {function} sc.yearsCellClick клик по ячейке с культурами и значением показателя
 *  @property {function} sc.refreshTable происходит по изменению значения в списках лет и полей
 *  @property {function} sc.editSeedingItem редактирование ячейки. НЕ ИСПОЛЬЗУЕТСЯ
 *  @property {function} showMessage показать сообщение. НЕ ИСПОЛЬЗУЕТСЯ
 */
 
var app = angular.module("SeedingList", ["ngGrid", "services", 'ngCookies', "modalWindows"]);
app.controller("SeedingListCtrl", ["$scope", "$http", "requests", "$location", '$cookieStore', "modals", "modalType", function ($scope, $http, requests, $location, $cookieStore, modals, modalType) {
    var sc = $scope;
    var firstLoad = true;
    var curYear;

    sc.tableData = [];
    sc.mySelections = [];
    sc.formData = {};
    sc.formData.Indicator = 1;
    sc.currentPlantId = '';
    sc.currentYearId = '';
    $("#btn").show();
    // templates for table
    // нетекущий год header
    var headerDisTemplate = '<div class="ngHeaderSortColumn {{col.headerClass}}" ng-style="{\'cursor\': col.cursor}" ng-class="{ \'ngSorted\': !noSortVisible }">' +
        '<div ng-click="col.sort($event)" ng-class="\'colt\' + col.index" class="ngHeaderText" style="color: grey; margin-left:45px;">{{col.displayName}}</div>' +
        '<div class="ngSortButtonDown" ng-show="col.showSortButtonDown()">' +
        '</div><div class="ngSortButtonUp" ng-show="col.showSortButtonUp()">' +
        '</div><div class="ngSortPriority">{{col.sortPriority}}</div>' +
        '<div ng-class="{ ngPinnedIcon: col.pinned, ngUnPinnedIcon: !col.pinned }" ng-click="togglePin(col)" ng-show="col.pinnable">' +
        '</div>' +
        '</div>' +
        '<div ng-show="col.resizable" class="ngHeaderGrip" ng-click="col.gripClick($event)" ng-mousedown="col.gripOnMouseDown($event)"></div>';
    // поле
    var fieldCellTemplate = '<div class="ngCellTextSeedding" ng-class="col.colIndex()" style="width:100px!important">' +
        '<span data-ng-model="row.entity[col.field][\'Id\']">{{row.entity[col.field][\'Name\']}}</span>' +
        '</div>';
    // культуры и показатели по годам
    var yearsCellTemplate = '<div style="height: auto" class="ngCellElement ngCellTextSeedding" ng-class="col.colIndex()" ng-click="yearsCellClick(row.entity.Field, col.displayName)">' +
        '<div ng-repeat="d in row.entity[col.field]" style="clear: right; height: 20px">' +
        '<span ng-style="getPlantStyle(d.Status)" style="float: left;max-width:50%;height: 20px;margin-right: 5px;text-overflow: ellipsis;white-space: nowrap;overflow: hidden;">{{d.PlantName}}</span>' +
        '<div style="float:right;max-width:50%;white-space: nowrap;overflow: hidden;margin-right: 10px;">' +
        '<span style="color:#d8a331;  white-space: nowrap;overflow: hidden;">{{d.Plan || \'0\'}}</span>' +
        '<span style="color:#06aa9f;">/<span><span style="color:#06aa9f;white-space: nowrap;overflow: hidden;">{{d.Fact|| \'0\'}}</span></span><div>' +
        '</div></div>';

    sc.getPlantStyle = function (status) {
        if (status != undefined) {
            if (status == 1) {
                return {color: "#06aa9f"};
            } else {
                return {color: "#5a5a5a"};
            }
        } else {
            return {color: "#5a5a5a"};
        }
    };

    sc.myColumnDefs = [];
    // пейджинг
    sc.totalServerItems = 0;
    sc.pagingOptions = {
        pageSizes: [10, 15, 20, 50],
        pageSize: $cookieStore.get("seedingListSettings") ? $cookieStore.get("seedingListSettings").pageSize : 10,
        currentPage: $cookieStore.get("seedingListSettings") ? $cookieStore.get("seedingListSettings").currentPage : 1,
        totalServerItems: sc.totalServerItems
    };
    sc.mySelection = [];

    //сортировка по столбцам
    sc.sortOptions = {
        fields: "Field",
        directions: "OrderBy" 
    };

    // смотрим за изменениям в пейджинге
    sc.$watch('pagingOptions', function (newVal, oldVal) {
            if (newVal.currentPage != null && newVal !== oldVal && !isNaN(newVal.currentPage) && !isNaN(oldVal.currentPage)) {
                if (newVal.pageSize !== oldVal.pageSize) {
                    sc.pagingOptions.currentPage = 1;
                }
                if (newVal.currentPage !== oldVal.currentPage) getSeeding();
                else if (newVal.pageSize !== oldVal.pageSize) getSeeding();
            }
        }
        ,
        true
    );

    // событие на сортировку по столбцу
    sc.$on("ngGridEventSorted", function (e, field) {
            sc.sortOptions.fields = field.field;
            sc.sortOptions.directions = field.sortDirection == "asc" ? 'OrderBy' : 'OrderByDescending';
            getSeeding();
        }
    );

    // свойства таблицы
    sc.gridOptions = {
        data: 'tableData',
        enableColumnResize: true,
        multiSelect: false,
        enableRowSelection: false,
        enableCellSelection: true,
        enableCellEdit: false,
        enablePaging: true,
        showFooter: true,
        pagingOptions: sc.pagingOptions,
        selectedItems: sc.mySelections,
        rowHeight: 49,
        headerRowHeight: 49,
        useExternalSorting: true,
        i18n: 'ru',
        beforeSelectionChange: function (row) {
            //console.info(row.selected);
            if (row.entity) {
                //console.info("afterSelectionChange row.entity  " + JSON.stringify(row.entity));
                if (row.selected) {
                    if (row.selected) sc.mySelections.push(row.entity);
                    else sc.mySelections.splice(sc.mySelections.indexOf(row.entity), 1)
                } else {
                    sc.mySelections = [];
                    sc.mySelections.push(row.entity);
                }
            }
            //console.info(sc.mySelections);
        },
        //footerTemplate: '',
        columnDefs: 'myColumnDefs',
        rowTemplate: '<div ng-style="{ \'cursor\': row.cursor }" ng-repeat="col in renderedColumns" ng-class="col.colIndex()" class="ngCellSeedding {{col.cellClass}}" >' +
        '<div class="ngVerticalBar" ng-style="{height: rowHeight}" ng-class="{ ngVerticalBarVisible: !$last }">&nbsp;</div>' +
        '<div ng-cell></div></div>'
    };

    sc.refreshTable = function (type) {
        if (type == "year") {
            sc.infoData.yearList.forEach(function (item) {
                if (item.Id === sc.formData.YearId) {
                    curYear = Number(item.Name);
                }
            });
            sc.myColumnDefs = [
                {
                    field: "Field",
                    displayName: "Поле",
                    width: "20%",
                    cellTemplate: fieldCellTemplate,
                    cellClass: "fieldCell"
                },
                {
                    field: "year1",
                    displayName: String(curYear - 2),
                    width: "16%",
                    headerCellTemplate: headerDisTemplate,
                    cellTemplate: yearsCellTemplate
                },
                {
                    field: "year2",
                    displayName: String(curYear - 1),
                    width: "16%",
                    headerCellTemplate: headerDisTemplate,
                    cellTemplate: yearsCellTemplate
                },
                {
                    field: "year3", displayName: String(curYear), width: "16%",
					headerCellTemplate: headerDisTemplate,
                    cellTemplate: yearsCellTemplate
                },
                {
                    field: "year4",
                    displayName: String(curYear + 1),
                    width: "16%",
                    headerCellTemplate: headerDisTemplate,
                    cellTemplate: yearsCellTemplate
                },
                {
                    field: "year5",
                    displayName: String(curYear + 2),
                    width: "16%",
                    headerCellTemplate: headerDisTemplate,
                    cellTemplate: yearsCellTemplate
                },
                {
                    field: "Status", visible: false
                }
            ];
            sc.gridOptions.columnDefs = sc.myColumnDefs;
            getSeeding();
        }
        if (!firstLoad) getSeeding();
    };

    requests.serverCall.post($http, "SeedingCtrl/GetInfoForSeedingBridle", true, function (data, status, headers, config) {
        console.info(data);
        sc.infoData = data;
        sc.formData.OrgId = $cookieStore.get("seedingListSettings") ? $cookieStore.get("seedingListSettings").OrgId : -1;
        sc.formData.PlantId = $cookieStore.get("seedingListSettings") ? $cookieStore.get("seedingListSettings").PlantId : -1;
        sc.formData.YearId = $cookieStore.get("seedingListSettings") ? $cookieStore.get("seedingListSettings").YearId : new Date().getFullYear();
        sc.formData.Indicator = /*$cookieStore.get("seedingListSettings") ? $cookieStore.get("seedingListSettings").Indicator :*/ 1;
		sc.formData.FieldId = -1;
    });

    var getSeeding = function () {
        requests.serverCall.post($http, "SeedingCtrl/GetSeedingBridleList", {
            PageSize: sc.pagingOptions.pageSize,
            PageNum: sc.pagingOptions.currentPage,
            SortField: /*sc.sortOptions.fields*/"FieldName",
            SortOrder: sc.sortOptions.directions,
            FilterOrgId: sc.formData.OrgId,
            FilterYearId: sc.formData.YearId,
            FilterPlantId: sc.formData.PlantId,
			FilterFieldId: sc.formData.FieldId,
            FilterIndicatorId: sc.formData.Indicator
        }, function (data, status, headers, config) {
            console.info(data);
            sc.totalServerItems = data.CountItems;
            sc.pagingOptions.totalServerItems = data.CountItems;
            sc.tableData = data.Values;
            $(".ngViewport").focus();
            firstLoad = false;
        });
    };

    //клик по ячейке с культурами и значением показателя
    sc.yearsCellClick = function (currentPlant, currentYear) {
        sc.currentPlantId = currentPlant.Id;
        sc.currentYearId = currentYear;
    };

    $(document).on('click', function (e) {
        if (!$(e.target).closest('.ngCellElement').length) {
            sc.currentPlantId = '';
            sc.currentYearId = '';
        }
    });

    sc.editSeedingItem = function () {
        $cookieStore.put('seedingListSettings', {
                OrgId: sc.formData.OrgId,
                PlantId: sc.formData.PlantId,
                YearId: sc.formData.YearId,
                Indicator: sc.formData.Indicator,
                pageSize: sc.pagingOptions.pageSize,
                currentPage: sc.pagingOptions.currentPage
            }
        );
        if (sc.currentYearId != undefined && sc.currentPlantId != undefined && sc.currentPlantId != '' && sc.currentYearId != '') {
            $location.path("/seeding_item/" + sc.currentYearId + "/" + sc.currentPlantId);
        } else showMessage("Редактирование севооборота", "Для редактирования необходимо выбрать ячейку");

    };

    var showMessage = function (title, message) {
        modals.showWindow(modalType.WIN_MESSAGE, {
            title: title,
            message: message,
            callback: function () {
            }
        });
    }

}])
;