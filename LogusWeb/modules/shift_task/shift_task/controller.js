/**
 *  @ngdoc controller
 *  @name ShiftTask.contorller:CreateShiftTaskCtrl
 *  @description
 *  # CreateShiftTaskCtrl
 *  Это контроллер, определеяющий поведение формы Сменных заданий
 *  @requires $scope
 *  @requires $http
 *  @requires requests
 *  @requires modalType
 *  @requires modals
 *  @requires $location
 *  @requires $cookieStore
 *  @requires $stateParams
 *  @requires $rootScope
 *  @requires preloader

 *  @property {Object} sc scope
 *  @property {Object} sc.shifts смены
 *  @property {bool} sc.editBtnVis видимость кнопок для Бачурина
 *  @property {bool} sc.createWayBtnVis видимость кнопки Создать путевой лист (для диспетчеров)
 *  @property {bool} sc.disCreateWayBill блокирование кноки Создать путевой лист, если он не был утвержден
 *  @property {string} sc.DayTaskId id текущего сменного задания для выделения после перехода с и на Путевые листы
 *  @property {Array} sc.PrintShiftInfo1 НЕ ЗНАЮ
 *  @property {Array} sc.PrintShiftInfo2 НЕ ЗНАЮ
 *  @property {Object} sc.multi scope
 *  @property {Object} sc.multi разрешает мультивыделение
 *  @property {Object} sc.Comment общий комментарий для дня
 *  @property {Array} sc.dataTraktors незанятая техника
 *  @property {Array} sc.dataEmployees незанятые водители 
 *  @property {Object} curUser информация о пользователе
 *  @property {int} sc.activeTab активная вкладка
 *  @property {Object} sc.tableDataFirst данные таблицы первой смены
 *  @property {Object} sc.tableDataSecond данные таблицы второй смены
 *  @property {Object} sc.selectedRow выделенные строки
 *  @property {Object} sc.columnDefinitions настройки колонок
 *  @property {Object} sc.gridFirstOptions настройки таблицы 1 смены
 *  @property {Object} sc.gridSecondOptions настройки таблицы 2 смены


 *  @property {function} sc.getClassCopyCell стиль скопированной строки
 *  @property {function} getDayTasks получение data по двум сменам за определенную дату
 *  @property {function} sendTask отправка измененного задания на сервер
 *  @property {function} sc.dateChangeShifts изменение даты отображения сменных заданий
 *  @property {function} sc.checkTab переключение вкладки смен
 *  @property {function} needSelection модальное окно с требованием выделения строки
 *  @property {function} selectRowByField выделение строчки по определенному столбцу
 *  @property {function} sc.gridEvDataListener листнер на событие изменение data в таблице
 *  @property {function} compare сравнение ячеек при сортировке по столбцу
 *  @property {function} selectFirstRow при переключении по вкладкам и обновлении даты - всегда выделяем верхние строки
 *  @property {function} keyDownGrid мультивыделение
 *  @property {function} keyUpGrid мультивыделение
 *  @property {function} sc.createTask создание сменного задания
 *  @property {function} sc.copyTaskRow коприрование сменного задания
 *  @property {function} sc.deleteTask удаление сменного задания (запрос)

 *  @property {function} deleateTasks удаление сменного задания (вырезка из массива таблицы)
 *  @property {function} sc.createWayBill создание путевого листа на основе сменного задания ( у роли 3 (Злодеев))
 *  @property {function} getPrintInfo получение информации для печати
 *  @property {function} sc.printTask выгрузка в Excel отчета
 *  @property {function} sc.updateSelectedCell модальное окно для выбора элемента в таблице сменных заданий
 *  @property {function} sc.editCell вызывается при редактировании ячейки
 *  @property {function} sc.mainCommentClick добавление важного комментария (только у роли Бачурина (2))
 *  @property {function} sc.copyTasksFromYesterday скопировать из предыдущего дня сменные задания (только у роли Бачурина (2))
 *  @property {function} sc.approveTask утверждение сменного задания (только у роли Бачурина (2))
 */

var app = angular.module("CreateShiftTask", ["ngGrid", "ui.bootstrap", "services", "modalWindows", 'Directives', "ui.utils", "LogusConfig"]);

app.controller("CreateShiftTaskCtrl", ["$rootScope", "$scope", "$http", "requests", "$location", '$cookieStore', '$rootScope', "modals", "modalType", "LogusConfig", "preloader", function ($rootScope, $scope, $http, requests, $location, $cookieStore, $rootScope, modals, modalType, LogusConfig, preloader) {
    var sc = $scope;
    sc.shifts = {};
    sc.editBtnVis = false;
    sc.createWayBtnVis = false;
    sc.disCreateWayBill = false;
	var firstLoad = false;
    sc.DayTaskId = "";
    sc.PrintShiftInfo1 = [];
    sc.PrintShiftInfo2 = [];
    sc.multi = false;
    sc.Comment = "";
    sc.dataTraktors = [];
    sc.dataEmployees = [];
	sc.mySelections1 =[];
	sc.infoData2 = [];
	sc.formData = [];
	var count = 0;
	var met = 1;
    var curUser = $cookieStore.get('currentUser');
    console.info(curUser);
    sc.tporgid = curUser.tporgid;
	sc.showDates = false;
	if (curUser.roleid != '4') {
		sc.showDates = true;
	}
	
	
	  if (curUser.roleid != '4') {
        sc.editBtnVis = true;
        sc.createWayBtnVis = false;
    } else if (curUser.roleid == '4') {
        sc.editBtnVis = false;
        sc.createWayBtnVis = true;
    }
	/*
	else if (curUser.roleid == '7') {
        sc.editBtnVis = false;
        sc.createWayBtnVis = false;
    }
    */
	
    if ($cookieStore.get('dateCalendar')) {
        if ($cookieStore.get('dateCalendar').date) sc.dateShift = $cookieStore.get('dateCalendar').date;
    }
    if ($cookieStore.get('shiftWay')) {
        if ($cookieStore.get('shiftWay').shift == '1') {
            sc.shifts = {shift2: false, shift1: true};
            sc.activeTab = '1';
        } else if ($cookieStore.get('shiftWay').shift == '2') {
            sc.shifts = {shift2: true, shift1: false};
            sc.activeTab = '2';

        }
    } else {
        sc.shifts = {shift1: false, shift2: true};
        sc.activeTab = '2';
    }
	
	sc.GetStyleShiftTask = function (Comment) {
		requests.serverCall.post($http, "DayTasksCtrl/GetStyleShiftTask",  {
			DateFilter: moment(sc.dateShift).format("MM.DD.YYYY"), Shift: sc.activeTab,Comment:Comment, FilterOrg : sc.formData.OrgId,
			checkRes : sc.AllResource
		}, function (data, status, headers, config) {
				    console.info(data);
				   sc.dataTraktors = data.Traktors;
				   sc.unusedTraktors = data.unsedTraktors;
				   
			       sc.dataEmployees = data.Employees;
				   sc.unsedEmployees = data.unsedEmployees;
				//    getDayTasks();
		        });
	 }
	 
    sc.tableDataFirst = [];
    sc.tableDataSecond = [];
    sc.selectedRow = [];

    sc.columnDefinitions = [
        {
            field: "PlantInfo", displayName: "Культура", width: "10%", cellClass: 'cellToolTip',
            cellTemplate: '<a tooltip="{{row.entity[col.field][\'Name\']}}" tooltip-popup-delay="1000" tooltip-class="my-custom-tooltip" tooltip-placement="top" tooltip-append-to-body="true">' +
            '<div class="ngCellText {{getClassCopyCell(row.entity.Status)}}" ng-class="col.colIndex()" ng-click="editCell(row.entity, row.getProperty(col.field), col.field)"">' +
            '<!--div data-ng-click="updateSelectedCell(col.field, row.entity[col.field][\'Id\'], \'Культура\',$event)">{{row.entity[col.field][\'Name\']}}</div-->' +
            '<!--<a tooltip="tool" tooltip-placement="right" ng-cell-text>--><input value="{{row.entity[col.field][\'Name\']}}" placeholder="Культура" style="border: none;{{row.entity[col.field][\'RepairStatus\']}}"  data-ng-click="updateSelectedCell(col.field, row.entity[col.field][\'Id\'], \'Культура\',row.entity,$event)" readonly>' +
            '<!--</a>--></div>' +
            '</a>'
        },
        {
            field: "FieldInfo", displayName: "№ поля", width: "7%",
            cellTemplate: '<a tooltip="{{row.entity[col.field][\'Name\']}}" tooltip-popup-delay="1000" tooltip-class="my-custom-tooltip" tooltip-placement="top" tooltip-append-to-body="true">' +
            '<div class="ngCellText {{getClassCopyCell(row.entity.Status)}}" ng-class="col.colIndex()" ng-click="editCell(row.entity, row.getProperty(col.field), col.field)">' +

            '<!--div data-ng-click="updateSelectedCell(col.field, row.entity[col.field][\'Id\'],\'№ поля\',$event)" >{{row.entity[col.field][\'Name\']}}</div-->' +
            '<input value="{{row.entity[col.field][\'Name\']}}" placeholder="№ поля" style="border: none;"  data-ng-click="updateSelectedCell(col.field, row.entity[col.field][\'Id\'],\'№ поля\',row.entity,$event)"  readonly>' +
            '</div>'
        },
        {
            field: "TypeTaskInfo", displayName: "Планируемые работы", width: "25%",
            cellTemplate: '<a tooltip="{{row.entity[col.field][\'Name\']}}" tooltip-popup-delay="1000" tooltip-class="my-custom-tooltip" tooltip-placement="top" tooltip-append-to-body="true">' +
            '<div class="ngCellText {{getClassCopyCell(row.entity.Status)}}" ng-class="col.colIndex()" ng-click="editCell(row.entity, row.getProperty(col.field), col.field)">' +
            '<!--div data-ng-click="updateSelectedCell(col.field, row.entity[col.field][\'Id\'],\'Планируемые работы\',$event)">{{row.entity[col.field][\'Name\']}}</div-->' +
            '<input value="{{row.entity[col.field][\'Name\']}}" placeholder="Планируемые работы" style="border: none;"  data-ng-click="updateSelectedCell(col.field, row.entity[col.field][\'Id\'],\'Планируемые работы\',row.entity,$event)" readonly>' +
            '</div>' +'</a>'
        },
        {
            field: "EquipmentsInfo", displayName: "Агрегат", width: "13%",
            cellTemplate: '<div class="ngCellText {{getClassCopyCell(row.entity.Status)}}" ng-class="col.colIndex()" ng-click="editCell(row.entity, row.getProperty(col.field), col.field)">' +
            '<!--div data-ng-click="updateSelectedCell(\'TracktorInfo\', row.entity[col.field][\'TracktorInfo\'][\'Id\'],\'Техника\', $event)" >{{row.entity[col.field][\'TracktorInfo\'][\'Name\']}} +</div-->' +
            '<a tooltip="{{row.entity[col.field][\'TracktorInfo\'][\'Name\']}}" tooltip-popup-delay="1000" tooltip-class="my-custom-tooltip" tooltip-placement="top" tooltip-append-to-body="true">' +
            '<input class="double-input" value="{{row.entity[col.field][\'TracktorInfo\'][\'Name\']}}" placeholder="Техника" style="border: none; {{row.entity[col.field][\'TracktorInfo\'][\'RepairStatus\']}}"  data-ng-click="updateSelectedCell(\'TracktorInfo\', row.entity[col.field][\'TracktorInfo\'][\'Id\'],\'Техника\',row.entity,$event)" readonly>' +
            '</a>' +
            '<!--div data-ng-click="updateSelectedCell(\'EquipInfo\', row.entity[col.field][\'EquipInfo\'][\'Id\'],\'Оборудование\', $event)">{{row.entity[col.field][\'EquipInfo\'][\'Name\']}}</div-->' +
            '<a tooltip="{{row.entity[col.field][\'EquipInfo\'][\'Name\']}}" tooltip-popup-delay="1000" tooltip-class="my-custom-tooltip" tooltip-placement="top" tooltip-append-to-body="true">' +
            '<input class="double-input" value="{{row.entity[col.field][\'EquipInfo\'][\'Name\']}}" placeholder="Оборудование" style="border: none;"  data-ng-click="updateSelectedCell(\'EquipInfo\', row.entity[col.field][\'EquipInfo\'][\'Id\'],\'Оборудование\',row.entity,$event)" readonly>' +
            '</a>' +
            '</div>'
        },
        {
            field: "AgroreqInfo", displayName: "Агротех.требования", width: "10%",
            cellTemplate: '<a tooltip="{{row.entity[col.field][\'Name\']}}" tooltip-popup-delay="1000" tooltip-class="my-custom-tooltip" tooltip-placement="top" tooltip-append-to-body="true">' +
            '<div class="ngCellText {{getClassCopyCell(row.entity.Status)}}" ng-class="col.colIndex()" ng-click="editCell(row.entity, row.getProperty(col.field), col.field)">' +
            '<input value="{{row.entity[col.field][\'Name\']}}" placeholder="Агротех.требования" style="border: none;"  data-ng-click="updateSelectedCell(col.field, row.entity[col.field][\'Id\'],\'Агротех.требования\',row.entity,$event)" readonly>' +
            '</div>' +
            '</a>'
        },
        {
            field: "EmployeesInfo", displayName: "Персонал", width: "12%",
            cellTemplate: '<div class="ngCellText {{getClassCopyCell(row.entity.Status)}}" ng-class="col.colIndex()" ng-click="editCell(row.entity, row.getProperty(col.field), col.field)">' +
            '<!--div data-ng-click="updateSelectedCell(\'Emp1\', row.entity[col.field][\'Emp1\'][\'Id\'],\'Водитель 1\',$event)" >{{row.entity[col.field][\'Emp1\'][\'Name\']}} /</div-->' +
            '<a tooltip="{{row.entity[col.field][\'Emp1\'][\'Name\']}}" tooltip-popup-delay="1000" tooltip-class="my-custom-tooltip" tooltip-placement="top" tooltip-append-to-body="true">' +
            '<input class="double-input" value="{{row.entity[col.field][\'Emp1\'][\'Name\']}}" placeholder="Водитель 1" style="border: none;{{row.entity[col.field][\'Emp1\'][\'RepairStatus\']}}"  data-ng-click="updateSelectedCell(\'Emp1\', row.entity[col.field][\'Emp1\'][\'Id\'],\'Водитель 1\',row.entity,$event)" readonly>' +
            '</a>' +
            '<!--div data-ng-click="updateSelectedCell(\'Emp2\', row.entity[col.field][\'Emp2\'][\'Id\'],\'Водитель 2\',$event)" >{{row.entity[col.field][\'Emp2\'][\'Name\']}}</div-->' +
            '<a tooltip="{{row.entity[col.field][\'Emp2\'][\'Name\']}}" tooltip-popup-delay="1000" tooltip-class="my-custom-tooltip" tooltip-placement="top" tooltip-append-to-body="true">' +
            '<input class="double-input" value="{{row.entity[col.field][\'Emp2\'][\'Name\']}}" placeholder="Водитель 2" style="border: none;{{row.entity[col.field][\'Emp2\'][\'RepairStatus\']}}"  data-ng-click="updateSelectedCell(\'Emp2\', row.entity[col.field][\'Emp2\'][\'Id\'],\'Водитель 2\',row.entity,$event)" readonly>' +
            '</a>' +
            ' </div>'
        },
        {
            field: "AvailableInfo", displayName: "Ответственный", width: "13%",
            cellTemplate: '<a tooltip="{{row.entity[col.field][\'Name\']}}" tooltip-popup-delay="1000" tooltip-class="my-custom-tooltip" tooltip-placement="top" tooltip-append-to-body="true">' +
            '<div class="ngCellText {{getClassCopyCell(row.entity.Status)}}" ng-class="col.colIndex()" ng-click="editCell(row.entity, row.getProperty(col.field), col.field)">' +
            '<!--div data-ng-click="updateSelectedCell(col.field, row.entity[col.field][\'Id\'],\'Ответственный\',$event)">{{row.entity[col.field][\'Name\']}}</div-->' +
            '<input value="{{row.entity[col.field][\'Name\']}}" placeholder="Ответственный" style="border: none;"  data-ng-click="updateSelectedCell(col.field, row.entity[col.field][\'Id\'],\'Ответственный\',row.entity,$event)"  readonly>' +
            '</div>' +
            '</a>'
        },
        {
            field: "Comment", displayName: " ", width: "5%",
            cellTemplate: 'templates/comment_shift_template.html', cellClass: 'cellToolTip'
        },
        {
            field: "Status", displayName: " ", width: "5%",
            cellTemplate: 'templates/status_shift_template.html'
        },
        {field: "DetailId", width: "0%"},
		{field: "CheckOrange", width: "0%"},
    ];

    // пока так, потом придумаю как убрать копипаст
    sc.gridFirstOptions = {
        data: 'tableDataFirst',
        enableColumnResize: true,
        enableRowSelection: true,
        showFooter: false,
        rowHeight: 49,
        headerRowHeight: 49,
        selectedItems: sc.selectedRow,
        columnDefs: sc.columnDefinitions,
        useExternalSorting: true,
        afterSelectionChange: function (row) {           			
			met = 1;
			for (var i = 0; i < row.length; i++ ) 
          { 
             sc.selectedRow.push(row[i].entity); 
          } 
			if (sc.mySelections1==row.entity) { 
		      	met=2;
			}
			sc.mySelections1 = row.entity;
            if (row.entity) {
                if (row.selected) {
                    if (sc.multi) sc.selectedRow.push(row.entity);
                    else {
				//	    console.log('clear');
                        sc.selectedRow = [];
                        sc.selectedRow.push(row.entity);
                    }
                } else {
                    sc.selectedRow.splice(sc.tableDataFirst.indexOf(row.entity), 1);
                }
			//	  console.log(row.entity.Status); 
                if (row.entity.Status == '3' || row.entity.Status == '' || row.entity.Status == '2' || row.entity.Status == '4'
				|| row.entity.Status == '0') 
				{
					sc.disCreateWayBill = true;
					}
                else {
				sc.disCreateWayBill = false;
				}
            }
			//  console.log(sc.selectedRow); 
        },
        beforeSelectionChange: function (row, event) {
            if (!sc.multi) {
                //console.info("beforeSelectionChange multi " + sc.multi);
                angular.forEach(sc.tableDataFirst, function (data, index) {
                    if (data.selected == true) sc.selectedRow.splice(index, 1);
                    sc.gridFirstOptions.selectRow(index, false);

                });
            }
            return true;
        }
    };

    sc.gridSecondOptions = {
        data: 'tableDataSecond',
        enableColumnResize: true,
        enableRowSelection: true,
        showFooter: false,
        rowHeight: 49,
        headerRowHeight: 49,
        selectedItems: sc.selectedRow,
        columnDefs: sc.columnDefinitions,
        useExternalSorting: true,
        afterSelectionChange: function (row, event) {
			  console.log('Статус:'+row.entity.Status);
			met = 1;
			for (var i = 0; i < row.length;i++ ) 
          { 
           sc.selectedRow.push(row[i].entity); 
          } 
			if (sc.mySelections1==row.entity) { 
			//console.log('есть');
			met = 2;			
			}
			sc.mySelections1 = row.entity;
            if (row.entity) {
                if (row.selected) {
                    if (sc.multi) {
                        if (row.selected) sc.selectedRow.push(row.entity);
                        else sc.selectedRow.splice(sc.selectedRow.indexOf(row.entity), 1)
                    }
                    else {
                        sc.selectedRow = [];
                        sc.selectedRow.push(row.entity);
                    }
                } else {
                    sc.selectedRow.splice(sc.tableDataSecond.indexOf(row.entity), 1);
                }
                if (row.entity.Status == '3' || row.entity.Status == '' || row.entity.Status == '2' || row.entity.Status == '4'
				|| row.entity.Status == '0') 
				{
					sc.disCreateWayBill = true;
				}
                else 
			    {sc.disCreateWayBill = false;}
            }
        },
        beforeSelectionChange: function (row, event) {
            if (!sc.multi) {
                angular.forEach(sc.tableDataSecond, function (data, index) {
                    if (data.selected == true) sc.selectedRow.splice(index, 1);
                    sc.gridSecondOptions.selectRow(index, false);
                });
            }
            return true;
        }
    };

    // стиль скопированной строки
    sc.getClassCopyCell = function (stat) {
        if (stat == '2') {
            return 'copy-cell';
        }
    };


	
	
    if (curUser.roleid == '4') {
        sc.gridFirstOptions.enableCellEdit = false;
        sc.gridSecondOptions.enableCellEdit = false;
    }
	    //ФИЛЬТР ОТДЕЛЕНИЯ
			sc.changeOrg = function () {
				if (firstLoad == true) {
			     getDayTasks();
				 	 sc.GetStyleShiftTask("FreeTechEmp");
				}
	    	}
				
			sc.checkRes = function () {
				 sc.GetStyleShiftTask("FreeTechEmp");
					}
				
				
		
    // получение data по двум сменам за определенную дату
    function getDayTasks() {
		
		if (firstLoad == false) {
			if (curUser.tporgid == 2) {
				sc.formData.OrgId = -1;
			}  else {
			sc.formData.OrgId = curUser.orgid;	
			}
		}	
        requests.serverCall.post($http, "DayTasksCtrl/GetDayTasks", {"DateFilter": moment(sc.dateShift).format("MM.DD.YYYY"), OrgId: curUser.orgid,FilterOrg: sc.formData.OrgId  }, function (data, status, headers, config) {
            console.info(data);
            sc.selectedRow = [];
            sc.DayTaskId = data.DayTaskId;
            sc.tableDataFirst = data.Shift1;
            sc.tableDataSecond = data.Shift2;
            sc.DayTaskId = data.DayTaskId;
            sc.Comment = data.Comment;
            if (!sc.infoData) {
                requests.serverCall.post($http, "DayTasksCtrl/GetInfoForCreateDayTasks", {OrgId: curUser.orgid, FilterOrg: sc.formData.OrgId}, function (data, status, headers, config) {
                    console.info(data);
                    sc.infoData = data;
					sc.formData.OrgId = sc.infoData.OrganizationsList[0].Id;
					firstLoad = true;
                }, function () {
                });
            }
            if ($cookieStore.get('selectedShiftTask')) {
        //        selectRowByField("DetailId", $cookieStore.get('selectedShiftTask').id);
                $cookieStore.remove('selectedShiftTask');
            } else selectFirstRow();
            getPrintInfo();
        }, function () {
        });
    }

    // отправка измененного задания на сервер
    function sendTask(task) {
        sc.dataTask = {};
        sc.dataTask.Date = moment(sc.dateShift).format("MM.DD.YYYY");
        sc.dataTask.ShiftInfo = sc.selectedRow[0];
		sc.dataTask.OrgId = curUser.orgid;
        console.log('SAVE sendTask selected ' + JSON.stringify(sc.dataTask));
        requests.serverCall.post($http, "DayTasksCtrl/UpdateShiftTask", sc.dataTask, function (data, status, headers, config) {
            console.info(data);
    //        selectRowByField("DetailId", data);
	//		getEmployees(); //для сокрытия используемой техники и персонала
	      sc.GetStyleShiftTask("FreeTechEmp");
        });
		
    }

    // изменение даты отображения сменных заданий
    sc.dateChangeShifts = function () {
        if (sc.dateShift) {
            $cookieStore.put('dateCalendar', {date: sc.dateShift});
            getDayTasks();
	//		getEmployees();
	   sc.GetStyleShiftTask("FreeTechEmp");
            var tomorrow = moment(sc.dateShift).add('days', 1)._d;
        } else {
            console.log('wrong date from datepick')
        }

    };
	   requests.serverCall.get($http, "DayTasksCtrl/GetBanDateWayBills", function (data, status, headers, config) {
        console.info(data);
		if (data!="bad") { 
        sc.ban_date = data;} else {var date = new Date; sc.ban_date="01.01."+date.getFullYear();}
    }, function () {
    });
	//ЗАПРЕТ РЕДАКТИРОВАНИЯ ПЛ
	sc.banEditWayBills = function () {
		count++;
		if (count>2) {
			
		 var ban_date1 = angular.copy(sc.ban_date);
    var ban_date2 =  moment(ban_date1).format("DD.MM.YYYY");	 
		console.log(ban_date2);
		console.log(sc.ban_date);
		 requests.serverCall.post($http, "DayTasksCtrl/SetBanDateWayBills", sc.ban_date, function (data, status, headers, config) {
          console.info(data);
	  });
		;}
		
		
	}

 	requests.serverCall.get($http, "DayTasksCtrl/GetBanDateTC", function (data, status, headers, config) {
        console.info(data);
		if (data!="bad") { 
        sc.tban_date = data;} else {var date = new Date; sc.tban_date="01.01."+date.getFullYear();}
    }, function () {
    });
	//ЗАПРЕТ РЕДАКТИРОВАНИЯ ТК
		sc.banEditTC = function () {
		count++;
		if (count>2) {
			
		 var tban_date1 = angular.copy(sc.tban_date);
     var tban_date2 =  moment(tban_date1).format("DD.MM.YYYY").toString();	 
		console.log(tban_date2);
		 requests.serverCall.post($http, "DayTasksCtrl/SetBanDateTC", sc.tban_date, function (data, status, headers, config) {
          console.info(data);
	  });
		;} 
		}

	$("#btn").show();
	
    // переключение вкладки смен
    sc.checkTab = function (tab) {
        sc.activeTab = tab;
        sc.selectedRow = [];
        if (!$cookieStore.get('selectedShiftTask'))selectFirstRow();
//		getEmployees();
   sc.GetStyleShiftTask("FreeTechEmp");
    };
    /*-----------------------------выделение строк----------------------------------*/
    // модальное окно с требованием выделения строки
    function needSelection(title, message) {
        modals.showWindow(modalType.WIN_MESSAGE, {
            title: title,
            message: message,
            callback: function () {
            }
        });
    }

    var _tasksArr, _gridOptions, _grid, _field, _val;
    // выделение строчки по определенному столбцу
    function selectRowByField(field, val) {
        console.info("selectRowByField " + field + ' ' + val);
        _field = field;
        _val = val;
        if (sc.activeTab == '1') {
            _tasksArr = sc.tableDataFirst;
            _gridOptions = sc.gridFirstOptions;
            _grid = sc.gridFirstOptions.ngGrid;
            //sc.tableDataFirst = sc.tableDataFirst;
        } else if (sc.activeTab == '2') {
            _tasksArr = sc.tableDataSecond;
            _gridOptions = sc.gridSecondOptions;
            _grid = sc.gridSecondOptions.ngGrid;
            //sc.tableDataSecond = sc.tableDataSecond;
        }
        sc.$on('ngGridEventData', sc.gridEvDataListener);
    }

    // листнер на событие изменение data в таблице
    sc.gridEvDataListener = function () {
        var offCallMeFn = $scope.$on("ngGridEventData", sc.gridEvDataListener);
        offCallMeFn();
        if (_val == "first" && _tasksArr.length > 0) _val = _tasksArr[0][_field];
        _tasksArr.forEach(function (item, i, arr) {
            if (item[_field] == _val) {
                _gridOptions.selectRow(i, true);	
                // console.info(_grid.rowMap[i] + '  ' + _grid.config.rowHeight);
                if (_grid) _grid.$viewport.scrollTop(_grid.rowMap[i] * _grid.config.rowHeight);
            } else {
                _gridOptions.selectRow(i, false);
            }
        });
        $(".ngViewport").focus();
    };

    var _sortedField;
    // событие на сортировку по столбцу
    sc.$on("ngGridEventSorted", function (e, field) {
            _sortedField = field;
            if (sc.activeTab == "1") {
                console.info(sc.tableDataFirst);
                var copyFirst = sc.tableDataFirst.sort(compare);
                sc.tableDataFirst = [];
                for (var t = 0, tl = copyFirst.length; t < tl; t++) {
                    sc.tableDataFirst.push(copyFirst[t]);
                }

            } else if (sc.activeTab == "2") {
                console.info(sc.tableDataSecond);
                var copySecond = sc.tableDataSecond.sort(compare);
                sc.tableDataSecond = [];
                for (var t = 0, tl = copySecond.length; t < tl; t++) {
                    // sc.tableDataSecond.splice(t, 0, copy1[t]);
                    sc.tableDataSecond.push(copySecond[t]);
                }
            }
      //      for (var d = 0, dl = sc.selectedRow.length; d < dl; d++) {
      //          selectRowByField("DetailId", sc.selectedRow[d]["DetailId"]);
       //    }

            if (!sc.$$phase) {
                sc.$apply(function () {
                });
            }
        }
    );

    // сравнение ячеек при сортировке по столбцу
    function compare(a, b) {
        var first, second;
        switch (_sortedField.field) {
            case "PlantInfo":
            case "FieldInfo":
            case "TypeTaskInfo":
            case "AvailableInfo":
                first = a[_sortedField.field]["Name"];
                second = b[_sortedField.field]["Name"];
                break;
            case "EquipmentsInfo":
                first = a[_sortedField.field]["TracktorInfo"]["Name"];
                second = b[_sortedField.field]["TracktorInfo"]["Name"];
                break;
            case "EmployeesInfo":
                first = a[_sortedField.field]["Emp1"]["Name"];
                second = b[_sortedField.field]["Emp1"]["Name"];
                break;
        }
        if (first == null && second == null)
            return 0;
        if (first == null && second != null)
            return _sortedField.sortDirection == "desc" ? 1 : -1;
        if (first != null && second == null)
            return _sortedField.sortDirection == "desc" ? -1 : 1;
        if (first < second)
            return _sortedField.sortDirection == "desc" ? -1 : 1;
        if (first > second)
            return _sortedField.sortDirection == "desc" ? 1 : -1;
        return 0;
    }


    //при переключении по вкладкам и обновлении даты - всегда выделяем верхние строки
    function selectFirstRow() {
        // пока такой костыль
        if (sc.activeTab == '1') {
            var copy = angular.copy(sc.tableDataFirst);
            sc.tableDataFirst = [];
            sc.tableDataFirst = copy;
        } else if (sc.activeTab == '2') {
            var copy1 = angular.copy(sc.tableDataSecond);
            sc.tableDataSecond = [];
            sc.tableDataSecond = copy1;
        }
    //    selectRowByField("DetailId", "first");
    }

    // мультивыделение
    sc.keyDownGrid = function (e) {
        if (curUser.roleid != '4') {
            if ((e.keyCode === 16 || e.keyCode === 17 || e.keyCode === 18 || e.keyCode === 91) && !sc.multi) {
              //  console.info("__________________ keyDown  " + e.keyCode);
                sc.multi = true;
             //   console.info(sc.multi);
            }
        }
    };

    sc.keyUpGrid = function (e) {
        // 16 - Shift, 17 - Ctrl, 18 - Alt, 91 - Win key (Start)
        if (curUser.roleid != '4') {
            if (e.keyCode === 16 || e.keyCode === 17 || e.keyCode === 18 || e.keyCode === 91) {
               // console.info("_________________ keyUp " + e.keyCode);
                sc.multi = false;
              //  console.info(sc.multi);
            }
        }
    };

    /*-----------------------------end выделение строк------------------------------*/

    /*-----------------------------верхние кнопки----------------------------------*/
    //создание сменного задания
    sc.createTask = function () {
        console.info(sc.selectedRow);
        var newTask = {
            MainComment: null,
            AvailableInfo: {Id: null, Name: null},
            DetailId: null,
            EmployeesInfo: {Emp1: {Id: null, Name: null}, Emp2: {Id: null, Name: null}},
            EquipmentsInfo: {EquipInfo: {Id: null, Name: null}, TracktorInfo: {Id: null, Name: null}},
            FieldInfo: {Id: null, Name: null},
            PlantInfo: {Id: null, Name: null},
            Shift: sc.activeTab,
            Comment: null,
            Status: null,
            TypeTaskInfo: {Id: null, Name: null}
        };
        newTask.Shift = sc.activeTab;

        sc.dataTask = {};
        sc.dataTask.Date = moment(sc.dateShift).format("MM.DD.YYYY");
        sc.dataTask.ShiftInfo = newTask;
        sc.dataTask.OrgId = curUser.orgid;
        requests.serverCall.post($http, "DayTasksCtrl/UpdateShiftTask", sc.dataTask, function (data, status, headers, config) {
            console.info(data);
            newTask["DetailId"] = data;
            newTask["Status"] = 0;
      //      selectRowByField("DetailId", data);
            if (sc.activeTab == '1') {
                if (sc.selectedRow.length > 0) {
                    for (var j = 0, jl = sc.tableDataFirst.length; j < jl; j++) {
                        if (sc.selectedRow[0] == sc.tableDataFirst[j]) {
                            sc.tableDataFirst.splice(j, 0, newTask);
                            j = jl;
                        }
                    }
                } else sc.tableDataFirst.splice(0, 0, newTask);
            } else if (sc.activeTab == '2') {
      //          selectRowByField("DetailId", data);
                if (sc.selectedRow.length > 0) {
                    for (var i = 0, il = sc.tableDataSecond.length; i < il; i++) {
                        if (sc.selectedRow[0] == sc.tableDataSecond[i]) {
                            sc.tableDataSecond.splice(i, 0, newTask);
                            i = il;
                        }
                    }
                } else sc.tableDataSecond.splice(0, 0, newTask);
            }

        });
    };

    // коприрование сменного задания
    sc.copyTaskRow = function () {
        if (sc.selectedRow.length > 0) {
            var detailIdArr = [];
            for (var i = 0, il = sc.selectedRow.length; i < il; i++) {
                detailIdArr.push(sc.selectedRow[i]["DetailId"]);
            }
            console.info(detailIdArr);
            requests.serverCall.post($http, "DayTasksCtrl/CopyLocalShiftTask", detailIdArr, function (data, status, headers, config) {
                console.info(data);
                //console.info(sc.tableDataSecond);
                if (data.length === 1) {
                    var copyTask = angular.copy(sc.selectedRow[0]);
/*----------------- Обнуление столбцов в скопированной таблице ---------------------*/
                    copyTask.EmployeesInfo.Emp1.Id = null;
                    copyTask.EmployeesInfo.Emp1.Name = null;
                    copyTask.EmployeesInfo.Emp2.Id = null;
                    copyTask.EmployeesInfo.Emp2.Name = null;
                    copyTask.EmployeesInfo.Emp2.Id = null;
                    copyTask.EquipmentsInfo.TracktorInfo.Id = null;
                    copyTask.EquipmentsInfo.TracktorInfo.Name = null;
/*--------------------------------------------------------------------------------------*/					
                    copyTask["MainComment"] = null;
                    copyTask["DetailId"] = data[0];
                    copyTask["Status"] = '2';
                    copyTask["Comment"] = null;
                    if (sc.activeTab == '1') {
                        selectRowByField("DetailId", copyTask["DetailId"]);
                        console.info("выделяем после копирования " + copyTask["DetailId"]);
                        for (var j = 0, jl = sc.tableDataFirst.length; j < jl; j++) {
                            if (sc.selectedRow[0] == sc.tableDataFirst[j]) {
                                sc.tableDataFirst.splice(j, 0, copyTask);
                                j = jl;
                            }
                        }
                    } else if (sc.activeTab == '2') {
                       selectRowByField("DetailId", copyTask["DetailId"]);
                        console.info("выделяем после копирования " + copyTask["DetailId"]);
                        for (var i = 0, il = sc.tableDataSecond.length; i < il; i++) {
                            if (sc.selectedRow[0] == sc.tableDataSecond[i]) {
                                sc.tableDataSecond.splice(i, 0, copyTask);
                                i = il;
                            }
                        }

                    }
                } else {
                    for (var j = 0, jl = data.length; j < jl; j++) {
                        var copyTask = angular.copy(sc.selectedRow[j]);
                        copyTask["MainComment"] = null;
                        copyTask["DetailId"] = data[j];
                        copyTask["Status"] = '2';
                        copyTask["Comment"] = null;
                        if (sc.activeTab == '1') {
                          selectRowByField("DetailId", copyTask["DetailId"]);
                            console.info("выделяем после копирования " + copyTask["DetailId"]);
                            sc.tableDataFirst.splice(0, 0, copyTask);
                        } else if (sc.activeTab == '2') {
                            selectRowByField("DetailId", copyTask["DetailId"]);
                            console.info("выделяем после копирования " + copyTask["DetailId"]);
                            sc.tableDataSecond.splice(0, 0, copyTask);

                        }
                    }
                }

            });

            
        } else needSelection('Копирование', "Для копирования задания необходимо выбрать задание");
    };


    // удаление сменного задания (запрос)
    sc.deleteTask = function () {
        if (sc.selectedRow.length > 0) {
            var detailIdArr = [];
            for (var i = 0, il = sc.selectedRow.length; i < il; i++) {
                detailIdArr.push(sc.selectedRow[i]["DetailId"]);
            }
            console.info(detailIdArr);
            modals.showWindow(modalType.WIN_DIALOG, {
                    title: "Удаление", message: "Удалить выбранное задание?", callback: function () {
                        requests.serverCall.post($http, "DayTasksCtrl/DeleteDayTask", detailIdArr, function (data, status, headers, config) {
                            console.info(data);
                            deleateTasks(detailIdArr);
				//			getEmployees();//для сокрытия используемой техники и персонала
				   sc.GetStyleShiftTask("FreeTechEmp");
                        }, function () {
                            deleateTasks(detailIdArr);
				//			getEmployees();//для сокрытия используемой техники и персонала
				   sc.GetStyleShiftTask("FreeTechEmp");
                        });
                    }

                }
            );
        } else needSelection("Удаление", "Для удаления необходимо выбрать задание");

    };

    // удаление сменного задания (вырезка из массива таблицы)
    function deleateTasks(idArr) {
        var tableData = [];
        if (sc.activeTab == '1') tableData = sc.tableDataFirst;
        else if (sc.activeTab == '2') tableData = sc.tableDataSecond;
        for (var j = 0, jl = idArr.length; j < jl; j++) {
            tableData.forEach(function (item, d) {
                if (idArr[j] == item["DetailId"] && item["Status"] != 3) {
                    tableData.splice(d, 1);
                    if (tableData.length > 0) {
                        if (d - 1 > 0)selectRowByField("DetailId", tableData[d - 1]["DetailId"]);
                        else selectRowByField("DetailId", tableData[0]["DetailId"]);
                    } else sc.selectedRow = [];
                }
            })
        }

    }

    // создание путевого листа на основе сменного задания ( у роли 3)
    sc.createWayBill = function () {
        $cookieStore.put('shiftWay', {shift: sc.activeTab});
        if (sc.selectedRow[0] && sc.selectedRow[0]["DetailId"]) {
            requests.serverCall.post($http, "DayTasksCtrl/IsDriverWayBillByShiftTaskId", sc.selectedRow[0]["DetailId"], function (data, status, headers, config) {
                console.log(data);
                if (data != null) {
                    if (data === true) {
                        $rootScope.CreateWayBillOnShiftTaskId = sc.selectedRow[0]["DetailId"];
                        $rootScope.activeTab = 3;
                        $location.path('/driver_way_bill_item/');
                    } else {
                        $rootScope.activeTab = 2;
                        $location.path('/edit_bill/fromtask/' + sc.selectedRow[0]["DetailId"]);
                    }
                } else {
                    modals.showWindow(modalType.WIN_ERROR, {
                        title: "Ошибка создания путевого листа",
                        message: "Невозможно создать путевой лист для этого сменного задания",
                        callback: function () {
                        }
                    });
                }
            });
        }
    };

    function getPrintInfo() {
        sc.PrintShiftInfo1 = [];
        sc.PrintShiftInfo2 = [];
        requests.serverCall.post($http, "DayTasksCtrl/GetPrintShiftTasks", sc.DayTaskId, function (data, status, headers, config) {
            sc.DateShifts = data.DateShifts;
            sc.PrintShiftInfo1 = data.ShiftInfo1;
            sc.PrintShiftInfo2 = data.ShiftInfo2;
        });
    }

    // выгрузка в Excel отчета
    sc.printTask = function () {
        var popupWin = window.open(LogusConfig.serverUrl + 'WaysListsCtrl/Report?id=' + sc.DayTaskId, '_blank');
        requests.serverCall.get($http, "WaysListsCtrl/Report?id=" + sc.DayTaskId, function (data, status, headers, config) {
            console.info(data);
        });
        /*if (typeof sc.DayTaskId != 'undefined' && sc.DayTaskId != 0 && (sc.PrintShiftInfo1.length > 0 || sc.PrintShiftInfo2.length > 0)) {
         var printContents = document.getElementById('print').innerHTML;
         var popupWin = window.open('', '_blank');
         if (typeof popupWin != 'undefined') {
         popupWin.document.write('<html><head><style type="text/css"> body { width:297mm; height:210mm; }</style></head><body >' + printContents + '</html>');
         popupWin.print();
         popupWin.close();
         }
         }*/
    };
    /*----------------------------- end верхние кнопки----------------------------------*/
    /*--------- модальное окно для выбора элемента в таблице сменных заданий ----------------------------*/

    // измениние значения в ячейке (только у роли Бачурина (2))
    sc.updateSelectedCell = function (colField, selectedVal, displayName, row) {
	
		met=1;
	 if (curUser.roleid != '4') { 
            if (!sc.multi) {
				  console.log('clear');
                sc.selectedRow = [];
                sc.selectedRow.push(row);
            }
		 	if (sc.mySelections1==row) {met=2;}
			console.log(row);
			sc.mySelections1 = row;
			console.log(sc.mySelections1);
            var typeModal = modalType.WIN_SELECT_SHIFT;
            var modalValues;
            var selectedColumn;		   
            if (sc.infoData) {
				if (met == 2) {				
				//	/*
				requests.serverCall.post($http, "DayTasksCtrl/GetStyleShiftTask",  {
		    	DateFilter: moment(sc.dateShift).format("MM.DD.YYYY"), Shift: sc.activeTab, DetailId : sc.selectedRow[0].DetailId, Comment:colField, FilterOrg : sc.formData.OrgId,
				checkRes : sc.AllResource,
	        	}, function (data, status, headers, config) {
				    console.info(data);
					if (data.TypeTasks) {
                   sc.infoData2.TasksList = data.TypeTasks;
					}
						if (data.Traktors) {
				   sc.dataTraktors = data.Traktors;
				   sc.unusedTraktors = data.unsedTraktors;
						}
					if (data.Employees) {
			        sc.dataEmployees = data.Employees;
					sc.unsedEmployees = data.unsedEmployees;
				    sc.infoData.ChiefsList = data.ChiefsList;
							}
					if (data.EquipmnetsList) {
					sc.infoData.EquipmnetsList = data.EquipmnetsList;
							}
							
					sc.infoData.FieldsList = data.FieldsList;
					sc.infoData.AllFields = data.AllFields;
                    sc.infoData.FieldsAndPlants = data.FieldsAndPlants;					
				//	*/
                switch (colField) {
                    case "PlantInfo":
					console.log(sc.infoData.FieldsAndPlants);
                        modalValues = sc.infoData.FieldsAndPlants;
                        break;
                    case "FieldInfo":
                        if (sc.selectedRow[0]["PlantInfo"]) {
                            if (sc.selectedRow[0]["PlantInfo"]["Id"]) {
                                console.info(sc.selectedRow[0]["PlantInfo"]["Id"]);
                                for (var i = 0, il = sc.infoData.FieldsAndPlants.length; i < il; i++) {
                                    if (sc.infoData.FieldsAndPlants[i].Id == sc.selectedRow[0]["PlantInfo"]["Id"]) {
                                        modalValues = sc.infoData.FieldsAndPlants[i].Values;
                                        //typeModal = modalType.WIN_SELECT_MULTI;
                                    }
                                }
                            } else
								
								{ modalValues = sc.infoData.ObjectList;
								//вывод всех полей 
				                 modalValues = sc.infoData.AllFields;
           
								//
								}
                        }
                        break;
                     case "TypeTaskInfo":
                      modalValues = sc.infoData2.TasksList;
                        break;
                    case "TracktorInfo":
                        selectedColumn = 'EquipmentsInfo';
                        modalValues = sc.dataTraktors; 
                        break;
                    case "EquipInfo":
                        selectedColumn = 'EquipmentsInfo';
                        modalValues = sc.infoData.EquipmnetsList;
                        break;
                    case "Emp1":
                        selectedColumn = 'EmployeesInfo';
                        modalValues = sc.dataEmployees; 
                        break;
                    case "Emp2":
                        selectedColumn = 'EmployeesInfo';
                        modalValues = sc.dataEmployees; 
                        break;
                    case "AvailableInfo":
                        modalValues = sc.infoData.ChiefsList;
                        break;
                    case "AgroreqInfo":
                        modalValues = sc.infoData.AgroreqList;
                        break;
                    default:
                        console.log('updateSelectedCell: Не пришел colField ' + colField);
                }

                modals.showWindow(typeModal, {
                    nameField: displayName,
                    values: modalValues,
                    selectedValue: selectedVal
                })
                    .result.then(function (result) {
                        console.info(result);
						 console.info(row.FieldInfo);
						 var cult = 0;
						  console.info(colField);
						   console.info(row);

						 if (colField == 'PlantInfo' && row.FieldInfo!=null && row.FieldInfo!=undefined && result.Values!=undefined)
						 {
							 console.info('в');
							 
							 for (var i=0;i<result.Values.length;i++)
							 {
								 if (result.Values[i].Id==row.FieldInfo.Id) {cult=1;}
							 }
							 if (cult==0) {row.FieldInfo = {Id : null, Name : null} ;   console.info('ноль');}
						 }
						 
					if (colField == 'FieldInfo') {
						console.log(result);
						}
						
						//подбор сотрудника по технике
						if (colField == 'TracktorInfo') {
							if (result.emp_emp_id != null) {
							row.EmployeesInfo.Emp1 = {Id:result.emp_emp_id, Name:result.Description};
					         }
							console.log(result);
							console.log(row);
						}

                        if (colField == 'EquipInfo' || colField == 'TracktorInfo' || colField == 'Emp1' || colField == 'Emp2') {
                            sc.selectedRow[0][selectedColumn][colField] = result;
                        } else {
                            sc.selectedRow[0][colField] = result;
                        }
                        if (sc.colField == 'PlantInfo') sc.selectedRow[0]['FieldInfo'] = '';
                        sendTask();
                    }, function () {
                        console.info('Modal dismissed at: ' + new Date());
                    });
				//
				});	
			}  	
            }
        }
             //
		
    };
    /*----------------------- end модальное окно для выбора элемента в таблице сменных заданий ------------------*/
    // вызывается при редактировании ячейки
    sc.editCell = function (row, cell, column) {
			met=1;
			 if (!sc.multi) {
			console.log('clear');
        sc.selectedRow = [];
        sc.selectedRow.push[row]; 
			 }
        sc.selectedCell = cell;
        sc.selectedColumn = column;
		
		if (sc.mySelections1==row) {met=2;
		}
			  console.log(row);
			sc.mySelections1 = row;
			//console.log(sc.mySelections1);
		
			if (met==2) {
        if (column == 'Comment' && curUser.roleid != '4') commentClick(row.Comment);
			}
        if (column == 'MainComment' && curUser.roleid != '4') mainCommentClick(cell);
		
    };

    //добавление комментария (только у роли Бачурина (2))
    function commentClick(comment) {
        console.info('comment');
        if (curUser.roleid != '4') {
            modals.showWindow(modalType.WIN_COMMENT, {
                nameField: "Комментарий",
                comment: comment
            })
                .result.then(function (result) {
                    console.info(result);
                    sc.selectedRow[0]['Comment'] = result;
                    sendTask();
                }, function () {
                    console.info('Modal dismissed at: ' + new Date());
                });
        }
    }
    //добавление важного комментария (только у роли Бачурина (2))
    sc.mainCommentClick = function () {
    //function mainCommentClick(comment) {
        //console.info(comment);
        if (curUser.roleid != '4') {
            modals.showWindow(modalType.WIN_COMMENT, {
                nameField: "Важно",
                title:"Важно",
                comment: sc.Comment
            })
                .result.then(function (result) {
                    console.info(result);
                    sc.Comment = result;
                     requests.serverCall.post($http, "DayTasksCtrl/UpdateCommentDayTask", {
                     DateFilter: moment(sc.dateShift).format("MM.DD.YYYY"),
                     Comment : result,
					 OrgId : curUser.orgid,
                     }, function (data, status, headers, config) {
                     });
                }, function () {
                    console.info('Modal dismissed at: ' + new Date());
                });
        }
    };

    /* --------------------------------нижние кнопки-----------------------------------------------*/
    // скопировать из предыдущего дня сменные задания (только у роли Бачурина (2))
    sc.copyTasksFromYesterday = function () {
        var dateCopy = moment(moment(sc.dateShift).add('days', -1)._d).format("DD.MM.YYYY");
        modals.showWindow(modalType.WIN_DIALOG, {
            title: "Копирование",
            message: "Вы уверены, что хотите скопировать задания с " + dateCopy + " ?",
            callback: function () {
                requests.serverCall.post($http, "DayTasksCtrl/CopyDayTasksShift", {
                    Date: moment(sc.dateShift).format("MM.DD.YYYY"),
                    Shift: sc.activeTab,
					FilterOrg: sc.formData.OrgId,
					OrgId :  curUser.orgid,
                }, function (data, status, headers, config) {
                    console.info(data);
                    if (data == '"нет данных"') {
                        modals.showWindow(modalType.WIN_MESSAGE, {
                            title: "Копирование заданий из предыдущего дня",
                            message: "За предыдущий день не было создано заданий",
                            callback: function () {
                            }
                        });
                    }
                    else getDayTasks();
                });
            }
        });
    };

    // утверждение сменного задания (только у роли Бачурина (2))
sc.approveTask = function () {
        sc.dataTasks = {};
        sc.chackTask = {};
		console.log(sc.formData.OrgId);
        sc.dataTasks.Date = moment(sc.dateShift).format("MM.DD.YYYY");
        if (sc.activeTab == '1') {sc.dataTasks.ShiftInfo = sc.tableDataFirst;
            sc.chackTask = sc.tableDataFirst.EquipmentsInfo;
        }
        else if (sc.activeTab == '2') {sc.dataTasks.ShiftInfo = sc.tableDataSecond;
                sc.chackTask = sc.tableDataFirst.EquipmentsInfo;
        }
        
        var chack = 0;
        console.log(sc.dataTasks.ShiftInfo);
        for (var i = 0; i < sc.dataTasks.ShiftInfo.length; i++){
            if(sc.dataTasks.ShiftInfo[i].EquipmentsInfo.TracktorInfo.RepairStatus == "color: red;")
            {
                chack = 1;
            }

        }
          if(chack == '1')    {
            
            modals.showWindow(modalType.WIN_DIALOG, {
                title: "Утверждение сменного задания",
                message: "В сменном задании присутствует техника находящиеся на ремонте. Желаете утвердить задания?",
                callback:   function () {
                                preloader.show();
                                requests.serverCall.post($http, "DayTasksCtrl/ApproveDayShiftTask", {
                                Date: moment(sc.dateShift).format("MM.DD.YYYY"),
                                Shift: sc.activeTab,
								FilterOrg: sc.formData.OrgId,
                            }, function (data, status, headers, config) {
                                    console.info(data);
                                    preloader.hide();
                                     modals.showWindow(modalType.WIN_MESSAGE, {
                                     title: "Утверждение сменного задания",
                                     message: "Сменное задание утверждено",
                                     callback:   function () {
                                                                getDayTasks();
                               //                                 getEmployees();
							                sc.GetStyleShiftTask("FreeTechEmp");
                                                             }
                            });
                    });

                }
            });
        }
        else {
            preloader.show();
            requests.serverCall.post($http, "DayTasksCtrl/ApproveDayShiftTask", {
            Date: moment(sc.dateShift).format("MM.DD.YYYY"),
            Shift: sc.activeTab,
				FilterOrg: sc.formData.OrgId,
            }, function (data, status, headers, config) {
                console.info(data);
                preloader.hide();
                modals.showWindow(modalType.WIN_MESSAGE, {
                title: "Утверждение сменного задания",
                message: "Сменное задание утверждено",
                callback: 	function () {
								getDayTasks();
		//						getEmployees();
		   sc.GetStyleShiftTask("FreeTechEmp");
							}
            });
        });
    }

    };
     /*
	function getEmployees () {
		requests.serverCall.post($http, "DayTasksCtrl/GetBalanceTraktorsInDayTask", {
			DateFilter: moment(sc.dateShift).format("MM.DD.YYYY"), Shift: sc.activeTab,
		}, function (data, status, headers, config) {
				console.info(data);
				sc.dataTraktors = data.Traktors;
				sc.dataEmployees = data.Employees;
		});
	}
   */
   /*
    //аудитор
	function func() {
   if (curUser.roleid == '12') {
	$( ".calendar").unbind( "click" );
	console.log('123');
    }
	}
	setTimeout(func, 100);
	*/
    // просмотреть неиспользуемую технику (только у роли Бачурина (2))
	/*
        requests.serverCall.post($http, "DayTasksCtrl/GetBalanceTraktorsInDayTask", {
            DateFilter: moment(sc.dateShift).format("MM.DD.YYYY"),
        }, function (data, status, headers, config) {
            console.info(data);
          //  sc.dataTraktors = data.Traktors;
         //   sc.dataEmployees = data.Employees;
        }, function () {
        });
		*/
    /* -------------------------------- end нижние кнопки-----------------------------------------------*/
}
]);