/**
 *  @ngdoc controller
 *  @name ShiftTask.contorller:CreateTaskCtrl
 *  @description
 *  # CreateTaskCtrl
 *  Не используется
 */
var app = angular.module("CreateTask", ["ngGrid","ui.bootstrap","services"]);

app.controller("CreateTaskCtrl", ["$rootScope","$scope", "$http","requests", "$location", '$cookieStore','$rootScope','$stateParams', function ($rootScope, $scope, $http, requests, $location, $cookieStore,$rootScope, $stateParams) {
    var sc = $scope;
    sc.fullLoads = true;
    sc.formData = {};
    sc.formData.ShiftNum = '';
    sc.formData.Date = '';

    sc.formData.ShiftNum = $stateParams.task_shift;
    sc.formData.Date = moment($stateParams.task_date).format("MM.DD.YYYY");

    console.info(sc.formData.ShiftNum);
    sc.taskInfo = moment($stateParams.task_date).format('DD.MM.YYYY')+' '+sc.formData.ShiftNum;

    requests.serverCall.post($http, "DayTasksCtrl/GetInfoForCreateDayTasks", {}, function (data, status, headers, config) {
        console.info(data);
        sc.fullLoads = false;
        sc.infoData = data;
    }, function () { sc.fullLoads = false; });

    sc.submitForm = function () {
        sc.fullLoads = true;
        requests.serverCall.post($http, "DayTasksCtrl/CreateDayTasksDetails", sc.formData, function (data, status, headers, config) {
            console.info(data);
            sc.fullLoads = false;
            $location.path('/shift_task');
        }, function () { sc.fullLoads = false; });
    }

    sc.canselWayBillClick = function(){
        $location.path('/shift_task');
    }
}]);

app.controller("EditTaskCtrl", ["$rootScope","$scope", "$http","requests", "$location", '$cookieStore','$rootScope','$stateParams', function ($rootScope, $scope, $http, requests, $location, $cookieStore,$rootScope, $stateParams) {
    var sc = $scope;
    sc.fullLoads = true;
    sc.formData = {};
    sc.formData.DetailId = '';

    requests.serverCall.post($http, "DayTasksCtrl/GetInfoForCreateDayTasks", {}, function (data, status, headers, config) {
        sc.fullLoads = true;
        console.info(data);
        sc.infoData = data;
        if($stateParams.detail_id){
            requests.serverCall.post($http, "DayTasksCtrl/GetDayTasksDetailsById", $stateParams.detail_id, function (data, status, headers, config) {
                console.info(data);
                sc.formData = data;
                sc.taskInfo = moment(sc.formData.Date).format('DD.MM.YYYY')+' '+sc.formData.ShiftNum;
                sc.fullLoads = false;
            },
            function () {
                sc.fullLoads = false;
            });
        }else{
            console.log('detail_id is null or undefined')
        }
    });

    sc.submitForm = function () {
        sc.fullLoads = true;
        sc.formData.DetailId = $stateParams.detail_id;
        sc.formData.Date =  moment(sc.formData.Date).format("MM.DD.YYYY");
        console.info(sc.formData);
        requests.serverCall.post($http, "DayTasksCtrl/UpdateDayTasksDetails", sc.formData, function (data, status, headers, config) {
            console.info(data);
            sc.fullLoads = false;
            $location.path('/shift_task');
        }, function () { sc.fullLoads = false;
						 getEmployees(); //для сокрытия используемой техники и персонала
		});
    }

    sc.canselWayBillClick = function(){
        $location.path('/shift_task');
    }
}])