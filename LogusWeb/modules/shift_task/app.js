'use strict';

                (function () {

// declare modules
    angular.module('LogusConfig', []);
    angular.module('Authentication', []);
    angular.module('Directives', []);
    angular.module('WayBillsList', []);
    angular.module('CreateShiftTask', []);
	angular.module('ShiftTaskRapairs', []);
    angular.module('CreateTask', []);
//angular.module('PrintWayBill',[]);
    angular.module('CloseWayBill', []);
    angular.module('CloseAddEditTask', []);
    angular.module('DriverWayBillsList', []);
    angular.module('DriverWayBillItem', []);
    angular.module('CloseDriverWayBill', []);
    angular.module('CloseDriverWayBillTask', []);
    angular.module('DocumentsList', []);
    angular.module('DocumentsItem', []);
    angular.module('SeedingList', []);
    angular.module('SeedingItem', []);
    angular.module('ActsList', []);
	angular.module('ActsList1', []);
    angular.module('ActsItem', []);
	angular.module('SparePartsItem', []);
	angular.module('MovementSpareParts', []);
	angular.module('ReserveSpareParts', []);
    angular.module('WriteOffSparePartsItem', []);
	angular.module('Administration', []);  
	angular.module('Roles', []);  
	angular.module('SectionTechcard', []); 
	angular.module('DemandWayBill', []); 
	angular.module('DemandWayBillItem', []);
    angular.module('IncomingList', []);
	  angular.module('MovementItem', []);
    angular.module('IncomingFuelList', []);
    angular.module('Techcard', []);
    angular.module('TCList', []);
	angular.module('TCTempList', []);
    angular.module('Main', []);
    angular.module('Cost', []);
    angular.module('Culture', []);
    angular.module('Map', []);
    angular.module('LeftPanel', []);
	angular.module('RightPanel', []);
    angular.module('Scheme', []);
    angular.module('Dictionary', []);
    angular.module('Product', []);
	angular.module('Service', []);
	angular.module('Crater', []);
    angular.module('Sales', []);
    angular.module('Gross', []);
    angular.module('TechPlan', []);
	angular.module('TMCPlan', []);
	angular.module('TechcardTemp', []);
    angular.module('KPI', []);
    angular.module('DoubleList', []);
    angular.module('Rates', []);
    angular.module('RateFuel', []);
    angular.module('BalanceMaterial', []);
    angular.module('BalanceSpareParts', []);
	angular.module('ComingSpareParts', []);
	angular.module('HistorySpareParts', []);
	angular.module('WriteOffSpareParts', []);
    angular.module('ComingInvoiceSpareParts', []); 
	angular.module('ComingTmc', []); 
	angular.module('MovementTmc', []); 
	angular.module('ComingInvoiceSparePartsItem', []); 
	angular.module('WriteOffActs', []); 
	angular.module('WriteOffActsItem', []); 
	angular.module('RepairsReportsCtrl', []);
	angular.module('RepairsSparePartsCtrl', []); 
	angular.module('RepairsDailyCtrl', []);
	angular.module('DailyRepairsItem', []);
	angular.module('ParamOrganization', []);
	angular.module('ControlTech', []);
    angular.module('WayBills', [
        'LogusConfig',
        'Authentication',
        'WayBillsOperation',
        'CreateShiftTask',
        'CreateTask',
        'CloseWayBill',
        /* 'PrintWayBill',*/
        'CloseAddEditTask',
        'Main',
        'ngRoute',
        'ui.router',
        'ngCookies'
    ]);
    var app = angular.module('wayBills', [ 'ngTouch', 'ui.grid', 'ui.grid.edit', 'ui.grid.cellNav', 'ui.grid.selection', "ui.router", "ui.bootstrap", "ngGrid", "ui.utils", 'ngRoute', 'ngCookies', 
        'LogusConfig', 'Directives', "Authentication", "WayBillsList", "WayBillsEdit", "CreateShiftTask",'ShiftTaskRapairs', "CreateTask", "CloseWayBill", 'CloseAddEditTask', 'DriverWayBillsList', 'DriverWayBillItem',
        'CloseDriverWayBill', 'CloseDriverWayBillTask'/*,"PrintWayBill"*/, "DocumentsList", 'DocumentsItem', 'SeedingList', 'SeedingItem', 'Reports',
        'ActsList','ActsList1', 'ActsItem','SparePartsItem','IncomingList', 'IncomingFuelList', 'DemandWayBillItem','MovementTmc',
        'Techcard','TCList','TCTempList', 'TechPlan','TMCPlan','TechcardTemp','Roles', 'KPI','DailyRepairsItem', 'DoubleList','Administration','SelectedPlants','SectionTechcard','DemandWayBill','RepairsDailyCtrl','ParamOrganization','WriteOffActs', 'WriteOffActsItem',
        'Main', 'Cost', 'Culture', 'Map','LeftPanel','RightPanel','Scheme', 'Dictionary', 'Product', 'Service','Crater', 'Sales', 'Gross', 'BalanceMaterial', 'BalanceSpareParts', 'ComingSpareParts', 'RepairsReportsCtrl', 'ControlTech',
        'WriteOffSpareParts','WriteOffSparePartsItem','MovementItem','HistorySpareParts', 'Rates','RateFuel','MovementSpareParts','ReserveSpareParts','ComingInvoiceSpareParts','ComingTmc', 'ComingInvoiceSparePartsItem','RepairsSparePartsCtrl']);

    app.config(['$stateProvider', '$urlRouterProvider', function (sp, urp) {
        urp.otherwise('/login');
        sp
            .state('login', {
                url: '/login',
                views: {
                    "way_bills_main": {
                        templateUrl: 'modules/authentication/login.html',
                        controller: 'LoginController'
                    }
                }
            })
            .state('shift_task', {
                url: '/shift_task',
                views: {
                    'way_bills_main': {
                        templateUrl: 'modules/shift_task/shift_task/shift_task.html',
                        controller: 'CreateShiftTaskCtrl'
                    }
                }
            })
			  .state('shift_task_repairs', {
                url: '/shift_task_repairs',
                views: {
                    'way_bills_main': {
                        templateUrl: 'modules/spare_parts/shift_task_repairs/shift_task_repairs.html',
                        controller: 'ShiftTaskRepairsCtrl'
                    }
                }
            })
			
            .state('create_task', {
                url: '/create_task/:task_date/:task_shift',
                views: {
                    'way_bills_main': {
                        templateUrl: 'modules/shift_task/create_task/create_task.html',
                        controller: 'CreateTaskCtrl'
                    }
                }
            })
            .state('edit_task', {
                url: '/edit_task/:detail_id',
                views: {
                    'way_bills_main': {
                        templateUrl: 'modules/shift_task/create_task/create_task.html',
                        controller: 'EditTaskCtrl'
                    }
                }
            })
            .state('way_bills', {
                url: '/way_bills',
                views: {
                    'way_bills_main': {
                        templateUrl: 'modules/way_bills_operation/list/list_way_bills.html',
                        controller: 'WayBillsIssuanceCtrl'
                    }
                }
            })
            .state('edit_bill', {
                url: '/edit_bill/:type/:way_bill_id',
                views: {
                    'way_bills_main': {
                        templateUrl: 'modules/way_bills_operation/create_and_edit/create_way_bill.html',
                        controller: 'WayBillEditCtrl'
                    }
                }
            })
            .state('close_bill', {
                url: '/close_bill/:way_bill_id',
                views: {
                    'way_bills_main': {
                        //templateUrl: 'modules/way_bills_operation/close_form_way_bill/close_form_way_bill.html',
                        templateUrl: 'modules/way_bills_operation/close_way_bill/close_way_bill.html',
                        controller: 'CloseWayBillCtrl'
                    }
                }
            })
            .state('close_bill_edit_task', {
                url: '/close_bill_edit_task/:way_bill_id/:type/:task_id',
                views: {
                    'way_bills_main': {
                        templateUrl: 'modules/way_bills_operation/close_way_bill/close_add_edit_task/close_add_edit_task.html',
                        controller: 'CloseEditTaskCtrl'
                    }
                }
            })
            .state('driver_way_bills', {
                url: '/driver_way_bills',
                views: {
                    'way_bills_main': {
                        templateUrl: 'modules/driver_way_bills/list/driver_way_bills_list.html',
                        controller: 'DriverWayBillsListCtrl'
                    }
                }
            })
            .state('driver_way_bill_item', {
                url: '/driver_way_bill_item/:item_id',
                views: {
                    'way_bills_main': {
                        templateUrl: 'modules/driver_way_bills/item/driver_way_bill_item.html',
                        controller: 'DriverWayBillItemCtrl'
                    }
                }
            })
            .state('close_driver_way_bill', {
                url: '/close_driver_way_bill/:way_bill_id',
                views: {
                    'way_bills_main': {
                        templateUrl: 'modules/driver_way_bills/close/close_driver_way_bill.html',
                        controller: 'CloseDriverWayBillCtrl'
                    }
                }
            })
            .state('close_driver_way_bill_task', {
                url: '/close_driver_way_bill_task/:way_bill_id/:task_id',
                views: {
                    'way_bills_main': {
                        templateUrl: 'modules/driver_way_bills/close/task/close_driver_way_bill_task.html',
                        controller: 'CloseDriverWayBillTaskCtrl'
                    }
                }
            })
			.state('administration',{      
                url: '/administration',
                views: {
                    'way_bills_main': {
                        templateUrl: 'modules/administration/administration.html',
                        controller: 'AdministrationCtrl'
                    }
                }
            })
				.state('roles',{      
                url: '/roles',
                views: {
                    'way_bills_main': {
                        templateUrl: 'modules/administration/roles.html',
                        controller: 'RolesCtrl'
                    }
                }
            })
			.state('selected_plants',{      
                url: '/selected_plants',
                views: {
                    'way_bills_main': {
                        templateUrl: 'modules/administration/selected_plants.html',
                        controller: 'SelectedPlantsCtrl'
                    }
                }
            })
			.state('param_organization', {      
                url: '/param_organization',
                views: {
                    'way_bills_main': {
                        templateUrl: 'modules/administration/param_organization.html',
                        controller: 'ParamOrganizationCtrl'
                    }
                }
            })
				.state('tech_control', {      
                url: '/tech_control',
                views: {
                    'way_bills_main': {
                        templateUrl: 'modules/administration/control_tech.html',
                        controller: 'TechControlCtrl'
                    }
                }
            })
				.state('section_techcard',{      
                url: '/section_techcard',
                views: {
                    'way_bills_main': {
                        templateUrl: 'modules/administration/section_techcard.html',
                        controller: 'SectionTechcardCtrl'
                    }
                }
            })
				.state('demand_waybill',{      
                url: '/demand_waybill',
                views: {
                    'way_bills_main': {
                        templateUrl: 'modules/spare_parts/demand_waybill/demand_waybill.html',
                        controller: 'DemandWayBillCtrl'
                    }
                }
            })
				.state('demand_waybill_item',{      
                url: '/demand_waybill_item',
                views: {
                    'way_bills_main': {
                        templateUrl: 'modules/spare_parts/demand_waybill/demand_waybill_item.html',
                        controller: 'DemandWayBillItemCtrl'
                    }
                }
            })
            .state('acts_list', {
                url: '/acts_list/:type',
                views: {
                    'way_bills_main': {
                        templateUrl: 'modules/acts/list/acts_list.html',
                        controller: 'ActsListCtrl'
                    }
                }
            })
			   .state('acts_list1', {
                url: '/acts_list1/:type',
                views: {
                    'way_bills_main': {
                        templateUrl: 'modules/acts/list/acts_list1.html',
                        controller: 'ActsListCtrl'
                    }
                }
            })
            .state('acts_item', {
                url: '/act/:type/:act_id',
                views: {
                    'way_bills_main': {
                        templateUrl: 'modules/acts/item/acts_item.html',
                        controller: 'ActsItemCtrl'
                    }
                }
            })
			 .state('spare_parts_item', {
                url: '/spare_parts/:spare_id',
                views: {
                    'way_bills_main': {
                        templateUrl: 'modules/spare_parts/coming_invoice/coming_spare_parts_item.html',
                        controller: 'SparePartsItemCtrl'
                    }
                }
            })
			 .state('write_off_acts_item', {
                url: '/write_off_acts/:spare_id',
                views: {
                    'way_bills_main': {
                        templateUrl: 'modules/spare_parts/writeoff_acts/writeoff_acts_item.html',
                        controller: 'WriteOffActsItemCtrl'
                    }
                }
            })
			.state('movement_spare_parts', {
                url: '/movement_spare_parts',
                views: {
                    'way_bills_main': {
                        templateUrl: 'modules/spare_parts/movement_spare_parts/movement_spare_parts.html',
                        controller: 'MovementSparePartsCtrl'
                    }
                }
            })
				.state('reserve_spare_parts', {
                url: '/reserve_spare_parts',
                views: {
                    'way_bills_main': {
                        templateUrl: 'modules/spare_parts/reserve_spare_parts/reserve_spare_parts.html',
                        controller: 'ReserveSparePartsCtrl'
                    }
                }
            })
			 .state('writeoff_spare_parts_item', {
                url: '/writeoff_spare_parts/:spare_id',
                views: {
                    'way_bills_main': {
                        templateUrl: 'modules/spare_parts/repairs_spare_parts/writeoff_spare_parts_item.html',
                        controller: 'WriteOffSparePartsItemCtrl'
                    }
                }
            })
            .state('incoming', {
                url: '/act/incoming/:type/:act_id',
                views: {
                    'way_bills_main': {
                        templateUrl: 'modules/acts/incoming/incoming_item.html',
                        controller: 'IncomingListCtrl'
                    }
                }
            })
            .state('incoming_fuel', {
                url: '/incoming_fuel',
                views: {
                    'way_bills_main': {
                        templateUrl: 'modules/acts/incoming_fuel/incomingFuel.html',
                        controller: 'IncomingFuelListCtrl'
                    }
                }
            })
			//MovementItemCtrl
			  .state('movement_item', {
                url: '/movement_item',
                views: {
                    'way_bills_main': {
                        templateUrl: 'modules/acts/movement/movement_item.html',
                        controller: 'MovementItemCtrl'
                    }
                }
            })
			
            .state('documents_list', {
                url: '/documents_list',
                views: {
                    'way_bills_main': {
                        templateUrl: 'modules/documents/list/documents_list.html',
                        controller: 'DocumentsListCtrl'
                    }
                }
            }).state('document_item', {
                url: '/document/:type/:doc_id',
                views: {
                    'way_bills_main': {
                        templateUrl: 'modules/documents/Item/documents_item.html',
                        controller: 'DocumentsItemCtrl'
                    }
                }
            }).state('seeding_list', {
                url: '/seeding_list',
                views: {
                    'way_bills_main': {
                        templateUrl: 'modules/seeding/list/seeding_list.html',
                        controller: 'SeedingListCtrl'
                    }
                }
            }).state('seeding_item', {
                url: '/seeding_item/:year/:field_id',
                views: {
                    'way_bills_main': {
                        templateUrl: 'modules/seeding/item/seeding_item.html',
                        controller: 'SeedingItemCtrl'
                    }
                }
            }).state('reports', {
                url: '/reports',
                views: {
                    'way_bills_main': {
                        templateUrl: 'modules/reports/reports.html',
                        controller: 'ReportsCtrl'
                    }
                }
            }).state('main', {
                url: '/main',
                views: {
                    'way_bills_main': {
                        templateUrl: 'modules/main/main.html',
                        controller: 'MainCtrl'
                    }
                }
            }).state('cost', {
                url: '/cost',
                views: {
                    'way_bills_main': {
                        templateUrl: 'modules/monitor/cost/cost.html',
                        controller: 'CostCtrl'
                    }
                }
            }).state('culture', {
                url: '/culture/:culture_id',
                views: {
                    'way_bills_main': {
                        templateUrl: 'modules/monitor/culture/culture.html',
                        controller: 'CultureCtrl'
                    }
                }
            }).state('map', {
                url: '/map',
                views: {
                    'way_bills_main': {
                        templateUrl: 'modules/monitor/map/map.html',
                        controller: 'MapCtrl'
                    }
                }
            }).state('scheme', {
                url: '/scheme',
                views: {
                    'way_bills_main': {
                        templateUrl: 'modules/monitor/scheme/scheme.html',
                        controller: 'SchemeCtrl'
                    }
                }
            }).state('techcard', {
                url: '/techcard/:year',
                views: {
                    'way_bills_main': {
                        templateUrl: 'modules/techcard/tc_list.html',
                        controller: 'TCListCtrl'
                    }
                }
            }).state('tcdetail',{
                url: '/tcdetail/:year/:id',
                views: {
                    'way_bills_main': {
                        templateUrl: 'modules/techcard/techcard.html',
                        controller: 'Techcard'
                    }
                }
            }).state('dictionary',{
                url: '/dictionary',
                views: {
                    'way_bills_main': {
                        templateUrl: 'modules/dictionary/dictionary.html',
                        controller: 'DictionaryCtrl'
                    }
                }
            }).state('product',{
                url: '/product',
                views: {
                    'way_bills_main': {
                        templateUrl: 'modules/product/product.html',
                        controller: 'ProductCtrl'
                    }
                }
            }).state('crater',{
                url: '/crater',
                views: {
                    'way_bills_main': {
                        templateUrl: 'modules/crater/crater.html',
                        controller: 'CraterCtrl'
                    }
				}
			}).state('service',{
                url: '/service',
                views: {
                    'way_bills_main': {
                        templateUrl: 'modules/service/service.html',
                        controller: 'ServiceCtrl'
                    }
				}
			}).state('sales',{
                url: '/sales/:year/:plant',
                views: {
                    'way_bills_main': {
                        templateUrl: 'modules/product/sales.html',
                        controller: 'SalesCtrl'
                    }
                }
            }).state('gross',{
                url: '/gross/:year/:plant',
                views: {
                    'way_bills_main': {
                        templateUrl: 'modules/product/gross.html',
                        controller: 'GrossCtrl'
                    }
                }
            }).state('techplan',{
                url: '/techplan',
                views: {
                    'way_bills_main': {
                        templateUrl: 'modules/techcard/techplan.html',
                        controller: 'TechplanCtrl'
                    }
                }
				  }).state('tmcplan',{
                url: '/tmcplan',
                views: {
                    'way_bills_main': {
                        templateUrl: 'modules/techcard/tmcplan.html',
                        controller: 'TMCplanCtrl'
                    }
                }
			   }).state('techcard_temp',{
                url: '/techcard_temp/:id',
                views: {
                    'way_bills_main': {
                        templateUrl: 'modules/techcard_temp/techcard_temp.html',
                        controller: 'TechcardTempCtrl'
                    }
                }
				        }).state('techcard_temp_list', {
                url: '/techcard_temp_list',
                views: {
                    'way_bills_main': {
                        templateUrl: 'modules/techcard_temp/tc_temp_list.html',
                        controller: 'TCTempListCtrl'
                    }
                }
            }).state('kpi',{
                url: '/kpi',
                views: {
                    'way_bills_main': {
                        templateUrl: 'modules/kpi/kpi_list.html',
                        controller: 'KPICtrl'
                    }
                }
            }).state('rates',{
                    url: '/rates',
                    views: {
                        'way_bills_main': {
                            templateUrl: 'modules/rates/rates.html',
                            controller: 'RatesCtrl'
                    }
                }
				     }).state('rate_fuel',{
                    url: '/rate_fuel',
                    views: {
                        'way_bills_main': {
                            templateUrl: 'modules/rate_fuel/rate_fuel.html',
                            controller: 'RateFuelCtrl'
                    }
                }
				 }).state('spare_parts',{
                    url: '/spare_parts',
                    views: {
                        'way_bills_main': {
                            templateUrl: 'modules/spare_parts/balance_spare_parts/balance_spare_parts.html',
                            controller: 'BalanceSparePartsCtrl'
                    }
                }
				 }).state('coming_invoice',{
                    url: '/coming_invoice',
                    views: {
                        'way_bills_main': {
                            templateUrl: 'modules/spare_parts/coming_invoice/coming_invoice.html',
                            controller: 'ComingInvoiceCtrl'
                    }
                }
				 }).state('coming_tmc',{
                    url: '/coming_tmc',
                    views: {
                        'way_bills_main': {
                            templateUrl: 'modules/acts/coming_acts/coming_tmc.html',
                            controller: 'ComingTmcCtrl'
                    }
                }
					 }).state('movement_tmc',{
                    url: '/movement_tmc',
                    views: {
                        'way_bills_main': {
                            templateUrl: 'modules/acts/movement/movement_tmc.html',
                            controller: 'MovementTmcCtrl'
                    }
                }
				 }).state('write_off_acts',{
                    url: '/write_off_acts',
                    views: {
                        'way_bills_main': {
                            templateUrl: 'modules/spare_parts/writeoff_acts/writeoff_acts.html',
                            controller: 'WriteOffActsCtrl'
                    }
                }
				 }).state('coming_spare_parts',{
                    url: '/coming_spare_parts',
                    views: {
                        'way_bills_main': {
                            templateUrl: 'modules/spare_parts/coming_spare_parts/coming_spare_parts.html',
                            controller: 'ComingSparePartsCtrl'
                    }
                }
				 }).state('history_spare_parts',{
                    url: '/history_spare_parts',
                    views: {
                        'way_bills_main': {
                            templateUrl: 'modules/spare_parts/history_spare_parts/history_spare_parts.html',
                            controller: 'HistorySparePartsCtrl'
                    }
                }
				 }).state('writeoff_spare_parts',{
                    url: '/writeoff_spare_parts',
                    views: {
                        'way_bills_main': {
                            templateUrl: 'modules/spare_parts/writeoff_spare_parts/writeoff_spare_parts.html',
                            controller: 'WriteOffSparePartsCtrl'
                    }
                }
				//наряд на ремонт
				 }).state('repairs_spare_parts',{
                    url: '/repairs_spare_parts',
                    views: {
                        'way_bills_main': {
                            templateUrl: 'modules/spare_parts/repairs_spare_parts/repairs.html',
                            controller: 'RepairsSparePartsCtrl'
                    }
                }
			//ЕЖЕДНЕВНЫЙ НАРЯД
			}).state('repairs_daily',{
                    url: '/daily_repairs',
                    views: {
                        'way_bills_main': {
                            templateUrl: 'modules/spare_parts/daily_repairs/daily_repairs.html',
                            controller: 'RepairsDailyCtrl'
                    }
                }
            })
			//Отчеты в агрогараже
			.state('repairs_reports',{
                    url: '/repairs_reports',
                    views: {
                        'way_bills_main': {
                            templateUrl: 'modules/spare_parts/reports/reports.html',
                            controller: 'RepairsReportsCtrl'
                    }
                }
            })
			.state('daily_repairs_item', {
                url: '/daily_repairs_item/:spare_id',
                views: {
                    'way_bills_main': {
                        templateUrl: 'modules/spare_parts/daily_repairs/daily_repairs_item.html',
                        controller: 'RepairsDailyItemCtrl'
                    }
                }
            }).state('balance_material', {
                url: '/balance_material',
                views: {
                    'way_bills_main': {
                        templateUrl: 'modules/acts/balance_material/balance_material.html',
                        controller: 'BalanceMaterialCtrl'
                    }
                }
            })
      
    }]);

    app.run(['$rootScope', '$location', '$cookieStore', '$http','requests',
        function ($rootScope, $location, $cookieStore, $http, requests) {
            var curUser = $cookieStore.get('currentUser');
			var dataUser = [];
            if (curUser) {
                if (curUser.currentRole) {
                    $rootScope.roleid = curUser.currentRole;
                    $rootScope.startroleid = curUser.roleid;
                } else {
                    $rootScope.startroleid = $rootScope.roleid = curUser.roleid;
                }
                console.info(curUser);
                console.log('run ' + $rootScope.roleid);
            }
            $rootScope.logoSrc = "images/logo.png";
            $rootScope.menuSkin = 'defualt';

			
			function checkElementArr(str ) {
				for (var i = 0; i < dataUser.SubsystemsList.length; i++) 
						{
				if (dataUser.SubsystemsList[i] == str) {return true;}
							
						}
						return false;	
			}
			
			function getNamePathByRole(id) {
				 switch (id) {
				   case "tasM5"	: return  {name : 'План-факт', link: '/cost'};
				   case "tasM6"	: return  {name : 'Затраты по культурам', link: '/culture/'};
				   case "tasM7"	: return  {name : 'Карта мониторинга', link: '/map'};   
				   
				    case "tas0"	: return {name : 'Сменное задание', link: '/shift_task', number: '1'};
                    case "tas1"	: return {name : 'Путевые листы механизаторов', link: '/way_bills', number: '2'};				
				    case "tas2"	: return {name : 'Путевые листы водителей', link: '/driver_way_bills', number: '3'};
                    case "tas3"	: return {name : 'Отчеты', link: '/reports', number: '4'};
					case "tas4"	: return {name : 'Начисление ЗП(сдельная)', link: '/documents_list', number: '4'};
                    case "tas5"	: return {name : 'Учет ТМЦ', link: '/balance_material', number: '4'};
					case "tas6"	: return {name : 'Приход горючего', link: '/incoming_fuel', number: '4'};
                    case "tas7"	: return {name : 'Урожай', link: '/product', number: '4'};
					case "tas8"	: return {name : 'Услуги', link: '/service', number: '4'};
                    case "tas9"	: return {name : 'Контроль качества', link: '/kpi'};
					case "tas10": return {name: 'Расценки', link: '/rates', number: '1'};
                    case "tas11": return {name: 'Норма расхода топлива', link: '/rate_fuel', number: '1'};
					case "tas12": return {name : 'Справочники', link: '/dictionary', number: '4'};
					
					case "plan0": return {name : 'Севооборот', link: '/seeding_list'};
					case "plan1": return {name : 'Тех.карты', link: '/techcard/'};
                    case "plan2": return {name : 'Шаблоны тех.карт', link: '/techcard_temp_list'};
					case "plan3": return {name : 'Распределение техники', link: '/techplan'};
                    case "plan4": return {name : 'Распределение ТМЦ', link: '/tmcplan'};
				
					case "gar0": return {name : 'Баланс', link: '/spare_parts', number: '4'};
					case "gar1": return {name : 'Приходные накладные', link: '/coming_invoice', number: '1'};
					case "gar2": return {name : 'Сменное задание на ремонт', link: '/shift_task_repairs', number: '4'};
					case "gar3": return {name : 'Наряды на ремонт', link: '/repairs_spare_parts', number: '4'};
					case "gar4": return {name : 'Ежедневные наряды', link: '/daily_repairs', number: '4'};
					case "gar5": return {name : 'Акты списания', link: '/write_off_acts', number: '4'};
					case "gar6": return {name : 'Требования-накладные', link: '/demand_waybill', number: '4'}
					case "gar7": return {name : 'История запчасти', link: '/history_spare_parts', number: '4'};
					case "gar8": return {name : 'Отчеты', link: '/repairs_reports', number: '4'};
					case "gar9": return {name : 'Справочники', link: '/dictionary', number: '4'};
				
                    return null;
				   
				 }
				
			}
			
            $rootScope.checkRoles = function(cultureId) {
                var roleid = $rootScope.roleid;

                var hideAllBtns = function() {
                    $rootScope.topBtnVis = false;
                    $rootScope.topBtnActVis = false;
                    $rootScope.zpBtnVis = false;
                    $rootScope.tcBtnVis = false;
                };
                if (roleid > 100) {
                    hideAllBtns();
                }
				 $("#btn").hide();
                console.log('checkRoles ' + roleid);
				
				if (roleid != 111 && roleid != 112 && roleid != 113  && roleid != 11 && roleid != 6 && roleid != 114 && roleid != 115) {
					roleid = 10;
				}
			     dataUser = $cookieStore.get('currentUser');
				 //console.log(dataUser.SubsystemsList);
						$("#btn").show();
						$rootScope.menu = [];
                      switch (roleid) {
					
					/*
                    case 2:
                    case 7:
                        //Сменное задание
                        $rootScope.logoSrc = 'images/logo_agro_prodaction.png';
                        $rootScope.menuSkin = 'defualt';
                        $rootScope.menu = [
                            {name : 'Сменное задание', link: '/shift_task'}
                        ];
                        $rootScope.selectedTab = 'Сменное задание';
                        break;
                    case 3:
                    case 4:
                        //Сменное задание, Путевые листы, Отчеты
                        $rootScope.logoSrc = 'images/logo_agro_prodaction.png';
                        $rootScope.menuSkin = 'defualt';
                        $rootScope.menu = [
                            {name : 'Сменное задание', link: '/shift_task'},
                            {name : 'Путевые листы механизаторов', link: '/way_bills'},
                            {name : 'Путевые листы водителей', link: '/driver_way_bills'},
                            {name : 'Отчеты', link: '/reports'}
                        ];
                        break;
                    case 5:
                        //Агроном
                        $rootScope.logoSrc = 'images/logo_agro_prodaction.png';
                        $rootScope.menuSkin = 'defualt';
                        $rootScope.menu = [
                            {name : 'Севооборот', link: '/documents_list'}
                        ];
                        break;
						*/
                    case 6:
                        //Планирование
						for (var i = 0; i < 5; i++) 
						{
					      if (checkElementArr("plan" + i)) { 
							
						var arr  = getNamePathByRole("plan" + i);
						if (arr != null) {
						$rootScope.menu.push(arr);
							
						}
						  }	
						}
						
                        $rootScope.logoSrc = 'images/logo_agro_prodaction.png';
                        $rootScope.menuSkin = 'defualt';
						/*
                        $rootScope.menu = [	
						   {name : 'Севооборот', link: '/seeding_list'},	
						   {name : 'Тех.карты', link: '/techcard/'},
					       {name : 'Шаблоны тех.карт', link: '/techcard_temp_list'},
                           {name : 'Распределение техники', link: '/techplan'},	
						   {name : 'Распределение ТМЦ', link: '/tmcplan'},
						// {name : 'Воронка производства', link: '/crater', number: '4'},	
                        ];
						*/
					$.ajax({
                      type: 'HEAD',
                      url: 'modules/crater/crater.html',
                      success: function() {
                       },
                    error: function() {
						for (var i = 0; i < $rootScope.menu.length; i++)
						{
						if ($rootScope.menu[i].name=="Воронка производства") {$rootScope.menu.splice(i, 1);}	
						}
                    }
                    });
                        break;
						/*
                    case 8:
                        //Сменное задание, Путевые листы, Отчеты + Бухгалтерия + Приход + Списание
                        $rootScope.logoSrc = 'images/logo_agro_prodaction.png';
                        $rootScope.menuSkin = 'defualt';
                        $rootScope.menu = [
                            {name : 'Сменное задание', link: '/shift_task', number: '1'},
                            {name : 'Путевые листы механизаторов', link: '/way_bills', number: '2'},
                            {name : 'Путевые листы водителей', link: '/driver_way_bills', number: '3'},
                            {name : 'Отчеты', link: '/reports', number: '4'},
                            {name : 'Начисление ЗП(сдельная)', link: '/documents_list', number: '4'},
                            {name : 'Учет ТМЦ', link: '/balance_material', number: '4'},
                            {name : 'Приход горючего', link: '/incoming_fuel', number: '4'},
                            {name : 'Урожай', link: '/product', number: '4'},
							{name : 'Услуги', link: '/service', number: '4'},	
                            {name : 'Контроль качества', link: '/kpi'},
                            {name: 'Расценки', link: '/rates', number: '1'},
                            {name : 'Справочники', link: '/dictionary', number: '4'}
                        ];
                        break;
						*/
                    case 10:
					/*
                        //Демонтрационный пользователь с главной панелью и переходом на все роли и мониторинг
                        $rootScope.logoSrc = 'images/logo_agro_prodaction.png';
                        $rootScope.menuSkin = 'defualt';
                        $rootScope.menu = [
                            {name : 'Сменное задание', link: '/shift_task', number: '1'},
                            {name : 'Путевые листы механизаторов', link: '/way_bills', number: '2'},
                            {name : 'Путевые листы водителей', link: '/driver_way_bills', number: '3'},
                            {name : 'Отчеты', link: '/reports', number: '4'},
                            {name : 'Начисление ЗП(сдельная)', link: '/documents_list', number: '4'},
                            {name : 'Учет ТМЦ', link: '/balance_material', number: '4'},
                            {name : 'Приход горючего', link: '/incoming_fuel', number: '4'},
                            {name : 'Урожай', link: '/product', number: '4'},
							{name : 'Услуги', link: '/service', number: '4'},	
                            {name : 'Контроль качества', link: '/kpi'},
                            {name: 'Расценки', link: '/rates', number: '1'},   
						  //  {name: 'Норма расхода топлива', link: '/rate_fuel', number: '1'}, 
                            {name : 'Справочники', link: '/dictionary', number: '4'}
                        ];
						*/						
                      setLocation('/main');
                      break;
                    case 11:
                        //ПРОИЗВОДСТВЕННЫЕ ЗАДАЧИ
                        $rootScope.logoSrc = 'images/logo_agro_prodaction.png';
                        $rootScope.menuSkin = 'defualt';
							for (var i = 0; i < 13; i++) 
						{
					       if (checkElementArr("tas" + i)) { 
						   console.log('121');
						   var arr  = getNamePathByRole("tas" + i);
						   if (arr != null) {
						  $rootScope.menu.push(arr);
						}
						  }	
						}
						/*
                        $rootScope.menu = [
                            {name : 'Сменное задание', link: '/shift_task', number: '1'},
                            {name : 'Путевые листы механизаторов', link: '/way_bills', number: '2'},
                            {name : 'Путевые листы водителей', link: '/driver_way_bills', number: '3'},
                            {name : 'Отчеты', link: '/reports', number: '4'},
                            {name : 'Начисление ЗП(сдельная)', link: '/documents_list', number: '4'},
                            {name : 'Учет ТМЦ', link: '/balance_material', number: '4'},
                            {name : 'Приход горючего', link: '/incoming_fuel', number: '4'},
                            {name : 'Урожай', link: '/product', number: '4'},
							{name : 'Услуги', link: '/service', number: '4'},		
                            {name : 'Контроль качества', link: '/kpi'},
                            {name: 'Расценки', link: '/rates', number: '1'},
							{name: 'Норма расхода топлива', link: '/rate_fuel', number: '1'}, 
                            {name : 'Справочники', link: '/dictionary', number: '4'},
                        ];
						*/				
                        break;
						/*
						//Auditor
						 case 12:
                        $rootScope.logoSrc = 'images/logo_agro_prodaction.png';
                        $rootScope.menuSkin = 'defualt';
                        $rootScope.menu = [
						    {name : 'Сменное задание', link: '/shift_task', number: '1'},
                            {name : 'Отчеты', link: '/reports', number: '4'},
							{name : 'План-факт', link: '/cost'},
                            {name : 'Тех.карты', link: '/techcard/'}
                        ];
                              break;
							  
							  //Кладовщик
							   case 15:
                        $rootScope.logoSrc = 'images/logo_agro_prodaction.png';
                        $rootScope.menuSkin = 'defualt';
                        $rootScope.menu = [
					    {name : 'Баланс', link: '/spare_parts', number: '4'},	
						{name : 'Приходные накладные', link: '/coming_invoice', number: '4'},
				    	{name : 'Наряды на ремонт', link: '/repairs_spare_parts', number: '4'},	
						{name : 'Ежедневные наряды', link: '/daily_repairs', number: '4'},
						{name : 'Акты списания', link: '/write_off_acts', number: '4'},
						{name : 'Требования-накладные', link: '/demand_waybill', number: '4'},
						{name : 'История запчасти', link: '/history_spare_parts', number: '4'},
						{name : 'Отчеты', link: '/repairs_reports', number: '4'},
						{name : 'Справочники', link: '/dictionary', number: '4'},
                        ];
                              break;
						 	//Agronom
						 case 14:
                        $rootScope.logoSrc = 'images/logo_agro_prodaction.png';
                        $rootScope.menuSkin = 'defualt';
                        $rootScope.menu = [
						//	 {name : 'Распределение ТМЦ', link: '/tmcplan'},
						     {name : 'Севооборот', link: '/seeding_list'},
							 {name : 'Тех.карты', link: '/techcard/'},
							 {name : 'Распределение техники', link: '/techplan'},
							 {name : 'Распределение ТМЦ', link: '/tmcplan'},
							 {name : 'Учет ТМЦ', link: '/balance_material', number: '4'},
							 {name : 'Урожай', link: '/product', number: '4'},
							 {name : 'Услуги', link: '/service', number: '4'},
							 
                        ];
                        break;
						 //ТМЦ
						 case 16:
					    $rootScope.logoSrc = 'images/logo_agro_prodaction.png';
                        $rootScope.menuSkin = 'defualt';
                        $rootScope.menu = [
				    	{name : 'Учет ТМЦ', link: '/balance_material', number: '4'},					 
                        ];
                        break;					 
                    case 13:
                        //Алименко
                        $rootScope.logoSrc = 'images/logo_agro_prodaction.png';
                        $rootScope.menuSkin = 'defualt';
                        $rootScope.menu = [
                            {name : 'Сменное задание', link: '/shift_task', number: '1'},
                            {name : 'Путевые листы механизаторов', link: '/way_bills', number: '2'},
                            {name : 'Путевые листы водителей', link: '/driver_way_bills', number: '3'},
                            {name : 'Отчеты', link: '/reports', number: '4'},
                            {name : 'Начисление ЗП(сдельная)', link: '/documents_list', number: '4'},
                            {name : 'Севооборот', link: '/seeding_list'},
                            {name : 'Тех.карты', link: '/techcard/'}
                        ];
                        break;
						*/
                    //трехзначные дополнительные роли для мониторинга, не приходят из базы, а устанавливаются в коде
                    case 111:
                        //ПЛАН-ФАКТНЫЕ ЗАТРАТЫ
                        $rootScope.logoSrc = 'images/logo_agro_analitics.png';
                        $rootScope.menuSkin = 'blue';
                    //    $rootScope.menu = [
                   //         {name : 'План-факт', link: '/cost'}
                   //     ];			
					       if (checkElementArr("tasM5")) { 
						   var arr  = getNamePathByRole("tasM5");
						   if (arr != null) {
						  $rootScope.menu.push(arr);
						}
						  }	
                        break;
                    case 112:
					//ДЕТАЛИЗАЦИЯ ЗАТРАТ ПО культурам
                        $rootScope.logoSrc = 'images/logo_agro_analitics.png';
                        $rootScope.menuSkin = 'blue';
						/*
                        $rootScope.menu = [
                            {name : 'Затраты по культурам', link: '/culture/'}
                        ];
						*/
						   if (checkElementArr("tasM6")) { 
						   var arr  = getNamePathByRole("tasM6");
						   if (arr != null) {
						  $rootScope.menu.push(arr);
						}
						  }	
						 if ($rootScope.menu.length != 0) {
                        cultureId = cultureId ? cultureId : 1;
                        setLocation('/culture/' + cultureId)
						 }
                        break;
                    case 113:
                        //map
                        $rootScope.logoSrc = 'images/logo_agro_monitoring.png';
                        $rootScope.menuSkin = 'green';
                      //  $rootScope.menu = [
                    //        {name : 'Карта мониторинга', link: '/map'}
                    //    ];
						   if (checkElementArr("tasM7")) { 
						   var arr  = getNamePathByRole("tasM7");
						   if (arr != null) {
						  $rootScope.menu.push(arr);
						}
						  }	
						
                        break;
						//Администирование
						 case 115:
                        //map
                        $rootScope.logoSrc = 'images/logo_agro_monitoring.png';
                        $rootScope.menuSkin = 'defualt';
						   if (checkElementArr("tasM4")) { 
                        $rootScope.menu = [
					//	 {name : 'Учетные записи пользователей', link: '/administration', number: '4'}, 
					     {name : 'Контроль техники', link: '/tech_control', number: '4'},
				  		 {name : 'Параметры организации', link: '/param_organization', number: '4'},
					     {name : 'Учетные записи пользователей', link: '/administration', number: '4'}, 
						 {name : 'Используемые культуры', link: '/selected_plants', number: '4'},
					     {name : 'Тех.карты', link: '/section_techcard', number: '4'},
                        ];
						   }
                        break;
						//АГРОГАРАЖ
                    case 114:
					 $rootScope.logoSrc = 'images/logo_agro_prodaction.png';
                        $rootScope.menuSkin = 'defualt';
								for (var i = 0; i < 10; i++) 
						{
					       if (checkElementArr("gar" + i)) { 
						   var arr  = getNamePathByRole("gar" + i);
						   if (arr != null) {
						  $rootScope.menu.push(arr);
						}
						  }	
						}
						/*
                        $rootScope.menu = [	
					    {name : 'Баланс', link: '/spare_parts', number: '4'},	
					    {name : 'Приходные накладные', link: '/coming_invoice', number: '1'},
						{name : 'Сменное задание на ремонт', link: '/shift_task_repairs', number: '4'},	 
				     	{name : 'Наряды на ремонт', link: '/repairs_spare_parts', number: '4'},	 
						{name : 'Ежедневные наряды', link: '/daily_repairs', number: '4'},
						{name : 'Акты списания', link: '/write_off_acts', number: '4'},
			    		{name : 'Требования-накладные', link: '/demand_waybill', number: '4'},
						{name : 'История запчасти', link: '/history_spare_parts', number: '4'},
				    	{name : 'Отчеты', link: '/repairs_reports', number: '4'},
						{name : 'Справочники', link: '/dictionary', number: '4'},
                        ];
						*/
						 break;
                    default:
                        console.warn('Проверьте роль!!!');
                        break;
							
                }
                if (roleid !== 10 && roleid !== 112) {
					if ($rootScope.menu.length != 0) {
                    $rootScope.selectedTab = $rootScope.menu[0].name;
                    setLocation($rootScope.menu[0].link)
					}
                }
            };

            $rootScope.setRole = function(role, cultureId) {
                var curUser = $cookieStore.get('currentUser');
                curUser.currentRole = role;
                $cookieStore.put('currentUser', curUser);
                $rootScope.roleid = role;
                $rootScope.checkRoles(cultureId);
            };

            var curUser = $cookieStore.get('currentUser');
            if (curUser) {
                if (curUser.username) {
                    $rootScope.userName = curUser.username;
                    $rootScope.exit = 'выход';
                }
                $rootScope.checkRoles();
            } else {
                $rootScope.userName = ' ';
                $rootScope.exit = ' ';
            }

            //выход
            // keep user logged in after page refresh
            $rootScope.globals = $cookieStore.get('globals') || {};
            if (curUser) {
                $http.defaults.headers.common['Authorization'] = 'Basic ' + curUser.authdata; // jshint ignore:line
            }

            $rootScope.$on('$locationChangeStart', function (event, next, current) {
                // redirect to login page if not logged in
                $rootScope.menuOpened = false;
                var curUser = $cookieStore.get('currentUser');
                if ($location.path() !== '/login' && !curUser) {
                    setLocation('/login');
                }
                if (curUser) {
                    if (curUser.username) {
                        $rootScope.userName = curUser.username;
                        $rootScope.exit = 'выход';
                    }
                    if (curUser.roleid == '8') {
                        $rootScope.topBtnActVis = true;
                    }
                } else {
                    $rootScope.userName = ' ';
                    $rootScope.exit = ' ';
                    $rootScope.topBtnVis = false;
                    $rootScope.topBtnActVis = false;
                }
            });

            $rootScope.gotoPage = function(m) {
                $rootScope.selectedTab = m.name;
                setLocation(m.link);
            };
              //на главную
            $rootScope.goToHome = function () {
          //    if ($rootScope.startroleid === 10) {
                    $rootScope.setRole(10);
          //      }
            };

            $rootScope.exitClick = function () {
                $cookieStore.remove('activeTab');
                $cookieStore.remove("seedingListSettings");
                $cookieStore.remove('globals');
                setLocation('/login');
            };

            $rootScope.openMenu = function() {
                if ($rootScope.menu.length > 1) {
                    $rootScope.menuOpened = !$rootScope.menuOpened;
                }
            }

            function setLocation(t) {
				console.log(t);
                $rootScope.activeTab = t;
                $location.path(t);
            }
        }]);
})();
