/**
 *  @ngdoc controller
 *  @name DriverWayBill.controller:DriverWayBillsList
 *  @description
 *  # DriverWayBillsList
 *  Это контроллер, определеяющий поведение формы списка путевых листов Водителей
 *  @requires $scope
 *  @requires $http
 *  @requires requests
 *  @requires $location
 *  @requires $cookieStore
 *  @requires modals
 *  @requires modalType

 *  @property {Object} sc: scope
 *  @property {Array} curUser: данные о пользователе
 *  @property {Array} sc.tableData: данные таблицы
 *  @property {Array} sc.selectedItems: выбранные строки таблицы
 *  @property {Object} sc.gridOptions: настройки таблицы
 *  @property {Object} cal: сохраненный календарь в пользовательской истории
 
 *  @property {function} needSelection: модальное окно с требованием выделить строку
 *  @property {function} sc.deleteBtnClick: кнопка Удалить - удаление выбранного путевого листа
 *  @property {function} sc.closeBtnClick: кнопка Закрыть - открытие формы Закрытия путевого листа водителя
 *  @property {function} sc.printBtnClick: кнопка Печать - печать выбранных путевых листов
 *  @property {function} sc.editBtnClick: кнопка Редактировать
 *  @property {function} sc.createBtnClick: кнопка Создать
 *  @property {function} sc.dateChange: изменение текущей даты
 *  @property {function} getDriverWayBillsList: перезагрузка списка путевых листов на текущую дату
 *  @property {function} selectRowByField: функция выделяет строку ранее выбранного путевого листа после перезагрузки списка или возвращения на форму
 */

var app = angular.module("DriverWayBillsList", ["ngGrid", "services", 'ngCookies']);

app.controller("DriverWayBillsListCtrl", ["$scope", "$http", "requests", "$location", '$cookieStore', 'modals', 'modalType', function ($scope, $http, requests, $location, $cookieStore, modals, modalType) {
    console.log('DriverWayBillsListCtrl');
    var sc = $scope;
    sc.infoData = [];
	sc.formData = [];
	sc.infoData.OrganizationsList = [];
		var date = new Date();
		sc.dateDriverWayBills = new Date(date.getFullYear(),date.getMonth(),1);
    var curUser = $cookieStore.get('currentUser');
      if (curUser.roleid != 10 && curUser.roleid != 4) {
		$("#delete").addClass("disabledbutton");
	}
	
	        sc.tporgid = curUser.tporgid;
	if (curUser.tporgid == 1) {
	sc.infoData.OrganizationsList.push({Id : curUser.orgid, Name : curUser.orgname });
	sc.formData.OrgId = curUser.orgid;	
			}
	
	
    var cal = $cookieStore.get('dateDriverWayBillsCalendar');
    if (cal && cal.date) {
    //    sc.dateDriverWayBills = cal.date;
	//	sc.dateDriverWayBillsEnd = sc.dateDriverWayBills;
    }
   //получаем дату запрета
	 requests.serverCall.get($http, "DayTasksCtrl/GetBanDateWayBills", function (data, status, headers, config) {
        console.info(data);
		if (data!="bad") { 
        sc.ban_date = data;} else {var date = new Date; sc.ban_date="01.01."+date.getFullYear();}
    }, function () {
    });
    sc.selectedItems = [];
    sc.tableData = [];

    sc.gridOptions = {
        data: 'tableData',
        enableColumnResize: true,
		enableSorting: false,
        multiSelect: false,
        enableRowSelection: true,
        enablePaging: true,
        showFooter: false,
        selectedItems: sc.selectedItems,
        rowHeight: 49,
        headerRowHeight: 49,
        columnDefs: [
            {
                field: "WaysListNum",
                displayName: "№",
                width: "10%",
                cellTemplate: '<a tooltip="{{row.getProperty(col.field)}}" tooltip-popup-delay="1000" tooltip-class="my-custom-tooltip" tooltip-placement="top" tooltip-append-to-body="true"><div class="ngCellText">{{row.getProperty(col.field)}}</div></a>'
            },
            {
                field: "DateBegin",
                displayName: "Дата",
                width: "10%",
                enableCellEdit: true,
                cellTemplate: '<a tooltip="{{row.getProperty(col.field)}}" tooltip-popup-delay="1000" tooltip-class="my-custom-tooltip" tooltip-placement="top" tooltip-append-to-body="true"><div class="ngCellText">{{row.getProperty(col.field)}}</div></a>'
            },
            {
                field: "ShiftNum",
                displayName: "Смена",
                width: "10%",
                cellTemplate: '<a tooltip="{{row.getProperty(col.field)}}" tooltip-popup-delay="1000" tooltip-class="my-custom-tooltip" tooltip-placement="top" tooltip-append-to-body="true"><div class="ngCellText">{{row.getProperty(col.field)}}</div></a>'
            },
            {
                field: "FioDriver",
                displayName: "Водитель",
                width: "20%",
                cellTemplate: '<a tooltip="{{row.getProperty(col.field)}}" tooltip-popup-delay="1000" tooltip-class="my-custom-tooltip" tooltip-placement="top" tooltip-append-to-body="true"><div class="ngCellText">{{row.getProperty(col.field)}}</div></a>'
            },
            {
                field: "CarsName",
                displayName: "TC",
                width: "20%",
                cellTemplate: '<a tooltip="{{row.getProperty(col.field)}}" tooltip-popup-delay="1000" tooltip-class="my-custom-tooltip" tooltip-placement="top" tooltip-append-to-body="true"><div class="ngCellText">{{row.getProperty(col.field)}}</div></a>'
            },
            {
                field: "CarsNum",
                displayName: "Гос. №",
                width: "15%",
                cellTemplate: '<a tooltip="{{row.getProperty(col.field)}}" tooltip-popup-delay="1000" tooltip-class="my-custom-tooltip" tooltip-placement="top" tooltip-append-to-body="true"><div class="ngCellText">{{row.getProperty(col.field)}}</div></a>'
            },
            {field: "Status", displayName: "Статус", width: "15%", cellTemplate: 'templates/status_cell_table.html'}
        ],
        afterSelectionChange: function (row) {
             sc.selectedItems = [];
            sc.selectedItems.push(row.entity);
			var dateBegin = sc.selectedItems[0].DateBegin.split(".");
			var dateBegin1 = new Date(dateBegin[2], (dateBegin[1] - 1), dateBegin[0]);
			var banDate = sc.ban_date.split("T")[0].split("-");
			var banDate1 = new Date(banDate[0], (banDate[1] - 1), parseInt(banDate[2])+1);
			console.log(banDate1);
			console.log(dateBegin1);
		 	console.log(banDate);
			console.log(dateBegin1.getTime()<banDate1.getTime());
		//	console.log(banDate);
			if (sc.selectedItems[0].Status==4 &&  dateBegin1.getTime()<banDate1.getTime()) 
		{	
			document.getElementById('close').disabled = true;
            document.getElementById('edit').disabled = true; } else 
			{
		   document.getElementById('close').disabled = false;
            document.getElementById('edit').disabled = false;}
        }
    };

    // модальное окно с требованием выделить строку
    function needSelection(title, message) {
        modals.showWindow(modalType.WIN_MESSAGE, {
            title: title,
            message: message,
            callback: function () {
            }
        });
    }

    sc.deleteBtnClick = function () {
        if (sc.selectedItems[0]) {
            modals.showWindow(modalType.WIN_DIALOG, {
                title: "Копирование",
                message: "Вы уверены, что хотите удалить путевой лист?",
                callback: function () {
                    console.log('delete ' + sc.selectedItems[0].WayBillId);
                    requests.serverCall.post($http, "DriverWayBillsCtrl/DeleteDriverWayBillItemById", sc.selectedItems[0].WayBillId, function (data, status, headers, config) {
                        console.info(data);
                        sc.formData = data;

                        getDriverWayBillsList();
                    });
                }
            });

        }
    };

    sc.closeBtnClick = function () {
        if (sc.selectedItems[0]) {
            console.info('close  ' + sc.selectedItems[0].WayBillId);
            $location.path('/close_driver_way_bill/' + sc.selectedItems[0].WayBillId);
        }
    };

    sc.printBtnClick = function () {
        if (sc.selectedItems[0]) {
            sc.getStyle = function (id) {
                var top = 42 + (18 * id);

                return {top: top.toString() + "mm"};
            };
            sc.getStyleHead = function (id) {
                var top = 42 + (8 * id);
                return {top: top.toString() + "mm"};
            };
            if (sc.selectedItems[0].GroupTraktor == 68 || sc.selectedItems[0].GroupTraktor == 69 || sc.selectedItems[0].GroupTraktor == 65) {
                requests.serverCall.post($http, "DriverWayBillsCtrl/GetPrintInfoForLightAvto", sc.selectedItems[0].WayBillId, function (data, status, headers, config) {
                    console.info(data);
                    sc.data = data;
                    setTimeout(function () {
                        var printContents = document.getElementById('carTask').innerHTML;
                        var popupWin = window.open('', '_blank');
                        if (typeof popupWin != 'undefined') {
                            popupWin.document.write('<html><head></head><body >' + printContents + '</html>');
                            popupWin.print();
                            popupWin.close();
                        }
                    }, 1000);
                });
            } 
			//автобусы
			 else if (sc.selectedItems[0].GroupTraktor == 64)   {
                requests.serverCall.post($http, "DriverWayBillsCtrl/GetPrintDriverWayInfo", sc.selectedItems[0].WayBillId, function (data, status, headers, config) {
                    console.info(data);
                    sc.data = data;
                    setTimeout(function () {
                        var printContents = document.getElementById('busTask').innerHTML;
                        var popupWin = window.open('', '_blank');
                        if (typeof popupWin != 'undefined') {
                            popupWin.document.write('<html><head></head><body >' + printContents + '</html>');
                            popupWin.print();
                            popupWin.close();
                        }
                    }, 1000);
                });
            } 
			else {
                requests.serverCall.post($http, "DriverWayBillsCtrl/GetPrintDriverWayInfo", sc.selectedItems[0].WayBillId, function (data, status, headers, config) {
                    console.info(data);
                    sc.data = data;
                    setTimeout(function () {
                        var printContents = document.getElementById('driverWayBill').innerHTML;
                        var popupWin = window.open('', '_blank');
                        if (typeof popupWin != 'undefined') {
                            popupWin.document.write('<html><head></head><body >' + printContents + '</html>');
                            popupWin.print();
                            popupWin.close();
                        }
                    }, 1000);
                });
            }

        } else needSelection('Печать путевого листа', "Для печати необходимо выбрать путевой лист");
    };

    sc.editBtnClick = function () {
        if (sc.selectedItems[0]) {
            console.info('edit  ' + sc.selectedItems[0].WayBillId);
            $location.path('/driver_way_bill_item/' + sc.selectedItems[0].WayBillId);
        }
    };

    sc.createBtnClick = function () {
        $location.path('/driver_way_bill_item/');
    };

    sc.dateChange = function () {
       // sc.filterText =  null;
        if (sc.dateDriverWayBills) {
            $cookieStore.put('dateDriverWayBillsCalendar', {date: sc.dateDriverWayBills});
            getDriverWayBillsList();
        } else {
            console.log('wrong date from datepick');
        }
    };

          	 sc.search = function() {
		if (sc.filterText.length == 0) {
		//sc.openRepair();
		} else { 
	  //  $("#datepick").addClass("crystalbutton");
		}
		
      if ($cookieStore.get('timerId2')) {
	   clearInterval($cookieStore.get('timerId2'));
   }
    $cookieStore.put('timerId2', setTimeout(getDriverWayBillsList, 1000));	
	
	};



    function getDriverWayBillsList() {
        sc.fullLoads = true;
        requests.serverCall.post($http, "DriverWayBillsCtrl/GetDriverWayBillsList", {
            DateFilter: moment(sc.dateDriverWayBills).format("MM.DD.YYYY"),
			DateFilterEnd: moment(sc.dateDriverWayBillsEnd).format("MM.DD.YYYY"),
            OrgId: curUser.orgid,
			TporgId : curUser.tporgid,
			RegNum: sc.filterText,
        }, function (data, status, headers, config) {
            console.info(data);
            sc.tableData = data;
            if (sc.tableData.length > 0) {
                if ($cookieStore.get('selectedDriverWayBill')) {
                    selectRowByField("WayBillId", $cookieStore.get('selectedDriverWayBill').id, true);
                    $cookieStore.remove('selectedDriverWayBill');
                } else selectRowByField("WayBillId", sc.tableData[0]["WayBillId"], true);
            }
        });
    }

    function selectRowByField(field, val, select) {
        sc.$on('ngGridEventData', function () {
            sc.tableData.forEach(function (item, i, arr) {
                if (item[field] == val) {
                    sc.gridOptions.selectRow(i, select);
                    if (sc.gridOptions.ngGrid) sc.gridOptions.ngGrid.$viewport.scrollTop(sc.gridOptions.ngGrid.rowMap[i] * sc.gridOptions.ngGrid.config.rowHeight);
                    if (select == true) {
                        sc.selectedItems = [];
                        sc.selectedItems.push(item);
                    }
                }
            });
        });
    }
}]);