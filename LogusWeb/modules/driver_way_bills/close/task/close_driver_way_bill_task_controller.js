/**
 *  @ngdoc controller
 *  @name DriverWayBill.controller:CloseDriverWayBillTaskCtrl
 *  @description
 *  # CloseDriverWayBillTaskCtrl
 *  Это контроллер, определеяющий поведение формы создания/редактирования задач при закрытии путевого листа водителя
 *  @requires $scope
 *  @requires $http
 *  @requires requests
 *  @requires $location
 *  @requires $stateParams
 *  @requires $cookieStore
 *  @requires modals
 *  @requires modalType
 *  @requires rootScope

 *  @property {Object} sc: scope
 *  @property {Object} curUser: данные о пользователе
 *  @property {int} way_bill_id: id редактируемого путевого листа
 *  @property {int} task_id: id редактируемой задачи путевого листа
 *  @property {Object} sc.formData: данные путевого листа
 *  @property {Object} cookieDate: выбранная дата. Устанавливается как дата начала путевого листа
 *  @property {Object} sc.infoData справочники
 *  @property {Object} sc.selectedRow выбранная задача
 *  @property {Object} tasksColumnDefinitions столбцы таблицы задач
 *  @property {Object} sc.gridOptions настройки таблицы задач
 
 *  @property {function} getTaskFormData: получить информацию о задании
 *  @property {function} sc.addComment: добавить комментарий через модальное окно
 *  @property {function} sc.refreshLayer: обновление карты
 *  @property {function} sc.submitForm: выход с сохранением
 *  @property {function} sc.changeConsumptionFact: пересчет стоимости топлива при изменении значения факт. расхода
 *  @property {function} sc.cancelClick: выход без сохранения изменений
 */
var app = angular.module("CloseDriverWayBillTask", ["ngGrid", "services", 'ngCookies', 'modalWindows']);

app.controller("CloseDriverWayBillTaskCtrl", ["$scope", "$http", "requests", "$location", '$cookieStore', '$rootScope', "modals", "modalType", "$stateParams", 'LogusConfig', 'map', function ($scope, $http, requests, $location, $cookieStore, $rootScope, modals, modalType, $stateParams, LogusConfig, map) {
    console.log('CloseDriverWayBillTaskCtrl');
    var sc = $scope;
    var curUser = $cookieStore.get('currentUser');
     sc.tporgid = curUser.tporgid;

    var way_bill_id = $stateParams.way_bill_id;
    var task_id = $stateParams.task_id;
    if (!way_bill_id) {
        $location.path('/driver_way_bills');
    }

    sc.formData = {};
	sc.infoData2 = [];
	sc.formData2 = [];
    sc.infoData2.IceBoxList =  [{ "Name":"%","Id":1 },{"Name":"л/ч","Id":2}];
	sc.formData2.TypeIceBoxId = 1;
	
	sc.checkRes = function () {				
			var Orgid = null;
			if (sc.AllResource != true) {
				Orgid = curUser.orgid;
			} 
			
			     requests.serverCall.post($http, "DriverWayBillsCtrl/GetInfoForDriverWayBillItem", { OrgId: Orgid },
				 function (data, status, headers, config) {
                    console.info(data);
                    sc.infoData = data;
                }, function () {});	
			} 
	
    requests.serverCall.post($http, "DriverWayBillsCtrl/GetInfoForDriverWayBillItem", {OrgId : curUser.orgid, TaskId: $stateParams.task_id}, function (data, status, headers, config) {
        console.info(data);
        sc.infoData = data;
        getTaskFormData();
    });

    function getTaskFormData() {
        if (task_id) {
            requests.serverCall.post($http, "DriverWayBillsCtrl/GetCloseDriverWayBillTaskById", task_id, function (data, status, headers, config) {
                console.info(data);
                sc.formData = data;
				sc.AllResource = data.AllResource;
			 if (sc.formData.Coef) {sc.formData.IsWeigth = true;}
				if (sc.formData.CheckNeedChange == null) {
				if (!sc.formData.Icebox_per) {
					sc.formData2.TypeIceBoxId = 2;
					sc.formData.Icebox2 = sc.formData.Icebox;
				} else {
					sc.formData.Icebox2 = sc.formData.Icebox_per;
				}
				}
				else {
				//	console.log('fgsdfg');
				sc.formData2.TypeIceBoxId = sc.formData.DemIceBox;	
			    sc.formData.Icebox2 = sc.formData.Icebox;	
				}
				
				if (!sc.formData.MileageCargo) {
					sc.formData.MileageCargo = 0;
				}
				
				
		       	 addRateUpdate();
			 
					if (sc.formData.CheckNeedChange == null) {
               	sc.formData.TypePrice = 4;
					}
            });
            return true;
        } else {
            addRateUpdate();
			loadRateFuel() ;
        }
        return false;
    };

	sc.refreshFuelRate = function () {
		//disabledbutton
		if (sc.formData2.TypeIceBoxId == 1) {
			jQuery('#icebox').addClass('disabledbutton');
			sc.formData.Icebox_hour = null;
		} else {
			jQuery('#icebox').removeClass('disabledbutton');
		}
		
		if  (sc.formData.CheckNeedChange == null)  { 
		if (sc.formData2.TypeIceBoxId == 1) {
			sc.formData.Icebox2 = sc.formData.Icebox_per;
		} else {
			sc.formData.Icebox2 = sc.formData.Icebox;
		}
		}
	}
	  function loadRateFuel() {
		  var Car = true;  var Tech = true;  var Sheet_id = way_bill_id;
		requests.serverCall.post($http, "RateFuel/Get", {Car, Tech, Sheet_id  }, function (data, status, headers, config) {
	  //  sc.formData = data;
	  if (data[0].CheckSeason) {   //лето
		 sc.formData.CoefCity = data[0].Summer_city;  
		 sc.formData.CoefField = data[0].Summer_field; 
		 sc.formData.CoefTrack = data[0].Summer_track; 
	  }  else {
		 sc.formData.CoefCity = data[0].Winter_city;  
		 sc.formData.CoefField = data[0].Winter_field; 
		 sc.formData.CoefTrack = data[0].Winter_track;   
	  }
	     sc.formData.Icebox_per = data[0].Icebox_per;
	     sc.formData.Icebox = data[0].Icebox;
		 
		sc.formData.CoefTrailer = data[0].Trailer;
		sc.formData.CoefFuel = data[0].Refill;
		sc.formData.CoefSink = data[0].Sink;
		
		 		if (!sc.formData.CoefSetting) {
					sc.formData.CoefSetting = data[0].Setting;
				}
		 
		 
	  //рефрежиратор
	   if (!sc.formData.Icebox_per) {
	   sc.formData2.TypeIceBoxId = 2;
	   sc.formData.Icebox2 = sc.formData.Icebox;
		} else {
		sc.formData.Icebox2 = sc.formData.Icebox_per;
		}
	   sc.formData.Body_lifting = data[0].Body_lifting;
	   sc.formData.Heating = data[0].Heating;
	   sc.formData.Engine_heating = data[0].Engine_heating;
	   sc.formData.Coef = data[0].Coef;
	   if (sc.formData.Coef) {sc.formData.IsWeigth = true;}
	  ///
		
		});
	  }
	
	
	sc.changeMileageTotal = function () {
		if (sc.formData.MileageOnTrack == null || sc.formData.MileageOnTrack=="") {sc.formData.MileageOnTrack = 0;}
		if (sc.formData.MileageOnGround == null || sc.formData.MileageOnGround=="") {sc.formData.MileageOnGround = 0;}
		if (sc.formData.MileageCity == null || sc.formData.MileageCity=="") {sc.formData.MileageCity=0;}
		sc.formData.MileageTotal = parseFloat(sc.formData.MileageOnTrack) + parseFloat(sc.formData.MileageOnGround) +
		parseFloat(sc.formData.MileageCity) ;
	}
	
	
	
    function addRateUpdate() {
        sc.refreshRate = function () {
            if (sc.formData.TaskTypeId) {
                requests.serverCall.post($http, "WaysListsCtrl/GetRate", {
                    IdTptas: sc.formData.TaskTypeId,
                    IdTrakt: sc.formData.TraktorId,
                    IdEqui: sc.formData.EquipmentId,
                    IdPlant: sc.formData.PlantId,
					TypeSheet : 2,
					SheetId : way_bill_id,
                }, function (data, status, headers, config) {
                    console.log(data);
                    sc.formData.RateShift = data.RateShift;
					if (sc.formData.TypePrice == -1) {
					sc.formData.RatePiecework = null;	
					} else {
                    sc.formData.RatePiecework = data.RatePiecework;
					}
                    sc.formData.IdRate = data.Id;
                }, function () {}, false, false);
            }
        };
		if (sc.formData.CheckNeedChange == null) {
		sc.formData.TypePrice = 4;
		}
		sc.refreshRate();
    }

         sc.typePriceChange = function () {
			  console.log(sc.formData.TypePrice);
			if (sc.formData.TypePrice == -1) {
					sc.formData.RatePiecework = null;	
					} else {
               if (sc.refreshRate) {  sc.refreshRate();}
					}

		 }
	
    sc.addComment = function () {
        modals.showWindow(modalType.WIN_COMMENT, {
            nameField: "Комментарий",
            comment: sc.formData.Comment
        })
            .result.then(function (result) {
                console.info(result);
                sc.formData.Comment = result;
            }, function () {
                console.info('Modal dismissed at: ' + new Date());
            });
    };

    sc.refreshLayer = function (data) {
        sc.formData.Area = 0;
        var url = LogusConfig.serverUrl + 'WaysListsCtrl/GetCloseInfoForMapByField?field_id=' + sc.formData.FieldId;
        console.log('refreshLayer ' + url);
        map.refreshField(url);
    };

    sc.submitForm = function () {
        sc.formData.WayBillId = way_bill_id;
		sc.formData.DemIceBox  = sc.formData2.TypeIceBoxId;	
		sc.formData.Icebox = sc.formData.Icebox2;
        requests.serverCall.post($http, "DriverWayBillsCtrl/SubmitCloseDriverWayBillTask", sc.formData, function (data, status, headers, config) {
            console.info(data);
            $location.path('/close_driver_way_bill/' + way_bill_id);
        });
    };

       sc.cancelClick = function ()   {
        $location.path('/close_driver_way_bill/' + way_bill_id);
      };

    sc.changeConsumptionFact = function() {
    sc.formData.FuelCost = Math.round(sc.formData.ConsumptionFact * sc.formData.TypeFuel.Price * 100) / 100;
    };
	
	  function checkValue(value) {
       if (value == null || value=="" || isNaN(value) || !isFinite(value)) {
		   return 0;
		  }
		  else {
		return value;
		  }
	  }
	
	sc.checkWeigth = function() {	
	if (!sc.formData.IsWeigth) {sc.formData.Coef = null ;}
	}
	
	
	//расчет расхода топлива
	sc.countConsFact = function() {	
		var mileage = (checkValue(sc.formData.CoefCity)*checkValue(sc.formData.MileageCity) + checkValue(sc.formData.CoefField)*checkValue(sc.formData.MileageOnGround) +
		checkValue(sc.formData.CoefTrack)*checkValue(sc.formData.MileageOnTrack));
		
		var icebox = 0;
		if (sc.formData2.TypeIceBoxId == 2) {
		 icebox = checkValue(sc.formData.Icebox2) * checkValue(sc.formData.Icebox_hour);
		} else {
			icebox = (mileage/100) * (checkValue(sc.formData.Icebox2)/100);
		}
		//console.log(icebox);
		var numRuns = 0;
		if (sc.formData.IsWeigth) {numRuns = sc.formData.NumRuns;} else {
			numRuns = 1;
		}
		
	   sc.formData.ConsumptionFact = mileage/100 +  (checkValue(sc.formData.NumLifts) *  checkValue(sc.formData.Body_lifting)) + 
		(checkValue(sc.formData.Engine_heating) * checkValue(sc.formData.Engine_heating_hour)) + (checkValue(sc.formData.Heating) * checkValue(sc.formData.Heating_hour)) + icebox
		+ (checkValue(sc.formData.Coef)*checkValue(sc.formData.Volumefact)*
		checkValue((checkValue(sc.formData.MileageCargo)/100/ checkValue(numRuns)))) + (checkValue(sc.formData.NumFuel)*checkValue(sc.formData.CoefFuel))   +
        (checkValue(sc.formData.NumSink)*checkValue(sc.formData.CoefSink)) 	+ 
		( checkValue(sc.formData.MileageTrailer)/100* checkValue(sc.formData.Trailerfact)*checkValue(sc.formData.CoefTrailer)  ) + 
         (checkValue(sc.formData.Setting) * checkValue(sc.formData.CoefSetting)) 		;
		
		sc.formData.ConsumptionFact = sc.formData.ConsumptionFact.toFixed(1);
		
	}
	
	
	
	
	
	
}]);