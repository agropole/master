/**
 *  @ngdoc controller
 *  @name DriverWayBill.controller:CloseDriverWayBill
 *  @description
 *  # DriverWayBillItem
 *  Это контроллер, определеяющий поведение формы закрытия путевого листа водителя
 *  @requires $scope
 *  @requires $http
 *  @requires requests
 *  @requires $location
 *  @requires $stateParams
 *  @requires $cookieStore
 *  @requires modals
 *  @requires modalType

 *  @property {Object} sc: scope
 *  @property {Object} curUser: данные о пользователе
 *  @property {int} way_bill_id: id путевого листа
 *  @property {Object} sc.formData: данные путевого листа
 *  @property {Object} sc.infoData справочники
 *  @property {Object} refuilingsData данные по топливу
 *  @property {Object} sc.refuilingsFormData отображаемые данные по топливу
 *  @property {Object} sc.uigridOptions настройки таблицы задач
 
 *  @property {function} ReloadCloseDriverWayBillData: перезагрузить информацию о путевом листе
 *  @property {function} needSelection: открыть модальное окно с требованием выделения строки
 *  @property {function} sc.refuelingClick: открыть окно с дозаправками
 *  @property {function} setRefuilingsDataByOrderNum
 *  @property {function} sc.closeRefuelingModal закрытие модального окна дозаправок
 *  @property {function} sc.cancelRefuelingModal не сохранять изменения о дозаправках
 *  @property {function} sc.clearClick очистить информацию о дозаправках
 *  @property {function} sc.addTask: добавление новой задачи
 *  @property {function} sc.cloneTask: копирование задачи (должнра быть выбрана строка в таблице)
 *  @property {function} sc.editTask: редактирование задачи (должнра быть выбрана строка в таблице)
 *  @property {function} sc.removeTask: удаление задачи (должнра быть выбрана строка в таблице)
 *  @property {function} sc.closeWayBill:  сохранить и закрыть путевой лист
 *  @property {function} sc.submitForm: закрытие формы с сохранением
 *  @property {function} sc.cancelClick: закрытие формы без сохранения
 */

var app = angular.module("CloseDriverWayBill", ["ngGrid", "services", 'ngCookies', 'modalWindows']);

app.controller("CloseDriverWayBillCtrl", ["$scope", "$http", "requests", "$location", '$cookieStore', "modals", "modalType", "$stateParams", function ($scope, $http, requests, $location, $cookieStore, modals, modalType, $stateParams) {
    var sc = $scope;

    var curUser = $cookieStore.get('currentUser');

    var way_bill_id = $stateParams.way_bill_id;
     sc.tporgid = curUser.tporgid;
    sc.formData = {};
    var refuilingsData = [];
    sc.refuilingsFormData = {};

    sc.uigridOptions = {
        enableRowSelection: true,
        rowHeight: 48,
        enableRowHeaderSelection: false,
        multiSelect: false
    };
    sc.uigridOptions.columnDefs = [
        {name: 'FieldName', displayName: '№ поля', width: '7%'},
        {name: 'PlantName', displayName: 'Культура', width: '10%'},
        {name: 'TaskName', displayName: 'Выполненные работы', width: '18%'},
        {name: 'EquipmentName', displayName: 'Оборудование', width: '14%'},
        {
            name: 'WorkTime',
            displayName: 'Отработано часов',
            headerCellTemplate: 'templates/headerVertical.html',
            width: '4%'
        },
        {
            name: 'WorkDay',
            displayName: 'Норма выработки',
            headerCellTemplate: 'templates/headerVertical.html',
            width: '4%'
        },
        {
            name: 'Salary',
            displayName: 'Расценка',
            headerCellTemplate: 'templates/headerVertical.html',
            width: '4%'
        },
        {
            name: 'Volumefact',
            displayName: 'Перевезено, тонн',
            headerCellTemplate: 'templates/headerVertical.html',
            width: '4%'
        },
        {
            name: 'MileageTotal',
            displayName: 'Пробег общий',
            headerCellTemplate: 'templates/headerVertical.html',
            width: '4%'
        },
        {
            name: 'NumRuns',
            displayName: 'Количество рейсов',
            headerCellTemplate: 'templates/headerVertical.html',
            width: '4%'
        },
        {
            name: 'ConsumptionFact',
            displayName: 'Расх. гор. факт',
            headerCellTemplate: 'templates/headerVertical.html',
            width: '4%'
        },
        {
            name: 'PriceTotal',
            displayName: 'Общая оплата',
            headerCellTemplate: 'templates/headerVertical.html',
            width: '4%'
        },
        {
            name: 'PriceAdd',
            displayName: 'Доп. оплата',
            headerCellTemplate: 'templates/headerVertical.html',
            width: '4%'
        },
        {name: 'AvailableName', displayName: 'Ответственный', width: '15%'}
    ];

    $scope.uigridOptions.onRegisterApi = function (gridApi) {
        //set gridApi on scope
        $scope.gridApi = gridApi;
    };

    var ReloadCloseDriverWayBillData = function () {
        if (way_bill_id) {
            requests.serverCall.post($http, "DriverWayBillsCtrl/GetCloseDriverWayBillItemById", way_bill_id, function (data, status, headers, config) {
                console.info(data);
                sc.formData = data;
				sc.AllResource = data.AllResource;
                sc.uigridOptions.data = sc.formData.Tasks;
                $cookieStore.put('selectedDriverWayBill', {id: way_bill_id});
            });
            return true;
        }
        return false;
    };

    if (!ReloadCloseDriverWayBillData()) {
        $location.path('/driver_way_bills');
    }

    // модальное окно с требованием выделения строки
    function needSelection(title, message) {
        modals.showWindow(modalType.WIN_MESSAGE, {
            title: title,
            message: message,
            callback: function () {
            }
        });
    }
	
	sc.checkRes = function () {				
			var Orgid = null;
			if (sc.AllResource != true) {
				Orgid = curUser.orgid;
			} 
			      requests.serverCall.post($http, "WaysListsCtrl/GetRefuellerInfo", {OrgId : Orgid}, function (data, status, headers, config) {
					console.info(data);
					sc.refuellers1 = data;
					sc.refuellers2 = data;
					sc.refuellers3 = data;
					});
			}
	

    requests.serverCall.post($http, "WaysListsCtrl/GetRefuellerInfo", {OrgId : curUser.orgid, WaysListId:$stateParams.way_bill_id}, function (data, status, headers, config) {
        console.info(data);
        sc.refuellers1 = data;
        sc.refuellers2 = data;
        sc.refuellers3 = data;
    });

    sc.refuelingClick = function () {
        requests.serverCall.post($http, "DriverWayBillsCtrl/UpdateCloseDriverWayBillItem", sc.formData, function (data, status, headers, config) {
            console.info(data);
        });
        $("#refueling_modal").show().css({opacity: 1, display: "block"});
        sc.refuilingsFormData.Volume1 = sc.refuilingsFormData.NameId1 = '';
        sc.refuilingsFormData.Volume2 = sc.refuilingsFormData.NameId2 = '';
        sc.refuilingsFormData.Volume3 = sc.refuilingsFormData.NameId3 = '';
        requests.serverCall.post($http, "WaysListsCtrl/GetRefuelingsByIdList", $stateParams.way_bill_id, function (data, status, headers, config) {
            refuilingsData = data;
            console.info(data);
            for (var j = 0, jl = refuilingsData.length; j < jl; j++) {
                if (refuilingsData[j].OrderNum == 1) {
                    sc.refuilingsFormData.Volume1 = refuilingsData[j].Volume;
                    sc.refuilingsFormData.NameId1 = refuilingsData[j].NameId;
                } else if (refuilingsData[j].OrderNum == 2) {
                    sc.refuilingsFormData.Volume2 = refuilingsData[j].Volume;
                    sc.refuilingsFormData.NameId2 = refuilingsData[j].NameId;
                } else if (refuilingsData[j].OrderNum == 3) {
                    sc.refuilingsFormData.Volume3 = refuilingsData[j].Volume;
                    sc.refuilingsFormData.NameId3 = refuilingsData[j].NameId;
                }
            }
        });
    };

    function setRefuilingsDataByOrderNum(orderNum, volume, name) {
        if (volume) {
            var refuilBe = false;
            refuilingsData.forEach(function (item, i, arr) {
                if (item.OrderNum == orderNum) {
                    item.Volume = volume;
                    item.NameId = name;
                    refuilBe = true;
                }
            });
            if (refuilBe == false) {
                refuilingsData.push({
                    OrderNum: orderNum,
                    Volume: volume,
                    NameId: name
                });
            }
        } else {
            refuilingsData.forEach(function (item, i, arr) {
                if (item.OrderNum == orderNum) {
                    refuilingsData.splice(i, 1);
                }
            })
        }
    }

    sc.closeRefuelingModal = function () {
        sc.fullLoads = true;
        setRefuilingsDataByOrderNum(1, sc.refuilingsFormData.Volume1, sc.refuilingsFormData.NameId1);
        setRefuilingsDataByOrderNum(2, sc.refuilingsFormData.Volume2, sc.refuilingsFormData.NameId2);
        setRefuilingsDataByOrderNum(3, sc.refuilingsFormData.Volume3, sc.refuilingsFormData.NameId3);
        requests.serverCall.post($http, "WaysListsCtrl/UpdateRefuelings", {
            WaysListId: $stateParams.way_bill_id,
            Refuelings: refuilingsData
        }, function (data, status, headers, config) {
            console.info(data);
         //   requests.serverCall.post($http, "DriverWayBillsCtrl/UpdateCloseDriverWayBillItem", sc.formData, function (data, status, headers, config) {
                console.info(data);
                ReloadCloseDriverWayBillData();
                $("#refueling_modal").css({opacity: 0, display: "none"});
          //  });
        });
    };

    sc.cancelRefuelingModal = function () {
        $("#refueling_modal").css({opacity: 0, display: "none"});
    };

    sc.clearClick = function (num) {
        if (num == 1) sc.refuilingsFormData.Volume1 = sc.refuilingsFormData.NameId1 = '';
        else if (num == 2) sc.refuilingsFormData.Volume2 = sc.refuilingsFormData.NameId2 = '';
        else if (num == 3) sc.refuilingsFormData.Volume3 = sc.refuilingsFormData.NameId3 = '';
    };


    sc.addTask = function () {
        requests.serverCall.post($http, "DriverWayBillsCtrl/UpdateCloseDriverWayBillItem", sc.formData, function (data, status, headers, config) {
            console.info(data);
        });
        $location.path('/close_driver_way_bill_task/' + way_bill_id + '/');
    };

    // коприрование задания
    sc.cloneTask = function () {
        // if (sc.selectedRow.length > 0) {
        /* var detailIdArr = [];
         for (var i = 0, il = sc.selectedRow.length; i < il; i++) {
         detailIdArr.push(sc.selectedRow[i]["DetailId"]);
         }*/
        requests.serverCall.post($http, "DriverWayBillsCtrl/UpdateCloseDriverWayBillItem", sc.formData, function (data, status, headers, config) {
            console.info(data);
        });
        var selRow = $scope.gridApi.selection.getSelectedRows()[0];
        if (selRow && selRow.TaskId) {
            requests.serverCall.post($http, "DriverWayBillsCtrl/CopyTaskInDriverWayList", selRow.TaskId, function (data, status, headers, config) {
                sc.fullLoads = false;
                console.info(data);
                ReloadCloseDriverWayBillData();
            });
        } else needSelection("Копирование", "Для копирования необходимо выбрать задание");
    };

    sc.editTask = function () {
        var selRow = $scope.gridApi.selection.getSelectedRows()[0];
        if (selRow && selRow.TaskId) {
            requests.serverCall.post($http, "DriverWayBillsCtrl/UpdateCloseDriverWayBillItem", sc.formData, function (data, status, headers, config) {
                console.info(data);
            });
            console.info('edit task ' + selRow.TaskId);
            $location.path('/close_driver_way_bill_task/' + way_bill_id + '/' + selRow.TaskId);
        } else needSelection("Редактирование", "Для редактирования необходимо выбрать задание");
    };

    sc.removeTask = function () {
        var selRow = $scope.gridApi.selection.getSelectedRows()[0];
        if (selRow && selRow.TaskId) {
            requests.serverCall.post($http, "DriverWayBillsCtrl/UpdateCloseDriverWayBillItem", sc.formData, function (data, status, headers, config) {
                console.info(data);
            });
            modals.showWindow(modalType.WIN_DIALOG, {
                    title: "Удаление", message: "Удалить выбранное задание?", callback: function () {
                        console.info('delete task ' + selRow.TaskId);
                        requests.serverCall.post($http, "DriverWayBillsCtrl/DeleteDriverWayBillTaskById", $scope.gridApi.selection.getSelectedRows()[0].TaskId, function (data, status, headers, config) {
                            console.info(data);
                            ReloadCloseDriverWayBillData();
                        });
                    }

                }
            );
        } else needSelection("Удаление", "Для удаления необходимо выбрать задание");
    };

    sc.closeWayBill = function () {
        requests.serverCall.post($http, "DriverWayBillsCtrl/UpdateCloseDriverWayBillItem", sc.formData, function (data, status, headers, config) {
            console.info(data);
            requests.serverCall.post($http, "DriverWayBillsCtrl/SetClosedStatusDriverWayBill", way_bill_id, function (data, status, headers, config) {
                console.info(data);
                $location.path('/driver_way_bills');
            });
        });
    };

    sc.submitForm = function () {
        if (sc.formData.timeStartFact && sc.formData.timeStartFact.length == 4)
            sc.formData.timeStartFact = sc.formData.timeStartFact.substring(0, 2) + ':' + sc.formData.timeStartFact.substring(2, 4);

        if (sc.formData.timeEndFact && sc.formData.timeEndFact.length == 4)
            sc.formData.timeEndFact = sc.formData.timeEndFact.substring(0, 2) + ':' + sc.formData.timeEndFact.substring(2, 4);
        requests.serverCall.post($http, "DriverWayBillsCtrl/UpdateCloseDriverWayBillItem", sc.formData, function (data, status, headers, config) {
            console.info(data);
            $cookieStore.put('selectedDriverWayBill', {id: way_bill_id});
            $location.path('/driver_way_bills');
        });
    };

    sc.cancelClick = function () {
        $location.path('/driver_way_bills');
    }
}]);