/**
 *  @ngdoc controller
 *  @name DriverWayBill.controller:DriverWayBillItem
 *  @description
 *  # DriverWayBillItem
 *  Это контроллер, определеяющий поведение формы создания/редактирования путевого листа водителя
 *  @requires $scope
 *  @requires $http
 *  @requires requests
 *  @requires $location
 *  @requires $stateParams
 *  @requires $cookieStore
 *  @requires modals
 *  @requires modalType
 *  @requires rootScope

 *  @property {Object} sc: scope
 *  @property {Object} curUser: данные о пользователе
 *  @property {int} itemId: id редактируемого путевого листа
 *  @property {int} shiftTaskId: id сменного задания, из которого был создан путевой лист
 *  @property {Object} sc.formData: данные путевого листа
 *  @property {Object} cookieDate: выбранная дата. Устанавливается как дата начала путевого листа
 *  @property {Object} sc.infoData справочники
 *  @property {Object} sc.selectedRow выбранная задача
 *  @property {Object} tasksColumnDefinitions столбцы таблицы задач
 *  @property {Object} sc.gridOptions настройки таблицы задач
 
 *  @property {function} sc.submitForm: сохранить Путевой лист
 *  @property {function} sc.cancelWayBillClick: выход без сохранения изменения в общей информации путевых листов
 *  @property {function} sc.addTask: кнопка Добавить - новая задача в текущий путевой лист
 *  @property {function} sc.removeTask: кнопка Удалить - удаление выбранной задачи
 *  @property {function} sc.editCell: кнопка Редактировать - изменение информации в выбранной задачи
 *  @property {function} sc.updateSelectedCell: редактирование свойсвт задачи - открытие справочных списков
 *  @property {function} sc.traktorChange: событие изменения Техники - автоматический выбор типа топлива
 */

var app = angular.module("DriverWayBillItem", ["ngGrid", "services", 'ngCookies', 'modalWindows']);

app.controller("DriverWayBillItemCtrl", ["$scope", "$http", "requests", "$location", '$cookieStore', '$rootScope', "modals", "modalType", "$stateParams", function ($scope, $http, requests, $location, $cookieStore, $rootScope, modals, modalType, $stateParams) {
    console.log('DriverWayBillItemCtrl');
    var sc = $scope;
    var curUser = $cookieStore.get('currentUser');//$cookieStore.get('globals').currentUser;
    var itemId = $stateParams.item_id;
    var shiftTaskId = $rootScope.CreateWayBillOnShiftTaskId;
    $rootScope.CreateWayBillOnShiftTaskId = null;
    sc.tporgid = curUser.tporgid;

    if (isNaN(itemId) || itemId == '') {
        itemId = null;
    }

    sc.formData = {
        TableTasksData: [],
        WayBillId: itemId,
        TypeFuel: 3
    };

    var cookieDate = $cookieStore.get('dateDriverWayBillsCalendar');
  //  if (cookieDate && cookieDate.date) {
    //    sc.formData.DateBegin = cookieDate.date;
  //  }

    sc.infoData = {};
    sc.selectedRow = [];

    var tasksColumnDefinitions = [
        {
            field: "FieldInfo", displayName: "Поле", width: "10%",
            cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()" ng-click="editCell(row.entity, row.getProperty(col.field), col.field)">' +
            '<input value="{{row.entity[col.field][\'Name\']}}" placeholder="Поле" style="border: none;"  data-ng-click="updateSelectedCell(col.field, row.entity[col.field][\'Id\'],\'Поле\',row.entity,$event)"  readonly>' +
            '</div>'
        },
        {
            field: "PlantInfo", displayName: "Культура", width: "10%",
            cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()" ng-click="editCell(row.entity, row.getProperty(col.field), col.field)"">' +
            '<input value="{{row.entity[col.field][\'Name\']}}" placeholder="Культура" style="border: none;{{row.entity[col.field][\'RepairStatus\']}}"  data-ng-click="updateSelectedCell(col.field, row.entity[col.field][\'Id\'], \'Культура\',row.entity,$event)" readonly>' +
            '</div>'
        },
        {
            field: "TypeTaskInfo", displayName: "Задание", width: "25%",
            cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()" ng-click="editCell(row.entity, row.getProperty(col.field), col.field)">' +
            '<input value="{{row.entity[col.field][\'Name\']}}" placeholder="Задание" style="border: none;"  data-ng-click="updateSelectedCell(col.field, row.entity[col.field][\'Id\'],\'Планируемые работы\',row.entity,$event)" readonly>' +
            '</div>'
        },
        {
            field: "EquipInfo", displayName: "Оборудование", width: "30%",
            cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()" ng-click="editCell(row.entity, row.getProperty(col.field), col.field)">' +
            '<input value="{{row.entity[col.field][\'Name\']}}" placeholder="Оборудование" style="border: none;"  data-ng-click="updateSelectedCell(col.field, row.entity[col.field][\'Id\'],\'Оборудование\',row.entity,$event)" readonly>' +
            '</div>'
        },
        {
            field: "AvailableInfo", displayName: "Ответственный", width: "20%",
            cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()" ng-click="editCell(row.entity, row.getProperty(col.field), col.field)">' +
            '<input value="{{row.entity[col.field][\'Name\']}}" placeholder="Ответственный" style="border: none;"  data-ng-click="updateSelectedCell(col.field, row.entity[col.field][\'Id\'],\'Ответственный\',row.entity,$event)"  readonly>' +
            '</div>'
        },
        {
            field: "Comment", displayName: " ", width: "5%",
            cellTemplate: 'templates/comment_shift_template.html', cellClass: 'cellToolTip'
        }
    ];

    sc.gridOptions = {
        data: 'formData.TableTasksData',
        enableColumnResize: true,
        multiSelect: false,
        enableRowSelection: true,
        enablePaging: true,
        showFooter: false,
        selectedItems: sc.selectedRow,
        rowHeight: 49,
        headerRowHeight: 49,
        columnDefs: tasksColumnDefinitions
    };

    sc.submitForm = function () {
        $cookieStore.put('dateDriverWayBillsCalendar', {date: sc.formData.DateBegin});

        var formDataSend = angular.copy(sc.formData);
        formDataSend.DateBegin = moment(formDataSend.DateBegin).format("MM.DD.YYYY");

        formDataSend.OrgId = curUser.orgid;
        formDataSend.UserId = curUser.userid;
        formDataSend.ShiftTaskId = shiftTaskId;
        formDataSend.TypeFuel = sc.formData.TypeFuel;

        if (formDataSend.timeStart && formDataSend.timeStart.length == 4)
            formDataSend.timeStart = formDataSend.timeStart.substring(0, 2) + ':' + formDataSend.timeStart.substring(2, 4);

        if (formDataSend.timeEnd && formDataSend.timeEnd.length == 4)
            formDataSend.timeEnd = formDataSend.timeEnd.substring(0, 2) + ':' + formDataSend.timeEnd.substring(2, 4);

        requests.serverCall.post($http, "DriverWayBillsCtrl/SubmitDriverWayBillItem", formDataSend, function (data, status, headers, config) {
            console.info(data);
            $cookieStore.put('selectedDriverWayBill', {id: data});
            $cookieStore.put('selectedShiftTask', {id: shiftTaskId});
            if (shiftTaskId != null) {
                requests.serverCall.post($http, "WaysListsCtrl/ApproveShiftTaskDetail", shiftTaskId, function (data, status, headers, config) {
                    if (shiftTaskId == null)
                        $location.path('/driver_way_bills');
                    else
                        $location.path('/shift_task');
                });
            } else {
                if (shiftTaskId == null)
                    $location.path('/driver_way_bills');
                else
                    $location.path('/shift_task');
            }
        });
    };

      	sc.checkRes = function () {				
			var Orgid = null;
			if (sc.formData.AllResource != true) {
				Orgid = curUser.orgid;
			} 
			
			     requests.serverCall.post($http, "DriverWayBillsCtrl/GetInfoForDriverWayBillItem", {OrgId : Orgid},
				 function (data, status, headers, config) {
                    console.info(data);
                    sc.infoData = data;
                }, function () {});	
			} 
	
	
	
    sc.cancelWayBillClick = function () {
        if (shiftTaskId == null)
            $location.path('/driver_way_bills');
        else
            $location.path('/shift_task');
    };

    sc.addTask = function () {
        var newTask = {
            AvailableInfo: {Id: "", Name: ""},
            TaskId: "",
            EquipInfo: {Id: "", Name: ""},
            FieldInfo: sc.formData.IsRepair ? {Id: 87, Name: "ГММ Орлово"} : {Id: "", Name: ""},
            PlantInfo: {Id: "", Name: ""},
            TypeTaskInfo: sc.formData.IsRepair ? {Id: 123, Name: "Текущий ремонт и ТО автотранспорта"} : {Id: "", Name: ""},
            Comment: null
        };
        sc.formData.TableTasksData.push(newTask);
    };

    sc.removeTask = function () {
        console.log(sc.selectedRow);
        if (sc.selectedRow) {
            var row = null;
            if (sc.selectedRow instanceof Array && sc.selectedRow.length > 0 && sc.selectedRow[0]) {
                row = sc.selectedRow[0];
            } else if (typeof sc.selectedRow == 'object' && !(sc.selectedRow instanceof Array)) {
                row = sc.selectedRow;
            }
            if (row) {
                var idx = sc.formData.TableTasksData.indexOf(row);
                if (idx > -1) {
                    sc.formData.TableTasksData.splice(idx, 1);
                    sc.selectedRow = null;
                }
            }
        }
    };

    sc.editCell = function (row, cell, column) {
        sc.selectedRow = row;
        sc.selectedCell = cell;
        sc.selectedColumn = column;
        // вызывается при редактировании ячейки с комментарием
        if (column == 'Comment') {
            modals.showWindow(modalType.WIN_COMMENT, {
                nameField: "Комментарий",
                comment: cell
            })
                .result.then(function (result) {
                    console.info(result);
                    sc.selectedRow['Comment'] = result;
                }, function () {
                    console.info('Modal dismissed at: ' + new Date());
                });
        }
    };

    sc.updateSelectedCell = function (colField, selectedVal, displayName, row) {
        sc.selectedRow = row;
        sc.colField = colField;
        var modalValues;
        if (sc.infoData) {
            switch (sc.colField) {
                case "PlantInfo":
                    if (sc.selectedRow["FieldInfo"] && sc.selectedRow["FieldInfo"]["Id"]) {
                        for (var i = 0, il = sc.infoData.FieldsAndPlants.length; i < il; i++) {
                            if (sc.infoData.FieldsAndPlants[i].Id == sc.selectedRow["FieldInfo"]["Id"]) {
                                modalValues = sc.infoData.FieldsAndPlants[i].Values;
                            }
                        }
                    } else modalValues = [];
                    break;
                case "FieldInfo":
                    modalValues = sc.infoData.FieldsAndPlants;
                    break;
                case "TypeTaskInfo":
                    modalValues = sc.infoData.TasksList;
                    break;
                case "TracktorInfo":
                    modalValues = sc.infoData.TraktorsList;
                    break;
                case "EquipInfo":
                    modalValues = sc.infoData.EquipmnetsList;
                    break;
                case "Emp1":
                    modalValues = sc.infoData.EmployeesList;
                    break;
                case "Emp2":
                    modalValues = sc.infoData.EmployeesList;
                    break;
                case "AvailableInfo":
                    modalValues = sc.infoData.ChiefsList;
                    break;
                default:
                    console.log('updateSelectedCell: Не пришел colField ' + sc.colField);
            }

            modals.showWindow(modalType.WIN_SELECT, {
                nameField: displayName,
                values: modalValues,
                selectedValue: selectedVal
            })
                .result.then(function (result) {
                    console.info(result);
                    sc.selectedRow[sc.selectedColumn] = result;
                    if (sc.colField == 'FieldInfo') sc.selectedRow['PlantInfo'] = '';
                    sc.selectedRowForSend = sc.selectedRow;
                }, function () {
                    console.info('Modal dismissed at: ' + new Date());
                });
        } else {
            console.log('не пришла sc.infoData');
        }
    };

    // Initialize
    requests.serverCall.post($http, "DriverWayBillsCtrl/GetInfoForDriverWayBillItem", {
        OrgId: curUser.orgid,
        ShiftTaskId: shiftTaskId,
		WaysListId: sc.formData.WayBillId,
    }, function (data, status, headers, config) {
        console.info(data);
        sc.infoData = data;
        if (sc.formData.WayBillId) {
            requests.serverCall.post($http, "DriverWayBillsCtrl/GetDriverWayBillItemById", sc.formData.WayBillId, function (data, status, headers, config) {
                console.info(data);
                sc.formData = data;
                $cookieStore.put('selectedDriverWayBill', {id: sc.formData.WayBillId});
            });
        } else {
            
            if (shiftTaskId != null) {
                console.log("shiftTaskId   " + shiftTaskId);
                requests.serverCall.post($http, "DriverWayBillsCtrl/GetDriverWayBillItemFromShiftTask", shiftTaskId, function (data, status, headers, config) {
                    console.info(data);
                    sc.formData = data;
					sc.AllResource =  data.AllResource ;
                    sc.formData.TypeFuel = 3;
                });
            }
        }
    });


    sc.setRepair = function () {
        if(sc.formData.IsRepair){
            if (!sc.formData.WayListNum || sc.formData.WayListNum.indexOf('Р-') == -1) {
                sc.formData.WayListNum = "Р/00/0";
            }
            sc.formData.FieldId = 87; //ГММ Орлово
            sc.formData.TaskTypeId = 123; //Текущий ремонт и ТО автотранспорта
		    sc.formData.TypeFuel = 3;     //дизельное топливо
        }
    };

    sc.traktorChange = function(item) {
        var typeFuelSel = item.Name_fuel;
        sc.formData.TypeFuel = typeFuelSel.Id;
    };
}]);