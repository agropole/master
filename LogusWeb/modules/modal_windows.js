/**
 * Created by alinashipilova on 07.05.15.
 */
(function () {
    var app = angular.module("modalWindows", ["LogusConfig"]).constant('modalType', {
        WIN_MESSAGE: 0,
        WIN_DIALOG: 1,
        WIN_ERROR: 2,
        WIN_SELECT: 3,
        WIN_COMMENT: 4,
        WIN_CREATE_FIELD: 5,
        WIN_SELECT_PAGINATION: 6,
        WIN_SELECT_MULTI: 7,
        WIN_DOUBLE_VALUES: 8,
        WIN_READ_ONLY: 9,
        WIN_MAP : 10,
        WIN_DOUBLE_LIST: 11,
		WIN_UPLOAD_FILE: 12,
        WIN_SELECT_SHIFT:13
    });
    app.filter('startFrom', function () {
        return function (input, start) {
            if (input) {
                start = +start;
                return input.slice(start);
            }
            return [];
        };
    });
    app.factory("modals", ["$http", "$modal", "$timeout", "modalType", 'filterFilter', "LogusConfig", "preloader", function ($http, $modal, $timeout, modalType, filterFilter, LogusConfig, preloader) {
        return {
            showWindow: function (type, data) {
                var tplUrl;
                var ctrl;
                var rslv;
                switch (type) {
                    case modalType.WIN_DIALOG:
                        tplUrl = 'templates/modals/dialog_modal.html';
                        ctrl = function ($scope, $modalInstance, title,id, question,question2, callback) {
                            var sc = $scope;

                            sc.title = title;
							sc.id = parseInt(id);
                            sc.question = question;
                            sc.question2 = question2;
                            sc.cancel = function (data) {	
                            $modalInstance.dismiss(data);
                            };
                            sc.ok = function (data) {
                            $modalInstance.dismiss(data);
                                callback();
                            }
                        };
                        rslv = {
                            title: function () {
                                return data.title;
                            },
							id: function () {
                                return parseInt(data.id);
                            },
                            question: function () {
                                return data.message;
                            },
							 question2: function () {
                                return data.message2;
                            },
                            callback: function () {
                                return data.callback;
                            }
                        };
                        break;

                    case modalType.WIN_MESSAGE:
                        tplUrl = 'templates/modals/message_modal.html';
                        ctrl = function ($scope, $modalInstance, title, message, callback) {
                            var sc = $scope;

                            sc.title = title;
                            sc.message = message;

                            sc.cancel = function () {
                                $modalInstance.dismiss('cancel');
                                callback();
                            };
                        };
                        rslv = {
                            title: function () {
                                return data.title;
                            },
                            message: function () {
                                return data.message;
                            },
                            callback: function () {
                                return data.callback;
                            }
                        };
                        break;

                    case modalType.WIN_ERROR:
                        tplUrl = 'templates/modals/error_modal.html';
                        ctrl = function ($scope, $modalInstance, title, message, values, callback) {
                            var sc = $scope;

                            sc.title = title;
                            sc.message = message;
                            sc.values = values;

                            sc.cancel = function () {
                                $modalInstance.dismiss('cancel');
                                callback();
                            };
                        };
                        rslv = {
                            title: function () {
                                return data.title;
                            },
                            message: function () {
                                return data.message;
                            },
                            values: function () {
                                return data.values;
                            },
                            callback: function () {
                                return data.callback;
                            }
                        };
                        break;
                    case modalType.WIN_SELECT:
                        tplUrl = 'templates/modals/select_modal.html';
                        ctrl = function ($scope, $modalInstance, nameField, values, selectedValue) {
                            var sc = $scope;


                            /*--------------------------------------------------up-down--------------------------------------------------------*/


                            var scrollHeight = 0;
                            var ADD_HEIGHT = 20;
                            var formHeight = $(".my-form").height();
                            var column = 1;

                            sc.keyDownIn = function (e, data) {

                                if (!$(".selectedValue").length) {
                                    $(".services li:first-child").addClass("selectedValue");
                                }
                                var listHeightHalf = $(".services").height() / 2;

                                if ($(".selectedValue") && $(".selectedValue").position() && $(".selectedValue").position().top > listHeightHalf) {
                                    column = 2;
                                }
                                /* console.info("keyDown");*/
                                if (e.keyCode == 13) { // enter
                                    var selected = $(".selectedValue");

                                    $timeout(function () {
                                        selected.trigger('click');
                                    }, 100);
                                }
                                if (e.keyCode == 38) { // up
                                    var selected = $(".selectedValue");
                                    $(".services li").removeClass("selectedValue");
                                    if (selected.prev().length == 0) {
                                        selected.siblings().last().addClass("selectedValue");
                                        $(".my-form").scrollTop($(".selectedValue").position().top);
                                        column = 2;
                                    } else {
                                        selected.prev().addClass("selectedValue");

                                        if (selected.prev().position().top < 100) {
                                            $(".my-form").scrollTop(-( $(".my-form").height() - selected.prev().position().top));
                                            scrollHeight = selected.prev().height();

                                        } else if ($(".selectedValue").position().top > $(".my-form").height() && column == 2) {
                                            $(".my-form").scrollTop($(".selectedValue").position().top);
                                            column = 1;

                                        }

                                    }
                                }
                                if (e.keyCode == 40) { // down

                                    var selected = $(".selectedValue");
                                    var elementPosition = $(".selectedValue").position().top;
                                    $(".services li").removeClass("selectedValue");
                                    if (selected.next().length != 0) var nextPositionLeft = selected.next().offset().left;

                                    if (selected.next().length == 0) {
                                        selected.siblings().first().addClass("selectedValue");
                                        $(".my-form").scrollTop(0);
                                        column = 1;
                                    } else {
                                        var elementNextPosition = selected.next().position().top;

                                        selected.next().addClass("selectedValue");
                                        if (elementPosition < 0 && column == 2) {
                                            elementPosition = (-1) * $(".selectedValue").position().top;
                                        }

                                        if (elementNextPosition > $(".my-form").height()) {
                                            $(".my-form").scrollTop(elementNextPosition - $(".my-form").height() + scrollHeight);
                                            scrollHeight = scrollHeight + selected.next().height() + 10;
                                            /* console.info($(".selectedValue").position().top);
                                             console.info($(".my-form").height());
                                             console.info($(".selectedValue").position().top - $(".my-form").height() + scrollHeight);
                                             console.info("----------------------------------------------------------");*/
                                        } else if (nextPositionLeft > 900) {
                                            $(".my-form").scrollTop(0);
                                            column = 2;
                                        }
                                    }
                                }
                            };


                            /*--------------------------------------------------end-up-down--------------------------------------------------------*/


                            sc.nameField = nameField;
                            sc.values = values;
                            sc.selectedValue = selectedValue;
                            setTimeout(function () {
                                angular.element('.search-modal').focus();
                                if ($(".selectedValue").length) {
                                    var selected = $(".selectedValue");
                                    $(".my-form").scrollTop($(".selectedValue").position().top - 200);
                                } else {
                                    $(".services li:first-child").addClass("selectedValue");
                                }
                            }, 100);


                            /*--------------------------------------------------*/


                            /*  $scope.search = {};
                             $scope.resetFilters = function () {
                             // needs to be a function or it won't trigger a $watch
                             $scope.search = {};
                             };
                             $scope.resetFilters = function () {
                             // needs to be a function or it won't trigger a $watch
                             $scope.search = {};
                             };
                             $scope.update = function () {
                             $scope.filtered = filterFilter(sc.values, {name: $scope.search.name});
                             $scope.totalItems = $scope.filtered.length;
                             }
                             $scope.update();
                             // pagination controls
                             $scope.currentPage = 1;
                             $scope.entryLimit = 16; // items per page
                             $scope.noOfPages = 5;
                             sc.max

                             $scope.updateSearch = function () {
                             $scope.filtered = filterFilter(sc.values, {name: sc.search.name});
                             };*/
                            /*--------------------------------------------------*/

                            sc.closeModal = function (data) {
                                $modalInstance.close(data);
                            };

                            sc.cancel = function () {
                                $modalInstance.dismiss('cancel');
                            };
                        };
                        rslv = {
                            nameField: function () {
                                return data.nameField;
                            },
                            values: function () {
                                return data.values;
                            },
                            selectedValue: function () {
                                return data.selectedValue;
                            }
                        };
                        break;

                    case modalType.WIN_SELECT_PAGINATION:
                        tplUrl = 'templates/modals/select_modal_pagination.html';
                        ctrl = function ($scope, $modalInstance, nameField, values, selectedValue) {
                            var sc = $scope;


                            /*--------------------------------------------------up-down--------------------------------------------------------*/


                            var scrollHeight = 0;
                            var ADD_HEIGHT = 20;
                            var formHeight = $(".my-form").height();
                            var column = 1;

                            sc.keyDownIn = function (e, data) {

                                if (!$(".selectedValue").length) {
                                    $(".services li:first-child").addClass("selectedValue");
                                }
                                var listHeightHalf = $(".services").height() / 2;

                                if ($(".selectedValue").position().top > listHeightHalf) {
                                    column = 2;
                                }
                                /* console.info("keyDown");*/
                                if (e.keyCode == 13) { // enter
                                    var selected = $(".selectedValue");

                                    $timeout(function () {
                                        selected.trigger('click');
                                    }, 100);
                                }
                                if (e.keyCode == 38) { // up
                                    var selected = $(".selectedValue");
                                    $(".services li").removeClass("selectedValue");
                                    if (selected.prev().length == 0) {
                                        selected.siblings().last().addClass("selectedValue");
                                        $(".my-form").scrollTop($(".selectedValue").position().top);
                                        column = 2;
                                    } else {
                                        selected.prev().addClass("selectedValue");

                                        if (selected.prev().position().top < 100) {
                                            $(".my-form").scrollTop(-( $(".my-form").height() - selected.prev().position().top));
                                            scrollHeight = selected.prev().height();

                                        } else if ($(".selectedValue").position().top > $(".my-form").height() && column == 2) {
                                            $(".my-form").scrollTop($(".selectedValue").position().top);
                                            column = 1;

                                        }

                                    }
                                }
                                if (e.keyCode == 40) { // down

                                    var selected = $(".selectedValue");
                                    var elementPosition = $(".selectedValue").position().top;
                                    $(".services li").removeClass("selectedValue");

                                    if (selected.next().length == 0) {
                                        selected.siblings().first().addClass("selectedValue");
                                        $(".my-form").scrollTop(0);
                                        column = 1;
                                    } else {
                                        var elementNextPosition = selected.next().position().top;

                                        selected.next().addClass("selectedValue");
                                        if (elementPosition < 0 && column == 2) {
                                            elementPosition = (-1) * $(".selectedValue").position().top;
                                        }

                                        if (elementNextPosition > $(".my-form").height()) {
                                            $(".my-form").scrollTop(elementNextPosition - $(".my-form").height() + scrollHeight);
                                            scrollHeight = scrollHeight + selected.next().height() + 10;
                                            /* console.info($(".selectedValue").position().top);
                                             console.info($(".my-form").height());
                                             console.info($(".selectedValue").position().top - $(".my-form").height() + scrollHeight);
                                             console.info("----------------------------------------------------------");*/
                                        } else if (elementNextPosition < 0 && column == 1) {
                                            $(".my-form").scrollTop(0);
                                            column = 2;
                                        }
                                    }
                                }
                            };


                            /*--------------------------------------------------end-up-down--------------------------------------------------------*/


                            sc.nameField = nameField;
                            sc.values = values;
                            sc.selectedValue = selectedValue;
                            setTimeout(function () {
                                angular.element('.search-modal').focus();
                                if ($(".selectedValue").length) {
                                    var selected = $(".selectedValue");
                                    $(".my-form").scrollTop($(".selectedValue").position().top - 200);
                                } else {
                                    $(".services li:first-child").addClass("selectedValue");
                                }
                            }, 100);


                            /*--------------------------------------------------*/


                            $scope.search = {};
                            $scope.resetFilters = function () {
                                // needs to be a function or it won't trigger a $watch
                                $scope.search = {};
                            };
                            $scope.resetFilters = function () {
                                // needs to be a function or it won't trigger a $watch
                                $scope.search = {};
                            };
                            $scope.update = function () {
                                $scope.filtered = filterFilter(sc.values, {name: $scope.search.name});
                                $scope.totalItems = $scope.filtered.length;
                            };
                            $scope.update();
                            // pagination controls
                            $scope.currentPage = 1;
                            $scope.entryLimit = 16; // items per page
                            $scope.noOfPages = 5;

                            $scope.updateSearch = function () {
                                $scope.filtered = filterFilter(sc.values, {name: sc.search.name});
                            };
                            /*--------------------------------------------------*/

                            sc.closeModal = function (data) {
                                $modalInstance.close(data);
                            };

                            sc.cancel = function () {
                                $modalInstance.dismiss('cancel');
                            };
                        };
                        rslv = {
                            nameField: function () {
                                return data.nameField;
                            },
                            values: function () {
                                return data.values;
                            },
                            selectedValue: function () {
                                return data.selectedValue;
                            }
                        };
                        break;

                    case modalType.WIN_SELECT_MULTI:
                        tplUrl = 'templates/modals/select_modal_multi.html';
                        ctrl = function ($scope, $modalInstance, nameField, values, selectedValue) {
                            var sc = $scope;
                            var selectionArray = [];
                            var scrollHeight = 0;
                            var ADD_HEIGHT = 20;
                            var formHeight = $(".my-form").height();
                            var column = 1;

                            for (var i= 0,li = selectedValue.length; i<li; i++) {
                                for (var j= 0, lj = values.length; j<lj; j++) {
                                    if (selectedValue[i].Id === values[j].Id) {
                                        values[j].selectedFlag = true;
                                        selectionArray.push(values[j]);
                                    }
                                }
                            }

                            sc.nameField = nameField;
                            sc.values = values;
                            setTimeout(function () {
                                angular.element('.search-modal').focus();
                            }, 100);

                            sc.clickRow = function (data) {
                                if (selectionArray.length > 0) {
                                    var del = false;
                                    for (var i = 0; i < selectionArray.length; i++) {
                                        if (selectionArray[i].Id == data.Id) {
                                            data.selectedFlag = false;
                                            selectionArray.splice(i, 1);
                                            i--;
                                            del = true;
                                        }
                                    }
                                    if (!del) {
                                        data.selectedFlag = true;
                                        selectionArray.push(data);
                                    }
                                } else {
                                    data.selectedFlag = true;
                                    selectionArray.push(data);
                                }
                            };

                            sc.closeModal = function () {
                                $modalInstance.close(selectionArray);
                            };

                            sc.clear = function() {
                                for (var i = 0; i < selectionArray.length; i++) {
                                    selectionArray[i].selectedFlag = false;
                                }
                                selectionArray = [];
                            };

                            sc.cancel = function () {
                                $modalInstance.dismiss('cancel');
                            };
                        };
                        rslv = {
                            nameField: function () {
                                return data.nameField;
                            },
                            values: function () {
                                return data.values;
                            },
                            selectedValue: function () {
                                return data.selectedValue;
                            }
                        };
                        break;

                    case modalType.WIN_COMMENT:
                        tplUrl = 'templates/modals/comment_modal.html';
                        ctrl = function ($scope, $modalInstance, title, comment) {
                            var sc = $scope;
                            sc.title = title;
                            sc.comment = comment;
                            setTimeout(function () {
                                angular.element('.textarea-comment-modal').focus()
                            }, 100);

                            sc.clear = function () {
                                sc.comment = "";
                                sc.closeModal(null);
                            };

                            sc.closeModal = function (data) {
                                $modalInstance.close(data);
                            };

                            sc.cancel = function () {
                                $modalInstance.dismiss('cancel');
                            };
                        };
                        rslv = {
                            title: function () {
                                return data.title;
                            },
                            comment: function () {
                                return data.comment;
                            }
                        };
                        break;
                    case modalType.WIN_CREATE_FIELD:
                        tplUrl = 'templates/modals/create_field_modal.html';
                        ctrl = function ($scope, $modalInstance, area, coordinates, center, callback) {
                            var sc = $scope;

                            sc.formData = {
                                Name: null,
                                OrgId: null,
                                PlantId: null,
                                Area: area,
                                Coordinates: coordinates,
                                Center: center
                            };

                            if (typeof showPreloader == 'undefined' || showPreloader)
                                preloader.show();

                            $http({
                                method: 'GET',
                                url: LogusConfig.serverUrl + 'REST/GetOgranizationList',
                            }).
                                success(function (data, status, headers, config) {
                                    preloader.hide();
                                    sc.infoOrgs = data.Value;
                                }).
                                error(function (data, status, headers, config) {
                                    preloader.hide();
                                    console.warn(data);
                                    console.warn(status);
                                    if (typeof errorCallback != 'undefined' && errorCallback != null) {
                                        //errorCallback();
                                        console.log('error');
                                    }
                                });

                            $http({
                                method: 'POST',
                                url: LogusConfig.serverUrl + "DayTasksCtrl/GetInfoForCreateDayTasks",
                                data: {RequestObject: {"OrgId": "1"}}
                            }).
                                success(function (data, status, headers, config) {
                                    preloader.hide();
                                    sc.infoPlants = data.Value.FieldsAndPlants;
                                }).
                                error(function (data, status, headers, config) {
                                    preloader.hide();
                                    console.warn(data);
                                    console.warn(status);
                                    if (typeof errorCallback != 'undefined' && errorCallback != null) {
                                        //errorCallback();
                                        console.log('error');
                                    }
                                });

                            sc.closeModal = function (data) {
                                console.info(JSON.stringify(data));
                                if (data.Name && data.OrgId && data.PlantId && data.Area && data.Date) {
                                    callback(data);
                                    $modalInstance.close();
                                }
                            };

                            sc.cancel = function () {
                                callback();
                                $modalInstance.close();
                            }
                        };
                        rslv = {
                            area: function () {
                                return data.area;
                            },
                            callback: function () {
                                return data.callback;
                            },
                            center: function () {
                                return data.center
                            },
                            coordinates: function () {
                                return data.coordinates
                            }
                        };
                        break;

                    case modalType.WIN_READ_ONLY:
                        tplUrl = 'templates/modals/read_only_modal.html';
                        ctrl = function ($scope, $modalInstance, nameField, values, selectedValue) {
                            var sc = $scope;

                            sc.nameField = nameField;
                            sc.values = values;
                            sc.selectedValue = selectedValue;

                            sc.closeModal = function (data) {
                                $modalInstance.close();
                            };

                            sc.cancel = function () {
                                $modalInstance.dismiss('cancel');
                            };
                        };
                        rslv = {
                            nameField: function () {
                                return data.nameField;
                            },
                            values: function () {
                                return data.values;
                            },
                            selectedValue: function () {
                                return data.selectedValue;
                            }
                        };
                        break;

                    case modalType.WIN_DOUBLE_VALUES:
                        tplUrl = 'templates/modals/double_values_modal.html';
                        ctrl = function ($scope, $modalInstance, title, firstvalue, secondvalue) {
                            //document.getElementById("planField").find('input')[0].focus();
                            $('#planField').find('input').focus();
                            var sc = $scope;
                            sc.title = title;
                            sc.firstvalue = firstvalue;
                            sc.secondvalue = secondvalue;

                            sc.closeModal = function (data) {
                                console.info(data);
                                console.info( sc.firstvalue);
                                $modalInstance.close(data);
                            };

                            sc.cancel = function () {
                                $modalInstance.dismiss('cancel');
                            };
                        };
                        rslv = {
                            title: function () {
                                return data.title;
                            },
                            firstvalue: function () {
                                return data.firstvalue;
                            },
                            secondvalue: function () {
                                return data.secondvalue;
                            }
                        };
                        break;
						case modalType.WIN_UPLOAD_FILE:
                        tplUrl = 'templates/modals/upload_file_modal.html';
                        ctrl = function ($scope, $modalInstance, title) {
                            $('#planField').find('input').focus();
                            var sc = $scope;
                            sc.title = title;
                            sc.closeModal = function (data) {
                                console.info(data);
                                console.info( sc.firstvalue);
                                $modalInstance.close(data);
                            };
                            sc.cancel = function () {
                                $modalInstance.dismiss('cancel');
                            };
                        };
                        rslv = {
                            title: function () {
                                return data.title;
                            }
                        };
                        break;
                    case modalType.WIN_MAP:
                        tplUrl = 'templates/modals/map_modal.html';
                        ctrl = function ($scope, $modalInstance, map) {
                            var sc = $scope;
                            sc.coord = data.coord;

                            sc.okModalMap = function (data) {
                                $modalInstance.close(map.getCoord());
                            };

                            sc.cancel = function () {
                                $modalInstance.dismiss('cancel');
                            };

                            sc.clear = function() {
                                map.clearMap();
                            };

                            var initMapEdit = function() {
                                map.clearAll();
                                map.initMap('map3');
                                map.clearMap();

                                sc.clearMeasures = function () {
                                    map.clearOverlay(function (area) {
                                        sc.formData.Area = area;
                                    });
                                };


                                map.setInsertFieldCb(function (newFieldId) {
                                    loadInfo(function() {
                                        sc.formData.FieldId = newFieldId;
                                    });
                                });

                                map.addInteraction();
                                map.allowSingleDraw();
                                if (sc.coord) {
                                    var coordArray = sc.coord.coord.replace('POLYGON ((', '').replace('))', '');
                                    coordArray = coordArray.split(", ");
                                    for (var i= 0, li = coordArray.length; i<li; i++) {
                                        coordArray[i] = [parseFloat(coordArray[i].split(" ")[0]), parseFloat(coordArray[i].split(" ")[1])];
                                    }
                                    map.addOnOverlay(coordArray);
                                }
                            };
                            setTimeout(initMapEdit, 100);
                        };
                        rslv = {
                            coord: data.coord,
                            coord2: data.coord2,
                            area: data.area,
                            fieldid: data.fieldid
                        };
                        break;
						case modalType.WIN_DOUBLE_LIST:
                        tplUrl = 'templates/modals/double_list_modal.html';
                        ctrl = function ($scope, $modalInstance) {
                            $('#modal_header').parent().parent().width(1000);
                            var sc = $scope;

                            sc.lists = data.lists;
                            sc.title1 = data.title1;
                            sc.title2 = data.title2;

                            sc.closeModal = function (data) {
                                $modalInstance.close({
                                    TotalRate: sc.lists.totalRate,
                                    ListSelected: sc.lists.ListSelected
                                });
                            };

                            sc.cancel = function () {
                                $modalInstance.dismiss('cancel');
                            };
                        }
                        break;
                        case modalType.WIN_SELECT_SHIFT:
                        tplUrl = 'templates/modals/select_modal_shift.html';
                        ctrl = function ($scope, $modalInstance, nameField, values, selectedValue) {
                            var sc = $scope;


                            /*--------------------------------------------------up-down--------------------------------------------------------*/


                            var scrollHeight = 0;
                            var ADD_HEIGHT = 20;
                            var formHeight = $(".my-form").height();
                            var column = 1;

                            sc.keyDownIn = function (e, data) {

                                if (!$(".selectedValue").length) {
                                    $(".services li:first-child").addClass("selectedValue");
                                }
                                var listHeightHalf = $(".services").height() / 2;

                                if ($(".selectedValue") && $(".selectedValue").position() && $(".selectedValue").position().top > listHeightHalf) {
                                    column = 2;
                                }
                                /* console.info("keyDown");*/
                                if (e.keyCode == 13) { // enter
                                    var selected = $(".selectedValue");

                                    $timeout(function () {
                                        selected.trigger('click');
                                    }, 100);
                                }
                                if (e.keyCode == 38) { // up
                                    var selected = $(".selectedValue");
                                    $(".services li").removeClass("selectedValue");
                                    if (selected.prev().length == 0) {
                                        selected.siblings().last().addClass("selectedValue");
                                        $(".my-form").scrollTop($(".selectedValue").position().top);
                                        column = 2;
                                    } else {
                                        selected.prev().addClass("selectedValue");

                                        if (selected.prev().position().top < 100) {
                                            $(".my-form").scrollTop(-( $(".my-form").height() - selected.prev().position().top));
                                            scrollHeight = selected.prev().height();

                                        } else if ($(".selectedValue").position().top > $(".my-form").height() && column == 2) {
                                            $(".my-form").scrollTop($(".selectedValue").position().top);
                                            column = 1;

                                        }

                                    }
                                }
                                if (e.keyCode == 40) { // down

                                    var selected = $(".selectedValue");
                                    var elementPosition = $(".selectedValue").position().top;
                                    $(".services li").removeClass("selectedValue");
                                    if (selected.next().length != 0) var nextPositionLeft = selected.next().offset().left;

                                    if (selected.next().length == 0) {
                                        selected.siblings().first().addClass("selectedValue");
                                        $(".my-form").scrollTop(0);
                                        column = 1;
                                    } else {
                                        var elementNextPosition = selected.next().position().top;

                                        selected.next().addClass("selectedValue");
                                        if (elementPosition < 0 && column == 2) {
                                            elementPosition = (-1) * $(".selectedValue").position().top;
                                        }

                                        if (elementNextPosition > $(".my-form").height()) {
                                            $(".my-form").scrollTop(elementNextPosition - $(".my-form").height() + scrollHeight);
                                            scrollHeight = scrollHeight + selected.next().height() + 10;
                                            /* console.info($(".selectedValue").position().top);
                                             console.info($(".my-form").height());
                                             console.info($(".selectedValue").position().top - $(".my-form").height() + scrollHeight);
                                             console.info("----------------------------------------------------------");*/
                                        } else if (nextPositionLeft > 900) {
                                            $(".my-form").scrollTop(0);
                                            column = 2;
                                        }
                                    }
                                }
                            };


                            /*--------------------------------------------------end-up-down--------------------------------------------------------*/


                            sc.nameField = nameField;
                            sc.values = values;
                            sc.selectedValue = selectedValue;
                            setTimeout(function () {
                                angular.element('.search-modal').focus();
                                if ($(".selectedValue").length) {
                                    var selected = $(".selectedValue");
                                    $(".my-form").scrollTop($(".selectedValue").position().top - 200);
                                } else {
                                    $(".services li:first-child").addClass("selectedValue");
                                }
                            }, 100);


                            /*--------------------------------------------------*/
                            sc.getStyle = function(status) {
                            if(status == true) { return {'color':'red'};}
							if(status == false) { return {'color':'#2bb72b'};}
                            }

                            /*  $scope.search = {};
                             $scope.resetFilters = function () {
                             // needs to be a function or it won't trigger a $watch
                             $scope.search = {};
                             };
                             $scope.resetFilters = function () {
                             // needs to be a function or it won't trigger a $watch
                             $scope.search = {};
                             };
                             $scope.update = function () {
                             $scope.filtered = filterFilter(sc.values, {name: $scope.search.name});
                             $scope.totalItems = $scope.filtered.length;
                             }
                             $scope.update();
                             // pagination controls
                             $scope.currentPage = 1;
                             $scope.entryLimit = 16; // items per page
                             $scope.noOfPages = 5;
                             sc.max

                             $scope.updateSearch = function () {
                             $scope.filtered = filterFilter(sc.values, {name: sc.search.name});
                             };*/
                            /*--------------------------------------------------*/

                            sc.closeModal = function (data) {
                                $modalInstance.close(data);
                            };

                            sc.cancel = function () {
                                $modalInstance.dismiss('cancel');
                            };
                        };
                        rslv = {
                            nameField: function () {
                                return data.nameField;
                            },
                            values: function () {
                                return data.values;
                            },
                            selectedValue: function () {
                                return data.selectedValue;
                            }
                        };
                        break; 
                    case modalType.WIN_DOUBLE_LIST:
                        tplUrl = 'templates/modals/double_list_modal.html';
                        ctrl = function ($scope, $modalInstance) {
                            $('#modal_header').parent().parent().width(1000);
                            var sc = $scope;

                            sc.lists = data.lists;
                            sc.title1 = data.title1;
                            sc.title2 = data.title2;

                            sc.closeModal = function (data) {
                                $modalInstance.close({
                                    TotalRate: sc.lists.totalRate,
                                    ListSelected: sc.lists.ListSelected
                                });
                            };

                            sc.cancel = function () {
                                $modalInstance.dismiss('cancel');
                            };
                        }
                        break;        
                    default:
                        return null;
                }

                return $modal.open({
                    templateUrl: tplUrl,
                    controller: ctrl,
                    resolve: rslv
                });
            }
        }
    }]);
})();