
var app = angular.module("TCTempList", ["ngGrid", "services", 'ngCookies', "modalWindows"]);
app.controller("TCTempListCtrl", ["$scope", "$http", "requests", "$location", "modals", "modalType","LogusConfig",'$rootScope',  function ($scope, $http, requests, $location, modals, modalType, LogusConfig,$rootScope) {
    var sc = $scope;
    sc.infoData = {};
	sc.infoData.YearList  = [];
	sc.formData = {};
    if ($rootScope.roleid==12)
	{
	var cellTemplate1='<div  class="ngCellText {{getClassCopyCell(row.entity.Status,row)}}"></div>'; 
	} else {
		var cellTemplate1='<div   class="ngCellText {{getClassCopyCell(row.entity.Status,row)}}" style="font-size: 36px!important;" ng-switch on="row.entity[col.field]"'+
            'ng-click="delRow(row.entity)">'+
            '<div  ng-switch-default>'+
            '<a tooltip="{{row.entity[col.field]}}" tooltip-class="my-custom-tooltip"'+
           'tooltip-placement="top" tooltip-append-to-body="true">'+
            '<div class="clear_row"/>'+
            '</a></div></div></div>'; 
		}

	   sc.formData.YearId = new Date().getFullYear();
		sc.clearTCForm = function() {
        sc.formData.PlantId = null;
        sc.formData.SortId = null;
		sc.formData.FieldId = null;
		sc.SortId = null;
        };
		
		sc.clear = function() {
		  sc.formData.SortId = null;
		  sc.formData.FieldId = null;
		  sc.SortId = null;
		  sc.refreshPlant();
			}
			
		//-------ГОД
	    var refreshYear = function(first) {
        sc.clearTCForm();
        requests.serverCall.post($http, "TechCard/GetPlantSortFieldByYear", sc.formData.YearId, function (data, status, headers, config) {
            console.info(data);
            sc.Plants = data;
            sc.refreshPlant();
			if (sc.plantId) {
		 	sc.formData.PlantId = sc.plantId;   
			}
			if (sc.SortId2) {
				sc.formData.SortId = sc.SortId2;
			}
            if (first) {
                sc.refreshYear = refreshYear;
            }
        }, function () {
        });
         };
	    refreshYear(true);
		
	
    sc.selectedRow = [];
    sc.gridOptions = {
        enableColumnResize: false,
        enableRowSelection: true,
        showFooter: false,
        rowHeight: 49,
        headerRowHeight: 49,
        useExternalSorting: true,
        data: 'TCData',
        selectedItems: sc.selectedRow,
        afterSelectionChange: function (row) {
          if (sc.selectedRow.length != 0)  {console.info(sc.selectedRow[0].Id);}
            if (row.entity) {
                if (row.selected) {
                    sc.selectedRow = [row];
                } else {
                    if (sc.selectedRow && sc.selectedRow > 0) {
                        sc.selectedRow.splice(sc.tableDataFirst.indexOf(row.entity), 1);
                    }
                }
            }	
        },
        beforeSelectionChange: function (row, event) {
            angular.forEach(sc.TCData, function (data, index) {
                if (data.selected == true) sc.selectedRow.splice(index, 1);
                sc.gridOptions.selectRow(index, false);
            });
			
            return true;
        }
    };
    sc.gridOptions.columnDefs = [
	{
            field: "Number",
            displayName: "№",
            width: "20%",
            cellTemplate: '<div class="ngCellText {{getClassCopyCell(row.entity.Status,row)}}" ng-class="col.colIndex()">' +
            '<input value="{{row.entity[col.field]}}" ng-model="row.entity[col.field]" placeholder="Культура" class="bordernone" readonly>' +
            '</div>'
        },
        {
            field: "Plant",
            displayName: "Культура",
            width: "35%",
            cellTemplate: '<div class="ngCellText {{getClassCopyCell(row.entity.Status,row)}}" ng-class="col.colIndex()">' +
            '<input value="{{row.entity[col.field]}}" ng-model="row.entity[col.field]" placeholder="Культура" class="bordernone" readonly>' +
            '</div>'
        },
        {
            field: "Sort", displayName: "Сорт", width: "30%",
            cellTemplate: '<div class="ngCellText {{getClassCopyCell(row.entity.Status,row)}}" ng-class="col.colIndex()">' +
            '<input value="{{row.entity[col.field]}}" ng-model="row.entity[col.field]" placeholder="Сорт" class="bordernone" readonly>' +
            '</div>'
        },
		 {
            field: "", displayName: " ", width:"10%",
            cellTemplate: '<div class="{{getClassTempCell(row.entity.TemplateStatus,row)}} ngCellText {{getClassCopyCell(row.entity.Status,row)}}">'+
			'<button type="button"  class="logus-btn btn btn-border ng-click-active" style="width: 100%;margin-top: 8px" ng-click="createTechCard(row.entity.Id,row.entity.PlantId, row.entity.Plant, row.entity.SortId)">Тех.карта</button></div>',
        },
        {
            field: "Delete", displayName: " ", width: "5%",
            cellTemplate: cellTemplate1, cellClass: 'cellToolTip'
        },
        {field: "Id", width: "0%"},
		{field: "PlantId", width: "0%"},
		{field: "SortId", width: "0%"}
    ];

        requests.serverCall.get($http, "TechCardTemp/GetTCTemp", function (data, status, headers, config) {
			console.log(data);
            sc.TCData = data;	
        }, function () {
        });
		
		sc.createTechCard = function (techId, plantId, plantName, sortId) {  
        sc.plantName = plantName;		
		//$("#infoparent").addClass("disabledbutton");
	     $("#infoparent").hide();
	     var checkCult = false; sc.SortId2 = null;
		// console.log(sc.Plants);
		 //GetPlantByTwoYears
		    requests.serverCall.get($http, "TechCardTemp/GetPlantByTwoYears", function (data1, status, headers, config) {
		     console.log(data1);
			  for (var i = 0, li = data1.length; i < li; i++) {
				  if (data1[i] == plantId) {
					 checkCult = true; 
				  }
			  }
			 
			 
		 for (var i = 0, li = sc.Plants.length; i < li; i++) {
			 console.log(sc.Plants[i].Id);
			if (sc.Plants[i].Id == plantId) {
			//	checkCult = true;
					 for (var j = 0, li = sc.Plants[i].Values.length; j < li; j++) {
						 if (sc.Plants[i].Values[j].Id == sortId) {
						     sc.SortId2 = sortId;
						 }
					 }
			}
		 }
		 if (checkCult) {			
		    requests.serverCall.post($http, "TechCardTemp/GetYearsCult", plantId, function (data, status, headers, config) {
            console.info(data);
		    sc.plantId = plantId; 
            sc.infoData.YearList = data;
		    sc.formData.YearId = new Date().getFullYear() ;
         //создать тех. карту по шаблону	   
	        $("#myModal").modal("show");
	         }, function () {
        });

		
		 }   else {
			   //  console.info('data');
			 		modals.showWindow(modalType.WIN_MESSAGE, {
                              title: "Предупреждение",
                              message: "Отсутствует культура в севообороте.",
                              callback: function () {
                              } 
                          });
			 
		 }
	
	     }, function () {
        });
	
	
		 
        }
		//создать
	sc.yes = function () {
			requests.serverCall.post($http, "TechCardTemp/CheckSortPlantYear", {
            FieldId: sc.formData.FieldId,
            YearId: sc.formData.YearId,
            SortId : sc.SortId != null ? sc.SortId : sc.formData.SortId,
            PlantId : sc.formData.PlantId,
			TempId : sc.selectedRow[0].entity.Id,
        }, function (data, status, headers, config) {
			if (data != "empty_message") {
            if (data.charAt(data.length-1) == '?') {
			var arr = data.split('/');	
			modals.showWindow(modalType.WIN_DIALOG, {
            title: "Сообщение", message: arr[0],
            message2: arr[1],
			callback: function () {
               //да
	        requests.serverCall.post($http, "TechCardTemp/CheckSortPlantYear", {
            FieldId: sc.formData.FieldId,
            YearId: sc.formData.YearId,
            SortId : sc.SortId != null ? sc.SortId : sc.formData.SortId,
            PlantId :sc.formData.PlantId,
			TempId : sc.selectedRow[0].entity.Id,
			CheckTemp : 1
        }, function (data, status, headers, config) {
			modals.showWindow(modalType.WIN_MESSAGE, {
                title: "Сообщение",
                message: data,
                callback: function () {
                }
            });
			
			
			  });
			   //
            }
        });	
				
				
				
			}
			else {
			
			
           modals.showWindow(modalType.WIN_MESSAGE, {
                title: "Сообщение",
                message: data,
                callback: function () {
                }
            });
			}
			//
			}
			
        });
		
		 $("#myModal").modal("hide");
		 $("div.modal-backdrop").hide();
	}
       sc.no = function () {
	      $("#myModal").modal("hide");
	      $("div.modal-backdrop").hide();
	 
	      }
      sc.cancel = function () {
		$("#myModal").modal("hide");    
	  }
		
	  var flagPlantChange;	
	  sc.refreshPlant = function() {
        console.log('REFRESH PLANT ' + flagPlantChange);
        if (flagPlantChange !== 'field') {
            sc.Fields = []; 
			sc.FieldsInfo = [];
            sc.formData.Area = 0;
            
            if (sc.formData.PlantId) {
                for (var i = 0, li = sc.Plants.length; i < li; i++) {
                    var p = sc.Plants[i];
                    if (p.Id === sc.formData.PlantId) {
                        sc.infoData.FieldList = p.Fields;
                        if (sc.formData.SortId) {
                            sc.formData.FieldId = null;
                            for (var j = 0, lj = p.Values.length; j < lj; j++) {
                                var s = p.Values[j];
                                if (s.Id === sc.formData.SortId) {
                                    sc.Fields = s.Fields;
									sc.FieldsInfo =  s.Fields;
                                }
                            }
                        }
                        if (sc.Fields.length === 0) {
                            sc.Fields = p.Fields;
							sc.FieldsInfo =  p.Fields;
                        }
						
                    }
                }		
                console.log(sc.Fields);				  
            }
        } else {
            flagPlantChange = null;
        }
		//	if (sc.SortId2) {
		//		sc.formData.SortId = sc.SortId2;
		//	}
    };
	    sc.$watch("formData.SortId", function(SortId) {
        sc.refreshPlant();
      });
	    sc.refreshField = function() {
        console.log('REFRESH FIELD ' + flagPlantChange);
            flagPlantChange = 'field';
            if (sc.formData.FieldId) {
				sc.SortId = sc.formData.SortId ;			
                sc.formData.SortId = null;
                for (var i=0, li = sc.infoData.FieldList.length; i<li; i++) {
                    if (sc.infoData.FieldList[i].Id === sc.formData.FieldId) {
                     //   sc.formData.Area = sc.infoData.FieldList[i].Area;
                   //     sc.Fields = [sc.infoData.FieldList[i]];
				   sc.FieldsInfo = [sc.infoData.FieldList[i]];
                    }
                }
            } else {
                flagPlantChange = null;
            }
         };
	
	

    sc.createTC = function() {
       $location.path('/techcard_temp/');

    };

    sc.delRow = function(row) {
        modals.showWindow(modalType.WIN_DIALOG, {
            title: "Удаление", message: "Вы уверены, что хотите удалить шаблон тех.карты?", callback: function () {
                sc.TCData.splice(sc.TCData.indexOf(row), 1);
                requests.serverCall.post($http, "TechCardTemp/DelTC_temp",row.Id, function (data, status, headers, config) {
                 //   loadTC();
                }, function () {
                });
            }
        });
       
    };

    sc.editTC = function() {
        if (sc.selectedRow[0].entity ) {
            console.info(sc.selectedRow[0].entity);
            $location.path('/techcard_temp/' + sc.selectedRow[0].entity.Id);
        } else {
           
        }
    };
}]);