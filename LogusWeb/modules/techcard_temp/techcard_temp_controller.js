var app = angular.module("TechcardTemp", ["ngGrid", "services", 'ngCookies', "modalWindows"]);
app.controller("TechcardTempCtrl", ["$scope", "$http", "requests", "$location", '$cookieStore', "modals", "modalType", "preloader","$stateParams", function ($scope, $http, requests, $location, $cookieStore, modals, modalType, preloader, $stateParams) {
   
	var sc = $scope;
    sc.formData = {};
    if ($stateParams.id && $stateParams.id != 0) {
        sc.formData.Id = $stateParams.id;
    }
     var met=0;
    var clearTCForm = function() {
        sc.formData.PlantId = null;
        sc.formData.SortId = null;
        sc.DataWorks = [];
        sc.DataSupportStuff = [];
        sc.DataSzr = [];
        sc.DataChemicalFertilizers = [];
        sc.DataSeed = [];
		sc.DataDopMaterial = [];
        sc.DataServices = [];
	
    };

    clearTCForm();

	sc.clear = function() {
	  sc.formData.SortId = null;
	}
	
	sc.checkFillRow = function(arr1,arr2) {
			//====выделение
					   for (var i = 0; i < arr1.length; i++) {
						if (arr2 != null && arr2.length != 0 ) {
						   for (var j = 0; j < arr2.length; j++) {	
						     arr1[i].selectedFlag = false;
						   if (arr2[j].Id == arr1[i].Id ) { 
					  	       arr1[i].selectedFlag = true;
							   break;
							}  }
							} else {
							      arr1[i].selectedFlag = false;
							}
							   }
							//====	
		
		
	}
	
	
    var setCommonGridOption = function(newGridOpt) {
        var commonGridOptions = {
            enableColumnResize: false,
            enableRowSelection: false,
            showFooter: false,
            rowHeight: 49,
            headerRowHeight: 49,
            useExternalSorting: true
        };
        for(var p in commonGridOptions) {
            newGridOpt[p] = commonGridOptions[p];
        };
    };

    requests.serverCall.get($http, "TechCard/GetInfoTCDetail", function (data, status, headers, config) {
        console.info(data);
        sc.infoData = data;
    }, function () {
    });
     
        clearTCForm();
        requests.serverCall.get($http, "TechCardTemp/GetPlantSort", function (data, status, headers, config) {
            console.info(data);
            sc.Plants = data;
                if (sc.formData.Id) loadTcById();
        }, function () {
        });
   

	
    sc.changeMainData = function() {
        recountData(sc.WorkData, sc.changeWorkData);
    };

   
    
    //----------------------------------------------------------end-НЗП---------------------------------------------------------------------------------
    //----------------------------------------------------------Работы----------------------------------------------------------------------------------
    sc.gridOptions_Works = {};
    setCommonGridOption(sc.gridOptions_Works);
    sc.gridOptions_Works.data = 'DataWorks';
    sc.gridOptions_Works.columnDefs = [
        {
            field: "index", displayName: " ", width: "5%",
            cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()">' +
            '<textinputnotcomma data="getIndex(row.entity)" ng-model="row.entity[col.field]" class="bordernone" readonly="1"/>' +
            '</div>'
        },
		 {
            field: "Id1", displayName: " ", width: "0%",
		    cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()">' +
            '<input value="{{row.entity[col.field]}}" class="bordernone" ng-model="row.entity[col.field]" placeholder="" readonly>' +
           '</div>'
        },
        {
            field: "Name", displayName: "Наименование", width: "25%",
            cellTemplate: '<a tooltip="{{row.entity[col.field][\'Name\']}}" tooltip-popup-delay="1000" tooltip-class="my-custom-tooltip" tooltip-placement="top" tooltip-append-to-body="true">' +
            '<div class="ngCellText" ng-class="col.colIndex()" ng-click="editCell(row.entity, row.getProperty(col.field), col.field)">' +
            '<input value="{{row.entity[col.field][\'Name\']}}" placeholder="Наимнование работы" style="border: none;"  data-ng-click="updateSelectedCell(col.field, row.entity[col.field][\'Id\'],\'№ поля\',row.entity,$event)"  readonly>' +
            '</div>'
        },
        {
            field: "UnicFlag", displayName: "Тип", width: "15%",
            cellTemplate: '<a>' +
            '<div class="ngCellText" ng-class="col.colIndex()" ng-click="editCell(row.entity, row.getProperty(col.field), col.field)">' +
            '<input ng-value="getFlagName(row.entity[col.field])" placeholder="Тип" style="border: none;"  data-ng-click="updateType(col.field, row.entity[col.field],\'Тип работ\',row.entity,$event)"  readonly>' +
            '</div></a>'
        },
        {
            field: "Count", displayName: "Кратность", width: "10%",
            cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()">' +
            '<textinputnotcomma value="{{row.entity[col.field]}}" data="row.entity[col.field]" regex-name="numbersWithPoint" field-change="changeWorkData(row.entity)" readonly="row.entity.UnicFlag"></textinputnotcomma>' +
            '</div>'
        },
        {
            field: "Factor", displayName: "Коэф.", width: "10%",
            cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()">' +
            '<textinputnotcomma value="{{row.entity[col.field]}}" data="row.entity[col.field]" regex-name="numbersWithPoint" field-change="changeWorkData(row.entity)" readonly="row.entity.UnicFlag"></textinputnotcomma>' +
            '</div>'
        },
        {
            field: "Workload", displayName: "Объем работ", width: "10%",
            cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()">' +
            '<textinputnotcomma value="{{row.entity[col.field]}}" data="row.entity[col.field]" regex-name="numbersWithPoint" field-change="changeWorkload(row.entity)" readonly="!row.entity.UnicFlag"></textinputnotcomma>' +
            '</div>'
        },
        {
            field: "DateStart",
            displayName: "Начало",
            width: "10%",
            cellTemplate:'<a tooltip="{{row.getProperty(col.field)}}" tooltip-popup-delay="1000" tooltip-class="my-custom-tooltip" tooltip-placement="top" tooltip-append-to-body="true">' +
            '<datepicktable class="logus-headers col-md-2 col-xs-4 col-sm-2 col-lg-2" style="padding-top: 6px;margin-left: -10px;font-size: 15px;" data="row.entity[col.field]" type="1" mask="\'99.99.9999\'" regex-name="\'date\'" date-change="dateChange(row.entity, curDate)">' +
            '</datepicktable>' +
            '</a>'
        },
        {
            field: "DateEnd",
            displayName: "Конец",
            width: "10%",
            cellTemplate:'<a tooltip="{{row.getProperty(col.field)}}" tooltip-popup-delay="1000" tooltip-class="my-custom-tooltip" tooltip-placement="top" tooltip-append-to-body="true">' +
            '<datepicktable class="logus-headers col-md-2 col-xs-4 col-sm-2 col-lg-2" style="padding-top: 6px;margin-left: -19px;font-size: 15px;" data="row.entity[col.field]" type="1" mask="\'99.99.9999\'" regex-name="\'date\'" date-change="dateChange(row.entity, curDate)">' +
            '</datepicktable>' +
            '</a>'
        },
        {
            field: "Delete", displayName: " ", width: "5%",
            cellTemplate: '<div class="ngCellText clear_row"  style="font-size: 36px!important;margin: auto;margin-top: 7px;" ng-switch on="row.entity[col.field]" ng-click="delRowWork(row.entity)"></div>'   
        },
        {field: "Id", width: "0%"}
    ];

    sc.getIndex = function(row) {
        return sc.DataWorks.indexOf(row) + 1;
    };

    sc.createTask = function() {
        var work = {
            Id: null,//sfsd
            Name: null,//sfsd
            Count: 0,//sfsd
            Factor: 0,
            Workload: 0,//sfsd
            DateStart: new Date(),//sfsd
            DateEnd: new Date(),//sfsd
            Equipment: null,
            Tech: null,
            Comment: '',//sfsd
            ShiftCount: 0,//sfsd
            UnicFlag: 0, //sfsd
        };
        sc.DataWorks.push(work);
        var grid = sc.gridOptions_Works.ngGrid;
        setTimeout(function() {
            grid.$viewport.scrollTop(sc.DataWorks.length * grid.config.rowHeight + 50);
        }, 100);
    };

    var typesDrop = [
        {Id: 0, Name:'По площади'},
        {Id: 1, Name:'Штучные(по сменам)'},
        {Id: 2, Name:'Штучные(по объему)'}
    ];

    sc.getFlagName = function(id) {
        for (var i=0,li = typesDrop.length; i<li; i++) {
            if (typesDrop[i].Id == id) {
                return typesDrop[i].Name;
            }
        }
    };

    sc.updateType = function (colField, selectedVal, displayName, row) {
        sc.colField = colField;
        var typeModal = modalType.WIN_SELECT;
        var modalValues = typesDrop;

        modals.showWindow(typeModal, {
            nameField: displayName,
            values: modalValues,
            selectedValue: selectedVal
        })
            .result.then(function (result) {
                console.info(sc.selectedColumn);
                console.info(result);
                  console.info(row);
                row.UnicFlag = result.Id;
                if (row.UnicFlag === 1 || row.UnicFlag === 2) {
                    row.Factor = 1;
                    row.Count = 1;
                }

                for (var i= 0, li = sc.DataSupportStuff.length; i<li; i++) {
                    if (row.Name.Id === sc.DataSupportStuff[i].NameSelected.Id) {
                        if (row.UnicFlag === 1 || row.UnicFlag === 2) {
                            sc.DataSupportStuff[i].Count = 1;
                        }
                        recountRowSupportStaff(sc.DataSupportStuff[i]);
                    }
                }
            }, function () {
                console.info('Modal dismissed at: ' + new Date());
            });
    };

    sc.changeWorkData = function(row) {
        if (!row.UnicFlag) {
            if (sc.formData.Area && row.Factor && row.Count) {
                row.Workload = row.Count * row.Factor * sc.formData.Area;
            } else {
                row.Workload = 0;
            }
            recountRow(row);

            recountData(sc.DataSupportStuff, recountRowSupportStaff);
            recountData(sc.DataSzr, recountRowMaterial);
            recountData(sc.DataChemicalFertilizers, recountRowMaterial);
            recountData(sc.DataSeed, recountRowMaterial);
            recountData(sc.DataDopMaterial, recountRowMaterial);
        }
    };
     //пересчет обьем работ
    sc.changeWorkload = function(row) {
        if (!row.UnicFlag && row.Workload) {
            recountRow(row);

            recountData(sc.DataSupportStuff, recountRowSupportStaff);
            recountData(sc.DataSzr, recountRowMaterial);
            recountData(sc.DataChemicalFertilizers, recountRowMaterial);
            recountData(sc.DataSeed, recountRowMaterial);
            recountData(sc.DataDopMaterial, recountRowMaterial);
        }
    };

    var recountData = function(data, f) {
        if (data) {
            for (var i = 0, li = data.length; i < li; i++) {
                f(data[i]);
            }
        }
    }

    sc.dateChange = function(row, date) {
        if (row.DateEnd && row.DateStart) {
            if (row.DateStart > row.DateEnd) {
                row.DateEnd = row.DateStart;
            }
        }
    };

    sc.updateSelectedCell = function (colField, selectedVal, displayName, row) {
        sc.colField = colField;
        var typeModal = modalType.WIN_SELECT;
        var modalValues;
        if (sc.infoData) {
            switch (sc.colField) {
                case "Name":
					displayName="Наименование работ";
                    modalValues = sc.infoData.WorkList;
                    break;
                case "Equipment":
				  sc.checkFillRow(sc.infoData.EquipmentList,row.Equipment );
                    typeModal = modalType.WIN_SELECT_MULTI;
                    modalValues = sc.infoData.EquipmentList;
                    selectedVal = row[sc.colField] ? row[sc.colField] : [];
                    break;
                case "Tech":
				 sc.checkFillRow(sc.infoData.TechList,row.Tech );
                    typeModal = modalType.WIN_SELECT_MULTI;
                    modalValues = sc.infoData.TechList;
                    selectedVal = row[sc.colField] ? row[sc.colField] : [];
                    break;
                case "NameSZR":
                    modalValues = sc.infoData.SzrList;
                    break;
                case "NameFertilizer":
                    modalValues = sc.infoData.FertilizerList;
                    break;
                case "NameSeed":
                    modalValues = sc.infoData.SeedList;
                    break;
					case "NameDopMaterial":
                    modalValues = sc.infoData.DopMaterialList;
                    break;
                case "NameSelectedWorkForMaterial":
				   var selected_works = [];
				   
				    for (var i = 0;i<sc.DataWorks.length;i++) {
					
					selected_works.push({Name: sc.DataWorks[i].Name.Name,
					                     Id : sc.DataWorks[i].Name.Id,
					                     index :i+1,
										 id_tc_oper: sc.DataWorks[i].Id,
										 Id1 : sc.DataWorks[i].Id1,
					});
					
                    }
                  console.log(selected_works);
				  modalValues = selected_works;	
				break;
                case "NameSelected":
				displayName="Наименование работ";
                    var selected_works = [];
                    for (var i = 0;i<sc.DataWorks.length;i++) {
					
					selected_works.push({Name: sc.DataWorks[i].Name.Name,
					                     Id : sc.DataWorks[i].Name.Id,
					                     index :i+1,
										 id_tc_oper: sc.DataWorks[i].Id,
										 Id1 : sc.DataWorks[i].Id1,
					});
					
                    }
					  for (var i = 0; i<sc.DataSupportStuff.length; i++) {
						   for (var j = 0;j<selected_works.length; j++) {
							 if (sc.DataSupportStuff[i].index == selected_works[j].index)
						   {
						 selected_works.splice(selected_works.indexOf(selected_works[j]),1);
						   }
						   }
					  }
				
					console.log(selected_works);
                    modalValues = selected_works;
                    break;
                case "NameService":
                    modalValues = sc.infoData.ServiceList; 
            }

            modals.showWindow(typeModal, {
                nameField: displayName,
                values: modalValues,
                selectedValue: selectedVal
            })
                .result.then(function (result) {
					//подтягивание Расценок
                    console.info(sc.selectedColumn);
                    console.info(result);
					  console.info(row);
                      //
		if (row.Tech!=null)	{if (row.Tech.length!=0) { var techId=row.Tech[0].Id} else {var techId=null;}} else {var techId=null;}	
        if (row.Equipment!=null)   { if (row.Equipment.length!=0) { var equiId=row.Equipment[0].Id} else {var equiId=null;}	} else 	{var equiId=null;}	
        if (row.Name!=null) {var tptasId=row.Name.Id;} else {var tptasId=null;}		
					  //		
         if (sc.colField === "Tech") {if (result.length!=0) {techId=result[0].Id;}}	  //техника
		 if (sc.colField === "Equipment") { if (result.length!=0) {equiId=result[0].Id;}}	  
		 if (sc.colField === "Name") {if (result.length!=0) {tptasId=result.Id;}}	  
		 
		 if (sc.colField != "FuelType") {
		   requests.serverCall.post($http, "TechCardTemp/GetRateTemp",{
			operationId : row.Id
        }, function (data, status, headers, config) {
            console.info(data);
			//id_tc_operation
			if (data.Id!=null && met!=0) {row.Id1=met+1; met = met + 1;}
			if (data.Id!=null && met==0) {row.Id1=data.Id; met=data.Id; };
        }, function () {
        });
		 }  
                    if (sc.colField === "Name" && row["Name"]) {
                        var old = row["Name"].Id;
                    }
                    row[colField] = result;
                  
                    if (sc.colField === "Name") {
                        recountRow(row);
                        if (old) changeWork(old, row.Name);
                    } else if (sc.colField === "Tech") {
                        for (var i= 0,li=sc.infoData.TechList.length; i<li; i++) {
                            sc.infoData.TechList[i].selectedFlag = false;
                        }
                    } else if (sc.colField === "Equipment") {
                        for (var i= 0,li=sc.infoData.EquipmentList.length; i<li; i++) {
                            sc.infoData.EquipmentList[i].selectedFlag = false;
                        }
                    } else if (sc.colField === "NameSelected") {
						
						row.index = result.index;
						if (result.Id1 != null) {
						row.id_tc_oper = result.Id1;	
						} else {
						row.id_tc_oper = result.id_tc_oper;
						;}
                        recountRowSupportStaff(row);
                    } else if (sc.colField === 'NameSelectedWorkForMaterial')  {
					
							//
						row.index = result.index;
						if (result.Id1 != null) {
						row.id_tc_oper = result.Id1;	
						console.log(result.Id1);
						} else {
						row.id_tc_oper = result.id_tc_oper;
						console.log(result.id_tc_oper);
						;}
						//
					} else if (sc.colField === 'NameFertilizer' || sc.colField === 'NameSeed' || sc.colField === 'NameSZR' || sc.colField==='NameDopMaterial') {
                    } else if (sc.colField === "FuelType") {
                        sc.recountFuel(row);

                    }
                }, function () {
                    console.info('Modal dismissed at: ' + new Date());
                });
        } else {
            console.log('не пришла sc.infoData');
        }
    };

    var changeWork = function(oldId, newWork) {
        for (var i= 0, li = sc.DataSupportStuff.length; i<li; i++) {
            if (sc.DataSupportStuff[i].NameSelected.Id === oldId) {
                sc.DataSupportStuff[i].NameSelected = newWork;
            }
        }

        changeWorkInMaterial(sc.DataSzr);
        changeWorkInMaterial(sc.DataChemicalFertilizers);
        changeWorkInMaterial(sc.DataSeed);
        changeWorkInMaterial(sc.DataDopMaterial);
        function changeWorkInMaterial(array) {
            for (var i= 0, li = array.length; i<li; i++) {
                if (array[i].NameSelectedWorkForMaterial.Id === oldId) {
                    array[i].NameSelectedWorkForMaterial = newWork;
                }
            }
        }
    };

    var recountRow = function(row) {
		
    };
      //удаление работы
    sc.delRowWork = function(row) {
        console.log(sc.DataWorks.indexOf(row));
        console.log(row);
		//
		//удаление материалов
	//	DataChemicalFertilizers
		 for (var i = 0; i<sc.DataChemicalFertilizers.length; i++) {
							 if (sc.DataChemicalFertilizers[i].index == sc.DataWorks.indexOf(row)+1)
						   {
						 sc.DataChemicalFertilizers.splice(sc.DataChemicalFertilizers.indexOf(sc.DataChemicalFertilizers[i]),1); i--;
						   }  
					  }
		//	DataSzr
		 for (var i = 0; i<sc.DataSzr.length; i++) {
							 if (sc.DataSzr[i].index == sc.DataWorks.indexOf(row)+1)
						   {
						 sc.DataSzr.splice(sc.DataSzr.indexOf(sc.DataSzr[i]),1);i--;
						   }  
					  }
			//	DataSeed
		 for (var i = 0; i<sc.DataSeed.length; i++) {
							 if (sc.DataSeed[i].index == sc.DataWorks.indexOf(row)+1)
						   {
						 sc.DataSeed.splice(sc.DataSeed.indexOf(sc.DataSeed[i]),1);i--;
						   }  
					  }
			//	DataDopMaterial
		 for (var i = 0; i<sc.DataDopMaterial.length; i++) {
							 if (sc.DataDopMaterial[i].index == sc.DataWorks.indexOf(row)+1)
						   {
						 sc.DataDopMaterial.splice(sc.DataDopMaterial.indexOf(sc.DataDopMaterial[i]),1);i--;
						   }  
					  }
		//1
		 for (var i = 0; i<sc.DataChemicalFertilizers.length; i++) {
			 if (sc.DataChemicalFertilizers[i].index>(sc.DataWorks.indexOf(row)+1))
			 {
				sc.DataChemicalFertilizers[i].index=sc.DataChemicalFertilizers[i].index-1; 
			 }
		 }
		//2
		 for (var i = 0; i<sc.DataSzr.length; i++) {
			 if (sc.DataSzr[i].index>(sc.DataWorks.indexOf(row)+1))
			 {
				sc.DataSzr[i].index=sc.DataSzr[i].index-1; 
			 }
		 }
		//3
		 for (var i = 0; i<sc.DataSeed.length; i++) {
			 if (sc.DataSeed[i].index>(sc.DataWorks.indexOf(row)+1))
			 {
				sc.DataSeed[i].index=sc.DataSeed[i].index-1; 
			 }
		 }
		 //4
		  for (var i = 0; i<sc.DataDopMaterial.length; i++) {
			 if (sc.DataDopMaterial[i].index>(sc.DataWorks.indexOf(row)+1))
			 {
				sc.DataDopMaterial[i].index=sc.DataDopMaterial[i].index-1; 
			 }
		 }
		//удаление вспомогательный персонал
			  for (var i = 0; i<sc.DataSupportStuff.length; i++) {
							 if (sc.DataSupportStuff[i].index == sc.DataWorks.indexOf(row)+1)
						   {
						 sc.DataSupportStuff.splice(sc.DataSupportStuff.indexOf(sc.DataSupportStuff[i]),1);
						   }  
					  }
		//пересчет индексов вспомогательного персонала
		 for (var i = 0; i<sc.DataSupportStuff.length; i++) {
			 if (sc.DataSupportStuff[i].index>(sc.DataWorks.indexOf(row)+1))
			 {
				sc.DataSupportStuff[i].index=sc.DataSupportStuff[i].index-1; 
			 }
		 }
        sc.DataWorks.splice(sc.DataWorks.indexOf(row), 1);	
    };


    //----------------------------------------------------------end-Работы------------------------------------------------------------------------------
    //----------------------------------------------------------Техника---------------------------------------------------------------------------------
    sc.gridOptions_Tech = {};
    setCommonGridOption(sc.gridOptions_Tech);
    sc.gridOptions_Tech.data = 'DataWorks';
    sc.gridOptions_Tech.columnDefs = [
        {
            field: "index", displayName: " ", width: "5%",
            cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()">' +
            '<textinputnotcomma data="getIndex(row.entity)" ng-model="row.entity[col.field]" class="bordernone" readonly="1"/>' +
            '</div>'
        },
        {
            field: "Name", displayName: "Тип работ", width: "45%",
            cellTemplate: '<a tooltip="{{row.entity[col.field][\'Name\']}}" tooltip-popup-delay="1000" tooltip-class="my-custom-tooltip" tooltip-placement="top" tooltip-append-to-body="true">' +
            '<div class="ngCellText" ng-class="col.colIndex()">' +
            '<input value="{{row.entity[col.field][\'Name\']}}" style="border: none;" readonly>' +
            '</div>'
        },
        {
            field: "Tech", displayName: "Агрегат", width: "50%",
            cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()" style="padding: 0px 5px;">' +
            '<input ng-value="getValueForInput(row.entity.Tech)" placeholder="Трактор, комбайн, автомобиль" style="border: none; height: 49%" data-ng-click="updateSelectedCell(\'Tech\', row.entity.Tech.Id,\'Техника\',row.entity,$event)"  readonly>' +
            '<input ng-value="getValueForInput(row.entity.Equipment)" placeholder="Машины, оборудование" style="border: none; height: 49%"  data-ng-click="updateSelectedCell(\'Equipment\', row.entity.Equipment.Id,\'Оборудование\',row.entity,$event)"  readonly>' +
            '</div>'
        },
        {field: "Id", width: "0%"}
    ];
    sc.getValueForInput = function(array) {
        var str = "";
        if (array && array.length > 0) {
            for (var i = 0, li = array.length; i < li; i++) {
                str += array[i].Name + ", "
            }
            str = str.substr(0, str.length - 2);
        }
        return str;
    };

    sc.editCell = function (row, cell, column) {
        if (column == 'Comment') commentClick(cell);
    };

    function commentClick(comment) {
        modals.showWindow(modalType.WIN_COMMENT, {
            nameField: "Комментарий",
            comment: comment
        })
            .result.then(function (result) {
                console.info(result);
                sc.selectedRow[0]['Comment'] = result;
                //TODO sendTask();
            }, function () {
                console.info('Modal dismissed at: ' + new Date());
            });
    }
    //----------------------------------------------------------end-Техника-----------------------------------------------------------------------------
    //----------------------------------------------------------Персонал--------------------------------------------------------------------------------
    sc.gridOptions_Emp = {};
    setCommonGridOption(sc.gridOptions_Emp);
    sc.gridOptions_Emp.data = 'DataWorks';
    sc.gridOptions_Emp.columnDefs = [
        {
            field: "index", displayName: " ", width: "5%",
            cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()">' +
            '<textinputnotcomma data="getIndex(row.entity)" ng-model="row.entity[col.field]" class="bordernone" readonly="1"/>' +
            '</div>'
        },
        {
            field: "Name", displayName: "Наименование работ", width: "95%",
            cellTemplate: '<a tooltip="{{row.entity[col.field][\'Name\']}}" tooltip-popup-delay="1000" tooltip-class="my-custom-tooltip" tooltip-placement="top" tooltip-append-to-body="true">' +
            '<div class="ngCellText" ng-class="col.colIndex()">' +
            '<input value="{{row.entity[col.field][\'Name\']}}" style="border: none;" readonly>' +
            '</div>'
        },
	
        {field: "Id", width: "0%"}
    ];
    //----------------------------------------------------------end-Персонал----------------------------------------------------------------------------
    //----------------------------------------------------------Вспомогательный-Персонал----------------------------------------------------------------


    sc.gridOptions_SupportStuff = {};
    setCommonGridOption(sc.gridOptions_SupportStuff);
    sc.gridOptions_SupportStuff.data = 'DataSupportStuff';
    sc.gridOptions_SupportStuff.columnDefs = [
        {
            field: "index", displayName: " ", width: "5%",
		    cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()">' +
            '<input value="{{row.entity[col.field]}}" class="bordernone" ng-model="row.entity[col.field]" placeholder="" readonly>' +
            '</div>'
        },
        {
            field: "NameSelected", displayName: "Наименование работ", width: "55%",
            cellTemplate: '<a tooltip="{{row.entity[col.field][\'Name\']}}" tooltip-popup-delay="1000" tooltip-class="my-custom-tooltip" tooltip-placement="top" tooltip-append-to-body="true">' +
            '<div class="ngCellText" ng-class="col.colIndex()" ng-click="editCell(row.entity, row.getProperty(col.field), col.field)">' +
            '<input value="{{row.entity[col.field][\'Name\']}}" placeholder="Наимнование работы" style="border: none;"  data-ng-click="updateSelectedCell(col.field, row.entity[col.field][\'Id\'],\'№ поля\',row.entity,$event)"  readonly>' +
            '</div>'
        },
        {
            field: "Count", displayName: "Кол-во чел", width: "15%",
            cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()">' +
            '<textinputnotcomma value="{{row.entity[col.field]}}" data="row.entity[col.field]" regex-name="numbersWithPoint" field-change="recountRowSupportStaff(row.entity)" readonly="row.entity.UnicFlag"></textinputnotcomma>' +
            '</div>'
        },
        {
            field: "Price", displayName: "Расценка,р/ПД", width: "20%",
            cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()">' +
            '<textinputnotcomma value="{{row.entity[col.field]}}" data="row.entity[col.field]" regex-name="numbersWithPoint" field-change="recountRowSupportStaff(row.entity)"></textinputnotcomma>' +
            '</div>'
        },
        {
            field: "Delete", displayName: " ", width: "5%",
            cellTemplate: '<div class="ngCellText {{getClassCopyCell(row.entity.Status)}}, clear_row" style="font-size: 36px!important;margin: auto;margin-top: 7px;" ng-switch on="row.entity[col.field]" ng-click="delRowSupportStaff(row.entity)"></div>'   
        },
        {field: "Id", width: "0%"},
		{
            field: "id_tc_oper", displayName: " ", width: "0%",
		    cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()">' +
            '<input value="{{row.entity[col.field]}}" class="bordernone" ng-model="row.entity[col.field]" placeholder="" readonly>' +
            '</div>'
        },
	
    ];

    sc.createNewSupportStaff = function() {
        var supportStuff =
        {
            NameSelected: null,
            Count: 1,
            Price: 0,
        };
        sc.DataSupportStuff.push(supportStuff);
    };
   //пересчет вспомогательный персонал
    var recountRowSupportStaff = function(row) {
		console.log(row.index);
        var workRow = getRowWork(row.index);
        row.UnicFlag = workRow.UnicFlag;
        if (row.UnicFlag) {
        } else {
            if (workRow.ShiftCount && row.Count && row.Price) {
            } else if (workRow.ShiftCount && row.Count && row.Cost) {
         
            }
        }
    };
    sc.recountRowSupportStaff = recountRowSupportStaff;

    var getRowWork = function(id) {
		  for (var i= 0; i< sc.DataWorks.length; i++) {
		sc.DataWorks[i].index=i+1;
		   }
        for (var i= 0, li = sc.DataWorks.length; i<li; i++) {
			console.log(sc.DataWorks[i].index);
            if (sc.DataWorks[i].index === id) {
					console.log(sc.DataWorks[i].index);
                return sc.DataWorks[i];
            }
        }
    };
    //удаление вспомогательный персонал
    sc.delRowSupportStaff = function(row) {
        console.log(sc.DataSupportStuff.indexOf(row));
        console.log(row);
        sc.DataSupportStuff.splice(sc.DataSupportStuff.indexOf(row), 1);
    };

    //----------------------------------------------------------end-Вспомогательный-Персонал------------------------------------------------------------
    //----------------------------------------------------------Материалы-------------------------------------------------------------------------------
    sc.getMaterialRowIndex = function(id) {
        for (var i =0, li=sc.DataWorks.length; i<li; i++) {
            if (sc.DataWorks[i].Name.Id === id) {
                return i+1;
            }
        }
    };

    sc.gridOptions_Szr = {};
    setCommonGridOption(sc.gridOptions_Szr);
    sc.gridOptions_Szr.data = 'DataSzr';

    sc.gridOptions_ChemicalFertilizers = {};
    setCommonGridOption(sc.gridOptions_ChemicalFertilizers);
    sc.gridOptions_ChemicalFertilizers.data = 'DataChemicalFertilizers';

    sc.gridOptions_Seed = {};
    setCommonGridOption(sc.gridOptions_Seed);
    sc.gridOptions_Seed.data = 'DataSeed';
	
	 sc.gridOptions_DopMaterial = {};
    setCommonGridOption(sc.gridOptions_DopMaterial);
    sc.gridOptions_DopMaterial.data = 'DataDopMaterial';

    var getColumns = function(material_column) {
        return [
           {
            field: "index", displayName: " ", width: "5%",
		    cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()">' +
            '<input value="{{row.entity[col.field]}}" class="bordernone" ng-model="row.entity[col.field]" placeholder="" readonly>' +
            '</div>'
             },
            {
                field: "NameSelectedWorkForMaterial", displayName: "Тип работ", width: "25%",
                cellTemplate: '<a tooltip="{{row.entity[col.field][\'Name\']}}" tooltip-popup-delay="1000" tooltip-class="my-custom-tooltip" tooltip-placement="top" tooltip-append-to-body="true">' +
                '<div class="ngCellText" ng-class="col.colIndex()">' +
                '<input value="{{row.entity[col.field][\'Name\']}}" placeholder="Тип работ" style="border: none;"  data-ng-click="updateSelectedCell(col.field, row.entity[col.field][\'Id\'],\'Наименование работ\',row.entity,$event)"  readonly>' +
                '</div>'
            },
            {
                field: material_column,
                displayName: "Материал",
                width: "45%",
                cellTemplate: '<a tooltip="{{row.entity[col.field][\'Name\']}}" tooltip-popup-delay="1000" tooltip-class="my-custom-tooltip" tooltip-placement="top" tooltip-append-to-body="true">' +
                '<div class="ngCellText" ng-class="col.colIndex()">' +
                '<input value="{{row.entity[col.field][\'Name\']}}" placeholder="Материал" style="border: none;"  data-ng-click="updateSelectedCell(col.field, row.entity[col.field][\'Id\'],\'Название материала\',row.entity,$event)"  readonly>' +
                '</div>'
            },
            {
                field: "ConsumptionRate", displayName: "Расход, ед/га", width: "20%",
                cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()">' +
                '<input value="{{row.entity[col.field]}}" ng-model="row.entity[col.field]" placeholder="Норма расхода" ng-change="recountRowMaterial(row.entity)">' +
                '</div>'
            },
            {
            field: "Delete", displayName: " ", width: "5%",
            cellTemplate: '<div class="ngCellText {{getClassCopyCell(row.entity.Status)}}, clear_row" style="font-size: 36px!important;margin: auto;margin-top: 7px;" ng-switch on="row.entity[col.field]" ng-click="delRowSZR(row.entity)"></div>'   
            },
            {field: "Id", width: "0%"},
				{
            field: "id_tc_oper", displayName: " ", width: "0%",
		    cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()">' +
            '<input value="{{row.entity[col.field]}}" class="bordernone" ng-model="row.entity[col.field]" placeholder="" readonly>' +
            '</div>'
        },
        ];
    };

    sc.gridOptions_Szr.columnDefs = getColumns('NameSZR');
    sc.gridOptions_ChemicalFertilizers.columnDefs = getColumns('NameFertilizer');
    sc.gridOptions_Seed.columnDefs = getColumns('NameSeed');
    sc.gridOptions_DopMaterial.columnDefs = getColumns('NameDopMaterial');
    sc.createNewSZR = function() {
        var newSZR = {
            NameSZR: null,
            Type: 1
        };
        createNewMaterial(newSZR, sc.DataSzr);
    };

    sc.createNewFertilizer = function() {
        var newFertilizer = {
            NameFertilizer: null,
            Type: 2
        };
        createNewMaterial(newFertilizer, sc.DataChemicalFertilizers);
    };

    sc.createNewSeed= function() {
        var newSeed = {
            NameSeed: null,
            Type: 4
        };
        createNewMaterial(newSeed, sc.DataSeed);
    };
	//
   sc.createNewDopMaterial= function() {
        var newDopMaterial  = {
            NameSeed: null,
            Type: 5
        };
        createNewMaterial(newDopMaterial, sc.DataDopMaterial);
    };
	//
	
	
    var createNewMaterial = function(newMaterial, data) {
        newMaterial.NameSelected = null;
        newMaterial.ConsumptionRate = 0;
        newMaterial.Price = 0;
        data.push(newMaterial);
    };

    var recountRowMaterial = function(row) {
    };
    sc.recountRowMaterial = recountRowMaterial;

    sc.delRowSZR = function(row) {
        if (row.Type === 1) {
            sc.DataSzr.splice(sc.DataSzr.indexOf(row), 1);
        } else if (row.Type === 2) {
            sc.DataChemicalFertilizers.splice(sc.DataChemicalFertilizers.indexOf(row), 1);
        } else if (row.Type === 4) {
            sc.DataSeed.splice(sc.DataSeed.indexOf(row), 1);		
        }
		else if (row.Type === 5) {
            sc.DataDopMaterial.splice(sc.DataDopMaterial.indexOf(row), 1);		
        }
    };
    //----------------------------------------------------------end-Материалы---------------------------------------------------------------------------
    //-------------------------------------------------------------Услуги-------------------------------------------------------------------------------
    sc.gridOptions_Services = {};
    setCommonGridOption(sc.gridOptions_Services);
    sc.gridOptions_Services.data = 'DataServices';
    sc.gridOptions_Services.columnDefs = [
            {
                field: "index", displayName: " ", width: "5%",
                cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()">' +
                '<textinputnotcomma data="getStuffRowIndex(row.entity.NameSelected.Id)" ng-model="row.entity[col.field]" class="bordernone" readonly="1"/>' +
                '</div>'
            },
            {
                field: "NameService", displayName: "Название услуги", width: "70%",
                cellTemplate: '<a tooltip="{{row.entity[col.field][\'Name\']}}" tooltip-popup-delay="1000" tooltip-class="my-custom-tooltip" tooltip-placement="top" tooltip-append-to-body="true">' +
                '<div class="ngCellText" ng-class="col.colIndex()">' +
                '<input value="{{row.entity[col.field][\'Name\']}}" placeholder="Тип работ" style="border: none;"  data-ng-click="updateSelectedCell(col.field, row.entity[col.field][\'Id\'],\'Наименование услуг\',row.entity,$event)"  readonly>' +
                '</div>'
            },
            {
                field: "Cost", displayName: "Стоимость, р", width: "20%",
                cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()">' +
                '<input value="{{row.entity[col.field]}}" ng-model="row.entity[col.field]" placeholder="Стоимость" ng-change="recountService()">' +
                '</div>'
            },
            {
            field: "Delete", displayName: " ", width: "5%",
            cellTemplate: '<div class="ngCellText {{getClassCopyCell(row.entity.Status)}}, clear_row" style="font-size: 36px!important;margin: auto;margin-top: 7px;" ng-switch on="row.entity[col.field]" ng-click="delRowService(row.entity)"></div>'
            },            
            {field: "Id", width: "0%"}
        ];

    sc.createNewServices = function() {
        var service =
        {
            NameService: null,
            Cost: 0
        };
        sc.DataServices.push(service);
    };

    sc.recountService = function() {
    };

    sc.delRowService = function(row) {
        console.log(sc.DataServices.indexOf(row));
        console.log(row);
        sc.DataServices.splice(sc.DataServices.indexOf(row), 1);
    };

    //----------------------------------------------------------submit----------------------------------------------------------------------------------
    sc.submitForm = function() {
  console.log(sc.DataDopMaterial);
  sc.formData.TypeModuleId = 1; //шаблон
            requests.serverCall.post($http, "TechCardTemp/CreateUpdateTC_temp", {
                TC: sc.formData,
                Works: sc.DataWorks,
                SupportStuff: sc.DataSupportStuff,
                DataSzr: sc.DataSzr,
                DataChemicalFertilizers: sc.DataChemicalFertilizers,
                DataSeed: sc.DataSeed,
                DataServices: sc.DataServices,
				DataDopMaterial :sc.DataDopMaterial,
            }, function (data, status, headers, config) {
                console.log(data);
                $location.path('/techcard_temp_list');
            }, function () {
            });
        
    };

    sc.cancel = function() {
        $location.path('/techcard_temp_list');
    };

    var loadTcById = function() {
        if (sc.formData.Id) {
            requests.serverCall.post($http, 'TechCardTemp/GetTC_tempById', sc.formData.Id, function (data, status, headers, config) {
                console.info(data);
                sc.DataSzr = data.DataSZR;
                sc.DataChemicalFertilizers = data.DataChemicalFertilizers;
                sc.DataSeed = data.DataSeed;
                sc.DataSupportStuff = data.SupportStuff;
                sc.DataServices = data.DataServices;
                sc.formData = data.TC;
                sc.DataWorks = data.Works;
				sc.DataDopMaterial =data.DataDopMaterial;
            }, function () {
            });
        }
    };

}]);