

/**
 * @ngdoc controller
 * @name app.controller:KPICtrl
 * @description
 * # IncomingListCtrl
 * Это контроллер позволяющий пользователю работать со списком прихода топлива "IncomingFuelList"
 * @requires $scope
 * @requires $http
 * @requires requests
 * @requires $location
 * @requires $cookieStore
 * @requires $rootScope
 * @requires modals
 * @requires modalType
 *
 * @property {function} dateChange:функция Которая позволяет пользователю изменить дату
 * <pre> 
         sc.dateChange = function ()
 * </pre>
 * @property {function} getDayIncoming:функция Которая позволяет ползователю изменить информацию о приходе топлива (data, status, headers, config)
 * <pre> 
         var getDayIncoming = function () 
 * </pre>
 * @property {function} afterSelectionChange,beforeSelectionChange:функции  Которые изменяют таблицу после её редоктирования пользователем до и после
 * <pre> 
         afterSelectionChange: function (row) { 
         { ...
           ...
           ...};
         beforeSelectionChange: function (row, event)
         { ...
           ...
           ...};
 * </pre>
 * @property {function} createTask:функция Которая создаёт новую строку для дальнейших её изменениях 
 * <pre> 
         sc.createTask = function () {
            console.info(sc.dateIncoming);
            var newIncome = {
                Fuel: null,
                Count: 0,
                Price: 0,
                Cost: 0,
                Date: sc.dateIncoming,
                Contractor: null,
                VAT: null,
                Type_salary_doc: null,
                Comment: ''
            }}; 
 * </pre>
 * @property {function} changeCountPrice:функция Которая позволяет изменить поля (price, count)
 * <pre> 
         sc.changeCountPrice = function(row) 
 * </pre>
 * @property {function} editCell:функция Которая вы
 * <pre> 
         sc.editCell = function (row, cell, column) 
 * </pre>
 * @property {function} commentClick:функция Которая 
 * <pre> 
         function commentClick(comment) 
 * </pre>
 * @property {function} updateSelectedCell:функция 
 * <pre> 
         sc.updateSelectedCell = function (colField, selectedVal, displayName, row) 
 * </pre>
 * @property {function} delRow:функция 
 * <pre> 
         sc.delRow = function(row) 
 * </pre>
 * @property {function} postIncome:функция Которая проверяет всё ли заполнено, и выдаёт ошибку если что то не так
 * <pre> 
         sc.postIncome = function() 
 * </pre>
 * @property {function} callback:функция которая позволяет пользователю вернуться 
 * <pre> 
         callback: function () 
 * </pre>

**/
var app = angular.module("KPI", ["ngGrid", "services", 'ngCookies', "modalWindows"]);
app.controller("KPICtrl", ["$scope", "$http", "requests", "$location", '$cookieStore', '$rootScope', "modals", "modalType", "preloader","$stateParams", 
				function ($scope, $http, requests, $location, $cookieStore, $rootScope, modals, modalType, preloader, $stateParams) {
    var sc = $scope;
    sc.YearId = new Date().getFullYear();

    var _lastYear;
    sc.refreshYear = function() {
        if (_lastYear != sc.YearId) {
            requests.serverCall.get($http, "KPICtrl/KPIInfo?year=" + sc.YearId, function (data, status, headers, config) {
                _lastYear = sc.YearId;
                console.info(data);
                sc.infoData = data;
                sc.IdTc = sc.infoData.TCList[0].Id;
            }, function () {
            });
        }
    };
    sc.refreshYear();

    sc.refreshPlant = function() {
        if (sc.YearId && sc.IdTc) {
            requests.serverCall.post($http, "KPICtrl/KPIWorks", {
                year: sc.YearId,
                id_tc: sc.IdTc
            }, function (data, status, headers, config) {
                console.info(data);
                sc.Works = data.Works;
                sc.Fields = data.Fields;
                sc.Area = 0;
                for (var i=0, li = sc.Fields.length; i<li; i++) {
                    sc.Area += sc.Fields[i].Area;
                }                
            }, function () {
            });
        } else {
            sc.panelInfo = [];
        }
    };

    sc.openWayLists = function(operationId, typeTaskId) {
        requests.serverCall.post($http, "KPICtrl/GetTasksByPlantYear", {
            year: sc.YearId,
            id_tc: sc.IdTc,
            id_tc_operation: operationId
        }, function(data) {
            var selWorks;
            for (var i=0, li=sc.Works.length; i<li; i++) {
                if (sc.Works[i].Id === operationId) {
                    selWorks = sc.Works[i].tasks != null && sc.Works[i].tasks != undefined ? sc.Works[i].tasks : [];
                }
            }
            openSelect(data, 'Выполненные задачи', selWorks, function(tasks) {
                requests.serverCall.post($http, "KPICtrl/AddTasksToWork", {
                    tasks: tasks,
                    req: {
                        id_tc_operation: operationId,
                        year: sc.YearId,
                        id_tc: sc.IdTc
                    }
                }, function(data) {
                    sc.Works = data.Works;
                });
            });
        }, function() {});
    };



    function openSelect(data, displayName, selectedValue, cb) {
        var typeModal = modalType.WIN_SELECT_MULTI;
        var modalValues = data;
        modals.showWindow(typeModal, {
            nameField: displayName,
            values: modalValues,
            selectedValue: selectedValue
        }).result.then(function (result) {
            console.info(result);
            cb(result);
        });
    }

    sc.openKpis = function(task, work) {
        requests.serverCall.post($http, "KPICtrl/GetKPI", task.Id, function(data) {
            modals.showWindow(modalType.WIN_DOUBLE_LIST, {
                title1: task.Date + " " + task.Field + " " + task.Name,
                title2: "Все показатели",
                lists: data
            }).result.then(function(result) {
                console.info(result);
                task.Kpi = result.TotalRate;
                var tasks = work.tasks;
                var total = tasks.length;
				var total_area =0;
                var sum = 0;
                for (var i=0, li=tasks.length; i<li; i++) {
                    sum += tasks[i].Kpi*tasks[i].Area;
					total_area +=tasks[i].Area;
                }
                work.TotalKPI = {
                    total: total,
                    sum: sum,
                    rate: (sum / total_area).toFixed(2)
                };
                requests.serverCall.post($http, "KPICtrl/AddKPIValue?TaskId=" + task.Id + "&TotalRate=" + result.TotalRate, result.ListSelected, function(data) {
                    console.info(data);
                }, function() {

                });
            });
        }, function() {});        
    };
    
    sc.delRow = function(task, work) {
        modals.showWindow(modalType.WIN_DIALOG, {
        title: "Удаление", message: "Вы уверены, что хотите удалить задачу?", callback: function () { 
            var tasks = work.tasks;
            var sum = 0;
            for (var i=0, li=tasks.length; i<li; i++) {
                if (task.Id === tasks[i].Id) {
                    tasks.splice(i,1);
                    i--;
                    li--;// li=tasks.length
                } else {
                    sum += tasks[i].Kpi; 
					total_area +=tasks[i].Area;
                }
            }  
            var total = tasks.length;
			var total_area =0;
            work.TotalKPI = {
                total: total,
                sum: sum,
                rate: (sum / total_area).toFixed(2)
            };
            requests.serverCall.get($http, "KPICtrl/DeleteTasksFromWork?tasId=" + task.Id, function(data) {
                console.info(data);
            }, function() {
            });
        }
    });
    };
}]);


