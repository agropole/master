/**
 *  @ngdoc controller
 *  @name app.controller:RatesCtrl
 *  @description
 *  # RatesCtrl
 *  Это контроллер, определеяющий поведение формы расценок по задачам
 *  @requires $scope
 *  @requires $http
 *  @requires requests
 *  @requires modalType
 *  @requires modals

 *  @property {Object} sc scope
 *  @property {Object} infoData справочники
 *  @property {Object} saveInfo информация для сохранения
 *  @property {function} load запрос на расценки
 *  @property {function} sc.addRate добавление новой расценки для задачи
 *  @property {function} sc.openSelect открытие модальных окон справочников
 *  @property {function} selectDialogName выбор названия моадльного окна справочников
 *  @property {function} addForSave добавление редактированной расценки для дальнейшего сохранения
 *  @property {function} sc.delRow удаление расценки
 *  @property {function} sc.save сохранение расценок
 *  @property {function} sc.changePrice пересчет стоимости после изменения цены
 *  @property {function} sc.save сохранить изменения
 *  @property {function} sc.cancel отмена изменений
 */

var app = angular.module("Rates", ["services", 'ngCookies', "modalWindows", "LogusConfig"]);
app.controller("RatesCtrl", ["$scope", "$http", "requests", "modals", "modalType", "LogusConfig", function ($scope, $http, requests, modals, modalType, LogusConfig) {
	var sc = $scope;
	var infoData = {};
	var saveInfo = [];

	function load() {
		requests.serverCall.get($http, "Rate/GetInfo", function (data, status, headers, config) {
			infoData = data;
		});
		requests.serverCall.get($http, "Rate/Get", function (data, status, headers, config) {
			sc.formData = data;

			setTimeout(function() {
				sc.addForSave = addForSave;
			}, 1000);
		});
	};
	load();

	sc.addRate = function(item) {
		var rate = {
			IdTptas:item.Id
		};
		item.Values.push(rate);
	};

	sc.openSelect = function(dictName, val) {
		var typeModal = modalType.WIN_SELECT;
        var modalValues = infoData[dictName];
        modals.showWindow(typeModal, {
            nameField: selectDialogName(dictName),
            values: modalValues,
            selectedValue: val.Id
        }).result.then(function (result) {
        	console.log(result);
            val[dictName] = result;
            addForSave(val);
        });
	};
	
	var selectDialogName = function(dictName) {
		switch (dictName) {
			case 'IdPlant':
				return 'Выбор культуры';
				break;
			case 'IdTrak':
				return 'Выбор техники';
				break;
			case 'IdEqui':
				return 'Выбор оборудования';
				break;
			case 'IdAgroReq':
				return 'Выбор агротехн.требований';
				break;
		}
	};

	var addForSave = function(row) {
		if (row.Id) {
			for (var i=0, li=saveInfo.length; i<li; i++) {
				var r = saveInfo[i];
				if (r.Document.Id == row.Id && r.command === 'update') {
					saveInfo.splice(i, 1);
					i--;
					li--;
				}
			}
			saveInfo.push({
				command: 'update',
				Document: row
			});
		}
	};

	sc.delRow = function(val, item) {
		for (var i=0, li = item.Values.length; i<li; i++) {
			var v = item.Values[i];
			if (v === val) {
				item.Values.splice(i,1);
				i--;
				li--;
			}
		}
		if (val.Id) {
			saveInfo.push({
				command: 'del',
				Document: val
			});
		}
	};

	sc.save = function() {
		for (var i=0,li = sc.formData.length; i<li; i++) {
			var t = sc.formData[i];
			for (var j=0, lj=t.Values.length; j<lj;j++) {
				var v = t.Values[j];
				if (!v.Id) {
					saveInfo.push({
						command: 'insert',
						Document: v
					});
				}
			}
		}
		
		requests.serverCall.post($http, "Rate/UpdateRate", saveInfo, function (data, status, headers, config) {
			load();
			saveInfo = [];
		});
	};

	sc.cancel = function() {
		load();
		saveInfo = [];
	};
}]);