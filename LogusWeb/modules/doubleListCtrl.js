/**
 *  @ngdoc controller
 *  @name app.controller:DoubleListCtrl
 *  @description
 *  # DoubleListCtrl
 *  Контроллер директивы doublelist. Выбор показателей из списка и оценка их.
 *  @requires $scope

 *  @property {Object} sc: scope
 *  @property {Object} sc.gridOptions: настройки таблицы
 *  @property {function} sc.selectKPI: выбрать показатель из списка
 *  @property {function} sc.deleteKPI: удалить выбранный показатель
 *  @property {function} sc.evaluate: при изменении значения - оценка показателя и выбор стиля
 *  @property {function} recountTotal: пересчет агрегированного значения
 *  @property {function} delKpiFromList: удаление показателя из левого или правого списка
 *  @property {function} init: инициализация списков
 */

var app = angular.module("DoubleList", ["ngGrid", "services", 'ngCookies', "modalWindows"]);
app.controller("DoubleListCtrl", ["$scope",  function ($scope) {
	var sc = $scope;

	sc.gridOptions = {
        enableColumnResize: false,
        enableRowSelection: false,
        showFooter: false,
        rowHeight: 49,
        headerRowHeight: 49,
        useExternalSorting: true,
        data: 'lists.ListSelected'
    };

    sc.gridOptions.columnDefs = [
        {
            field: "Name",
            displayName: "Показатель",
            width: "55%",
            cellTemplate: '<div class="ngCellText ng-scope" ng-class="row.entity.paintclass">' +
            '<input value="{{row.entity[col.field]}}" ng-model="row.entity[col.field]" placeholder="Культура" class="bordernone" readonly style="background-color: transparent">' +
            '</div>'
        },
        {
            field: "StandartValue", displayName: "Ст.значение", width: "20%",
            cellTemplate: '<div class="ngCellText ng-scope" ng-class="row.entity.paintclass">' +
            '<input ng-model="row.entity[col.field]" placeholder="эталон"  ng-change="evaluate(row.entity)">/' +
            '</div>'
        },
        {
            field: "Value", displayName: "Ф. значение", width: "20%",
            cellTemplate: '<div class="ngCellText ng-scope" ng-class="row.entity.paintclass">' +
            '<input ng-model="row.entity[col.field]" placeholder="значение"  ng-change="evaluate(row.entity)">' +
            '</div>'
        },
        {
            field: "Delete", displayName: " ", width: "5%",
            cellTemplate: '<div class="ngCellText ng-scope" ng-class="row.entity.paintclass">'+ 
                        '<button type="button" class="logus-btn btn" style="float:left; width: 32px;height: 39px;" ng-click="deleteKPI(row.entity)">></button>' +
                        '</div>',
            cellClass: 'cellToolTip'
        },
        {field: "Id", width: "0%"}
    ];

    sc.selectKPI = function(kpi) {
        delKpiFromList(kpi, sc.lists.ListOther);
    	sc.lists.ListSelected.push(kpi);
        recountTotal();

        var grid = sc.gridOptions.ngGrid;
        setTimeout(function() {
            grid.$viewport.scrollTop(l.length * grid.config.rowHeight);
        }, 100);
    };

    sc.deleteKPI = function(kpi) {
        var l = sc.lists.ListSelected;
        delKpiFromList(kpi, l);
        sc.lists.ListOther.unshift(kpi);
        recountTotal();
    };

    sc.evaluate = function(kpi) {
        console.info('kpi');
        if (kpi.StandartValue && kpi.Value) {
            if (kpi.StandartValue == 0 && kpi.Value == 0) {
                kpi.paintclass = "greenkpi";
                kpi.R = 1;
            } else if (kpi.StandartValue == 0 && kpi.Value != 0) {
                kpi.paintclass = "redkpi";
                kpi.R = 0;
            } else {
                var deviation = Math.abs(kpi.Value - kpi.StandartValue) / kpi.StandartValue* 100;
                console.log(deviation);
                kpi.deviation = deviation;
                if (deviation <= kpi.DevLimit) {
                    kpi.paintclass = "greenkpi";
                    kpi.R = kpi.Rate / 100;
                } else if (deviation <= kpi.DevLimit2) {
                    kpi.paintclass = "yellowkpi";
                    kpi.R = kpi.Rate2 / 100;
                } else if (kpi.DevLimit3 && deviation <= kpi.DevLimit3) {
                    kpi.paintclass = "redkpi";
                    kpi.R = kpi.Rate3 / 100;
                } else {
                    kpi.paintclass = "redkpi";
                    kpi.R = 0;
                }
            }
        } else {
            delete kpi.R;
        }
        recountTotal();
    };

    function recountTotal() {
        var list = sc.lists.ListSelected;
        var totalRate = 1;
		console.log(list);
        for (var i=0, li=list.length; i<li; i++) {
            if (list[i].R !== null && list[i].R !== undefined) {
				
                totalRate = totalRate * list[i].R;
				console.log(totalRate);
            }
        }
        sc.lists.totalRate = Math.round(totalRate * 100);
    }

    function delKpiFromList(kpi, list) {
    	for (var i=0, li=list.length; i<li; i++) {
    		if (list[i].IdKpi == kpi.IdKpi) {
    			list.splice(i, 1);
    			return;
    		}
    	}
	}

    function init() {
        var list = sc.lists.ListSelected;
        for (var i=0, li=list.length; i<li; i++) {
            sc.evaluate(list[i]);
        }
    }
    init();
}]);