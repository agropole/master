/**
 * @ngdoc controller
 * @name Warehouse.controlle:IncomingFuelListCtrl
 * @description
 * # IncomingFuelListCtrl
 * Это контроллер позволяющий пользователю работать со списком прихода топлива "IncomingFuelList"
 * @requires $scope
 * @requires $http
 * @requires requests
 * @requires $location
 * @requires $cookieStore
 * @requires $rootScope
 * @requires modals
 * @requires modalType
 *
 * @property {function} dateChange:функция Которая позволяет пользователю выбрать дату
 * <pre> 
         sc.dateChange = function ()
 * </pre>
 * @property {function} getDayIncoming:функция Которая загружает таблицу после выбора даты
 * <pre> 
         var getDayIncoming = function () 
 * </pre>
 * @property {function} afterSelectionChange,beforeSelectionChange:функции  Которые изменяют таблицу после её редоктирования пользователем до и после
 * <pre> 
         afterSelectionChange: function (row) { 
         { ...
           ...
           ...};
         beforeSelectionChange: function (row, event)
         { ...
           ...
           ...};
 * </pre>
 * @property {function} createTask:функция Которая создаёт новую строку для дальнейших её изменениях 
 * <pre> 
         sc.createTask = function () {
            console.info(sc.dateIncoming);
            var newIncome = {
                Fuel: null,
                Count: 0,
                Price: 0,
                Cost: 0,
                Date: sc.dateIncoming,
                Contractor: null,
                VAT: null,
                Type_salary_doc: null,
                Comment: ''
            }}; 
 * </pre>
 * @property {function} changeCountPrice:функция Которая позволяет изменить поля (price, count)
 * <pre> 
         sc.changeCountPrice = function(row) 
 * </pre>
 * @property {function} editCell:функция Которая показывает иконку коментария
 * <pre> 
         sc.editCell = function (row, cell, column) 
 * </pre>
 * @property {function} commentClick:функция Которая вызывает модальное окно коменнтария
 * <pre> 
         function commentClick(comment) 
 * </pre>
 * @property {function} updateSelectedCell:функция Которая вызывает модальное окно выбора
 * <pre> 
         sc.updateSelectedCell = function (colField, selectedVal, displayName, row) 
 * </pre>
 * @property {function} delRow:функция Которая удаляет строку из таблицы 
 * <pre> 
         sc.delRow = function(row) 
 * </pre>
 * @property {function} postIncome:функция Которая проверяет всё ли заполнено, и выдаёт ошибку если что то не так
 * <pre> 
         sc.postIncome = function() 
 * </pre>
 * @property {function} callback:функция которая позволяет пользователю вернуться 
 * <pre> 
         callback: function () 
 * </pre>

**/
var app = angular.module("IncomingFuelList", ["ngGrid", "services", 'ngCookies', "modalWindows"]);
app.controller("IncomingFuelListCtrl", ["$scope", "$http", "requests", "$location", '$cookieStore', '$rootScope', "modals", "modalType",
    function ($scope, $http, requests, $location, $cookieStore, $rootScope, modals, modalType) {
        var sc = $scope;
        var cellTemplate = '<div class="ngCellText"  data-ng-model="row"><div data-ng-click="updateSelectedCell(col.field, row.entity[col.field],col.displayName,$event)">{{row.entity[col.field]}}</div></div>';
        var curUser = $cookieStore.get('currentUser');
        sc.editBtnVis = true;

        // изменение даты отображения приходда топлива
        sc.dateChange = function () {
            if (sc.dateIncoming) {
                $cookieStore.put('dateCalendar', {date: sc.dateIncoming});
                getDayIncoming();
            } else {
                console.log('wrong date from datepick')
            }
        };

        if ($cookieStore.get('dateCalendar')) {
            if ($cookieStore.get('dateCalendar').date) sc.dateIncoming = $cookieStore.get('dateCalendar').date;
        }

        var getDayIncoming = function () {
            if (!sc.infoData) {
                requests.serverCall.get($http, "ActsSzrCtrl/GetInfoForCreateIncomingFuel", function (data, status, headers, config) {
                    console.info(data);
                    sc.infoData = data;
                }, function () {
                });
            }
            requests.serverCall.post($http, "ActsSzrCtrl/GetIncomeFuelByDate", sc.dateIncoming, function (data, status, headers, config) {
                console.info(data);
                sc.selectedRow = [];
                sc.tableData = data;
            }, function () {
            });
        };

        sc.tableData = [];
        sc.selectedRow = [];

        sc.columnDefinitions = [
            {
                field: "Fuel", displayName: "Тип топлива", width: "20%", cellClass: 'cellToolTip',
                cellTemplate: '<a tooltip="{{row.entity[col.field][\'Name\']}}" tooltip-popup-delay="1000" tooltip-class="my-custom-tooltip" tooltip-placement="top" tooltip-append-to-body="true">' +
                '<div class="ngCellText" ng-class="col.colIndex()" ng-click="editCell(row.entity, row.getProperty(col.field), col.field)"">' +
                '<input value="{{row.entity[col.field][\'Name\']}}" placeholder="Тип топлива" style="border: none;"  data-ng-click="updateSelectedCell(col.field, row.entity[col.field][\'Id\'],\'Тип топлива\',row.entity,$event)"  readonly>' +
                '</a>'
            },
            {
                field: "Count", displayName: "Количество", width: "12%",
                cellTemplate: '<a tooltip="{{row.entity[col.field][\'Name\']}}" tooltip-popup-delay="1000" tooltip-class="my-custom-tooltip" tooltip-placement="top" tooltip-append-to-body="true">' +
                '<div class="ngCellText" ng-class="col.colIndex()">' +
                '<input value="{{row.entity[col.field]}}" ng-model="row.entity[col.field]" placeholder="Количество" ng-class="{bordernone: row.entity[\'Id\']}" ng-readonly="row.entity[\'Id\']" ng-change="changeCountPrice(row.entity)">' +
                '</div>' +
                '</a>'
            },
            {
                field: "Type_salary_doc", displayName: "Ед.", width: "10%",
                cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()">' +
                '<input value="{{row.entity[col.field][\'Name\']}}" placeholder="Единицы измерения" style="border: none;" readonly>' +
                '</div>'
            },
            {
                field: "Price", displayName: "Цена", width: "12%",
                cellTemplate: '<a tooltip="{{row.entity[col.field][\'Name\']}}" tooltip-popup-delay="1000" tooltip-class="my-custom-tooltip" tooltip-placement="top" tooltip-append-to-body="true">' +
                '<div class="ngCellText" ng-class="col.colIndex()">' +
                '<input value="{{row.entity[col.field]}}" ng-model="row.entity[col.field]" placeholder="Цена" ng-class="{bordernone: row.entity[\'Id\']}" ng-readonly="row.entity[\'Id\']" ng-change="changeCountPrice(row.entity)">' +
                '</div>' +
                '</a>'
            },
            {
                field: "Cost", displayName: "Стоимость", width: "12%",
                cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()">' +
                '<input value="{{row.entity[col.field]}}" placeholder="Стоимость" style="border: none;" readonly>' +
                '</div>'
            },
            {
                field: "Contractor", displayName: "Контрагент", width: "12%",
                cellTemplate: '<a tooltip="{{row.entity[col.field][\'Name\']}}" tooltip-popup-delay="1000" tooltip-class="my-custom-tooltip" tooltip-placement="top" tooltip-append-to-body="true">' +
                '<div class="ngCellText" ng-class="col.colIndex()" ng-click="editCell(row.entity, row.getProperty(col.field), col.field)">' +
                '<input value="{{row.entity[col.field][\'Name\']}}" placeholder="Контрагент" style="border: none;"  data-ng-click="updateSelectedCell(col.field, row.entity[col.field][\'Id\'],\'Контрагент\',row.entity,$event)"  readonly>' +
                '</div>'
            },
            {
                field: "VAT", displayName: "НДС", width: "7%",
                cellTemplate: '<a tooltip="{{row.entity[col.field][\'Name\']}}" tooltip-popup-delay="1000" tooltip-class="my-custom-tooltip" tooltip-placement="top" tooltip-append-to-body="true">' +
                '<div class="ngCellText" ng-class="col.colIndex()" ng-click="editCell(row.entity, row.getProperty(col.field), col.field)">' +
                '<input value="{{row.entity[col.field][\'Name\']}}" placeholder="НДС" style="border: none;"  data-ng-click="updateSelectedCell(col.field, row.entity[col.field][\'Id\'],\'Значение НДС\',row.entity,$event)"  readonly>' +
                '</div>'
            },
            {
                field: "Comment", displayName: "Комментарий", width: "10%",
                cellTemplate: 'templates/comment_shift_template.html', cellClass: 'cellToolTip'
            },
            {
                field: "Delete", displayName: " ", width: "5%",
                cellTemplate: 'templates/delete_row.html', cellClass: 'cellToolTip'
            },
            {field: "Id", width: "0%"}
        ];

        // пока так, потом придумаю как убрать копипаст
        sc.gridFirstOptions = {
            data: 'tableData',
            enableColumnResize: true,
            enableRowSelection: false,
            showFooter: false,
            rowHeight: 49,
            headerRowHeight: 49,
            selectedItems: sc.selectedRow,
            columnDefs: sc.columnDefinitions,
            useExternalSorting: true,
            afterSelectionChange: function (row) {
                if (row.entity) {
                    //console.info("afterSelectionChange row.entity  " + JSON.stringify(row.entity));
                    if (row.selected) {
                        sc.selectedRow = [];
                        sc.selectedRow.push(row.entity);
                    } else {
                        sc.selectedRow.splice(sc.tableDataFirst.indexOf(row.entity), 1);
                    }
                    //console.info("afterSelectionChange sc.selectedRow  " + JSON.stringify(sc.selectedRow));
                    if (row.entity.Status == '3' || row.entity.Status == '' || row.entity.Status == '2' || row.entity.Status == '0') sc.disCreateWayBill = true;
                    else sc.disCreateWayBill = false;
                }
            },
            beforeSelectionChange: function (row, event) {
                if (!sc.multi) {
                    //console.info("beforeSelectionChange multi " + sc.multi);
                    angular.forEach(sc.tableDataFirst, function (data, index) {
                        if (data.selected == true) sc.selectedRow.splice(index, 1);
                        sc.gridFirstOptions.selectRow(index, false);

                    });
                    //console.info("beforeSelectionChange sc.selectedRow " + JSON.stringify(sc.selectedRow));
                }
                return true;
            }
        };

        sc.createTask = function () {
            console.info(sc.dateIncoming);
            var newIncome = {
                Fuel: null,
                Count: 0,
                Price: 0,
                Cost: 0,
                Date: sc.dateIncoming,
                Contractor: null,
                VAT: null,
                Type_salary_doc: null,
                Comment: ''
            };
            sc.tableData.push(newIncome);
            setTimeout(function() {
                var grid = sc.gridFirstOptions.ngGrid;
                grid.$viewport.scrollTop(sc.tableData.length * grid.config.rowHeight + 50);
            }, 100);
        };

        sc.changeCountPrice = function(row) {
            if (row.Price === null) {
                row.Price = 0;
            }
            if (row.Count === null) {
                row.Count = 0;
            }
            row.Cost = parseFloat(row.Price) * parseFloat(row.Count);
        };

        // вызывается при редактировании ячейки
        sc.editCell = function (row, cell, column) {
            if (!row.Id) {
                sc.selectedCell = cell;
                sc.selectedColumn = column;
                if (column == 'Comment') commentClick(cell);
            }
        };

        function commentClick(comment) {
            //console.info(comment);
            if (curUser.roleid == '2' || curUser.roleid == '10') {
                modals.showWindow(modalType.WIN_COMMENT, {
                    nameField: "Комментарий",
                    comment: comment
                })
                    .result.then(function (result) {
                        console.info(result);
                        sc.selectedRow[0]['Comment'] = result;
                        //TODO sendTask();
                    }, function () {
                        console.info('Modal dismissed at: ' + new Date());
                    });
            }
        }

        sc.updateSelectedCell = function (colField, selectedVal, displayName, row) {
            if (!row.Id) {
                if (!sc.multi) {
                    sc.selectedRow = [];
                    sc.selectedRow.push(row);
                }
                var typeModal = modalType.WIN_SELECT;
                var modalValues;
                if (sc.infoData) {
                    switch (colField) {
                        case "Fuel":
                            modalValues = sc.infoData.Fuels;
                            break;
                        case "Contractor":
                            modalValues = sc.infoData.Contractors;
                            break;
                        case "VAT":
                            modalValues = sc.infoData.VATs;
                            break;
                    }

                    modals.showWindow(typeModal, {
                        nameField: displayName,
                        values: modalValues,
                        selectedValue: selectedVal
                    })
                        .result.then(function (result) {
                            console.info(result);
                            console.info(sc.selectedColumn);
                            sc.selectedRow[0][sc.selectedColumn] = result;
                            if (colField === "Fuel") {
                                sc.selectedRow[0].Type_salary_doc = result.type_salary_doc
                            }
                        }, function () {
                            console.info('Modal dismissed at: ' + new Date());
                        });
                } else {
                    console.log('не пришла sc.infoData');
                }
            }
        };

        sc.delRow = function(row) {
            console.log(sc.tableData.indexOf(row));
            console.log(row);
            sc.tableData.splice(sc.tableData.indexOf(row), 1);
        };

        sc.postIncome = function() {
            var rowsToSend = [];
            var errors = 0;
            for (var i= 0, li = sc.tableData.length; i<li; i++) {
                var r = sc.tableData[i];
                if (!r.Id) {
                    if (r.Fuel && r.Price && r.Cost) {
                        var rToSend = r;
                        rToSend.Date = sc.dateIncoming;
                        rowsToSend.push(rToSend);
                    } else {
                        errors++;
                    }
                }
            }
            if (errors === 0) {
                console.info(rowsToSend);
                requests.serverCall.post($http, "ActsSzrCtrl/PostIncomeFuel", rowsToSend, function (data, status, headers, config) {
                    console.info(data);
                    getDayIncoming();
                }, function () {
                });
            } else {
                modals.showWindow(modalType.WIN_ERROR, {
                    title: "Ошибка накладной",
                    message: "Заполните новые поля или удалите незаполненные строки",
                    callback: function () {
                    }
                });
            }
        };



}]);