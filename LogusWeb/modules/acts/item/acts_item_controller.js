
var app = angular.module("ActsItem", ["ngGrid", "services", 'ngCookies', "modalWindows", "LogusConfig","ui.bootstrap",'Directives',"ui.utils"]);
app.controller("ActsItemCtrl", ["$scope", "$http", "requests", "$location", '$cookieStore', '$rootScope', "modals", "modalType", '$stateParams', 'preloader', "LogusConfig", function ($scope, $http, requests, $location, $cookieStore, $rootScope, modals, modalType, $stateParams, preloader, LogusConfig) {
    var sc = $scope;
    var curUser = $cookieStore.get('currentUser');
    $rootScope.userName = curUser.username;
    $rootScope.exit = 'выход';
    sc.mySelections = [];
	sc.mySelections1 = [];
	sc.mySelections2 = [];
    sc.tableData = [];
    sc.formData = {};
    sc.infoData = {};
	sc.OrganizationsList = [];
	 var countName=0;
    sc.balanceList = {};
    var rowsForUpd = [];
	var rowsForSave = [];
	var rowsForNotSave = [];
	var allrows = [];
	var allNumbers = [];
    var checkAllNumbers = [];
	 sc.multi = false; 
	 var firstLoad = false;
	  var count = 1;
	sc.allFields = {};
	var zero = 0;
	var zero1 = 0;
	var met = 1;
	var mark = 0; var tma_id = 0;
    sc.formData.ActId = $stateParams.act_id;
	console.log(sc.formData.ActId);
    //тип операции с item (передается по клику по кнопке в documents_list)
    sc.typeOperation = $stateParams.type;
    var materialId;
	 var metka=0;  var m1=0;var m2=0;var m3=0; countWin=0;count=0;
    var name; 
	//автоматически ставим номер акта
	  if (sc.typeOperation != 'edit') {
	      requests.serverCall.post($http, "ActsSzrCtrl/GetActsLists", {
            PageSize: 10,
            PageNum: 1,
        }, function (data, status, headers, config) {
			if (data.Values.length != 0) {
			sc.formData.Number=++data.Values[0].Number;}
          else {sc.formData.Number = 1;}			
        });
	  }
	  sc.tporgid = curUser.tporgid;
	  
	
			
    // свойства таблицы
    sc.gridOptions = {
    data: 'tableData',
    enableColumnResize: true,
    enableRowSelection: true,
    showFooter: false,
    selectedItems: sc.mySelections,
    rowHeight: 49,
    headerRowHeight: 49,
    
    columnDefs: [
	  {
            field: "Number",
            displayName: "№ п/п",
            width: "10%",
            cellTemplate: '<a tooltip="{{row.entity[col.field][\'Name\']}}" tooltip-popup-delay="1000" tooltip-class="my-custom-tooltip" tooltip-placement="top" tooltip-append-to-body="true">' +
            '<div class="ngCellText" ng-class="col.colIndex()">' +
            '<input value="{{row.entity[col.field]}}" ng-model="row.entity[col.field]" placeholder="№ п/п" class="bordernone" readonly>' +
            '</div>' +'</a>',headerCellTemplate: "<div   ng-class=\"'colt' + col.index\" class=\"ngHeaderText\">{{col.displayName}}</div>"
        },
        {
            field: "Material",displayName: "Материал",width: "15%",
            cellTemplate: '<a tooltip="{{row.entity[col.field][\'Name\']}}" tooltip-popup-delay="1000" tooltip-class="my-custom-tooltip" tooltip-placement="top" tooltip-append-to-body="true">' +
            '<div class="ngCellText" ng-class="col.colIndex()">' +
            '<input value="{{row.entity[col.field][\'Name\']}}" placeholder="Материал" style="border: none;"  data-ng-click="updateSelectedCell(col.field, row.entity[col.field][\'Id\'], \'Материал\',row.entity,$event)" readonly>' +
            '</div>' + '</a>',headerCellTemplate: '<div class="ngHeaderSortColumn {{col.headerClass}}" ng-style="{\'cursor\': col.cursor}" ng-class="{ \'ngSorted\': !noSortVisible }">' +
        '<div ng-click="sorting(); col.sort()"  ng-class="\'colt\' + col.index" class="ngHeaderText" >{{col.displayName}}</div>' +
        '<div class="ngSortButtonDown" ng-show="col.showSortButtonDown()">' +'</div><div class="ngSortButtonUp" ng-show="col.showSortButtonUp()">'
        },
        {
            field: "Plant",displayName: "Культура",width: "15%",
            cellTemplate: '<a tooltip="{{row.entity[col.field][\'Name\']}}" tooltip-popup-delay="1000" tooltip-class="my-custom-tooltip" tooltip-placement="top" tooltip-append-to-body="true">' +
            '<div class="ngCellText" ng-class="col.colIndex()">' +
            '<input value="{{row.entity[col.field][\'Name\']}}"  placeholder="Культура" style="border: none;"  data-ng-click="updateSelectedCell(col.field, row.entity[col.field][\'Id\'], \'Культура\',row.entity,$event)" readonly>' +
            '</div>' +'</a>',headerCellTemplate: "<div   ng-class=\"'colt' + col.index\" class=\"ngHeaderText\">{{col.displayName}}</div>"
        },
        {
            field: "Field", displayName: "Поле", width: "10%",
            cellTemplate: '<a tooltip="{{row.entity[col.field][\'Name\']}}" tooltip-popup-delay="1000" tooltip-class="my-custom-tooltip" tooltip-placement="top" tooltip-append-to-body="true">' +
            '<div class="ngCellText" ng-class="col.colIndex()">' +
            '<input value="{{row.entity[col.field][\'Name\']}}" placeholder="Поле" style="border: none;"  data-ng-click="updateSelectedCell(col.field, row.entity[col.field][\'Id\'], \'Поле\',row.entity,$event)" readonly>' +
            '</div>' +'</a>',headerCellTemplate: "<div   ng-class=\"'colt' + col.index\" class=\"ngHeaderText\">{{col.displayName}}</div>"
        },
        {
            field: "Area", displayName: "Площадь", width: "10%",
            cellTemplate: '<a tooltip="{{row.getProperty(col.field)}}" tooltip-popup-delay="1000" tooltip-class="my-custom-tooltip" tooltip-placement="top" tooltip-append-to-body="true">' +
            '<div class="ngCellText" ng-class="col.colIndex()">' +
            '<input data="{{row.entity[col.field]}}" ng-model="row.entity[col.field]"  ng-class="{bordernone: row.entity[\'Id\']}" ng-change="recountCost(row.entity)">' +
            '</div>' +
            '</a>',headerCellTemplate: "<div   ng-class=\"'colt' + col.index\" class=\"ngHeaderText\">{{col.displayName}}</div>"
        },
		 {
            field: "Count",displayName: "Количество",width: "10%",
           cellTemplate: '<a tooltip="{{row.getProperty(col.field)}}" tooltip-popup-delay="1000" tooltip-class="my-custom-tooltip" tooltip-placement="top" tooltip-append-to-body="true">' +
            '<div class="ngCellText" ng-class="col.colIndex()">' +
            '<input data="{{row.entity[col.field]}}" ng-model="row.entity[col.field]"  ng-class="{bordernone: row.entity[\'Id\']}" ng-change="recountCost(row.entity)">' +
            '</div>' +
            '</a>',headerCellTemplate: "<div   ng-class=\"'colt' + col.index\" class=\"ngHeaderText\">{{col.displayName}}</div>"
        },
        {
            field: "ConsumptionRate", displayName: "Норма расхода", width: "10%",
           cellTemplate: '<a tooltip="{{row.entity[col.field][\'Name\']}}" tooltip-popup-delay="1000" tooltip-class="my-custom-tooltip" tooltip-placement="top" tooltip-append-to-body="true">' +
            '<div class="ngCellText" ng-class="col.colIndex()">' +
            '<input value="{{row.entity[col.field]}}" ng-model="row.entity[col.field]" placeholder="Стоимость" class="bordernone" readonly>' +
            '</div>' +'</a>',headerCellTemplate: "<div   ng-class=\"'colt' + col.index\" class=\"ngHeaderText\">{{col.displayName}}</div>"
        },
		    {
		    field: "Price",
            displayName: "Цена",
            width: "10%",
            cellTemplate: '<a tooltip="{{row.getProperty(col.field)}}" tooltip-popup-delay="1000" tooltip-class="my-custom-tooltip" tooltip-placement="top" tooltip-append-to-body="true">' +
            '<div class="ngCellText" ng-class="col.colIndex()">' +
            '<input data="{{row.entity[col.field]}}" ng-model="row.entity[col.field]" ng-class="{bordernone: row.entity[\'Id\']}" ng-change="recountCost(row.entity)">' +
            '</div>' +'</a>',headerCellTemplate: "<div   ng-class=\"'colt' + col.index\" class=\"ngHeaderText\">{{col.displayName}}</div>" },	     
        {
            field: "Cost",
            displayName: "Сумма",
            width: "10%",
            cellTemplate: '<a tooltip="{{row.entity[col.field][\'Name\']}}" tooltip-popup-delay="1000" tooltip-class="my-custom-tooltip" tooltip-placement="top" tooltip-append-to-body="true">' +
            '<div class="ngCellText" ng-class="col.colIndex()">' +
            '<input value="{{row.entity[col.field]}}" ng-model="row.entity[col.field]" placeholder="Стоимость" class="bordernone" readonly>' +
            '</div>' +'</a>',headerCellTemplate: "<div   ng-class=\"'colt' + col.index\" class=\"ngHeaderText\">{{col.displayName}}</div>"
        },
        {field: "Id", width: "0%"},
    ],
    afterSelectionChange: function (row) {	
	met = 1;
	for (var i = 0; i < row.length; i++ ) 
   { 
     sc.mySelections.push(row[i].entity); 
     console.log(sc.mySelections); 
        } 
		if (sc.mySelections1==row.entity) { console.log('есть');met=2;}
			sc.mySelections1 = row.entity;
	  if (row.entity) {
		  console.log(row.selected);
                if (row.selected) {
                    if (sc.multi) sc.mySelections.push(row.entity);
                    else {
                        sc.mySelections = [];
                        sc.mySelections.push(row.entity);
                    }
                } else {
                    sc.mySelections.splice(sc.tableData.indexOf(row.entity), 1);
                }
            }
	
    },   
    beforeSelectionChange: function (row, event) {
		   if (!sc.multi) {
                angular.forEach(sc.tableData, function (data, index) {
                    if (data.selected == true) sc.mySelections.splice(index, 1);
                    sc.gridOptions.selectRow(index, false);

                });
            }
		
                return true;
            }
    };    
	sc.Cost1 = 0;
    sc.recountCost = function(row) {
    row.Count = row.Count.toString().replace(",", "."); 
    row.Area = row.Area.toString().replace(",", ".");	
	row.Cost = (row.Count * row.Price).toFixed(2);
	row.ConsumptionRate = (row.Count/row.Area).toFixed(2);	
        if (row.Price === null) {
            row.Price = 0;
        }
        if (row.Count === null) {
            row.Count = 0;
        }
		   recountTotalCost();
       if (row.ConsumptionRate==Infinity) {row.ConsumptionRate = 0;}
    };
    var _field, _val;
    // выделение строчки по определенному столбцу
    function selectRowByField(field, val) {
        _field = field;
        _val = val;
        sc.$on('ngGridEventData', sc.gridEvDataListener);
    }

	/*
	sc.checkRes = function () {				
			var Orgid = null;
			if (sc.AllResource != true) {
				Orgid = curUser.orgid;
			} 
        requests.serverCall.post($http, "ActsSzrCtrl/GetDictionaryForActs", {OrgId: Orgid  }, function (data, status, headers, config) {
        sc.infoData = data;
		sc.allFields = data.AllFields;
		    });

			}
			*/
    // мультивыделение
    sc.keyDownGrid = function (e) {
        if (curUser.roleid == '2' || curUser.roleid == '10') {
            if ((e.keyCode === 16 || e.keyCode === 17 || e.keyCode === 18 || e.keyCode === 91) && !sc.multi) {
             //   console.info("__________________ keyDown  " + e.keyCode);
                sc.multi = true;
             //   console.info(sc.multi);
            }
        }
    };
	    sc.keyUpGrid = function (e) {
        // 16 - Shift, 17 - Ctrl, 18 - Alt, 91 - Win key (Start)
        if (curUser.roleid == '2' || curUser.roleid == '10') {
            if (e.keyCode === 16 || e.keyCode === 17 || e.keyCode === 18 || e.keyCode === 91) {
             //   console.info("_________________ keyUp " + e.keyCode);
                sc.multi = false;
             //   console.info(sc.multi);
            }
        }
    };
    // листнер на событие изменение data в таблице
    sc.gridEvDataListener = function () {
        var offCallMeFn = $scope.$on("ngGridEventData", sc.gridEvDataListener);
        offCallMeFn();
        if (_val == "first" && sc.tableData.length > 0) _val = sc.tableData[0][_field];
		console.log(_val);
        sc.tableData.forEach(function (item, i, arr) {
            if (item[_field] == _val) {
                sc.gridOptions.selectRow(i, true);
                if (sc.gridOptions.ngGrid) sc.gridOptions.ngGrid.$viewport.scrollTop(sc.gridOptions.ngGrid.rowMap[i] * sc.gridOptions.ngGrid.config.rowHeight);
            } else {
                sc.gridOptions.selectRow(i, false);
            }
        });
        $(".ngViewport").focus();
    };
  //получение данных баланса
	    requests.serverCall.post($http, "ActsSzrCtrl/GetBalanceMaterialsList", {}, function (data, status, headers, config) {
          console.info(data);
          sc.balancelist = data;
	  });
	  //
	  	sc.changeOrg = function() {
		if (firstLoad == true) {
			 GetDictionaryForActs();
		}
		firstLoad = true;
		
	}
	  
    // получение масива в выпадающие списки
		   var GetDictionaryForActs = function () {  
          requests.serverCall.post($http, "ActsSzrCtrl/GetDictionaryForActs", {OrgId: sc.OrgId }, function (data, status, headers, config) {
          console.info(data);
         sc.infoData = data;
		sc.allFields = data.AllFields;
        console.info(sc.formData);
		    });
		   }
		   
		  if (sc.typeOperation == 'create') {
				requests.serverCall.post($http, "ActsSzrCtrl/GetDictionaryForActs", {OrgId: curUser.orgid }, function (data, status, headers, config) {
                console.info(data);
                sc.infoData = data;
                sc.OrganzationsList = data.OrganzationsList;
		  if (curUser.tporgid == 2) {
				sc.OrgId = -1;
			}  else {
			sc.OrgId = curUser.orgid;	
			}
		    sc.allFields = data.AllFields;
		 });
	   }
		
        if (sc.typeOperation != 'create') {
			
			requests.serverCall.post($http, "ActsSzrCtrl/GetDictionaryForActs", {OrgId: curUser.orgid }, function (data, status, headers, config) {
         console.info(data);
         sc.infoData = data;
          sc.OrganzationsList = data.OrganzationsList;
		if (curUser.tporgid == 2) {
				sc.OrgId = -1;
			}  else {
			sc.OrgId = curUser.orgid;	
			}
		sc.allFields = data.AllFields;
			//
            requests.serverCall.post($http, "ActsSzrCtrl/GetActSzrDetails", sc.formData.ActId, function (data, status, headers, config) {
                console.info(data);
                sc.formData = data;
                sc.tableData = data.MaterialsList; 
				sc.formData.ActId = data.Id;
				console.info(sc.formData.ActId);
				sc.AllResource = data.AllResource;
				recountTotalCost();
				recountNumber();
                if (sc.typeOperation == 'copy') {
                    sc.formData.Id = null;
                }
            });
			//
			});
			
        }

	   var recountNumber = function () {
		 for (var i = 0; i < sc.tableData.length; i++) { 
		  {
			sc.tableData[i].Number = i + 1;  
		  }
	   }
	   }
	
	
    //при изменении даты в ячейке
    sc.dateChange = function (row, date) {
        sc.tableData.forEach(function (item, d) {
            if (item == row) item["Date"] = moment(date).format("MM.DD.YYYY");
        })
    };

    // при редатировании ячейки с выпадающим списком
    sc.editDropCell = function (row, column, selectedItem) {
        sc.tableData.forEach(function (item, d) {
            if (item == row) item[column] = selectedItem;
        })
    };

    // при редатировании ячейки с input
    sc.editInputCell = function (row, column, value) {
        sc.tableData.forEach(function (item, d) {
            if (item == row) item[column] = value;
        })
    };
       getNamefromId = function (Id) {	   
		   for (var i=0; i< sc.infoData.EmployeesList1.length; i++) {
			   if (Id == sc.infoData.EmployeesList1[i].Id) {
				return sc.infoData.EmployeesList1[i].Name;
			   }
		   }
    }

    // редактирование ячек с модальными окнами
    sc.updateSelectedCell = function (colField, selectedVal, displayName, row) {
		met=1;
            sc.colField = colField;
            sc.selectedRow = [];
            sc.selectedRow.push(row);
			if (sc.mySelections1==row) {met=2;}
			  console.log(row);
			sc.mySelections1 = row;
			console.log(sc.mySelections1);
            var modalValues;
            if (sc.infoData) {
				//
				if (met==2) {
                switch (sc.colField) {
                    case "Material":
                        modalValues = sc.infoData.MaterialsList;
                        break;
                    case "Field":
                        if (row["Plant"]) {
                            if (row["Plant"]["Id"]) {
                                for (var i = 0, il = sc.infoData.FieldsAndPlants.length; i < il; i++) {
                                    if (sc.infoData.FieldsAndPlants[i].Name == row["Plant"]["Name"]) {
                                        modalValues = sc.infoData.FieldsAndPlants[i].Values;
                                    }
                                }
                            } else modalValues = sc.infoData.ObjectList;
                        }
                        break;
                    case "Plant":
                        modalValues = sc.infoData.FieldsAndPlants;
                        break;
                }
             
				
                modals.showWindow(modalType.WIN_SELECT, {
                    nameField: displayName,
                    values: modalValues,
                    selectedValue: selectedVal
                })
				
			    //	
                    .result.then(function (result) {
                        row[sc.colField] = result;
						//автоматический выбор площади по полю
						  if (sc.colField == 'Field') {
						//проверка max 8 связок
						console.log(sc.mySelections); 
						console.log(sc.tableData);  
							  var mas = [];
						for (var i=0;i<sc.tableData.length;i++) {
						if (sc.tableData[i].Field.Id!=null ) {
							mas.push(sc.tableData[i].Field.Id);	
						}	
						}	
	                    console.log(mas);  		
						mas = unique(mas);
					    console.log(mas);  
					    if (mas.length > 8) {sc.tableData.splice(sc.tableData.indexOf(sc.mySelections[0]), 1);
						modals.showWindow(modalType.WIN_MESSAGE, {
                              title: "Предупреждение",
                              message: "Количество связок Культура-Поле не должно превышать 8. Строка будет удалена",
                              callback: function () {
                              } 
                          });

						}
                       if (row.Plant.Id==36) {
                     for (var i = 0, li = sc.allFields.length; i < li; i++) {
						 if (sc.allFields[i].Name==row.Field.Name) {row.Area=sc.allFields[i].Area_plant;}
					 }						 
					   }	else {						
					   row.Area = result.Area_plant;
					   if (parseInt(result.Area_plant)==0) {
					   modals.showWindow(modalType.WIN_MESSAGE, {
                              title: "Предупреждение",
                              message: "В севообороте отсутствует информация о площади на поле "+row.Field.Name+ " по культуре "+row.Plant.Name+
							  ". Сохранение с нулевым значением будет невозможно.",
                              callback: function () {
                              }  
                          });
					   }
					   }
						  }
						//подтягивание цены с баланса
						   if (sc.colField == 'Material') {
                            metka = 0;								   
						    materialId = result.Id;
							
						for (var i = 0, li = sc.balancelist.length; i < li; i++) {
						if (materialId == sc.balancelist[i].IdMaterial && sc.formData.ResponsibleId==sc.balancelist[i].IdEmployees)
						{row.Price = sc.balancelist[i].Current_price; metka=1;  break;} 	
						} 

						}
						if (sc.colField == 'Field') {	
						sc.recountCost(row);
						}
						   if (sc.colField == 'Plant') {
                            row['Field'] = {Id: null, Name: null};
							sc.recountCost(row);
                        } else if (sc.colField == 'Material') {
							 
                            console.info(sc.infoData.MaterialsList);
                            for (var i = 0, li = sc.infoData.MaterialsList.length; i < li; i++) {
                                if (sc.infoData.MaterialsList[i].Id === result.Id) {
                                    sc.recountCost(row);
                                }
                            }
                        }
                    }, function () {});
					//
				}
            } else {}
       
    };
	function unique(arr) {
  var obj = {};

  for (var i = 0; i < arr.length; i++) {
    var str = arr[i];
    obj[str] = true; 
  }

  return Object.keys(obj); 
}
    //кнопка "Создать" - создаем строчку в таблице
    sc.createAct = function () {
        var newAct = {
            Material: {Id: null, Name: null},
            Plant: {Id: null, Name: null},
            Field: {Id: null, Name: null},
            Area: 0,
            ConsumptionRate: 0,
            Count: 0,
            Price: 0,
            Cost: 0
        };
        sc.tableData.push(newAct);
		recountNumber();
		 setTimeout(function() {
            var grid = sc.gridOptions.ngGrid;
            grid.$viewport.scrollTop(sc.tableData.length * grid.config.rowHeight + 100);
        }, 100);
    };
	   
	//сохранение строк в БД
	    saveRow = function () {
				zero1=0;
		        requests.serverCall.post($http, "ActsSzrCtrl/CreateUpdateActSzrDetail", rowsForSave, function (data, status, headers, config) { 	
		        console.info(data);  		
				rowsForSave = [];
				sc.formData.ActId = data;	
				sc.formData.Pk = data;
				tma_id = data;
			
				     requests.serverCall.post($http, "ActsSzrCtrl/UpdateGeneralInfo", sc.formData, function (data, status, headers, config) {
		});
			
			      mark = 1;
                          modals.showWindow(modalType.WIN_MESSAGE, {
                              title: "Сохранение документа",
                              message: "Документ успешно сохранен",
                              callback: function () {
             
                              }
							  
                          });
                      });	
					
	    }
    //поиск элемента в массиве
	    find = function (array, value) {
	        for (var i = 0; i < array.length; i++) {
	            if (array[i] == value) return i;
	        }
	        return -1;}
	//функция обновления
	 updateRow = function () {
	  //обновляем данные
	  zero = 0;
	 if (mark!=1) 	{sc.formData.ActId = $stateParams.act_id;} else {sc.formData.ActId = tma_id;}
	     mark =0;
	 console.log(sc.formData.ActId);
	 
	 	  for (var i = 0; i < sc.tableData.length; i++) { 
		  if (sc.tableData[i].Id) {
	        rowsForUpd.push({
                   command: 'Upd',
                   Document: sc.tableData[i],
				   Date: sc.formData.Date,
                   Number: sc.formData.Number, 
                   Year: sc.formData.Year,
                   SoilTypeId: sc.formData.SoilTypeId,
                   RecipientId: sc.formData.RecipientId,
                   ResponsibleId: sc.formData.ResponsibleId,
                });	
		  }
		  }
	 
	  console.log('upd');
          requests.serverCall.post($http, "ActsSzrCtrl/UpdateRow", rowsForUpd, function (data, status, headers, config) {
          rowsForUpd = [];
		  
	     requests.serverCall.post($http, "ActsSzrCtrl/UpdateGeneralInfo", sc.formData, function (data, status, headers, config) {
		});

                   }, function() {
					}); 	 
	 }
	
   //кнопка "Сохранить" - сохраняем таблицу
    sc.saveAct = function () {
			          var metk = 0;  zero1 = 0;  zero = 0;
					  for (var i = 0; i < sc.tableData.length; i++) { 
					  if (parseInt(sc.tableData[i].Area) == 0) {
						 metk = 1;
				modals.showWindow(modalType.WIN_MESSAGE, {
                              title: "Предупреждение",
                              message: "Площадь в акте списания имеет нулевое значение. Сохранение невозможно.",
                              callback: function () {
                              }  
                          });
						 break;
					  }
					  }
					   console.log(sc.tableData.length);
                       rowsForUpd = [];
			  for (var i = 0; i < sc.tableData.length; i++) { 
		          if (sc.tableData[i].Id) {
	               rowsForUpd.push({
                   command: 'Upd',
                   Document: sc.tableData[i],
				   Date: sc.formData.Date,
                   Number: sc.formData.Number, 
                   Year: sc.formData.Year,
                   SoilTypeId: sc.formData.SoilTypeId,
                   RecipientId: sc.formData.RecipientId,
                   ResponsibleId: sc.formData.ResponsibleId,
                });	
		  }
		  }
		
		      //   console.log(rowsForUpd);
                 if (rowsForUpd.length != 0) {
				  for (var i = 0; i < rowsForUpd.length; i++) { 	   
				  if (rowsForUpd[i].Document.Cost == 0 && metk != 1) {
					  zero = 1;
					 //сообщение если цена 0
                  modals.showWindow(modalType.WIN_DIALOG, {
                    title: "Предупреждение", message: "Цена при обновлении данных равна 0. Сохранить?", callback: function () {
						if (metk != 1) {
                            updateRow(); //если да
						}}});					 
					  //
				 break; }
				  }
				   	
                if (zero!=1 && metk!=1) {updateRow(); zero = 0; }
				 
				   ;}		   
		//
		 rowsForSave = [];
        var sendedData = getRowsForCreate();
        console.info(sendedData);
        sc.formData.MaterialsList = sendedData;
        sc.formData.Income = false;
	   m2=0;
	   if (sc.formData.Id != undefined) {
		  for (var i = 0; i < sc.formData.MaterialsList.length; i++) { 
		sc.formData.MaterialsList[i].Pk = -1;  		
		  }
	   }
	    for (var i = 0, li = sc.formData.MaterialsList.length; i < li; i++) {
			    rowsForSave.push({
                      command: 'Save',
                      Document: sc.formData.MaterialsList[i],
                      Date: sc.formData.Date,
                      Number: sc.formData.Number,
                      Name: sc.formData.Name,
                      OrganizationId: curUser.orgid,
                      Pk: sc.formData.ActId,
                      Year: sc.formData.Year,
                      SoilTypeId: sc.formData.SoilTypeId,
                      RecipientId: sc.formData.RecipientId,
                      ResponsibleId: sc.formData.ResponsibleId
                      }); 	
		
		}
			
	     m3=0;

		if (m3==0 && rowsForSave.length!=0)  {
			//
			 for (var i = 0; i < rowsForSave.length; i++) { 	   
				  if (rowsForSave[i].Document.Cost == 0 && metk != 1) {
					  zero1 = 1;
					 //сообщение если цена 0
                  modals.showWindow(modalType.WIN_DIALOG, {
                    title: "Предупреждение", message: "Цена при сохранении данных равна 0. Сохранить?", callback: function () {
                    saveRow(); //если да 
                    }
                }
            );					 
					  //
				 break; }
				  }   
                if (zero1 != 1 && metk != 1) {saveRow(); zero1 = 0;}
			}
	   //
	   
        //сверяем количество с баланса
		allNumbers = [];
        checkAllNumbers = [];
    };

    //кнопка "Удалить" - удаляем строчку в таблице
    sc.deleteWork = function () {
        sc.formData.MaterialsList = sc.tableData;
		console.log(sc.mySelections);
        if (sc.mySelections.length!=0) {
            modals.showWindow(modalType.WIN_DIALOG, {
                    title: "Удаление", message: "Удалить выбранный документ?", callback: function () {
						for (var i=0; i<sc.mySelections.length;i++) {
                             sc.tableData.splice(sc.tableData.indexOf(sc.mySelections[i]), 1);
						}
						recountNumber();
						for (var j = 0; j<sc.mySelections.length;j++) {
                       if (sc.mySelections[j].Id) {sc.mySelections2.push(sc.mySelections[j]);}
						}		
                requests.serverCall.post($http, "ActsSzrCtrl/DeleteTransaction", sc.mySelections2, function (data, status, headers, config) {
		        console.info(data);
				sc.mySelections2 = [];
				recountTotalCost();
					});
                    }
                }
            );
        } else {
            modals.showWindow(modalType.WIN_MESSAGE, {
                title: "Удаление строки",
                message: "Для удаления необходимо выбрать строку",
                callback: function () {
                }
            });
        }
		recountTotalCost();
    };
	
  var recountTotalCost = function() {
	   sc.Cost1 = 0;
	for (var i = 0; i < sc.tableData.length; i++ ) 
   { 
	sc.Cost1 = parseFloat(sc.Cost1) + parseFloat(sc.tableData[i].Cost);		
   }
	  $("#cost1").html('Итого: '+ sc.Cost1.toFixed(2));
  }
	
	
    // кнопка Печать
    sc.printBtn = function () {
        if ($stateParams.act_id) sc.formData.ActId = $stateParams.act_id;
        if (sc.formData.ActId) {
            requests.serverCall.post($http, "ActsSzrCtrl/GetPrintActInfo", sc.formData.ActId, function (data, status, headers, config) {
                console.info(data);
                sc.data = data;
                setTimeout(function () {
                    var printContents = document.getElementById('actForm').innerHTML;
                    var popupWin = window.open('', '_blank');
                    if (typeof popupWin != 'undefined') {
                        popupWin.document.write('<html><head></head><body >' + printContents + '</html>');
                        popupWin.print();
                        popupWin.close();
                    }
                }, 1000);
            });
        } else {
            modals.showWindow(modalType.WIN_MESSAGE, {
                title: "Печать",
                message: "Для печати необходимо сохранить данные",
                callback: function () {

                }
            });
        }

    }
	
    var getRowsForCreate = function() {
        var sendedData = [];
        console.info('get for create ');
        console.info(sc.tableData);
		m1=0;
		
		
        for (var i= 0, li = sc.tableData.length; i<li; i++) {
            if (sc.tableData[i].Id == null) {
                sendedData.push(sc.tableData[i]);
            }
        }
        return sendedData;
    };

    var getNew;

    //кнопка Отмена
    sc.cancelClick = function () {
        if (sc.formData.ActId) $cookieStore.put('selectedAct', {id: sc.formData.ActId});
        $location.path('/acts_list/1');
    };

    //кнопка Скопировать
    sc.copyTaskRow = function () {           
         var newAct = {
          Material: {Id:sc.mySelections[0].Material.Id, Name: sc.mySelections[0].Material.Name},
		   Plant:{Id:sc.mySelections[0].Plant.Id, Name: sc.mySelections[0].Plant.Name},
		   Field:{Id:sc.mySelections[0].Field.Id, Name: sc.mySelections[0].Field.Name},
		   Area:sc.mySelections[0].Area,
           Price: sc.mySelections[0].Price,
		   Cost:sc.mySelections[0].Cost,
		   Count:sc.mySelections[0].Count,
		   ConsumptionRate:sc.mySelections[0].ConsumptionRate,	
        };
        sc.tableData.push(newAct); 
		console.log(sc.tableData);
		recountTotalCost();
		recountNumber();
		 setTimeout(function() {
            var grid = sc.gridOptions.ngGrid;
            grid.$viewport.scrollTop(sc.tableData.length * grid.config.rowHeight + 100);
        }, 100);
    };
	 sc.checkNumbers = function () { 
   sc.formData.Number = sc.formData.Number.toString().replace(/[^0-9]/gim,'');
	 }
		//сортировка
    sc.sorting = function () {
        var names = [];
        var fullData1 = [];
		filteredData1 = [];
		   for (var i = 0, li = sc.tableData.length; i < li; i++) {
                fullData1.push(sc.tableData[i]);
            }
            for (var i = 0, li = fullData1.length; i < li; i++) {
                names.push(fullData1[i].Material.Name);
            }
            names.sort();
            if (countName % 2 == 0) { names.reverse(); }
            for (var i = 0, li = fullData1.length; i < li; i++) {
                for (var j = 0, l1 = fullData1.length; j < l1; j++) {
            if (names[i] == fullData1[j].Material.Name) {filteredData1.push( fullData1[j]);fullData1.splice(j,1); break; }
                }
            }
            sc.tableData = filteredData1;
            countName++;
			recountNumber();
    }

}
]);