var app = angular.module("IncomingList", ["ngGrid", "services", 'ngCookies', "modalWindows"]);
app.controller("IncomingListCtrl", ["$scope", "$http", "requests", "$location", '$cookieStore', '$rootScope', "modals", "modalType",'$stateParams',
    function ($scope, $http, requests, $location, $cookieStore, $rootScope, modals, modalType, $stateParams) {
    var sc = $scope;
    var curUser = $cookieStore.get('currentUser');
    $rootScope.userName = curUser.username;
    $rootScope.exit = 'выход';
    sc.mySelections = [];
	sc.mySelections1 = [];
	sc.mySelections2 = [];
    sc.tableData = [];
    sc.formData = {};
    sc.infoData = {};
	var firstLoad = false;
    var rowsForUpd = [];
	var rowsForSave = [];
	sc.multi = false;
	var count = 1;
	var met = 1;
    sc.formData.ActId = $cookieStore.get('act_id');
	console.log('id redact - ' + sc.formData.ActId);
    //тип операции с item (передается по клику по кнопке в documents_list)
    sc.typeOperation = $cookieStore.get('typeoper');
		console.log(sc.typeOperation);
		
	if (sc.typeOperation=='create') {	sc.formData.Torg12 = true; }
	 var metka=0;  var m1=0;var m2=0;var m3=0; countWin=0;count=0;
    var name; 
    // свойства таблицы
    sc.gridOptions = {
    data: 'tableData',
    enableColumnResize: true,
    enableRowSelection: true,
    showFooter: false,
    selectedItems: sc.mySelections,
    rowHeight: 49,
    headerRowHeight: 49,
	virtualizationThreshold: 500,
    
   columnDefs: [
    		{field: "Number", displayName: "№ п/п", width: "7%",
            cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()">' +
            '<input value="{{row.entity[col.field]}}" ng-model="row.entity[col.field]" placeholder="№ п/п" class="bordernone" readonly>' +
            '</div>',headerCellTemplate: "<div   ng-class=\"'colt' + col.index\" class=\"ngHeaderText\">{{col.displayName}}</div>"
            },
			{field: "SpareParts", displayName: "Наименование", width: "25%", cellTemplate: '<a tooltip="{{row.entity[col.field][\'Name\']}}" tooltip-popup-delay="1000" tooltip-class="my-custom-tooltip" tooltip-placement="top" tooltip-append-to-body="true">' +
            '<div class="ngCellText" ng-class="col.colIndex()">' +
            '<input value="{{row.entity[col.field][\'Name\']}}" placeholder="Наименование" style="border: none;"  data-ng-click="updateSelectedCell(col.field, row.entity[col.field][\'Id\'], \'Запчасть\',row.entity,$event)" readonly>' +
            '</div>' +
            '</a>', headerCellTemplate: '<div class="ngHeaderSortColumn {{col.headerClass}}" ng-style="{\'cursor\': col.cursor}" ng-class="{ \'ngSorted\': !noSortVisible }">' +
            '<div ng-click="sorting(); col.sort()"  ng-class="\'colt\' + col.index" class="ngHeaderText" >{{col.displayName}}</div>' +
			'<div class="ngSortButtonDown" ng-show="col.showSortButtonDown()">' +'</div><div class="ngSortButtonUp" ng-show="col.showSortButtonUp()">'},
			
			{field: "Storage", displayName: "Ответственный", width: "15%", cellTemplate: '<a tooltip="{{row.entity[col.field][\'Name\']}}" tooltip-popup-delay="1000" tooltip-class="my-custom-tooltip" tooltip-placement="top" tooltip-append-to-body="true">' +
            '<div class="ngCellText" ng-class="col.colIndex()">' +
            '<input value="{{row.entity[col.field][\'Name\']}}" placeholder="Ответственный" style="border: none;"  data-ng-click="updateSelectedCell(col.field, row.entity[col.field][\'Id\'], \'Склад\',row.entity,$event)" readonly>' +
            '</div>' +
            '</a>', headerCellTemplate: "<div   ng-class=\"'colt' + col.index\" class=\"ngHeaderText\">{{col.displayName}}</div>"},
			
		    {field: "Count", displayName: "Количество", width: "10%",
            cellTemplate: '<a tooltip="{{row.entity[col.field][\'Name\']}}" tooltip-popup-delay="1000" tooltip-class="my-custom-tooltip" tooltip-placement="top" tooltip-append-to-body="true">' +
            '<div class="ngCellText" ng-class="col.colIndex()">' +
            '<input value="{{row.entity[col.field]}}" ng-model="row.entity[col.field]" placeholder="0" ng-class="{bordernone: row.entity[\'Id\']}"  ng-change="changeCountPrice(row.entity)">' +
            '</div>' + '</a>',  headerCellTemplate: "<div   ng-class=\"'colt' + col.index\" class=\"ngHeaderText\">{{col.displayName}}</div>"
            },
            {field: "Price", displayName: "Цена", width: "10%",
			 cellTemplate: '<a tooltip="{{row.entity[col.field][\'Name\']}}" tooltip-popup-delay="1000" tooltip-class="my-custom-tooltip" tooltip-placement="top" tooltip-append-to-body="true">' +
             '<div class="ngCellText" ng-class="col.colIndex()">' +
             '<input value="{{row.entity[col.field]}}" ng-model="row.entity[col.field]" placeholder="0" ng-class="{bordernone: row.entity[\'Id\']}"   ng-change="changeCountPrice(row.entity)">' +
             '</div>' +'</a>',  headerCellTemplate: "<div   ng-class=\"'colt' + col.index\" class=\"ngHeaderText\">{{col.displayName}}</div>"},
				
             {field: "Cost1", displayName: "Сумма без НДС", width: "10%",
          cellTemplate: '<a tooltip="{{row.entity[col.field][\'Name\']}}" tooltip-popup-delay="1000" tooltip-class="my-custom-tooltip" tooltip-placement="top" tooltip-append-to-body="true">' +
             '<div class="ngCellText" ng-class="col.colIndex()">' +
             '<input value="{{row.entity[col.field]}}" ng-model="row.entity[col.field]" placeholder="0" ng-class="{bordernone: row.entity[\'Id\']}"   ng-change="clickCost1(row.entity)">' +
             '</div>' +'</a>',  headerCellTemplate: "<div   ng-class=\"'colt' + col.index\" class=\"ngHeaderText\">{{col.displayName}}</div>"},
			 
			  {field: "Cost2", displayName: "НДС", width: "8%",
            cellTemplate: '<a tooltip="{{row.entity[col.field][\'Name\']}}" tooltip-popup-delay="1000" tooltip-class="my-custom-tooltip" tooltip-placement="top" tooltip-append-to-body="true">' +
            '<div class="ngCellText" ng-class="col.colIndex()">' +
            '<input value="{{row.entity[col.field]}}" ng-model="row.entity[col.field]" placeholder="0" ng-class="{bordernone: row.entity[\'Id\']}" ng-change="changeCountPrice(row.entity)">' +
            '</div>' + '</a>',  headerCellTemplate: "<div   ng-class=\"'colt' + col.index\" class=\"ngHeaderText\">{{col.displayName}}</div>"
            },
			 
			   {field: "Cost3", displayName: "Всего с НДС", width: "15%",
             cellTemplate: '<a tooltip="{{row.entity[col.field][\'Name\']}}" tooltip-popup-delay="1000" tooltip-class="my-custom-tooltip" tooltip-placement="top" tooltip-append-to-body="true">' +
             '<div class="ngCellText" ng-class="col.colIndex()">' +
             '<input value="{{row.entity[col.field]}}" ng-model="row.entity[col.field]" placeholder="0" ng-class="{bordernone: row.entity[\'Id\']}"   ng-change="clickCost3(row.entity)">' +
             '</div>' +'</a>',  headerCellTemplate: "<div   ng-class=\"'colt' + col.index\" class=\"ngHeaderText\">{{col.displayName}}</div>"},
			 {field: "Id", width: "0%"},
    ],
    afterSelectionChange: function (row) {	
	met = 1;
	for (var i=0; i<row.length;i++ ) 
   { 
     sc.mySelections.push(row[i].entity); 
     console.log(sc.mySelections); 
        } 
		if (sc.mySelections1==row.entity) { console.log('есть');met=2;}
			sc.mySelections1 = row.entity;
	  if (row.entity) {
		  console.log(row.selected);
                if (row.selected) {
                    if (sc.multi) sc.mySelections.push(row.entity);
                    else {
                        sc.mySelections = [];
                        sc.mySelections.push(row.entity);
                    }
                } else {
                    sc.mySelections.splice(sc.tableData.indexOf(row.entity), 1);
                }
            }
	
    },   
    beforeSelectionChange: function (row, event) {
		   if (!sc.multi) {
                angular.forEach(sc.tableData, function (data, index) {
                    if (data.selected == true) sc.mySelections.splice(index, 1);
                    sc.gridOptions.selectRow(index, false);
                });
            }
                return true;
            }
    };    
	//сумма без ндс
	  sc.clickCost1 = function(row) {
		  console.log('row');
		  row.Cost1 = row.Cost1.toString().replace(",", "."); 	
		 if (row.Price === null) {
            row.Price = 0;
        }
        if (row.Count === null) {
            row.Count = 0;
        }
		  console.log(row.Price);
      row.Price = (row.Cost1 / row.Count).toFixed(2);
	  console.log(row.Price);
	  row.Cost3 = (parseFloat(row.Cost1) + parseFloat(row.Cost2/100*row.Cost1)).toFixed(2);
	  
	  if (isNaN(row.Cost3)) {row.Cost3 = 0;}
	  
	  }
	  //сумма с ндс
	    sc.clickCost3 = function(row) {
		row.Cost3 = row.Cost3.toString().replace(",", "."); 	
		 if (row.Price === null) {
            row.Price = 0;
        }
        if (row.Count === null) {
            row.Count = 0;
        }
		  if (row.Cost2 === null) {
            row.Cost2 = 0;
        }
       row.Cost1 = (row.Cost3 - ((row.Cost3 * row.Cost2)/(100 + row.Cost2))).toFixed(2);
	   if (row.Count != 0) {
	     row.Price = (parseFloat(row.Cost1) / row.Count).toFixed(2);
	   } else {
		  row.Price = 0; 
	   }
	   
	    if (isNaN(row.Cost1)) {row.Cost1 = 0;}
	   
		}
	  
	  
    sc.changeCountPrice = function(row) {
		console.log(row);	
		if (row.Count != undefined ) {
	row.Count = row.Count.toString().replace(",", "."); 
		}	
			if (row.Price != undefined ) {
	row.Price = row.Price.toString().replace(",", "."); 
			}
				if (row.Cost2 != undefined ) {
	row.Cost2 = row.Cost2.toString().replace(",", ".");
				}
	 if (row.Price === null) {
            row.Price = 0;
        }
        if (row.Count === null) {
            row.Count = 0;
        }	 
	row.Cost1 = (row.Count * row.Price).toFixed(2);
	row.Cost3 = (parseFloat(row.Cost1) + parseFloat(row.Cost2/100*row.Cost1)).toFixed(2);
	sc.Cost1 = 0; sc.Cost2 = 0; sc.Cost3 = 0;
	for (var i = 0; i < sc.tableData.length; i++ ) 
   { 
	sc.Cost1 = parseFloat(sc.Cost1) + parseFloat(sc.tableData[i].Cost1);
	sc.Cost2 = parseFloat(sc.Cost2)+ parseFloat(sc.tableData[i].Cost2)/100*parseFloat(sc.tableData[i].Cost1);
	sc.Cost3 = parseFloat(sc.Cost3) + parseFloat(sc.tableData[i].Cost3);		
   }
   
   if (isNaN(sc.Cost1)) {sc.Cost1 = 0;}
    if (isNaN(sc.Cost2)) {sc.Cost2 = 0;}
    if (isNaN(sc.Cost3)) {sc.Cost3 = 0;}
    if (isNaN(row.Cost1)) {row.Cost1 = 0;}
	 if (isNaN(row.Cost3)) {row.Cost3 = 0;}
   
    $("#cost1").html('Итого: '+ sc.Cost1.toFixed(2));
	$("#cost2").html('Сумма НДС: '+ sc.Cost2.toFixed(2));
	$("#cost3").html('Итого c НДС: '+ sc.Cost3.toFixed(2));
	
		if (row.Id != undefined || row.Id != null ) {
       rowsForUpd.push({
        Document: row,
	    Date: sc.formData.Date,
        Number: sc.formData.Number,
        Pk: sc.formData.ActId,
        ContractorsId: sc.formData.ContractorsId,
        StorageId: sc.formData.StorageId,
        Torg12: sc.formData.Torg12,
		TypeAct : 1,
                });	
		}
    };

    var _field, _val;
    // выделение строчки по определенному столбцу
    function selectRowByField(field, val) {
        _field = field;
        _val = val;
        sc.$on('ngGridEventData', sc.gridEvDataListener);
    }

	  if (curUser.tporgid == 2) {
				sc.OrgId = -1;
			}  else {
			sc.OrgId = curUser.orgid;	
			}
	
	
	   	sc.changeOrg = function() {
		if (firstLoad == true) {
				 requests.serverCall.post($http, "ActsSzrCtrl/GetDictionaryTmc", {OrgId: sc.OrgId  }, function (data, status, headers, config) {
        console.info(data);
        sc.infoData = data;
	//	 sc.OrganzationsList = data.OrganzationsList;
		var AllStore = {Id:-1, Name :""};
		sc.infoData.StorageList.splice(0,0,AllStore);
     //   console.info(sc.formData);
				 });
		}
		firstLoad = true;
		
	    }
	
    // мультивыделение
    sc.keyDownGrid = function (e) {
        if (curUser.roleid == '2' || curUser.roleid == '10') {
            if ((e.keyCode === 16 || e.keyCode === 17 || e.keyCode === 18 || e.keyCode === 91) && !sc.multi) {
                sc.multi = true;
            }
        }
    };
	    sc.keyUpGrid = function (e) {
        if (curUser.roleid == '2' || curUser.roleid == '10') {
            if (e.keyCode === 16 || e.keyCode === 17 || e.keyCode === 18 || e.keyCode === 91) {
                sc.multi = false;
            }
        }
    };
    // листнер на событие изменение data в таблице
    sc.gridEvDataListener = function () {
        var offCallMeFn = $scope.$on("ngGridEventData", sc.gridEvDataListener);
        offCallMeFn();
        if (_val == "first" && sc.tableData.length > 0) _val = sc.tableData[0][_field];
		console.log(_val);
        sc.tableData.forEach(function (item, i, arr) {
            if (item[_field] == _val) {
                sc.gridOptions.selectRow(i, true);
                if (sc.gridOptions.ngGrid) sc.gridOptions.ngGrid.$viewport.scrollTop(sc.gridOptions.ngGrid.rowMap[i] * sc.gridOptions.ngGrid.config.rowHeight);
            } else {
                sc.gridOptions.selectRow(i, false);
            }
        });
        $(".ngViewport").focus();
    };

	 requests.serverCall.post($http, "ActsSzrCtrl/GetDictionaryTmc", {OrgId: curUser.orgid }, function (data, status, headers, config) {
        console.info(data);
        sc.infoData = data;
		 sc.OrganzationsList = data.OrganzationsList;
		var AllStore = {Id:-1, Name :""};
		sc.infoData.StorageList.splice(0,0,AllStore);
        console.info(sc.formData);
		
        if (sc.typeOperation != 'create') {
        //    requests.serverCall.post($http, "SparePartsCtrl/GetInvoiceDetails", sc.formData.ActId, function (data, status, headers, config) {
		 requests.serverCall.post($http, "ActsSzrCtrl/GetTmcDetails", sc.formData.ActId, function (data, status, headers, config) {	
                console.info(data);
                sc.formData = data;
                sc.tableData = data.SparePartsList;
				
							var count = 1; 
						    sc.Cost1 = 0;
							sc.Cost2 = 0;
							sc.Cost3 = 0;
							for (i = 0; i < sc.tableData.length; i++) {
								sc.tableData[i].Number = count;
								count++; 
								sc.Cost1 = parseFloat(sc.Cost1) + parseFloat(sc.tableData[i].Cost1);
							    sc.Cost3 = parseFloat(sc.Cost3) + parseFloat(sc.tableData[i].Cost3);
								sc.Cost2 = parseFloat(sc.Cost2) + parseFloat(sc.tableData[i].Cost2)/100*parseFloat(sc.tableData[i].Cost1);
							}
			            	console.log(sc.Cost1);
							$("#cost1").html('Итого: '+ sc.Cost1.toFixed(2));
							$("#cost2").html('Сумма НДС: '+ sc.Cost2.toFixed(2));
							$("#cost3").html('Итого c НДС: '+ sc.Cost3.toFixed(2));
					
					if ($cookieStore.get('from_incoming') == 2) { 
					         sc.formData = JSON.parse($.session.get('formData_incoming'));
						     sc.tableData = JSON.parse($.session.get('tableData_incoming'));
					         console.log(JSON.parse($.session.get('tableData_incoming')));
						     sc.Cost1 = 0;
							 sc.Cost2 = 0;
							 sc.Cost3 = 0;
							 var count = 1; 
							for (i = 0; i < sc.tableData.length; i++) {
								sc.tableData[i].Number = count;
								count++; 
								sc.Cost1 = parseFloat(sc.Cost1) + parseFloat(sc.tableData[i].Cost1);
								sc.Cost2 =parseFloat(sc.Cost2)+ parseFloat(sc.tableData[i].Cost2)/100*parseFloat(sc.tableData[i].Cost1);
							   sc.Cost3 = parseFloat(sc.Cost3) + parseFloat(sc.tableData[i].Cost3);
							}
                    $("#cost1").html('Итого: '+ sc.Cost1.toFixed(2));
					$("#cost2").html('Сумма НДС: '+ sc.Cost2.toFixed(2));
					$("#cost3").html('Итого c НДС: '+ sc.Cost3.toFixed(2));							
					}
					$cookieStore.put('from_incoming', null);
            });
        }
		else 
		{
					if ($cookieStore.get('from_incoming') == 2) { 
							sc.formData = JSON.parse($.session.get('formData_incoming'));
							sc.tableData = JSON.parse($.session.get('tableData_incoming'));
							console.log(JSON.parse($.session.get('tableData_incoming')));
							var count = 1;
							for (i = 0; i < sc.tableData.length; i++) {
								sc.tableData[i].Number=count;
								count++; 
							}         
					}
					$cookieStore.put('from_incoming', null);
		}
    });
	

    //при изменении даты в ячейке
    sc.dateChange = function (row, date) {
        sc.tableData.forEach(function (item, d) {
            if (item == row) item["Date"] = moment(date).format("MM.DD.YYYY");
        })
    };

    // при редатировании ячейки с выпадающим списком
    sc.editDropCell = function (row, column, selectedItem) {
        sc.tableData.forEach(function (item, d) {
            if (item == row) item[column] = selectedItem;
        })
    };

    // при редатировании ячейки с input
    sc.editInputCell = function (row, column, value) {
        sc.tableData.forEach(function (item, d) {
            if (item == row) item[column] = value;
        })
    };


    // редактирование ячек с модальными окнами
    sc.updateSelectedCell = function (colField, selectedVal, displayName, row) {
		met = 1;
            sc.colField = colField;
			if (sc.mySelections1==row) {met=2;}
			  console.log(row);
			sc.mySelections1 = row;
			console.log(sc.mySelections1);
            var modalValues;
            if (sc.infoData) {
				if (met==2) {
                switch (sc.colField) {
                   case "SpareParts":
                    modalValues = sc.infoData.SparePartsList;
                    break;
			   case "Storage":
			   sc.infoData.StorageList2 = [];
			 for (var i = 0; i < sc.infoData.StorageList.length; i++) {
			if (i!=0)	sc.infoData.StorageList2.push(sc.infoData.StorageList[i]) ;
			 }
			   
			   
             modalValues = sc.infoData.StorageList2;
			   break;
                }
                modals.showWindow(modalType.WIN_SELECT, {
                    nameField: displayName,
                    values: modalValues,
                    selectedValue: selectedVal
                })
                    .result.then(function (result) {
                        row[sc.colField] = result;
						  switch (colField) {
						case "SpareParts":
                   		sc.changeCountPrice(row);
						 break;
						  case "Storage":
						   sc.changeCountPrice(row);
						  break;
						}
							
                    }, function () {});
				}
            } else {}
       
    };
	function unique(arr) {
      var obj = {};
      for (var i = 0; i < arr.length; i++) {
        var str = arr[i];
        obj[str] = true; 
  }

  return Object.keys(obj);
}

	function getMaxOfArray(numArray) { 
		return Math.max.apply(null, numArray);
	}
    
	//кнопка "Создать" - создаем строчку в таблице
    sc.createAct = function () {
		var c = []; 
		for (i = 0; i < sc.tableData.length; i++) {
			c.push(sc.tableData[i].Number);
		}
		var con=getMaxOfArray(c);
	  		  console.log(con);
		if (con==-Infinity)	 {con=0;} 
        con = parseInt(con)+1; 
		var storage = {Id: null, Name: null};
		var name = null;
		if (sc.formData.StorageId!=-1) {
		for (i = 0; i < sc.infoData.StorageList.length; i++) {
			if (sc.infoData.StorageList[i].Id==sc.formData.StorageId)		{
				name = sc.infoData.StorageList[i].Name; break;
			}
		}
			
		storage = {Id: sc.formData.StorageId, Name: name};
		}
		
        var newAct = {
			Number: con,
            SpareParts: {Id: null, Name: null},
			Storage: storage,
            Cost1: 0,
			Cost2: 20, 
			Cost3: 0,
        };
        sc.tableData.push(newAct); 
		setTimeout(function() {
            var grid = sc.gridOptions.ngGrid;
            grid.$viewport.scrollTop(sc.tableData.length * grid.config.rowHeight + 100);
        }, 100);
    };
	   
	
    //поиск элемента в массиве
	    find = function (array, value) {
	        for (var i = 0; i < array.length; i++) {
	            if (array[i] == value) return i;
	        }
	        return -1;}
			
	//функция обновления
	 updateRow = function () {
	  //обновляем данные
	 console.log(sc.formData.ActId);
	      requests.serverCall.post($http, "ActsSzrCtrl/UpdateTmcAct", rowsForUpd, function (data, status, headers, config) {
          rowsForUpd = [];
		  console.info(sc.formData.ActId); 
		  
		 requests.serverCall.post($http, "ActsSzrCtrl/GetTmcDetails", sc.formData.ActId, function (data, status, headers, config) {	
                console.info(data);
                sc.formData = data;
                sc.tableData = data.SparePartsList;
           });
		    modals.showWindow(modalType.WIN_MESSAGE, {
                              title: "Сохранение документа",
                              message: "Документ успешно сохранен",
                              callback: function () {
             
                              }
							  
                          });
		
                   }, function() {
					}); 	 
	 }
	
   //кнопка "Сохранить" - сохраняем таблицу
    sc.saveAct = function () {	
	$.session.remove('formData_incoming');
	$.session.remove('tableData_incoming');	
	$cookieStore.put('from_incoming', null);
	console.log(sc.formData.tableData);
	 console.log(sc.formData.ActId);
	 	sc.formData.TypeAct=1;
		var mek=0; var mel=0; 
		console.log(sc.formData.Torg12);
		rowsForSave = [];
		if (sc.formData.Torg12 && (!sc.formData.StorageId || !sc.formData.Number || !sc.formData.ContractorsId || sc.tableData.length==0)) {
			mek=1; mel=1; rowsForSave = [];  rowsForUpd = [];
			  modals.showWindow(modalType.WIN_MESSAGE, {
                              title: "Предупреждение",
                              message: "Заполните данные по приходной накладной",
                              callback: function () {
                              }  
                          });
		}
		if (!sc.formData.StorageId && mek!=1) {
			mek=1; mel=1; rowsForSave=[];  rowsForUpd = [];
			  modals.showWindow(modalType.WIN_MESSAGE, {
                              title: "Предупреждение",
                              message: "Заполните данные по приходной накладной",
                              callback: function () {
                              }  
                          });		
		}
		
		if (mek != 1) {
	 //       requests.serverCall.post($http, "SparePartsCtrl/UpdateInvoiceGeneralInfo", sc.formData, function (data, status, headers, config) {
		  requests.serverCall.post($http, "ActsSzrCtrl/UpdateTmcGeneralInfo", sc.formData, function (data, status, headers, config) {
		});
		}
		console.log(sc.tableData);
		//сохранение
		for (var i=0; i<sc.tableData.length;i++) {
			if (mek!=1 && (sc.tableData[i].Id == undefined || sc.tableData[i].Id == null)) {
	 rowsForSave.push({
                      Document: sc.tableData[i],
                      Date: sc.formData.Date,
                      Number: sc.formData.Number,
                      Pk: sc.formData.ActId,
                      ContractorsId: sc.formData.ContractorsId,
                      StorageId: sc.formData.StorageId,
                      Torg12: sc.formData.Torg12,
					  Id : sc.formData.ActId,
					  TypeAct : 1,
					  OrganizationId: curUser.orgid,
                      });
			}			  
		}
		//сохранение только шапки
		if (!sc.formData.Torg12 && sc.tableData.length==0)
		{
			 rowsForSave.push({
                      Date: sc.formData.Date,
                      Number: sc.formData.Number,
                      Pk: sc.formData.ActId,
                      ContractorsId: sc.formData.ContractorsId,
                      StorageId: sc.formData.StorageId,
                      Torg12: sc.formData.Torg12,
					  Id : sc.formData.ActId,
					  TypeAct : 1,
					  OrganizationId: curUser.orgid,
                      });
		}
		//проверка уникальности запчастей
		var checksp=0;
		for (var i=0; i<sc.tableData.length;i++) {
			for (var j=0; j<sc.tableData.length;j++) {
				if (checksp==1) {break;}
			if (i!=j && sc.tableData[i].SpareParts.Id==sc.tableData[j].SpareParts.Id && sc.tableData[i].Price == sc.tableData[j].Price)	{
					mek=1; mel=1; rowsForSave=[];  rowsForUpd = [];
			  modals.showWindow(modalType.WIN_MESSAGE, {
                              title: "Предупреждение",
                              message: "Повторяются записи с одинаковыми материалами и ценой",
                              callback: function () {
                              }  
                          });
						checksp=1;   break;
			}
			
			}
			
		}
		if (mek!=1) {
		for (var i=0; i<sc.tableData.length;i++) {
			if (sc.formData.Torg12  && (sc.tableData[i].Count == 0 || sc.tableData[i].Price == 0))
			{
				mek = 1; mel=1;
				rowsForSave = [];
			    modals.showWindow(modalType.WIN_MESSAGE, {
                              title: "Предупреждение",
                              message: "Заполните новые поля или удалите незаполненные строки",
                              callback: function () {
             
                              }
							  
                          });	
				break;
			}		
		}
		}

	    if (rowsForSave.length!=0 && mek != 1) {
			for (var i = 0; i < rowsForSave.length; i++)
			{
				if (rowsForSave[i].Document.Cost2 == null || rowsForSave[i].Document.Cost2 == "") {rowsForSave[i].Document.Cost2=0;}
				if (rowsForSave[i].Document.Count == null || rowsForSave[i].Document.Count == "") {rowsForSave[i].Document.Count=0;}
				if (rowsForSave[i].Document.Price == null || rowsForSave[i].Document.Price == "") {rowsForSave[i].Document.Price=0;}
			if (rowsForSave[i].Document!=null && ( rowsForSave[i].Document.SpareParts.Id == null || rowsForSave[i].Document.Count == null || rowsForSave[i].Document.Price == null ||
			rowsForSave[i].Document.Cost1 == null || rowsForSave[i].Document.Cost2 == null || rowsForSave[i].Document.Cost3 == null || rowsForSave[i].Document.Storage.Id == null)) {
				mek = 1;
				rowsForSave = [];
			  modals.showWindow(modalType.WIN_MESSAGE, {
                              title: "Предупреждение",
                              message: "Заполните новые поля или удалите незаполненные строки",
                              callback: function () {
             
                              }
							  
                          });	
				break;
			} 	
			}	
			console.log(mek);
			 if (mek != 1) {	
		  requests.serverCall.post($http, "ActsSzrCtrl/CreateUpdateTmcDetail", rowsForSave, function (data, status, headers, config) { 	
		        console.info(data);  		
				rowsForSave = [];
				 sc.formData.ActId = data;	
		      requests.serverCall.post($http, "ActsSzrCtrl/GetTmcDetails", sc.formData.ActId, function (data, status, headers, config) {	
                console.info(data);
                sc.formData = data;
                sc.tableData = data.SparePartsList;
           });
                          modals.showWindow(modalType.WIN_MESSAGE, {
                              title: "Сохранение документа",
                              message: "Документ успешно сохранен",
                              callback: function () {
             
                              }
							  
                          });
                      });
					  
		 }
		}
		 console.info(sc.tableData); 
		 if (mek != 1) {
        for (var i=0; i < sc.tableData.length ; i++)
			{
				if (sc.tableData[i].Storage.Id == null)
				{
					mel = 1;
				rowsForUpd = [];
			  modals.showWindow(modalType.WIN_MESSAGE, {
                              title: "Предупреждение",
                              message: "Заполните новые поля или удалите незаполненные строки",
                              callback: function () {
             
                              }
							  
                          });
					break;
				}	
			}
		 }
			
		      console.log(rowsForUpd);
		 		 if (rowsForUpd.length!=0) {
			for (var i=0; i < rowsForUpd.length ; i++)
			{
				if (rowsForUpd[i].Document.SpareParts.Id == null || rowsForUpd[i].Document.Storage.Id == null || rowsForUpd[i].Document.Count == null 
				|| rowsForUpd[i].Document.Price == null ||
			rowsForUpd[i].Document.Cost1 == null || rowsForUpd[i].Document.Cost2 == null || rowsForUpd[i].Document.Cost3 == null) {
				mel = 1;
				rowsForUpd = [];
			  modals.showWindow(modalType.WIN_MESSAGE, {
                              title: "Предупреждение",
                              message: "Заполните новые поля или удалите незаполненные строки",
                              callback: function () {
             
                              }
							  
                          });	
				break;
			} 
			
			} 
					 if (mel != 1) {
					  updateRow();
					 }
				  }
				  
				  
    };

    //кнопка "Удалить" - удаляем строчку в таблице
    sc.deleteWork = function () {
		
        sc.formData.SparePartsList = sc.tableData;
		console.log(sc.mySelections);
        if (sc.mySelections.length!=0) {
            modals.showWindow(modalType.WIN_DIALOG, {
                    title: "Удаление", message: "Удалить выбранный документ?", callback: function () {
						
						for (var i= 0; i<sc.mySelections.length;i++) {
                     if (!sc.mySelections[i].Id)  {sc.tableData.splice(sc.tableData.indexOf(sc.mySelections[i]), 1); }
						}
						for (var j = 0; j < sc.mySelections.length; j++) {
                       if (sc.mySelections[j].Id) {sc.mySelections2.push(sc.mySelections[j]);}
						}		
								console.log(sc.mySelections2);
								
						if (sc.mySelections2.length!=0) {
					    requests.serverCall.post($http, "ActsSzrCtrl/DeleteTmcAct", sc.mySelections2, function (data, status, headers, config) {
						console.info(data);
					
						if (data=='success') {
							for (var i = 0; i < sc.mySelections.length;i++) {
                     if (sc.mySelections[i].Id)  {sc.tableData.splice(sc.tableData.indexOf(sc.mySelections[i]), 1); }
						}} else {
							
							 modals.showWindow(modalType.WIN_MESSAGE, {
                              title: "Предупреждение",
                              message: data,
                              callback: function () {
                              }  
                          });
							
						}
							
						
						sc.mySelections2 = [];					
							var count=1; 
							for (i = 0; i < sc.tableData.length; i++) {
								sc.tableData[i].Number = count;
								count++; 
							}          
						});
								}
						var count=1; 
							for (i = 0; i < sc.tableData.length; i++) {
								sc.tableData[i].Number = count;
								count++; 
							} 
                    }
                }
            );
        } else {
            modals.showWindow(modalType.WIN_MESSAGE, {
                title: "Удаление строки",
                message: "Для удаления необходимо выбрать строку",
                callback: function () {
                }
            });
        }
		
    };

    // кнопка Печать
   // sc.printBtn = function () {
   // }

    var getRowsForCreate = function() {
        var sendedData = [];
        console.info('get for create ');
        console.info(sc.tableData);
		m1=0;
		
		
        for (var i = 0, li = sc.tableData.length; i<li; i++) {
            if (sc.tableData[i].Id == null) {
                sendedData.push(sc.tableData[i]);
            }
        }
        return sendedData;
    };

    var getNew;

    //кнопка Отмена
    sc.cancelClick = function () {
	//  $cookieStore.put('from_incoming', null);
	//  $.session.remove('formData_incoming');
	//  $.session.remove('tableData_incoming');
	
	//	$cookieStore.put('checkCancelInvoice1', 1);
	//	$cookieStore.put('checkCancelInvoice2', 1);
	//	$cookieStore.put('checkCancelInvoice3', 1);
        $location.path('/coming_tmc'); 
		
    };

    //кнопка Скопировать
    sc.copyTaskRow = function () {           
          var newAct = {
          SpareParts: {Id:sc.mySelections[0].SpareParts.Id, Name: sc.mySelections[0].SpareParts.Name},
		  Storage:{Id:sc.mySelections[0].Storage.Id, Name: sc.mySelections[0].Storage.Name},
          Price: sc.mySelections[0].Price,
		  Cost1:sc.mySelections[0].Cost1,
		  Cost2:sc.mySelections[0].Cost2,
		  Cost3:sc.mySelections[0].Cost3,
		  Count:sc.mySelections[0].Count,	
        };
	   sc.tableData.push(newAct);
		var count=1; 
		for (i = 0; i < sc.tableData.length; i++) {
		sc.tableData[i].Number = count;
		count++; 
		} 
		console.log(sc.tableData);
    };

	//
		//сортировка
    sc.sorting = function () {
        var names = [];
        var fullData1 = [];
		filteredData1 =[];
		   for (var i = 0, li = sc.formData.SparePartsList.length; i < li; i++) {
                fullData1.push(sc.formData.SparePartsList[i]);
            }
            for (var i = 0, li = fullData1.length; i < li; i++) {
                names.push(fullData1[i].SpareParts.Name);
            }
            names.sort();
            if (count % 2 == 0) { names.reverse(); }
            for (var i = 0, li = fullData1.length; i < li; i++) {
                for (var j = 0, l1 = fullData1.length; j < l1; j++) {
            if (names[i] == fullData1[j].SpareParts.Name) {filteredData1.push( fullData1[j]);fullData1.splice(j,1); break; }
                }
            }
            sc.tableData = filteredData1;
            count++;
    }
   
}]);
