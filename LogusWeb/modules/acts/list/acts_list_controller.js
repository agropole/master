var app = angular.module("ActsList", ["ngGrid", "services", 'ngCookies', "modalWindows","LogusConfig"]);
app.controller("ActsListCtrl", ["$scope", "$http", "requests", "$location", '$cookieStore', '$rootScope', "modals", "modalType",'$stateParams', 'LogusConfig',  function ($scope, $http, requests, $location, $cookieStore, $rootScope, modals, modalType,$stateParams,LogusConfig) {
    var sc = $scope;
    var type = $stateParams.type;
	var met = 1;
    sc.title = type == 1 ? 'АКТЫ СПИСАНИЯ ТМЦ' : 'ПОСТУПЛЕНИЕ ТМЦ';
    var filteredData = [];
	var filteredData = []; 
	var fullData;
    var curUser = $cookieStore.get('currentUser');
    $rootScope.userName = curUser.username;
    $rootScope.exit = 'выход';

    if ($cookieStore.get('dateDocumentsCalendar')) {
        if ($cookieStore.get('dateDocumentsCalendar').date) {
            sc.dateDocuments = $cookieStore.get('dateDocumentsCalendar').date;
        }
    }
	var firstLoad = false;
    sc.tableData = [];
    sc.mySelections = [];
    sc.formData = {};
    sc.infoData = {};
	sc.materialsData = {};
    var rowsForDel = [];
	var rowsForUpd = [];
	var nextNumber;
	sc.balanceList ={};
	sc.selectedRow1 = [];
	var rowsForSave = [];
	var rowsForNotSave = [];
	var allrows = [];
	var bufRows = [];
	var allNumbers = [];
    var checkAllNumbers = [];
    var dates = [];
	var act_numbers = [];
    var metka = 0;
	var nameMaterial; var idMaterial;
    var nameResponsible; var idResponsible;
	var path;  var m2=0;var m3=0; countWin=0;count=0;
	sc.OrganizationsList = [];
	  sc.tporgid = curUser.tporgid;
  	if (curUser.tporgid == 1) {
	sc.OrganizationsList.push({Id : curUser.orgid, Name : curUser.orgname });
	sc.OrgId = curUser.orgid;	
			}
	    //устанавливаем даты в период по умолчанию (начало - 1 день текущего месяца, окончание - сегодня)
    sc.formData.machineStartDate = sc.formData.driverStartDate = sc.formData.refuelStartDate = sc.formData.exportStartDate = moment().startOf('month').utc().toString("MM.DD.YYYY");	
    // пейджинг
    sc.totalServerItems = 0;
	console.log(type);
	if (type != 1 ) {
	var pageSize = 10;
	} else {
		var pageSize = 100;
	}
    sc.pagingOptions = {
        pageSizes: [10, 15, 20, 50, 100],
        pageSize: pageSize,
        currentPage: 1,
        totalServerItems: sc.totalServerItems
    };

	
		sc.changeOrg = function() {
		if (firstLoad == true) {
			getActs();	
		}
		firstLoad = true;
		
	}
	
    // смотрим за изменениям в пейджинге
    sc.$watch('pagingOptions', function (newVal, oldVal) {
        if (newVal.currentPage != null && newVal !== oldVal && !isNaN(newVal.currentPage) && !isNaN(oldVal.currentPage)) {
            if (newVal.pageSize !== oldVal.pageSize) {
                sc.pagingOptions.currentPage = 1;
             if (type == 1) { getActs();}  
            }
            if (newVal.currentPage !== oldVal.currentPage) { 
			if (type == 1) { getActs();}
			;}
        }
    }, true);


    // событие на сортировку по столбцу
    sc.$on("ngGridEventSorted", function (e, field) {
            sc.sortOptions.fields = field.field;
            sc.sortOptions.directions = field.sortDirection == "asc" ? 'OrderBy' : 'OrderByDescending';
            getActs();
        }
    );
	//функция выбора строки (перемещение материалов)
	function selectFirstRow()
	{
	 if ($cookieStore.get('selectedAct')) {
                selectRowByField("Pk", $cookieStore.get('selectedAct').id);
               $cookieStore.remove('selectedAct');
            } else selectRowByField("Pk", "first");	
			
	}
		//поиск по номеру накладкой
		 sc.search = function() {
   if ($cookieStore.get('timerActsId')) {
	   clearInterval($cookieStore.get('timerActsId'));
   }
   $cookieStore.put('timerActsId', setTimeout(GetInvoiceActsByDate, 1000));	
	};
		function GetInvoiceActsByDate() {
		requests.serverCall.post($http, "ActsSzrCtrl/GetActsLists", {
            PageSize: sc.pagingOptions.pageSize,
            PageNum: sc.pagingOptions.currentPage,
			CheckSearch : sc.filterText,
			OrgId : sc.OrgId,
        }, function (data, status, headers, config) {
            console.info(data);
            sc.totalServerItems = data.CountItems;
            sc.pagingOptions.totalServerItems = data.CountItems;
            sc.tableData = fullData = data.Values;
			//
			 if (!sc.filterText) {
            sc.tableData = fullData;    
        } else {
             filteredData = [];
            for (var i=0, li = fullData.length; i<li; i++) {
                var d = fullData[i];
			if (d.Number!=null)	console.log(d);
                var text = '';
                for (var p in d) {
					if (p == 'Number')
					{
                    text += d[p];
					}
                }
                if (text.toLowerCase().indexOf(sc.filterText.toLowerCase()) != -1) {
                    filteredData.push(fullData[i]);
                }
            }
            sc.tableData = filteredData;
        }
			//
         //   if ($cookieStore.get('selectedAct')) {
         //       selectRowByField("Id", $cookieStore.get('selectedAct').id);
        //       $cookieStore.remove('selectedAct');
        //    } else selectRowByField("Id", "first");
        });
		}
		
		sc.refreshTable = function() {
		loadTypeList(sc.materialsData.MatId);
    	
    };
	    loadTypeList = function(num) {
			
		 }
	   	if (type == 3 || type == 1) { 
	     requests.serverCall.post($http, "ActsSzrCtrl/GetInfoBalanceMaterials",{OrgId :curUser.orgid }, function (data, status, headers, config) {
        console.info(data);
        sc.materialsData = data;
		sc.OrganizationsList = data.OrganizationsList;
		if (curUser.tporgid == 2) {
				sc.OrgId = -1;
			}  else {
			sc.OrgId = curUser.orgid;	
			}
	    }, function () { });
		
		}
		
	//проверка цифр
	sc.checkNumbers = function(row) {
	row.Act_number=row.Act_number.toString().replace(/[^0-9]/gim,'');
	}
	
	//СЧИТАЕМ ПРОИЗВЕДЕНИЕ
     sc.changeCountPrice = function(row) {
		 //update
			if (row.Pk!=undefined) {
       rowsForUpd.push({
                   Document: row,
                });	
		}
		 
		 //
     row.Kol=row.Kol.toString().replace(",", "."); 
     row.Price=row.Price.toString().replace(",", "."); 
            if (row.Price === null) {
                row.Price = 0;
            }
            if (row.Kol === null) {
                row.Kol = 0;
            }
            row.Cost = (parseFloat(row.Price) * parseFloat(row.Kol)).toFixed(2);
        };
	   // акты списания
	if (type == 1) { 
	 sc.button_name = 'Выгрузить';
    sc.gridOptions = {
        data: 'tableData',
        enableColumnResize: true,
        multiSelect: false,
        enableRowSelection: true,
        enableCellEdit: true,
        enablePaging: true,
        showFooter: true,
        pagingOptions: sc.pagingOptions,
        selectedItems: sc.mySelections,
        rowHeight: 49,
        headerRowHeight: 49,
        useExternalSorting: true,
        i18n: 'ru',
        //footerTemplate: '',
		
        columnDefs: [
			{field: "DateBegin", displayName: "Дата", width: "15%", cellTemplate:'<a tooltip="{{row.getProperty(col.field)}}" tooltip-popup-delay="1000" tooltip-class="my-custom-tooltip" tooltip-placement="top" tooltip-append-to-body="true"><div class="ngCellText">{{row.getProperty(col.field)}}</div></a>',
			headerCellTemplate: "<div   ng-class=\"'colt' + col.index\" class=\"ngHeaderText\">{{col.displayName}}</div>"},
			 {field: "Number", displayName: "№ акта", width: "15%",  cellTemplate:'<a tooltip="{{row.getProperty(col.field)}}" tooltip-popup-delay="1000" tooltip-class="my-custom-tooltip" tooltip-placement="top" tooltip-append-to-body="true"><div class="ngCellText">{{row.getProperty(col.field)}}</div></a>',
			headerCellTemplate: "<div   ng-class=\"'colt' + col.index\" class=\"ngHeaderText\">{{col.displayName}}</div>"},
            {field: "Organization", displayName: "Организация", width: "30%", cellTemplate:'<a tooltip="{{row.getProperty(col.field)}}" tooltip-popup-delay="1000" tooltip-class="my-custom-tooltip" tooltip-placement="top" tooltip-append-to-body="true"><div class="ngCellText">{{row.getProperty(col.field)}}</div></a>',
			headerCellTemplate: "<div   ng-class=\"'colt' + col.index\" class=\"ngHeaderText\">{{col.displayName}}</div>"},
            {field: "RecipientName", displayName: "Получатель", width: "25%", cellTemplate:'<a tooltip="{{row.getProperty(col.field)}}" tooltip-popup-delay="1000" tooltip-class="my-custom-tooltip" tooltip-placement="top" tooltip-append-to-body="true"><div class="ngCellText">{{row.getProperty(col.field)}}</div></a>',
			headerCellTemplate: "<div   ng-class=\"'colt' + col.index\" class=\"ngHeaderText\">{{col.displayName}}</div>"},
			
		//	{field: "Year", displayName: "Год урожая", width: "11%", cellTemplate:'<a tooltip="{{row.getProperty(col.field)}}" tooltip-popup-delay="1000" tooltip-class="my-custom-tooltip" tooltip-placement="top" tooltip-append-to-body="true"><div class="ngCellText">{{row.getProperty(col.field)}}</div></a>',
		//	headerCellTemplate: "<div   ng-class=\"'colt' + col.index\" class=\"ngHeaderText\">{{col.displayName}}</div>"} ,
			
			{field: "Cost1", displayName: "Итого", width: "15%", cellTemplate:'<a tooltip="{{row.getProperty(col.field)}}" tooltip-popup-delay="1000" tooltip-class="my-custom-tooltip" tooltip-placement="top" tooltip-append-to-body="true"><div class="ngCellText">{{row.getProperty(col.field)}}</div></a>',
			headerCellTemplate: "<div   ng-class=\"'colt' + col.index\" class=\"ngHeaderText\">{{col.displayName}}</div>"} ,
			{field: "Id", displayName: "", width: "0%", cellTemplate:'<a tooltip="{{row.getProperty(col.field)}}" tooltip-popup-delay="1000" tooltip-class="my-custom-tooltip" tooltip-placement="top" tooltip-append-to-body="true"><div class="ngCellText">{{row.getProperty(col.field)}}</div></a>',
			headerCellTemplate: "<div   ng-class=\"'colt' + col.index\" class=\"ngHeaderText\">{{col.displayName}}</div>"} 
        ],   
		
        afterSelectionChange: function (row) {
            sc.mySelections = [];
            sc.mySelections.push(row.entity);
        }
    }; }
  
    var dt = new Date();
	var month=(dt.getMonth()+1).toString();
	var day =dt.getDate().toString();
	if (day.length==1) {day ='0'+day; }
	if (month.length==1) {month ='0'+ month; }
	   //КНОПКА "СОЗДАТЬ" СТРОКУ В ТАБЛИЦЕ
    sc.createAct1 = function () {
		  requests.serverCall.post($http, "ActsSzrCtrl/GetBalanceMaterialsList", {}, function (data, status, headers, config) {
           console.info(data);
           sc.balancelist = data;
	  });
	  
		//списание
		if (type == 1) {
		$location.path(type == 1 ? '/act/create/' : '/act/incoming/create/');
		}
		
    };
	//"скопировать"
	 sc.copyBtnClick1 = function () {
		 var newAct = {
            DateBegin: day +'.'+month+'.'+dt.getFullYear(),
            Kol: sc.mySelections[0].Kol,
            Price: sc.mySelections[0].Price,
			Cost:sc.mySelections[0].Cost,
			Material: {Id:sc.mySelections[0].Material.Id, Name: sc.mySelections[0].Material.Name},
			Responsible: {Id:sc.mySelections[0].Responsible.Id, Name: sc.mySelections[0].Responsible.Name},
        };
        sc.tableData.splice(0, 0, newAct); 
		 
		   
	 }
	
	 // редактирование ячеек с модальными окнами
    sc.updateSelectedCell = function (colField, selectedVal, displayName, row) {
        if (!row.Id) {
            sc.colField = colField;
            sc.selectedRow = [];
			if (sc.mySelections1==row) { var met=2;}
			  console.log(row);
			sc.mySelections1 = row;
			console.log(sc.mySelections1);
			sc.selectedRow.push(row);
				 //update
			if (row.Pk!=undefined) {
       rowsForUpd.push({
                   Document: row,
                });	
		}
		//
			
			
            var modalValues;
			if (met==2) {
            if (sc.infoData) {
                switch (sc.colField) {
					
                    case "Material":
                        modalValues = sc.infoData.MaterialsList;
                        break;
					case "Responsible":	 
					 modalValues = sc.infoData.EmployeesList;
                        break;	
                     case "Recipient":	 
					 modalValues = sc.infoData.EmployeesList;
                      break;							
                }

                modals.showWindow(modalType.WIN_SELECT, {
                    nameField: displayName,
                    values: modalValues,
                    selectedValue: selectedVal
                })
                    .result.then(function (result) { 
                        console.info(result);
                        row[sc.colField] = result;
						 if (sc.colField == 'Material')
						 {nameMaterial=result.Name;
					 idMaterial =result.Id;
					 }
                        if (sc.colField == 'Responsible')
						 {nameResponsible=result.Name;
					 idResponsible=result.Id;
					 }

                        if (sc.colField == 'Responsible' ) {
							 metka=0;
						for (var i = 0, li = sc.balancelist.length; i < li; i++) {
					if (idMaterial==sc.balancelist[i].IdMaterial && idResponsible==sc.balancelist[i].IdEmployees && type==3)
						{row.Price=sc.balancelist[i].Current_price;metka=1; break;} 	
						}
						   if (metka == 0 && type == 3) {
					sc.tableData.splice(sc.tableData.indexOf(row), 1);
						    //если нет связки Получатель-Материал
							 modals.showWindow(modalType.WIN_MESSAGE, {
                title: "Ошибка",
                message: "Отсутствует " + nameMaterial + " у " + nameResponsible + "Строка удалена.",
                callback: function () {
                }
            });
			////				
						}
                            console.info(sc.infoData.MaterialsList);
                        }
                    }, function () {
                        console.info('Modal dismissed at: ' + new Date());
                    });

            } else {
                console.log('не пришла sc.infoData');
            }
			//
		}
        }
    };
	compareNum = function(personA, personB) {
  return personA.Act_number - personB.Act_number;
}
	//Удаление строки в таблице 
	    sc.delRow = function(row) {
		 modals.showWindow(modalType.WIN_DIALOG, {
         title: "Удаление", message: "Вы уверены, что хотите удалить запись?", callback: function () {
	     rowsForDel = [];
         sc.tableData.splice(sc.tableData.indexOf(row), 1);
            rowsForDel.push({
                   command: 'del',
                   Document: row
                });	
        	if (row.Pk!= undefined)	{		
		  requests.serverCall.post($http, "ActsSzrCtrl/DeleteRow", rowsForDel, function (data, status, headers, config) {
			  getActSzrDetails2();
        }, function() {
        });	
			};
	 }}); };
	//сохранение строк в БД
	    saveRow = function () {
//-------------------------------
	     if (sc.formData.MaterialsList.length!=0) {
			 console.log(rowsForSave);
	         requests.serverCall.post($http, "ActsSzrCtrl/CreateUpdateActSzrDetail1", rowsForSave, function (data, status, headers, config) {
            console.info(data);
			rowsForSave=[];
			bufRows=[];
            requests.serverCall.post($http, "ActsSzrCtrl/GetActSzrDetails1", {
				PageSize: sc.pagingOptions.pageSize,
				PageNum: sc.pagingOptions.currentPage,
				OrgId : sc.OrgId,
		}, function (data, status, headers, config) {
		 sc.pagingOptions.totalServerItems = data.CountItems;
                sc.formData = data;
                allrows=data.MaterialsList;
			 for (var j = 0, l1 = rowsForNotSave.length; j < l1; j++) {
               {allrows.push(rowsForNotSave[j].MaterialsList) ;}
                }	
				console.log(allrows);
			allrows.sort(compareNum);
			 for (var j = 0, l1 = allrows.length; j < l1; j++) {
               {bufRows.unshift(allrows[j]) ;}
                }
			
			console.log(bufRows);
			
                sc.tableData = bufRows;
                allrows=[];
                rowsForNotSave=[];
			    selectFirstRow();
                modals.showWindow(modalType.WIN_MESSAGE, {
                    title: "Сохранение документа",
                    message: "Документ успешно сохранен.",
                    callback: function () {
                    }
                }); 
		
              
            });
		 });
		 }		  
					
       //----------------------------
	    }
	//сохранить строку в таблице (перемещение)
	    sc.saveAct1 = function () {
     var sendedData = getRowsForCreate();
        console.info(sendedData);
        sc.formData.MaterialsList = sendedData;
		sc.formData.EmployeesList = sendedData;
        console.log(sc.formData);
	    rowsForSave=[];
        m2=0;
	    for (var i = 0, li = sc.formData.MaterialsList.length; i < li; i++) {
		for (var j = 0, l = sc.balancelist.length; j < l; j++) {
			console.log(sc.formData);
				////////////////////если количество не превышено   
				     if (sc.formData.MaterialsList[i].Material.Id == sc.balancelist[j].IdMaterial && sc.formData.EmployeesList[i].Responsible.Id == sc.balancelist[j].IdEmployees
               && sc.formData.EmployeesList[i].Kol <= sc.balancelist[j].Balance) {
				      rowsForSave.push({
                      EmployeesList: sc.formData.EmployeesList[i],
					  MaterialsList : sc.formData.MaterialsList[i],
                       OrgId :  curUser.orgid,
                      });    
			   } 
			
			if (sc.formData.MaterialsList[i].Material.Id == sc.balancelist[j].IdMaterial && sc.formData.EmployeesList[i].Responsible.Id == sc.balancelist[j].IdEmployees) {break;}
			if (j==sc.balancelist.length-1) {
			//добавляем правильную строку	
			     rowsForSave.push({ 
                      EmployeesList: sc.formData.EmployeesList[i],
					  MaterialsList : sc.formData.MaterialsList[i],
					  OrgId :  curUser.orgid,
                      }); 	
			}
		}}
	   //если все строки в таблице правильные, то saveRow()
	     m3=0;
	    for (var i = 0, li = sc.formData.MaterialsList.length; i < li; i++) {
		for (var j = 0, l = sc.balancelist.length; j < l; j++) {
		 if (sc.formData.MaterialsList[i].Material.Id == sc.balancelist[j].IdMaterial && sc.formData.EmployeesList[i].Responsible.Id == sc.balancelist[j].IdEmployees
               && sc.formData.EmployeesList[i].Kol > sc.balancelist[j].Balance) {
				   m3=1;
			   }	
		}}
		if (m3==0 && rowsForSave.length!=0)  {saveRow();}
	   //
	   
        //сверяем количество с баланса
		allNumbers =[];
        checkAllNumbers=[];
       for (var i = 0, li = sc.formData.MaterialsList.length; i < li; i++) {
           for (var j = 0, l = sc.balancelist.length; j < l; j++) {
			      
			   //если количество превышено
               if (sc.formData.MaterialsList[i].Material.Id == sc.balancelist[j].IdMaterial && sc.formData.EmployeesList[i].Responsible.Id == sc.balancelist[j].IdEmployees
               && sc.formData.EmployeesList[i].Kol > sc.balancelist[j].Balance) {
				   //массив всех Id 
	              allNumbers.push(i);
                  countWin++;
                   modals.showWindow(modalType.WIN_DIALOG, {
                       title: "Предупреждение",id:parseInt(i), message: "Не достаточно " + sc.formData.MaterialsList[i].Material.Name + " у "
                        + sc.formData.EmployeesList[i].Responsible.Name + " Сохранить запись?", callback:function () {	
                      }   
				 }).result.then(function (result) {
                }, function (data) {
                    count++;
					console.log(data);
				if (data.Number==1) 
				{
					checkAllNumbers.push(data.Id);
				 //записываем  неправильную строку в массив
                      rowsForSave.push({
                      EmployeesList: sc.formData.EmployeesList[data.Id],
					  MaterialsList : sc.formData.MaterialsList[data.Id],
					  OrgId :  curUser.orgid,
                      });

					  
				 }
				 //если не сохранили неправильную строку
				 if (data.Number==2)
				{
				  checkAllNumbers.push(data.Id);
			   rowsForNotSave.push({
                      EmployeesList: sc.formData.EmployeesList[data.Id],
					  MaterialsList : sc.formData.MaterialsList[data.Id],
					  OrgId :  curUser.orgid,
                     
                      });	
				}
				 if (count==countWin && rowsForSave.length!=0)  {
				     for (var i = 0, l = allNumbers.length; i < l; i++) {
				         if (find(checkAllNumbers, allNumbers[i]) === -1) {
				             rowsForNotSave.push({
				              EmployeesList: sc.formData.EmployeesList[i],
						      MaterialsList : sc.formData.MaterialsList[i],
				              OrgId :  curUser.orgid,
				             });

				         ;}  
				                 ;}
				 count=0;countWin=0; saveRow();  }
                });   
		 
                   ;}  
               ;}
           ;}		
    };
	/////конец сохранения
	
		sc.checkRes = function () {				
			var Orgid = null;
			if (sc.AllResource != true) {
				Orgid = curUser.orgid;
			} 
			  	 requests.serverCall.post($http, "ActsSzrCtrl/GetDictionaryForActs1", {OrgId : Orgid }, function (data, status, headers, config) {
					console.info(data);
					sc.infoData = data;
					});

			}
	
	
	//сохранение строки (поступление)
	  sc.saveAct2 = function () {
	 var sendedData = getRowsForCreate();
        console.info(sendedData);
        sc.formData.MaterialsList1 = sendedData;
		sc.formData.EmployeesList = sendedData;
        console.log(sc.formData);
		
	     if (sc.formData.MaterialsList1.length!=0) {
			 sc.formData.OrgId = curUser.orgid;
        requests.serverCall.post($http, "ActsSzrCtrl/CreateUpdateActSzrDetail2", sc.formData, function (data, status, headers, config) {
            console.info(data);
            requests.serverCall.post($http, "ActsSzrCtrl/GetActSzrDetails2", {
			PageSize: sc.pagingOptions.pageSize,
			PageNum: sc.pagingOptions.currentPage,
			OrgId : curUser.orgid,
		}, function (data, status, headers, config) {
		 sc.pagingOptions.totalServerItems = data.CountItems;
                console.info(data);
                sc.formData = data;
                sc.tableData = data.MaterialsList1;
			    selectFirstRow();
		
                modals.showWindow(modalType.WIN_MESSAGE, {
                    title: "Сохранение документа",
                    message: "Документ успешно сохранен.",
                    callback: function () {
                    }
                }); 
		
              
            });
		 });   } ;
            //update
            if (rowsForUpd.length!=0)
			{
			 requests.serverCall.post($http, "ActsSzrCtrl/UpdateComing", rowsForUpd, function (data, status, headers, config) {
                       rowsForUpd = [];	
				 }, function() {
					}); 
			}
	  }
	//конец сохранения
	
	    var getRowsForCreate = function() {
        var sendedData = [];
        console.info('get for create ');
        console.info(sc.tableData);
        for (var i= 0, li = sc.tableData.length; i<li; i++) {
            if (sc.tableData[i].Id == null) {
                sendedData.push(sc.tableData[i]);
            }
        }
        return sendedData;
    };
	
	
    $('a').tooltip({
        'delay': { show: 5000, hide: 3000 }
    });

    getActs();
    var _field, _val;
    // выделение строчки по определенному столбцу
    function selectRowByField(field, val) {
        _field = field;
        _val = val;
        sc.$on('ngGridEventData', sc.gridEvDataListener);
    }

    // листнер на событие изменение data в таблице
    sc.gridEvDataListener = function () {
        var offCallMeFn = $scope.$on("ngGridEventData", sc.gridEvDataListener);
        offCallMeFn();
        if (_val == "first" && sc.tableData.length > 0) _val = sc.tableData[0][_field];
        sc.tableData.forEach(function (item, i, arr) {
            if (item[_field] == _val) {
                sc.gridOptions.selectRow(i, true);
                if (sc.gridOptions.ngGrid) sc.gridOptions.ngGrid.$viewport.scrollTop(sc.gridOptions.ngGrid.rowMap[i] * sc.gridOptions.ngGrid.config.rowHeight);
            }
        });
        $(".ngViewport").focus();
    };
    
    // получение списка актов списания
    function getActs() {
		console.log(type);
		if (type == 1) {
		 requests.serverCall.post($http, "ActsSzrCtrl/GetActsLists", {
            PageSize: sc.pagingOptions.pageSize,
            PageNum: sc.pagingOptions.currentPage,
			OrgId : sc.OrgId,
        }, function (data, status, headers, config) {
            console.info(data);
            sc.totalServerItems = data.CountItems;
            sc.pagingOptions.totalServerItems = data.CountItems;
            sc.tableData = data.Values;
            if ($cookieStore.get('selectedAct')) {
                selectRowByField("Id", $cookieStore.get('selectedAct').id);
               $cookieStore.remove('selectedAct');
            } else selectRowByField("Id", "first");
        });
	}
    }
     
    // модальное окно с требованием выделить строку
    function needSelection(title, message) {
        modals.showWindow(modalType.WIN_MESSAGE, {
            title: title,
            message: message,
            callback: function () {
            }
        });
    }
    //редактирование документа
    sc.editBtnClick = function () {
        if (sc.mySelections[0]) {
            //console.info('edit  ' + sc.mySelections[0].WaysListId);
            $location.path((type == 1 ? '/act/edit/' : '/act/incoming/edit/') + sc.mySelections[0].Id);
        } else needSelection('Редактирование документа', "Для редактирования необходимо выбрать документ");
    };
    //удаление документа
    sc.deleteBtnClick = function () {
        if (sc.mySelections[0]) {
            modals.showWindow(modalType.WIN_DIALOG, {
                    title: "Удаление", message: "Удалить выбранный документ?", callback: function () {
                        requests.serverCall.post($http, "ActsSzrCtrl/DeleteActsById",
                            {
                                PageSize: sc.pagingOptions.pageSize,
                                PageNum: sc.pagingOptions.currentPage,
                                SortField: sc.sortOptions.fields,
                                SortOrder: sc.sortOptions.directions,
                                Id: sc.mySelections[0].Id
                            },
                            function (data, status, headers, config) {
                            sc.tableData.forEach(function (item, d) {
                                if (item["Id"] == sc.mySelections[0].Id) sc.tableData.splice(d, 1);
                            });
                            console.info(data);
                        });
                    }
                }
            );
        } else needSelection('Удаление документа', "Для удаления необходимо выбрать документ");
    };

    // копирование документа (открывается страница как на создание-редактирование, но уже предзаполненная инфо с сервера (по идее копия как выделенная, но на текущую дату)
    sc.copyBtnClick = function () {
        if (sc.mySelections[0]) {
            $location.path('/act/copy/' + sc.mySelections[0].Id);
        } else needSelection('Копирование документа', "Для копирования необходимо выбрать документ");
    }
    sc.openBalanceMaterial = function() {
        var url = '/balance_material';
        $location.path(url);
    };
	//загрузить/выгрузить
    sc.loadBtnClick = function() {
	if(sc.button_name=="Загрузить")
	{
	  modals.showWindow(modalType.WIN_UPLOAD_FILE, {
                title: LogusConfig.serverUrl,
            }).result.then(function (result) {
                    console.info(result);
                }, function () {
                    console.info('Modal dismissed at: ');
                });
	}			
	if(sc.button_name=="Выгрузить")
	{
	 var dateStart = angular.copy(sc.formData.driverStartDate);
	 var dateEnd = angular.copy(sc.formData.driverEndDate);
     var dstart =  moment(dateStart).format("MM.DD.YYYY");	 
	 var dend =  moment(dateEnd).format("MM.DD.YYYY");
	  dates.push(dstart);
	  dates.push(dend);
	  var popupWin = window.open(LogusConfig.serverUrl + 'TMCExport/Exp?startDate=' + dstart
      + '&endDate=' + dend, '_blank');
	}	
			
    }
}]);


