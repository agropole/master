var app = angular.module("BalanceMaterial", ["ngGrid", "services", 'ngCookies', "modalWindows"]);
app.controller("BalanceMaterialCtrl", ["$scope", "$http", "requests", "$location","$cookieStore", function ($scope, $http, requests, $location, $cookieStore) {
    var sc = $scope;
    sc.infoData = {};
    sc.formData = {}; 
	var firstLoad = false;
    sc.OrganizationsList = [];
	var filteredData = [];
   var count = 1;
   var curUser = $cookieStore.get('currentUser');
   console.log(curUser);
   sc.tporgid = curUser.tporgid;
	requests.serverCall.post($http, "ActsSzrCtrl/GetInfoBalanceMaterials",{OrgId :curUser.orgid  }, function (data, status, headers, config) {
        console.info(data);
        sc.infoData = data;
		if (curUser.tporgid == 2) {
				sc.formData.OrgId = -1;
			}  else {
			sc.formData.OrgId = curUser.orgid;	
			}
        loadTypeList();
    }, function () {
    });
    sc.selectedRow = [];
    sc.gridOptions = {
        enableColumnResize: false,
        enableRowSelection: false,
        showFooter: false,
        rowHeight: 49,
        headerRowHeight: 49,
        useExternalSorting: true,
        data: 'TableData',
        selectedItems: sc.selectedRow,
        afterSelectionChange: function (row) {
            console.info(sc.selectedRow);
            if (row.entity) {
                if (row.selected) {
                    sc.selectedRow = [row];
                } else {
                    if (sc.selectedRow && sc.selectedRow > 0) {
                        sc.selectedRow.splice(sc.tableDataFirst.indexOf(row.entity), 1);
                    }
                }
            }
        },
        beforeSelectionChange: function (row, event) {
            angular.forEach(sc.TCData, function (data, index) {
                if (data.selected == true) sc.selectedRow.splice(index, 1);
                sc.gridOptions.selectRow(index, false);
            });
            return true;
        }
    };
    sc.gridOptions.columnDefs = [
        {
            field: "Name", displayName: "Название", width: "25%", cellClass: 'cellToolTip',
              cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()">' +
            '<input value="{{row.entity[col.field]}}" ng-model="row.entity[col.field]" placeholder="Тип" class="bordernone" readonly>' +
            '</div>', headerCellTemplate: '<div class="ngHeaderSortColumn {{col.headerClass}}" ng-style="{\'cursor\': col.cursor}" ng-class="{ \'ngSorted\': !noSortVisible }">' +
        '<div ng-click="sorting(); col.sort()"  ng-class="\'colt\' + col.index" class="ngHeaderText" >{{col.displayName}}</div>' +
        '<div class="ngSortButtonDown" ng-show="col.showSortButtonDown()">' +
        '</div><div class="ngSortButtonUp" ng-show="col.showSortButtonUp()">'
        },
        {
            field: "Description", displayName: "Тип", width: "15%",
            cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()">' +
            '<input value="{{row.entity[col.field]}}" ng-model="row.entity[col.field]" placeholder="Тип" class="bordernone" readonly>' +
            '</div>',headerCellTemplate: "<div   ng-class=\"'colt' + col.index\" class=\"ngHeaderText\">{{col.displayName}}</div>"
        },
        {
            field: "Balance", displayName: "Остаток", width: "15%",
              cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()">' +
            '<input value="{{row.entity[col.field]}}" ng-model="row.entity[col.field]" placeholder="Баланс" class="bordernone" readonly>' +
            '</div>',headerCellTemplate: "<div   ng-class=\"'colt' + col.index\" class=\"ngHeaderText\">{{col.displayName}}</div>"
        },
        {
            field: "Current_price", displayName: "Цена, р/ед", width: "15%",
             cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()">' +
            '<input value="{{row.entity[col.field]}}" ng-model="row.entity[col.field]" placeholder="Цена" class="bordernone" readonly>' +
            '</div>',headerCellTemplate: "<div   ng-class=\"'colt' + col.index\" class=\"ngHeaderText\">{{col.displayName}}</div>"
        },
		 {
            field: "Summa", displayName: "Сумма", width: "10%",
             cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()">' +
            '<input value="{{row.entity[col.field]}}" ng-model="row.entity[col.field]" placeholder="Цена" class="bordernone" readonly>' +
            '</div>',headerCellTemplate: "<div   ng-class=\"'colt' + col.index\" class=\"ngHeaderText\">{{col.displayName}}</div>"
        },
         {
            field: "Employees", displayName: "Ответственный", width: "20%",
          cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()">' +
            '<input value="{{row.entity[col.field]}}" ng-model="row.entity[col.field]" placeholder="Ответственный" class="bordernone" readonly>' +
            '</div>',headerCellTemplate: "<div   ng-class=\"'colt' + col.index\" class=\"ngHeaderText\">{{col.displayName}}</div>"
        },
        {field: "Id", width: "0%"}
    ];
 
	sc.changeOrg = function() {
		if (firstLoad == true) {
			loadTypeList();
		}
		firstLoad = true;		
	}
	
	sc.changeEmp = function() {
		if (firstLoad == true) {
			loadTypeList();
		}
		firstLoad = true;	
		
	}
	
    var fullData; var fullData1;
    var loadTypeList = function() {
        requests.serverCall.post($http, "ActsSzrCtrl/GetBalanceMaterialsList", 
       {MatId : sc.formData.MatId, OrgId: sc.formData.OrgId, IdEmployees: sc.formData.EmpId }, function (data, status, headers, config) {
            sc.TableData = fullData = fullData1 = data;
        }, function () {
		}); 
    };

    sc.openIncomingList = function() {
		var url = '/coming_tmc';
     //   var url = '/acts_list/2';
        if (sc.selectedRow[1]) {
            url += sc.selectedRow[1].entity.mat.Id;
        }
        $location.path(url);
    };

    sc.openActsList = function() {
      var url = '/acts_list/1';
        if (sc.selectedRow[1]) {
            url += sc.selectedRow[1].entity.mat.Id;
        }
        $location.path(url);
    };
    // перемещение материалов
	  sc.openMovementList = function() {
		 var url = '/movement_tmc';    
      //  var url = '/acts_list1/3';
        if (sc.selectedRow[1]) {
            url += sc.selectedRow[1].entity.mat.Id;
        }
        $location.path(url);
    };
	
	
    sc.refreshTable = function() {
    	loadTypeList();
    };
    sc.search = function() {
        if (!sc.filterText) {
            sc.TableData = fullData;    
        } else {
             filteredData = [];
            for (var i=0, li=fullData.length; i<li; i++) {
                var d = fullData[i];
                var text = '';
                for (var p in d) {
                    text += d[p];
                }
                if (text.toLowerCase().indexOf(sc.filterText.toLowerCase()) != -1) {
                    filteredData.push(fullData[i]);
                }
            }
            sc.TableData = filteredData;
        }
    }
	//сортировка
    sc.sorting = function () {
        var names = [];
        var filteredData1 = [];
        if (!sc.filterText) {
            fullData1 = [];
		
            for (var i = 0, li = fullData.length; i < li; i++) {
                fullData1.push(fullData[i]);
            };
			
        }
        if (sc.filterText) {
          fullData1 = [];
            for (var i = 0, li = filteredData.length; i < li; i++) {
                fullData1.push(filteredData[i]);
            };
        } 
            for (var i = 0, li = fullData1.length; i < li; i++) {
                names.push(fullData1[i].Name);
            }
            names.sort();
            if (count % 2 == 0) { names.reverse(); }
            for (var i = 0, li = fullData1.length; i < li; i++) {
                for (var j = 0, l1 = fullData1.length; j < l1; j++) {
                    if (names[i] == fullData1[j].Name) {
						filteredData1.push(fullData1[j]);fullData1.splice(j,1);
						break;
						}
                }
            }
            sc.TableData = filteredData1;
            count++;
    }
	
	
	
	

}]);