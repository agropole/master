var app = angular.module("MovementTmc", ["ngGrid", "services", 'ngCookies', "modalWindows","LogusConfig"]);
app.controller("MovementTmcCtrl", ["$scope", "$http", "requests", "$location", '$cookieStore', '$rootScope', "modals", "modalType",'$stateParams', 'LogusConfig',  function ($scope, $http, requests, $location, $cookieStore, $rootScope, modals, modalType,$stateParams,LogusConfig) {
    var sc = $scope;
    sc.selectedRow = [];
	var rowsForDel = [];
	var rowsForUpd = [];
	var filteredData = [];
	 sc.infoData = {};
	  var curUser = $cookieStore.get('currentUser');
	 sc.formData = {};
	 sc.multi = false;
	 var nextNumber;
	  $cookieStore.put('dateDailyCalendar', {date: null});
	 $cookieStore.put('dateEndDailyCalendar', {date: null});
	 var enddate = new Date();
	 var startD = new Date(enddate.getFullYear(), enddate.getMonth(), 1);
	 // checkCancelInvoice
	 	if ($cookieStore.get('dateInvoiceStartCalendar') && $cookieStore.get('checkCancelInvoice1') == 1) {
        if ($cookieStore.get('dateInvoiceStartCalendar').date) 
			console.log($cookieStore.get('dateInvoiceStartCalendar').date);
		sc.formData.invoiceStartDate = $cookieStore.get('dateInvoiceStartCalendar').date;
		$cookieStore.put('checkCancelInvoice1', null);
    } else {
	  sc.formData.invoiceStartDate = startD;	
	}
	 	 if ($cookieStore.get('dateInvoiceEndCalendar') &&  $cookieStore.get('checkCancelInvoice2') == 1) {
        if ($cookieStore.get('dateInvoiceEndCalendar').date) 
			console.log($cookieStore.get('dateInvoiceEndCalendar').date);
		sc.formData.invoiceEndDate = $cookieStore.get('dateInvoiceEndCalendar').date;
		$cookieStore.put('checkCancelInvoice2', null);
    } else {
	  sc.formData.invoiceEndDate = enddate;	
	}
	//поставщик
		 if ($cookieStore.get('Contractor')  && $cookieStore.get('checkCancelInvoice3') == 1) {
		sc.formData.ContractorsId = $cookieStore.get('Contractor');
		$cookieStore.put('checkCancelInvoice3', null);
    }
	
	sc.totalServerItems = 0;
    sc.pagingOptions = {
        pageSizes: [10, 15, 20, 50, 100],
        pageSize: 100,
        currentPage: 1,
        totalServerItems: sc.totalServerItems
    };
	   // смотрим за изменениям в пейджинге
    sc.$watch('pagingOptions', function (newVal, oldVal) {
        if (newVal.currentPage != null && newVal !== oldVal && !isNaN(newVal.currentPage) && !isNaN(oldVal.currentPage)) {
            if (newVal.pageSize !== oldVal.pageSize) {
                sc.pagingOptions.currentPage = 1;
            loadService();
            }
            if (newVal.currentPage !== oldVal.currentPage) 
			{ 
		   sc.infoData.StorageId = 0;
			loadService();
			
			;}
        }
    }, true);
	 
    sc.gridOptions = {
       enableColumnResize: true,
        enableRowSelection: true,
        showFooter: true,
        rowHeight: 49,
        headerRowHeight: 49,
        useExternalSorting: false,
        data: 'TableData',
		enablePaging: true,
        pagingOptions: sc.pagingOptions,
        selectedItems: sc.selectedRow,
		i18n: 'ru',
        afterSelectionChange: function (row) {
       	for (var i=0; i<row.length;i++ ) 
   { 
     sc.selectedRow.push(row[i].entity); 
     console.log(sc.selectedRow); 
        } 
	  if (row.entity) {
		  console.log(row.selected);
                if (row.selected) {
                    if (sc.multi) sc.selectedRow.push(row.entity);
                    else {
                        sc.selectedRow = [];
                        sc.selectedRow.push(row.entity);
                    }
                } else {
                    sc.selectedRow.splice(sc.TableData.indexOf(row.entity), 1);
                }
            }
	   
        },
        beforeSelectionChange: function (row, event) {
        	   if (!sc.multi) {
                angular.forEach(sc.TableData, function (data, index) {
                    if (data.selected == true) sc.selectedRow.splice(index, 1);
                    sc.gridOptions.selectRow(index, false);

                });
            }
		return true;
        }
    };

    sc.gridOptions.columnDefs = [
          {field: "DateBegin", displayName: "Дата", width: "15%", cellTemplate: '<a tooltip="{{row.getProperty(col.field)}}" tooltip-popup-delay="1000" tooltip-class="my-custom-tooltip" tooltip-placement="top" tooltip-append-to-body="true">' +
            '<div class="ngCellText" ng-class="col.colIndex()">' +
            '<input data="{{row.entity[col.field]}}" ng-change="changeCountPrice(row.entity)" ng-model="row.entity[col.field]" ng-readonly="row.entity.Id" ng-class="{bordernone: row.entity[\'Id\']}">' +
            '</div>' +
            '</a>', headerCellTemplate: "<div   ng-class=\"'colt' + col.index\" class=\"ngHeaderText\">{{col.displayName}}</div>"},
			
		     {field: "Number", displayName: "Номер ТН", width: "20%",
			 cellTemplate: '<a tooltip="{{row.entity[col.field][\'Name\']}}" tooltip-popup-delay="1000" tooltip-class="my-custom-tooltip" tooltip-placement="top" tooltip-append-to-body="true">' +
             '<div class="ngCellText" ng-class="col.colIndex()">' +
             '<input value="{{row.entity[col.field]}}" ng-model="row.entity[col.field]" placeholder="Номер" ng-class="{bordernone: row.entity[\'Id\']}" ng-readonly="row.entity[\'Id\']"  ng-change="changeCountPrice(row.entity)">' +
             '</div>' +'</a>',  headerCellTemplate: "<div   ng-class=\"'colt' + col.index\" class=\"ngHeaderText\">{{col.displayName}}</div>"},
			
			{field: "Storage", displayName: "Отправитель", width: "20%", cellTemplate: '<a tooltip="{{row.entity[col.field][\'Name\']}}" tooltip-popup-delay="1000" tooltip-class="my-custom-tooltip" tooltip-placement="top" tooltip-append-to-body="true">' +
            '<div class="ngCellText" ng-class="col.colIndex()">' +
            '<input value="{{row.entity[col.field][\'Name\']}}" placeholder="Отправитель" style="border: none;"  data-ng-click="updateSelectedCell(col.field, row.entity[col.field][\'Id\'], \'Поставщик\',row.entity,$event)" readonly>' +
            '</div>' +
            '</a>', headerCellTemplate: "<div   ng-class=\"'colt' + col.index\" class=\"ngHeaderText\">{{col.displayName}}</div>"},
			
			{field: "Contractor", displayName: "Получатель", width: "20%", cellTemplate: '<a tooltip="{{row.entity[col.field][\'Name\']}}" tooltip-popup-delay="1000" tooltip-class="my-custom-tooltip" tooltip-placement="top" tooltip-append-to-body="true">' +
            '<div class="ngCellText" ng-class="col.colIndex()">' +
            '<input value="{{row.entity[col.field][\'Name\']}}" placeholder="Получатель" style="border: none;"  data-ng-click="updateSelectedCell(col.field, row.entity[col.field][\'Id\'], \'Склад\',row.entity,$event)"  readonly>' +
            '</div>' +
            '</a>', headerCellTemplate: "<div   ng-class=\"'colt' + col.index\" class=\"ngHeaderText\">{{col.displayName}}</div>"},	 
			
			  {field: "Cost", displayName: "Сумма", width: "17%",
			 cellTemplate: '<a tooltip="{{row.entity[col.field][\'Name\']}}" tooltip-popup-delay="1000" tooltip-class="my-custom-tooltip" tooltip-placement="top" tooltip-append-to-body="true">' +
             '<div class="ngCellText" ng-class="col.colIndex()">' +
             '<input value="{{row.entity[col.field]}}" ng-model="row.entity[col.field]" placeholder="Сумма" ng-class="{bordernone: row.entity[\'Id\']}" ng-readonly="row.entity[\'Id\']"  ng-change="changeCountPrice(row.entity)">' +
             '</div>' +'</a>',  headerCellTemplate: "<div   ng-class=\"'colt' + col.index\" class=\"ngHeaderText\">{{col.displayName}}</div>"},
			
			{field: "Comment", displayName: " ", width: "8%",
            cellTemplate: 'templates/comment_shift_template.html', cellClass: 'cellToolTip'},
			
			 {field: "Id", width: "0%"},		 	 
    ];
	
	      /*
		requests.serverCall.post($http, "SparePartsCtrl/GetDictionarySpareParts", true, function (data, status, headers, config) {
        console.info(data);
        sc.infoData = data;
		var AllContractors = {Id:-1, Name :"Все"};
		sc.infoData.ContractorsList.splice(0,0,AllContractors);
		 });
		 */
		    requests.serverCall.post($http, "ActsSzrCtrl/GetInfoBalanceMaterials",{OrgId :curUser.orgid }, function (data, status, headers, config) {
				console.info(data);
				 sc.infoData = data;
			//	sc.materialsData = data;
				sc.OrganizationsList = data.OrganizationsList;
			var AllContractors = {Id:-1, Name :"Все"};
		    sc.infoData.ContractorsList.splice(0,0,AllContractors);
				if (curUser.tporgid == 2) {
					sc.OrgId = -1;
			}  else {
			sc.OrgId = curUser.orgid;	
			}  
			  loadService();
			
			
			}, function () { });
		 //==
		 
		 
		  var fullData;
			function loadService() {
		//	requests.serverCall.post($http, "ActsSzrCtrl/GetTmcActsByDate", {
			requests.serverCall.post($http, "ActsSzrCtrl/GetTmcMoveByDate", {
				PageSize : sc.pagingOptions.pageSize,
				PageNum : sc.pagingOptions.currentPage,
				DateStart : sc.formData.invoiceStartDate,
				DateEnd : sc.formData.invoiceEndDate,
				ContractorsId : sc.formData.ContractorsId,
				OrgId : sc.OrgId ,
			}, function (data, status, headers, config) {
				console.log(data);
		    sc.totalServerItems = data.CountItems;
            sc.pagingOptions.totalServerItems = data.CountItems;
            sc.TableData = fullData =  data.Values;
        }, function () {
        });
		
		
    };

  
  sc.refreshTable = function () {
	 $cookieStore.put('Contractor', sc.formData.ContractorsId);
	loadService();
		
  }
     sc.openBalanceMaterial = function() {
        var url = '/balance_material';
        $location.path(url);
    };
  
   sc.dateChangeSpareParts = function () {
	    $cookieStore.put('dateInvoiceStartCalendar', {date: sc.formData.invoiceStartDate});
		$cookieStore.put('dateInvoiceEndCalendar', {date: sc.formData.invoiceEndDate});
	    loadService();
		console.log(sc.formData.invoiceStartDate);
   }
       //создать акт прихода
    sc.createIncomingAct = function() {	
		$cookieStore.put('typeoper', 'create');
		$cookieStore.put('act_id', null);
	    $location.path('/movement_item'); 
    };
	
	
	//редактирование акт прихода
    sc.editBtnClick = function () {
        if (sc.selectedRow[0]) {
		$cookieStore.put('typeoper', 'edit');	
		$cookieStore.put('act_id', sc.selectedRow[0].Id);
       //   $location.path('/act/incoming/1/');
	     $location.path('/movement_item');
        } else needSelection('Редактирование документа', "Для редактирования необходимо выбрать документ");
    };
  //сохранить
    sc.save = function() {
        var rowsForSave = getNewRows();
        if (rowsForSave.complete) {
            saveRows(rowsForSave.rows, loadService);
        } else {
            saveError();
        }
    };
	
	function getMaxOfArray(numArray) {
   return Math.max.apply(null, numArray);
   }
	    // модальное окно с требованием выделить строку
    function needSelection(title, message) {
        modals.showWindow(modalType.WIN_MESSAGE, {
            title: title,
            message: message,
            callback: function () {
            }
        });
    }
	
	sc.changeOrg = function() {
		loadService();
		
	}
	
		sc.printHtml = function()  { 
		   requests.serverCall.post($http, "ActsSzrCtrl/GetPrintMoveInfo", 
			   {ActId: sc.selectedRow[0].Id},
			   function (data, status, headers, config) {
                    console.info(data);
					sc.printData = data;
                    setTimeout(function () {
                        var printContents = document.getElementById('demand').innerHTML;
                        var popupWin = window.open('', '_blank');
                        if (typeof popupWin != 'undefined') {
                            popupWin.document.write('<html><head></head><body >' + printContents + '</html>');
                            popupWin.print();
                            popupWin.close();
                        }
                    }, 1000);
                });
	}
		
		sc.printClick = function()  {
		if (sc.selectedRow[0].Id) {
            var popupWin = window.open(LogusConfig.serverUrl + 'SparePartsCtrl/GetPrintWriteoffInfo?req=' + sc.selectedRow[0].Id + '&act_number='
                                                                                        + 'MoveAct' 
																					    +'&date='+ sc.selectedRow[0].DateBegin,
																						'_blank');
        } else {
            modals.showWindow(modalType.WIN_MESSAGE, {
                title: "Печать",
                message: "Для печати необходимо сохранить данные",
                callback: function () {

                }
            });
        }
	}
	
	
	
	//поиск по Номеру ПН
	 sc.search = function() {
       $("#drop").addClass("crystalbutton");
	   $("#datepick").addClass("crystalbutton");
   if ($cookieStore.get('timerId')) {
	   clearInterval($cookieStore.get('timerId'));
   }
    $cookieStore.put('timerId', setTimeout(GetInvoiceActsByDate, 1000));	
	};
	function GetInvoiceActsByDate() {
		
	requests.serverCall.post($http, "ActsSzrCtrl/GetTmcMoveByDate", {
	 PageSize: sc.pagingOptions.pageSize,
     PageNum: sc.pagingOptions.currentPage,
	 DateStart : null,
	 DateEnd : null,
	 ContractorsId : null,
	 CheckSearch : sc.filterText,
        }, function (data, status, headers, config) {
		    sc.totalServerItems = data.CountItems;
            sc.pagingOptions.totalServerItems = data.CountItems;
            sc.TableData = fullData =  data.Values;
        }, function () {
        });	
	}
	$('#drop').click(function(){
	sc.filterText =  "";
    $("#datepick").removeClass("crystalbutton");
    $("#drop").removeClass("crystalbutton");
     });
	$('button.date-picker-btn').click(function(){
    sc.filterText =  "";
    $("#datepick").removeClass("crystalbutton");
    $("#drop").removeClass("crystalbutton");
     });
	//удаление строки
	   sc.deleteAct = function() {
		    console.log(sc.selectedRow);
		    if (sc.selectedRow.length != 0) {
            modals.showWindow(modalType.WIN_DIALOG, {
            title: "Удаление", message: "Удалить выбранный документ?", callback: function () {
				rowsForDel.push({
                 rows:sc.selectedRow[0]
              		});
					 console.log(rowsForDel);	
			    requests.serverCall.post($http, "ActsSzrCtrl/DeleteTmc", rowsForDel, function (data, status, headers, config) {
					 rowsForDel = [];
                     console.info(data);
					   if (data=='success') {
				sc.TableData.splice(sc.TableData.indexOf(sc.selectedRow[0]), 1);   
					   } else {
						    modals.showWindow(modalType.WIN_MESSAGE, {
                              title: "Предупреждение",
                              message: data,
                              callback: function () {
                              }  
                          });
					   }
					   
                        });
						;}
                    }  
            );
        } else needSelection('Удаление документа', "Для удаления необходимо выбрать документ");
		
	   }
	function sec1() { 
 $("div.ngFooterSelectedItems").remove();
}
setInterval(sec1, 200) 

}]);



