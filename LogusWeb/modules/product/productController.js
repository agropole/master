/**
 *  @ngdoc controller
 *  @name Product.contorller:ProductCtrl
 *  @description
 *  # ProductCtrl
 *  Это контроллер, определеяющий поведение формы баланса склада продукции
 *  @requires $scope
 *  @requires $location
 *  @requires $http
 *  @requires requests

 *  @property {Object} sc scope
 *  @property {Object} sc.infoData справочники
 *  @property {Object} sc.selectedRow выделенные строки
 *  @property {Object} sc.gridOptions настройки таблицы
 *  @property {Object} sc scope
 
 *  @property {function} loadProductList загрузка списка продукции
 *  @property {function} sc.changePrice изменении цены продажи
 *  @property {function} sc.openGrossHarvest открыть форму урожая
 *  @property {function} sc.updateSelectedCell открытие модальных окон выбора
 *  @property {function} sc.openSales открыть форму продаж
 */
var app = angular.module("Product", ["ngGrid", "services", 'ngCookies', "modalWindows"]);
app.controller("ProductCtrl", ["$scope", "$http", "requests", "$location", function ($scope, $http, requests, $location) {
    var sc = $scope;
    sc.infoData = {};

    requests.serverCall.get($http, "Products/GetInfoProduct", function (data, status, headers, config) {
        console.info(data);
        sc.infoData = data;
		sc.OrgId = -1;
        sc.YearId = new Date().getFullYear();

        loadProductList();
    }, function () {
    });

    sc.selectedRow = [];
    sc.gridOptions = {
        enableColumnResize: false,
        enableRowSelection: true,
        showFooter: false,
        rowHeight: 49,
        headerRowHeight: 49,
        useExternalSorting: false,
        data: 'TableData',
        selectedItems: sc.selectedRow,
        afterSelectionChange: function (row) {
            console.info(sc.selectedRow);
            if (row.entity) {
                if (row.selected) {
                    sc.selectedRow = [row];
                } else {
                    if (sc.selectedRow && sc.selectedRow > 0) {
                        sc.selectedRow.splice(sc.tableDataFirst.indexOf(row.entity), 1);
                    }
                }
            }
        },
        beforeSelectionChange: function (row, event) {
            angular.forEach(sc.TCData, function (data, index) {
                if (data.selected == true) sc.selectedRow.splice(index, 1);
                sc.gridOptions.selectRow(index, false);
            });
            return true;
        }
    };
    sc.gridOptions.columnDefs = [
        {
            field: "Plant",
            displayName: "Культура",
            width: "15%",
            cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()">' +
            '<input value="{{row.entity[col.field].Name}}" ng-model="row.entity[col.field].Name" placeholder="Культура" class="bordernone" readonly>' +
            '</div>'
        },
        {
            field: "GrossHarvest", displayName: "Сбор, т", width: "10%",
            cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()">' +
            '<input value="{{row.entity[col.field]}}" ng-model="row.entity[col.field]" placeholder="Валовый сбор" class="bordernone" readonly>' +
            '</div>'
        },
        {
            field: "Balance", displayName: "Остаток, т", width: "15%",
            cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()">' +
            '<input value="{{row.entity[col.field]}}" ng-model="row.entity[col.field]" placeholder="Остаток" class="bordernone" readonly>' +
            '</div>'
        },
        {
            field: "CurrentPrice", displayName: "Цена, р/т", width: "15%",
            cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()">' +
            '<input value="{{row.entity[col.field]}}" ng-model="row.entity[col.field]" placeholder="Цена" ng-change="changePrice(row.entity)">' +
            '</div>'
        },
		  {
            field: "ProfitPlan", displayName: "План, р", width: "13%",
            cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()">' +
            '<input value="{{row.entity[col.field]}}" ng-model="row.entity[col.field]" placeholder="Прибыль" class="bordernone" readonly>' +
            '</div>'
        },
        {
            field: "Sales", displayName: "Факт, р", width: "13%",
            cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()">' +
            '<input value="{{row.entity[col.field]}}" ng-model="row.entity[col.field]" placeholder="Прибыль" class="bordernone" readonly>' +
            '</div>'
        },
        {
            field: "Profit", displayName: "Отклонение", width: "19%",
            cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()">' +
            '<input value="{{row.entity[col.field]}}" ng-model="row.entity[col.field]" placeholder="Прибыль" class="bordernone" readonly>' +
            '</div>'
        },

        {field: "Id", width: "0%"},
		{field: "Kol", width: "0%"},
    ];

    var loadProductList = function() {
		  console.log(sc.YearId);
        requests.serverCall.post($http, "Products/YearCultureProduct", {Year: sc.YearId}, function (data, status, headers, config) {
            sc.TableData = data;
        }, function () {
        });
    };

	
	sc.recountBalance = function() {}
	
	
	
    sc.changePrice = function(row) {
        if (row.CurrentPrice && row.Balance) {
           row.ProfitPlan = (row.CurrentPrice * row.Balance).toFixed(2);
           row.Profit = (row.Sales - row.CurrentPrice * row.Kol).toFixed(2);
		   console.log(row.Profit);
            requests.serverCall.post($http, "Products/UpdateCultureCurPrice", row, function (data, status, headers, config) {
                console.info(data);
            }, function () {
            });
        }
    };

	 sc.refreshYear = function() {
		loadProductList();  
	 }
	
	
    sc.openGrossHarvest = function() {
        var url = '/gross/' + sc.YearId + "/";
        if (sc.selectedRow[0]) {
            url += sc.selectedRow[0].entity.Plant.Id;
        }
        $location.path(url);
    };

    sc.openSales = function() {
        var url = '/sales/' + sc.YearId + "/";
        if (sc.selectedRow[0]) {
            url += sc.selectedRow[0].entity.Plant.Id;
        }
        $location.path(url);
    };

}]);