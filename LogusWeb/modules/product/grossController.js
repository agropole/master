
var app = angular.module("Gross", ["ngGrid", "services", 'ngCookies', "modalWindows"]);
app.controller("GrossCtrl", ["$scope", "$http", "requests", "$location",'$stateParams',"modalType","modals","$cookieStore",
 function ($scope, $http, requests, $location, $stateParams, modalType, modals,$cookieStore) {
    var sc = $scope;
    sc.selectedRow = [];
	 var rowsForDel = [];
	 var rowsForUpd = [];
	 sc.multi = false;
	   var curUser = $cookieStore.get('currentUser');
   console.log(curUser);
   sc.tporgid = curUser.tporgid;
    sc.gridOptions = {
        enableColumnResize: true,
        enableRowSelection: true,
        showFooter: false,
        rowHeight: 49,
        headerRowHeight: 49,
        useExternalSorting: false,
        data: 'TableData',
        selectedItems: sc.selectedRow,
        afterSelectionChange: function (row) {
			met = 1;
       	for (var i = 0; i < row.length; i++ ) 
   { 
     sc.selectedRow.push(row[i].entity); 
     console.log(sc.selectedRow); 
   }
     if (sc.mySelections1 == row.entity) { 
	          met = 2;
	 }
			sc.mySelections1 = row.entity;
   
	  if (row.entity) {
		   for (var i = 0, li = sc.TableData.length; i<li; i++) {
				sc.TableData[i].CheckDate = null;
			}
			row.entity.CheckDate = 1;
		  
		  console.log(row.selected);
                if (row.selected) {
                    if (sc.multi) sc.selectedRow.push(row.entity);
                    else {
                        sc.selectedRow = [];
                        sc.selectedRow.push(row.entity);
                    }
                } else {
                    sc.selectedRow.splice(sc.TableData.indexOf(row.entity), 1);
                }
            }
        },
      beforeSelectionChange: function (row, event) {
		   if (!sc.multi) {
                angular.forEach(sc.TableData, function (data, index) {
                    if (data.selected == true) sc.mySelections.splice(index, 1);
                    sc.gridOptions.selectRow(index, false);

                });
            }
		
                return true;
            }
    };

    sc.gridOptions.columnDefs = [
        { 
            field: "Date",
            displayName: "Дата",
            width: "20%",
            cellTemplate:'<div id="checkDate"><a tooltip="{{row.getProperty(col.field)}}" tooltip-popup-delay="1000" tooltip-class="my-custom-tooltip" tooltip-placement="top" tooltip-append-to-body="true">' +
            '<datepicktable class="logus-headers col-md-2 col-xs-4 col-sm-2 col-lg-2" style="padding-top: 6px;margin-left: -22px;font-size: 15px;" data="row.entity[col.field]" mask="\'99.99.9999\'" regex-name="\'date\'" ng-class="{clicknone: row.entity[\'CheckDate\']==null}" date-change="dateChange(row.entity, curDate)">' +
            '</datepicktable>' +
            '</a></div>'
        },
        {
            field: "Plant",
            displayName: "Культура",
            width: "25%",
            cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()">' +
            '<input value="{{row.entity[col.field].Name}}" ng-model="row.entity[col.field].Name" placeholder="Культура" class="bordernone" readonly>' +
            '</div>'
        },
        {
            field: "Field", displayName: "Поле", width: "30%",
            cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()">' +
            '<input value="{{row.entity[col.field].Name}}" ng-model="row.entity[col.field].Name" placeholder="Поле" class="bordernone" readonly data-ng-click="updateSelectedCell(col.field, row.entity[col.field][\'Id\'],\'Поле\',row.entity,$event)">' +
            '</div>'
        },
        {
            field: "Count", displayName: "Количество, т", width: "25%",
            cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()">' +
            '<input value="{{row.entity[col.field]}}" ng-change="changeCount(row.entity)" ng-model="row.entity[col.field]" placeholder="0" ng-class="{bordernone: row.entity[\'Id\']}">' +
            '</div>'
        },
		  {
            field: "CheckDate", displayName: "", width: "0%",
            cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()">' +
            '<input value="{{row.entity[col.field]}}" ng-model="row.entity[col.field]" placeholder="Цена" ng-class="{bordernone: row.entity[\'Id\']}">' +
            '</div>'
        },
        {field: "Id", width: "0%"}
    ];


 var checkLoad = false;
    var loadInfo = function() {
		var OrgId = null;
		if (checkLoad == false) {
		OrgId = curUser.orgid;
		}
		else {
				OrgId = sc.OrgId;
			}
        requests.serverCall.post($http, "Products/GetGrossInfo", {Year: $stateParams.year, OrgId: OrgId}, function (data, status, headers, config) {
            console.info(data);
            sc.date = data.LastDate;
            sc.YearList = data.YearList;
            sc.PlantInfo = data.Plants;
			if (checkLoad == false) {
			sc.OrganizationsList = data.OrganizationsList;
			if (curUser.tporgid == 2) {
				sc.OrgId = -1;
			}  else {
			sc.OrgId = curUser.orgid;	
			}
			}
            sc.YearId = $stateParams.year;
            sc.PlantId = $stateParams.plant ? $stateParams.plant : 1;
			checkLoad = true;
            loadGross();
        }, function () {
        });
    };
    loadInfo();
 
 
 var firstLoad = false;
	sc.changeOrg = function() {
		if  (firstLoad == true) {
		loadInfo();
		}
		firstLoad = true;
	}
	
	
    sc.cultureChange = function() {
        $stateParams.plant = sc.PlantId;
        var newRows = getNewRows();
        if (newRows) {
            if (newRows.rows.length === 0) {
                loadGross();
            } else {
                if (newRows.complete) {
                    saveRows(newRows.rows, loadGross);
                } else {
                    saveError();
                }
            }
        }
    };
    sc.changeCount = function() {
		console.log(sc.selectedRow[0]);
	if (sc.selectedRow[0].Id) {
		 	 rowsForUpd.push({
                   rows:sc.selectedRow[0]
                });
		//  console.info(rowsForUpd);
		  console.info(rowsForUpd);
		}
	}
	sc.dateChange = function(row, curDate) {
		if (sc.selectedRow[0]!= undefined && sc.selectedRow[0].Id) {
			 rowsForUpd.push({
                   rows:sc.selectedRow[0]
                });
		  console.info(rowsForUpd);
		}
	}
    var goToYear = function() {
        var url = '/gross/' + sc.YearId + "/";
        if (sc.PlantId) {
            url += sc.PlantId;
        }
        $location.path(url);
    };

    sc.refreshYear = function() {
        var newRows = getNewRows();
        if (newRows) {
            if (newRows.rows.length === 0) {
                goToYear();
            } else {
                if (newRows.complete) {
                    saveRows(newRows.rows, goToYear);
                } else {
                    saveError();
                }
            }
        }
    };

    var loadGross = function() {
        requests.serverCall.post($http, "Products/GetGross", {
            Year: sc.YearId,
            PlantId: sc.PlantId ? sc.PlantId : null,
			OrgId : sc.OrgId
        }, function (data, status, headers, config) {
            console.log(data);
            sc.TableData = data;
        }, function () {
        });
    };
    // мультивыделение
    sc.keyDownGrid = function (e) {
            if ((e.keyCode === 16 || e.keyCode === 17 || e.keyCode === 18 || e.keyCode === 91) && !sc.multi) {
            //   console.info("__________________ keyDown  " + e.keyCode);
                sc.multi = true;
             //   console.info(sc.multi);
            }
        
    };

    sc.keyUpGrid = function (e) {
        // 16 - Shift, 17 - Ctrl, 18 - Alt, 91 - Win key (Start)
            if (e.keyCode === 16 || e.keyCode === 17 || e.keyCode === 18 || e.keyCode === 91) {
           //     console.info("_________________ keyUp " + e.keyCode);
                sc.multi = false;
              //  console.info(sc.multi);
            }
        
    };
    sc.updateSelectedCell = function (colField, selectedVal, displayName, row) {
		if (sc.selectedRow[0].Id) {
		 rowsForUpd.push({
                   rows:sc.selectedRow[0]
                });
		  console.info(rowsForUpd);
		}
      //  if (!row.Id) {
		  met = 1;	
		  if (sc.mySelections1 == row) {met = 2; 
		    for (var i = 0, li = sc.TableData.length; i<li; i++) {
				sc.TableData[i].CheckDate = null;
			}
			row.CheckDate = 1;
		  }
		  
            sc.selectedRow = [];
            sc.selectedRow.push(row);
            var typeModal = modalType.WIN_SELECT;
            var modalValues;
			if (met==2) {
            switch (colField) {
                case "Sort":
                    if (row.Plant) {
                        for (var i= 0, li = sc.PlantInfo.length; i<li; i++) {
                            if (sc.PlantInfo[i].Id === row.Plant.Id) {
                                modalValues = sc.PlantInfo[i].Values;
                            }
                        }
                    }
                    break;
                case "Field":
                    if (row.Sort) {
                        var valuesForFields;
						console.log(sc.PlantInfo);
                        for (var i= 0, li = sc.PlantInfo.length; i<li; i++) {
                            if (sc.PlantInfo[i].Id === row.Plant.Id) {
                                valuesForFields = sc.PlantInfo[i].Values;
                            }
                        }
                        for (var i= 0, li = valuesForFields; i<li; i++) {
                            if (valuesForFields[i].Id === row.Sort.Id) {
                                modalValues = valuesForFields[i].Values;
                            }
                        }
                    } else if (row.Plant){
                        valuesForFields = sc.PlantInfo;
                        for (var i= 0, li = valuesForFields.length; i<li; i++) {
                            if (valuesForFields[i].Id === row.Plant.Id) {
                                modalValues = valuesForFields[i].Fields;
                            }
                        }
                    }
                    break;
            }
	  }
            if (modalValues && modalValues.length > 0) {
                modals.showWindow(typeModal, {
                    nameField: displayName,
                    values: modalValues,
                    selectedValue: selectedVal
                })
                    .result.then(function (result) {
                        console.info(result);
                        console.info(colField);
                        sc.selectedRow[0][colField] = result;
                        if (colField === "Sort") {
                            row.Field = null;
                        }
                    }, function () {
                        console.info('Modal dismissed at: ' + new Date());
                    });
            } 
     // }
    };

    sc.add = function() {
        var pl;
        for (var i= 0, li=sc.PlantInfo.length; i<li; i++) {
            if (sc.PlantInfo[i].Id === sc.PlantId) {
                pl = sc.PlantInfo[i];
            }
        }
        var newRow = {
            Date: new Date(),
            Plant: pl,
         //   Count: 0,
         //   Price: 0,
          //  Cost: 0
        };
        sc.TableData.splice(0,0,newRow);
    };

    sc.save = function() {
        var rowsForSave = getNewRows();
        if (rowsForSave.complete) {
            saveRows(rowsForSave.rows, loadGross);
        } else {
            saveError();
        }
    };
	//////////////////скопировать
	  sc.copyRow = function() {
		     var pl;
        for (var i= 0, li = sc.PlantInfo.length; i<li; i++) {
            if (sc.PlantInfo[i].Id === sc.PlantId) {
                pl = sc.PlantInfo[i];
            }
        }
			var newGross = {
            Date: sc.selectedRow[0].Date,
            Plant: pl,
			Field : {Id:sc.selectedRow[0].Field.Id, Name: sc.selectedRow[0].Field.Name},
            Count: sc.selectedRow[0].Count,
        };
        sc.TableData.splice(0, 0, newGross);     
	  }
	
	
     //назад 
	   sc.back = function() {
        var url = '/product';
        $location.path(url);
    };
	    // модальное окно с требованием выделить строку
    function needSelection(title, message) {
        modals.showWindow(modalType.WIN_MESSAGE, {
            title: title,
            message: message,
            callback: function () {
            }
        });
    }
	//удаление строки
	   sc.delRow = function() {
		   console.log(sc.selectedRow);
		    if (sc.selectedRow.length!=0) {
            modals.showWindow(modalType.WIN_DIALOG, {
                    title: "Удаление", message: "Удалить выбранный документ?", callback: function () {
						for (var i=0; i<sc.selectedRow.length;i++) {
						if (sc.selectedRow[i].Id==undefined) {
					    sc.TableData.splice(sc.TableData.indexOf(sc.selectedRow[i]), 1);
							}
						else {
				 rowsForDel.push({
                   rows:sc.selectedRow[i]
                });		
				  sc.TableData.splice(sc.TableData.indexOf(sc.selectedRow[i]), 1);
						}
						}
							console.log(rowsForDel);
							
                        requests.serverCall.post($http, "Products/DeleteGross", rowsForDel, function (data, status, headers, config) {
							rowsForDel = [];
					  // loadGross();
                            console.info(data);
                        });
						;}
                    }
                
            );
        } else needSelection('Удаление документа', "Для удаления необходимо выбрать документ");
	   }
	
    var saveError = function() {
        modals.showWindow(modalType.WIN_ERROR, {
            title: "Ошибка накладной",
            message: "Заполните новые поля или удалите незаполненные строки",
            callback: function () {
            }
        });
    };

    var saveRows = function (rowsForSave, cb) {
        requests.serverCall.post($http, "Products/AddGross", rowsForSave, function() {
            if (cb) cb();
			 requests.serverCall.post($http, "Products/UpdateGross", rowsForUpd, function (data, status, headers, config) {
					   rowsForUpd = [];
					    if (cb) cb();
					  // loadGross();
                            console.info(data);
                        })
        }, function() {
            if (cb) cb();
        });
    };

    var getNewRows = function() {
        if (sc.TableData) {
            var newRows = [];
            var complete = true;
            for (var i = 0, li = sc.TableData.length; i < li; i++) {
                if (!sc.TableData[i].Id) {
                    var r = sc.TableData[i];
                    newRows.push(sc.TableData[i]);
                    if (!r.Plant || !r.Field || !r.Count) {
                        complete = false;
                    }
                }
            }
            return {
                rows: newRows,
                complete: complete
            };
        }
    };
}]);