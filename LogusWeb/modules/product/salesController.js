/**
 *  @ngdoc controller
 *  @name Product.contorller:SalesCtrl
 *  @description
 *  # SalesCtrl
 *  Это контроллер, определеяющий поведение форму продаж продукции
 *  @requires $scope
 *  @requires $location
 *  @requires $http
 *  @requires requests

 *  @property {Object} sc scope
 *  @property {Object} sc.infoData справочники
 *  @property {Object} sc.selectedRow выделенные строки
 *  @property {Object} sc.gridOptions настройки таблицы
 *  @property {Object} sc scope
 
 *  @property {function} goToYear изменить год
 *  @property {function} sc.refreshYearSales загрзука продаж годовых
 *  @property {function} loadSales загрузить данные таблицы
 *  @property {function} sc.updateSelectedCell открытие модальных окон выбора
 *  @property {function} sc.add добавить новую запись в таблице
 *  @property {function} sc.changePrice пересчет стоимости после изменения цены
 *  @property {function} sc.save сохранить изменения
 *  @property {function} saveError обработка ошибок изменения
 *  @property {function} saveRows сохранение записей
 *  @property {function} getNewRows получение новых записей для дальнейшего сохранения
 */
var app = angular.module("Sales", ["ngGrid", "services", 'ngCookies', "modalWindows"]);
app.controller("SalesCtrl", ["$scope", "$http", "requests", "$location",'$stateParams',"modalType","modals", function ($scope, $http, requests, $location, $stateParams, modalType, modals) {
    var sc = $scope;
    sc.selectedRow = [];
    var rowsForDel = [];
	var rowsForUpd = [];
	sc.multi = false;
	var balance ;
    sc.gridOptions = {
        enableColumnResize: false,
        enableRowSelection: true,
        showFooter: false,
        rowHeight: 49,
        headerRowHeight: 49,
        useExternalSorting: false,
        data: 'TableData',
        selectedItems: sc.selectedRow,
        afterSelectionChange: function (row) {
       	for (var i = 0; i < row.length;i++ ) 
   { 
     sc.selectedRow.push(row[i].entity); 
     console.log(sc.selectedRow); 
        } 
	  if (row.entity) {
		    for (var i = 0, li = sc.TableData.length; i<li; i++) {
				sc.TableData[i].CheckDate = null;
			}
			row.entity.CheckDate = 1;
		  
		  console.log(row.selected);
                if (row.selected) {
                    if (sc.multi) sc.selectedRow.push(row.entity);
                    else {
                        sc.selectedRow = [];
                        sc.selectedRow.push(row.entity);
                    }
                } else {
                    sc.selectedRow.splice(sc.TableData.indexOf(row.entity), 1);
                }
            }
	   
        },
        beforeSelectionChange: function (row, event) {
        if (!sc.multi) {
                angular.forEach(sc.TableData, function (data, index) {
                    if (data.selected == true) sc.mySelections.splice(index, 1);
                    sc.gridOptions.selectRow(index, false);

                });
            }
		
                return true;
        }
    };

    sc.gridOptions.columnDefs = [
        {
            field: "Date",
            displayName: "Дата",
            width: "10%",
            cellTemplate:'<a tooltip="{{row.getProperty(col.field)}}" tooltip-popup-delay="1000" tooltip-class="my-custom-tooltip" tooltip-placement="top" tooltip-append-to-body="true">' +
            '<datepicktable class="logus-headers col-md-2 col-xs-4 col-sm-2 col-lg-2" style="padding-top: 6px;margin-left: -22px;font-size: 15px;" data="row.entity[col.field]" mask="\'99.99.9999\'" regex-name="\'date\'" ng-class="{clicknone: row.entity[\'CheckDate\']==null}" date-change="dateChange(row.entity, curDate)">' +
            '</datepicktable>' +
            '</a>'
        },
        {
            field: "Plant",
            displayName: "Культура",
            width: "20%",
            cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()">' +
            '<input value="{{row.entity[col.field].Name}}" ng-model="row.entity[col.field].Name" placeholder="Культура" class="bordernone" readonly>' +
            '</div>'
        },
		  {
            field: "Balance", displayName: "Остаток, т", width: "20%",
            cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()">' +
            '<input value="{{row.entity[col.field]}}" ng-model="row.entity[col.field]" placeholder="0" ng-change="changePrice(row.entity)" ng-class="{bordernone: row.entity[\'Id\']}" readonly>' +
            '</div>'
        },
        {
            field: "Count", displayName: "Количество, т", width: "15%",
            cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()">' +
            '<input value="{{row.entity[col.field]}}" ng-model="row.entity[col.field]" placeholder="0" ng-change="changePrice(row.entity)" ng-class="{bordernone: row.entity[\'Id\']}">' +
            '</div>'
        },
        {
            field: "Price", displayName: "Цена, р/т", width: "15%",
            cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()">' +
            '<input value="{{row.entity[col.field]}}" ng-model="row.entity[col.field]" placeholder="0" ng-change="changePrice(row.entity)" ng-class="{bordernone: row.entity[\'Id\']}">' +
            '</div>'
        },
        {
            field: "Cost", displayName: "Стоимость, р", width: "20%",
            cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()">' +
            '<input value="{{row.entity[col.field]}}" ng-model="row.entity[col.field]" placeholder="0" class="bordernone" readonly>' +
            '</div>'
        },
		  {
            field: "CheckDate", displayName: "", width: "0%",
            cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()">' +
            '<input value="{{row.entity[col.field]}}" ng-model="row.entity[col.field]" placeholder="Цена" ng-class="{bordernone: row.entity[\'Id\']}">' +
            '</div>'
        },
        {field: "Id", width: "0%"},
		 {field: "Balance2", width: "0%"},
    ];



    var loadInfo = function() {
        requests.serverCall.post($http, "Products/GetGrossInfo", {Year: $stateParams.year}, function (data, status, headers, config) {
            console.info(data);
            sc.date = data.LastDate;
            sc.YearList = data.YearList;
            sc.PlantInfo = data.Plants;
            sc.YearId = $stateParams.year;
            sc.PlantId = $stateParams.plant ? $stateParams.plant : 1;
            loadSales();
        }, function () {
        });
    };
    loadInfo();

	
	sc.dateChange = function(row, curDate) {	
		  console.info(row);
        if (sc.selectedRow[0] != undefined && sc.selectedRow[0].Id) {
			 rowsForUpd.push({
                   rows:sc.selectedRow[0]
                });
		  console.info(rowsForUpd);
		}
		  
	}

    sc.cultureChange = function() {
        $stateParams.plant = sc.PlantId;
        var newRows = getNewRows();
        if (newRows) {
            if (newRows.rows.length === 0) {
                loadSales();
            } else {
                if (newRows.complete) {
                    saveRows(newRows.rows, loadSales);
                } else {
                    saveError();
                }
            }
        }
    };

    var goToYear = function() {
        var url = '/sales/' + sc.YearId + "/";
        if (sc.PlantId) {
            url += sc.PlantId;
        }
        $location.path(url);
    };

    sc.refreshYearSales = function() {
        var newRows = getNewRows();
        if (newRows) {
            if (newRows.rows.length === 0) {
                goToYear();
            } else {
                if (newRows.complete) {
                    saveRows(newRows.rows, goToYear);
                } else {
                    saveError();
                }
            }
        }
    };

    var loadSales = function() {
        requests.serverCall.post($http, "Products/GetSales", {
            Year: sc.YearId,
            PlantId: sc.PlantId ? sc.PlantId : null
        }, function (data, status, headers, config) {
			if (data.length!=0) {
            console.log(data);
			balance = data.Balance;
			}
            sc.TableData = data.res;
        }, function () {
        });
    };
    // мультивыделение
    sc.keyDownGrid = function (e) {
            if ((e.keyCode === 16 || e.keyCode === 17 || e.keyCode === 18 || e.keyCode === 91) && !sc.multi) {
            //   console.info("__________________ keyDown  " + e.keyCode);
                sc.multi = true;
             //   console.info(sc.multi);
            }
        
    };

    sc.keyUpGrid = function (e) {
        // 16 - Shift, 17 - Ctrl, 18 - Alt, 91 - Win key (Start)
            if (e.keyCode === 16 || e.keyCode === 17 || e.keyCode === 18 || e.keyCode === 91) {
           //     console.info("_________________ keyUp " + e.keyCode);
                sc.multi = false;
              //  console.info(sc.multi);
            }
        
    };
    sc.updateSelectedCell = function (colField, selectedVal, displayName, row) {
        if (sc.selectedRow[0].Id) {
			 rowsForUpd.push({
                   rows:sc.selectedRow[0]
                });
		  console.info(rowsForUpd);
		}
		if (!row.Id) {
            sc.selectedRow = [];
            sc.selectedRow.push(row);
            var typeModal = modalType.WIN_SELECT;
            var modalValues;
            switch (colField) {
                case "Sort":
                    if (row.Plant) {
                        for (var i= 0, li = sc.PlantInfo.length; i<li; i++) {
                            if (sc.PlantInfo[i].Id === row.Plant.Id) {
                                modalValues = sc.PlantInfo[i].Values;
                            }
                        }
                    }
                    break;
            }
            if (modalValues && modalValues.length > 0) {
                modals.showWindow(typeModal, {
                    nameField: displayName,
                    values: modalValues,
                    selectedValue: selectedVal
                })
                    .result.then(function (result) {
                        console.info(result);
                        console.info(colField);
                        sc.selectedRow[0][colField] = result;
                    }, function () {
                        console.info('Modal dismissed at: ' + new Date());
                    });
            } 
        }
    };

    sc.add = function() {
        var pl;
        for (var i = 0, li = sc.PlantInfo.length; i<li; i++) {
            if (sc.PlantInfo[i].Id === sc.PlantId) {
                pl = sc.PlantInfo[i];
            }
        }
        var newRow = {
            Date: new Date(),
            Plant: pl,
        };
        sc.TableData.splice(0,0,newRow);
		 sc.recountBalance();
    };

    sc.changePrice = function(row) {
        if (row.Price && row.Count) {
            row.Cost = (row.Price * row.Count).toFixed(2);
        } else {
            row.Cost = 0;
        }
		if (sc.selectedRow[0] != undefined && sc.selectedRow[0].Id) {
			 rowsForUpd.push({
                   rows:sc.selectedRow[0]
                });
		  console.info(rowsForUpd);
		}
	     sc.recountBalance();
    };
	
	 sc.recountBalance = function() {
		var total = 0;
		 		for (var j = 0; j < sc.TableData.length; j++) {
					var count = parseInt(sc.TableData[j].Count);
					if (!isNaN(count)) {
			       total = total + count ;
					}
					  }
		 		 for (var i = 0; i < sc.TableData.length; i++) {
					 sc.TableData[i].Balance = balance - total;
		 }
	 }
	
	
	
   //назад 
	   sc.back = function() {
        var url = '/product';
        $location.path(url);
    };
		//////////////////скопировать
	  sc.copyRow = function() {
		     var pl;
        for (var i= 0, li=sc.PlantInfo.length; i<li; i++) {
            if (sc.PlantInfo[i].Id === sc.PlantId) {
                pl = sc.PlantInfo[i];
            }
        }

			var newSales = {
            Date: sc.selectedRow[0].Date,
            Plant: pl,
            Count: sc.selectedRow[0].Count,
			Price : sc.selectedRow[0].Price,
			Cost : sc.selectedRow[0].Cost,
			Balance2 : sc.selectedRow[0].Balance2,
        };
        sc.TableData.splice(0, 0, newSales);  
         sc.recountBalance();		
	  }
	  	    // модальное окно с требованием выделить строку
    function needSelection(title, message) {
        modals.showWindow(modalType.WIN_MESSAGE, {
            title: title,
            message: message,
            callback: function () {
            }
        });
    }
	//удаление строки
	   sc.delRow = function() {
		   console.log(sc.selectedRow);
		    if (sc.selectedRow.length!=0) {
            modals.showWindow(modalType.WIN_DIALOG, {
                    title: "Удаление", message: "Удалить выбранный документ?", callback: function () {
						for (var i=0; i<sc.selectedRow.length;i++) {
						if (sc.selectedRow[i].Id==undefined) {
					    sc.TableData.splice(sc.TableData.indexOf(sc.selectedRow[i]), 1);
									 sc.recountBalance();
							}
						else {
				 rowsForDel.push({
                   rows:sc.selectedRow[i]
                });		
				 sc.TableData.splice(sc.TableData.indexOf(sc.selectedRow[i]), 1);
						}
						}
							console.log(rowsForDel);	
                        requests.serverCall.post($http, "Products/DeleteSales", rowsForDel, function (data, status, headers, config) {
							rowsForDel = [];
				     		 sc.recountBalance();
                            console.info(data);
                        });
						;}
                    });
        } else needSelection('Удаление документа', "Для удаления необходимо выбрать документ");
	   }
	  
	  //сохранить
    sc.save = function() {
        var rowsForSave = getNewRows();
        if (rowsForSave.complete) {
            saveRows(rowsForSave.rows, loadSales);
        } else {
            saveError();
        }
    };

    var saveError = function() {
        modals.showWindow(modalType.WIN_ERROR, {
            title: "Ошибка накладной",
            message: "Заполните новые поля или удалите незаполненные строки",
            callback: function () {
            }
        });
    };

    var saveRows = function (rowsForSave, cb) {
		console.log(rowsForSave);
		var n = 0;
		//если превышено, то выводим сообщение
	 for (var i = 0, li = sc.TableData.length; i < li; i++) {
		 n = n + parseInt(sc.TableData[i].Count);
		 }
		if (n > balance && n!=0) {
         //
		modals.showWindow(modalType.WIN_ERROR, {
            title: "Предупреждение",
            message: "Превышен остаток. Баланс: "+balance+", Количество: "+n+".",
            callback: function () {
            }
        });
	   console.log(sc.TableData);
	   rowsForSave = [];	  
		}  else {
        requests.serverCall.post($http, "Products/AddSales", rowsForSave, function() {
            if (cb) cb();
			//обновление
			 requests.serverCall.post($http, "Products/UpdateSales", rowsForUpd, function (data, status, headers, config) {
					   rowsForUpd = [];
					    if (cb) cb();
                            console.info(data);
                        })
						
        }, function() {
            if (cb) cb();
        });
		//
		;}
    };

    var getNewRows = function() {
        if (sc.TableData) {
            var newRows = [];
            var complete = true;
            for (var i = 0, li = sc.TableData.length; i < li; i++) {
                if (!sc.TableData[i].Id) {
                    var r = sc.TableData[i];
                    newRows.push(sc.TableData[i]);
                    if (!r.Plant || !r.Count || !r.Price) {
                        complete = false;
                    }
                }
            }
            return {
                rows: newRows,
                complete: complete
            };
        }
    };
}]);