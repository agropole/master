/**
 *  @ngdoc controller
 *  @name app.controller:MainCtrl
 *  @description
 *  # MainCtrl
 *  Это контроллер, определеяющий поведение главной формы. Главная форма отображается не во всех ролях
 *  @requires $scope
 *  @requires $location
 *  @requires $cookieStore
 *  @requires LogusConfig

 *  @property {Object} sc scope
 *  @property {Object} sc.userName имя пользователя
 *  @property {Object} sc.settingsUpload параметры отправки файла на сервер
 
 
 *  @property {function} sc.clickRole выбор роли
 *  @property {function} sc.fileInputChanged событие выбора файла
 *  @property {function} prepareFile прикрепление файла
 *  @property {function} sendToServer отправка файла
 */
var app = angular.module("Main", ["services", 'ngCookies', "modalWindows", "LogusConfig"]);
app.controller("MainCtrl", ["$scope", "$location", '$rootScope', "LogusConfig", "requests","$http", function ($scope, $location, $rootScope, LogusConfig,requests,$http) {
    var sc = $scope;
    sc.userName = $rootScope.userName;
    sc.clickRole = function(role) {
        $rootScope.setRole(role);
    };

    sc.settingsUpload = {};
    sc.settingsUpload.typeFileShort = 'xml';
    sc.fileInputChanged = function () {
        if (document.getElementById('file-input').value.lastIndexOf(sc.settingsUpload.typeFileShort) === -1) {
            alert('Only XML files are allowed!');
            document.getElementById('file-input').value = '';
            return;
        }
        var fileInput = document.getElementById('file-input')
        //берем массив всех файлов
        sc.files = fileInput.files;
        console.info(sc.files);
        prepareFile(sc.files[0]);
    };

    var prepareFile = function (file) {
        var data = new FormData();
        data.append("file", file);
        sendToServer(data);
    };

    var sendToServer = function (d) {
        console.info(d);
        $.ajax({
            type: "POST",
            url: LogusConfig.serverUrl + 'ParseXMLController/ParseGrossHarvest',//ParseMaterial
            contentType: false,
            processData: false,
            data: d,
            success: function (result) {
                console.log(result);
            },
            error: function (xhr, status, p3, p4) {
            }
        });
    };
        //'/REST/ParseXml',
   //requests.serverCall.get($http, "GPSCtrl/StartParseGps", function (data, status, headers, config) {
   //     console.info(data);
   // }, function () {
   // });
	

	/*
var arr = [
           { 
            req_id: 23232323, // id координаты 
            time : "2016-02-20 12:06:53", // дата и время координаты
			track_id : 37006, // id техники
			direction : 324.58,  // 3 знака до запятой  и 2 после 
			speed : 324.58,    // 3 знака до запятой  и 2 после 
			mileage : 324.58,  // 3 знака до запятой  и 2 после 
			lat : 39.69654,   //широта 
			lon : 51.79199   //долгота
            }, {
			 req_id: 23232323, // id координаты 
            time : "2016-02-20 12:06:53", // дата и время координаты
			track_id : 37006, // id техники
			direction : 324.58,  // 3 знака до запятой  и 2 после 
			speed : 324.58,    // 3 знака до запятой  и 2 после 
			mileage : 324.58,  // 3 знака до запятой  и 2 после 
			lat : 39.69654,   //широта 
			lon : 51.79199   //долгота
            }];
var arr = 
           
{"status":3,"task_id":22548,"time_begin_str":"2017-02-16 10:45:49","time_end_str":"2017-02-16 10:45:55"};
           
			console.log(JSON.stringify(arr));
			
			
	/	
	var  year = 2017 ;
	
	$.ajax({ 
    url: LogusConfig.serverUrl + 'AgronomArmCtrl/GetFieldsToPlants', 
	//  url: 'http://localhost:19663/api/GPSCtrl/SaveGpsDataToDB', 
      type: 'POST', 
      contentType: "application/json;charset=utf-8",
      data : parseInt(2017), //JSON.stringify(arr),

     success: function (data) { 
     console.log(data); 
     }, 

error: function () { 
console.log("Связь с сервером была потеряна!"); 
} 
});
	//*/
	
	
	
	
	
	
	
	
	
}]);


