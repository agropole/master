/**
 *  @ngdoc controller
 *  @name app.controller:ReportsCtrl
 *  @description
 *  # ReportsCtrl
 *  Это контроллер, определеяющий поведение формы отчетов
 *  @requires $scope
 *  @requires $http
 *  @requires requests
 *  @requires modalType
 *  @requires modals
 *  @requires LogusConfig

 *  @property {Object} sc scope
 *  @property {Object} infoData справочники
 *  @property {Object} formData данные формы
 *  @property {string} activeTab выбранная вкладка отчета
 *  @property {Array} monthList справочник месяцев
 *  @property {int} currentYear выбранный год
 *  @property {Array} yearsList справочник годов

 *  @property {function} sc.setTab выбор новой складки
 *  @property {function} sc.submitForm выгрузка отчета в новом окне
 *  @property {function} sc.traktorChange выбор единицы техники
 */

var app = angular.module("Reports", ["services", 'ngCookies', "modalWindows", "LogusConfig"]);
app.controller("ReportsCtrl", ["$scope", "$http", "requests", "modals", "modalType", "LogusConfig", function ($scope, $http, requests, modals, modalType, LogusConfig) {
    var sc = $scope;
    sc.formData = [];
	sc.infoData2 = [];
	sc.infoData3 = [];
	sc.infoData4 = [];
	sc.infoData5 = [];
    sc.activeTab = '';
    sc.monthList = [{Id: 1, Name: "Январь"}, {Id: 2, Name: "Февраль"}, {Id: 3, Name: "Март"}, {
        Id: 4,
        Name: "Апрель"
    }, {Id: 5, Name: "Май"}, {Id: 6, Name: "Июнь"}, {Id: 7, Name: "Июль"}, {Id: 8, Name: "Август"}, {
        Id: 9,
        Name: "Сентябрь"
    }, {Id: 10, Name: "Октябрь"}, {Id: 11, Name: "Ноябрь"}, {Id: 12, Name: "Декабрь"}];
    sc.currentYear = new Date().getFullYear()+1;
    sc.yearsList = [];
    sc.formData.yearId = sc.currentYear-1;
    sc.formData.monthId = new Date().getMonth() + 1;
    sc.formData.IsBig1 = true;
    sc.formData.Plan = true;
	sc.formData.Fact = true;
	sc.formData.Detal = false;
	sc.formData.sheet = false;
	sc.formData.timeIsBig = false;
	sc.formData.IsSimple = false;
	$("#btn").show();
	var now = new Date();
	//sc.formData.docStartDate = moment(now).format("MM.DD.YYYY");
	//sc.formData.docEndDate = moment(now).format("MM.DD.YYYY");
	sc.formData.docStartDate = now;
	sc.formData.docEndDate = now;
    for (var i = 0; i < 20; i++) {
        sc.yearsList.push({Id: (sc.currentYear - i), Name: '' + (sc.currentYear - i) + ''});
    }
    ;
    console.info(sc.yearsList);
    //устанавливаем даты в период по умолчанию (начало - 1 день текущего месяца, окончание - сегодня)
    sc.formData.machineStartDate = sc.formData.driverStartDate = sc.formData.refuelStartDate = moment().startOf('month').utc().toString("MM.DD.YYYY");

	//sc.formData.exportStartDate = moment().startOf('month').utc().toString("MM.DD.YYYY");
	sc.formData.exportStartDate = new Date(now.getFullYear(),0,1);
    sc.formData.exportEndDate = new Date();
	
	   GetReportsInfo = function (StartDate, EndDate ) {
    requests.serverCall.post($http, "ReportsCtrl/GetReportsInfo", {StartDate: StartDate, EndDate: EndDate }, function (data, status, headers, config) {
        console.info(data);
        sc.infoData = data;
        sc.infoData.exportList =  [{"Name":"Механизаторы","Id":1},{"Name":"Водители","Id":2}];
		sc.infoData.positionList =  [{"Name":"Все","Id":3},{"Name":"Механизаторы","Id":1},{"Name":"Водители","Id":2}];
      	sc.infoData.ConsPlants = Object.values(sc.infoData.AllPlants);

	     console.log(sc.infoData.ConsPlants);
	     console.log(sc.infoData.AllPlants);
	     sc.formData.employeerId = -1;
	     sc.formData.FieldId = -1;
	     sc.formData.PlantId = -1;
		 sc.formData.materialId = -1 ;
		 sc.infoData5.TmcList = sc.infoData.TmcList;
		  Journal();
   });
	   }
	   
	   	function loadSectionTech()  {	
		    requests.serverCall.get($http, "AdministrationCtrl/GetSectionTechcard", function (data, status, headers, config) {
			console.log(data);
		    $("#nzp").empty();
			 sc.sectionTechList = [];
			  sc.sectionTechList.push({Id: -1, Name: "Не выбрано"});
			  sc.formData.dateId = -1;
            for (var i = 0; i < data.length; i++) {
            sc.sectionTechList.push({Id: (data[i]), Name: data[i]});
			 } 
				console.log(sc.sectionTechList); 
        }, function () {
        });
          };
		  
	   loadSectionTech();
	   
	  
	   GetReportsInfo(sc.formData.exportStartDate, sc.formData.exportEndDate);
	   
    getResp = function () {
		console.log(sc.formData.docStartDate);
     requests.serverCall.post($http, "ReportsCtrl/GetResp",{
            StartDate:  sc.formData.docStartDate, 
			EndDate: sc.formData.docEndDate
        }, function (data, status, headers, config) {
            console.info(data[0].ResponsibleList);
         if (data.length != 0)  { sc.infoData.responsibleList = data[0].ResponsibleList;}
        }, function () {
        });
	}
	getResp();
		
   sc.getStartDate = function (data) {
	   getResp();
   };
      sc.getEndDate = function (data) {
	  getResp();
   };
   
    getRespSal = function () {
        requests.serverCall.post($http, "CostCtrl/GetRespSheetsEmployees", { 
            start:  sc.formData.timeStartDate, 
			end: sc.formData.timeEndDate
        }, 
		function (data, status, headers, config) 
		{
			sc.infoData.responsibleListSal = data;
			}, function () {
        });
	}
   
   //отчет ЗП
      sc.getStartDateSal = function (data) {
	   getRespSal();
   };
      sc.getEndDateSal = function (data) {
	  getRespSal();
   };
   
   
   //по себестоимости
    sc.getCultStartDate = function (data) {
    checkPlanFactDetal();
   };
      sc.getCultEndDate = function (data) {
    checkPlanFactDetal();
   };
   
     sc.checkPlan = function () {
	  checkPlanFactDetal();

   }
   sc.checkFact = function () {
	 checkPlanFactDetal();
           
   }
   
   sc.getConsStartDate = function () {
	   var start = new Date(sc.formData.consStartDate).getFullYear();
	   var end = new Date(sc.formData.consEndDate).getFullYear();
       if (start != end) {   
	   $("#culture").addClass("disabledbutton");
	   $("#typeTMC").addClass("disabledbutton");
	   $("#loadReport").addClass("disabledbutton");
   $("#ConDate").children("div").children("div").children("p").children("input").addClass("errorinput");
	   } else {
		$("#culture").removeClass("disabledbutton");
	   $("#typeTMC").removeClass("disabledbutton");
	   $("#loadReport").removeClass("disabledbutton") ;   
	 $("#ConDate").children("div").children("div").children("p").children("input").removeClass("errorinput"); 
	   }
	   
	   GetReportsInfo(sc.formData.consStartDate, sc.formData.exportEndDate);  
   }
   
      sc.getWriteOffDate = function () {
	   GetReportsInfo(sc.formData.exportStartDate, sc.formData.exportEndDate); 
   }
   
    checkPlanFactDetal = function () {
		var start = new Date(sc.formData.yearId, 0, 1, 0, 0, 0, 0);
		var end = new Date(sc.formData.yearId, 11, 31, 0, 0, 0, 0);
    if ((sc.formData.Fact && sc.formData.Plan) || ((sc.formData.pfStartDate.valueOf() != start.valueOf() || sc.formData.pfEndDate.valueOf() != end.valueOf())))
	   { 
       $("#detal").addClass("disabledbutton")  
	   sc.formData.Detal = false;
	   }
	   else {  $("#detal").removeClass("disabledbutton") }
	 }
	 
   sc.checkJournal = function() { 
 	Journal();}
	
Journal = function() {
	if(sc.formData.Journal != false)
	{sc.formData.Journal = true;
     sc.infoData.TmcList = [{"Name": "СЗР", "Id": 1}, {"Name":"Мин. удобрения", "Id":2}];
    }
	else {sc.formData.Journal = false; 
	      sc.infoData.TmcList = sc.infoData5.TmcList;}
}

   //////////планируемые затраты
    sc.refreshYear = function () {
    requests.serverCall.post($http, "ReportsCtrl/GetCulture",{
            Year:  sc.formData.yearId, 
        }, function (data, status, headers, config) {
            console.info(data[0].CultList);
         if (data.length!=0)  { sc.infoData3.cultList=data[0].CultList;}
        }, function () {
        });
	 }
   //по себестоимости 
   sc.refreshYearPlanFact = function () {
   sc.formData.pfStartDate =  new Date(sc.formData.yearId, 0, 1, 0, 0, 0, 0);
   sc.formData.pfEndDate = new Date(sc.formData.yearId, 11, 31, 0, 0, 0, 0); 
   requests.serverCall.post($http, "ReportsCtrl/GetPlanFactCult",{
            Year:  sc.formData.yearId, 
        }, function (data, status, headers, config) {
            console.info(data);
		  console.info(data[0].CultList);
         if (data[0].CultList != undefined)  { sc.infoData2.cultPlanFactList = data[0].CultList;}
        }, function () {
        });
	   
   }
   
   
     sc.bigReport = function () {
    console.log(sc.formData.IsBig);
	 }
   
    sc.setTab = function (tab) {
        sc.activeTab = tab;
    };

    sc.submitForm = function () {
        switch (sc.activeTab) {
            case "machineOperators":
                var dateStart = angular.copy(sc.formData.machineStartDate),
                    dateEnd = angular.copy(sc.formData.machineEndDate);
                var popupWin = window.open(LogusConfig.serverUrl + 'ReportsCtrl/GetWayListExportExcel?orgId=' + sc.formData.OrgMachineOperatorsId 
                                                                                              + '&startDate=' + moment(dateStart).format("MM.DD.YYYY") 
                                                                                              + '&endDate=' + moment(dateEnd).format("MM.DD.YYYY"), '_blank');
                break;
            case "drivers":
                var dateStart = angular.copy(sc.formData.driverStartDate),
                    dateEnd = angular.copy(sc.formData.driverEndDate);
                var popupWin = window.open(LogusConfig.serverUrl + 'ReportsCtrl/GetDriverWayListExportExcel?orgId=' + sc.formData.OrgDriversId 
                                                                                                    + '&startDate=' + moment(dateStart).format("MM.DD.YYYY") 
                                                                                                    + '&endDate=' + moment(dateEnd).format("MM.DD.YYYY"), '_blank');
                break;
            case "refuel":
                var dateStart = angular.copy(sc.formData.refuelStartDate),
                    dateEnd = angular.copy(sc.formData.refuelEndDate);
                var popupWin = window.open(LogusConfig.serverUrl + 'ReportsCtrl/GetRefuilngsExportExcel?TraktorId=' + sc.formData.TraktorId 
                                                                                                    + '&startDate=' + moment(dateStart).format("MM.DD.YYYY") 
                                                                                                    + '&endDate=' + moment(dateEnd).format("MM.DD.YYYY"), '_blank');
                break;
				case "time":
		     var number = 2;
		    if (sc.formData.sheet) {
			  number = 3;
		     }
				var dateStart = angular.copy(sc.formData.timeStartDate),
                    dateEnd = angular.copy(sc.formData.timeEndDate);
                var popupWin = window.open(LogusConfig.serverUrl + 'CostCtrl/GetRepairsSpendingsExcel?emp_id=' + sc.formData.timeEmpId + 
																									'&start=' + moment(dateStart).format("MM.DD.YYYY") + 
																									'&end=' + moment(dateEnd).format("MM.DD.YYYY") +
																									'&is_big=' + sc.formData.timeIsBig +  
                  																					'&isSheet=' + number ,'_blank');																							 
                break;
				
            case "table":
			   var dateStart = angular.copy(sc.formData.tableStartDate);
                var dateEnd = angular.copy(sc.formData.tableEndDate);
				 if (sc.formData.tableStartDate.getMonth() == sc.formData.tableEndDate.getMonth()) {
               var popupWin = window.open(LogusConfig.serverUrl + 'CostCtrl/GetDailyEmployeesWortTimeExpExl?startDate=' + moment(dateStart).format("MM.DD.YYYY") 
	                                                                                  + '&position=' + sc.formData.positionId
                                                                                      + '&endDate=' + moment(dateEnd).format("MM.DD.YYYY"), '_blank');																							  
				 } else {
					    modals.showWindow(modalType.WIN_MESSAGE, {
                              title: "Предупреждение",
                              message: "Неверно задан период выгрузки",
                              callback: function () {
                              } 
                          });
				 }
				         break;
				
				  case "writeoff":
				  console.log(sc.formData.materialId);
				    var dateStart = angular.copy(sc.formData.exportStartDate),
                    dateEnd = angular.copy(sc.formData.exportEndDate);
					
					
					if (sc.formData.Journal != false)
					{
                var popupWin = window.open(LogusConfig.serverUrl + 'CostCtrl/GetJournalExportExcel?startDate=' + moment(dateStart).format("MM.DD.YYYY") 
	                                                                                              + '&endDate=' + moment(dateEnd).format("MM.DD.YYYY") 
																								  + '&employeerId=' + sc.formData.employeerId
																								  + '&fieldId=' + sc.formData.FieldId
																								  + '&plantId=' + sc.formData.PlantId
																								  + '&materialId=' + sc.formData.materialId, '_blank');		
					}
					else {var popupWin = window.open(LogusConfig.serverUrl + 'ReportsCtrl/GetWriteOffExportExcel?employeerId=' + sc.formData.employeerId
                                                                                                             + '&fieldId=' + sc.formData.FieldId
																											 + '&plantId=' + sc.formData.PlantId
																											 + '&materialId=' + sc.formData.materialId
																											 + '&startDate=' + moment(dateStart).format("MM.DD.YYYY")	
																											 + '&endDate=' + moment(dateEnd).format("MM.DD.YYYY"), '_blank');
						
						}	 				
                break;
				case "respo":
                var popupWin = window.open(LogusConfig.serverUrl + 'ReportsCtrl/GetResponsablesExcel?employeerId=' + sc.formData.employeerId , '_blank');
                break; 
            case "trucks":
                var dateStart = angular.copy(sc.formData.trucksStartDate),
                    dateEnd = angular.copy(sc.formData.trucksEndDate);
                var popupWin = window.open(LogusConfig.serverUrl + 'ReportsCtrl/GetReportTrucksExcel?startDate=' + moment(dateStart).format("MM.DD.YYYY") 
                                                                                                    + '&endDate=' + moment(dateEnd).format("MM.DD.YYYY"), '_blank');
                break;
            case "export":
                var dateStart = angular.copy(sc.formData.exportStartDate),
                    dateEnd = angular.copy(sc.formData.exportEndDate);
                var popupWin = window.open(LogusConfig.serverUrl + 'Export/ExportXML?startDate=' + moment(dateStart).format("MM.DD.YYYY") 
                                                                                                    + '&endDate=' + moment(dateEnd).format("MM.DD.YYYY")
												    + '&exportType=' + sc.formData.exportId , '_blank');
                break;
            case "doc":
				    var dateStart = angular.copy(sc.formData.docStartDate),
                    dateEnd = angular.copy(sc.formData.docEndDate);
                var popupWin = window.open(LogusConfig.serverUrl + 'ReportsCtrl/GetDocResExcel?empId=' + sc.formData.empId
																											 + '&startDate=' + moment(dateStart).format("MM.DD.YYYY")
																											 + '&check=' + sc.formData.IsBig
																											 + '&endDate=' + moment(dateEnd).format("MM.DD.YYYY"), '_blank');																							 
                break;
				case "plan":
                var popupWin = window.open(LogusConfig.serverUrl + 'ReportsCtrl/GetCultResExcel?cultId=' + sc.formData.cultId
				                                                           + '&year=' + sc.formData.yearId, '_blank');																							 
                break;
				  case "rate":
                var popupWin = window.open(LogusConfig.serverUrl + 'CostCtrl/GetCostResExcel', '_blank');																							 
                break;
				
			case "cons":
				    var dateStart = angular.copy(sc.formData.consStartDate),
                    dateEnd = angular.copy(sc.formData.consEndDate);
                var popupWin = window.open(LogusConfig.serverUrl + 'CostCtrl/GetConsumptionExcel?startDate=' + moment(dateStart).format("MM.DD.YYYY") 
                                                                                                    + '&endDate=' + moment(dateEnd).format("MM.DD.YYYY")
																										+ '&check=' + sc.formData.IsBig1
																									+ '&matId=' + sc.formData.materialId	
																									+ '&PlantId=' + sc.formData.PlantId, '_blank');
																									break;
			 case "planChartCult":
              var popupWin = window.open(LogusConfig.serverUrl + 'CostCtrl/GetPlanChartCultResExcel?cultId=' + sc.formData.cultId
				                                                                       + '&year=' + sc.formData.yearId, '_blank');																							 
                break;
			 
			case "planfact":
			console.log(sc.formData.dateId); 
				    var dateStart = angular.copy(sc.formData.pfStartDate),
                    dateEnd = angular.copy(sc.formData.pfEndDate);
					console.log(dateEnd);
					console.log(moment(dateEnd).format("MM.DD.YYYY"));
					if (sc.formData.Plan && sc.formData.Fact)
					{
					 var popupWin = window.open(LogusConfig.serverUrl + 'CostCtrl/GetPlanFactDeviationExcel?startDate=' + moment(dateStart).format("MM.DD.YYYY") 
                                                                                                    + '&endDate=' + moment(dateEnd).format("MM.DD.YYYY")
																									+ '&plantId=' + sc.formData.cultPfId
																									+ '&selDate=' + sc.formData.dateId
																									, '_blank');	
						
					}
					else {
                var popupWin = window.open(LogusConfig.serverUrl + 'ReportsCtrl/GetPlanFactExcel?startDate=' + moment(dateStart).format("MM.DD.YYYY") 
                                                                                                    + '&endDate=' + moment(dateEnd).format("MM.DD.YYYY")
																									+ '&plantId=' + sc.formData.cultPfId
																								    + '&year=' + sc.formData.yearId
																									+ '&plan=' + sc.formData.Plan
																								    + '&fact=' + sc.formData.Fact
																								    + '&detal=' + sc.formData.Detal
																									+ '&date=' + sc.formData.dateId, '_blank');
					}
																									break;	
				 case "nzp":
              var popupWin = window.open(LogusConfig.serverUrl + 'CostCtrl/GetNZPResExcel?year=' + sc.formData.yearId2 
			                                                                                     + '&extend='+sc.formData.IsBigNzp
			                                                                                     + '&simple='+sc.formData.IsSimple
			  , '_blank');																							 
                break;
				
        }
    };

    sc.cleanParam = function() {
       sc.formData.employeerId = -1;
	   sc.formData.FieldId = -1;
	   sc.formData.PlantId = -1;
	   sc.formData.materialId = -1;
    };


    sc.traktorChange = function(item) {
        var typeFuelSel = item.Name_fuel;
        sc.formData.TypeFuel = typeFuelSel.Id;
    };
}]);