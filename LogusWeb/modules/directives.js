/**
 * Created by alinashipilova on 15.04.15.
 */
(function () {
    var app = angular.module("Directives", ["ui.utils", 'ngSanitize', 'ui.select', 'DoubleList']);

    var validationFunc = function (reg, value, scope) {
        var valid = value ? (value.search(reg) > -1) : false;
        scope.fvalid = valid;
        return !valid;
    };

    var CUSTOM_FILTERS = {
        numbersOnly: /^\d+$/,
        numbersWithDot: /^\d*(\.\d+)?$/,
        timeHHmm: /^((0|_)(\d|_)|(1|_)(\d|_)|(2|_)([0-3]|_)):([0-5]|_)(\d|_)$/,
        numbersWithPoint: /^\d+(\.\d+)?$/
    };

    app.directive('dropdown', function () {
        return {
            restrict: "EA",
            scope: {
                info: '=info',
                drop: '=drop',
                data: '=data',
                fieldChange: '&',
                regexName: '@regexName',
                validErrorText: "=validErrortext"
            },
            link: function (scope) {
                scope.$watch("data", function (data) {
                    if (data) {
                        if (scope.fieldChange) scope.fieldChange(data)
                    }
                });
                scope.getDropStyle = function (d) {
                    if (d.Bold != undefined) {
                        if (d.Bold == true) {
                            return {color: "#008c83"};
                        } else {
                            return {color: "#5a5a5a"};
                        }
                    } else {
                        return {};
                    }
                }
            },
            controller: function ($scope, $element) {
                disableAutoClose = [function () {
                    return {
                        link: function ($scope, $element) {
                            $element.on('click', function ($event) {
                                console.log("Dropdown should not close");
                                $event.stopPropagation();
                            });
                        }
                    };
                }];
            },
            template: '<div class="logus-text" >{{info}}</div>' +
            '<div class="select-style-dr"><select class="logus-input logus-drop" ng-model="data" ui-select2="{allowClear: true,  closeOnSelect: true}">' +
            '<option ng-style="getDropStyle(d)" ng-repeat="d in drop | filter: data" value="{{d.Id}}">{{d.Name}}</option>' +
            '</select></div>'
        };
    });
    app.directive('dropdowntf', function () {
        return {
            restrict: "E",
            scope: {
                info: '=info',
                infotf: '=infotf',
                drop: '=drop',
                data: '=data',
                regexName: '@regexName',
                validErrorText: "=validErrortext"
            },
            link: function (scope) {
                scope.$watch("data", function (data) {
                    if (data) {
                        console.info(data);
                        if (scope.drop) {
                            for (var i = 0, il = scope.drop.length; i < il; i++) {
                                if (scope.drop[i].Id == data) {
                                    scope.RegNum = scope.drop[i].RegNum;
                                }
                            }
                        }
                    }
                });
                scope.getDropStyle = function (d) {
                    if (d.Bold != undefined) {
                        if (d.Bold == true) {
                            return {color: "#008c83"};
                        } else {
                            return {color: "#5a5a5a"};
                        }
                    } else {
                        return {};
                    }
                };
                scope.$watch("RegNum", function (regNum) {
                    if (regNum) {
                        console.info(regNum);
                        if (scope.drop) {
                            for (var i = 0, il = scope.drop.length; i < il; i++) {
                                if (scope.drop[i].RegNum == regNum) {
                                    console.info(scope.drop[i].Name);
                                }
                            }
                        }
                    }
                })
            },
            template: '<div class="row">' +
            '<div class="col-md-8 col-xs-10 col-sm-8 col-lg-8 equipment">' +
            '<div class="logus-text" >{{info}}</div>' +
            '<div class="select-style"><select class="logus-input logus-drop" ng-model="data" ui-select2="{allowClear: true,  closeOnSelect: true}">' +
            '<option ng-style="getDropStyle(d)" ng-repeat="d in drop" value="{{d.Id}}" selected>{{d.Name}} ({{d.RegNum || \'--\'}})</option>' +
            '</select></div>' +
            '</div>' +
            '<div class="gos-num">' +
            '<div class="logus-text">{{infotf}}</div>' +
            '<div class="logus-input logus-textfield">{{RegNum}}</div>' +
            '</div>' +
            '</div>'
        }
    });

    app.directive('textinput', function () {
        return {
            restrict: "E",
            scope: {
                info: '=info',
                data: '=data',
                mask: "=mask",
                regexName: '@regexName',
                validErrorText: "=validErrortext",
                readonly: '=readonly'
            },
            link: function (scope, element, attrs) {
                scope.valErText = scope.validErrorText || "Введено недопустимое значение!";
                scope.regularExpression = '^.*$';
                if (scope.regexName) {

                    scope.regularExpression = CUSTOM_FILTERS[scope.regexName].source;
                }
            },
            controller: function ($scope) {
                $scope.fvalid = false;
            },
            template: '<div class="logus-text" >{{info}}</div>' +
            '<input class="logus-input" ng-model="data" ui-mask="{{mask}}" pattern="{{regularExpression}}" ng-readonly="readonly || false">' +
            '<div ng-show="fvalid" class="error-hint" >' +
            '<div class="error-hint-bottom-triangle"></div>' +
            '<p>{{valErText}}</p>' +
            '</div>'
        };
    });

    app.directive('textinputnotcomma', function () {
        return {
            restrict: "E",
            scope: {
                info: '=info',
                data: '=data',
                mask: "=mask",
			    placeholder: '=placeholder',
                regexName: '@regexName',
                validErrorText: "=validErrortext",
                readonly: '=readonly',
                fieldChange: '&',
                focusme: "=focusme"
            },
            link: function (scope, element, attrs) {
                scope.$watch("data", function (data) {
                    if (data) {
                        var newData = String(data).replace(",", ".");
                        scope.data = newData;
                    }
                    scope.regularExpression = '^.*$';
                    if (scope.regexName) {

                        scope.regularExpression = CUSTOM_FILTERS[scope.regexName].source;
                    }

                    if (data) {
                        if (scope.fieldChange) scope.fieldChange(data)
                    } else {
                        data = 0;
                    }
                });
            },
            controller: function ($scope) {
                $scope.fvalid = false;
            },
            template: '<div class="logus-text" >{{info}}</div>' +
            '<input class="logus-input" focus-me="focusme" ng-model="data" pattern="{{regularExpression}}" ui-mask="{{mask}}" placeholder="{{placeholder}}" ng-readonly="readonly" ng-change="changeInput()" ng-class="{bordernone: readonly}">'
        };
    });

    app.directive('focusMe', function($timeout) {
        return {
            link: function(scope, element, attrs) {
                scope.$watch(attrs.focusMe, function(value) {
                    if(value === true) {
                        console.log('value=',value);
                        setTimeout(function() {
                            element[0].focus();
                            scope[attrs.focusMe] = false;
                        }, 100);
                        //$timeout(function() {

                        //});
                    }
                });
            }
        };
    });
    
    app.directive('dateinput', function () {
        return {
            restrict: "E",
            scope: {
                info: '=info',
                dateTime: '=dataTime',
                data: '=data',
                regexName: '@regexName',
                validErrorText: "=validErrortext"
            },
            link: function (scope) {
                scope.$watch("dataTime", function (value) {
                    console.info(value);
                    if (value) {
                        var dt = new Date(value);
                        scope.data = dt;
                    }
                });
            },
            template: '<div class="logus-text" >{{info}}</div>' +
            '<input type="text" class="logus-input" ng-model="dataTime" value="data">'
        };
    });
    app.directive('textfield', function () {
        return {
            restrict: "E",
            scope: {
                info: '=info',
                data: '=data',
                regexName: '@regexName',
                validErrorText: "=validErrortext"
            },
            template: '<div class="logus-text" >{{info}}</div>' +
            '<div class="logus-input logus-textfield" ng-model="data">{{data}}</div>'
        };
    });

    app.directive('datepick', function () {
        return {
            restrict: "EA",
            scope: {
                mask: '=emask',
                data: '=data',
                regexName: '@regexName',
                validErrorText: "=validErrortext",
                dateChange: '&'
            },
            link: function (scope, element, attrs) {
                scope.data = scope.data ? new Date(scope.data) : new Date();
                scope.valErText = scope.validErrorText || "Введено недопустимое значение!";
                scope.format = 'dd.MM.yyyy';

                scope.open = function ($event) {
                    $event.preventDefault();
                    $event.stopPropagation();
                    scope.opened = true;
                };

                scope.$watch("data", function (value) {
                    scope.dateChange(value);
                });
            },
            controller: function ($scope) {
                $scope.fvalid = false;
            },
            template: '<p class="input-group ">' +
            '<input type="text" class="logus-input" datepicker-popup="{{format}}" datepicker-options="dateOptions" ng-model="data" is-open="opened" readonly="true"' +
            'current-text="Сегодня" close-text="Закрыть" clear-text="Очистить" starting-day="1" ng-required="true" />' +
            '<button  type="button" class="date-picker-btn" ng-click="open($event)"><img src="images/calendar/kalendar.png"></button>' +
            '</p>'
        }
    });


    app.directive('datepicktable', function () {
        return {
            restrict: "EA",
            scope: {
                mask: '=emask',
                data: '=data',
				type: '=type',
                regexName: '@regexName',
                validErrorText: "=validErrortext",
                dateChange: '&'
            },
            link: function (scope, element, attrs) {
                scope.data = scope.data ? new Date(scope.data) : new Date();
                scope.valErText = scope.validErrorText || "Введено недопустимое значение!";
				if (scope.type == 1) {
				scope.format = 'dd.MM';		
				} else {
				scope.format = 'dd.MM.yyyy';	
				}
                scope.open = function ($event) {
                    $event.preventDefault();
                    /*$event.stopPropagation();*/
                    scope.opened = true;
                };

                scope.$watch("data", function (value) {
                    if (value) scope.dateChange(value);
                });
            },
            controller: function ($scope) {
                $scope.fvalid = false;
            },
            template: '<p class="input-group">' +
            '<input  type="text" class="calendar logus-input inputdate bordernone" ng-click="open($event)" datepicker-popup="{{format}}" datepicker-options="dateOptions" ng-model="data" is-open="opened" readonly="true"' +
            'current-text="Сегодня" close-text="Закрыть" clear-text="Очистить" starting-day="1" ng-required="true" datepicker-append-to-body="true" is-open="isOpen"/>' +
            '</p>'
        }
    });

    app.directive('doubledatepick', function () {
        return {
            restrict: "EA",
            scope: {
                mask: '=emask',
                infofirst: '=infofirst',
                datafirst: '=datafirst',
                infosecond: '=infosecond',
                datasecond: '=datasecond',
                regexName: '@regexName',
                validErrorText: "=validErrortext",
                startDateChange: '&',
				endDateChange: '&'
            },
            link: function (scope, element, attrs) {
                //scope.datevalid = true;
                scope.datafirst = scope.datafirst ? new Date(scope.datafirst) : new Date();
                scope.datasecond = scope.datasecond ? new Date(scope.datasecond) : new Date();
                scope.valErText = scope.validErrorText || "Введено недопустимое значение!";
                scope.format = 'dd.MM.yyyy';

                scope.open = function (type, $event) {
                    $event.preventDefault();
                    $event.stopPropagation();
                    if (type === 'first') scope.openedFirst = true;
                    else scope.openedSecond = true;
                };

                scope.$watch("datasecond", function (value) {
                    compareDate();
                });
                scope.$watch("datafirst", function (value) {
                    compareDate();
                });
				 scope.$watch("datafirst", function (value) {
                    scope.startDateChange(value);
                });
				 scope.$watch("datasecond", function (value) {
                    scope.endDateChange(value);
                });
				

                var compareDate = function () {
                    var first = moment(scope.datafirst),
                        second = moment(scope.datasecond);
                    if (first > second) {
                        scope.fvalid = true;
                    } else {
                        scope.fvalid = false;
                    }
                }
            },
            controller: function ($scope) {
                $scope.fvalid = false;
            },
            template: '<div class="row" style="margin-left: 0px;"><div style="width: 130px;float: left;margin-right: 10px;"><div class="logus-text" >{{infofirst}}</div>' +
            '<p class="input-group ">' +
            '<input id="first" type="text" class="logus-input" style="width: 118px;" datepicker-popup="{{format}}" datepicker-options="dateOptions" ng-model="datafirst" is-open="openedFirst" readonly="true"' +
            'current-text="Сегодня" close-text="Закрыть" clear-text="Очистить" starting-day="1" ng-required="true"  ng-class="{errorinput: fvalid}" required/>' +
            '<button type="button" class="date-picker-btn" ng-click="open(\'first\',$event)"><img src="images/calendar/kalendar.png"></button>' +
            '</p></div>' +
            '<div style="width: 130px;float: left;margin-right: 10px;"><div class="logus-text" >{{infosecond}}</div>' +
            '<p class="input-group ">' +
            '<input id="second" type="text" class="logus-input" style="width: 118px;" datepicker-popup="{{format}}" datepicker-options="dateOptions" ng-model="datasecond" is-open="openedSecond" readonly="true"' +
            'current-text="Сегодня" close-text="Закрыть" clear-text="Очистить" starting-day="1" ng-required="true" ng-class="{errorinput: fvalid}" required />' +
            '<button type="button" class="date-picker-btn" ng-click="open(\'second\',$event)"><img src="images/calendar/kalendar.png"></button>' +
            '</p></div></div>'
        }
    });

    app.directive('textfieldbig', function () {
        return {
            restrict: "E",
            scope: {
                info: '=info',
                data: '=data',
                regexName: '@regexName',
                validErrorText: "=validErrortext"
            },
            link: function (scope, element, attrs) {
                scope.valErText = scope.validErrorText || "Введено недопустимое значение!";
                if (scope.regexName) {
                    var ngModelCtrl = element.find("input").controller("ngModel");
                    var reg = new RegExp(CUSTOM_FILTERS[scope.regexName], 'ig');
                    ngModelCtrl.$parsers.unshift(function (viewValue) {
                        ngModelCtrl.$setValidity('strongValidation', validationFunc(reg, viewValue, scope));
                        return viewValue;
                    });
                }
            },
            controller: function ($scope) {
                $scope.fvalid = false;
            },
            template: '<div class="logus-text-big" >{{info}}</div>' +
            '<div class="logus-textfield-big" ng-model="data">{{data}}</div>'
        };
    });

    app.directive('doubledrop', function () {
        return {
            restrict: "EA",
            scope: {
                infoparent: '=infoparent',
                dropparent: '=dropparent',
                dataparent: '=dataparent',
                infochild: '=infochild',
                datachild: '=datachild',
                fieldChange: '&'
            },
            link: function (scope) {
                scope.dropchild = [];
                scope.$watch("dataparent", function (dataparent) {
                    if (dataparent) {
                        if (dataparent) {
                            if (scope.fieldChange) scope.fieldChange(dataparent)
                        }
                        if (scope.dropparent) {
                            scope.dropparent.forEach(function (item, i, arr) {
                                if (item.Id == dataparent) {
                                    // из-за следущего присвоения какая-то ошибка(((
                                    scope.dropchild = item.Values;
                                    if (scope.dropchild.length == 1) {
                                        scope.datachild = scope.dropchild[0].Id;
                                    } else {
                                        scope.datachild = null;
                                    }
                                }
                            })
                        }
                    }
                });
                scope.getDropStyle = function (d) {
                    if (d.Bold != undefined) {
                        if (d.Bold == true) {
                            return {color: "#008c83"};
                        } else {
                            return {color: "#5a5a5a"};
                        }
                    } else {
                        return {};
                    }
                };
                scope.$watch("datachild", function (datachild) {
                    if (scope.dropchild.length == 1) {
                        scope.datachild = scope.dropchild[0].plantId;
                    }
                })
            },
            template: '<div class="row">' +
            '<div class="field" style="float: left;margin-right: 10px;">' +
            '<div class="logus-text" >{{infoparent}}</div>' +
            '<div class="select-style-dr">' +
            '<select class="logus-input logus-drop" ng-model="dataparent" ui-select2="{allowClear: true,  closeOnSelect: true}">' +
            '<option ng-style="getDropStyle(d)" ng-repeat="d in dropparent" value="{{d.Id}}">{{d.Name}}</option>' +
            '</select>' +
            '</div>' +
            '</div>' +
            '<div class="culture" style="float: left;">' +
            '<div class="logus-text" >{{infochild}}</div>' +
            '<div class="select-style-dr">' +
            '<select class="logus-input logus-drop" ng-model="datachild" ui-select2="{allowClear: true,  closeOnSelect: true}">' +
            '<option ng-style="getDropStyle(r)" ng-repeat="r in dropchild" value="{{r.Id}}" >{{r.Name}}</option>' +
            '</select>' +
            '</div>' +
            '</div>' +
            '</div>'
        };
    });

    app.directive('dropdownsearch', function () {
        return {
            restrict: "EA",
            scope: {
                info: '=info',
                drop: '=drop',
                data: '=data',
                fieldChange: '&',
                regexName: '@regexName',
                validErrorText: "=validErrortext",
				selectfunction: '=selectfunction'
            },
            link: function (scope) {
                scope.$watch("drop", function(data){
                    scope.d = {};
                    if (scope.data !== null) {
                        if (scope.drop) {
                            scope.drop.forEach(function (item, i, arr) {
                                if (item.Id == scope.data) {
                                    scope.d.selected = item;

                                }
                            });
                            if (scope.d.selected && scope.fieldChange) scope.fieldChange(data);
                        }
                    }
                });
                scope.$watch("data", function (data) {
                    scope.d = {};
                    if (data !== null) {
                        if (scope.drop) {
                            scope.drop.forEach(function (item, i, arr) {
                                if (item.Id == data) {
                                    scope.d.selected = item;
                                     if (scope.selectfunction) {
                                        scope.selectfunction(item);
                                    }
                                }
                            });
                            if (scope.d.selected && scope.fieldChange) scope.fieldChange(data);
                        }
                    } else {
                        scope.fieldChange(data);
                    }
                });
                scope.$watch("d.selected", function (d) {
                    if (d) {
                        if (scope.drop) {
                            scope.drop.forEach(function (item, i, arr) {
                                if (item == d) {
                                    scope.data = item.Id;
                                }
                            })
                        }
                    }
                });
                scope.getDropStyle = function (d) {
                    if (d.Bold != undefined) {
                        if (d.Bold == true) {
                            return 'color: #008c83';
                        } else {
                            return 'color: "#5a5a5a';
                        }
                    } else {
                        return '';
                    }
                };
            },
            template: '<div class="logus-text">{{info}}</div>' +
            '<ui-select ng-model="d.selected" theme="selectize" ng-disabled="disabled" ng-required="true">' +
            '<match>{{$select.selected.Name}}</match>' +
            '<choices ng-model="data" repeat="d in drop | filter: $select.search">' +
            '<span style="{{getDropStyle(d)}}" ng-bind-html="d.Name | highlight: $select.search"></span>' +
            '</choices>' +
            '</ui-select>'
        };
    });
    app.directive('dropdownsearchblue', function () {
        return {
            restrict: "EA",
            scope: {
                info: '=info',
                drop: '=drop',
                data: '=data',
                fieldChange: '&',
                regexName: '@regexName',
                validErrorText: "=validErrortext"
            },
            link: function (scope) {
                scope.$watch("drop", function(data){
                    scope.d = {};
                    if (scope.data !== null) {
                        if (scope.drop) {
                            scope.drop.forEach(function (item, i, arr) {
                                if (item.Id == scope.data) {
                                    scope.d.selected = item;

                                }
                            });
                            if (scope.d.selected && scope.fieldChange) scope.fieldChange(data);
                        }
                    }
                });
                scope.$watch("data", function (data) {
                    scope.d = {};
                    if (data) {
                        if (scope.drop) {
                            scope.drop.forEach(function (item, i, arr) {
                                if (item.Id == data) {
                                    scope.d.selected = item;

                                }
                            });
                            if (scope.d.selected && scope.fieldChange) {
                                scope.fieldChange(data);
                            }
                        }
                    }
                });
                scope.$watch("d.selected", function (d) {
                    if (d) {
                        if (scope.drop) {
                            scope.drop.forEach(function (item, i, arr) {
                                if (item == d) {
                                    scope.data = item.Id;
                                }
                            })
                        }
                    }
                });
                scope.getDropStyle = function (d) {
                    if (d.Bold != undefined) {
                        if (d.Bold == true) {
                            return 'color: #008c83';
                        } else {
                            return 'color: "#5a5a5a';
                        }
                    } else {
                        return '';
                    }
                };
            },
            template: '<div class="logus-text-blue">{{info}}</div>' +
            '<ui-select ng-model="d.selected" theme="selectizeblue" ng-disabled="disabled" ng-required="true">' +
            '<match>{{$select.selected.Name}}</match>' +
            '<choices ng-model="data" repeat="d in drop | filter: $select.search">' +
            '<span style="{{getDropStyle(d)}}" ng-bind-html="d.Name | highlight: $select.search"></span>' +
            '</choices>' +
            '</ui-select>'
        };
    });

    app.directive("dropdownsearchtf", function () {
        return {
            restrict: "E",
            scope: {
                info: '=info',
                infotf: '=infotf',
                drop: '=drop',
                data: '=data',
                regexName: '@regexName',
                validErrorText: "=validErrortext",
                selectfunction: '=selectfunction'
            },
            link: function (scope) {
                scope.getDropStyle = function (d) {
                    if (d.Bold != undefined) {
                        if (d.Bold == true) {
                            return 'color: #008c83';
                        } else {
                            return 'color: #5a5a5a';
                        }
                    } else {
                        return '';
                    }
                };
                scope.$watch("data", function (data) {
                    scope.d = {};
                    if (data) {
                        if (scope.drop) {
                            scope.drop.forEach(function (item, i, arr) {
                                if (item.Id == data) {
                                    scope.d.selected = item;
                                    if (scope.selectfunction) {
                                        scope.selectfunction(item);
                                    }
                                }
                            })
                        }
                        
                    }
                });
                scope.$watch("d.selected", function (d) {
                    if (d) {
                        if (scope.drop) {
                            scope.drop.forEach(function (item, i, arr) {
                                if (item == d) {
                                    scope.data = item.Id;
                                    scope.RegNum = item.RegNum;
                                }
                            })
                        }
                    }
                });
                scope.getNameWithRegName = function (d) {
                    var regNum = (d.RegNum !== null && d.RegNum !== "") ? d.RegNum : '--';
                    return d.Name + ' (' + regNum + ')';
                }
            },
            template: '<div class="row-margin-top">' +
            '<div style="width:70%; float: left;">' +
            '<div class="logus-text" >{{info}}</div>' +
            '<ui-select ng-model="d.selected" theme="selectize" ng-disabled="disabled" required>' +
            '<match>{{$select.selected.Name}}</match>' +
            '<choices ng-model="data" repeat="d in drop | filter: $select.search">' +
            '<span style="{{getDropStyle(d)}}" ng-bind-html="getNameWithRegName(d) | highlight: $select.search"></span>' +
            '</choices>' +
            '</ui-select>' +
            '</div>' +
            '<div class="gos-num">' +
            '<div class="logus-text">{{infotf}}</div>' +
            '<div class="logus-input logus-textfield">{{RegNum}}</div>' +
            '</div>' +
            '</div>'
        }
    });

    app.directive("dropinput", function () {
        return {
            restrict: "E",
            scope: {
                info: '=info',
                drop: '=drop',
                data: '=data',
                kindinput: '=kindinput',
                infoinput: '=infoinput',
                datainput: '=datainput',
                regexName: '@regexName'
            },
            link: function (scope) {
                // при первой загрузке данных не очищаем input
                scope.firstLoad = true;
                scope.getDropStyle = function (d) {
                    if (d.Bold != undefined) {
                        if (d.Bold == true) {
                            return 'color: #008c83';
                        } else {
                            return 'color: #5a5a5a';
                        }
                    } else {
                        return '';
                    }
                };
                scope.$watch("data", function (data) {
                    scope.d = {};
                    if (data) {
                        if (scope.drop) {
                            scope.drop.forEach(function (item, i, arr) {
                                if (item.Id == data) {
                                    scope.d.selected = item;
                                }
                            })
                        }
                        if (scope.fieldChange) scope.fieldChange(item.Name)
                    }
                });
                scope.$watch("d.selected", function (d) {
                    if (d) {
                        if (scope.drop) {
                            scope.drop.forEach(function (item, i, arr) {
                                if (item == d) {
                                    scope.data = item.Id;
                                    console.info(scope.firstLoad);
                                    // если ничего не было выбрано изначально
                                    if(!scope.datainput) scope.firstLoad = false;
                                    if (!scope.firstLoad) {
                                        scope.datainput = item[scope.kindinput];
                                    }
                                    scope.firstLoad = false;
                                }
                            })
                        }
                    }
                });
                scope.$watch("datainput", function (datainput) {
                    if (datainput) {
                        var newData = String(datainput).replace(",", ".");
                        scope.datainput = newData;
                    }
                    scope.regularExpression = '^.*$';
                    if (scope.regexName) {
                        scope.regularExpression = CUSTOM_FILTERS[scope.regexName].source;
                    }
                });
            },
            template: '<div>' +
            '<div class="equipment float-left">' +
            '<div class="logus-text" >{{info}}</div>' +
            '<ui-select ng-model="d.selected" theme="selectize" ng-disabled="disabled" required>' +
            '<match>{{$select.selected.Name}}</match>' +
            '<choices ng-model="data" repeat="d in drop | filter: $select.search">' +
            '<span style="{{getDropStyle(d)}}" ng-bind-html="d.Name | highlight: $select.search"></span>' +
            '</choices>' +
            '</ui-select>' +
            '</div>' +
            '<div class="linked-input">' +
            '<div class="logus-text">{{infoinput}}</div>' +
            '<input class="logus-input" ng-model="datainput" pattern="{{regularExpression}}"/>' +
            '</div></div>'
        }
    });
    
    app.directive('doubledropnew', function () {
        return {
            restrict: "EA",
            scope: {
                infoparent: '=infoparent',
                dropparent: '=dropparent',
                dataparent: '=dataparent',
                infochild: '=infochild',
                datachild: '=datachild',
                fieldChange: '&'
            },
            link: function (scope) {
                scope.dropchild = [];
                // при первой загрузке данных не очищаем культуру
                scope.firstLoad = true;
                scope.$watch("dataparent", function (dataparent) {
                    scope.dParent = {};
                    if (dataparent) {
                        if (dataparent) {
                            if (scope.fieldChange) scope.fieldChange(dataparent)
                        }
                        if (scope.dropparent) {
                            scope.dropparent.forEach(function (item, i, arr) {
                                if (item.Id == dataparent) {
                                    scope.dropchild = item.Values;
                                    if (scope.dropchild != null && scope.dropchild.length == 1 ) {
                                        scope.datachild = scope.dropchild[0].Id;
                                    }
                                    scope.dParent.selected = item;
                                }
                            })
                        }
                        if (scope.fieldChange) scope.fieldChange(dataparent)
                    }
                });
                scope.$watch("dParent.selected", function (dParent) {
                    if (dParent) {
                        if (scope.dropparent) {
                            scope.dropparent.forEach(function (item, i, arr) {
                                if (item == dParent) {
                                    scope.dataparent = item.Id;
                                    scope.dropchild = item.Values;
									//очищаем если нет культур
									 if (scope.dropchild == null) {
										 scope.firstLoad = false; 
									 }
                                if (scope.dropchild != null && scope.dropchild.length == 1 ) {
                                        scope.datachild = scope.dropchild[0].Id;
                                   //     scope.firstLoad = false;
                                    } else {
                                        if (!scope.firstLoad) {
                                            scope.dChild = {};
                                            scope.datachild = null;
                                        }
                                   //     scope.firstLoad = false;
                                    }
                                }
                            })
                        }
                    }
                });
                scope.getDropStyle = function (d) {
                    if (d.Bold != undefined) {
                        if (d.Bold == true) {
                            return {color: "#008c83"};
                        } else {
                            return {color: "#5a5a5a"};
                        }
                    } else {
                        return {};
                    }
                };
                scope.$watch("datachild", function (datachild) {
                    scope.dChild = {};
                    if (datachild) {
                        console.info(scope.datachild);
                        if (scope.dropchild) {
                            scope.dropchild.forEach(function (item, i, arr) {
                                if (item.Id == datachild) {
                                    scope.dChild.selected = item;
                                }
                            });
                        }
                    }
                });
                scope.$watch("dChild.selected", function (dChild) {
                    if (dChild) {
                        if (scope.dropchild) {
                            scope.dropchild.forEach(function (item, i, arr) {
                                if (item == dChild) {
                                    scope.datachild = item.Id;
                                }
                            })
                        }
                    }
                });
            },
            template: '<div style="width:100%;">' +
            '<div id="infoparent" style="float: left;width: 50%;">' +
            '<div class="logus-text" >{{infoparent}}</div>' +
            '<ui-select ng-model="dParent.selected" theme="selectize" ng-disabled="disabled">' +
            '<match>{{$select.selected.Name}}</match>' +
            '<choices ng-model="dataparent" repeat="dParent in dropparent | filter: $select.search">' +
            '<span style="{{getDropStyle(dParent)}}" ng-bind-html="dParent.Name | highlight: $select.search"></span>' +
            '</choices>' +
            '</ui-select>' +
            '</div>' +
            '<div style="float: left;width: 50%;padding-left: 10px">' +
            '<div class="logus-text" >{{infochild}}</div>' +
            '<ui-select ng-model="dChild.selected" theme="selectize" ng-disabled="disabled">' +
            '<match>{{$select.selected.Name}}</match>' +
            '<choices ng-model="datachild" repeat="dChild in dropchild | filter: $select.search">' +
            '<span style="{{getDropStyle(dChild)}}" ng-bind-html="dChild.Name | highlight: $select.search"></span>' +
            '</choices>' +
            '</ui-select>' +
            '</div>' +
            '</div>'
        };
    });
    ////////////////////
	 app.directive('doubledropallplants', function () {
        return {
            restrict: "EA",
            scope: {
                infoparent: '=infoparent',
                dropparent: '=dropparent',
                dataparent: '=dataparent',
                infochild: '=infochild',
                datachild: '=datachild',
				data: '=data',
                fieldChange: '&',
				plantChange: '&'
            },
            link: function (scope) {
                scope.dropchild = [];
                // при первой загрузке данных не очищаем культуру
                scope.firstLoad = true;
                scope.$watch("dataparent", function (dataparent) {
						console.info('=1');
                    scope.dParent = {};
					   console.info(dataparent);
                    if (dataparent) {
                        if (dataparent) {
                            if (scope.fieldChange) scope.fieldChange(dataparent)
                        }
                        if (scope.dropparent) {
                            scope.dropparent.forEach(function (item, i, arr) {
							//	console.info(item.Id);
							//		  console.info(dataparent);
                                if (item.Id == dataparent) {						 
                                    scope.dropchild = item.Values;
                                    if (scope.dropchild != null && scope.dropchild.length != 1 ) {
                                       scope.datachild = scope.dropchild[0].Id;
                                    }
                                    scope.dParent.selected = item;
                                }
                            })
                        }
                        if (scope.fieldChange) scope.fieldChange(dataparent);
						
						//все поля Allplants
				//		if (dataparent == -1) {
					//	scope.dropchild = scope.data;
				//		}
                   }
                
				
				
                });
                scope.$watch("dParent.selected", function (dParent) {
					console.info('=2');
                    if (dParent) {
                        if (scope.dropparent) {
							 scope.dropchild = [];
                            scope.dropparent.forEach(function (item, i, arr) {
                                if (item == dParent) {
                                    scope.dataparent = item.Id;
                                    scope.dropchild = item.Values;
                                if (scope.dropchild != null && scope.dropchild.length == 1 ) {
                                       scope.datachild = scope.dropchild[0].Id;
                                        scope.firstLoad = false;
                                    } else {
                                        if (!scope.firstLoad) {
                                            scope.dChild = {};
                                          scope.datachild = null;
                                        }
                                        scope.firstLoad = false;
                                    }
                                }
                            })
                        }
                    }
                });
                scope.getDropStyle = function (d) {
                    if (d.Bold != undefined) {
                        if (d.Bold == true) {
                            return {color: "#008c83"};
                        } else {
                            return {color: "#5a5a5a"};
                        }
                    } else {
                        return {};
                    }
                };
                scope.$watch("datachild", function (datachild) {
					console.info('=3');
                    scope.dChild = {};
					console.info(datachild);
					if (datachild==null) {
						scope.dChild.selected = {Id : -1, Name: "Все"};
					}
                    if (datachild) {				
                        console.info(scope.datachild);
                        if (scope.dropchild) {
                            scope.dropchild.forEach(function (item, i, arr) {
                                if (item.Id == datachild) {
                                    scope.dChild.selected = item;
                                }
                            });
                        }
                    }
					console.info(scope.dataparen);
					console.info(datachild);
					//после сброса параметров - очищаем выводим список всех культур
					if (scope.dataparen && scope.dataparent.length==0) {
						scope.dropchild = scope.data;	//Allplants
			}
					
						
                });
				////если не выбрано поле - выбираем все культуры
				  scope.$watch("data", function (data) {
						console.log(scope.data);
					scope.dropchild = scope.data;		
                });
				//
                scope.$watch("dChild.selected", function (dChild) {
					console.info('=4');
                    if (dChild) {
                        if (scope.dropchild) {
                            scope.dropchild.forEach(function (item, i, arr) {
                                if (item == dChild) {
                                    scope.datachild = item.Id;
                                }
                            })
                        }
						console.info(scope.dropparent);
						console.info(scope.datachild);
						
                    }
                });
            },
            template: '<div style="width:100%;">' +
            '<div style="float: left;width: 50%;">' +
            '<div class="logus-text" >{{infoparent}}</div>' +
            '<ui-select ng-model="dParent.selected" theme="selectize" ng-disabled="disabled">' +
            '<match>{{$select.selected.Name}}</match>' +
            '<choices ng-model="dataparent" repeat="dParent in dropparent | filter: $select.search">' +
            '<span style="{{getDropStyle(dParent)}}" ng-bind-html="dParent.Name | highlight: $select.search"></span>' +
            '</choices>' +
            '</ui-select>' +
            '</div>' +
            '<div style="float: left;width: 50%;padding-left: 10px">' +
            '<div class="logus-text" >{{infochild}}</div>' +
            '<ui-select ng-model="dChild.selected" theme="selectize" ng-disabled="disabled">' +
            '<match>{{$select.selected.Name}}</match>' +
            '<choices ng-model="datachild" repeat="dChild in dropchild | filter: $select.search">' +
            '<span style="{{getDropStyle(dChild)}}" ng-bind-html="dChild.Name | highlight: $select.search"></span>' +
            '</choices>' +
            '</ui-select>' +
            '</div>' +
            '</div>'
        };
    });
	//
	
    app.directive('datepicknamed', function () {
        return {
            restrict: "EA",
            scope: {
                info: '=info',
                mask: '=emask',
                data: '=data',
                regExName: '=regexName',
                validErrorText: "=validErrortext",
                dateChange: '&'
            },
            link: function (scope, element, attrs) {
                scope.data = scope.data ? new Date(scope.data) : new Date();
                scope.valErText = scope.validErrorText || "Введено недопустимое значение!";
                scope.format = 'dd.MM.yyyy';

                scope.open = function ($event) {
                    $event.preventDefault();
                    $event.stopPropagation();
                    scope.opened = true;
                };

                scope.$watch("data", function (value) {
                    scope.dateChange(value);
                });
            },
            controller: function ($scope) {
                $scope.fvalid = false;
            },
            template: '<div class="logus-text" >{{info}}</div>' + '<p class="input-group ">' +
            '<input type="text" class="logus-input" datepicker-popup="{{format}}" datepicker-options="dateOptions" ng-model="data" is-open="opened"' +
            'current-text="Сегодня" close-text="Закрыть" clear-text="Очистить" starting-day="1" ng-required="true" ng-class="{errorinput: fvalid}"/>' +
            '<button type="button" class="date-picker-btn" ng-click="open($event)"><img src="images/calendar/kalendar.png"></button>' +
            '</p>'
        }
    });

    app.directive('dropsearchtable', function () {
        return {
            restrict: "EA",
            scope: {
                drop: '=drop',
                data: '=data'
            },
            link: function (scope) {
                scope.$watch("data", function (data) {
                    scope.d = {};
                    if (data) {
                        if (scope.drop) {
                            scope.drop.forEach(function (item, i, arr) {
                                if (item.Id == data) {
                                    scope.d.selected = item;
                                }
                            })
                        }
                    }
                });
                scope.$watch("d.selected", function (d) {
                    if (d) {
                        if (scope.drop) {
                            scope.drop.forEach(function (item, i, arr) {
                                if (item == d) {
                                    scope.data = item.Id;
                                }
                            })
                        }
                    }
                });
                scope.getDropStyle = function (d) {
                    if (d.Bold != undefined) {
                        if (d.Bold == true) {
                            return 'color: #008c83';
                        } else {
                            return 'color: "#5a5a5a';
                        }
                    } else {
                        return '';
                    }
                }
            },
            template: '<ui-select ng-model="d.selected" theme="selectize" ng-disabled="disabled" append-to-body="true">' +
            '<match>{{$select.selected.Name}}</match>' +
            '<choices ng-model="data" repeat="d in drop | filter: $select.search">' +
            '<span style="{{getDropStyle(d)}}" ng-bind-html="d.Name | highlight: $select.search"></span>' +
            '</choices>' +
            '</ui-select>'
        };
    });

    app.directive('accordeonpanel', function () {
        return {
            restrict: "E",
            link: function (scope, element, attrs) {
                scope.itemOpened = [];
                if (scope.data) {
                    scope.data.forEach(function (item) {
                        item.itemOpened.push('false');
                    });
                }
            },
            controller: function ($scope, modals, modalType) {
                $scope.status = {
                    isFirstOpen: true,
                    isFirstDisabled: false
                };
                $scope.close = function () {
                    if ($scope.closeFunc) $scope.closeFunc();
                }
            },
            templateUrl: 'templates/accordeonTmpl.html'
        };
    });

    app.directive('doublelist', function() {
        return {
            restrict: "E",
            link:  function (scope) {

            },
            controller: "DoubleListCtrl",
            templateUrl: 'templates/doubleListTmpl.html'
        }
    });
})
();
