/**
 *  @ngdoc controller
 *  @name WayList.controller:WayBillEditCtrl
 *  @description
 *  # WayBillEditCtrl
 *  Это контроллер, определеяющий поведение формы создания и редактирования путевого листа
 *  @requires $scope
 *  @requires $http
 *  @requires requests
 *  @requires $location
 *  @requires $stateParams
 *  @requires $cookieStore

 *  @property {Object} sc: scope
 *  @property {Object} curUser: информация о пользователе
 *  @property {Object} formData: информация о путевом листе
 *  @property {string} submitPath: путь запроса на сохрениние или обновление информации

 *  @property {function} checkUserRole: разрешить или запретить добавление нового выделенного поля с карты
 *  @property {function} loadInfo: начальная загрузка информации формы. Для создания - только выпадающие списки. Для редактирования списки и информация по путевому листу.
 *  @property {function} submitCallback: функция возврата после сохранения информации. При создании - запомнить id листа, чтобы выбрать его в списке. При редактировании - просто переход назад. Из сменного задания - переход на форму сменного задания.
 *  @property {function} sc.cancelWayBillClick: отмена изменений и обратный переход
 *  @property {function} sc.submitForm: сохранение изменеий и обратный переход
 *  @property {function} initMap: инициализация функционала карты
 *  @property {function} sc.setRepair : если устанавливают флаг Ремонтный путевой лист - подставляем маску для номера и значения по уолчанию
 *  @property {function} sc.traktorChange: при выборе техники подсталять автоматически тип топлива для данной единицы.
 */

var app = angular.module("WayBillsEdit", ["ngGrid","ui.bootstrap","services","mapModule"]);
app.controller("WayBillEditCtrl", ["$scope", '$cookieStore', "$http","requests", "$location",'$stateParams','$cookieStore', "map",'LogusConfig', 
function ($scope, $cookieStore, $http, requests, $location, $stateParams, $cookieStore, map, LogusConfig) {
	
	var sc = $scope;
    var firstLoad = false;
    sc.formData = {};
    var curUser = $cookieStore.get('currentUser');
     sc.tporgid = curUser.tporgid;
    function checkUserRole() {
        if (curUser.roleid == "4" || curUser.roleid == "10") {
            sc.formData.editBtnVisible = true;
        } else {
            sc.formData.editBtnVisible = false;
        }
    }
	 map.clearAll();
	         
			sc.checkRes = function () {				
			var Orgid = null;
			if (sc.formData.AllResource != true) {
				Orgid = curUser.orgid;
			} 
			
			     requests.serverCall.post($http, "WaysListsCtrl/GetInfoForCreateWaysList", {OrgId : Orgid},
				 function (data, status, headers, config) {
					console.info('GetInfo===');
                    console.info(data);
                    sc.infoData = data;
                }, function () {});	
			} 
			 
			 
		sc.clear = function () { 
				console.log('clear'); 
				map.clearAll(); 
				initMap(sc, map, curUser, LogusConfig); 
				map.refreshField(LogusConfig.serverUrl + 'WaysListsCtrl/GetCloseInfoForMapByField?field_id=' + sc.formData.FieldId); 
           };

    var loadInfo, submitCallback, submitPath;
    switch ($stateParams.type) {
        case "create":
            if($cookieStore.get('dateWayBillsCalendar')){
          //      if($cookieStore.get('dateWayBillsCalendar').date) sc.formData.DateBegin = $cookieStore.get('dateWayBillsCalendar').date;
            }

            loadInfo = function (cb) {
                requests.serverCall.post($http, "WaysListsCtrl/GetInfoForCreateWaysList", {OrgId : curUser.orgid, WaysListId:$stateParams.way_bill_id}
				, function (data, status, headers, config) {
					console.info('GetInfo2');
                    console.info(data);
                    sc.infoData = data;
                    if (cb) cb();
                    checkUserRole();
                }, function () {});
            };

            sc.cancelWayBillClick = function () {
				$location.path('/way_bills');	
            };

            submitPath = "WaysListsCtrl/CreateWaysList";
            submitCallback = function (data, status, headers, config) {
                console.info(data);
                $location.path('/way_bills');
            };
            break;
        case "edit":
            sc.numWaysList = $stateParams.way_bill_id;
            console.info('numWaysList ' + sc.numWaysList);
            //AllResource : sc.formData.AllResource
            loadInfo = function(cb) {
                requests.serverCall.post($http, "WaysListsCtrl/GetInfoForCreateWaysList", {OrgId : curUser.orgid, WaysListId:$stateParams.way_bill_id,
            }, function (data, status, headers, config) {
                    console.info(data);
                    sc.infoData = data;

                    if ($stateParams.way_bill_id) {
                        requests.serverCall.post($http, "WaysListsCtrl/GetWaysListById", {"WaysListId": $stateParams.way_bill_id}, function (data, status, headers, config) {
                            console.info(data);
                            sc.formData = data;
                            if (cb) cb();
                            console.info($stateParams.way_bill_id);
                            $cookieStore.put('selectedWayBill', {id: $stateParams.way_bill_id});
                            checkUserRole();
                        }, function () {});
                    } else {
                        console.log('way_bill_id is null or undefined')
                    }
                });
            };

            sc.cancelWayBillClick = function () {
				$location.path('/way_bills');
            };
            submitPath = "WaysListsCtrl/UpdateWaysListById";
            submitCallback = function (data, status, headers, config) {
                console.info(data);
				$location.path('/way_bills');
            };
            break;
        case "fromtask":
            sc.DetailId = $stateParams.way_bill_id;
            console.info('DetailId ' + sc.numWaysList);
            $cookieStore.put('selectedShiftTask', {id: sc.DetailId});

            loadInfo = function(cb) {
				  requests.serverCall.post($http, "WaysListsCtrl/GetInfoForCreateWaysList", {OrgId : curUser.orgid, DayTaskId:$stateParams.way_bill_id}, function (data, status, headers, config) {  //sc.DetailId
                    console.info(data);
                    sc.infoData = data;
                    requests.serverCall.post($http, "WaysListsCtrl/GetWaysListByShiftTask", sc.DetailId, function (data, status, headers, config) {
                        console.info(data);
                        sc.formData = data;
						
                        if (cb) cb();
                        console.info($cookieStore.get('shiftWay').shift);
                        sc.formData.DateBegin = $cookieStore.get('dateCalendar').date;
                        if ($cookieStore.get('shiftWay').shift == '1') sc.formData.DateBegin = moment($cookieStore.get('dateCalendar').date).add('days', 1)._d;
                        console.log("after2 " + sc.formData.FieldId);
                        checkUserRole();
                    }, function () {
                    });
                });
            };

            sc.cancelWayBillClick = function () {
                $location.path('/shift_task');
            };

            submitPath = "WaysListsCtrl/CreateWaysList";
            submitCallback = function (data, status, headers, config) {
                console.info(data);
                $location.path('/shift_task');
            };
            break;
    }
       loadInfo();


    sc.submitForm = function () {
        var formDataSend = {};

        $cookieStore.put('dateWayBillsCalendar', {date:sc.formData.DateBegin});

        formDataSend = angular.copy(sc.formData);
        formDataSend.DateBegin = moment(formDataSend.DateBegin).format("MM.DD.YYYY");

        formDataSend.OrgId = curUser.orgid;
        formDataSend.UserId = curUser.userid;

        var timeStart = formDataSend.timeStart;
        if(timeStart) if(timeStart.length == 4) formDataSend.timeStart = timeStart.substring(0,2)+':'+timeStart.substring(2,4);

        var timeEnd = formDataSend.timeEnd;
        if(timeEnd) if(timeEnd.length == 4) formDataSend.timeEnd = timeEnd.substring(0,2)+':'+timeEnd.substring(2,4);

        if ($stateParams.type === "fromtask") {
            formDataSend.DetailId = sc.DetailId;
        }

        requests.serverCall.post($http, submitPath, formDataSend, submitCallback, function() {});
    };

    function initMap(sc, map, curUser, LogusConfig) {
		map.initMap('map1');
		map.clearMap();
        sc.refreshLayer = function (data) {
            map.refreshField(LogusConfig.serverUrl + 'WaysListsCtrl/GetCloseInfoForMapByField?field_id=' + sc.formData.FieldId);
        };
        map.allowSingleDraw('');
        if (curUser.roleid == "4" || curUser.roleid == "10") {
            map.addInteraction();
            map.setInsertFieldCb(function (newFieldId) {
                loadInfo(function() {
                    sc.formData.FieldId = newFieldId;
                });
            });
            sc.getField = function () {
                map.getField();
            };
        }

    }
    initMap(sc, map, curUser, LogusConfig);

    sc.setRepair = function () {
        if(sc.formData.IsRepair){
            if (!sc.formData.WayListNum || sc.formData.WayListNum.indexOf('Р-') == -1) {
                sc.formData.WayListNum = "P/00/0";
            }
            //sc.formData.FieldId = 87; //ГММ Орлово
            sc.formData.TaskTypeId = 124; //Текущий ремонт и ТО с/х техники
			sc.formData.TypeFuel = 3;     //дизельное топливо
        }
    };
	
    sc.traktorChange = function(item) {
        var typeFuelSel = item.Name_fuel;
		console.log(typeFuelSel);
        sc.formData.TypeFuel = typeFuelSel.Id;
		sc.nameOfFuel = typeFuelSel.Name;
    };
}]);
