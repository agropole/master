/**
 *  @ngdoc controller
 *  @name WayList.controller:WayBillsIssuanceCtrl
 *  @description
 *  # WayBillsIssuanceCtrl
 *  Это контроллер, определеяющий поведение формы создания и редактирования путевого листа
 *  @requires $scope
 *  @requires $http
 *  @requires requests
 *  @requires $location
 *  @requires $stateParams
 *  @requires $cookieStore
  * @requires $rootScope
 *  @requires modals
 *  @requires modalType

 *  @property {Object} sc: scope
 *  @property {Object} curUser: информация о пользователе
 *  @property {Object} tableData: список путевых листов
 *  @property {Object} mySelections: выбранные путевые листы
 *  @property {Object} gridOptions: настройки таблицы путевых листов

 *  @property {function} sc.dateChangeWayBills: событие изменение даты - загрузка списка путевых листов на выбранную дату
 *  @property {function} sc.createBtnClick: создать новый путевой лист - открытие формы создания
 *  @property {function} sc.editBtnClick: редактирование путевого листа - необходимо выделить путевой лист в таблице
 *  @property {function} sc.deleteBtnClick: удаление путевого листа - необходимо выделить путевой лист в таблице
 *  @property {function} getWayBills: запрос путевых листов по текущей дате
 *  @property {function} selectRowByField: выделить строки
 *  @property {function} sc.issueBtnClick : печать путевого листа - необходимо выделить путевой лист в таблице
 *  @property {function} sc.closeBtnClick: закрытие путевого листа - необходимо выделить путевой лист в таблице
 *  @property {function} needSelection: проверка выделен ли путевой лист
 */

var app = angular.module("WayBillsList", ["ngGrid", "services", 'ngCookies', "modalWindows"]);
app.controller("WayBillsIssuanceCtrl", ["$scope", "$http", "requests", "$location", '$cookieStore','$rootScope', "modals", "modalType",'$stateParams', function ($scope, $http, requests, $location, $cookieStore,$rootScope, modals, modalType,$stateParams) {
    var sc = $scope;
    sc.infoData = [];
	sc.formData = [];
	sc.infoData.OrganizationsList = [];
	var date = new Date();
	sc.dateWayBill = new Date(date.getFullYear(),date.getMonth(),1);
	
    if ($cookieStore.get('dateWayBillsCalendar')) {
        if ($cookieStore.get('dateWayBillsCalendar').date) {
		//	sc.dateWayBill = $cookieStore.get('dateWayBillsCalendar').date;
		//	sc.dateWayBillEnd = sc.dateWayBill;
		}
    }
     //получаем дату запрета
	 requests.serverCall.get($http, "DayTasksCtrl/GetBanDateWayBills", function (data, status, headers, config) {
        console.info(data);
		if (data != "bad") { 
        sc.ban_date = data;} else {var date = new Date; sc.ban_date="01.01." + date.getFullYear();}
    }, function () {
    });
	
	
    var curUser = $cookieStore.get('currentUser');
    console.log(curUser);
	if (curUser.roleid != 10 && curUser.roleid != 4 ) {
		$("#delete").addClass("disabledbutton");
	}
        sc.tporgid = curUser.tporgid;
	if (curUser.tporgid == 1) {
	sc.infoData.OrganizationsList.push({Id : curUser.orgid, Name : curUser.orgname });
	sc.formData.OrgId = curUser.orgid;	
			}
	
	
    sc.tableData = [];
    sc.mySelections = [];
    sc.gridOptions = {
        data: 'tableData',
		enableSorting: false,
        enableColumnResize: true,
        multiSelect: false,
        enableRowSelection: true,
        enablePaging: true,
        showFooter: false,
        selectedItems: sc.mySelections,
        rowHeight: 49,
        headerRowHeight: 49,
        columnDefs: [
            {field: "WaysListNum", displayName: "№", width: "8%", cellTemplate:'<a tooltip="{{row.getProperty(col.field)}}" tooltip-popup-delay="1000" tooltip-class="my-custom-tooltip" tooltip-placement="top" tooltip-append-to-body="true"><div class="ngCellText">{{row.getProperty(col.field)}}</div></a>'},
            {field: "DateBegin", displayName: "Дата", width: "10%", enableCellEdit: true, cellTemplate:'<a tooltip="{{row.getProperty(col.field)}}" tooltip-popup-delay="1000" tooltip-class="my-custom-tooltip" tooltip-placement="top" tooltip-append-to-body="true"><div class="ngCellText">{{row.getProperty(col.field)}}</div></a>'},
            {field: "ShiftNum", displayName: "Смена", width: "10%", cellTemplate:'<a tooltip="{{row.getProperty(col.field)}}" tooltip-popup-delay="1000" tooltip-class="my-custom-tooltip" tooltip-placement="top" tooltip-append-to-body="true"><div class="ngCellText">{{row.getProperty(col.field)}}</div></a>'},
            {field: "FioDriver", displayName: "Водитель", width: "16%", cellTemplate:'<a tooltip="{{row.getProperty(col.field)}}" tooltip-popup-delay="1000" tooltip-class="my-custom-tooltip" tooltip-placement="top" tooltip-append-to-body="true"><div class="ngCellText">{{row.getProperty(col.field)}}</div></a>'},
            {field: "CarsName", displayName: "TC", width: "15%", cellTemplate:'<a tooltip="{{row.getProperty(col.field)}}" tooltip-popup-delay="1000" tooltip-class="my-custom-tooltip" tooltip-placement="top" tooltip-append-to-body="true"><div class="ngCellText">{{row.getProperty(col.field)}}</div></a>'},
            {field: "CarsNum", displayName: "Гос. №", width: "10%", cellTemplate:'<a tooltip="{{row.getProperty(col.field)}}" tooltip-popup-delay="1000" tooltip-class="my-custom-tooltip" tooltip-placement="top" tooltip-append-to-body="true"><div class="ngCellText">{{row.getProperty(col.field)}}</div></a>'},
            {field: "NameEq", displayName: "Оборудование", width: "21%", cellTemplate:'<a tooltip="{{row.getProperty(col.field)}}" tooltip-popup-delay="1000" tooltip-class="my-custom-tooltip" tooltip-placement="top" tooltip-append-to-body="true"><div class="ngCellText">{{row.getProperty(col.field)}}</div></a>'},
            {field: "Status", displayName: "Статус", width: "10%", cellTemplate: 'templates/status_cell_table.html'}
        ],
        afterSelectionChange: function (row) {
            sc.mySelections = [];
            sc.mySelections.push(row.entity);
			var dateBegin = sc.mySelections[0].DateBegin.split(".");
			var dateBegin1 = new Date(dateBegin[2], (dateBegin[1] - 1), dateBegin[0]);
			var banDate = sc.ban_date.split("T")[0].split("-");
			var banDate1 = new Date(banDate[0], (banDate[1] - 1), parseInt(banDate[2])+1);
		if (sc.mySelections[0].Status==4 && dateBegin1.getTime()<banDate1.getTime()) 
		{
           console.log('закрыть лист');			
			document.getElementById('close').disabled = true;
            document.getElementById('edit').disabled = true; } else 
			{
		   document.getElementById('close').disabled = false;
            document.getElementById('edit').disabled = false;}
          		
        }
    };

    sc.dateChangeWayBills = function () {
		//sc.filterText =  null;
        if (sc.dateWayBill) {
            $cookieStore.put('dateWayBillsCalendar', {date: sc.dateWayBill});
            getWayBills();
        } else {
            console.log('wrong date from datepick')
        }
    };

    sc.createBtnClick = function () {
        $location.path('/edit_bill/create/');
    };

    sc.editBtnClick = function () {
        if (sc.mySelections[0]) {
            console.info('edit  ' + sc.mySelections[0].WaysListId);
            $location.path('/edit_bill/edit/' + sc.mySelections[0].WaysListId);
        } else needSelection('Редактирование путевого листа', "Для редактирования необходимо выбрать путевой лист");
    };

    sc.deleteBtnClick = function () {
        if (sc.mySelections[0]) {
            modals.showWindow(modalType.WIN_DIALOG, {
                    title: "Удаление", message: "Удалить путевой лист?", callback: function () {
                        requests.serverCall.post($http, "WaysListsCtrl/DeleteWayLists", sc.mySelections[0].WaysListId, function (data, status, headers, config) {
                            console.info(data);
                            getWayBills();
                        });
                    }

                }
            );
        } else needSelection('Удаление путевого листа', "Для удаления необходимо выбрать путевой лист");
    };

     	 sc.search = function() {
		if (sc.filterText.length == 0) {
		//sc.openRepair();
		} else { 
	  //  $("#datepick").addClass("crystalbutton");
		}
		
      if ($cookieStore.get('timerId2')) {
	   clearInterval($cookieStore.get('timerId2'));
   }
    $cookieStore.put('timerId2', setTimeout(getWayBills, 1000));	
	
	};

    function getWayBills() {
        sc.mySelections = [];
        requests.serverCall.post($http, "WaysListsCtrl/GetWaysLists", {
             DateFilter: moment(sc.dateWayBill).format("MM.DD.YYYY"),
			 DateFilterEnd: moment(sc.dateWayBillEnd).format("MM.DD.YYYY"),
             OrgId: curUser.orgid,
			 TporgId : curUser.tporgid,
			 RegNum: sc.filterText,
        }, function (data, status, headers, config) {
            console.info(data);
            sc.tableData = data;
            if (sc.tableData.length > 0) {
                if ($cookieStore.get('selectedWayBill')) {
                    selectRowByField("WaysListId", $cookieStore.get('selectedWayBill').id, true);
                    $cookieStore.remove('selectedWayBill');
                } else selectRowByField("WaysListId", sc.tableData[0]["WaysListId"], true);
            }
        }, function () {
        });
    }

    function selectRowByField(field, val, select) {
        sc.$on('ngGridEventData', function () {
            sc.tableData.forEach(function (item, i, arr) {
                if (item[field] == val) {
                    sc.gridOptions.selectRow(i, select);
                    if (sc.gridOptions.ngGrid) sc.gridOptions.ngGrid.$viewport.scrollTop(sc.gridOptions.ngGrid.rowMap[i] * sc.gridOptions.ngGrid.config.rowHeight);
                    if (select == true) {
                        sc.mySelections = [];
                        sc.mySelections.push(item);
                    }
                }
            });
        });
    }

    sc.issueBtnClick = function () {
        if (sc.mySelections[0]) {
            sc.getStyle = function (id) {
                var top = 42 + (18 * id);

                return {top: top.toString() + "mm"};
            };
            sc.getStyleHead = function (id) {
                var top = 42 + (8 * id);
                return {top: top.toString() + "mm"};
            };
            var urlParam = function (name) {
                var results = new RegExp('[\\?&]' + name + '=([^&#]*)').exec(window.location.href);
                var param = " ";
                console.info(results);
                if (results != null) param = results[1];
                return param;
            };
            console.info('issue  ' + sc.mySelections[0].WaysListId);
            requests.serverCall.post($http, "WaysListsCtrl/PrintWaysListById", {"WaysListId": sc.mySelections[0].WaysListId}, function (data, status, headers, config) {
                console.info(data);
                sc.data = data;
                setTimeout(function () {
                    var printContents = document.getElementById('printWay').innerHTML;
                    var popupWin = window.open('', '_blank');
                    if (typeof popupWin != 'undefined') {
                        popupWin.document.write('<html><head></head><body >' + printContents + '</html>');
                        popupWin.print();
                        popupWin.close();
                    }
                }, 1000);
            });
        } else needSelection('Печать путевого листа', "Для печати необходимо выбрать путевой лист");
    };

    sc.closeBtnClick = function () {
        if (sc.mySelections[0]) {
            console.info('close  ' + sc.mySelections[0].WaysListId);
            $location.path('/close_bill/' + sc.mySelections[0].WaysListId);
        } else needSelection('Закрытие путевого листа', "Для закрытия необходимо выбрать путевой лист");
    }

    function needSelection(title, message) {
        modals.showWindow(modalType.WIN_MESSAGE, {
            title: title,
            message: message,
            callback: function () {
            }
        });
    }
}]);


