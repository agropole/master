/**
 *  @ngdoc controller
 *  @name WayList.controller:CloseEditTaskCtrl
 *  @description
 *  # CloseEditTaskCtrl
 *  Это контроллер, определеяющий поведение формы создания и редактирования заданий при закрытии путевого листа
 *  @requires $scope
 *  @requires $http
 *  @requires requests
 *  @requires $location
 *  @requires $stateParams
 *  @requires modals
 *  @requires modalType
 *  @requires LogusConfig
 *  @requires map
 *  @requires $cookieStore

 *  @property {Object} sc: scope
 *  @property {string} type:функция Которая позволяет выбрать справочник
 *  @property {Object} curUser: информация о пользователе
 *  @property {Object} formData: информация о задании
 *  @property {function} getInfoCallback:функция callback после получения справочных списков. Имеет два типа: при создании и при редактировании
 *  @property {function} refreshLayer:функция обновления слоев карты
 *  @property {function} initMapEdit: инициализация необходимого функционала карты
 *  @property {function} sc.changeField: выбор другого поля, при редактировании
 *  @property {function} sc.countArea: пересчет обработанной площади по координатам
 *  @property {function} sc.addComment: открытие окна ввода комментария
 *  @property {function} sc.changeConsumptionFact: пересчет стоимости топлива при изменении значения Фактический расход
 *  @property {function} sc.refreshRate: запрос на новую расценку при изменеии значений: Оборудование, Задание или Агротех. требования
 *  @property {function} sc.submitForm: выход с сохранением
 *  @property {function} sc.cancelClick: высход без сохранения
 */
var app = angular.module("CloseAddEditTask", ["services", 'ngCookies', "mapModule", "modalWindows",'Directives', "ui.utils", "LogusConfig"]);
app.controller("CloseEditTaskCtrl", ["$scope", "$http", "requests", "$location", '$stateParams', "modals", "modalType", 'LogusConfig', "map",'$cookieStore', function ($scope, $http, requests, $location, $stateParams, modals, modalType, LogusConfig, map, $cookieStore) {

     var sc = $scope;
     var type = $stateParams.type;
     var curUser = $cookieStore.get('currentUser');
     sc.tporgid = curUser.tporgid;
     sc.formData = {};
	 map.clearAll();  
	 sc.clear = function () { 
		map.clearAll(); 
		initMapEdit(map, sc, refreshLayer); 
	    refreshLayer(startFieldId);
		};
	
		sc.checkRes = function () {				
			var Orgid = null;
			if (sc.AllResource != true) {
				Orgid = curUser.orgid;
			} 
			
			     requests.serverCall.post($http, "WaysListsCtrl/GetInfoForCreateWaysList", {OrgId : Orgid, WaysListId:null},
				 function (data, status, headers, config) {
                    console.info(data);
                    sc.infoData = data;
                }, function () {});	
			} 
	
	
	
    var getInfoCallback, refreshLayer;
    switch (type) {
        case "create":
            getInfoCallback = function (data, status, headers, config) {
                console.info(data);
                sc.infoData = data;
                sc.formData.TypePrice = 1;
            };

            refreshLayer = function (data) {
                sc.formData.Area = 0;
                var url = LogusConfig.serverUrl + 'WaysListsCtrl/GetCloseInfoForMapByField?field_id=' + sc.formData.FieldId;
                console.log('refreshLayer ' + url);
                map.refreshField(url);
            };
            setTimeout(function() {
                sc.refreshRate = refreshRate;
            }, 1000);
            
            break;
        case "edit":
            var startFieldId, startArea;
            getInfoCallback = function (data, status, headers, config) {
                console.info(data);
                sc.infoData = data;

                if ($stateParams.way_bill_id) {
                    requests.serverCall.post($http, "WaysListsCtrl/GetTaskById", $stateParams.task_id, function (data, status, headers, config) {
                        console.info(data);
                        startFieldId = data.FieldId;
                        startArea = data.Area;
                        sc.formData = data;
						sc.AllResource = data.AllResource;
                        if (!sc.formData.TypePrice) 
							sc.formData.TypePrice = 1;
                        setTimeout(function() {
                            sc.refreshRate = refreshRate;
                        }, 1000);
                    }, function () {
                    });
                } else {
                    console.log('way_bill_id is null or undefined')
                }
            };

            refreshLayer = function (data) {
                var url;
                if (startFieldId != data) {
                    sc.formData.Area = 0;
                    url = LogusConfig.serverUrl + 'WaysListsCtrl/GetCloseInfoForMapByField?field_id=' + sc.formData.FieldId + '&task_id=' + $stateParams.task_id;
                } else if (startFieldId == data) {
                    sc.formData.Area = startArea;
                    url = LogusConfig.serverUrl + 'WaysListsCtrl/GetCloseInfoForMap?task_id=' + $stateParams.task_id;
                }
                console.log('refreshLayer ' + url);
                map.refreshField(url);
            };

            sc.changeField = function (fieldId) {
                if (sc.infoData.FieldsAndPlants) {
                    var fieldsPlantsDrop = sc.infoData.FieldsAndPlants;
                    for (var i = 0, il = fieldsPlantsDrop.length; i < il; i++) {
                        if (fieldsPlantsDrop[i].Id == fieldId) {
                            //if(!sc.$$phase) {
                            //sc.$apply(function () {
                            sc.dropPlants = fieldsPlantsDrop[i].Values;
                            if (sc.dropPlants.length == 1) {
                                sc.formData.PlantId = sc.dropPlants[0].plantId;
                            }
                            //});
                            //}

                        }
                    }
                }
            };

            sc.countArea = function () {
                requests.serverCall.post($http, "WaysListsCtrl/calcThreadedArea", $stateParams.task_id, function (data, status, headers, config) {
                    console.info(data);
                    startArea = sc.formData.Area = data;
                    refreshLayer(startFieldId);
                });
            };
			
				sc.countCons = function () {
				  requests.serverCall.post($http, "WaysListsCtrl/getConsWialon", $stateParams.task_id, function (data, status, headers, config) {
                    console.info(data);
                    sc.formData.WialonConsFact = data[0];
                });
            };
			
				
			
			
            break;
    }
        requests.serverCall.post($http, "WaysListsCtrl/GetInfoForCreateWaysList", {OrgId : curUser.orgid, TaskId: $stateParams.task_id}, getInfoCallback);
	  

    sc.addComment = function () {
        modals.showWindow(modalType.WIN_COMMENT, {
            nameField: "Комментарий",
            comment: sc.formData.Comment
        })
            .result.then(function (result) {
                console.info(result);
                sc.formData.Comment = result;
            }, function () {
                console.info('Modal dismissed at: ' + new Date());
            });
    };

    function initMapEdit(map, sc, refreshLayer) {
        console.info(map);
        map.initMap('map');
        map.clearMap();
        map.addInteraction();
        map.setAreaInput(function (area) {
            sc.formData.Area = area;
            sc.$apply();
        });
        sc.clearMeasures = function () {
            map.clearOverlay(function (area) {
                sc.formData.Area = area;
            });
        };
        sc.refreshLayer = refreshLayer;
    }
    initMapEdit(map, sc, refreshLayer);

    sc.changeConsumptionFact = function() {
        sc.formData.CostFuel = Math.round(sc.formData.ConsumptionFact * sc.formData.TypeFuel.Price * 100) / 100;
    };

    function refreshRate() {
        if (sc.formData.TaskTypeId) {
            requests.serverCall.post($http, "WaysListsCtrl/GetRate", {
                IdTptas: sc.formData.TaskTypeId,
                IdTrakt: sc.formData.Traktor ? sc.formData.Traktor.Id : null,
                IdEqui: sc.formData.EquipmentId,
                IdPlant: sc.formData.PlantId,
                IdAgroreq: sc.formData.AgroreqId,
				SheetId: $stateParams.way_bill_id,
            }, function (data, status, headers, config) {
                console.log(data);
                sc.formData.RateShift = data.RateShift;
                sc.formData.RatePiecework = data.RatePiecework;
                sc.formData.IdRate = data.Id;
            }, function () {}, false, false);
        }
    }

    sc.submitForm = function () {
        if (sc.formData.Area && sc.formData.Volumefact && sc.formData.RatePiecework) {
            modals.showWindow(modalType.WIN_MESSAGE, {
                title: "Закрытие",
                message: "Вы заполнили оба поля 'Факт объем, тонн' и 'В пер. на усл. га'. Закрыть задание и расчитать зарплату по Га?",
                callback: function () {
                    submit();
                }
            });
        } else {
            submit();
        }
        
        function submit() {
            map.clearAll();
            sc.formData.WaysListId = $stateParams.way_bill_id;
            if (sc.formData.Area) {
                sc.formData.TypePrice = 2;
            } else if (sc.formData.Volumefact){
                sc.formData.TypePrice = 3;
            } else {
                sc.formData.TypePrice = null;
            }
            requests.serverCall.post($http, "WaysListsCtrl/UpdateTaskInWayList", sc.formData, function (data, status, headers, config) {
                console.info(data);
                $cookieStore.put('WayBilsId', $stateParams.way_bill_id);
				$location.path('/close_bill/' + $stateParams.way_bill_id);
            }, function () {
            });
        }
    };

    sc.cancelClick = function () {
        map.clearAll();
        $cookieStore.put('WayBilsId', $stateParams.way_bill_id);
		$location.path('/close_bill/' + $stateParams.way_bill_id);
    };

	if (sc.formData.TypePrice === 1) {
	sc.NameTypePrice = "Расценка,руб/га";
	} else {
			sc.NameTypePrice = "Расценка,руб/т";
		}
		
		
	
	
	
	
}]);