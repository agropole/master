/**
 *  @ngdoc controller
 *  @name WayList.controller:CloseWayBillCtrl
 *  @description
 *  # CloseWayBillCtrl
 *  Это контроллер, определеяющий поведение формы закрытия путевого листа с отобряжением списка задач
 *  @requires $scope
 *  @requires $http
 *  @requires requests
 *  @requires $location
 *  @requires $stateParams
 *  @requires modals
 *  @requires modalType
 *  @requires $cookieStore

 *  @property {Object} sc: scope
 *  @property {Object} curUser: информация о пользователе
 *  @property {Object} formData: информация о задании
 *  @property {Object} refuilingsData: данные о заправках
 *  @property {Object} refuilingsFormData: отображаемые данные о дозаправках
 *  @property {Object} sc.selectedRow: выбранная строка
 *  @property {Object} sc.uigridOptions: настройки таблицы Задач потевого листа
 *  @property {function} sc.addTask: добавление новой задачи
 *  @property {function} sc.cloneTask: копирование задачи (должнра быть выбрана строка в таблице)
 *  @property {function} sc.editTask: редактирование задачи (должнра быть выбрана строка в таблице)
 *  @property {function} sc.removeTask: удаление задачи (должнра быть выбрана строка в таблице)
 *  @property {function} needSelection: открывает модальное окно с требованием выбрать задачу для редактирование, удаление или копирования
 *  @property {function} sc.submitForm: выход с сохранением
 *  @property {function} sc.closeWayList: закрыть путевой лист
 *  @property {function} sc.cancelClick: отменить изменения
 *  @property {function} sc.refuelingClick: открыть модальное окно с дозаправками
 *  @property {function} setRefuilingsDataByOrderNum
 *  @property {function} sc.closeRefuelingModal закрытие модального окна дозаправок
 *  @property {function} sc.cancelRefuelingModal не сохранять изменения о дозаправках
 *  @property {function} sc.clearClick очистить информацию о дозаправках
 */

var app = angular.module("CloseWayBill", ['ngTouch', 'ui.grid', 'ui.grid.selection', "services", 'ngCookies', "modalWindows"]);
app.controller("CloseWayBillCtrl", ["$scope", "$http", "requests", "$location", '$cookieStore', '$stateParams', "modals", "modalType", function ($scope, $http, requests, $location, $cookieStore, $stateParams, modals, modalType) {
    var sc = $scope;

    var curUser = $cookieStore.get('currentUser');
     sc.tporgid = curUser.tporgid;
    sc.formData = {};
    var refuilingsData = [];
    sc.refuilingsFormData = {};
    sc.selectedRow = [];

    sc.uigridOptions = {
        enableRowSelection: true,
        rowHeight: 48,
        enableRowHeaderSelection: false,
        selectedItems: sc.selectedRow,
        multiSelect: false,
        afterSelectionChange: function (row) {
            sc.mySelections = [];
            sc.mySelections.push(row.entity);
        }
    };
    sc.uigridOptions.columnDefs = [
        {name: 'FieldName', displayName: '№ поля', width: '7%'},
        {name: 'PlantName', displayName: 'Культура', width: '10%'},
        {name: 'TaskName', displayName: 'Выполняемые работы', width: '14%'},
        {name: 'EquipmentName', displayName: 'Оборудование', width: '14%'},
        {
            name: 'WorkTime',
            displayName: 'Отработано часов',
            headerCellTemplate: 'templates/headerVertical.html',
            width: '4%'
        },
        {
            name: 'WorkDay',
            displayName: 'Норма выработки',
            headerCellTemplate: 'templates/headerVertical.html',
            width: '4%'
        },
        {name: 'Salary', displayName: 'Расценка', headerCellTemplate: 'templates/headerVertical.html', width: '4%'},
        {
            name: 'MotoHours',
            displayName: 'Отраб. мото часов',
            headerCellTemplate: 'templates/headerVertical.html',
            width: '4%'
        },
        {
            name: 'Volumefact',
            displayName: 'Фактически вып. объем',
            headerCellTemplate: 'templates/headerVertical.html',
            width: '4%'
        },
        {
            name: 'Area',
            displayName: 'Фактически условные га',
            headerCellTemplate: 'templates/headerVertical.html',
            width: '4%'
        },
        {
            name: 'PriceTotal',
            displayName: 'Общая оплата',
            headerCellTemplate: 'templates/headerVertical.html',
            width: '4%'
        },
        {
            name: 'PriceAdd',
            displayName: 'Доп. оплата',
            headerCellTemplate: 'templates/headerVertical.html',
            width: '4%'
        },
        {
            name: 'Consumption',
            displayName: 'Расход горючего всего, л',
            headerCellTemplate: 'templates/headerVertical.html',
            width: '4%'
        },
        {
            name: 'ConsumptionFact',
            displayName: 'Расход горючего фактически, л',
            headerCellTemplate: 'templates/headerVertical.html',
            width: '4%'
        },
        {name: 'AvailableName', displayName: 'Ответственный', width: '15%'}
    ];

    $scope.uigridOptions.onRegisterApi = function (gridApi) {
        //set gridApi on scope
        $scope.gridApi = gridApi;
    };
    /*
    requests.serverCall.post($http, "WaysListsCtrl/GetInfoForCreateWaysList",{OrgId : curUser.orgid, WaysListId:null}, function (data, status, headers, config) {
        console.info(data);
        sc.infoData = data;
    });
     */
	
    if ($stateParams.way_bill_id) {
        requests.serverCall.post($http, "WaysListsCtrl/GetCloseInfoByWaysList", {"WaysListId": $stateParams.way_bill_id}, function (data, status, headers, config) {
            console.info(data);
            sc.formData = data;
			sc.AllResource = data.AllResource;
            sc.uigridOptions.data = sc.formData.Tasks;
        }, function () {});
    } else {
        console.log('way_bill_id is null or undefined')
    }

    sc.addTask = function () {
        requests.serverCall.post($http, "WaysListsCtrl/UpdateCloseInfoByWaysList", sc.formData, function (data, status, headers, config) {
            console.info(data);
            $location.path('/close_bill_edit_task/' + $stateParams.way_bill_id + "/create/");
        }, function () {
        });
    };

    // коприрование сменного задания
    sc.cloneTask = function () {
        var selRow = $scope.gridApi.selection.getSelectedRows()[0];
        console.info($stateParams.way_bill_id);
        if (selRow && selRow.TaskId) {
            requests.serverCall.post($http, "WaysListsCtrl/UpdateCloseInfoByWaysList", sc.formData, function (data, status, headers, config) {//перед копированием все сохраняем
                console.info(data);
            }, function () {
            });
            requests.serverCall.post($http, "WaysListsCtrl/CopyTaskInWayList", selRow.TaskId, function (data, status, headers, config) {
                console.info(data);
                requests.serverCall.post($http, "WaysListsCtrl/GetCloseInfoByWaysList", {"WaysListId": $stateParams.way_bill_id}, function (data, status, headers, config) {
                    console.info(data);
                    sc.formData = data;
                    sc.uigridOptions.data = sc.formData.Tasks;
                }, function () {
                });

            });
        } else needSelection("Копирование", "Для копирования необходимо выбрать задание");
    };


    sc.editTask = function () {
        var selRow = $scope.gridApi.selection.getSelectedRows()[0];
        if (selRow && selRow.TaskId) {
            requests.serverCall.post($http, "WaysListsCtrl/UpdateCloseInfoByWaysList", sc.formData, function (data, status, headers, config) {
                console.info(data);
                console.info('selected id task ' + selRow.TaskId);
                $location.path('/close_bill_edit_task/' + $stateParams.way_bill_id + '/edit/' + selRow.TaskId);
            }, function () {
            });
        } else needSelection("Редактирование", "Для редактирования необходимо выбрать задание");
    };

    sc.removeTask = function () {
        var selRow = $scope.gridApi.selection.getSelectedRows()[0];
        if (selRow && selRow.TaskId) {
            console.info('deleted id task ' + $scope.gridApi.selection.getSelectedRows()[0].TaskId);
            requests.serverCall.post($http, "WaysListsCtrl/UpdateCloseInfoByWaysList", sc.formData, function (data, status, headers, config) {//перед копированием все сохраняем
                console.info(data);
            }, function () {
            });
            modals.showWindow(modalType.WIN_DIALOG, {
                    title: "Удаление", message: "Удалить выбранное задание?", callback: function () {
                        requests.serverCall.post($http, "WaysListsCtrl/DeleteTaskFromWaysList", selRow.TaskId, function (data, status, headers, config) {
                            console.info(data);
                            requests.serverCall.post($http, "WaysListsCtrl/GetCloseInfoByWaysList", {"WaysListId": $stateParams.way_bill_id}, function (data, status, headers, config) {
                                console.info(data);
                                sc.formData = data;
                                sc.uigridOptions.data = sc.formData.Tasks;
                            }, function () {
                            });

                        });
                    }

                }
            );
        } else needSelection("Удаление", "Для удаления необходимо выбрать задание");
    };

    function needSelection(title, message) {
        modals.showWindow(modalType.WIN_MESSAGE, {
            title: title,
            message: message,
            callback: function () {}
        });
    }

    sc.submitForm = function () {
        requests.serverCall.post($http, "WaysListsCtrl/UpdateCloseInfoByWaysList", sc.formData, function (data, status, headers, config) {
            console.info(data);
            $cookieStore.put('selectedWayBill', {id: $stateParams.way_bill_id});
            $location.path('/way_bills');
        }, function () {});
    };

    sc.closeWayList = function () {
        requests.serverCall.post($http, "WaysListsCtrl/ClosedWaysList", $stateParams.way_bill_id, function (data, status, headers, config) {
            console.info(data);
			
			//==
			  requests.serverCall.post($http, "WaysListsCtrl/UpdateCloseInfoByWaysList", sc.formData, function (data, status, headers, config) {
            console.info(data);
        }, function () {});
			//==
			
			
            $cookieStore.put('selectedWayBill', {id: $stateParams.way_bill_id});
            $location.path('/way_bills');
        }, function () {});
		
    };

    sc.cancelClick = function () {
        $cookieStore.put('selectedWayBill', {id: $stateParams.way_bill_id});
        $location.path('/way_bills');
    };

	
		sc.checkRes = function () {				
			var Orgid = null;
			if (sc.AllResource != true) {
				Orgid = curUser.orgid;
			} 
			      requests.serverCall.post($http, "WaysListsCtrl/GetRefuellerInfo", {OrgId : Orgid}, function (data, status, headers, config) {
					console.info(data);
					sc.refuellers1 = data;
					sc.refuellers2 = data;
					sc.refuellers3 = data;
					});
			}
	
    //----------------------дозаправки--------------------------------------------------------------------------------------------------------------------------------------------------------------------
    //----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    requests.serverCall.post($http, "WaysListsCtrl/GetRefuellerInfo", {OrgId : curUser.orgid, WaysListId:$stateParams.way_bill_id}, function (data, status, headers, config) {
        console.info(data);
        sc.refuellers1 = data;
        sc.refuellers2 = data;
        sc.refuellers3 = data;
    });

    sc.refuelingClick = function () {
          requests.serverCall.post($http, "WaysListsCtrl/UpdateCloseInfoByWaysList", sc.formData, function (data, status, headers, config) {
              console.info(data);
           });
        $("#refueling_modal").show().css({opacity: 1, display: "block"});
        sc.refuilingsFormData.Volume1 = sc.refuilingsFormData.NameId1 = '';
        sc.refuilingsFormData.Volume2 = sc.refuilingsFormData.NameId2 = '';
        sc.refuilingsFormData.Volume3 = sc.refuilingsFormData.NameId3 = '';
        requests.serverCall.post($http, "WaysListsCtrl/GetRefuelingsByIdList", $stateParams.way_bill_id, function (data, status, headers, config) {
            sc.refuilingsData = data;
            console.info(data);
            for (var j = 0, jl = sc.refuilingsData.length; j < jl; j++) {
                if (sc.refuilingsData[j].OrderNum == 1) {
                    sc.refuilingsFormData.Volume1 = sc.refuilingsData[j].Volume;
                    sc.refuilingsFormData.NameId1 = sc.refuilingsData[j].NameId;
                } else if (sc.refuilingsData[j].OrderNum == 2) {
                    sc.refuilingsFormData.Volume2 = sc.refuilingsData[j].Volume;
                    sc.refuilingsFormData.NameId2 = sc.refuilingsData[j].NameId;
                } else if (sc.refuilingsData[j].OrderNum == 3) {
                    sc.refuilingsFormData.Volume3 = sc.refuilingsData[j].Volume;
                    sc.refuilingsFormData.NameId3 = sc.refuilingsData[j].NameId;
                }
            }
        }, function () {
        });

    };

    function setRefuilingsDataByOrderNum(orderNum, volume, name) {
        if (volume) {
            var refuilBe = false;
            sc.refuilingsData.forEach(function (item, i, arr) {
                if (item.OrderNum == orderNum) {
                    item.Volume = volume;
                    item.NameId = name;
                    refuilBe = true;
                }
            });
            if (refuilBe == false) {
                sc.refuilingsData.push({
                    OrderNum: orderNum,
                    Volume: volume,
                    NameId: name
                });
            }
        } else {
            sc.refuilingsData.forEach(function (item, i, arr) {
                if (item.OrderNum == orderNum) {
                    sc.refuilingsData.splice(i, 1);
                }
            })
        }
    }

    sc.closeRefuelingModal = function () {
        sc.fullLoads = true;
        setRefuilingsDataByOrderNum(1, sc.refuilingsFormData.Volume1, sc.refuilingsFormData.NameId1);
        setRefuilingsDataByOrderNum(2, sc.refuilingsFormData.Volume2, sc.refuilingsFormData.NameId2);
        setRefuilingsDataByOrderNum(3, sc.refuilingsFormData.Volume3, sc.refuilingsFormData.NameId3);
        requests.serverCall.post($http, "WaysListsCtrl/UpdateRefuelings", {
            WaysListId: $stateParams.way_bill_id,
            Refuelings: sc.refuilingsData
        }, function (data, status, headers, config) {
            console.info(data);
         //   requests.serverCall.post($http, "WaysListsCtrl/UpdateCloseInfoByWaysList", sc.formData, function (data, status, headers, config) {
                console.info(data);
                requests.serverCall.post($http, "WaysListsCtrl/GetCloseInfoByWaysList", {"WaysListId": $stateParams.way_bill_id}, function (data, status, headers, config) {
                    console.info(data);
                    sc.formData = data;
                    sc.uigridOptions.data = sc.formData.Tasks;
                    $("#refueling_modal").css({opacity: 0, display: "none"});
                });
         //   });
        });
    };

    sc.cancelRefuelingModal = function () {
        $("#refueling_modal").css({opacity: 0, display: "none"});
    };

    sc.clearClick = function (num) {
        if (num == 1) sc.refuilingsFormData.Volume1 = sc.refuilingsFormData.NameId1 = '';
        else if (num == 2) sc.refuilingsFormData.Volume2 = sc.refuilingsFormData.NameId2 = '';
        else if (num == 3) sc.refuilingsFormData.Volume3 = sc.refuilingsFormData.NameId3 = '';
    };
}])
;

