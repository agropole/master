

(function () {
    var app = angular.module("services", ["modalWindows", "LogusConfig"]);
    /**
     *  @ngdoc service
     *  @name app.factory:requests
     *  @description
     *  # map
     *  Это фабрика, определяющая работу запросов на сервер
     *  @requires $http
     *  @requires modalType
     *  @requires modals
     *  @requires LogusConfig
     *  @requires preloader

     *  @return {function} serverCall вызоз запроса get или post

     */
    app.factory("requests", ["$http", "modals", "modalType", "LogusConfig", 'preloader', function ($http, modals, modalType, LogusConfig, preloader) {
        return {
            serverCall: {
                get: function ($http, servClass, successCallback, errorCallback, showErrorWindow, showPreloader) {
                    var url = LogusConfig.serverUrl + servClass;

                    if (typeof showPreloader == 'undefined' || showPreloader)
                        preloader.show();

                    // Если не передали параметр, то показывать окно
                    if (typeof showErrorWindow == 'undefined')
                        showErrorWindow = true;

                    console.info(url);
                    $http({
                        method: 'GET',
                        url: url
                    }).success(function (data, status, headers, config) {
                            preloader.hide();
                            if (typeof data.ErrorCode == 'undefined' || data.ErrorCode == 0) {
                                if (typeof successCallback != 'undefined' && successCallback != null) {
                                    successCallback(data.Value ? data.Value : data, status, headers, config);
                                } else {
                                    console.log(data.Value);
                                }
                            } else {
                                var eStr = "";
                                if (typeof data.ErrorList != 'undefined' && data.ErrorList != null) {
                                    data.ErrorList.forEach(function (item, i, arr) {
                                        eStr += '\n';
                                        eStr += item;
                                    });
                                }

                                console.warn('Server error on "' + config.url + '":\n[' + data.ErrorCode + '] ' + data.ErrorMessage + eStr);
                                if (showErrorWindow) {
                                    modals.showWindow(modalType.WIN_ERROR, {
                                        title: "Ошибка",
                                        message: data.ErrorMessage,
                                        values: data.ErrorList,
                                        callback: function () {
                                        }
                                    });
                                }
                                if (typeof errorCallback != 'undefined' && errorCallback != null) {
                                    errorCallback();
                                }
                            }
                        }).error(function (data, status, headers, config) {
                            preloader.hide();
                            console.warn(data);
                            console.warn(status);
                            if (typeof errorCallback != 'undefined' && errorCallback != null) {
                                errorCallback();
                            }
                        });

                },
                post: function ($http, servClass, d, successCallback, errorCallback, showErrorWindow, showPreloader) {
                    var url = LogusConfig.serverUrl + servClass;

                    if (typeof showPreloader == 'undefined' || showPreloader) {
                        preloader.show();
                    }

                    // Если не передали параметр, то показывать окно
                    if (typeof showErrorWindow == 'undefined')
                        showErrorWindow = true;

                    console.info(url);
                    console.info(d);
                    $http({
                        method: 'POST',
                        url: url,
                        data: {RequestObject: d},
                        headers: {'Authorization': 'auth'}
                    }).
                        success(function (data, status, headers, config) {
                            preloader.hide();
                            if (typeof data.ErrorCode == 'undefined' || data.ErrorCode == 0) {
                                if (typeof successCallback != 'undefined' && successCallback != null) {
                                    successCallback(data.Value ? data.Value : data, status, headers, config);
                                } else {
                                    console.log(data.Value);
                                }
                            } else {
                                var eStr = "";
                                if (typeof data.ErrorList != 'undefined' && data.ErrorList != null) {
                                    data.ErrorList.forEach(function (item, i, arr) {
                                        eStr += '\n';
                                        eStr += item;
                                    });
                                }

                                console.warn('Server error on "' + config.url + '":\n[' + data.ErrorCode + '] ' + data.ErrorMessage + eStr);
                                if (showErrorWindow) {
                                    modals.showWindow(modalType.WIN_ERROR, {
                                        title: "Ошибка",
                                        message: data.ErrorMessage,
                                        values: data.ErrorList,
                                        callback: function () {
                                        }
                                    });
                                }
                                if (typeof errorCallback != 'undefined' && errorCallback != null) {
                                    errorCallback();
                                }
                            }
                        }).
                        error(function (data, status, headers, config) {
                            preloader.hide();
                            console.warn(data);
                            console.warn(status);
                            if (typeof errorCallback != 'undefined' && errorCallback != null) {
                                errorCallback();
                            }
                        });
                }
            },
            addAndScroll: function (scope, data, formId) {
                scope.formData.push(data);
                var scroll = $(formId).find(".scroll-y");
                scroll.animate({scrollTop: scroll.find(".scroll-item").length * 258}, 'slow');
            }
            ,
            showSaveModal: function () {
                $("#save_modal").show().css({opacity: 1, display: "block"})
                setTimeout(function () {
                    $("#save_modal").css({opacity: 0, display: "none"});
                }, 1500);
            }
        };
    }
    ])
    ;
    /**
     *  @ngdoc service
     *  @name app.factory:preloader
     *  @description
     *  # map
     *  Это фабрика, определяющая работу preloader
     *  @requires $http

     *  @return {function} show показать прелоадер
     *  @return {function} hide скрыть прелоадер

     */
    app.factory("preloader", ["$http", function ($http) {
        return {
            show: function () {
                $("#preloader").css({display: "block"})
            },
            hide: function () {
                $("#preloader").css({display: "none"});
            }
        };
    }]);
})
();