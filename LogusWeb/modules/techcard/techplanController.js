var app = angular.module("TechPlan", ["ngGrid", "services", 'ngCookies', "modalWindows", "LogusConfig" ]);
app.controller("TechplanCtrl", ["$scope", "$http", "requests", "$location", "preloader","$stateParams", "LogusConfig",
					function ($scope, $http, requests, $location, preloader, $stateParams,LogusConfig) {

	var sc = $scope;
    var start = 0;

    sc.formData = {
        DateStart: new Date(),
        DateEnd: new Date(new Date().getTime() + 7 * 24 * 3600 * 1000)
    };
    
    requests.serverCall.get($http, "TechCard/GetTechReportInfo", function (data, status, headers, config) {
        data.TypeTraktor.splice(0,0, {Id: null, Name: 'Не выбрано'});
        data.TypeEqui.splice(0,0, {Id: null, Name: 'Не выбрано'});
        sc.infoData = data;
        start = 1;
        sc.refresh();
    }, function() {});

    sc.refresh = function(val)   {
        if (val === 1 && sc.formData.DateEnd < sc.formData.DateStart) {
            console.log('refresh1');
            sc.formData.DateEnd = new Date(sc.formData.DateStart.getTime() + 7 * 24 * 3600 * 1000)
        } else if (val === 2 && sc.formData.DateStart > sc.formData.DateEnd) {
            console.log('refresh2');
            sc.formData.DateStart = new Date(sc.formData.DateEnd.getTime() - 7 * 24 * 3600 * 1000)
        } else if (start === 1) {
            console.log('refresh3');
    	    requests.serverCall.post($http, "TechCard/GetTechReport", sc.formData, function (data, status, headers, config) {
                console.info(data);
                var data1 = data.TraktList;
        		var data2 = data.EquiList;
                sc.FreeTrakt = data.FreeTrakt;
                sc.FreeEqui = data.FreeEqui;
                $(function () {
                    $("#GanttContainer").ejGantt({
                        dataSource: data1,
                        allowSelection:true,
                        allowColumnResize: true,
                        taskIdMapping: "Id",
                        taskNameMapping: "Name",
                        startDateMapping: "StartDate",
                        durationMapping: "Duration",
                        childMapping: "Children",
                        treeColumnIndex: 1,
        				allowGanttChartEditing: false,
        				enableResize:true,				
                    });
                    $("#GanttContainer2").ejGantt({
                        dataSource: data2,
                        allowSelection:true,
                        allowColumnResize: true,
                        taskIdMapping: "Id",
                        taskNameMapping: "Name",
                        startDateMapping: "StartDate",
                        durationMapping: "Duration",
                        childMapping: "Children",
                        treeColumnIndex: 1,
        				allowGanttChartEditing: false,
        				enableResize:true,				
                    });
                });
            }, function () {
            });
        }
    };
	
	 
	
	 sc.refresh2 = function(val) {
		  sc.refresh(val);
	       gantt2 = Gantt.getGanttInstance();
		   gantt2.config.xml_date = "%d-%m-%Y";
		   gantt2.config.readonly = true;
		   gantt2.config.grid_width = 575;
		   //
		   gantt2.config.show_task_cells = false;
		   gantt2.config.branch_loading = true;
		   gantt2.config.scale_unit = "month";
		   gantt2.config.smart_scales = false;
		   //колонки
		   gantt3 = Gantt.getGanttInstance();
		   gantt3.config.xml_date = "%d-%m-%Y";
		   gantt3.config.readonly = true;
		   gantt3.config.grid_width = 575;
		   //
		   gantt3.config.show_task_cells = false;
		   gantt3.config.branch_loading = true;
		   gantt3.config.scale_unit = "month";
		   gantt3.config.smart_scales = false;
		   
	    gantt3.config.start_date = sc.formData.DateStart;
	    gantt3.config.end_date = sc.formData.DateEnd;
		   
		   
	   gantt2.config.columns = [
       {name:"text",       label:"Наименование работ",  tree:true, width: "30", resize: true },
       {name:"start_date", label:"Дата начала", align: "center" },
       {name:"duration",   label:"Продолжительность",   align: "center" },
	 //{name:"end_date", label:"",width: "0",  },
	   {name:"cost", label:"Стоимость",   align: "center",  width: "0" },
       ];
	   
	   gantt2.config.fit_tasks = false; 
	   gantt2.config.start_date = sc.formData.DateStart;
	   gantt2.config.end_date = sc.formData.DateEnd;

	  gantt3.config.columns =  [
       {name:"text",       label:"Наименование работ",  tree:true, width: "30", resize: true },
       {name:"start_date", label:"Дата начала", align: "center" },
       {name:"duration",   label:"Продолжительность",   align: "center" },
	 //{name:"end_date", label:"",width: "0",  },
	   {name:"cost", label:"Стоимость",   align: "center",  width: "0" },
       ];
	   
	    if (val === 1 && sc.formData.DateEnd < sc.formData.DateStart) {
            console.log('refresh1');
            sc.formData.DateEnd = new Date(sc.formData.DateStart.getTime() + 7 * 24 * 3600 * 1000)
        } else if (val === 2 && sc.formData.DateStart > sc.formData.DateEnd) {
            console.log('refresh2');
            sc.formData.DateStart = new Date(sc.formData.DateEnd.getTime() - 7 * 24 * 3600 * 1000)
        } else if (start === 1) {
            console.log('refresh3');	 
		}
		  
		 //fill red 
		function renderLabel(progress, color) {
        var relWidth = progress;
		var cssClass = "custom_progress";
		return "<div id='mn1' class='" + cssClass + "' style='width:" + relWidth + "%; background-color: " + color + "'   > </div>";

	}

	
	Date.prototype.addDays = function(days) {
      var date = new Date(this.valueOf());
      date.setDate(date.getDate() + days);
      return date;
        }
		
		function  getHtmlForGantt(task, AllData) {
		
	 var renderHtml = '';
	if (task.parent == 0) {
	var arrTask = [];
  //     console.log(sc.TechData);
	  for (var i=0; i < AllData.length; i++) {
	   if (AllData[i].parent == task.id) {
	   arrTask.push(AllData[i]);
	   }
	  }
	//  console.log(arrTask);
	  //2 step
	  var arrDates = [];
	  for (var i=0; i < arrTask.length; i++) {
	  
	   for (var j=0; j < arrTask.length; j++) {
	  
	  if (i != j && arrTask[i].start_date <= arrTask[j].end_date &&
	  arrTask[j].start_date <= arrTask[i].end_date) {
		//start_date
		if (arrTask[j].start_date >= arrTask[i].start_date) {
	     var date1 = arrTask[j].start_date ; 
		 } else {
		   var date1 = arrTask[i].start_date ;
		 }
		 //end_date
		 if (arrTask[j].end_date <= arrTask[i].end_date) {
	     var date2 = arrTask[j].end_date.addDays(-1); 
		 } else {
		   var date2 = arrTask[i].end_date.addDays(-1); 
		 }
		 arrDates.push({'date1': date1, 'date2': date2 }) ;
		  	
	  }  
	  }
	  }	  
	//= закрашивание по дням
	   var oneDay = 100 / task.duration;
	   var day = task.start_date;
	   for (var i=0; i < task.duration; i++) {
		    
		var checkDay = 0; var checkWhite = 0;
	//	console.log(arrDates.length);
		 
	    for (var j=0; j < arrDates.length; j++) {
			
		//белым то, что не входит в диапазон 
			  if (day < sc.formData.DateStart || day > sc.formData.DateEnd)  {
				 renderHtml = renderHtml + renderLabel(oneDay, "white") ;  
                  checkDay = 1;
		            break;			 
			  }
			//red
	    if (day >= arrDates[j].date1 && day <= arrDates[j].date2)  {
		   renderHtml = renderHtml + renderLabel(oneDay, "red") ;
		   checkDay = 1;
		   break;
		}
	   }
	   
	   		//white
			if (checkDay == 0) {
		    for (var j=0; j < arrTask.length; j++) {
			if (day >= arrTask[j].start_date && day <= arrTask[j].end_date) {
				 checkWhite = 1;
				 break;
			 }	 
		 }
		if (checkWhite == 0) {
			 renderHtml = renderHtml + renderLabel(oneDay, "white") ;  
		}
			}
			
	   //green
	   if (checkDay == 0 && checkWhite != 0) {  
	   if (day < sc.formData.DateStart || day > sc.formData.DateEnd)  {
		    renderHtml = renderHtml + renderLabel(oneDay, "white") ;  
	   } else  {
	     renderHtml = renderHtml + renderLabel(oneDay, "#32CD32") ;  
	   }
	   }
	   
	   day = day.addDays(1);
	  }
	  
	  }
      //console.log(renderHtml);
        return  renderHtml;	
			
		}
		
			gantt2.templates.task_text = function (start, end, task) {
               return   getHtmlForGantt(task, sc.TechData);
	         };
      
	  	 gantt3.templates.task_text = function (start, end, task) {
               return   getHtmlForGantt(task, sc.EquipData);
	         };
	  
	     gantt2.templates.rightside_text = function(start, end, task)  {
            if(task.type == gantt2.config.types.milestone){
                return task.text;
            }
            return "";
        };
		 gantt2.init("gantt_here2");
		 
	     gantt3.templates.rightside_text = function(start, end, task)  {
            if(task.type == gantt3.config.types.milestone){
                return task.text;
            }
            return "";
        };
		 gantt3.init("gantt_here3");

	    requests.serverCall.post($http, "TechCard/GetTechEqiepReport", sc.formData, function (data, status, headers, config) {
		 sc.TechData = data.data;
		 sc.EquipData = data.data2;
		 gantt2.clearAll(); 
		 gantt2.parse({data : data.data });	    
         gantt3.clearAll(); 
		 gantt3.parse({data : data.data2 });			   
          }, function () {
           });
	 } 
	   sc.refresh2();
		
		sc.printReport = function() {
		   var year = new Date().getFullYear();
		   var popupWin = window.open(LogusConfig.serverUrl + 'CostCtrl/GetPlanChartTechResExcel?techIdType=' + sc.formData.TypeTraktorId
																							   + '&eqIdType=' + sc.formData.TypeEquiId
				                                                                               + '&year=' + year, '_blank');
		
		}
		
	
}]);