var app = angular.module("TMCPlan", ["ngGrid", "services", 'ngCookies', "modalWindows"]);
app.controller("TMCplanCtrl", ["$scope", "$http", "requests", "$location", "preloader","$stateParams", '$cookieStore',
					function ($scope, $http, requests, $location, preloader, $stateParams, $cookieStore) {

	var sc = $scope;
    var start = 0;
	sc.infoData = {};
	sc.allData = {};
	var curUser = $cookieStore.get('currentUser');
    sc.formData = {
        DateStart: new Date(),
        DateEnd: new Date(new Date().getTime() + 7 * 24 * 3600 * 1000)
    };
    //
	    sc.selectedRow = [];
        sc.gridOptions = {
        enableColumnResize: false,
        enableRowSelection: false,
        showFooter: false,
        rowHeight: 49,
        headerRowHeight: 49,
        useExternalSorting: true,
        data: 'TableData',
        selectedItems: sc.selectedRow,
        afterSelectionChange: function (row) {
            console.info(sc.selectedRow);
            if (row.entity) {
                if (row.selected) {
                    sc.selectedRow = [row];
                } else {
                    if (sc.selectedRow && sc.selectedRow > 0) {
                        sc.selectedRow.splice(sc.tableDataFirst.indexOf(row.entity), 1);
                    }
                }
            }
        },
        beforeSelectionChange: function (row, event) {
            angular.forEach(sc.TCData, function (data, index) {
                if (data.selected == true) sc.selectedRow.splice(index, 1);
                sc.gridOptions.selectRow(index, false);
            });
            return true;
        }
    };
    sc.gridOptions.columnDefs = [
        {
            field: "Name", displayName: "Название", width: "50%", cellClass: 'cellToolTip',
              cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()">' +
            '<input value="{{row.entity[col.field]}}" ng-model="row.entity[col.field]" placeholder="Название" class="bordernone" readonly>' +
            '</div>', headerCellTemplate: "<div   ng-class=\"'colt' + col.index\" class=\"ngHeaderText\">{{col.displayName}}</div>"
        },
		 {
            field: "Sum", displayName: "Стоимость", width: "50%",
             cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()">' +
            '<input value="{{row.entity[col.field]}}" ng-model="row.entity[col.field]" placeholder="Стоимость" class="bordernone" readonly>' +
            '</div>',headerCellTemplate: "<div   ng-class=\"'colt' + col.index\" class=\"ngHeaderText\">{{col.displayName}}</div>"
        },
        {field: "Id", width: "0%"}
    ];
	
	    requests.serverCall.post($http, "ActsSzrCtrl/GetInfoBalanceMaterials", {OrgId:curUser.orgid }, function (data, status, headers, config) {
       // console.info(data);
		data.TypeMaterials.shift();
		data.TypeMaterials.unshift({Id : -1, Name : "Все"});
        sc.infoData.TypeTMC = data.TypeMaterials;
		sc.formData.TmcId = -1;
	    start = 1;
        sc.refresh2();
    }, function () {
    });
	
	 sc.refresh2 = function(val) {
		   gantt.config.xml_date = "%d-%m-%Y";
		   gantt.config.readonly = true;
		   gantt.config.grid_width = 575;
		  // console.log(gantt.config.columns);
		   gantt.config.show_task_cells = false;
		   gantt.config.branch_loading = true;
		   gantt.config.scale_unit = "month";
		   gantt.config.smart_scales = false;
		   
		  gantt.config.fit_tasks = false; 
		  gantt.config.start_date = sc.formData.DateStart;
	      gantt.config.end_date = sc.formData.DateEnd;
		   
		   //колонки
	   gantt.config.columns = [
       {name:"text",       label:"Наименование работ",  tree:true, width: "270", resize: true },
	   {name:"consumption", label:"Количество",align: "center"  },
       {name:"cost", label:"Стоимость",align: "center"  },
       {name:"start_date", label:"Дата начала", align: "center" },
       {name:"duration",   label:"Продолжительность",   align: "center" },
	 //{name:"end_date", label:"",width: "0",  },
       ];
  
	    if (val === 1 && sc.formData.DateEnd < sc.formData.DateStart) {
            console.log('refresh1');
            sc.formData.DateEnd = new Date(sc.formData.DateStart.getTime() + 7 * 24 * 3600 * 1000)
        } else if (val === 2 && sc.formData.DateStart > sc.formData.DateEnd) {
            console.log('refresh2');
            sc.formData.DateStart = new Date(sc.formData.DateEnd.getTime() - 7 * 24 * 3600 * 1000)
        } else if (start === 1) {
            console.log('refresh3');	 
		}
		 
     requests.serverCall.post($http, "TechCard/GetTmcMaterial",sc.formData, function (data, status, headers, config) {
	  console.log(data);
	  sc.TableData = data;
    }, function() {});

	   gantt.templates.rightside_text = function(start, end, task){
            if(task.type == gantt.config.types.milestone){
                return task.text;
            }
            return "";
        };
		 gantt.init("gantt_here");
	
	function createBox(sizes, class_name){
			var box = document.createElement('div');
			box.style.cssText = [
				"height:" + sizes.height + "px",
				"line-height:" + sizes.height + "px",
				"width:" + sizes.width + "px",
				"top:" + sizes.top + 'px',
				"left:" + sizes.left + "px",
				"position:absolute",
				"background-color: #32CD32;"
			].join(";");
			box.className = class_name;
			return box;
		}

		gantt.addTaskLayer(function show_hidden(task) {
			
			if (gantt.hasChild(task.id)) {
				var sub_height = gantt.config.row_height - 5,
					el = document.createElement('div'),
					sizes = gantt.getTaskPosition(task);

				var sub_tasks = gantt.getChildren(task.id);
				var child_el;

				for (var i = 0; i < sub_tasks.length; i++) {
					var child = gantt.getTask(sub_tasks[i]);
					var child_sizes = gantt.getTaskPosition(child);

					child_el = createBox({
						height: sub_height,
						top:sizes.top,
						left:child_sizes.left,
						width: child_sizes.width,
					}, "child_preview gantt_task_line");
					child_el.innerHTML =  child.text;
				//	console.log(child_el);
					el.appendChild(child_el);
				}
				return el;
			}
		
			return false;
			
		});
		
	    requests.serverCall.post($http, "TechCard/GetTMCReport", sc.formData, function (data, status, headers, config) {
           console.info(data);
		   gantt.clearAll(); 
		   gantt.parse(data);
				     
          }, function () {
           });
	 }
	 

		 
	sc.refresh2();
	
    sc.refresh = function(val) {
	
    };

}]);