/**
 *  @ngdoc controller
 *  @name TC.controller:Techcard
 *  @description
 *  # Techcard
 *  Это контроллер, определеяющий поведение форму редактирования/создания ТК
 *  @requires $scope
 *  @requires $http
 *  @requires requests
 *  @requires $location
 *  @requires modals
 *  @requires modalType
 *  @requires LogusConfig

 *  @property {Object} sc scope
 *  @property {string} sc.formData данные ТК
 *  @property {Object} sc.formData.YearId текущий год
 *  @property {Object} sc.formData.PlantId текущая культура
 *  @property {Object} sc.formData.SortId текущий сорт. Не может быть выбрано, если указано поле
 *  @property {Object} sc.formData.FieldId текущее поле. Не может быть выбрано, если указан сорт
 *  @property {Object} sc.DataNZP НЗП незавершенное производство. Считается на сервере
 *  @property {Object} sc.DataWorks данные таблицы работ, топлива и зарплаты
 *  @property {Object} sc.DataSupportStuff данные таблицы вспомогательного персонала
 *  @property {Object} sc.DataSzr данные таблицы СЗР
 *  @property {Object} sc.DataChemicalFertilizers данные таблицы Мин. удобрений
 *  @property {Object} sc.DataSeed данные таблицы Семена
 *  @property {Object} sc.DataServices данные таблицы Услуги
 *  @property {Object} sc.DataTotal данные таблицы Свода
 *  @property {Object} sc.formData.Productivity урожайность
 *  @property {Object} sc.formData.GrossHarvest валовый сбор

 *  @property {Object} sc.gridTotal настройки таблицы Свод
 *  @property {Object} sc.gridOptions_NZP настройки таблицы НЗП
 *  @property {Object} sc.gridOptions_Works настройки таблицы Работы
 *  @property {Object} sc.gridOptions_Tech настройки таблицы Техника
 *  @property {Object} sc.gridOptions_Emp настройки таблицы Персонал
 *  @property {Object} sc.gridOptions_SupportStuff настройки таблицы Вспомгательный персонал
 *  @property {Object} sc.gridOptions_Szr настройки таблицы СЗР
 *  @property {Object} sc.gridOptions_ChemicalFertilizers настройки таблицы Мин.удобрений
 *  @property {Object} sc.gridOptions_Seed настройки таблицы Семян
 *  @property {Object} sc.gridOptions_Services настройки таблицы Услуг
 *  @property {Object} sc.gridOptions_NZP настройки таблицы СЗР
        
 *  @property {function} clearTCForm очистить данные
 *  @property {function} setCommonGridOption установка настроек таблиц
 *  @property {function} refreshYear изменение года
 *  @property {function} sc.refreshField изменение поля
 *  @property {function} sc.changeMainData пересчет после изменения значения полей Урожайность или Площади
 *  @property {function} recountTotal пересчет таблицы Свод
 *  @property {function} recountTotalOne пересчет одной строки в таблице Свод
 *  @property {function} sc.countNZP получение расчета данных по НЗП
 *  @property {function} sc.getIndex получение номера строки в таблицах Работы, Персонал, Техника
 *  @property {function} sc.createTask добавление новой работы
 *  @property {function} typesDrop типы расчета работ
 *  @property {function} sc.getFlagName получить название типа расчета
 *  @property {function} sc.updateType расчет работы после изменения типа расчета
 *  @property {function} sc.changeWorkData расчет после изменения данных
 *  @property {function} sc.changeWorkload расчет после изменения Объема работ
 *  @property {function} recountData пересчет таблиц
 *  @property {function} sc.dateChange изменение дат работ
 *  @property {function} sc.updateSelectedCell открытие модальных окон со справочниками
 *  @property {function} changeWork изменение Типа работы
 *  @property {function} recountRow пересчет строки работы
 *  @property {function} sc.delRowWork удаление строки работы
 *  @property {function} sc.recountFuel пересчитать топлиова
 *  @property {function} sc.getValueForInput получение строки для отображения списка оборудования или техники
 *  @property {function} sc.editCell редактирование комментария
 *  @property {function} commentClick модальное окно добавления комментария
 *  @property {function} sc.getStuffRowIndex нумерация строк таблицы в соответствии с номерами Работ
 *  @property {function} sc.createNewSupportStaff добавление новой записи Вспомогательного персонала
 *  @property {function} recountRowSupportStaff пересчет зп Вспомогательного персонала
 *  @property {function} getRowWork получить данные по соответствующей работе
 *  @property {function} sc.delRowSupportStaff удалить запись Вспомогательного персонала
 *  @property {function} sc.changeNorm  пересчет после изменения Нормы выработки
 *  @property {function} sc.getMaterialRowIndex индекс строки в соответствии с работой
 *  @property {function} sc.createNewSZR добавление новой записи СЗР
 *  @property {function} sc.createNewFertilizer добавление новой записи Мин.удобрений
 *  @property {function} sc.createNewSeed добавление новой записи Семена
 *  @property {function} createNewMaterial генерация новой записи Материалов
 *  @property {function} recountRowMaterial пересчет записи материалов
 *  @property {function} sc.delRowSZR удаление записи материалов
 *  @property {function} sc.createNewServices добавление новой записи услуг
 *  @property {function} sc.recountService пересчет суммы услуг
 *  @property {function} sc.delRowService удаление записи услуг
 *  @property {function} sc.submitForm выход с сохранением
 *  @property {function} sc.cancel выход без сохранения
 *  @property {function} sc.loadTcById загрузка информации по карте
 */

var app = angular.module("Techcard", ["ngGrid", "services", 'ngCookies', "modalWindows"]);
app.controller("Techcard", ["$scope", "$http", "requests", "$location", '$cookieStore', "modals", "modalType", "preloader","$stateParams", function ($scope, $http, requests, $location, $cookieStore, modals, modalType, preloader, $stateParams) {
    var sc = $scope;
    sc.formData = {};
	var CheckChange;
    if ($stateParams.id && $stateParams.id != 0) {
        sc.formData.Id = $stateParams.id;
    }
	if ($cookieStore.get('CheckChange') != null) {
	console.log($cookieStore.get('CheckChange').id);
	CheckChange = $cookieStore.get('CheckChange').id;
	// $cookieStore.put('CheckChange', {id: 2});
	}
    var met=0;
    var clearTCForm = function() {
        sc.formData.PlantId = null;
        sc.formData.SortId = null;
        sc.DataNZP = [{
            Energy: 0,
            Salary: 0,
            SZR:0,
            ChemicalFertilizers: 0,
            Seed: 0,
			DopMaterial: 0,
            Total: 0,
        }];
        sc.DataWorks = [];
        sc.DataSupportStuff = [];
        sc.DataSzr = [];
        sc.DataChemicalFertilizers = [];
        sc.DataSeed = [];
		sc.DataDopMaterial = [];
        sc.DataServices = [];
        sc.DataTotal = [
            {Name: 'Всего', Total: 0, PerGa: 0, PerT: 0, ParamIds: [4,5,6]},
            {Name: 'НЗП', Total: 0, PerGa: 0, PerT: 0, ParamIds: [7,8,9]},
            {Name: 'ЗП', Total: 0, PerGa: 0, PerT: 0, ParamIds: [10,11,12]},
            {Name: 'Отчисления', Total: 0, PerGa: 0, PerT: 0, ParamIds: [13,14,15]},
            {Name: 'Топливо', Total: 0, PerGa: 0, PerT: 0, ParamIds: [16,17,18]},
            {Name: 'Семена', Total: 0, PerGa: 0, PerT: 0, ParamIds: [19,20,21]},
            {Name: 'Удобрения', Total: 0, PerGa: 0, PerT: 0, ParamIds: [22,23,24]},
            {Name: 'СЗР', Total: 0, PerGa: 0, PerT: 0, ParamIds: [25,26,27]},
            {Name: 'Пр. материалы', Total: 0, PerGa: 0, PerT: 0, ParamIds: [61,62,63]},
            {Name: 'Услуги', Total: 0, PerGa: 0, PerT: 0, ParamIds: [28,29,30]},
        ];
    };

    clearTCForm();

    sc.formData.Productivity = 0;
    sc.formData.GrossHarvest = 0;

    var setCommonGridOption = function(newGridOpt) {
        var commonGridOptions = {
            enableColumnResize: false,
            enableRowSelection: false,
            showFooter: false,
            rowHeight: 49,
            headerRowHeight: 49,
            useExternalSorting: true
        };
        for(var p in commonGridOptions) {
            newGridOpt[p] = commonGridOptions[p];
        };
    };

    requests.serverCall.get($http, "TechCard/GetInfoTCDetail", function (data, status, headers, config) {
        console.info(data);
        sc.infoData = data;
        //года
        //работы
        //оборудование
        //техника
        //сзр
        //удобрения
        //семена
        sc.formData.YearId = $stateParams.year ? $stateParams.year: new Date().getFullYear() + 1;
        refreshYear(true);
    }, function () {
    });

     var refreshYear = function(first) {
        clearTCForm();
        requests.serverCall.post($http, "TechCard/GetPlantSortFieldByYear", sc.formData.YearId, function (data, status, headers, config) {
            console.info(data);
            sc.Plants = data;
            refreshPlant();
            if (first) {
                sc.refreshYear = refreshYear;
                if (sc.formData.Id) loadTcById();
            }
        }, function () {
        });
    };

    var flagPlantChange;
    var refreshPlant = function() {
        console.log('REFRESH PLANT ' + flagPlantChange);
        if (flagPlantChange !== 'field') {
            sc.Fields = [];
            sc.formData.Area = 0;
            
            if (sc.formData.PlantId) {
                
                for (var i = 0, li = sc.Plants.length; i < li; i++) {
                    var p = sc.Plants[i];
                    if (p.Id === sc.formData.PlantId) {
                        sc.infoData.FieldList = p.Fields;
                        if (sc.formData.SortId) {
                            sc.formData.FieldId = null;
                            for (var j = 0, lj = p.Values.length; j < lj; j++) {
                                var s = p.Values[j];
                                if (s.Id === sc.formData.SortId) {
                                    sc.Fields = s.Fields;
                                }
                            }
                        }
                        if (sc.Fields.length === 0) {
                            sc.Fields = p.Fields;
                        }
                        for (var i= 0, li = sc.Fields.length; i<li; i++) {
                            
                            sc.formData.Area += sc.Fields[i].Area;
                        }
						
                    }
                }			
            }
            if (!sc.formData.FieldId)
            for (var i= 0, li=sc.DataWorks.length; i<li; i++) {
                sc.changeWorkData(sc.DataWorks[i]);
            }
        } else {
            flagPlantChange = null;
        }
    };
    sc.refreshPlant = refreshPlant;

    sc.refreshField = function() {
        console.log('REFRESH FIELD ' + flagPlantChange);
        //if (flagPlantChange !== 'plant1' && flagPlantChange !== 'plant2') {
            flagPlantChange = 'field';
            if (sc.formData.FieldId) {
                sc.formData.SortId = null;
                for (var i=0, li = sc.infoData.FieldList.length; i<li; i++) {
                    if (sc.infoData.FieldList[i].Id === sc.formData.FieldId) {
                        sc.formData.Area = sc.infoData.FieldList[i].Area;
                        sc.Fields = [sc.infoData.FieldList[i]];
                    }
                }
                for (var i= 0, li=sc.DataWorks.length; i<li; i++) {
                    sc.changeWorkData(sc.DataWorks[i]);
                }
            } else {
                flagPlantChange = null;
            }
    };

    sc.$watch("formData.SortId", function(SortId) {
        refreshPlant();
    });

    sc.changeMainData = function() {
        sc.formData.GrossHarvest = (sc.formData.Area * sc.formData.Productivity).toFixed(2);
        recountData(sc.WorkData, sc.changeWorkData);
        recountTotal();
	sc.DataTotal[1].PerT = (sc.DataTotal[1].Total/sc.formData.GrossHarvest).toFixed(2);
	sc.DataTotal[1].PerGa = (sc.DataTotal[1].Total/sc.formData.Area).toFixed(2);
	sc.DataTotal[0].PerT = (sc.DataTotal[1].Total/sc.formData.GrossHarvest).toFixed(2);
                 if (sc.DataTotal[1].PerT == Infinity || isNaN(sc.DataTotal[1].PerT)) {sc.DataTotal[1].PerT=0;}
	             if (sc.DataTotal[1].PerGa == Infinity || isNaN(sc.DataTotal[1].PerGa)) {sc.DataTotal[1].PerGa=0;}
		         if (sc.DataTotal[1].Total == Infinity || isNaN(sc.DataTotal[1].Total)) {sc.DataTotal[1].Total=0;}
				 if (sc.DataTotal[0].PerT == Infinity || isNaN(sc.DataTotal[0].PerT)) {sc.DataTotal[0].PerT=0;}
    };

    //----------------------------------------------------------Total-------------------------------------------------------------------------------
    sc.gridTotal = {};
    setCommonGridOption(sc.gridTotal);
    sc.gridTotal.data = 'DataTotal';
    sc.gridTotal.columnDefs = [
        {
            field: "Name", displayName: "Свод затрат", width: "25%", cellClass: 'cellToolTip',
            cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()">' +
            '<input value="{{row.entity[col.field]}}" style="border: none;" readonly>' +
            '</a>'
        },
        {
            field: "Total", displayName: "Всего", width: "25%", cellClass: 'cellToolTip',
            cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()">' +
            '<input value="{{row.entity[col.field]}}" style="border: none;text-align:right;" readonly>' +
            '</a>',headerCellTemplate: "<div style=\"text-align:center;\"  ng-class=\"'colt' + col.index\" class=\"ngHeaderText\">{{col.displayName}}</div>"
        },
        {
            field: "PerGa", displayName: "на 1 га", width: "25%", cellClass: 'cellToolTip',
            cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()">' +
            '<input value="{{row.entity[col.field]}}" style="border: none;text-align:right;" readonly>' +
            '</a>',headerCellTemplate: "<div style=\"text-align:center;\"  ng-class=\"'colt' + col.index\" class=\"ngHeaderText\">{{col.displayName}}</div>"
        },
        {
            field: "PerT", displayName: "на 1 тн", width: "25%", cellClass: 'cellToolTip',
            cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()">' +
            '<input value="{{row.entity[col.field]}}" style="border: none;text-align:right;margin-left: -15px;" readonly>' +
            '</a>',headerCellTemplate: "<div style=\"text-align:center;\"  ng-class=\"'colt' + col.index\" class=\"ngHeaderText\">{{col.displayName}}</div>"
        }
    ];

    var recountTotal = function(ind) {
        ind = ind? ind: ['npz', 'zp', 'fuel','seed','fertilizer','szr','service'];
        for (var k= 0, lk=ind.length; k<lk; k++)
        {
            var sum = 0;
            switch (ind[k]) {
                case 'nzp':
                    for (var p in sc.DataNZP[0]) {
                        sum += sc.DataNZP[0][p];
                    };
                    sc.DataTotal[1].Total = parseFloat(sum).toFixed(2);
					sc.DataTotal[1].PerT=(sc.DataTotal[1].Total/sc.formData.GrossHarvest).toFixed(2);
					sc.DataTotal[1].PerGa=(sc.DataTotal[1].Total/sc.formData.Area).toFixed(2);
                 if (sc.DataTotal[1].PerT==Infinity || isNaN(sc.DataTotal[1].PerT)) {sc.DataTotal[1].PerT=0;}
	             if (sc.DataTotal[1].PerGa==Infinity || isNaN(sc.DataTotal[1].PerGa)) {sc.DataTotal[1].PerGa=0;}
		         if (sc.DataTotal[1].Total==Infinity || isNaN(sc.DataTotal[1].Total)) {sc.DataTotal[1].Total=0;}
	  
                    recountTotalOne(parseFloat(sc.DataTotal[1]));
                    break;
                case 'zp':
                    for (var i = 0, li = sc.DataWorks.length; i < li; i++) {
                        sum += parseFloat(sc.DataWorks[i].EmpCost);
                    }
                    for (var i = 0, li = sc.DataSupportStuff.length; i < li; i++) {
                        sum += parseFloat(sc.DataSupportStuff[i].Cost);
                    }
			
                    sc.DataTotal[2].Total = sum.toFixed(2);
                    sc.DataTotal[3].Total = (sum * 0.321).toFixed(2);
                    recountTotalOne(sc.DataTotal[2]);
                    recountTotalOne(sc.DataTotal[3]);
                    break;
                case 'fuel':
                    for (var i = 0, li = sc.DataWorks.length; i < li; i++) {
                        sum += parseFloat(sc.DataWorks[i].FuelCost);
                    }
				
                    sc.DataTotal[4].Total = parseFloat(sum).toFixed(2);
                    recountTotalOne(sc.DataTotal[4]);
                    break;
                case 'seed':
                    if (sc.DataSeed) {
                        for (var i = 0, li = sc.DataSeed.length; i < li; i++) {
                            sum += parseFloat(sc.DataSeed[i].Cost);
                        }
                        sc.DataTotal[5].Total = parseFloat(sum).toFixed(2); 
                        recountTotalOne(sc.DataTotal[5]);
                    }
                    break;
                case 'fertilizer':
                    if (sc.DataChemicalFertilizers) {
                        for (var i = 0, li = sc.DataChemicalFertilizers.length; i < li; i++) {
                            sum += parseFloat(sc.DataChemicalFertilizers[i].Cost);
                        }
                        sc.DataTotal[6].Total = parseFloat(sum).toFixed(2);
                        recountTotalOne(sc.DataTotal[6]);
                    }
                    break;
					  case 'dopMaterial':
                    if (sc.DataDopMaterial) {
                        for (var i = 0, li = sc.DataDopMaterial.length; i < li; i++) {
                            sum += parseFloat(sc.DataDopMaterial[i].Cost);
                        }
                        sc.DataTotal[8].Total = parseFloat(sum).toFixed(2);
                        recountTotalOne(sc.DataTotal[8]);
                    }
                    break;				
                case 'szr':
                    if (sc.DataSzr) {
                        for (var i = 0, li = sc.DataSzr.length; i < li; i++) {
                            sum += parseFloat(sc.DataSzr[i].Cost);
							
                        }
                        sc.DataTotal[7].Total = parseFloat(sum).toFixed(2);
                        recountTotalOne(sc.DataTotal[7]);
                    }
                    break;
                case 'service':
                    var s = 0;
                    for (var i=0, li=sc.DataServices.length; i<li; i++) {
                        s += parseFloat(sc.DataServices[i].Cost);
                    }
                    sc.DataTotal[9].Total = parseFloat(s).toFixed(2);
					      recountTotalOne(sc.DataTotal[9]);
                    break;
            }
        }

        //sc.DataTotal[0] = {Name: 'Всего', Total: 0, PerGa: 0, PerT: 0};
        sc.DataTotal[0].Total = 0;
        sc.DataTotal[0].PerGa = 0;
        sc.DataTotal[0].PerT = 0;
        for (var i= 1, li=sc.DataTotal.length; i<li; i++) {
          sc.DataTotal[0].Total = parseFloat(sc.DataTotal[0].Total) + parseFloat(sc.DataTotal[i].Total);
          sc.DataTotal[0].PerGa =  parseFloat(sc.DataTotal[0].PerGa) + parseFloat(sc.DataTotal[i].PerGa);
          sc.DataTotal[0].PerT =  parseFloat(sc.DataTotal[0].PerT) + parseFloat(sc.DataTotal[i].PerT);
        }
	    sc.DataTotal[0].Total = sc.DataTotal[0].Total.toFixed(2);
	//	console.log(sc.DataTotal[0].Total);
		 sc.DataTotal[0].PerGa =sc.DataTotal[0].PerGa.toFixed(2);
		 sc.DataTotal[0].PerT =sc.DataTotal[0].PerT.toFixed(2);
    };

    var recountTotalOne = function(d) {
        d.PerT = (d.Total / sc.formData.GrossHarvest).toFixed(2);
        d.PerGa = (d.Total / sc.formData.Area).toFixed(2);
		 if (d.PerT==Infinity || isNaN(d.PerT)) {d.PerT=0;}
	     if (d.PerGa==Infinity || isNaN(d.PerGa)) {d.PerGa=0;}
		 if (d.Total==Infinity || isNaN(d.Total)) {d.Total=0;}
    };
    //----------------------------------------------------------end-Total---------------------------------------------------------------------------
    //----------------------------------------------------------НЗП---------------------------------------------------------------------------------
    sc.gridOptions_NZP = {};
    setCommonGridOption(sc.gridOptions_NZP);
    sc.gridOptions_NZP.data = 'DataNZP';
    sc.gridOptions_NZP.columnDefs = [
        {
            field: "Energy", displayName: "ГСМ и энергия", width: "20%", cellClass: 'cellToolTip',
            cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()">' +
            '<input value="{{row.entity[col.field]}}" style="border: none;" readonly>' +
            '</a>'
        },
        {
            field: "Salary", displayName: "Зарплата", width: "20%",
            cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()">' +
            '<input value="{{row.entity[col.field]}}" style="border: none;" readonly>' +
            '</div>'
        },
        {
            field: "SZR", displayName: "СЗР", width: "15%",
            cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()">' +
            '<input value="{{row.entity[col.field]}}" style="border: none;" readonly>' +
            '</div>'
        },
        {
            field: "ChemicalFertilizers", displayName: "Мин. удобрения", width: "15%",
            cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()">' +
            '<input value="{{row.entity[col.field]}}" style="border: none;" readonly>' +
            '</div>'
        },
        {
            field: "Seed", displayName: "Семена", width: "15%",
            cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()">' +
            '<input value="{{row.entity[col.field]}}" style="border: none;" readonly>' +
            '</div>'
        },
		 {
            field: "DopMaterial", displayName: "Пр. материалы", width: "15%",
            cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()">' +
            '<input value="{{row.entity[col.field]}}" style="border: none;" readonly>' +
            '</div>'
        }
    ];

    sc.countNZP = function() {
		var FieldsId=[];
				for (var i = 0, li = sc.Fields.length; i < li; i++) {
		FieldsId.push(sc.Fields[i].Id);	
				}
				console.log(FieldsId);
        requests.serverCall.post($http, "TechCard/GetNZP",{
            YearId:  sc.formData.YearId,
			Fields : sc.Fields,
			Area : parseFloat(sc.formData.Area)
        }, function (data, status, headers, config) {
            console.info(data);
            sc.DataNZP = data;
            recountTotal(['nzp']);
        }, function () {
        });
    };

    
    //----------------------------------------------------------end-НЗП---------------------------------------------------------------------------------
    //----------------------------------------------------------Работы----------------------------------------------------------------------------------
    sc.gridOptions_Works = {};
    setCommonGridOption(sc.gridOptions_Works);
    sc.gridOptions_Works.data = 'DataWorks';
    sc.gridOptions_Works.columnDefs = [
        {
            field: "index", displayName: " ", width: "5%",
            cellTemplate: '<div class="{{getClassChanges()}} ngCellText" ng-class="col.colIndex()">' +
            '<textinputnotcomma data="getIndex(row.entity)" ng-model="row.entity[col.field]" class="bordernone" readonly="1"/>' +
            '</div>'
        },
		 {
            field: "Id1", displayName: " ", width: "0%",
		    cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()">' +
            '<input value="{{row.entity[col.field]}}" class="bordernone" ng-model="row.entity[col.field]" placeholder="" readonly>' +
           '</div>'
        },
        {
            field: "Name", displayName: "Наименование", width: "20%",
            cellTemplate: '<a tooltip="{{row.entity[col.field][\'Name\']}}" tooltip-popup-delay="1000" tooltip-class="my-custom-tooltip" tooltip-placement="top" tooltip-append-to-body="true">' +
            '<div class="{{getClassChanges()}} ngCellText" ng-class="col.colIndex()" ng-click="editCell(row.entity, row.getProperty(col.field), col.field)">' +
            '<input value="{{row.entity[col.field][\'Name\']}}" placeholder="Наимнование работы" style="border: none;"  data-ng-click="updateSelectedCell(col.field, row.entity[col.field][\'Id\'],\'№ поля\',row.entity,$event)"  readonly>' +
            '</div>'
        },
        {
            field: "UnicFlag", displayName: "Тип", width: "15%",
            cellTemplate: '<a>' +
            '<div class="{{getClassChanges()}} ngCellText" ng-class="col.colIndex()" ng-click="editCell(row.entity, row.getProperty(col.field), col.field)">' +
            '<input ng-value="getFlagName(row.entity[col.field])" placeholder="Тип" style="border: none;"  data-ng-click="updateType(col.field, row.entity[col.field],\'Тип работ\',row.entity,$event)"  readonly>' +
            '</div></a>'
        },
        {
            field: "Count", displayName: "Кратность", width: "10%",
            cellTemplate: '<div class="{{getClassChanges()}} ngCellText" ng-class="col.colIndex()">' +
           '<textinputnotcomma value="{{row.entity[col.field]}}" data="row.entity[col.field]" placeholder="0" regex-name="numbersWithPoint" field-change="changeWorkData(row.entity)" readonly="row.entity.UnicFlag"></textinputnotcomma>' +
           '</div>'
	//	 cellTemplate: '<div class="{{getClassChanges()}} ngCellText" ng-class="col.colIndex()">' +
    //            '<input value="{{row.entity[col.field]}}" ng-model="row.entity[col.field]" placeholder="0" ng-change="changeWorkData(row.entity)" readonly="row.entity.UnicFlag">' +
    //            '</div>'
        },
        {
            field: "Factor", displayName: "Коэф.", width: "10%",
            cellTemplate: '<div class="{{getClassChanges()}} ngCellText" ng-class="col.colIndex()">' +
            '<textinputnotcomma value="{{row.entity[col.field]}}" data="row.entity[col.field]" regex-name="numbersWithPoint" field-change="changeWorkData(row.entity)" readonly="row.entity.UnicFlag"></textinputnotcomma>' +
            '</div>'
        },
        {
            field: "Workload", displayName: "Объем работ", width: "10%",
            cellTemplate: '<div class="{{getClassChanges()}} ngCellText" ng-class="col.colIndex()">' +
            '<textinputnotcomma value="{{row.entity[col.field]}}" data="row.entity[col.field]" regex-name="numbersWithPoint" field-change="changeWorkload(row.entity)" readonly="!row.entity.UnicFlag"></textinputnotcomma>' +
            '</div>'
        },
        {
            field: "DateStart",
            displayName: "Начало",
            width: "10%",
            cellTemplate:'<a tooltip="{{row.getProperty(col.field)}}" tooltip-popup-delay="1000" tooltip-class="my-custom-tooltip" tooltip-placement="top" tooltip-append-to-body="true">' +
            '<datepicktable class="{{getClassChanges()}} logus-headers col-md-2 col-xs-4 col-sm-2 col-lg-2" style="padding-top: 6px;margin-left: -22px;font-size: 15px;" data="row.entity[col.field]" mask="\'99.99.9999\'" regex-name="\'date\'" date-change="dateChange(row.entity, curDate)">' +
            '</datepicktable>' +
            '</a>'
        },
        {
            field: "DateEnd",
            displayName: "Конец",
            width: "10%",
            cellTemplate:'<a tooltip="{{row.getProperty(col.field)}}" tooltip-popup-delay="1000" tooltip-class="my-custom-tooltip" tooltip-placement="top" tooltip-append-to-body="true">' +
            '<datepicktable class="{{getClassChanges()}} logus-headers col-md-2 col-xs-4 col-sm-2 col-lg-2" style="padding-top: 6px;margin-left: -22px;font-size: 15px;" data="row.entity[col.field]" mask="\'99.99.9999\'" regex-name="\'date\'" date-change="dateChange(row.entity, curDate)">' +
            '</datepicktable>' +
            '</a>'
        },
        {
            field: "DayCount", displayName: "Дни", width: "5%",
            cellTemplate: '<div class="{{getClassChanges()}} ngCellText" ng-class="col.colIndex()">' +
            '<input value="{{row.entity[col.field]}}" ng-model="row.entity[col.field]" class="bordernone" readonly>' +
            '</div>'
        },
        {
            field: "Delete", displayName: " ", width: "5%",
            cellTemplate: '<div class="{{getClassChanges()}} ngCellText clear_row"  style="font-size: 36px!important;margin: auto;margin-top: 7px;" ng-switch on="row.entity[col.field]" ng-click="delRowWork(row.entity)"></div>'   
        },
        {field: "Id", width: "0%"}
    ];

    sc.getIndex = function(row) {
        return sc.DataWorks.indexOf(row) + 1;
    };

    sc.createTask = function() {
        var work = {
            Id: null,//sfsd
            Name: null,//sfsd
            Count: 0,//sfsd
            Factor: 0,
            Workload: 0,//sfsd
            DateStart: new Date(),//sfsd
            DateEnd: new Date(),//sfsd
            DayCount: 0,//sfsd
            Equipment: null,
            Tech: null,
            Comment: '',//sfsd
            Consumption: 0,//sfsd
            FuelType: null,//sfsd
            FuelCost: 0,//sfsd
            ShiftRate: 0,//sfsd
            ShiftCount: 0,//sfsd
            EmpPrice: 0,
            EmpCost: 0,
            UnicFlag: 0, //sfsd
			Rate : 0
        };
        sc.DataWorks.push(work);
        var grid = sc.gridOptions_Works.ngGrid;
        setTimeout(function() {
            grid.$viewport.scrollTop(sc.DataWorks.length * grid.config.rowHeight + 50);
        }, 100);
    };

    var typesDrop = [
        {Id: 0, Name:'По площади'},
        {Id: 1, Name:'Штучные(по сменам)'},
        {Id: 2, Name:'Штучные(по объему)'}
    ];

    sc.getFlagName = function(id) {
        for (var i=0,li = typesDrop.length; i<li; i++) {
            if (typesDrop[i].Id == id) {
                return typesDrop[i].Name;
            }
        }
    };
    sc.updateType = function (colField, selectedVal, displayName, row) {
        sc.colField = colField;
        var typeModal = modalType.WIN_SELECT;
        var modalValues = typesDrop;

        modals.showWindow(typeModal, {
            nameField: displayName,
            values: modalValues,
            selectedValue: selectedVal
        })
            .result.then(function (result) {
                console.info(sc.selectedColumn);
                console.info(result);
                  console.info(row);
                row.UnicFlag = result.Id;
                if (row.UnicFlag === 1 || row.UnicFlag === 2) {
                    row.Factor = 1;
                    row.Count = 1;
                }

                for (var i= 0, li = sc.DataSupportStuff.length; i<li; i++) {
                    if (row.Name.Id === sc.DataSupportStuff[i].NameSelected.Id) {
                        if (row.UnicFlag === 1 || row.UnicFlag === 2) {
                            sc.DataSupportStuff[i].Count = 1;
                        }
                        recountRowSupportStaff(sc.DataSupportStuff[i]);
                    }
                }
            }, function () {
                console.info('Modal dismissed at: ' + new Date());
            });
    };

    sc.changeWorkData = function(row) {
        if (!row.UnicFlag) {
            if (sc.formData.Area && row.Factor && row.Count) {
                row.Workload = row.Count * row.Factor * sc.formData.Area;
            } else {
                row.Workload = 0;
            }
            recountRow(row);

            recountData(sc.DataSupportStuff, recountRowSupportStaff);
            recountData(sc.DataSzr, recountRowMaterial);
            recountData(sc.DataChemicalFertilizers, recountRowMaterial);
            recountData(sc.DataSeed, recountRowMaterial);
            recountData(sc.DataDopMaterial, recountRowMaterial);
            recountTotal();
			console.log("пересчет все");
        }
    };
     //пересчет обьем работ
    sc.changeWorkload = function(row) {
		 row.FuelCount = row.Consumption*row.Workload;
        if (!row.UnicFlag && row.Workload) {
            recountRow(row);

            recountData(sc.DataSupportStuff, recountRowSupportStaff);
            recountData(sc.DataSzr, recountRowMaterial);
            recountData(sc.DataChemicalFertilizers, recountRowMaterial);
            recountData(sc.DataSeed, recountRowMaterial);
            recountData(sc.DataDopMaterial, recountRowMaterial);
            recountTotal();
		//	console.log("пересчет все");
        }
    };

    var recountData = function(data, f) {
        if (data) {
            for (var i = 0, li = data.length; i < li; i++) {
                f(data[i]);
            }
        }
    }

    sc.dateChange = function(row, date) {
		
        if (row.DateEnd && row.DateStart) {
            if (row.DateStart > row.DateEnd) {
                row.DateEnd = row.DateStart;
            }
			console.log(new Date(row.DateEnd));
           var timeDiff = new Date(row.DateEnd).getTime() - new Date(row.DateStart).getTime();
            row.DayCount = Math.ceil(timeDiff / (1000 * 3600 * 24)) + 1;
        }
		
    };

	sc.checkFillRow = function(arr1,arr2) {
			//====выделение
					   for (var i = 0; i < arr1.length; i++) {
						if (arr2 != null && arr2.length != 0 ) {
						   for (var j = 0; j < arr2.length; j++) {	
						     arr1[i].selectedFlag = false;
						   if (arr2[j].Id == arr1[i].Id ) { 
					  	       arr1[i].selectedFlag = true;
							   break;
							}  }
							} else {
							      arr1[i].selectedFlag = false;
							}
							   }
							//====	
		
		
	}
	
	
	
	
    sc.updateSelectedCell = function (colField, selectedVal, displayName, row) {
        sc.colField = colField;
        var typeModal = modalType.WIN_SELECT;
        var modalValues;
        if (sc.infoData) {
            switch (sc.colField) {
                case "Name":
					displayName="Наименование работ";
                    modalValues = sc.infoData.WorkList;
                    break;
                case "FuelType":
                    modalValues = sc.infoData.TypeFuelList;
                    break;
                case "Equipment":
				      	//====выделение
					  sc.checkFillRow(sc.infoData.EquipmentList,row.Equipment );
							//====	
                    typeModal = modalType.WIN_SELECT_MULTI;
                    modalValues = sc.infoData.EquipmentList;
                    selectedVal = row[sc.colField] ? row[sc.colField] : [];
                    break;
                case "Tech":
				//====выделение
					  sc.checkFillRow(sc.infoData.TechList,row.Tech );
							//====	
                    typeModal = modalType.WIN_SELECT_MULTI;
                    modalValues = sc.infoData.TechList;
                    selectedVal = row[sc.colField] ? row[sc.colField] : [];
                    break;
                case "NameSZR":
                    modalValues = sc.infoData.SzrList;
                    break;
                case "NameFertilizer":
                    modalValues = sc.infoData.FertilizerList;
                    break;
                case "NameSeed":
			
                    modalValues = sc.infoData.SeedList;
                    break;
					case "NameDopMaterial":
                    modalValues = sc.infoData.DopMaterialList;
                    break;
                case "NameSelectedWorkForMaterial":
				   var selected_works = [];
                    for (var i = 0;i<sc.DataWorks.length;i++) {
					console.log(sc.DataWorks[i]);
					
					
				//	selected_works.push({Name: sc.DataWorks[i].Name.Name,
				selected_works.push({Name: (i+1) + " " + sc.DataWorks[i].Name.Name,
					                     Id : sc.DataWorks[i].Name.Id,
					                     index :i+1,
										 id_tc_oper: sc.DataWorks[i].Id,
										 Id1 : sc.DataWorks[i].Id1,
					});
					
                    }
					console.log(selected_works);
					
					
					
				  modalValues = selected_works;	
				break;
                case "NameSelected":
				displayName="Наименование работ";
                    var selected_works = [];
                    for (var i = 0;i<sc.DataWorks.length;i++) {
					
					selected_works.push({Name: sc.DataWorks[i].Name.Name,
					                     Id : sc.DataWorks[i].Name.Id,
					                     index :i+1,
										 id_tc_oper: sc.DataWorks[i].Id,
										 Id1 : sc.DataWorks[i].Id1,
					});
					
                    }
					  for (var i = 0; i<sc.DataSupportStuff.length; i++) {
						   for (var j = 0;j<selected_works.length; j++) {
							 if (sc.DataSupportStuff[i].index == selected_works[j].index)
						   {
						 selected_works.splice(selected_works.indexOf(selected_works[j]),1);
						   }
						   }
					  }
				
					console.log(selected_works);
                    modalValues = selected_works;
                    break;
                case "NameService":
                    modalValues = sc.infoData.ServiceList; 
            }

            modals.showWindow(typeModal, {
                nameField: displayName,
                values: modalValues,
                selectedValue: selectedVal
            })
                .result.then(function (result) {
					//подтягивание Расценок
                    console.info(sc.selectedColumn);
                    console.info(result);
					  console.info(row);
					   console.info(sc.colField);
					  // console.info('row.Tech[0].Id');
                      //
		if (row.Tech!=null)	{if (row.Tech.length!=0) { var techId=row.Tech[0].Id} else {var techId=null;}} else {var techId=null;}	
        if (row.Equipment!=null)   { if (row.Equipment.length!=0) { var equiId=row.Equipment[0].Id} else {var equiId=null;}	} else 	{var equiId=null;}	
        if (row.Name!=null) {var tptasId=row.Name.Id;} else {var tptasId=null;}		
					  //
					
         if (sc.colField === "Tech") {if (result.length!=0) {techId=result[0].Id;}}	  //техника
		 if (sc.colField === "Equipment") { if (result.length!=0) {equiId=result[0].Id;}}	  
		 if (sc.colField === "Name") {if (result.length!=0) {tptasId=result.Id;}}	  
		 
		 if (sc.colField != "FuelType") {
		   requests.serverCall.post($http, "TechCard/GetRate",{
            tptasId:tptasId, //тип работы
			techId: techId,  //техника
			equiId : equiId, //оборудование
			plantId : sc.formData.PlantId,   //культура
			operationId : row.Id
        }, function (data, status, headers, config) {
            console.info(data);
			//id_tc_operation 
			if (data.Id != null && met != 0) {row.Id1 = met + 1; met = met + 1; }
			if (data.Id != null && met == 0) {row.Id1=data.Id; met=data.Id; };
			
			
           if (data.FuelConsumption!=null) { row.Consumption = data.FuelConsumption;}  //расход топлива
              row.FuelCount = row.Consumption*row.Workload;  //потребность
			if (techId != null) {row.FuelType = data.Typefuel;}     //тип топлива
		  if (row.FuelType != null)  {   row.FuelCost = (row.FuelCount*row.FuelType.Price).toFixed(2); recountTotal(['fuel']);  }   //Стоимость
			if (data.ShiftOutput != null)  {row.ShiftRate=data.ShiftOutput; row.ShiftCount=(row.Workload/row.ShiftRate).toFixed(2);}  //см. норма
			if (data.RateShift != null)  {row.Rate=data.RateShift;}  //тариф
			if (data.RatePiecework != null)  {row.EmpPrice=data.RatePiecework;}  //расценка
			if (row.Rate != null && row.ShiftCount != null  &&  row.EmpPrice != null  && row.Workload != null) {row.EmpCost = (row.Rate*row.ShiftCount + row.EmpPrice*row.Workload).toFixed(2);}
			else {row.EmpCost=0;}
        }, function () {
        });
		 }  
	  if (row.FuelType!=null)  {row.FuelCost = (row.FuelCount*row.FuelType.Price).toFixed(2); recountTotal(['fuel']); }     //Стоимость
		 
					//
				 
                    if (sc.colField === "Name" && row["Name"]) {
                        var old = row["Name"].Id;
                    }
                    row[colField] = result;
                  
                    if (sc.colField === "Name") {
						 
                        recountRow(row);
                        if (old) changeWork(old, row.Name);
                        recountTotal(['zp', 'fuel', 'seed','fertilizer','szr']);
                    } else if (sc.colField === "Tech") {
                        for (var i= 0,li=sc.infoData.TechList.length; i<li; i++) {
                            sc.infoData.TechList[i].selectedFlag = false;
                        }
                    } else if (sc.colField === "Equipment") {
                        for (var i= 0,li=sc.infoData.EquipmentList.length; i<li; i++) {
                            sc.infoData.EquipmentList[i].selectedFlag = false;
                        }
                    } else if (sc.colField === "NameSelected") {
						
						row.index=result.index;
						if (result.Id1 != null) {
						row.id_tc_oper = result.Id1;	
						} else {
						row.id_tc_oper = result.id_tc_oper;
						;}
                        recountRowSupportStaff(row);
                    } else if (sc.colField === 'NameSelectedWorkForMaterial')  {
						console.log(result.Name);
						var str = result.Name.replace(/[0-9]/g, '');
						row.NameSelectedWorkForMaterial.Name = str;
						row.index = result.index;
						if (result.Id1 != null) {
						row.id_tc_oper = result.Id1;
                        console.log(row);						
						console.log(result.Id1);
						} else {
						row.id_tc_oper = result.id_tc_oper;
						console.log(result.id_tc_oper);
						;}
					}
					else if (sc.colField === 'NameFertilizer' || sc.colField === 'NameSeed' || sc.colField === 'NameSZR' || sc.colField==='NameDopMaterial') {
					
                        row.Price = result.Price;
						console.log('dfdf');
						sc.recountRowMaterial(row); //пересчет стоимости
						if (result.Price == 0) {row.Cost = 0;}
                    } else if (sc.colField === "FuelType") {
                        sc.recountFuel(row);
                    }
                }, function () {
                    console.info('Modal dismissed at: ' + new Date());
                });
        } else {
            console.log('не пришла sc.infoData');
        }
    };

    var changeWork = function(oldId, newWork) {
        for (var i= 0, li = sc.DataSupportStuff.length; i<li; i++) {
            if (sc.DataSupportStuff[i].NameSelected.Id === oldId) {
                sc.DataSupportStuff[i].NameSelected = newWork;
            }
        }

        changeWorkInMaterial(sc.DataSzr);
        changeWorkInMaterial(sc.DataChemicalFertilizers);
        changeWorkInMaterial(sc.DataSeed);
        changeWorkInMaterial(sc.DataDopMaterial);
        function changeWorkInMaterial(array) {
            for (var i= 0, li = array.length; i<li; i++) {
                if (array[i].NameSelectedWorkForMaterial.Id === oldId) {
                    array[i].NameSelectedWorkForMaterial = newWork;
                }
            }
        }
    };

    var recountRow = function(row) {
		
    };
      //удаление работы
    sc.delRowWork = function(row) {
        console.log(sc.DataWorks.indexOf(row));
        console.log(row);
		//удаление материалов
	//	DataChemicalFertilizers
		 for (var i = 0; i<sc.DataChemicalFertilizers.length; i++) {
							 if (sc.DataChemicalFertilizers[i].index == sc.DataWorks.indexOf(row)+1)
						   {
						 sc.DataChemicalFertilizers.splice(sc.DataChemicalFertilizers.indexOf(sc.DataChemicalFertilizers[i]),1); i--;
						   }  
					  }
		//	DataSzr
		 for (var i = 0; i<sc.DataSzr.length; i++) {
							 if (sc.DataSzr[i].index == sc.DataWorks.indexOf(row)+1)
						   {
						 sc.DataSzr.splice(sc.DataSzr.indexOf(sc.DataSzr[i]),1);i--;
						   }  
					  }
			//	DataSeed
		 for (var i = 0; i<sc.DataSeed.length; i++) {
							 if (sc.DataSeed[i].index == sc.DataWorks.indexOf(row)+1)
						   {
						 sc.DataSeed.splice(sc.DataSeed.indexOf(sc.DataSeed[i]),1);i--;
						   }  
					  }
			//	DataDopMaterial
		 for (var i = 0; i<sc.DataDopMaterial.length; i++) {
							 if (sc.DataDopMaterial[i].index == sc.DataWorks.indexOf(row)+1)
						   {
						 sc.DataDopMaterial.splice(sc.DataDopMaterial.indexOf(sc.DataDopMaterial[i]),1);i--;
						   }  
					  }
		
		//удаление вспомогательный персонал
			  for (var i = 0; i<sc.DataSupportStuff.length; i++) {
							 if (sc.DataSupportStuff[i].index == sc.DataWorks.indexOf(row)+1)
						   {
						 sc.DataSupportStuff.splice(sc.DataSupportStuff.indexOf(sc.DataSupportStuff[i]),1);
						   }  
					  }
		//пересчет индексов вспомогательного персонала
		 for (var i = 0; i<sc.DataSupportStuff.length; i++) {
			 if (sc.DataSupportStuff[i].index>(sc.DataWorks.indexOf(row)+1))
			 {
				sc.DataSupportStuff[i].index=sc.DataSupportStuff[i].index-1; 
			 }
		 }
		//1
		 for (var i = 0; i<sc.DataChemicalFertilizers.length; i++) {
			 if (sc.DataChemicalFertilizers[i].index>(sc.DataWorks.indexOf(row)+1))
			 {
				sc.DataChemicalFertilizers[i].index=sc.DataChemicalFertilizers[i].index-1; 
			 }
		 }
		//2
		 for (var i = 0; i<sc.DataSzr.length; i++) {
			 if (sc.DataSzr[i].index>(sc.DataWorks.indexOf(row)+1))
			 {
				sc.DataSzr[i].index=sc.DataSzr[i].index-1; 
			 }
		 }
		//3
		 for (var i = 0; i<sc.DataSeed.length; i++) {
			 if (sc.DataSeed[i].index>(sc.DataWorks.indexOf(row)+1))
			 {
				sc.DataSeed[i].index=sc.DataSeed[i].index-1; 
			 }
		 }
		 //4
		  for (var i = 0; i<sc.DataDopMaterial.length; i++) {
			 if (sc.DataDopMaterial[i].index>(sc.DataWorks.indexOf(row)+1))
			 {
				sc.DataDopMaterial[i].index=sc.DataDopMaterial[i].index-1; 
			 }
		 }
		 
		//
        sc.DataWorks.splice(sc.DataWorks.indexOf(row), 1);
		
		//
		  recountTotal(['zp']);
        //TODO проверить и удалить строки из всех таблиц материалов и Вспомогательного персонала
    };


    //----------------------------------------------------------end-Работы------------------------------------------------------------------------------
    //----------------------------------------------------------Техника---------------------------------------------------------------------------------
    sc.gridOptions_Tech = {};
    setCommonGridOption(sc.gridOptions_Tech);
    sc.gridOptions_Tech.data = 'DataWorks';
    sc.gridOptions_Tech.columnDefs = [
        {
            field: "index", displayName: " ", width: "5%",
            cellTemplate: '<div class="{{getClassChanges()}} ngCellText" ng-class="col.colIndex()">' +
            '<textinputnotcomma data="getIndex(row.entity)" ng-model="row.entity[col.field]" class="bordernone" readonly="1"/>' +
            '</div>'
        },
        {
            field: "Name", displayName: "Тип работ", width: "20%",
            cellTemplate: '<a tooltip="{{row.entity[col.field][\'Name\']}}" tooltip-popup-delay="1000" tooltip-class="my-custom-tooltip" tooltip-placement="top" tooltip-append-to-body="true">' +
            '<div class="{{getClassChanges()}} ngCellText" ng-class="col.colIndex()">' +
            '<input value="{{row.entity[col.field][\'Name\']}}" style="border: none;" readonly>' +
            '</div>'
        },
        {
            field: "Tech", displayName: "Агрегат", width: "20%",
            cellTemplate: '<div class="{{getClassChanges()}} ngCellText" ng-class="col.colIndex()" style="padding: 0px 5px;">' +
            '<input ng-value="getValueForInput(row.entity.Tech)" placeholder="Трактор, комбайн, автомобиль" style="border: none; height: 49%" data-ng-click="updateSelectedCell(\'Tech\', row.entity.Tech.Id,\'Техника\',row.entity,$event)"  readonly>' +
            '<input ng-value="getValueForInput(row.entity.Equipment)" placeholder="Машины, оборудование" style="border: none; height: 49%"  data-ng-click="updateSelectedCell(\'Equipment\', row.entity.Equipment.Id,\'Оборудование\',row.entity,$event)"  readonly>' +
            '</div>'
        },
        {
            field: "Consumption", displayName: "Расход,л/ед", width: "10%",
            cellTemplate: '<div class="{{getClassChanges()}} ngCellText" ng-class="col.colIndex()">' +
            '<input value="{{row.entity[col.field]}}" ng-model="row.entity[col.field]" placeholder="Расход" ng-change="recountFuel(row.entity)">' +
            '</div>'
        },
        {
            field: "FuelCount", displayName: "Потребность,л", width: "10%",
            cellTemplate: '<div class="{{getClassChanges()}} ngCellText" ng-class="col.colIndex()">' +
            '<input ng-model="row.entity.FuelCount" placeholder="Потребность" readonly class="bordernone">' +
            '</div>'
        },
        {
            field: "FuelType", displayName: "Тип", width: "8%",
            cellTemplate: '<a tooltip="{{row.entity[col.field][\'Name\']}}" tooltip-popup-delay="1000" tooltip-class="my-custom-tooltip" tooltip-placement="top" tooltip-append-to-body="true">' +
            '<div class="{{getClassChanges()}} ngCellText" ng-class="col.colIndex()" ng-click="editCell(row.entity, row.getProperty(col.field), col.field)">' +
            '<input value="{{row.entity[col.field][\'Name\']}}" placeholder="Тип" style="border: none;"    readonly>' +
            '</div>'
        },
        {
            field: "FuelPrice", displayName: "Цена, р/л", width: "10%",
            cellTemplate: '<div class="{{getClassChanges()}} ngCellText" ng-class="col.colIndex()">' +
            '<input value="{{row.entity.FuelType.Price ? row.entity.FuelType.Price : 0}}" ng-model="row.entity.FuelType.Price" placeholder="Цена" readonly class="bordernone">' +
            '</div>'
        },
        {
            field: "FuelCost", displayName: "Стоимость, р", width: "12%",
            cellTemplate: '<div class="{{getClassChanges()}} ngCellText" ng-class="col.colIndex()">' +
            '<input value="{{row.entity[col.field]}}" class="bordernone" ng-model="row.entity[col.field]" placeholder="Стоимость" readonly>' +
            '</div>'
        },
        {
            field: "Comment", displayName: " ", width: "5%",
            cellTemplate: 'templates/comment_shift_template.html', cellClass: 'cellToolTip'
        },
        {field: "Id", width: "0%"}
    ];

    sc.recountFuel = function(row) {
		   row.FuelCount = row.Consumption*row.Workload; 
		   row.FuelCost = (row.FuelCount*row.FuelType.Price).toFixed(2); 
        recountTotal(['fuel']);
	
    };

    sc.getValueForInput = function(array) {
        var str = "";
        if (array && array.length > 0) {
            for (var i = 0, li = array.length; i < li; i++) {
                str += array[i].Name + ", "
            }
            str = str.substr(0, str.length - 2);
        }
        return str;
    };

    sc.editCell = function (row, cell, column) {
        if (column == 'Comment') commentClick(cell);
    };

    function commentClick(comment) {
        modals.showWindow(modalType.WIN_COMMENT, {
            nameField: "Комментарий",
            comment: comment
        })
            .result.then(function (result) {
                console.info(result);
                sc.selectedRow[0]['Comment'] = result;
                //TODO sendTask();
            }, function () {
                console.info('Modal dismissed at: ' + new Date());
            });
    }
    //----------------------------------------------------------end-Техника-----------------------------------------------------------------------------
    //----------------------------------------------------------Персонал--------------------------------------------------------------------------------
    sc.gridOptions_Emp = {};
    setCommonGridOption(sc.gridOptions_Emp);
    sc.gridOptions_Emp.data = 'DataWorks';
    sc.gridOptions_Emp.columnDefs = [
        {
            field: "index", displayName: " ", width: "5%",
            cellTemplate: '<div class="{{getClassChanges()}} ngCellText" ng-class="col.colIndex()">' +
            '<textinputnotcomma data="getIndex(row.entity)" ng-model="row.entity[col.field]" class="bordernone" readonly="1"/>' +
            '</div>'
        },
        {
            field: "Name", displayName: "Наименование работ", width: "35%",
            cellTemplate: '<a tooltip="{{row.entity[col.field][\'Name\']}}" tooltip-popup-delay="1000" tooltip-class="my-custom-tooltip" tooltip-placement="top" tooltip-append-to-body="true">' +
            '<div class="{{getClassChanges()}} ngCellText" ng-class="col.colIndex()">' +
            '<input value="{{row.entity[col.field][\'Name\']}}" style="border: none;" readonly>' +
            '</div>'
        },
        {
            field: "ShiftRate", displayName: "См.норма,ед", width: "10%",
            cellTemplate: '<div class="{{getClassChanges()}} ngCellText" ng-class="col.colIndex()">' +
            '<input value="{{row.entity[col.field]}}" ng-model="row.entity[col.field]" placeholder="Норма выработки" ng-change="changeNorm(row.entity)">' +
            '</div>'
        },
        {
            field: "ShiftCount", displayName: "Смены", width: "10%",
            cellTemplate: '<div class="{{getClassChanges()}} ngCellText" ng-class="col.colIndex()">' +
            '<input value="{{row.entity[col.field]}}" ng-model="row.entity[col.field]" placeholder="Кол-во смен" class="bordernone" readonly>' +
            '</div>'
        },
		 {
            field: "Rate", displayName: "Тариф", width: "15%",
            cellTemplate: '<div class="{{getClassChanges()}} ngCellText" ng-class="col.colIndex()">' +
            '<input value="{{row.entity[col.field]}}" ng-model="row.entity[col.field]" placeholder="0" ng-change="changeNorm(row.entity)">' +
            '</div>'
        },
        {
            field: "EmpPrice", displayName: "Расценка, руб/смена", width: "10%",
            cellTemplate: '<div class="{{getClassChanges()}} ngCellText" ng-class="col.colIndex()">' +
            '<input value="{{row.entity[col.field]}}" ng-model="row.entity[col.field]" placeholder="Расценка" ng-change="changeNorm(row.entity)">' +
            '</div>'
        },
        {
            field: "EmpCost", displayName: "Стоимость, руб", width: "15%",
            cellTemplate: '<div class="{{getClassChanges()}} ngCellText" ng-class="col.colIndex()">' +
            '<input value="{{row.entity[col.field]}}" ng-model="row.entity[col.field]" placeholder="Стоимость" class="bordernone" readonly>' +
            '</div>'
        },
        {field: "Id", width: "0%"}
    ];

    sc.changeNorm = function(row) {
	 row.ShiftCount=(row.Workload/row.ShiftRate).toFixed(2); // смены
	 row.EmpCost = (row.Rate*row.ShiftCount + row.EmpPrice*row.Workload).toFixed(2);	//стоимость
	 if (row.ShiftCount==Infinity) {row.ShiftCount=0;}
        recountTotal();
    };
    //----------------------------------------------------------end-Персонал----------------------------------------------------------------------------
    //----------------------------------------------------------Вспомогательный-Персонал----------------------------------------------------------------

    sc.getStuffRowIndex = function(id) {
	//	console.log(id);
        for (var i =0, li=sc.DataWorks.length; i<li; i++) {
			console.log(row.index);
			return row.index;
			if (sc.DataWorks[i].Name!=null) {
            if (sc.DataWorks[i].Name.Id === id) {
               return i+1;
           }
			}
        }
	
    };

    sc.gridOptions_SupportStuff = {};
    setCommonGridOption(sc.gridOptions_SupportStuff);
    sc.gridOptions_SupportStuff.data = 'DataSupportStuff';
    sc.gridOptions_SupportStuff.columnDefs = [
        {
            field: "index", displayName: " ", width: "5%",
		    cellTemplate: '<div class="{{getClassChanges()}} ngCellText" ng-class="col.colIndex()">' +
            '<input value="{{row.entity[col.field]}}" class="bordernone" ng-model="row.entity[col.field]" placeholder="" readonly>' +
            '</div>'
        },
        {
            field: "NameSelected", displayName: "Наименование работ", width: "35%",
            cellTemplate: '<a tooltip="{{row.entity[col.field][\'Name\']}}" tooltip-popup-delay="1000" tooltip-class="my-custom-tooltip" tooltip-placement="top" tooltip-append-to-body="true">' +
            '<div class="{{getClassChanges()}} ngCellText" ng-class="col.colIndex()" ng-click="editCell(row.entity, row.getProperty(col.field), col.field)">' +
            '<input value="{{row.entity[col.field][\'Name\']}}" placeholder="Наимнование работы" style="border: none;"  data-ng-click="updateSelectedCell(col.field, row.entity[col.field][\'Id\'],\'№ поля\',row.entity,$event)"  readonly>' +
            '</div>'
        },
        {
            field: "Count", displayName: "Кол-во чел", width: "15%",
            cellTemplate: '<div class="{{getClassChanges()}} ngCellText" ng-class="col.colIndex()">' +
            '<textinputnotcomma value="{{row.entity[col.field]}}" placeholder="0"  data="row.entity[col.field]" regex-name="numbersWithPoint" field-change="recountRowSupportStaff(row.entity)" readonly="row.entity.UnicFlag"></textinputnotcomma>' +
            '</div>'
        },
        {
            field: "Price", displayName: "Расценка,р/ПД", width: "20%",
            cellTemplate: '<div class="{{getClassChanges()}} ngCellText" ng-class="col.colIndex()">' +
            '<input value="{{row.entity[col.field]}}" ng-model="row.entity[col.field]" placeholder="0" ng-change="recountRowSupportStaff(row.entity)">' +
            '</div>'
        },
        {
            field: "Cost", displayName: "Стоимость,р", width: "20%",
            cellTemplate: '<div class="{{getClassChanges()}} ngCellText" ng-class="col.colIndex()">' +
            '<input value="{{row.entity[col.field]}}" class="bordernone" ng-model="row.entity[col.field]" placeholder="0" readonly>' +
            '</div>'
        },
        {
            field: "Delete", displayName: " ", width: "5%",
            cellTemplate: '<div class="{{getClassChanges()}} ngCellText {{getClassCopyCell(row.entity.Status)}}, clear_row" style="font-size: 36px!important;margin: auto;margin-top: 7px;" ng-switch on="row.entity[col.field]" ng-click="delRowSupportStaff(row.entity)"></div>'   
        },
        {field: "Id", width: "0%"},
		{
            field: "id_tc_oper", displayName: " ", width: "0%",
		    cellTemplate: '<div class="{{getClassChanges()}} ngCellText" ng-class="col.colIndex()">' +
            '<input value="{{row.entity[col.field]}}" class="bordernone" ng-model="row.entity[col.field]" placeholder="" readonly>' +
            '</div>'
        },
	
    ];

    sc.createNewSupportStaff = function() {
        var supportStuff =
        {
            NameSelected: null,
            Count: 1,
        //    Price: 0,
        //    Cost: 0
        };
        sc.DataSupportStuff.push(supportStuff);
    };
   //пересчет вспомогательный персонал
    var recountRowSupportStaff = function(row) {
	//	console.log(row.index);
        var workRow = getRowWork(row.index);
        row.UnicFlag = workRow.UnicFlag;
        if (row.UnicFlag) {
            if (workRow.Workload && row.Price) {
                row.Cost = workRow.Workload * row.Price;
            }
        } else {
            if (workRow.ShiftCount && row.Count && row.Price) {
                row.Cost = row.Count * row.Price * workRow.ShiftCount;
            } else if (workRow.ShiftCount && row.Count && row.Cost) {
                row.Price = Math.round(row.Cost / (workRow.ShiftCount * row.Count));
            }
        }
        recountTotal(['zp']);
    };
    sc.recountRowSupportStaff = recountRowSupportStaff;

    var getRowWork = function(id) {
		  for (var i= 0; i< sc.DataWorks.length; i++) {
		sc.DataWorks[i].index=i+1;
		   }
        for (var i= 0, li = sc.DataWorks.length; i<li; i++) {
		//	console.log(sc.DataWorks[i].index);
            if (sc.DataWorks[i].index === id) {
				//	console.log(sc.DataWorks[i].index);
                return sc.DataWorks[i];
            }
        }
    };
    //удаление вспомогательный персонал
    sc.delRowSupportStaff = function(row) {
        console.log(sc.DataSupportStuff.indexOf(row));
        console.log(row);
        sc.DataSupportStuff.splice(sc.DataSupportStuff.indexOf(row), 1);
		 recountTotal(['zp']);
    };

    //----------------------------------------------------------end-Вспомогательный-Персонал------------------------------------------------------------
    //----------------------------------------------------------Материалы-------------------------------------------------------------------------------
    sc.getMaterialRowIndex = function(id) {
        for (var i =0, li=sc.DataWorks.length; i<li; i++) {
            if (sc.DataWorks[i].Name.Id === id) {
                return i+1;
            }
        }
    };

    sc.gridOptions_Szr = {};
    setCommonGridOption(sc.gridOptions_Szr);
    sc.gridOptions_Szr.data = 'DataSzr';

    sc.gridOptions_ChemicalFertilizers = {};
    setCommonGridOption(sc.gridOptions_ChemicalFertilizers);
    sc.gridOptions_ChemicalFertilizers.data = 'DataChemicalFertilizers';

    sc.gridOptions_Seed = {};
    setCommonGridOption(sc.gridOptions_Seed);
    sc.gridOptions_Seed.data = 'DataSeed';
	
	 sc.gridOptions_DopMaterial = {};
    setCommonGridOption(sc.gridOptions_DopMaterial);
    sc.gridOptions_DopMaterial.data = 'DataDopMaterial';

    var getColumns = function(material_column) {
        return [
            {
            field: "index", displayName: " ", width: "5%",
		    cellTemplate: '<div class="{{getClassChanges()}} ngCellText" ng-class="col.colIndex()">' +
            '<input value="{{row.entity[col.field]}}" class="bordernone" ng-model="row.entity[col.field]" placeholder="" readonly>' +
            '</div>'
             },
            {
                field: "NameSelectedWorkForMaterial", displayName: "Тип работ", width: "15%",
                cellTemplate: '<a tooltip="{{row.entity[col.field][\'Name\']}}" tooltip-popup-delay="1000" tooltip-class="my-custom-tooltip" tooltip-placement="top" tooltip-append-to-body="true">' +
                '<div class="{{getClassChanges()}} ngCellText" ng-class="col.colIndex()">' +
                '<input value="{{row.entity[col.field][\'Name\']}}" placeholder="Тип работ" style="border: none;"  data-ng-click="updateSelectedCell(col.field, row.entity[col.field][\'Id\'],\'Наименование работ\',row.entity,$event)"  readonly>' +
                '</div>'
            },
            {
                field: material_column,
                displayName: "Материал",
                width: "15%",
                cellTemplate: '<a tooltip="{{row.entity[col.field][\'Name\']}}" tooltip-popup-delay="1000" tooltip-class="my-custom-tooltip" tooltip-placement="top" tooltip-append-to-body="true">' +
                '<div class="{{getClassChanges()}} ngCellText" ng-class="col.colIndex()">' +
                '<input value="{{row.entity[col.field][\'Name\']}}" placeholder="Материал" style="border: none;"  data-ng-click="updateSelectedCell(col.field, row.entity[col.field][\'Id\'],\'Название материала\',row.entity,$event)"  readonly>' +
                '</div>'
            },
            {
                field: "MaterialArea",
                displayName: "Площадь внесения, га",
                width: '15%',
                cellTemplate: '<div class="{{getClassChanges()}} ngCellText" ng-class="col.colIndex()">' +
                    '<input value="{{row.entity[col.field]}}" ng-model="row.entity[col.field]" placeholder="0" ng-change="recountRowMaterial(row.entity)">' +
                '</div>'
            },
            {
                field: "ConsumptionRate", displayName: "Расход, ед/га", width: "15%",
                cellTemplate: '<div class="{{getClassChanges()}} ngCellText" ng-class="col.colIndex()">' +
                '<input value="{{row.entity[col.field]}}" ng-model="row.entity[col.field]" placeholder="0" ng-change="recountRowMaterial(row.entity)">' +
                '</div>'
            },
            {
                field: "Price", displayName: "Цена, р/ед", width: "15%",
                cellTemplate: '<div class="{{getClassChanges()}} ngCellText" ng-class="col.colIndex()">' +
                '<input value="{{row.entity[col.field]}}" ng-model="row.entity[col.field]" placeholder="0" ng-change="recountRowMaterial(row.entity)">' +
                '</div>'
            },
            {
                field: "Cost", displayName: "Стоимость, р", width: "15%",
                cellTemplate: '<div class="{{getClassChanges()}} ngCellText" ng-class="col.colIndex()">' +
                '<input value="{{row.entity[col.field]}}" ng-model="row.entity[col.field]" placeholder="0" class="bordernone" readonly>' +
                '</div>'
            },
            {
            field: "Delete", displayName: " ", width: "5%",
            cellTemplate: '<div class="{{getClassChanges()}} ngCellText {{getClassCopyCell(row.entity.Status)}}, clear_row" style="font-size: 36px!important;margin: auto;margin-top: 7px;" ng-switch on="row.entity[col.field]" ng-click="delRowSZR(row.entity)"></div>'   
            },
            {field: "Id", width: "0%"},
			{
            field: "id_tc_oper", displayName: " ", width: "0%",
		    cellTemplate: '<div class=" ngCellText" ng-class="col.colIndex()">' +
            '<input value="{{row.entity[col.field]}}" class="bordernone" ng-model="row.entity[col.field]" placeholder="" readonly>' +
            '</div>'
        },
        ];
    };

    sc.gridOptions_Szr.columnDefs = getColumns('NameSZR');
    sc.gridOptions_ChemicalFertilizers.columnDefs = getColumns('NameFertilizer');
    sc.gridOptions_Seed.columnDefs = getColumns('NameSeed');
    sc.gridOptions_DopMaterial.columnDefs = getColumns('NameDopMaterial');
    sc.createNewSZR = function() {
        var newSZR = {
            NameSZR: null,
            Type: 1
        };
        createNewMaterial(newSZR, sc.DataSzr);
    };

    sc.createNewFertilizer = function() {
        var newFertilizer = {
            NameFertilizer: null,
            Type: 2
        };
        createNewMaterial(newFertilizer, sc.DataChemicalFertilizers);
    };

    sc.createNewSeed = function() {
        var newSeed = {
            NameSeed: null,
            Type: 4
        };
        createNewMaterial(newSeed, sc.DataSeed);
    };
	//
   sc.createNewDopMaterial= function() {
        var newDopMaterial  = {
            NameSeed: null,
            Type: 5
        };
        createNewMaterial(newDopMaterial, sc.DataDopMaterial);
    };
	//
	
	
    var createNewMaterial = function(newMaterial, data) {
        newMaterial.NameSelected = null;
        newMaterial.MaterialArea = sc.formData.Area;
    //    newMaterial.ConsumptionRate = 0;
    //    newMaterial.Price = 0;
   //     newMaterial.Cost = 0;
        data.push(newMaterial);
    };

    var recountRowMaterial = function(row) {
        if (row.MaterialArea && row.ConsumptionRate && row.Price) {
            row.Cost = row.MaterialArea * row.ConsumptionRate * row.Price;
        } else if (!row.Cost) {
            row.Cost = 0;
        }
	//	console.log(row);
        recountTotal(['seed','fertilizer','szr','dopMaterial']);
		
    };
    sc.recountRowMaterial = recountRowMaterial;

    sc.delRowSZR = function(row) {
        if (row.Type === 1) {
            sc.DataSzr.splice(sc.DataSzr.indexOf(row), 1);
        } else if (row.Type === 2) {
            sc.DataChemicalFertilizers.splice(sc.DataChemicalFertilizers.indexOf(row), 1);
        } else if (row.Type === 4) {
            sc.DataSeed.splice(sc.DataSeed.indexOf(row), 1);		
        }
		else if (row.Type === 5) {
            sc.DataDopMaterial.splice(sc.DataDopMaterial.indexOf(row), 1);		
        }
			 recountTotal(['seed','fertilizer','szr','dopMaterial']);
    };
    //----------------------------------------------------------end-Материалы---------------------------------------------------------------------------
    //-------------------------------------------------------------Услуги-------------------------------------------------------------------------------
    sc.gridOptions_Services = {};
    setCommonGridOption(sc.gridOptions_Services);
    sc.gridOptions_Services.data = 'DataServices';
    sc.gridOptions_Services.columnDefs = [
            {
                field: "index", displayName: " ", width: "5%",
                cellTemplate: '<div class="{{getClassChanges()}} ngCellText" ng-class="col.colIndex()">' +
                '<textinputnotcomma data="getStuffRowIndex(row.entity.NameSelected.Id)" ng-model="row.entity[col.field]" class="bordernone" readonly="1"/>' +
                '</div>'
            },
            {
                field: "NameService", displayName: "Название услуги", width: "70%",
                cellTemplate: '<a tooltip="{{row.entity[col.field][\'Name\']}}" tooltip-popup-delay="1000" tooltip-class="my-custom-tooltip" tooltip-placement="top" tooltip-append-to-body="true">' +
                '<div class="{{getClassChanges()}} ngCellText" ng-class="col.colIndex()">' +
                '<input value="{{row.entity[col.field][\'Name\']}}" placeholder="Тип работ" style="border: none;"  data-ng-click="updateSelectedCell(col.field, row.entity[col.field][\'Id\'],\'Наименование услуг\',row.entity,$event)"  readonly>' +
                '</div>'
            },
            {
                field: "Cost", displayName: "Стоимость, р", width: "20%",
                cellTemplate: '<div class="{{getClassChanges()}} ngCellText" ng-class="col.colIndex()">' +
                '<input value="{{row.entity[col.field]}}" ng-model="row.entity[col.field]" placeholder="0" ng-change="recountService()">' +
                '</div>'
            },
            {
            field: "Delete", displayName: " ", width: "5%",
            cellTemplate: '<div class="{{getClassChanges()}} ngCellText {{getClassCopyCell(row.entity.Status)}}, clear_row" style="font-size: 36px!important;margin: auto;margin-top: 7px;" ng-switch on="row.entity[col.field]" ng-click="delRowService(row.entity)"></div>'
            },            
            {field: "Id", width: "0%"}
        ];

    sc.createNewServices = function() {
        var service =
        {
            NameService: null,
         //   Cost: 0
        };
        sc.DataServices.push(service);
    };

    sc.recountService = function() {
        recountTotal(['service']);
    };

    sc.delRowService = function(row) {
        console.log(sc.DataServices.indexOf(row));
        console.log(row);
        sc.DataServices.splice(sc.DataServices.indexOf(row), 1);
		 recountTotal(['service']);
    };

    //----------------------------------------------------------submit----------------------------------------------------------------------------------
    sc.submitForm = function() {
 console.log(sc.DataTotal[0].Total);
            requests.serverCall.post($http, "TechCard/CreateUpdateTC", {
                TC: sc.formData,
                NZP: sc.DataNZP[0],
                Works: sc.DataWorks,
                SupportStuff: sc.DataSupportStuff,
                DataSzr: sc.DataSzr,
                DataChemicalFertilizers: sc.DataChemicalFertilizers,
                DataSeed: sc.DataSeed,
                DataServices: sc.DataServices,
				DataDopMaterial :sc.DataDopMaterial,
                DataTotal: sc.DataTotal
            }, function (data, status, headers, config) {
                console.log(data);
                $location.path('/techcard/' + sc.formData.YearId);
            }, function () {
            });
        
    };

    sc.cancel = function() {
        $location.path('/techcard/' + sc.formData.YearId);
    };

	  sc.getClassChanges = function () { 
	  
		if (CheckChange==1) 
		{
			console.log('disabledbutton');
            return 'disabledbutton';
			} else 
			{
		return '';
			}
			
    };
 
 
    var loadTcById = function() {
        if (sc.formData.Id) {
            requests.serverCall.post($http, 'TechCard/GetTCById', sc.formData.Id, function (data, status, headers, config) {
                console.info(data);
                sc.DataSzr = data.DataSZR;
                sc.DataChemicalFertilizers = data.DataChemicalFertilizers;
                sc.DataSeed = data.DataSeed;
                sc.DataNZP = [data.NZP];
                sc.DataSupportStuff = data.SupportStuff;
                sc.DataServices = data.DataServices;
                sc.DataTotal = data.DataTotal;
                sc.formData = data.TC;
                sc.DataWorks = data.Works;
				sc.DataDopMaterial = data.DataDopMaterial; 
				//recountTotal();
					//пересчет таблицы
      if (CheckChange==0) {
	for (var i=0;i<sc.DataWorks.length;i++)
	{	
		//топливо
	 sc.DataWorks[i].FuelCount = sc.DataWorks[i].Consumption*sc.DataWorks[i].Workload;  //потребность
    if (sc.DataWorks[i].FuelType!=null) {sc.DataWorks[i].FuelCost = (sc.DataWorks[i].FuelCount*sc.DataWorks[i].FuelType.Price).toFixed(2); recountTotal(['fuel']);}     //Стоимость	
		//зп
	 sc.DataWorks[i].ShiftCount = (sc.DataWorks[i].Workload/sc.DataWorks[i].ShiftRate).toFixed(2);  //смены
	sc.DataWorks[i].EmpCost = (sc.DataWorks[i].Rate*sc.DataWorks[i].ShiftCount + sc.DataWorks[i].EmpPrice*sc.DataWorks[i].Workload).toFixed(2);  //стоимость зп
	}
	//stuff
	for (var i=0;i<sc.DataSupportStuff.length;i++)
	{
		for (var j=0;j<sc.DataWorks.length;j++)
	{
		if (sc.DataSupportStuff[i].index==sc.DataWorks[j].index && sc.DataWorks[j].UnicFlag==0)
		{
			sc.DataSupportStuff[i].Cost = sc.DataWorks[j].ShiftCount*sc.DataSupportStuff[i].Count*sc.DataSupportStuff[i].Price; break;
		}
		if (sc.DataSupportStuff[i].index==sc.DataWorks[j].index && (sc.DataWorks[j].UnicFlag==1 || sc.DataWorks[j].UnicFlag==2))
		{
			sc.DataSupportStuff[i].Cost=sc.DataWorks[j].Workload*sc.DataSupportStuff[i].Price; break;
		}
	}	
	}
   //seed
   for (var i=0;i<sc.DataSzr.length;i++)
	{	
	sc.DataSzr[i].Cost=sc.DataSzr[i].MaterialArea*sc.DataSzr[i].ConsumptionRate*sc.DataSzr[i].Price;
	}
		//2
	 for (var i=0;i<sc.DataChemicalFertilizers.length;i++)
	{	
	sc.DataChemicalFertilizers[i].Cost=sc.DataChemicalFertilizers[i].MaterialArea*sc.DataChemicalFertilizers[i].ConsumptionRate*sc.DataChemicalFertilizers[i].Price;
	}
	//3		
	 for (var i=0;i<sc.DataSeed.length;i++)
	{	
	sc.DataSeed[i].Cost=sc.DataSeed[i].MaterialArea*sc.DataSeed[i].ConsumptionRate*sc.DataSeed[i].Price;
	}
	//4
   for (var i=0;i<sc.DataDopMaterial.length;i++)
	{	
	sc.DataDopMaterial[i].Cost=sc.DataDopMaterial[i].MaterialArea*sc.DataDopMaterial[i].ConsumptionRate*sc.DataDopMaterial[i].Price;
	}
	  }
            }, function () {
            });
        }
    };

}]);