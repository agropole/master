/**
 *  @ngdoc controller
 *  @name TC.controller:TCListCtrl
 *  @description
 *  # TCListCtrl
 *  Это контроллер, определеяющий поведение формы списка Техкарт
 *  @requires $scope
 *  @requires $http
 *  @requires requests
 *  @requires $location
 *  @requires modals
 *  @requires modalType
 *  @requires LogusConfig

 *  @property {Object} sc scope
 *  @property {string} infoData справочники
 *  @property {Object} YearId: текущий год
 *  @property {Object} gridOptions настройки таблицы
 *  @property {Object} selectedRow выделенная строка


 *  @property {function} loadTC загрузка списка
 *  @property {function} sc.openTotalReport отчет по всем ТК
 *  @property {function} sc.openTcReport отчет по выбранной ТК
 *  @property {function} sc.refreshYear: изменение года
 *  @property {function} sc.createTC: создание новой ТК
 *  @property {function} sc.delRow: удалить ТК
 *  @property {function} sc.editTC: открыть ТК для редактирования

 */
var app = angular.module("TCList", ["ngGrid", "services", 'ngCookies', "modalWindows"]);
app.controller("TCListCtrl", ["$scope", "$http", "requests", "$location",'$cookieStore', "modals", "modalType","LogusConfig",'$rootScope',  function ($scope, $http, requests, $location,$cookieStore, modals, modalType, LogusConfig,$rootScope) {
    var sc = $scope;
    sc.infoData = {};
  if ($rootScope.roleid==12)
	{
	var cellTemplate1='<div  class="{{getClassTempCell(row.entity.TemplateStatus,row)}} ngCellText {{getClassCopyCell(row.entity.Status,row)}}"></div>'; 
	} else {
		var cellTemplate1='<div class="{{getClassTempCell(row.entity.TemplateStatus,row)}} {{getClassDateCell(row.entity.Changing_date1)}}  ngCellText {{getClassCopyCell(row.entity.Status,row)}}" style="font-size: 36px!important;" ng-switch on="row.entity[col.field]"'+
            'ng-click="delRow(row.entity)"'+
            '><div  ng-switch-default>'+
            '<a tooltip="{{row.entity[col.field]}}" tooltip-class="my-custom-tooltip"'+
           'tooltip-placement="top" tooltip-append-to-body="true">'+
            '<div class="clear_row"/>'+
            '</a></div></div></div>'; 
		}
	
	
    requests.serverCall.get($http, "TechCard/GetInfoTCDetail", function (data, status, headers, config) {
      //  console.info(data);
        sc.infoData = data;
        sc.YearId = new Date().getFullYear();
        loadTC();
    }, function () {
    });
	   //получаем дату запрета
	 requests.serverCall.get($http, "DayTasksCtrl/GetBanDateTC", function (data, status, headers, config) {
        console.info(data);
		if (data!="bad") { 
        sc.ban_date = data;} else {var date = new Date; sc.ban_date="01.01."+date.getFullYear();}
    }, function () {
    });

		
    sc.selectedRow = [];
    sc.gridOptions = {
        enableColumnResize: false,
        enableRowSelection: true,
        showFooter: false,
        rowHeight: 49,
        headerRowHeight: 49,
        useExternalSorting: true,
        data: 'TCData',
        selectedItems: sc.selectedRow,
        afterSelectionChange: function (row) {
          if (sc.selectedRow.length!=0)  {console.info(sc.selectedRow[0].Id);}
            if (row.entity) {
                if (row.selected) {
                    sc.selectedRow = [row];
                } else {
                    if (sc.selectedRow && sc.selectedRow > 0) {
                        sc.selectedRow.splice(sc.tableDataFirst.indexOf(row.entity), 1);
                    }
                }
            }	
			//
			  sc.selectedRow=[];
            sc.selectedRow.push(row.entity);
			//console.log(sc.selectedRow[0]);
			var dateBegin = sc.selectedRow[0].Changing_date1.split(".");
			var dateBegin1 = new Date(dateBegin[2], (dateBegin[1] - 1), dateBegin[0]);
			var banDate = sc.ban_date.split("T")[0].split("-");
			var banDate1 = new Date(banDate[0], (banDate[1] - 1), parseInt(banDate[2])+1);
		if (dateBegin1.getTime()<banDate1.getTime()) 
		{
            document.getElementById('edit').disabled = true;
			} else 
			{
           document.getElementById('edit').disabled = false;
			}
        },
        beforeSelectionChange: function (row, event) {
            angular.forEach(sc.TCData, function (data, index) {
                if (data.selected == true) sc.selectedRow.splice(index, 1);
                sc.gridOptions.selectRow(index, false);
            });
			
            return true;
        }
    };
    sc.gridOptions.columnDefs = [
        {
            field: "Plant",
            displayName: "Культура",
            width: "20%",
            cellTemplate: '<div class=" {{getClassTempCell(row.entity.TemplateStatus,row)}} ngCellText {{getClassCopyCell(row.entity.Status,row)}}" ng-class="col.colIndex()">' +
            '<input value="{{row.entity[col.field]}}" ng-model="row.entity[col.field]" placeholder="Культура" class="bordernone" readonly>' +
            '</div>'
        },
        {
            field: "Sort", displayName: "Сорт", width: "15%",
            cellTemplate: '<div class="{{getClassTempCell(row.entity.TemplateStatus,row)}} ngCellText {{getClassCopyCell(row.entity.Status,row)}}" ng-class="col.colIndex()">' +
            '<input value="{{row.entity[col.field]}}" ng-model="row.entity[col.field]" placeholder="Сорт" class="bordernone" readonly>' +
            '</div>'
        },
        {
            field: "Field", displayName: "Поле", width: "10%",
            cellTemplate: '<div class="{{getClassTempCell(row.entity.TemplateStatus,row)}} ngCellText {{getClassCopyCell(row.entity.Status,row)}}" ng-class="col.colIndex()">' +
            '<input value="{{row.entity[col.field]}}" ng-model="row.entity[col.field]" placeholder="Поле" class="bordernone" readonly>' +
            '</div>'
        },
        {
            field: "Cost", displayName: "Стоимость", width: "10%",
            cellTemplate: '<div class="{{getClassTempCell(row.entity.TemplateStatus,row)}} ngCellText {{getClassCopyCell(row.entity.Status,row)}}" ng-class="col.colIndex()">' +
            '<input value="{{row.entity[col.field]}}" ng-model="row.entity[col.field]" placeholder="Стоимость" class="bordernone" readonly>' +
            '</div>'
        },
	      {
            field: "Changing_date1",
            displayName: "Сохранено",
            width: "15%",
            cellTemplate: '<div class="{{getClassTempCell(row.entity.TemplateStatus,row)}} ngCellText {{getClassCopyCell(row.entity.Status,row)}}" ng-class="col.colIndex()">' +
            '<input value="{{row.entity[col.field]}}" ng-model="row.entity[col.field]" placeholder="Культура" class="bordernone" readonly>' +
            '</div>'
        },
		{
            field: "TemplateStatus", displayName: " ", width: "5%",
            cellTemplate: 'templates/status_techcard_template.html'
        },
        {
            field: "", displayName: " ", width:"10%",
            cellTemplate: '<div class="{{getClassTempCell(row.entity.TemplateStatus,row)}} ngCellText {{getClassCopyCell(row.entity.Status,row)}}"><button type="button"  class="logus-btn btn btn-border ng-click-active" style="width: 100%;margin-top: 8px" ng-click="openTcReport(row.entity.Id)">XLS</button></div>',
        },
		 {
            field: "", displayName: " ", width:"10%",
            cellTemplate: '<div class="{{getClassTempCell(row.entity.TemplateStatus,row)}} ngCellText {{getClassCopyCell(row.entity.Status,row)}}"><button type="button"  class="logus-btn btn btn-border ng-click-active" style="width: 100%;margin-top: 8px" ng-click="copyTcReport(row.entity.Id)">Copy</button></div>',
        },
        {
            field: "Delete", displayName: " ", width: "5%",
            cellTemplate: cellTemplate1, cellClass: 'cellToolTip'
        },
        {field: "Id", width: "0%"}
    ];
    sc.openTotalReport = function() {
        var popupWin = window.open(LogusConfig.serverUrl + 'TechCard/GetTCReportByYear?year=' + sc.YearId);
                
    };

    sc.openTcReport = function(id) {
        var popupWin = window.open(LogusConfig.serverUrl + 'TechCard/GetTCReportById?tcId=' + id);
    };
     // Скопировать тех карту в следующий год
	  sc.copyTcReport = function(id) {
       requests.serverCall.post($http, "TechCard/CopyTCReportById",id, function (data, status, headers, config) {
         message('Сохранение', "Тех. карта успешна скопирована в "+(sc.YearId+1)+" год");
        }, function () {
        });
		console.log(id);
    };
	
	  function message(title, message) {
        modals.showWindow(modalType.WIN_MESSAGE, {
            title: title,
            message: message,
            callback: function () {
            }
        });
    }
    var loadTC = function() {
        requests.serverCall.post($http, "TechCard/GetTCByYear",sc.YearId, function (data, status, headers, config) {
            sc.TCData = data;	
        }, function () {
        });
    };

	
	
    sc.refreshYear = function() {
        loadTC();
    };

    sc.createTC = function() {
        $location.path('/tcdetail/' + sc.YearId + "/");
    };

    sc.delRow = function(row) {
		// console.log(sc.selectedRow[0]);
		console.log(row);
	//	if (row.Id!=undefined) {
        modals.showWindow(modalType.WIN_DIALOG, {
            title: "Удаление", message: "Вы уверены, что хотите удалить тех.карту?", callback: function () {
                sc.TCData.splice(sc.TCData.indexOf(row), 1);
                requests.serverCall.post($http, "TechCard/DelTC",row.Id, function (data, status, headers, config) {
                    loadTC();
                }, function () {
                });
            }
        });
	//	}
    };
	//проверка даты
	  sc.getClassDateCell = function (date) {
      	var dateBegin = date.split(".");
			var dateBegin1 = new Date(dateBegin[2], (dateBegin[1] - 1), dateBegin[0]);
			var banDate = sc.ban_date.split("T")[0].split("-");
			var banDate1 = new Date(banDate[0], (banDate[1] - 1), parseInt(banDate[2])+1);
		if (dateBegin1.getTime()<banDate1.getTime()) 
		{
             return 'disabledbutton'
			} else 
			{
		return ''
			}
			
    };
 // стиль строки 
 
    sc.getClassCopyCell = function (stat,row) {
        if (stat > 0 && row.selected==false ) {
            return 'zero';
        }
		if (row.selected==true) {  return '';}
    };
	
	//стиль шаблона
	 sc.getClassTempCell = function (stat,row) {
        if (stat > 0 && row.selected==false ) {
            return 'tempColor';
        }
		if (row.selected==true) {  return '';}
    };
	//
	if ($rootScope.roleid==12)
	{	
    $("#create").hide();
	$("#edit").hide();
	
		
	}
		//обновление всех данных тех. карты
      sc.yes = function () {
	$location.path('/tcdetail/' + sc.YearId + "/" + sc.selectedRow[0].Id);	  
    $("#myModal").modal("hide");
	$("div.modal-backdrop").hide();
	$cookieStore.put('CheckChange', {id: 0});	
	}
	//режим просмотра
   sc.no = function () {
	$("#myModal").modal("hide");
	$("div.modal-backdrop").hide();
	$location.path('/tcdetail/' + sc.YearId + "/" + sc.selectedRow[0].Id);
	$cookieStore.put('CheckChange', {id: 1});	 
	}
      sc.cancelClick = function () {
		$("#myModal").modal("hide");    
	  }
    sc.editTC = function() {
		console.log(sc.selectedRow[0].Id);
        if (sc.selectedRow[0].Id && sc.YearId > 2015) {
			 requests.serverCall.post($http, 'TechCard/GetTCById', sc.selectedRow[0].Id, function (data, status, headers, config) {
                console.info(data);
              if (data.TC.CheckСhange==1) {
			$("#myModal").modal("show");}
			else {$location.path('/tcdetail/' + sc.YearId + "/" + sc.selectedRow[0].Id);}  	
            }, function () {
            });
			
		//	$location.path('/tcdetail/' + sc.YearId + "/" + sc.selectedRow[0].Id); //--потом убрать
           // console.info(sc.selectedRow[0].entity);
           $location.path('/tcdetail/' + sc.YearId + "/" + sc.selectedRow[0].entity.Id);
        } 
    };
}]);