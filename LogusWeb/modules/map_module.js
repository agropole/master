/**
 *  @ngdoc service
 *  @name app.factory:map
 *  @description
 *  # map
 *  Это фабрика, определяющая работу карты в путевых листах
 *  @requires $http
 *  @requires $requests
 *  @requires modalType
 *  @requires modals

 *  @property {Object} raster тайлы карты
 *  @property {Object} map карта
 *  @property {Object} featureOverlay слой действий (рисование)
 *  @property {Object} fieldsLayer слой полей
 *  @property {Object} oldArea площадь 
 *  @property {Object} areaInput callback-функция для отображения выделенной площади
 *  @property {Object} insertFieldCb callback-функция для добавления нового поля
 
 *  @property {function} addInteraction добавление слоя действий
 *  @property {function} getRGB функция перевода hex to rgb
 *  @property {function} createFieldStyle стиль полей
 *  @property {function} formatArea площадь для рисования
 *  @property {function} clearOverlay очистить рисование
 *  @property {function} clearField очистить поля
 *  @property {function} getArea получить площадь в Га

 *  @return {function} initMap принициализация карты с переметром - контейнер для добавления
 *  @return {function} clearOverlay очищение слоя рисования
 *  @return {function} refreshField перерисовка слоя поля
 *  @return {function} setAreaInput присваивание areaInput
 *  @return {function} allowSingleDraw рисование только по одному полигону
 *  @return {function} getField добавление нового поля из нарисованного
 *  @return {function} clearMap очищение карты
 *  @return {function} setInsertFieldCb присваивание insertFieldCb
 *  @return {function} addInteraction включение режима рисования
 *  @return {function} clearAll очистить слой рисования
 *  @return {function} addOnOverlay добавление полигона для редактирования
 *  @return {function} getCoord получить координаты редактируемого полигона

 */

    var app = angular.module("mapModule", []);

    app.factory("map", ["modals", "modalType", "$http", "requests", function (modals, modalType, $http, requests) {
        
		//------------create map-------------------------------------------------------------------
        
        //var raster = new ol.layer.Tile({source: new ol.source.OSM({})}); //
		yaex = [-20037508.342789244, -20037508.342789244, 20037508.342789244, 20037508.342789244];
		proj4.defs('EPSG:3395', '+proj=merc +lon_0=0 +k=1 +x_0=0 +y_0=0 +datum=WGS84 +units=m +no_defs');
		ol.proj.get('EPSG:3395').setExtent(yaex);

		satel = new ol.layer.Group({
            visible: true,
			preload: Infinity,
            layers: [
			        new ol.layer.Tile({
                    source:new ol.source.TileArcGISRest({
            		url: 'http://server.arcgisonline.com/arcgis/rest/services/World_Imagery/MapServer',
                })}),
				new ol.layer.Tile({
				source: new ol.source.XYZ({
				url: 'http://agropole.logus-vrn.ru/bin/tiles.fcgi?l=photo&x={x}&y={y}&z={z}&zo=1',
				projection: 'EPSG:3395',
				tileGrid: ol.tilegrid.createXYZ({			
                extent: yaex
				})			
        })})
            ]
        });/*new ol.layer.Tile({
		source: new ol.source.XYZ({
            url: 'http://agropole.logus-vrn.ru/bin/tiles.fcgi?l=photo&x={x}&y={y}&z={z}&zo=1',
            projection: 'EPSG:3395',
            tileGrid: ol.tilegrid.createXYZ({
				
                extent: yaex
            })			
        }),
        visible: true
    });*/
           console.log('1111');
		var featureOverlay = new ol.layer.Vector({
		source: new ol.source.Vector(),
		style: function(feature, resolution) {
                var styleArray = [];
                var prop = feature.getProperties();
                feature.selectable = false;
                var fillStyle = new ol.style.Style({
                    fill : new ol.style.Fill({
                        color: 'rgba(255, 255, 255, 0.1)'
                    }),
                    stroke: new ol.style.Stroke({
                   //     color: '#f00',
				        color: '#009ACD',
                        width: 2
                    }),
                    text : createTextStyle(feature, resolution, 'center')
                });
                styleArray.push(fillStyle);
                return styleArray;
            },
		
		});
		
		
		var map = new ol.Map({
            layers: [satel, featureOverlay],
			view: new ol.View({
                center: ol.proj.transform([39.603334,51.761755], 'EPSG:4326', 'EPSG:3857'),
                zoom: 15
            })
        });
        featureOverlay.setMap(map);

        var draw,modify;
        function addInteraction() {
            modify = new ol.interaction.Modify({
                //features: featureOverlay.getSource().getFeatures(),
				source: featureOverlay.getSource(),
				deleteCondition: function(event) {
                    return ol.events.condition.shiftKeyOnly(event) &&
                        ol.events.condition.singleClick(event);
                }
            });
            map.addInteraction(modify);

            draw = new ol.interaction.Draw({
                //features: featureOverlay.getSource().getFeatures(),
				source: featureOverlay.getSource(),
                type: ("Polygon"),
                style: function (feature, resolution) {
                    var styleArray = [];
                    var prop = feature.getProperties();
                    feature.selectable = false;
                    var fillStyle = new ol.style.Style({
                        fill: new ol.style.Fill({
                            color: 'rgba(255, 255, 255, 0.2)'
                        }),
                        stroke: new ol.style.Stroke({
                            color: '#009ACD',
                            width: 2
                        }),
                        text: createTextStyle(feature, resolution, 'center')
                    });
                    styleArray.push(fillStyle);
                    return styleArray;
                }
            });
            map.addInteraction(draw);
        }

        //------------map style-------------------------------------------------------------------
        function getRGB(c, alpha) {
            var color = String(c);
            var HexToR = parseInt(color.substring(2, 4), 16);
            var HexToG = parseInt(color.substring(4, 6), 16);
            var HexToB = parseInt(color.substring(6, 8), 16);
            var rgbaColor = "rgba(" + HexToR + ", " + HexToG + ", " + HexToB + "," + alpha + ")";
            return rgbaColor;
        }
        //field style
        function createFieldStyle() {
            return function(feature, resolution) {
                var styleArray = [];
                var prop = feature.getProperties();
                feature.selectable = false;
                var fillStyle = new ol.style.Style({
                    fill : new ol.style.Fill({
                        color : getRGB(prop.color, 0.2),
                    }),
                    stroke : new ol.style.Stroke({
                        color : getRGB(prop.color, 1),
                        width : "2"
                    }),
                });
                styleArray.push(fillStyle);
                if (prop.name) {
                    var text1 = new ol.style.Style({
                        text : createFieldTextStyle(prop.name, resolution, 'top')
                    });
                    styleArray.push(text1);
                }
                if (prop.name) {
                    var text2 = new ol.style.Style({
                        text : createFieldTextStyle(prop.plant, resolution, 'bottom', 1)
                    });
                    styleArray.push(text2);
                }
                return styleArray;
            };
        }

        //text polygon style
        function createFieldTextStyle(t, resolution, pos) {
            var textStyle = new ol.style.Text({
                textAlign : 'center',
                textBaseline : 'alphabetic',
                font : (pos === 'top') ? 'normal 18px DINPro' : 'normal 14px DINPro',
                text : t,
                fill : new ol.style.Fill({
                    color : "#000"
                }),
                stroke: new ol.style.Stroke({color: '#fff', width: 5}),
                offsetY : (pos === 'top') ? -15 : 0,
                offsetX : 0,
            });
            return textStyle;
        }
        //overlay text style
        function createTextStyle(feature, resolution, pos) {
            if (feature.getGeometry().getType() === "Polygon") {
                var geom = feature.getGeometry();
                var area = formatArea((geom));
                var t = area + ' га';
                var textStyle = new ol.style.Text({
                    textAlign : 'center',
                    textBaseline : 'alphabetic',
                    font : 'bold 24px DINPro-Bold',
                    text : t,
                    fill : new ol.style.Fill({
                        color : "#780407"
                    }),
                    offsetY : 10,
                    offsetX : 0,
                });
                getArea();
                return textStyle;
            } else {
                return null;
            }
        }
        //square text format
        var wgs84Sphere = new ol.Sphere(6378137);
        function formatArea(polygon) {
            var area;
            var sourceProj = map.getView().getProjection();
            var geom = (polygon.clone().transform(sourceProj, 'EPSG:4326'));
            var coordinates = geom.getLinearRing(0).getCoordinates();
            area = Math.abs(wgs84Sphere.geodesicArea(coordinates));
            area = area / 10000 * 10;
            area = Math.round(area) / 10;
            return area;
        }

       function clearOverlay() {
            var features = featureOverlay.getSource().getFeatures();
			features.forEach((feature) => {
			featureOverlay.getSource().removeFeature(feature);
    });	
        }
      //    clearOverlay();
		
		
        function clearField() {
            if(fieldsLayer) {
                map.removeLayer(fieldsLayer);
            }
			
        }

				
        var fieldsLayer;
		var oldArea;
        var areaInput;
        function getArea() {
            var area = 0;
            var fs = featureOverlay.getSource().getFeatures();
            fs.forEach(function (f, ind, array)	 {
                area += formatArea(f.getGeometry());
            });
            area = Math.round(area*10) / 10;
            if (area !== oldArea) {
                console.log('Общая площадь ' + fs.length + " участков = " + area + " га");
                oldArea = area;
                console.info(area);
            }
            if (areaInput) {
                areaInput(area);
            }
        }
        var insertFieldCb;

        return {
            initMap: function(target) {
                map.setTarget("map8"); // почему, но так карта перестает пропадать
				//map.renderer;
				map.setTarget(target);
				
            },
            clearOverlay: function(cb) {
                clearOverlay();
                if (cb) {
                    cb(0);
                }
            },
            refreshField: function(urlRefresh) {
				 console.log('clearOverlay');
                clearOverlay();
                clearField();
                console.log(urlRefresh);
                var fieldSource = new ol.source.Vector({
                    url: urlRefresh,
                    format: new ol.format.GeoJSON()
                });
				
                fieldSource.addEventListener('addfeature', function(e) {
                    var fs = fieldSource.getFeatures();
                    var f = fs[0];
                    if (f.getGeometry().getType() === 'Polygon'){
                        var geom = f.getGeometry().getInteriorPoint().getCoordinates();
                        console.log(JSON.stringify(geom));
                        map.getView().setCenter(geom);
                        map.getView().setZoom(15);
                    }
                });
				
                fieldsLayer = new ol.layer.Vector({
                    source: fieldSource,
                    style : createFieldStyle()
                });
                map.addLayer(fieldsLayer);
				
            },
            setAreaInput: function(inputFunction) {
                areaInput = inputFunction;
            },
            allowSingleDraw: function() {
                map.on('click', function() {
                    var l = featureOverlay.getSource().getFeatures().length;
                    if (l > 1) {
                        var array = featureOverlay.getSource().getFeatures();
                        featureOverlay.getSource().removeFeature(array[0]);
                    }
                    if (l === 1) {
                  //     clearField();
                    }
                });
            },
            getField : function(cb) {
                if (featureOverlay.getSource().getFeatures().length > 0) {
                    //var f = featureOverlay.getSource().getFeatures().getArray()[0];
					var f = featureOverlay.getSource().getFeatures()[0];
                    var g = f.getGeometry().getCoordinates()[0];
                    var str1 = "";
                    var str2 = "";
                    for (var i = g.length - 1; i >= 0; i--) {
                        var c = ol.proj.transform(g[i], 'EPSG:3857', 'EPSG:4326');
                        var s = c[0] + " " + c[1];
                        str1 += s;
                        var c2 = ol.proj.transform(g[g.length - 1 - i], 'EPSG:3857', 'EPSG:4326');
                        var s2 = c2[0] + " " + c2[1];
                        str2 += s2;

                        if (i !== 0) {
                            str1 += ", ";
                            str2 += ", ";
                        }
                    }

                    if (cb) {
                        cb();
                    }

                    var center = ol.proj.transform(f.getGeometry().getInteriorPoint().getCoordinates(), 'EPSG:3857', 'EPSG:4326');

                    modals.showWindow(modalType.WIN_CREATE_FIELD, {
                        area: oldArea,
                        coordinates: [str1, str2],
                        center: center[0] + " " + center[1],
                        callback: function (data) {
							 console.log('clearOverlay');
                            clearOverlay();
                            if (data) {
                                console.info(JSON.stringify(data));
                                requests.serverCall.post($http, "WaysListsCtrl/InsertNewField", data, function (newFieldId, status, headers, config) {
                                    console.info(newFieldId);
                                    insertFieldCb(newFieldId);
                                }, function () { });
                                clearField();
                            } else {

                            }
                        }
                    });
                }
            },
            clearMap: function() {
				clearOverlay();
                clearField();
            },
            setInsertFieldCb: function(cb) {
                insertFieldCb = cb;
            },
            addInteraction: function() {
                addInteraction();
            },
            clearAll: function() {
                //map.removeOverlay(featureOverlay);
				map.removeLayer(featureOverlay);
                if (draw) {
                    draw.f = []; //опытным путем... хз, что это значит и как сделать по-другому
                    map.removeInteraction(draw);
                    map.removeInteraction(modify);
                }
            },
            addOnOverlay: function(coord) {
                console.log(JSON.stringify(coord));
                for (var i= 0, li=coord.length; i<li; i++) {
                    coord[i] = ol.proj.transform(coord[i], 'EPSG:4326', 'EPSG:3857');
                }
                var geom1 = new ol.geom.Polygon([coord], 'XY');
                var f = new ol.Feature({
                    geometry: geom1
                });
                var geom2 = geom1.getInteriorPoint().getCoordinates();
                console.log(geom2);
                map.getView().setCenter(geom2);
                map.getView().setZoom(15);

                featureOverlay.getSource().addFeature(f);
            },
            getCoord: function() {
                var f = featureOverlay.getSource().getFeatures()[0];
                var g = f.getGeometry().getCoordinates()[0];
                var str1 = "";
                var str2 = "";
                for (var i = g.length - 1; i >= 0; i--) {
                    var c = ol.proj.transform(g[i], 'EPSG:3857', 'EPSG:4326');
                    var s = c[0] + " " + c[1];
                    str1 += s;
                    var c2 = ol.proj.transform(g[g.length - 1 - i], 'EPSG:3857', 'EPSG:4326');
                    var s2 = c2[0] + " " + c2[1];
                    str2 += s2;

                    if (i !== 0) {
                        str1 += ", ";
                        str2 += ", ";
                    }
                }

                var center = ol.proj.transform(f.getGeometry().getInteriorPoint().getCoordinates(), 'EPSG:3857', 'EPSG:4326');

                return {
                    coord: "POLYGON ((" + str1 + "))",
                    coord2: "POLYGON ((" + str2 + "))",
                    center: "POINT (" + center[0] + " " + center[1] + ")",
                    area: oldArea
                }
            }
        };
    }]);
