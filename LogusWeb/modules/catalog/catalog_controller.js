/**
 *  @ngdoc controller
 *  @name app.controller:CatalogController
 *  @description
 *  Справочник культур. Не используется
 */
var app = angular.module("Catalog", ["ngGrid", "services", 'ngCookies', "modalWindows"]);
app.controller("CatalogController", ["$scope", "$http", "requests", "$location", '$cookieStore', '$rootScope', "modals", "modalType", '$stateParams', function ($scope, $http, requests, $location, $cookieStore, $rootScope, modals, modalType, $stateParams) {
    var sc = $scope;

    var curUser = $cookieStore.get('currentUser');
    $rootScope.userName = curUser.username;
    $rootScope.exit = 'выход';
    if (curUser.roleid == '6') {
        $rootScope.topBtnVis = false;
    }
    var firstLoad = true;

    sc.tableDataPlants = [];
    sc.tableDataSorts = [];
    sc.mySelections = [];
    sc.formData = {};

    // свойства таблицы культур
    sc.gridOptionsPlants = {
        data: 'tableDataPlants',
        multiSelect: false,
        enableColumnResize: true,
        enableRowSelection: true,
        showFooter: false,
        rowHeight: 49,
        headerRowHeight: 49,
        selectedItems: sc.selectedRow,
        columnDefs: [
            {
                field: "TypePlant", displayName: "Тип культуры", width: "25%",
                cellTemplate: '<a tooltip="{{row.entity[col.field][\'Name\']}}" tooltip-popup-delay="1000" tooltip-class="my-custom-tooltip" tooltip-placement="top" tooltip-append-to-body="true">' +
                '<div class="ngCellText" ng-class="col.colIndex()">' +
                '<dropsearchtable drop="infoData.ShiftList" data="row.entity[col.field].Id" ng-click="editDropCell(row.entity, col.field, row.entity[col.field])"></dropsearchtable>' +
                '</div>' +
                '</a>'
            },
            {
                field: "PlantName", displayName: "Культура", width: "25%",
                cellTemplate: '<a tooltip="{{row.entity[col.field][\'Name\']}}" tooltip-popup-delay="1000" tooltip-class="my-custom-tooltip" tooltip-placement="top" tooltip-append-to-body="true">' +
                '<div class="ngCellText" ng-class="col.colIndex()" ng-click="editCell(row.entity, row.getProperty(col.field), col.field)">' +
                '<input value="{{row.entity[col.field][\'Name\']}}" placeholder="Культура" style="border: none;"  data-ng-click="updateSelectedCell(col.field, row.entity[col.field][\'Id\'],\'Культура\',row.entity,$event)"  readonly>' +
                '</div>'
            },
            {
                field: "ShortPlantName", displayName: "Краткое имя", width: "25%",
                cellTemplate: '<a tooltip="{{row.entity[col.field][\'Name\']}}" tooltip-popup-delay="1000" tooltip-class="my-custom-tooltip" tooltip-placement="top" tooltip-append-to-body="true">' +
                '<div class="ngCellText" ng-class="col.colIndex()" ng-click="editCell(row.entity, row.getProperty(col.field), col.field)">' +
                '<input value="{{row.entity[col.field][\'Name\']}}" placeholder="Краткое имя" style="border: none;"  data-ng-click="updateSelectedCell(col.field, row.entity[col.field][\'Id\'],\'Краткое имя\',row.entity,$event)"  readonly>' +
                '</div>'
            },
            {
                field: "Description", displayName: "Описание", width: "25%",
                cellTemplate: '<a tooltip="{{row.entity[col.field][\'Name\']}}" tooltip-popup-delay="1000" tooltip-class="my-custom-tooltip" tooltip-placement="top" tooltip-append-to-body="true">' +
                '<div class="ngCellText" ng-class="col.colIndex()" ng-click="editCell(row.entity, row.getProperty(col.field), col.field)">' +
                '<input value="{{row.entity[col.field][\'Name\']}}" placeholder="Описание" style="border: none;"  data-ng-click="updateSelectedCell(col.field, row.entity[col.field][\'Id\'],\'Описание\',row.entity,$event)"  readonly>' +
                '</div>'
            }
        ],
        afterSelectionChange: function (row) {
            sc.mySelections = [];
            sc.mySelections.push(row.entity);
            var sortsTest = [{Id: 1, Name: 'Первый'}, {Id: 21, Name: 'Второй'}, {Id: 3, Name: 'Третий'}];
            sc.tableDataSorts = sortsTest;
        }
    };

    /*requests.serverCall.post($http, "CatalogController/GetCatalog", true, function (data, status, headers, config) {
     console.info(data);
     sc.infoData = data;
     });*/

    // редактирование ячек с модальными окнами
    sc.updateSelectedCell = function (colField, selectedVal, displayName, row) {
        sc.colField = colField;
        var modalValues;
        if (sc.infoData) {
            switch (sc.colField) {
                case "TypePlant":
                    modalValues = sc.infoData.TypePlant;
                    break;
                case "PlantName":
                    modalValues = sc.infoData.PlantName;
                    break;
                case "ShortPlantName":
                    modalValues = sc.infoData.ShortPlantName;
                    break;
                case "Description":
                    modalValues = sc.infoData.Description;
                    break;
            }
            modals.showWindow(modalType.WIN_SELECT, {
                nameField: displayName,
                values: modalValues,
                selectedValue: selectedVal
            })
                .result.then(function (result) {
                    console.info(result);
                    sc.mySelections[0][sc.colField] = result;
                    if (sc.colField == 'PlantName') sc.mySelections[0]['Sort'] = '';
                }, function () {
                    console.info('Modal dismissed at: ' + new Date());
                });

        } else {
            console.log('не пришла sc.infoData');
        }
    };

    sc.createPlant = function () {
        var newCulture = {
            TypePlant: {Id: null, Name: null},
            PlantName: null,
            ShortPlantName: null,
            Description: null
        };
        sc.tableDataPlants.splice(0, 0, newCulture);
    };

    sc.deletePlant = function () {
        if (sc.mySelections[0]) {
            modals.showWindow(modalType.WIN_DIALOG, {
                    title: "Удаление", message: "Удалить выбранную культуру?", callback: function () {
                        if (sc.mySelections[0].detailDocId) {
                            /* requests.serverCall.post($http, "SalariesCtrl/DeleteEmployeeSalaryInfo", sc.mySelections[0]["detailDocId"], function (data, status, headers, config) {
                             console.info(data);
                             sc.tableData.forEach(function (item, d) {
                             if (item["detailDocId"] == sc.mySelections[0].detailDocId) sc.tableData.splice(d, 1);
                             })
                             });*/
                        } else {
                            sc.tableData.forEach(function (item, d) {
                                if (item == sc.mySelections[0]) sc.tableData.splice(d, 1);
                            })
                        }
                        //selectRowByField("detailDocId", "first");
                    }
                }
            );
        } else {
            modals.showWindow(modalType.WIN_MESSAGE, {
                title: "Удаление строки",
                message: "Для удаления необходимо выбрать строку",
                callback: function () {
                }
            });
        }
    };


    // свойства таблицы сортов
    sc.gridOptionsSorts = {
        data: 'tableDataSorts',
        enableColumnResize: true,
        enableRowSelection: true,
        showFooter: false,
        rowHeight: 49,
        headerRowHeight: 49,
        selectedItems: sc.selectedRow,
        multiSelect: false,
        columnDefs: [
            {
                field: "Sort", displayName: "Сорт", width: "100%",
                cellTemplate: '<a tooltip="{{row.entity[col.field][\'Name\']}}" tooltip-popup-delay="1000" tooltip-class="my-custom-tooltip" tooltip-placement="top" tooltip-append-to-body="true">' +
                '<div class="ngCellText" ng-class="col.colIndex()" ng-click="editCell(row.entity, row.getProperty(col.field), col.field)">' +
                '<input value="{{row.entity[col.field][\'Name\']}}" placeholder="Сорт" style="border: none;"  data-ng-click="updateSelectedCell(col.field, row.entity[col.field][\'Id\'],\'Сорт\',row.entity,$event)"  readonly>' +
                '</div>'
            }
        ],
        afterSelectionChange: function (row) {
            sc.mySelections = [];
            sc.mySelections.push(row.entity);
        }
    };

    sc.createSort = function () {
        var newSort = {
            Sort: {Id: null, Name: null}
        };
        sc.tableDataSorts.splice(0, 0, newSort);
    };

    sc.deleteSort = function () {
        if (sc.mySelections[0]) {
            modals.showWindow(modalType.WIN_DIALOG, {
                    title: "Удаление", message: "Удалить выбранный сорт?", callback: function () {
                        if (sc.mySelections[0].detailDocId) {
                            /* requests.serverCall.post($http, "SalariesCtrl/DeleteEmployeeSalaryInfo", sc.mySelections[0]["detailDocId"], function (data, status, headers, config) {
                             console.info(data);
                             sc.tableData.forEach(function (item, d) {
                             if (item["detailDocId"] == sc.mySelections[0].detailDocId) sc.tableData.splice(d, 1);
                             })
                             });*/
                        } else {
                            sc.tableData.forEach(function (item, d) {
                                if (item == sc.mySelections[0]) sc.tableData.splice(d, 1);
                            })
                        }
                        //selectRowByField("detailDocId", "first");
                    }
                }
            );
        } else {
            modals.showWindow(modalType.WIN_MESSAGE, {
                title: "Удаление строки",
                message: "Для удаления необходимо выбрать строку",
                callback: function () {
                }
            });
        }
    };


    sc.cancelClick = function () {
        $location.path('/catalog');
    }
}]);