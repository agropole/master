var app = angular.module("RightPanel", ["services"]);
app.controller("RightPanelCtrl", ["$scope", "$cookieStore", "$http", "requests", "$stateParams", "LogusConfig","$location","modals","modalType", function ($scope, $cookieStore, $http, requests, $stateParams, LogusConfig, $location, modals,modalType) {
    var sc = $scope;
	
    sc.infoData = {};
	sc.infoData1 = {};
	sc.spoilerCult = false;
	sc.spoilerSzr = false;
	sc.spoilerFields = false;	
	sc.formData = [];
    sc.infoData1.configUrl = LogusConfig.serverUrl;
   
    var curUser = $cookieStore.get('currentUser');
    if (curUser.currentRole === 112) {
        sc.cultureShow = true;
        sc.mapShow = false;
    } else if (curUser.currentRole === 113) {
        sc.cultureShow = false;
        sc.mapShow = true;
        
    }
	var markerVectorLayer ; var vectorSource; var marker;var win ;
	  sc.showMarker = function(name, coord) {
	if (markerVectorLayer) {
		  map.removeLayer(markerVectorLayer);
	}
	 var path = '../photo_fields\\' + name;
	  win = window.open(path, '', "width=500,height=300");
	//удаляем маркер
	 win.onbeforeunload = function(){
     map.removeLayer(markerVectorLayer);
 };
	
	if (coord) {
		var lst = [];
		var arr = coord.split(',');
		var lon = parseFloat(arr[0]);
		var lat = parseFloat(arr[1]);
	
	 marker = new ol.Feature({
    geometry: new ol.geom.Point(
    ol.proj.fromLonLat([lon, lat])
	),  
	});
	marker.setStyle(new ol.style.Style({
        image: new ol.style.Icon(({
            crossOrigin: 'anonymous',
            src: 'images/map-marker.png'
        }))
    }));
	marker.xp = -1;
	 vectorSource = new ol.source.Vector({
	features: [marker]
	});
	
	 markerVectorLayer = new ol.layer.Vector({
	 source: vectorSource,
	});
	  map.addLayer(markerVectorLayer);
	
	var fs = vectorSource.getFeatures();
	var f = fs[0];
	
	//нажатие на маркер
	map.on("click", function(e) {
    map.forEachFeatureAtPixel(e.pixel, function (feature, l) {   
	if (feature.xp == -1) {
	win.focus();
    map.unByKey(e);
	}
    })
       });
	//
	
	animateMap(fs[0].getGeometry().getCoordinates(), true);

	}
	  }
	
	    sc.delPhoto = function(id) {			
		modals.showWindow(modalType.WIN_DIALOG, {
         title: "Удаление", message: "Вы уверены, что хотите удалить запись?", callback: function () {
			
	    requests.serverCall.post($http, "AdministrationCtrl/DeletePhoto",id, function (data, status, headers, config) {
                        console.info(data);
						$("#" + id).hide();
                    }, function () {
                    });
					              
          }});
	   }
   
	 $("#StartDate").datetimepicker({
		language: "ru",
		format: 'dd-mm-yyyy hh:ii',  
		autoclose: true,
        todayBtn: true,
		todayHighlight: true,
		pickerPosition: 'top-right'
	});
	$("#EndDate").datetimepicker({
		language: "ru",
		format: 'dd-mm-yyyy hh:ii',  
		autoclose: true,
        todayBtn: true,
		todayHighlight: true,
		pickerPosition: 'top-right'
	});

	sc.getGPStrack = function ()
	{
		var trak_id = document.getElementById('traktor').innerHTML;
		
		var start_time = document.getElementById('StartDate').value;
		var stop = sc.formData.Stop;
		var parking = sc.formData.Parking;
		console.log(start_time);
		var end_time = document.getElementById('EndDate').value;
		console.log(end_time);
		if (end_time !== '' && start_time !== '')
		{
			console.log(trak_id);
			getGPStrack(trak_id, start_time, end_time, stop,parking);
			
			
		}
		
	}
	
	
    /*-------------------------------------- end отказы - приятние счетов -------------------------------------*/
    /*------------------------------------- пока так левая панель выезжает ------------------------------------*/
    $('.infoButton').click(function (e) {
		  var element = document.getElementById('popup');
           $(element).popover('destroy');
		   sc.formData.Parking = false;
		   sc.formData.Stop = false;
        if ($('.panelInfo').hasClass('openPanel')) {          
			$('.infoButton').removeClass('infoButtonSelected');
			$('.panelInfo').removeClass('openPanel');
			var div = document.getElementById("infoPN");
			if (div.style.display !== "none") {
			div.style.display = "none";
			}
			} else {
            $('.infoButton').addClass('infoButtonSelected');
			var div = document.getElementById("infoPN");
			if (div.style.display === "none") {
			div.style.display = "block";
			}
			setTimeout(function () {
                sc.$apply(function () {
                    $('.panelInfo').addClass('openPanel');
                })
            }, 1);
        }
    });

    var createSearchList = function() {
        sc.searchDrop = getFeatures();
    };

    $('body').click(function (e) {
        if (!$(e.target).closest('.panelSearch').length && !$(e.target).closest('.tools').length) {
            $('.panelSearch').removeClass('openSearchPanel');
        }
    });
    //ФОТО
	sc.SpoilerPhoto = function (chack) {
	if(sc.spoilerFields == false && chack == 1) {
		sc.spoilerFields = true;
	    sc.spoilerSzr = false;
		sc.spoilerCult = false;
        }
	else sc.spoilerFields = false;	
	}
	
	
	
sc.ProgressClick = function () {
	 $location.path('/progress');	
	}

sc.SpoilerCulture = function(chack){
	if(sc.spoilerCult == false && chack == 1) {
		sc.spoilerCult = true;
		sc.spoilerSzr = false;
		sc.spoilerFields = false;
		}
	else sc.spoilerCult = false;
}
sc.SpoilerTMC = function(chack)  {
	if(sc.spoilerSzr == false && chack == 1) {
		sc.spoilerSzr = true;
		sc.spoilerCult = false;
		sc.spoilerFields = false;
			if(Object.keys(sc.seed).length != 0)sc.seedShow = true;
            else sc.seedShow = false;
            if(Object.keys(sc.szr).length  != 0)sc.szrShow = true;
            else sc.szrShow = false;
            if(Object.keys(sc.minFert).length  != 0)sc.minFertShow = true;
            else sc.minFertShow = false;
            if(Object.keys(sc.otherMater).length  != 0)sc.otherMater = true;
            else sc.otherMater = false;
        }
	else sc.spoilerSzr = false;
}

 sc.$on('SpoilerHide', function (event, args) {
            if (args.hide == true){
            	sc.SpoilerTMC();
            	sc.SpoilerCulture();
            }
        })

}]);