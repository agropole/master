/**
 *  @ngdoc controller
 *  @name app.controller:LoginController
 *  @description
 *  # LoginController
 *  Это контроллер, определеяющий поведение формы авторизации
 *  @requires $scope
 *  @requires $rootScope
 *  @requires $cookieStore
 *  @requires $location
 *  @requires AuthenticationService

 *  @property {Object} $rootScope: глобальная область видимости
 *  @property {Object} $scope: область видимости формы
 *  @property {function} $scope.login: событие нажатия кнопки ВОЙТИ
 */
'use strict';
angular.module('Authentication')

.controller('LoginController',
['$scope', '$rootScope', '$cookieStore', '$location', 'AuthenticationService',"requests",'$http',"modals","modalType",
    function ($scope, $rootScope,$cookieStore, $location, AuthenticationService,requests, $http, modals,modalType) {
        // reset login status
        $rootScope.userName = ' ';
        $rootScope.exit = ' ';
        AuthenticationService.ClearCredentials();

		$scope.typeButton = '1';
		$scope.textArr = '';
		
		 requests.serverCall.post($http, "REST/CheckKey", {}, function (data, status, headers, config) {
                  		console.log(data);
				  if (data == 2) {
						$scope.typeButton = '2';  
						$scope.textArr = 'Программа не активирована.';
				  }
				  
                });
		
		 $scope.yes = function () {
			 var key = $scope.Key;
			 
			  requests.serverCall.post($http, "REST/ActivateSite", { Comment: key }, function (data, status, headers, config) {
				 	 
			 modals.showWindow(modalType.WIN_MESSAGE, {
                title: "Сообщение",
                message: data,
                callback: function () {
                }
            });
				$scope.no();
				 $scope.typeButton = '1'; 
				 $scope.textArr = '';
				  
                });
			 
		 }
		
		/*
		$('#key').keypress(function(eventObject)  { 
			$scope.Key = $scope.Key.toUpperCase();
		});
		*/
		
		     $scope.no = function () {
	           $("#myModal").modal("hide");
	           $("div.modal-backdrop").hide();
	      }
		  
      $scope.cancel = function () {
		 $("#myModal").modal("hide");    
	  }
		
		$scope.activate = function () {
			    $("#myModal").modal("show");
			
			/*
			 requests.serverCall.post($http, "REST/ActivateSite", {}, function (data, status, headers, config) {
                  		console.log(data.Value);
				  if (!data.Value) {
						$scope.typeButton = '2';  
						$scope.textArr = 'Программа не активирована.';
				  }
				  
                });
				*/
				
		}
		
		
		
		//войти
        $scope.login = function () {
			
            $scope.dataLoading = true;
            AuthenticationService.Login($scope.username, $scope.password, function (response) {
                if (response.success) {
                    AuthenticationService.SetCredentials($scope.username, $scope.password, response.data);
                    $rootScope.checkRoles();
                } else {
                    $scope.error = response.message;
                    $scope.dataLoading = false;
                }
            });
        };
    }]);