/**
 *  @ngdoc controller
 *  @name ShiftTask.contorller:CreateShiftTaskCtrl
 *  @description
 *  # CreateShiftTaskCtrl
 *  Это контроллер, определеяющий поведение формы Сменных заданий
 *  @requires $scope
 *  @requires $http
 *  @requires requests
 *  @requires modalType
 *  @requires modals
 *  @requires $location
 *  @requires $cookieStore
 *  @requires $stateParams
 *  @requires $rootScope
 *  @requires preloader

 *  @property {Object} sc scope
 *  @property {Object} sc.shifts смены
 *  @property {bool} sc.editBtnVis видимость кнопок для Бачурина
 *  @property {bool} sc.createWayBtnVis видимость кнопки Создать путевой лист (для диспетчеров)
 *  @property {bool} sc.disCreateWayBill блокирование кноки Создать путевой лист, если он не был утвержден
 *  @property {string} sc.DayTaskId id текущего сменного задания для выделения после перехода с и на Путевые листы
 *  @property {Array} sc.PrintShiftInfo1 НЕ ЗНАЮ
 *  @property {Array} sc.PrintShiftInfo2 НЕ ЗНАЮ
 *  @property {Object} sc.multi scope
 *  @property {Object} sc.multi разрешает мультивыделение
 *  @property {Object} sc.Comment общий комментарий для дня
 *  @property {Array} sc.dataTraktors незанятая техника
 *  @property {Array} sc.dataEmployees незанятые водители 
 *  @property {Object} curUser информация о пользователе
 *  @property {int} sc.activeTab активная вкладка
 *  @property {Object} sc.tableDataFirst данные таблицы первой смены
 *  @property {Object} sc.tableDataSecond данные таблицы второй смены
 *  @property {Object} sc.selectedRow выделенные строки
 *  @property {Object} sc.columnDefinitions настройки колонок
 *  @property {Object} sc.gridFirstOptions настройки таблицы 1 смены
 *  @property {Object} sc.gridSecondOptions настройки таблицы 2 смены


 *  @property {function} sc.getClassCopyCell стиль скопированной строки
 *  @property {function} getDayTasks получение data по двум сменам за определенную дату
 *  @property {function} sendTask отправка измененного задания на сервер
 *  @property {function} sc.dateChangeShifts изменение даты отображения сменных заданий
 *  @property {function} sc.checkTab переключение вкладки смен
 *  @property {function} needSelection модальное окно с требованием выделения строки
 *  @property {function} selectRowByField выделение строчки по определенному столбцу
 *  @property {function} sc.gridEvDataListener листнер на событие изменение data в таблице
 *  @property {function} compare сравнение ячеек при сортировке по столбцу
 *  @property {function} selectFirstRow при переключении по вкладкам и обновлении даты - всегда выделяем верхние строки
 *  @property {function} keyDownGrid мультивыделение
 *  @property {function} keyUpGrid мультивыделение
 *  @property {function} sc.createTask создание сменного задания
 *  @property {function} sc.copyTaskRow коприрование сменного задания
 *  @property {function} sc.deleteTask удаление сменного задания (запрос)

 *  @property {function} deleateTasks удаление сменного задания (вырезка из массива таблицы)
 *  @property {function} sc.createWayBill создание путевого листа на основе сменного задания ( у роли 3 (Злодеев))
 *  @property {function} getPrintInfo получение информации для печати
 *  @property {function} sc.printTask выгрузка в Excel отчета
 *  @property {function} sc.updateSelectedCell модальное окно для выбора элемента в таблице сменных заданий
 *  @property {function} sc.editCell вызывается при редактировании ячейки
 *  @property {function} sc.mainCommentClick добавление важного комментария (только у роли Бачурина (2))
 *  @property {function} sc.copyTasksFromYesterday скопировать из предыдущего дня сменные задания (только у роли Бачурина (2))
 *  @property {function} sc.approveTask утверждение сменного задания (только у роли Бачурина (2))
 */

var app = angular.module("ShiftTaskRapairs", ["ngGrid", "ui.bootstrap", "services", "modalWindows", 'Directives', "ui.utils", "LogusConfig"]);

app.controller("ShiftTaskRepairsCtrl", ["$rootScope", "$scope", "$http", "requests", "$location", '$cookieStore', '$rootScope', "modals", "modalType", "LogusConfig", "preloader", function ($rootScope, $scope, $http, requests, $location, $cookieStore, $rootScope, modals, modalType, LogusConfig, preloader) {
    var sc = $scope;
    sc.shifts = {};
    sc.editBtnVis = false;
    sc.createWayBtnVis = false;
    sc.disCreateWayBill = false;
	var firstLoad = false;
    sc.DayTaskId = "";
    sc.PrintShiftInfo1 = [];
    sc.PrintShiftInfo2 = [];
    sc.multi = false;
    sc.Comment = "";
    sc.dataTraktors = [];
    sc.dataEmployees = [];
	sc.mySelections1 =[];
	sc.infoData2 = [];
	sc.formData = [];
	var count = 0;
	var met = 1;
    var curUser = $cookieStore.get('currentUser');
    console.info(curUser);
    sc.tporgid = curUser.tporgid;
    if (curUser.roleid == '2' || curUser.roleid == '10') {
        sc.editBtnVis = true;
        sc.createWayBtnVis = false;
    } else if (curUser.roleid == '3' || curUser.roleid == '4' || curUser.roleid == '8') {
        sc.editBtnVis = false;
        sc.createWayBtnVis = true;
    } else if (curUser.roleid == '7'){
        sc.editBtnVis = false;
        sc.createWayBtnVis = false;
    }
  
	
    if ($cookieStore.get('dateCalendar')) {
        if ($cookieStore.get('dateCalendar').date) sc.dateShift = $cookieStore.get('dateCalendar').date;
    }
    if ($cookieStore.get('shiftWay')) {
        if ($cookieStore.get('shiftWay').shift == '1') {
            sc.shifts = {shift2: false, shift1: true};
            sc.activeTab = '1';
        } else if ($cookieStore.get('shiftWay').shift == '2') {
            sc.shifts = {shift2: true, shift1: false};
            sc.activeTab = '2';

        }
    } else {
        sc.shifts = {shift1: false, shift2: true};
        sc.activeTab = '2';
    }
	 
    sc.tableDataFirst = [];
    sc.tableDataSecond = [];
    sc.selectedRow = [];

    sc.columnDefinitions = [
      	   {field: "Number", displayName: "№ НН", width: "6%",
            cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()">' +
            '<input value="{{row.entity[col.field]}}" ng-model="row.entity[col.field]" placeholder="№ п/п" class="bordernone" readonly>' +
            '</div>',headerCellTemplate: "<div   ng-class=\"'colt' + col.index\" class=\"ngHeaderText\">{{col.displayName}}</div>"
            },
		   {
            field: "EquipmentsInfo", displayName: "Агрегат", width: "15%",
            cellTemplate: '<div class="ngCellText {{getClassCopyCell(row.entity.Status)}}" ng-class="col.colIndex()" ng-click="editCell(row.entity, row.getProperty(col.field), col.field)">' +
            '<!--div data-ng-click="updateSelectedCell(\'TracktorInfo\', row.entity[col.field][\'TracktorInfo\'][\'Id\'],\'Техника\', $event)" >{{row.entity[col.field][\'TracktorInfo\'][\'Name\']}} +</div-->' +
            '<a tooltip="{{row.entity[col.field][\'TracktorInfo\'][\'Name\']}}" tooltip-popup-delay="1000" tooltip-class="my-custom-tooltip" tooltip-placement="top" tooltip-append-to-body="true">' +
            '<input class="double-input" value="{{row.entity[col.field][\'TracktorInfo\'][\'Name\']}}" placeholder="Техника" style="border: none; {{row.entity[col.field][\'TracktorInfo\'][\'RepairStatus\']}}"  data-ng-click="updateSelectedCell(\'TracktorInfo\', row.entity[col.field][\'TracktorInfo\'][\'Id\'],\'Техника\',row.entity,$event)" readonly>' +
            '</a>' +
            '<!--div data-ng-click="updateSelectedCell(\'EquipInfo\', row.entity[col.field][\'EquipInfo\'][\'Id\'],\'Оборудование\', $event)">{{row.entity[col.field][\'EquipInfo\'][\'Name\']}}</div-->' +
            '<a tooltip="{{row.entity[col.field][\'EquipInfo\'][\'Name\']}}" tooltip-popup-delay="1000" tooltip-class="my-custom-tooltip" tooltip-placement="top" tooltip-append-to-body="true">' +
            '<input class="double-input" value="{{row.entity[col.field][\'EquipInfo\'][\'Name\']}}" placeholder="Оборудование" style="border: none;"  data-ng-click="updateSelectedCell(\'EquipInfo\', row.entity[col.field][\'EquipInfo\'][\'Id\'],\'Оборудование\',row.entity,$event)" readonly>' +
            '</a>' +
            '</div>'
        },
        {
            field: "TypeTaskInfo", displayName: "Планируемые работы", width: "15%",
            cellTemplate: '<a tooltip="{{row.entity[col.field][\'Name\']}}" tooltip-popup-delay="1000" tooltip-class="my-custom-tooltip" tooltip-placement="top" tooltip-append-to-body="true">' +
            '<div class="ngCellText {{getClassCopyCell(row.entity.Status)}}" ng-class="col.colIndex()" ng-click="editCell(row.entity, row.getProperty(col.field), col.field)">' +
            '<!--div data-ng-click="updateSelectedCell(col.field, row.entity[col.field][\'Id\'],\'Планируемые работы\',$event)">{{row.entity[col.field][\'Name\']}}</div-->' +
            '<input value="{{row.entity[col.field][\'Name\']}}" placeholder="Планируемые работы" style="border: none;"  data-ng-click="updateSelectedCell(col.field, row.entity[col.field][\'Id\'],\'Планируемые работы\',row.entity,$event)" readonly>' +
            '</div>' +'</a>'
        },
       {
            field: "EmpList", displayName: "Персонал", width: "29%",
            cellTemplate: '<div class="{{getClassChanges()}} ngCellText" ng-class="col.colIndex()" style="padding: 0px 5px;">' +
            '<input ng-value="getValueForInput(row.entity.EmpList)" placeholder="Персонал" style="border: none; height: 98%" data-ng-click="updateSelectedCell(\'EmpList\', row.entity.EmpList.Id,\'Персонал\',row.entity,$event)"  readonly>' +
            '</div>'
        },
        {
            field: "AvailableInfo", displayName: "Ответственный", width: "15%",
            cellTemplate: '<a tooltip="{{row.entity[col.field][\'Name\']}}" tooltip-popup-delay="1000" tooltip-class="my-custom-tooltip" tooltip-placement="top" tooltip-append-to-body="true">' +
            '<div class="ngCellText {{getClassCopyCell(row.entity.Status)}}" ng-class="col.colIndex()" ng-click="editCell(row.entity, row.getProperty(col.field), col.field)">' +
            '<!--div data-ng-click="updateSelectedCell(col.field, row.entity[col.field][\'Id\'],\'Ответственный\',$event)">{{row.entity[col.field][\'Name\']}}</div-->' +
            '<input value="{{row.entity[col.field][\'Name\']}}" placeholder="Ответственный" style="border: none;"  data-ng-click="updateSelectedCell(col.field, row.entity[col.field][\'Id\'],\'Ответственный\',row.entity,$event)"  readonly>' +
            '</div>' +
            '</a>'
        },
        {
            field: "Comment", displayName: " ", width: "5%",
            cellTemplate: 'templates/comment_shift_template.html', cellClass: 'cellToolTip'
        },
		 {
            field: "NumDailyId", displayName: "№ ЕН", width:"10%",
            cellTemplate: '<div ng-show="!row.entity.DetailId && !row.entity.NumDailyId"  class="{{getClassTempCell(row.entity.TemplateStatus,row)}} '+
			'ngCellText {{getClassCopyCell(row.entity.Status,row)}}"><button type="button"'+  
			'class="logus-btn btn btn-border ng-click-active" style="width: 100%;margin-top: 3px"    ng-click="saveRepairs(row.entity)">Сохранить</button></div>' +
			
			'<div ng-show="row.entity.DetailId && !row.entity.NumDailyId"  class="{{getClassTempCell(row.entity.TemplateStatus,row)}} '+
			'ngCellText {{getClassCopyCell(row.entity.Status,row)}}"><button type="button"'+  
			'class="logus-btn btn btn-border ng-click-active" style="width: 100%;margin-top: 3px"    ng-click="createDaily(row.entity)">ЕН</button></div>'+
	
	        '<div ng-show="row.entity.NumDailyId" class="ngCellText" ng-class="col.colIndex()">' +
            '<input value="{{row.entity[col.field]}}" ng-model="row.entity[col.field]" placeholder="№ е/н" class="bordernone" readonly>' +
            '</div>'
        },
		
	
        {
            field: "Status", displayName: " ", width: "5%",
            cellTemplate: 'templates/status_shift_template.html'
        },
        {field: "DetailId", width: "0%"},
    ];

    // пока так, потом придумаю как убрать копипаст
    sc.gridFirstOptions = {
        data: 'tableDataFirst',
        enableColumnResize: true,
        enableRowSelection: true,
        showFooter: false,
        rowHeight: 49,
        headerRowHeight: 49,
        selectedItems: sc.selectedRow,
        columnDefs: sc.columnDefinitions,
        useExternalSorting: true,
        afterSelectionChange: function (row) {	 
			met = 1;
			for (var i = 0; i < row.length; i++ ) 
          { 
             sc.selectedRow.push(row[i].entity); 
          } 
			if (sc.mySelections1==row.entity) { 
			console.log('есть');   met=2;
			}
			sc.mySelections1 = row.entity;
            if (row.entity) {
                if (row.selected) {
                    if (sc.multi) sc.selectedRow.push(row.entity);
                    else {
					    console.log('clear');
                        sc.selectedRow = [];
                        sc.selectedRow.push(row.entity);
                    }
                } else {
                    sc.selectedRow.splice(sc.tableDataFirst.indexOf(row.entity), 1);
                }
            }
			  //console.log(sc.selectedRow); 
        },
        beforeSelectionChange: function (row, event) {
            if (!sc.multi) {
                angular.forEach(sc.tableDataFirst, function (data, index) {
                    if (data.selected == true) sc.selectedRow.splice(index, 1);
                    sc.gridFirstOptions.selectRow(index, false);

                });
            }
            return true;
        }
    };

	sc.getDisplayName = function (row) { 
	
	return "123";
	}
	
	
	sc.createDaily = function (row) { 
	   var checkRow = 0;
	   console.log(row);
	   if (row.EmpList == null || row.EmpList.length == 0) {
		  checkRow = 1;	  
	   }
	   if (row.TypeTaskInfo == null || row.TypeTaskInfo.Id == null) {
		  checkRow = 1;	  
	      }
      if (checkRow == 1)  {needSelection('Предупреждение', 'Заполните поля: "Планируемые работы", "Персонал". ' )  ;  }  else {
		  	  sc.dataTask = {};
        sc.dataTask.Date = moment(sc.dateShift).format("MM.DD.YYYY");
        sc.dataTask.ShiftInfo = row;
		  requests.serverCall.post($http, "SparePartsCtrl/CreateShiftDaily", sc.dataTask, function (data, status, headers, config) {
                    console.info(data);
					row.NumDailyId = data;
                    getDayTasks();   
                }, function () {
                });
		  
	  }
	
	}
	  //загрузить открытые ранее
	sc.loadTask = function () {
		  requests.serverCall.post($http, "SparePartsCtrl/LoadRepairs", {DateFilter: moment(sc.dateShift).format("MM.DD.YYYY") }, function (data, status, headers, config) {
                    console.info(data);

					//===
	 requests.serverCall.post($http, "SparePartsCtrl/GetDayRepairs",
		{"DateFilter": moment(sc.dateShift).format("MM.DD.YYYY"), OrgId: curUser.orgid,FilterOrg: sc.formData.OrgId  }, function (data, status, headers, config) {
            console.info(data);
            sc.selectedRow = [];
            sc.DayTaskId = data.DayTaskId;
            sc.tableDataFirst = data.Shift1;
            sc.DayTaskId = data.DayTaskId;
            sc.Comment = data.Comment;
        }, function () {
        });
					///
                }, function () {
                });
		
	}
	
	
	
	//сохранить строку
	sc.saveRepairs = function (row) {
		var checkRow = 0;
		//console.log(row);
		
		if ( (row.EquipmentsInfo.EquipInfo == null || row.EquipmentsInfo.EquipInfo.Id  == null)  
		&& (row.EquipmentsInfo.TracktorInfo == null || row.EquipmentsInfo.TracktorInfo.Id == null)) {
			checkRow = 1;	
		}
		if (row.AvailableInfo == null || row.AvailableInfo.Id == null ) 
		{
				checkRow = 1;	
		}
		if (checkRow == 1 )  {needSelection('Предупреждение', 'Заполните поля: "Агрегат", "Ответственный". ' )  ;  }
		
		else {
			  sc.dataTask = {};
        sc.dataTask.Date = moment(sc.dateShift).format("MM.DD.YYYY");
        sc.dataTask.ShiftInfo = row;
		sc.dataTask.OrgId = curUser.orgid;
			
			
			  requests.serverCall.post($http, "SparePartsCtrl/CreateShiftRep", sc.dataTask, function (data, status, headers, config) {
                    console.info(data);
					row.DetailId = data;
					   $("#create").removeClass("disabledbutton");
                }, function () {
                });
			
		}
		
		
	}
	
    // стиль скопированной строки
    sc.getClassCopyCell = function (stat) {
        if (stat == '2') {
            return 'copy-cell';
        }
    };

    if (curUser.roleid == '3' || curUser.roleid == '4' || curUser.roleid == '8') {
        sc.gridFirstOptions.enableCellEdit = false;
    }
	    //ФИЛЬТР ОТДЕЛЕНИЯ
			sc.changeOrg = function () {
				if (firstLoad == true) {
			     getDayTasks();
				}
	    	}
				
			sc.checkRes = function () {
			//	 sc.GetStyleShiftTask("FreeTechEmp");
					}
				
				
		
    // получение data по двум сменам за определенную дату
    function getDayTasks() {
		
		if (firstLoad == false) {
			if (curUser.tporgid == 2) {
				sc.formData.OrgId = -1;
			}  else {
			sc.formData.OrgId = curUser.orgid;	
			}
		}	
        requests.serverCall.post($http, "SparePartsCtrl/GetDayRepairs",
		{"DateFilter": moment(sc.dateShift).format("MM.DD.YYYY"), OrgId: curUser.orgid,FilterOrg: sc.formData.OrgId  }, function (data, status, headers, config) {
            console.info(data);
            sc.selectedRow = [];		
			sc.dataEmployees = data.FreeEmp;
            sc.DayTaskId = data.DayTaskId;
            sc.tableDataFirst = data.Shift1;
            sc.DayTaskId = data.DayTaskId;
            sc.Comment = data.Comment;
            if (!sc.infoData) {
				firstLoad = true;
			//		sc.formData.OrgId = sc.infoData.OrganizationsList[0].Id;
				/*
                requests.serverCall.post($http, "DayTasksCtrl/GetDictionaryRepairs", {OrgId: curUser.orgid, FilterOrg: sc.formData.OrgId}, function (data, status, headers, config) {
                    console.info(data);
                    sc.infoData = data;
					sc.formData.OrgId = sc.infoData.OrganizationsList[0].Id;
					firstLoad = true;
                }, function () {
                });
				*/
			
			  requests.serverCall.post($http, "SparePartsCtrl/GetDictionaryRepairs", {repairStartDate : moment(sc.dateShift).format("MM.DD.YYYY")}, 
			  function (data, status, headers, config) {
						console.info(data);
						sc.infoData = data;
						firstLoad = true;
					    }, function () {
                });
					
					
					
            }
            if ($cookieStore.get('selectedShiftTask')) {
                selectRowByField("DetailId", $cookieStore.get('selectedShiftTask').id);
                $cookieStore.remove('selectedShiftTask');
            } 
		//	else
		//		selectFirstRow();
       //     getPrintInfo();
        }, function () {
        });
    }

    // отправка измененного задания на сервер
    function sendTask(task) {
        sc.dataTask = {};
        sc.dataTask.Date = moment(sc.dateShift).format("MM.DD.YYYY");
        sc.dataTask.ShiftInfo = sc.selectedRow[0];
		sc.dataTask.OrgId = curUser.orgid;
        console.log('SAVE sendTask selected ' + JSON.stringify(sc.dataTask));
        requests.serverCall.post($http, "DayTasksCtrl/UpdateShiftTask", sc.dataTask, function (data, status, headers, config) {
            console.info(data);
            selectRowByField("DetailId", data);
        });
		
    }

    // изменение даты отображения сменных заданий
    sc.dateChangeShifts = function () {
        if (sc.dateShift) {
            $cookieStore.put('dateCalendar', {date: sc.dateShift});
            getDayTasks();
            var tomorrow = moment(sc.dateShift).add('days', 1)._d;
        } else {
            console.log('wrong date from datepick')
        }

    };

	$("#btn").show();
	
	/*
    // переключение вкладки смен
    sc.checkTab = function (tab) {
        sc.activeTab = tab;
        sc.selectedRow = [];
        if (!$cookieStore.get('selectedShiftTask'))selectFirstRow();
    };
	*/
	
    /*-----------------------------выделение строк----------------------------------*/
    // модальное окно с требованием выделения строки
    function needSelection(title, message) {
        modals.showWindow(modalType.WIN_MESSAGE, {
            title: title,
            message: message,
            callback: function () {
            }
        });
    }

    var _tasksArr, _gridOptions, _grid, _field, _val;
    // выделение строчки по определенному столбцу
    function selectRowByField(field, val) {
        console.info("selectRowByField " + field + ' ' + val);
        _field = field;
        _val = val;
        if (sc.activeTab == '1') {
            _tasksArr = sc.tableDataFirst;
            _gridOptions = sc.gridFirstOptions;
            _grid = sc.gridFirstOptions.ngGrid;
            //sc.tableDataFirst = sc.tableDataFirst;
        } 
        sc.$on('ngGridEventData', sc.gridEvDataListener);
    }

    // листнер на событие изменение data в таблице
    sc.gridEvDataListener = function () {
        var offCallMeFn = $scope.$on("ngGridEventData", sc.gridEvDataListener);
        offCallMeFn();
        if (_val == "first" && _tasksArr.length > 0) _val = _tasksArr[0][_field];
        //console.info("gridEvDataListener " + _field + '  ' + _val);
        _tasksArr.forEach(function (item, i, arr) {
            if (item[_field] == _val) {
                _gridOptions.selectRow(i, true);
                // console.info(_grid.rowMap[i] + '  ' + _grid.config.rowHeight);
                if (_grid) _grid.$viewport.scrollTop(_grid.rowMap[i] * _grid.config.rowHeight);
            } else {
                _gridOptions.selectRow(i, false);
            }
        });
        $(".ngViewport").focus();
    };

    var _sortedField;
    // событие на сортировку по столбцу
    sc.$on("ngGridEventSorted", function (e, field) {
            _sortedField = field;
            if (sc.activeTab == "1") {
                console.info(sc.tableDataFirst);
                var copyFirst = sc.tableDataFirst.sort(compare);
                sc.tableDataFirst = [];
                for (var t = 0, tl = copyFirst.length; t < tl; t++) {
                    sc.tableDataFirst.push(copyFirst[t]);
                }

            } 
            for (var d = 0, dl = sc.selectedRow.length; d < dl; d++) {
                selectRowByField("DetailId", sc.selectedRow[d]["DetailId"]);
            }

            if (!sc.$$phase) {
                sc.$apply(function () {
                });
            }
        }
    );

    // сравнение ячеек при сортировке по столбцу
    function compare(a, b) {
        var first, second;
        switch (_sortedField.field) {
            case "PlantInfo":
            case "FieldInfo":
            case "TypeTaskInfo":
            case "AvailableInfo":
                first = a[_sortedField.field]["Name"];
                second = b[_sortedField.field]["Name"];
                break;
            case "EquipmentsInfo":
                first = a[_sortedField.field]["TracktorInfo"]["Name"];
                second = b[_sortedField.field]["TracktorInfo"]["Name"];
                break;
            case "EmployeesInfo":
                first = a[_sortedField.field]["Emp1"]["Name"];
                second = b[_sortedField.field]["Emp1"]["Name"];
                break;
        }
        if (first == null && second == null)
            return 0;
        if (first == null && second != null)
            return _sortedField.sortDirection == "desc" ? 1 : -1;
        if (first != null && second == null)
            return _sortedField.sortDirection == "desc" ? -1 : 1;
        if (first < second)
            return _sortedField.sortDirection == "desc" ? -1 : 1;
        if (first > second)
            return _sortedField.sortDirection == "desc" ? 1 : -1;
        return 0;
    }


    //при переключении по вкладкам и обновлении даты - всегда выделяем верхние строки
    function selectFirstRow() {
        // пока такой костыль
        if (sc.activeTab == '1') {
            var copy = angular.copy(sc.tableDataFirst);
            sc.tableDataFirst = [];
            sc.tableDataFirst = copy;
        } 
        selectRowByField("DetailId", "first");
    }

    // мультивыделение
    sc.keyDownGrid = function (e) {
        if (curUser.roleid == '2' || curUser.roleid == '10') {
            if ((e.keyCode === 16 || e.keyCode === 17 || e.keyCode === 18 || e.keyCode === 91) && !sc.multi) {
              //  console.info("__________________ keyDown  " + e.keyCode);
                sc.multi = true;
             //   console.info(sc.multi);
            }
        }
    };

    sc.keyUpGrid = function (e) {
        // 16 - Shift, 17 - Ctrl, 18 - Alt, 91 - Win key (Start)
        if (curUser.roleid == '2' || curUser.roleid == '10') {
            if (e.keyCode === 16 || e.keyCode === 17 || e.keyCode === 18 || e.keyCode === 91) {
               // console.info("_________________ keyUp " + e.keyCode);
                sc.multi = false;
              //  console.info(sc.multi);
            }
        }
    };


    //создание сменного задания
    sc.createTask = function () {
      $("#create").addClass("disabledbutton");
        console.info(sc.selectedRow);
		
						  requests.serverCall.post($http, "SparePartsCtrl/GetDictionaryRepairs",
						  {repairStartDate : moment(sc.dateShift).format("MM.DD.YYYY")}, function (data, status, headers, config) {
						console.info(data);
						sc.infoData = data;
	     //  sc.formData.RepairNumber = data.NewActNumber;
		
        var newTask = {
			Number: data.NewActNumber,
            MainComment: null,
            AvailableInfo: {Id: null, Name: null},
            DetailId: null,
            EmployeesInfo: {Emp1: {Id: null, Name: null}, Emp2: {Id: null, Name: null}},
            EquipmentsInfo: {EquipInfo: {Id: null, Name: null}, TracktorInfo: {Id: null, Name: null}},
            Shift: sc.activeTab,
            Comment: null,
            Status: null,
            TypeTaskInfo: {Id: null, Name: null}
        };
        newTask.Shift = sc.activeTab;

        sc.dataTask = {};
        sc.dataTask.Date = moment(sc.dateShift).format("MM.DD.YYYY");
        sc.dataTask.ShiftInfo = newTask;
        sc.dataTask.OrgId = curUser.orgid;
		
		sc.tableDataFirst.push(newTask); 
				 });
		/*
        requests.serverCall.post($http, "DayTasksCtrl/UpdateShiftTask", sc.dataTask, function (data, status, headers, config) {
            console.info(data);
            newTask["DetailId"] = data;
            newTask["Status"] = 0;
            selectRowByField("DetailId", data);
            if (sc.activeTab == '1') {
                if (sc.selectedRow.length > 0) {
                    for (var j = 0, jl = sc.tableDataFirst.length; j < jl; j++) {
                        if (sc.selectedRow[0] == sc.tableDataFirst[j]) {
                            sc.tableDataFirst.splice(j, 0, newTask);
                            j = jl;
                        }
                    }
                } else sc.tableDataFirst.splice(0, 0, newTask);
            } 	
        });
		*/
		
		
    };


	
	

    // удаление сменного задания (запрос)
    sc.deleteTask = function () {
		
        if (sc.selectedRow.length > 0) {
            var detailIdArr = [];
            for (var i = 0, il = sc.selectedRow.length; i < il; i++) {
                detailIdArr.push(sc.selectedRow[i]["DetailId"]);
            }
            console.info(detailIdArr);
            modals.showWindow(modalType.WIN_DIALOG, {
                    title: "Удаление", message: "Удалить выбранное задание?", callback: function () {
					 requests.serverCall.post($http, "SparePartsCtrl/DeleteDayRep", detailIdArr, function (data, status, headers, config) {
                            console.info(data);
							
                            deleateTasks(detailIdArr);
                        }, function () {
                            deleateTasks(detailIdArr);
                        });
                    }

                }
            );
        } else needSelection("Удаление", "Для удаления необходимо выбрать задание");
  
    };

    // удаление сменного задания (вырезка из массива таблицы)
    function deleateTasks(idArr) {
        var tableData = [];
			tableData = sc.tableDataFirst;
        for (var j = 0, jl = idArr.length; j < jl; j++) {
            tableData.forEach(function (item, d) {
                if (idArr[j] == item["DetailId"] && item["Status"] != 3) {
                    tableData.splice(d, 1);
                    if (tableData.length > 0) {
                        //console.info("выделяем после удаления " + tableData[0]["DetailId"]);
                        if (d - 1 > 0)selectRowByField("DetailId", tableData[d - 1]["DetailId"]);
                        else selectRowByField("DetailId", tableData[0]["DetailId"]);
                    } else sc.selectedRow = [];
                }
            })
        }

			               //проверка кнопки сохранить
							var checkRow = 0;
							   for (var i = 0, il = sc.tableDataFirst.length; i < il; i++) {
								   if (!sc.tableDataFirst[i].DetailId)  {
									   checkRow = 1;
									 	 //  $("#create").removeClass("disabledbutton");   
								   }
								   
							   }
							if (checkRow == 0) { $("#create").removeClass("disabledbutton");   }
		
    }

	
	
	

    function getPrintInfo() {
        sc.PrintShiftInfo1 = [];
        sc.PrintShiftInfo2 = [];
        requests.serverCall.post($http, "DayTasksCtrl/GetPrintShiftTasks", sc.DayTaskId, function (data, status, headers, config) {
            sc.DateShifts = data.DateShifts;
            sc.PrintShiftInfo1 = data.ShiftInfo1;
            sc.PrintShiftInfo2 = data.ShiftInfo2;
        });
    }

    // выгрузка в Excel отчета
    sc.printTask = function () {
		if (sc.DayTaskId) {
        var popupWin = window.open(LogusConfig.serverUrl + 'CostCtrl/GetDayRepairExcel?id=' + sc.DayTaskId + 
		                                              '&date=' + moment(sc.dateShift).format("MM.DD.YYYY"), '_blank');
		}
    };
	
	   sc.getValueForInput = function(array) {
        var str = "";
        if (array && array.length > 0) {
            for (var i = 0, li = array.length; i < li; i++) {
                str += array[i].Name + ", "
            }
            str = str.substr(0, str.length - 2);
        }
        return str;
    };

	
    // измениние значения в ячейке (только у роли Бачурина (2))
    sc.updateSelectedCell = function (colField, selectedVal, displayName, row) {
	
		met=1;
        if (curUser.roleid == '2' || curUser.roleid == '10') {
            if (!sc.multi) {
				  console.log('clear');
                sc.selectedRow = [];
                sc.selectedRow.push(row);
            }
		 	if (sc.mySelections1==row) {met=2;}
			console.log(row);
			sc.mySelections1 = row;
			console.log(sc.mySelections1);
            var typeModal = modalType.WIN_SELECT_SHIFT;
            var modalValues;
            var selectedColumn;		   
            if (sc.infoData) {
				if (met == 2) {				
                switch (colField) {
                    case "PlantInfo":
					console.log(sc.infoData.FieldsAndPlants);
                        modalValues = sc.infoData.FieldsAndPlants;
                        break;
                     case "TypeTaskInfo":
					 				if (row.DetailId) {
                      modalValues = sc.infoData.TypeTasksList;
									}
                        break;
                    case "TracktorInfo":
                        selectedColumn = 'EquipmentsInfo';
                        modalValues = sc.infoData.TraktorsShiftList; 
                        break;
                    case "EquipInfo":
                        selectedColumn = 'EquipmentsInfo';
                        modalValues = sc.infoData.EquipmnetsList;
                        break;
                    case "EmpList":
						if (row.DetailId) {
							//
							
							//====выделение
					   for (var i = 0; i < sc.infoData.EmployeesList.length; i++) {
						if (row.EmpList != null && row.EmpList.length != 0 ) {
						   for (var j = 0; j < row.EmpList.length; j++) {	
						     sc.infoData.EmployeesList[i].selectedFlag = false;
						   if (row.EmpList[j].Id == sc.infoData.EmployeesList[i].Id ) { 
					  	       sc.infoData.EmployeesList[i].selectedFlag = true;
							   break;
							}  }
							} else {
							sc.infoData.EmployeesList[i].selectedFlag = false;
							}
							   }
							//====	
                            typeModal = modalType.WIN_SELECT_MULTI;
							modalValues = sc.infoData.EmployeesList;
						   selectedVal = row[sc.colField] ? row[sc.colField] : [];
						
						//кнопка очистить
						function func2() {
						$( "#clear" ).click(function() {
                            for (var i = 0; i < sc.infoData.EmployeesList.length; i++) {
								sc.infoData.EmployeesList[i].selectedFlag = false;	
							}
                          });
	                    }
						setTimeout(func2, 100);
						
						}
                        break;
                    case "AvailableInfo":
                        modalValues = sc.infoData.EmployeesList;
                        break;
                    default:
                        console.log('updateSelectedCell: Не пришел colField ' + colField);
                }
			
					if ( (!((!row.DetailId && colField == 'TypeTaskInfo') || (!row.DetailId && colField == 'EmpList'))) && !row.NumDailyId && 
					!( (colField == 'TracktorInfo' || colField=='EquipInfo' || colField=='AvailableInfo')  &&  row.DetailId)    ) {
                    modals.showWindow(typeModal, {
                    nameField: displayName,
                    values: modalValues,
                    selectedValue: selectedVal
                })
                    .result.then(function (result) {
                        console.info(result);
						 console.info(row.FieldInfo);
						 var cult = 0;
						  console.info(colField);
						   console.info(row);

						 if (colField == 'PlantInfo' && row.FieldInfo!=null && row.FieldInfo != undefined && result.Values != undefined)
						 {
							 for (var i=0;i<result.Values.length;i++)
							 {
								 if (result.Values[i].Id==row.FieldInfo.Id) {cult=1;}
							 }
							 if (cult==0) {row.FieldInfo = {Id : null, Name : null} ;   console.info('ноль');}
						 }
				 				
							if (colField == 'TracktorInfo') {
								row.EquipmentsInfo.EquipInfo = null;
						        }
								if (colField == 'EquipInfo') {
								row.EquipmentsInfo.TracktorInfo = null;
						        }	
								

                        if (colField == 'EquipInfo' || colField == 'TracktorInfo' || colField == 'Emp1' || colField == 'Emp2') {
                            sc.selectedRow[0][selectedColumn][colField] = result;
                        } else {
                            sc.selectedRow[0][colField] = result;
                        }
                        if (sc.colField == 'PlantInfo') sc.selectedRow[0]['FieldInfo'] = '';
                     //   sendTask();
                    }, function () {
                        console.info('Modal dismissed at: ' + new Date());
                    });				
					
					
					}
			}  	
            }
        }
             //	
    };

    // вызывается при редактировании ячейки
    sc.editCell = function (row, cell, column) {
			met=1;
			 if (!sc.multi) {
			console.log('clear');
        sc.selectedRow = [];
        sc.selectedRow.push[row]; 
			 }
        sc.selectedCell = cell;
        sc.selectedColumn = column;
		
		if (sc.mySelections1 == row) { met=2;
		}
			  console.log(row);
			sc.mySelections1 = row;
			//console.log(sc.mySelections1);
		
			if (met == 2 && !row.NumDailyId && !row.DetailId) {
            if (column == 'Comment' && (curUser.roleid == '2' || curUser.roleid == '10')) commentClick(row.Comment);
			}
        if (column == 'MainComment' && (curUser.roleid == '2' || curUser.roleid == '10')) mainCommentClick(cell);
		
    };

    //добавление комментария (только у роли Бачурина (2))
    function commentClick(comment) {
        console.info('comment');
        if (curUser.roleid == '2' || curUser.roleid == '10') {
            modals.showWindow(modalType.WIN_COMMENT, {
                nameField: "Комментарий",
                comment: comment
            })
                .result.then(function (result) {
                    console.info(result);
                    sc.selectedRow[0]['Comment'] = result;
               //     sendTask();
                }, function () {
                    console.info('Modal dismissed at: ' + new Date());
                });
        }
    }
    //добавление важного комментария (только у роли Бачурина (2))
    sc.mainCommentClick = function () {
            modals.showWindow(modalType.WIN_COMMENT, {
                nameField: "Важно",
                title:"Важно",
                comment: sc.Comment
            })
                .result.then(function (result) {
                    console.info(result);
                    sc.Comment = result;
			      requests.serverCall.post($http, "SparePartsCtrl/UpdateCommentRepTask", {
                     DateFilter: moment(sc.dateShift).format("MM.DD.YYYY"),
                     Comment : result,
					 OrgId : curUser.orgid,
                     }, function (data, status, headers, config) {
                     });
                }, function () {
                    console.info('Modal dismissed at: ' + new Date());
                });

    };

    
sc.approveTask = function () {
	  /*
        sc.dataTasks = {};
        sc.chackTask = {};
		console.log(sc.formData.OrgId);
        sc.dataTasks.Date = moment(sc.dateShift).format("MM.DD.YYYY");
        if (sc.activeTab == '1') {sc.dataTasks.ShiftInfo = sc.tableDataFirst;
            sc.chackTask = sc.tableDataFirst.EquipmentsInfo;
        }
        else if (sc.activeTab == '2') {sc.dataTasks.ShiftInfo = sc.tableDataSecond;
                sc.chackTask = sc.tableDataFirst.EquipmentsInfo;
        }
        
        var chack = 0;
        console.log(sc.dataTasks.ShiftInfo);
        for (var i = 0; i < sc.dataTasks.ShiftInfo.length; i++){
            if(sc.dataTasks.ShiftInfo[i].EquipmentsInfo.TracktorInfo.RepairStatus == "color: red;")
            {
                chack = 1;
            }

        }
          if (chack == '1')    {
            
            modals.showWindow(modalType.WIN_DIALOG, {
                title: "Утверждение сменного задания",
                message: "В сменном задании присутствует техника находящиеся на ремонте. Желаете утвердить задания?",
                callback:   function () {
                                preloader.show();
                                requests.serverCall.post($http, "DayTasksCtrl/ApproveDayShiftTask", {
                                Date: moment(sc.dateShift).format("MM.DD.YYYY"),
                                Shift: sc.activeTab,
								FilterOrg: sc.formData.OrgId,
                            }, function (data, status, headers, config) {
                                    console.info(data);
                                    preloader.hide();
                                     modals.showWindow(modalType.WIN_MESSAGE, {
                                     title: "Утверждение сменного задания",
                                     message: "Сменное задание утверждено",
                                     callback:   function () {
                                                                getDayTasks();
                                                             }
                            });
                    });

                }
            });
        }
        else {
            preloader.show();
            requests.serverCall.post($http, "DayTasksCtrl/ApproveDayShiftTask", {
            Date: moment(sc.dateShift).format("MM.DD.YYYY"),
            Shift: sc.activeTab,
				FilterOrg: sc.formData.OrgId,
            }, function (data, status, headers, config) {
                console.info(data);
                preloader.hide();
                modals.showWindow(modalType.WIN_MESSAGE, {
                title: "Утверждение сменного задания",
                message: "Сменное задание утверждено",
                callback: 	function () {
								getDayTasks();
							}
            });
        });
    }

	*/
    };


}
]);