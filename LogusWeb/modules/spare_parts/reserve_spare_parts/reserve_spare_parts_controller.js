
var app = angular.module("ReserveSpareParts", ["ngGrid", "services", 'ngCookies', "modalWindows"]);
app.controller("ReserveSparePartsCtrl", ["$scope", "$http", "requests", "$location",'$stateParams',"modalType","modals", function ($scope, $http, requests, $location, $stateParams, modalType, modals) {
    var sc = $scope;
    sc.selectedRow = [];
	 var rowsForDel = [];
	 var rowsForUpd = [];
	 sc.multi = false;
	 sc.infoData = {};
	 var nextNumber;
	    // пейджинг
    sc.totalServerItems = 0;
    sc.pagingOptions = {
        pageSizes: [10, 15, 20, 50, 100],
        pageSize: 10,
        currentPage: 1,
        totalServerItems: sc.totalServerItems
    };
	   // смотрим за изменениям в пейджинге
    sc.$watch('pagingOptions', function (newVal, oldVal) {
        if (newVal.currentPage != null && newVal !== oldVal && !isNaN(newVal.currentPage) && !isNaN(oldVal.currentPage)) {
            if (newVal.pageSize !== oldVal.pageSize) {
                sc.pagingOptions.currentPage = 1;
            loadService();
            }
            if (newVal.currentPage !== oldVal.currentPage) 
			{ 
			loadService();
			
			;}
        }
    }, true);
	
	
    sc.gridOptions = {
        enableColumnResize: true,
        enableRowSelection: true,
        showFooter: true,
        rowHeight: 49,
        headerRowHeight: 49,
        useExternalSorting: false,
        data: 'TableData',
		enablePaging: true,
        pagingOptions: sc.pagingOptions,
        selectedItems: sc.selectedRow,
		i18n: 'ru',
        afterSelectionChange: function (row) {
       	for (var i=0; i<row.length;i++ ) 
   { 
     sc.selectedRow.push(row[i].entity); 
     console.log(sc.selectedRow); 
        }
 console.log(row);		
	  if (row.entity) {
		  console.log(row.selected);
                if (row.selected) {
                    if (sc.multi) sc.selectedRow.push(row.entity);
                    else {
                        sc.selectedRow = [];
                        sc.selectedRow.push(row.entity);
                    }
                } else {
                    sc.selectedRow.splice(sc.TableData.indexOf(row.entity), 1);
                }
            }
	   
        },
        beforeSelectionChange: function (row, event) {
        	   if (!sc.multi) {
                angular.forEach(sc.TableData, function (data, index) {
                    if (data.selected == true) sc.mySelections.splice(index, 1);
                    sc.gridOptions.selectRow(index, false);

                });
            }
		return true;
        }
    };

    sc.gridOptions.columnDefs = [
        {
            field: "Date",
            displayName: "Дата НР",
            width: "10%",
            cellTemplate:'<a tooltip="{{row.getProperty(col.field)}}" tooltip-popup-delay="1000" tooltip-class="my-custom-tooltip" tooltip-placement="top" tooltip-append-to-body="true">' +
            '<datepicktable class="logus-headers col-md-2 col-xs-4 col-sm-2 col-lg-2" style="padding-top: 6px;margin-left: -22px;font-size: 15px;" data="row.entity[col.field]" mask="\'99.99.9999\'" regex-name="\'date\'" date-change="dateChange(row.entity, curDate)" ng-class="{clicknone: row.entity[\'Id\']}">' +
            '</datepicktable>' +
            '</a>'
         },
	      {field: "Act_number", displayName: "Номер НР", width: "10%",
           cellTemplate: '<a tooltip="{{row.entity[col.field][\'Name\']}}" tooltip-popup-delay="1000" tooltip-class="my-custom-tooltip" tooltip-placement="top" tooltip-append-to-body="true">' +
            '<div class="ngCellText" ng-class="col.colIndex()">' +
            '<input value="{{row.entity[col.field]}}" ng-model="row.entity[col.field]" placeholder="0" ng-class="{bordernone: row.entity[\'Id\']}" ng-readonly="row.entity[\'Id\']" ng-change="changeCountPrice(row.entity)">' +
            '</div>' + '</a>',  headerCellTemplate: "<div   ng-class=\"'colt' + col.index\" class=\"ngHeaderText\">{{col.displayName}}</div>"
            },
	        {field: "SpareParts", displayName: "Наименование", width: "15%", cellTemplate: '<a tooltip="{{row.entity[col.field][\'Name\']}}" tooltip-popup-delay="1000" tooltip-class="my-custom-tooltip" tooltip-placement="top" tooltip-append-to-body="true">' +
            '<div class="ngCellText" ng-class="col.colIndex()">' +
            '<input value="{{row.entity[col.field][\'Name\']}}" placeholder="Наименование" style="border: none;"  data-ng-click="updateSelectedCell(col.field, row.entity[col.field][\'Id\'], \'Материал\',row.entity,$event)" readonly>' +
            '</div>' +
            '</a>', headerCellTemplate: "<div   ng-class=\"'colt' + col.index\" class=\"ngHeaderText\">{{col.displayName}}</div>"},
			
			{field: "VendorCode", displayName: "Артикул", width: "15%", cellTemplate: '<a tooltip="{{row.entity[col.field][\'Name\']}}" tooltip-popup-delay="1000" tooltip-class="my-custom-tooltip" tooltip-placement="top" tooltip-append-to-body="true">' +
            '<div class="ngCellText" ng-class="col.colIndex()">' +
            '<input value="{{row.entity[col.field][\'Name\']}}" placeholder="Артикул" style="border: none;"  data-ng-click="updateSelectedCell(col.field, row.entity[col.field][\'Id\'], \'Материал\',row.entity,$event)" readonly>' +
            '</div>' +
            '</a>', headerCellTemplate: "<div   ng-class=\"'colt' + col.index\" class=\"ngHeaderText\">{{col.displayName}}</div>"},
			
		    {field: "Count", displayName: "Количество", width: "10%",
            cellTemplate: '<a tooltip="{{row.entity[col.field][\'Name\']}}" tooltip-popup-delay="1000" tooltip-class="my-custom-tooltip" tooltip-placement="top" tooltip-append-to-body="true">' +
            '<div class="ngCellText" ng-class="col.colIndex()">' +
            '<input value="{{row.entity[col.field]}}" ng-model="row.entity[col.field]" placeholder="0" ng-class="{bordernone: row.entity[\'Id\']}" ng-readonly="row.entity[\'Id\']" ng-change="changeCountPrice(row.entity)">' +
            '</div>' + '</a>',  headerCellTemplate: "<div   ng-class=\"'colt' + col.index\" class=\"ngHeaderText\">{{col.displayName}}</div>"
            },
			
			{field: "CarsName", displayName: "TC/Оборудование", width: "30%", 	 cellTemplate: '<a tooltip="{{row.entity[col.field][\'Name\']}}" tooltip-popup-delay="1000" tooltip-class="my-custom-tooltip" tooltip-placement="top" tooltip-append-to-body="true">' +
             '<div class="ngCellText" ng-class="col.colIndex()">' +
             '<input value="{{row.entity[col.field]}}" ng-model="row.entity[col.field]" placeholder="TC/Оборудование" ng-class="{bordernone: row.entity[\'Id\']}" ng-readonly="row.entity[\'Id\']"  ng-change="changeCountPrice(row.entity)">' +
             '</div>' +'</a>',  headerCellTemplate: "<div   ng-class=\"'colt' + col.index\" class=\"ngHeaderText\">{{col.displayName}}</div>"},
			
            {field: "CarsNum", displayName: "Гос. №", width: "10%",
			 cellTemplate: '<a tooltip="{{row.entity[col.field][\'Name\']}}" tooltip-popup-delay="1000" tooltip-class="my-custom-tooltip" tooltip-placement="top" tooltip-append-to-body="true">' +
             '<div class="ngCellText" ng-class="col.colIndex()">' +
             '<input value="{{row.entity[col.field]}}" ng-model="row.entity[col.field]" placeholder="Гос. №" ng-class="{bordernone: row.entity[\'Id\']}" ng-readonly="row.entity[\'Id\']"  ng-change="changeCountPrice(row.entity)">' +
             '</div>' +'</a>',  headerCellTemplate: "<div   ng-class=\"'colt' + col.index\" class=\"ngHeaderText\">{{col.displayName}}</div>"},
				
        {field: "Id", width: "0%"},
    ];
 // получение масива в выпадающие списки
    requests.serverCall.post($http, "SparePartsCtrl/GetDictionarySpareParts", true, function (data, status, headers, config) {
        console.info(data);
        sc.infoData = data;
        console.info(sc.formData);
        sc.infoData.StorageList.push({Id: 0, Name:'Все'});
    });

    function loadService() {
        requests.serverCall.post($http, "SparePartsCtrl/GetReserveSpareParts", {
	 PageSize: sc.pagingOptions.pageSize,
     PageNum: sc.pagingOptions.currentPage,
	 MatTypeId : sc.infoData.StorageId,
        }, function (data, status, headers, config) {
			rowsForUpd = [];
		 sc.totalServerItems = data.CountItems;
            sc.pagingOptions.totalServerItems = data.CountItems;
            sc.TableData = data.Values;
        }, function () {
        });
    };
   loadService();
  sc.refreshTable = function(type) {
	  loadService();
       console.log(sc.infoData.StorageId);
  }
 

 sc.openBalance = function() {
       $location.path('/spare_parts'); 
 }



	function sec1() { 
 $("div.ngFooterSelectedItems").remove();
}
setInterval(sec1, 200) 

	
	
	
}]);