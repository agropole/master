
var app = angular.module("MovementSpareParts", ["ngGrid", "services", 'ngCookies', "modalWindows"]);
app.controller("MovementSparePartsCtrl", ["$scope", "$http", "requests", "$location",'$stateParams',"modalType","modals", function ($scope, $http, requests, $location, $stateParams, modalType, modals) {
    var sc = $scope;
    sc.selectedRow = [];
	sc.mySelections1 = [];
	 var rowsForDel = [];
	 var rowsForUpd = [];
	  sc.infoData = {};
	 	var met=1;
	 sc.multi = false;
	 var nextNumber;
	    // пейджинг
    sc.totalServerItems = 0;
    sc.pagingOptions = {
        pageSizes: [10, 15, 20, 50, 100],
        pageSize: 10,
        currentPage: 1,
        totalServerItems: sc.totalServerItems
    };
	   // смотрим за изменениям в пейджинге
    sc.$watch('pagingOptions', function (newVal, oldVal) {
        if (newVal.currentPage != null && newVal !== oldVal && !isNaN(newVal.currentPage) && !isNaN(oldVal.currentPage)) {
            if (newVal.pageSize !== oldVal.pageSize) {
                sc.pagingOptions.currentPage = 1;
            loadService();
            }
            if (newVal.currentPage !== oldVal.currentPage) 
			{ 
			loadService();
			
			;}
        }
    }, true);
	
	
    sc.gridOptions = {
        enableColumnResize: true,
        enableRowSelection: true,
        showFooter: true,
        rowHeight: 49,
        headerRowHeight: 49,
        useExternalSorting: false,
        data: 'TableData',
		enablePaging: true,
        pagingOptions: sc.pagingOptions,
        selectedItems: sc.selectedRow,
		i18n: 'ru',
    afterSelectionChange: function (row) {	
	met = 1;
	for (var i=0; i<row.length;i++ ) 
   { 
     sc.selectedRow.push(row[i].entity); 
    // console.log(sc.selectedRow); 
        } 
		if (sc.mySelections1 == row.entity) { console.log('есть');met=2;}
			sc.mySelections1 = row.entity;
	  if (row.entity) {
		  console.log(row.selected);
                if (row.selected) {
                    if (sc.multi) sc.selectedRow.push(row.entity);
                    else {
                        sc.selectedRow = [];
                        sc.selectedRow.push(row.entity);
                    }
                } else {
                    sc.selectedRow.splice(sc.TableData.indexOf(row.entity), 1);
                }
            }
	 console.log(sc.selectedRow); 
    },   
    beforeSelectionChange: function (row, event) {
		   if (!sc.multi) {
                angular.forEach(sc.TableData, function (data, index) {
                    if (data.selected == true) sc.selectedRow.splice(index, 1);
                    sc.gridOptions.selectRow(index, false);
                });
            }
                return true;
            }
    };

    sc.gridOptions.columnDefs = [
        {
            field: "Date",
            displayName: "Дата",
            width: "8%",
            cellTemplate:'<a tooltip="{{row.getProperty(col.field)}}" tooltip-popup-delay="1000" tooltip-class="my-custom-tooltip" tooltip-placement="top" tooltip-append-to-body="true">' +
            '<datepicktable class="logus-headers col-md-2 col-xs-4 col-sm-2 col-lg-2" style="padding-top: 6px;margin-left: -22px;font-size: 15px;" data="row.entity[col.field]" mask="\'99.99.9999\'" regex-name="\'date\'" date-change="dateChange(row.entity, curDate)" ng-class="{clicknone: row.entity[\'Id1\']}">' +
            '</datepicktable>' +
            '</a>'
         },
	        {field: "Act_number", displayName: "№ акта", width: "8%",
            cellTemplate: '<a tooltip="{{row.entity[col.field][\'Name\']}}" tooltip-popup-delay="1000" tooltip-class="my-custom-tooltip" tooltip-placement="top" tooltip-append-to-body="true">' +
            '<div class="ngCellText" ng-class="col.colIndex()">' +
            '<input value="{{row.entity[col.field]}}" ng-model="row.entity[col.field]" placeholder="" ng-class="{bordernone: row.entity[\'Id\']}" ng-readonly="row.entity[\'Id1\']" ng-change="changeCountPrice(row.entity)">' +
            '</div>' + '</a>',  headerCellTemplate: "<div   ng-class=\"'colt' + col.index\" class=\"ngHeaderText\">{{col.displayName}}</div>"
            },
	        {field: "SpareParts", displayName: "Наименование", width: "14%", cellTemplate: '<a tooltip="{{row.entity[col.field][\'Name\']}}" tooltip-popup-delay="1000" tooltip-class="my-custom-tooltip" tooltip-placement="top" tooltip-append-to-body="true">' +
            '<div class="ngCellText" ng-class="col.colIndex()">' +
            '<input value="{{row.entity[col.field][\'Name\']}}" placeholder="Наименование" style="border: none;"  data-ng-click="updateSelectedCell(col.field, row.entity[col.field][\'Id\'], \'Наименование\',row.entity,$event)" readonly>' +
            '</div>' +
            '</a>', headerCellTemplate: "<div   ng-class=\"'colt' + col.index\" class=\"ngHeaderText\">{{col.displayName}}</div>"},
			
			{field: "VendorCode", displayName: "Артикул", width: "8%", cellTemplate: '<a tooltip="{{row.entity[col.field][\'Name\']}}" tooltip-popup-delay="1000" tooltip-class="my-custom-tooltip" tooltip-placement="top" tooltip-append-to-body="true">' +
            '<div class="ngCellText" ng-class="col.colIndex()">' +
            '<input value="{{row.entity[col.field][\'Name\']}}" placeholder="Артикул" style="border: none;"  data-ng-click="updateSelectedCell(col.field, row.entity[col.field][\'Id\'], \'Артикул\',row.entity,$event)" readonly>' +
            '</div>' +
            '</a>', headerCellTemplate: "<div   ng-class=\"'colt' + col.index\" class=\"ngHeaderText\">{{col.displayName}}</div>"},
			
			{field: "StorageWriteOff", displayName: "Склад списания", width: "15%", cellTemplate: '<a tooltip="{{row.entity[col.field][\'Name\']}}" tooltip-popup-delay="1000" tooltip-class="my-custom-tooltip" tooltip-placement="top" tooltip-append-to-body="true">' +
            '<div class="ngCellText" ng-class="col.colIndex()">' +
            '<input value="{{row.entity[col.field][\'Name\']}}" placeholder="Склад" style="border: none;"  data-ng-click="updateSelectedCell(col.field, row.entity[col.field][\'Id\'], \'Склад списания\',row.entity,$event)"  readonly>' +
            '</div>' +
            '</a>', headerCellTemplate: "<div   ng-class=\"'colt' + col.index\" class=\"ngHeaderText\">{{col.displayName}}</div>"},	
			
			  {field: "Price", displayName: "Цена", width: "9%",
             cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()">' +
             '<input value="{{row.entity[col.field]}}" placeholder="0" style="border: none;" readonly>' +
             '</div>',  headerCellTemplate: "<div   ng-class=\"'colt' + col.index\" class=\"ngHeaderText\">{{col.displayName}}</div>"},
			
		   {field: "Count", displayName: "Количество", width: "10%",
            cellTemplate: '<a tooltip="{{row.entity[col.field][\'Name\']}}" tooltip-popup-delay="1000" tooltip-class="my-custom-tooltip" tooltip-placement="top" tooltip-append-to-body="true">' +
            '<div class="ngCellText" ng-class="col.colIndex()">' +
            '<input value="{{row.entity[col.field]}}" ng-model="row.entity[col.field]" placeholder="" ng-class="{bordernone: row.entity[\'Id\']}" ng-readonly="row.entity[\'Id1\']" ng-change="changeCountPrice(row.entity)">' +
            '</div>' + '</a>',  headerCellTemplate: "<div   ng-class=\"'colt' + col.index\" class=\"ngHeaderText\">{{col.displayName}}</div>"
            },
         
             {field: "Cost", displayName: "Сумма", width: "9%",
             cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()">' +
             '<input value="{{row.entity[col.field]}}" placeholder="0" style="border: none;" readonly>' +
             '</div>',  headerCellTemplate: "<div   ng-class=\"'colt' + col.index\" class=\"ngHeaderText\">{{col.displayName}}</div>"},
			 		
			{field: "StorageComing", displayName: "Склад оприходования", width: "28%", cellTemplate: '<a tooltip="{{row.entity[col.field][\'Name\']}}" tooltip-popup-delay="1000" tooltip-class="my-custom-tooltip" tooltip-placement="top" tooltip-append-to-body="true">' +
            '<div class="ngCellText" ng-class="col.colIndex()">' +
            '<input value="{{row.entity[col.field][\'Name\']}}" placeholder="Склад" style="border: none;"  data-ng-click="updateSelectedCell(col.field, row.entity[col.field][\'Id\'], \'Склад оприходования\',row.entity,$event)"  readonly>' +
            '</div>' +
            '</a>', headerCellTemplate: "<div   ng-class=\"'colt' + col.index\" class=\"ngHeaderText\">{{col.displayName}}</div>"},	 
        {field: "Id", width: "0%"},
		 {field: "CheckCount", width: "0%"},
    ];


       // получение масива в выпадающие списки
    requests.serverCall.post($http, "SparePartsCtrl/GetDictionarySparePartsBalance", true, function (data, status, headers, config) {
        console.info(data);
        sc.infoData = data;
        console.info(sc.formData);
        });



    function loadService() {
        requests.serverCall.post($http, "SparePartsCtrl/GetMovementSpareParts", {
	 PageSize: sc.pagingOptions.pageSize,
     PageNum: sc.pagingOptions.currentPage,
        }, function (data, status, headers, config) {
			rowsForUpd = [];
		 sc.totalServerItems = data.CountItems;
            sc.pagingOptions.totalServerItems = data.CountItems;
            sc.TableData = data.Values;
        }, function () {
        });
    };
  loadService();
  
   sc.dateChange = function (row) {
	   
	   if (row.Id!=undefined && row.Id!=null && row.Id!=0 && sc.selectedRow[0]!=null) {
		     	    console.log(sc.selectedRow[0]);
				   rowsForUpd.push({
                   Document: sc.selectedRow[0],
                });	
		         }	
	
   }
  
      // мультивыделение
    sc.keyDownGrid = function (e) {
            if ((e.keyCode === 16 || e.keyCode === 17 || e.keyCode === 18 || e.keyCode === 91) && !sc.multi) {
            //   console.info("__________________ keyDown  " + e.keyCode);
                sc.multi = true;
             //   console.info(sc.multi);
            }
        
    };

    sc.keyUpGrid = function (e) {
        // 16 - Shift, 17 - Ctrl, 18 - Alt, 91 - Win key (Start)
            if (e.keyCode === 16 || e.keyCode === 17 || e.keyCode === 18 || e.keyCode === 91) {
           //     console.info("_________________ keyUp " + e.keyCode);
                sc.multi = false;
              //  console.info(sc.multi);
            }
        
    };
  
   sc.openBalance = function() {
	   $location.path('/spare_parts'); 
 }

  
 // редактирование ячек с модальными окнами
    sc.updateSelectedCell = function (colField, selectedVal, displayName, row) {
		met=1;
            sc.colField = colField;
          //  sc.selectedRow = [];
         //   sc.selectedRow.push(row);
			if (sc.mySelections1==row) {met=2;}
			  console.log(row);
			sc.mySelections1 = row;
			console.log(sc.mySelections1);
            var modalValues;
            if (sc.infoData) {
				if (met==2) {
             console.log(sc.infoData);
                switch (sc.colField) {
                   case "SpareParts":
                    modalValues = sc.infoData.SparePartsList;
                    break;
			 case "VendorCode":
             modalValues = sc.infoData.VendorCodeList;
			 break;
			 case "StorageComing":		 
			var list =[];
			for (var i=0;i<sc.infoData.FullStorageList.length;i++)
			{
			if (sc.infoData.FullStorageList[i].Id != row.StorageWriteOff.Id)
			{
				list.push(sc.infoData.FullStorageList[i]);
			}					
			}
			
			
           //  modalValues = sc.infoData.StorageList;
		   modalValues = list;
			 break;
			 case "StorageWriteOff":
             modalValues = sc.infoData.StorageList;
			 break;
                }
                modals.showWindow(modalType.WIN_SELECT, {
                    nameField: displayName,
                    values: modalValues,
                    selectedValue: selectedVal
                })
                    .result.then(function (result) {
                        row[sc.colField] = result;
						  switch (colField) {  
						case "SpareParts":
						row.VendorCode = {Id: result.Id, Name :result.Description};  
                    requests.serverCall.post($http, "SparePartsCtrl/GetListStorageSpareParts",{
                        Spare_id:  row.SpareParts.Id,
                 }, function (data, status, headers, config) {
                       console.info(data);
                    sc.infoData.StorageList = data;
                    }, function () {
                      });						
						 break;
	                    case "VendorCode":
                         row.SpareParts = {Id: result.Id, Name :result.Description};
						 break;
						 //подтягиваем цену с баланса
						 case "StorageWriteOff":
                        requests.serverCall.post($http, "SparePartsCtrl/GetPriceCountBalance",{
                        Spare_id:  row.SpareParts.Id,
			            Store_id: row.StorageWriteOff.Id,
                 }, function (data, status, headers, config) {
                       console.info(data);
                      row.Price = parseFloat(data[0].Price).toFixed(2);
					  row.CheckCount = data[0].Count;
                    }, function () {
                      });
						 break;
						 
						}
						sc.changeCountPrice(row);	
						
                    }, function () {});
				}
            } else {}
       
    };

    sc.add = function() {	
		  var c = [];
		   //  console.log(con);
	for (i = 0; i < sc.TableData.length; i++) {
		c.push(sc.TableData[i].Act_number);
      }
	  con=getMaxOfArray(c);
	  console.log(con);
	  if (con == -Infinity) {con=0;}
        con = parseInt(con)+1;
		
        var newRow = {
            Date: new Date(),
            SpareParts: {Id: null, Name: null},
            VendorCode: {Id: null, Name: null},
			StorageWriteOff: {Id: null, Name: null},
			StorageComing: {Id: null, Name: null},
            Price: 0,
            Cost: 0,
			Count : 0,
			Act_number : con
        };
        sc.TableData.splice(0,0,newRow);
				console.log(newRow);
    };
  //сохранить
    sc.save = function() {
        var rowsForSave = getNewRows();
        if (rowsForSave.complete) {
            saveRows(rowsForSave.rows, loadService);
        } else {
            saveError();
        }
    };
	
	function getMaxOfArray(numArray) {
   return Math.max.apply(null, numArray);
   }
	

	
	//////////////////скопировать
	  sc.copyRow = function() {
		  var con = getMaxOfArray()
		  var c = [];
		     console.log(con);
	for (i = 0; i < sc.TableData.length; i++) {
		c.push(sc.TableData[i].Act_number);
      }
	  con=getMaxOfArray(c);
        con = parseInt(con)+1;
			var newStr = {
            Date: sc.selectedRow[0].Date,
			SpareParts : {Id:sc.selectedRow[0].SpareParts.Id, Name: sc.selectedRow[0].SpareParts.Name},
			VendorCode : {Id:sc.selectedRow[0].VendorCode.Id, Name: sc.selectedRow[0].VendorCode.Name},
			StorageComing : {Id:sc.selectedRow[0].StorageComing.Id, Name: sc.selectedRow[0].StorageComing.Name},
			StorageWriteOff : {Id:sc.selectedRow[0].StorageWriteOff.Id, Name: sc.selectedRow[0].StorageWriteOff.Name},
            Count: sc.selectedRow[0].Count,
		    Price: sc.selectedRow[0].Price,
		    Cost: sc.selectedRow[0].Cost,
            Act_number : con,	
        };
        sc.TableData.splice(0, 0, newStr);     
	  }
	

	    // модальное окно с требованием выделить строку
    function needSelection(title, message) {
        modals.showWindow(modalType.WIN_MESSAGE, {
            title: title,
            message: message,
            callback: function () {
            }
        });
    }
	//удаление строки
	   sc.delRow = function() {
		    console.log(sc.selectedRow);
		    if (sc.selectedRow.length!=0) {
            modals.showWindow(modalType.WIN_DIALOG, {
                    title: "Удаление", message: "Удалить выбранный документ?", callback: function () {
						for (var i=0; i<sc.selectedRow.length;i++) {
						if (sc.selectedRow[i].Id==undefined) {
					    sc.TableData.splice(sc.TableData.indexOf(sc.selectedRow[i]), 1);
							}
						else {
				 rowsForDel.push({
                   rows:sc.selectedRow[i]
                });		
						}
						}
							console.log(rowsForDel);
							
                        requests.serverCall.post($http, "SparePartsCtrl/DeleteMovementSpareParts", rowsForDel, function (data, status, headers, config) {
							rowsForDel = [];
                       // sc.TableData.splice(sc.TableData.indexOf(sc.selectedRow[0]), 1);
					   loadService();
                            console.info(data);
                        });
						;}
                    }
                
            );
        } else needSelection('Удаление документа', "Для удаления необходимо выбрать документ");
	   }
	
    var saveError = function() {
        modals.showWindow(modalType.WIN_ERROR, {
            title: "Ошибка",
            message: "Заполните новые поля или удалите незаполненные строки",
            callback: function () {
            }
        });
    };
 ////

 //СЧИТАЕМ ПРОИЗВЕДЕНИЕ
     sc.changeCountPrice = function(row) {
		 console.log(row.Id);
			 if (row.Id!=undefined && row.Id!=null && row.Id!=0 && sc.selectedRow[0]!=null) {
				   rowsForUpd.push({
                   Document: sc.selectedRow[0],
                });	
		         }	 
	row.Act_number = row.Act_number.toString().replace(/[^0-9]/gim,'');	 
    row.Count = row.Count.toString().replace(/[^0-9]/gim,'');
//	row.Price = row.Price.toString().replace(/[^0-9]/gim,'');
	row.Cost = row.Cost.toString().replace(/[^0-9]/gim,'');
     row.Count = row.Count.toString().replace(",", "."); 
  //   row.Price = row.Price.toString().replace(",", "."); 
            if (row.Price === null) {
                row.Price = 0;
            }
            if (row.Count === null) {
                row.Count = 0;
            }
       row.Cost = (parseFloat(row.Price) * parseFloat(row.Count)).toFixed(2);
        };
		
    var saveRows = function (rowsForSave, cb) {
		var check = 0;
		console.log(rowsForSave);
		for (var i=0;i<rowsForSave.length;i++)
		{
			if (rowsForSave[i].Count>rowsForSave[i].CheckCount) {check = 1; rowsForSave=[];
			
			needSelection('Предупреждение', "Недостаточное количество запчастей на складе");
			break;
			}
			
		}
        	if (check!=1) {
		    for (var i=0;i<rowsForUpd.length;i++)
		{
			if (rowsForUpd[i].Document.Count > rowsForUpd[i].Document.CheckCount) {check = 1; //rowsForUpd = [];
			
			needSelection('Предупреждение', "Количество превышает баланс");
			break;
			}
			
		}	
			}
		
		
		
		if (check!=1) {
		if (rowsForUpd.length!=0) {
		    requests.serverCall.post($http, "SparePartsCtrl/UpdateMovementSpareParts", rowsForUpd, function (data, status, headers, config) {
			console.log(data);
			if (data!=undefined) {if (cb) cb();}		
                       rowsForUpd = [];		   
		 }, function() {
        });
		}
		if (rowsForSave.length!=0) {
        requests.serverCall.post($http, "SparePartsCtrl/SaveMovementSpareParts", rowsForSave, function (data, status, headers, config) {
		if (data != undefined) {if (cb) cb();}
		rowsForSave = [];	rowsForUpd = [];	
        }, function() {
        });
		}
		}
    };

    var getNewRows = function() {
        if (sc.TableData) {
            var newRows = [];
            var complete = true;
            for (var i = 0, li = sc.TableData.length; i < li; i++) {
                if (!sc.TableData[i].Id) {
                    var r = sc.TableData[i];
                    newRows.push(sc.TableData[i]);
      if (r.Price==null || r.Count==null || r.SpareParts.Id==null || r.VendorCode.Id==null || r.Cost==null || r.StorageWriteOff.Id==null ||
	  r.StorageComing.Id==null || (r.Act_number==null || r.Act_number==NaN || r.Act_number=="") ) {
                        complete = false;
                    }
                }
            }
            return {
                rows: newRows,
                complete: complete
            };
        }
    };
	function sec1() { 
 $("div.ngFooterSelectedItems").remove();
}
setInterval(sec1, 200) 

	
	
	
}]);