
var app = angular.module("HistorySpareParts", ["ngGrid", "services", 'ngCookies', "modalWindows", "LogusConfig","ui.bootstrap",'Directives',"ui.utils"]);
app.controller("HistorySparePartsCtrl", ["$scope", "$http", "requests", "$location", '$cookieStore', '$rootScope', "modals", "modalType", '$stateParams', 'preloader', "LogusConfig", function ($scope, $http, requests, $location, $cookieStore, $rootScope, modals, modalType, $stateParams, preloader, LogusConfig) {
    var sc = $scope;
    var curUser = $cookieStore.get('currentUser');
    $rootScope.userName = curUser.username;
	 $cookieStore.put('dateDailyCalendar', {date: null});
	 $cookieStore.put('dateEndDailyCalendar', {date: null});
    $rootScope.exit = 'выход';
    sc.mySelections = [];
	sc.mySelections1 =[];
	sc.mySelections2 =[];
	sc.mySelections3 =[];
    sc.tableData = [];
	sc.tableData2 = [];
	sc.tableData3 = [];
    sc.formData = {};
    sc.infoData = {};
    sc.pageNumber = 'pageOne';

    // свойства таблицы1
    sc.gridOptions = {
    data: 'tableData',
    enableColumnResize: true,
    enableRowSelection: true,
    showFooter: false,
    selectedItems: sc.mySelections,
    rowHeight: 49,
    headerRowHeight: 49,
    
   columnDefs: [
   			{field: "SpareParts1", displayName: "Наименование запчасти", width: "25%", cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()">' +
             '<input value="{{row.entity[col.field]}}" placeholder="0" style="border: none;" readonly>' +
             '</div>',  headerCellTemplate: "<div   ng-class=\"'colt' + col.index\" class=\"ngHeaderText\">{{col.displayName}}</div>"},
   
   	        {field: "Act_number", displayName: "№ накл.", width: "10%",  cellTemplate: '<a tooltip="{{row.entity[col.field][\'Name\']}}" tooltip-popup-delay="1000" tooltip-class="my-custom-tooltip" tooltip-placement="top" tooltip-append-to-body="true">' +
             '<div class="ngCellText" ng-class="col.colIndex()">' +
             '<input value="{{row.entity[col.field]}}" ng-model="row.entity[col.field]" placeholder="№ накл." ng-class="{bordernone: row.entity[\'Id\']}" ng-readonly="row.entity[\'Id\']"  ng-change="changeCountPrice(row.entity)">' +
             '</div>' +'</a>', headerCellTemplate: "<div   ng-class=\"'colt' + col.index\" class=\"ngHeaderText\">{{col.displayName}}</div>"},
			
    	    {field: "DateBegin1", displayName: "Дата", width: "10%", cellTemplate: '<a tooltip="{{row.getProperty(col.field)}}" tooltip-popup-delay="1000" tooltip-class="my-custom-tooltip" tooltip-placement="top" tooltip-append-to-body="true">' +
            '<div class="ngCellText" ng-class="col.colIndex()">' +
            '<input data="{{row.entity[col.field]}}" ng-change="changeCountPrice(row.entity)" ng-model="row.entity[col.field]" ng-readonly="row.entity.Id" ng-class="{bordernone: row.entity[\'Id\']}">' +
            '</div>' +
            '</a>', headerCellTemplate: "<div   ng-class=\"'colt' + col.index\" class=\"ngHeaderText\">{{col.displayName}}</div>"},
			
			  {field: "Price", displayName: "Цена", width: "10%",
			 cellTemplate: '<a tooltip="{{row.entity[col.field][\'Name\']}}" tooltip-popup-delay="1000" tooltip-class="my-custom-tooltip" tooltip-placement="top" tooltip-append-to-body="true">' +
             '<div class="ngCellText" ng-class="col.colIndex()">' +
             '<input value="{{row.entity[col.field]}}" ng-model="row.entity[col.field]" placeholder="Цена" ng-class="{bordernone: row.entity[\'Id\']}" ng-readonly="row.entity[\'Id\']"  ng-change="changeCountPrice(row.entity)">' +
             '</div>' +'</a>',  headerCellTemplate: "<div   ng-class=\"'colt' + col.index\" class=\"ngHeaderText\">{{col.displayName}}</div>"},
			 
		    {field: "Count", displayName: "Количество", width: "15%",
            cellTemplate: '<a tooltip="{{row.entity[col.field][\'Name\']}}" tooltip-popup-delay="1000" tooltip-class="my-custom-tooltip" tooltip-placement="top" tooltip-append-to-body="true">' +
            '<div class="ngCellText" ng-class="col.colIndex()">' +
            '<input value="{{row.entity[col.field]}}" ng-model="row.entity[col.field]" placeholder="0" ng-class="{bordernone: row.entity[\'Id\']}" ng-readonly="row.entity[\'Id\']" ng-change="changeCountPrice(row.entity)">' +
            '</div>' + '</a>',  headerCellTemplate: "<div   ng-class=\"'colt' + col.index\" class=\"ngHeaderText\">{{col.displayName}}</div>"
            },

             {field: "Cost3", displayName: "Сумма", width: "10%",
             cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()">' +
             '<input value="{{row.entity[col.field]}}" placeholder="0" style="border: none;" readonly>' +
             '</div>',  headerCellTemplate: "<div   ng-class=\"'colt' + col.index\" class=\"ngHeaderText\">{{col.displayName}}</div>"},
				
			{field: "Storage1", displayName: "Склад", width: "20%",    cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()">' +
             '<input value="{{row.entity[col.field]}}" placeholder="0" style="border: none;" readonly>' +
             '</div>',  headerCellTemplate: "<div   ng-class=\"'colt' + col.index\" class=\"ngHeaderText\">{{col.displayName}}</div>"}, 
			 {field: "Id", width: "0%"},
			 {field: "ActId", width: "0%"},
    ],
    afterSelectionChange: function (row) {	
	met = 1;
	for (var i=0; i<row.length;i++ ) 
   { 
     sc.mySelections.push(row[i].entity); 
     //console.log(sc.mySelections); 
        } 
		if (sc.mySelections1==row.entity) { console.log('есть');met=2;}
			sc.mySelections1 = row.entity;
	  if (row.entity) {
		  //console.log(row.selected);
                if (row.selected) {
                    if (sc.multi) sc.mySelections.push(row.entity);
                    else {
                        sc.mySelections = [];
                        sc.mySelections.push(row.entity);
                    }
                } else {
                    sc.mySelections.splice(sc.tableData.indexOf(row.entity), 1);
                }
            }
	
    },   
    beforeSelectionChange: function (row, event) {
	console.log(row.entity.ActId);
	$cookieStore.put('typeoper', 'edit');	
	$cookieStore.put('act_id', row.entity.ActId);
	$cookieStore.put('checkHistoryLoc', 'invoice');	
	var popupWin = window.open('#/spare_parts/', '_blank');
		   if (!sc.multi) {
                angular.forEach(sc.tableData, function (data, index) {
                    if (data.selected == true) sc.mySelections.splice(index, 1);
                    sc.gridOptions.selectRow(index, false);
                });
            }
                return true;
            }
    };    
	
	  // свойства таблицы2
    sc.gridOptions2 = {
    data: 'tableData2',
    enableColumnResize: true,
    enableRowSelection: true,
    showFooter: false,
    selectedItems: sc.mySelections2,
    rowHeight: 49,
    headerRowHeight: 49,
    
   columnDefs: [
   			{field: "SpareParts1", displayName: "Наименование запчасти", width: "25%",   cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()">' +
             '<input value="{{row.entity[col.field]}}" placeholder="0" style="border: none;" readonly>' +
             '</div>',  headerCellTemplate: "<div   ng-class=\"'colt' + col.index\" class=\"ngHeaderText\">{{col.displayName}}</div>"},
   
   	        {field: "Act_number", displayName: "№", width: "10%",    cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()">' +
             '<input value="{{row.entity[col.field]}}" placeholder="0" style="border: none;" readonly>' +
             '</div>',  headerCellTemplate: "<div   ng-class=\"'colt' + col.index\" class=\"ngHeaderText\">{{col.displayName}}</div>"},
			
    	    {field: "Date1", displayName: "Дата", width: "10%", cellTemplate: '<a tooltip="{{row.getProperty(col.field)}}" tooltip-popup-delay="1000" tooltip-class="my-custom-tooltip" tooltip-placement="top" tooltip-append-to-body="true">' +
            '<div class="ngCellText" ng-class="col.colIndex()">' +
            '<input data="{{row.entity[col.field]}}" ng-change="changeCountPrice(row.entity)" ng-model="row.entity[col.field]" ng-readonly="row.entity.Id" ng-class="{bordernone: row.entity[\'Id\']}">' +
            '</div>' +
            '</a>', headerCellTemplate: "<div   ng-class=\"'colt' + col.index\" class=\"ngHeaderText\">{{col.displayName}}</div>"},
			
		    {field: "Count", displayName: "Количество", width: "10%",
            cellTemplate: '<a tooltip="{{row.entity[col.field][\'Name\']}}" tooltip-popup-delay="1000" tooltip-class="my-custom-tooltip" tooltip-placement="top" tooltip-append-to-body="true">' +
            '<div class="ngCellText" ng-class="col.colIndex()">' +
            '<input value="{{row.entity[col.field]}}" ng-model="row.entity[col.field]" placeholder="0" ng-class="{bordernone: row.entity[\'Id\']}" ng-readonly="row.entity[\'Id\']" ng-change="changeCountPrice(row.entity)">' +
            '</div>' + '</a>',  headerCellTemplate: "<div   ng-class=\"'colt' + col.index\" class=\"ngHeaderText\">{{col.displayName}}</div>"
            },
			 {field: "CarsName", displayName: "TC/Оборудование", width: "25%",
            cellTemplate: '<a tooltip="{{row.entity[col.field][\'Name\']}}" tooltip-popup-delay="1000" tooltip-class="my-custom-tooltip" tooltip-placement="top" tooltip-append-to-body="true">' +
            '<div class="ngCellText" ng-class="col.colIndex()">' +
            '<input value="{{row.entity[col.field]}}" ng-model="row.entity[col.field]" placeholder="TC/Оборудование" ng-class="{bordernone: row.entity[\'Id\']}" ng-readonly="row.entity[\'Id\']" ng-change="changeCountPrice(row.entity)">' +
            '</div>' + '</a>',  headerCellTemplate: "<div   ng-class=\"'colt' + col.index\" class=\"ngHeaderText\">{{col.displayName}}</div>"
            },
	        {field: "CarsNum", displayName: "Гос. №", width: "10%",
            cellTemplate: '<a tooltip="{{row.entity[col.field][\'Name\']}}" tooltip-popup-delay="1000" tooltip-class="my-custom-tooltip" tooltip-placement="top" tooltip-append-to-body="true">' +
            '<div class="ngCellText" ng-class="col.colIndex()">' +
            '<input value="{{row.entity[col.field]}}" ng-model="row.entity[col.field]" placeholder="Гос. №" ng-class="{bordernone: row.entity[\'Id\']}" ng-readonly="row.entity[\'Id\']" ng-change="changeCountPrice(row.entity)">' +
            '</div>' + '</a>',  headerCellTemplate: "<div   ng-class=\"'colt' + col.index\" class=\"ngHeaderText\">{{col.displayName}}</div>"
            },
			{field: "CheckWrite", displayName: "Списано", width: "10%",   cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()">' +
             '<input value="{{row.entity[col.field]}}" placeholder="0" style="border: none;" readonly>' +
             '</div>',  headerCellTemplate: "<div   ng-class=\"'colt' + col.index\" class=\"ngHeaderText\">{{col.displayName}}</div>"},
			 {field: "Id", width: "0%"},
			 {field: "ActId", width: "0%"},
    ],
    afterSelectionChange: function (row) {	
	met = 1;
	for (var i=0; i<row.length;i++ ) 
   { 
     sc.mySelections2.push(row[i].entity); 
     console.log(sc.mySelections2); 
        } 
		if (sc.mySelections1==row.entity) { console.log('есть');met=2;}
			sc.mySelections1 = row.entity;
	  if (row.entity) {
		  console.log(row.selected);
                if (row.selected) {
                    if (sc.multi) sc.mySelections2.push(row.entity);
                    else {
                        sc.mySelections2 = [];
                        sc.mySelections2.push(row.entity);
                    }
                } else {
                    sc.mySelections2.splice(sc.tableData2.indexOf(row.entity), 1);
                }
            }
	
    },   
    beforeSelectionChange: function (row, event) {
		$cookieStore.put('tOperation', 'edit');	
		$cookieStore.put('repair_id', row.entity.ActId);
		$cookieStore.put('checkHistoryLoc', 'repairs');	
	    var popupWin = window.open('#/spare_parts/', '_blank');	
		   if (!sc.multi) {
                angular.forEach(sc.tableData2, function (data, index) {
                    if (data.selected == true) sc.mySelections2.splice(index, 1);
                    sc.gridOptions2.selectRow(index, false);
                });
            }
                return true;
            }
    };  
     // таблица 3 
    sc.gridOptions3 = {
    data: 'tableData3',
    enableColumnResize: true,
    enableRowSelection: true,
    showFooter: false,
    selectedItems: sc.mySelections3,
    rowHeight: 49,
    headerRowHeight: 49,
    
   columnDefs: [
   			{field: "Name", displayName: "Наименование", width: "35%",   cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()">' +
             '<input value="{{row.entity[col.field]}}" placeholder="0" style="border: none;" readonly>' +
             '</div>',  headerCellTemplate: "<div   ng-class=\"'colt' + col.index\" class=\"ngHeaderText\">{{col.displayName}}</div>"},
   
   	          {field: "Description", displayName: "Артикул", width: "35%",   cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()">' +
             '<input value="{{row.entity[col.field]}}" placeholder="0" style="border: none;" readonly>' +
             '</div>',  headerCellTemplate: "<div   ng-class=\"'colt' + col.index\" class=\"ngHeaderText\">{{col.displayName}}</div>"},
			  {
            field: "", displayName: " ", width:"30%",
            cellTemplate: '<div class="{{getClassTempCell(row.entity.TemplateStatus,row)}} ngCellText {{getClassCopyCell(row.entity.Status,row)}}"><button type="button"  class="logus-btn btn btn-border ng-click-active" style="width: 100%;margin-top: 8px" ng-click="openHistory(row.entity.Id)">История Запчасти</button></div>',
        },
			 {field: "Id", width: "0%"},
    ],
    afterSelectionChange: function (row) {	
	for (var i=0; i<row.length;i++ ) 
   { 
     sc.mySelections3.push(row[i].entity); 
     console.log(sc.mySelections3); 
        } 
			sc.mySelections1 = row.entity;
	  if (row.entity) {
		  console.log(row.selected);
                if (row.selected) {
                    if (sc.multi) sc.mySelections3.push(row.entity);
                    else {
                        sc.mySelections3 = [];
                        sc.mySelections3.push(row.entity);
                    }
                } else {
                    sc.mySelections3.splice(sc.tableData3.indexOf(row.entity), 1);
                }
            }
	
    },   
    beforeSelectionChange: function (row, event) {
		   if (!sc.multi) {
                angular.forEach(sc.tableData3, function (data, index) {
                    if (data.selected == true) sc.mySelections3.splice(index, 1);
                    sc.gridOptions3.selectRow(index, false);
                });
            }
                return true;
            }
    }; 

    var _field, _val;
    // выделение строчки по определенному столбцу
    function selectRowByField(field, val) {
        _field = field;
        _val = val;
        sc.$on('ngGridEventData', sc.gridEvDataListener);
    }


    // листнер на событие изменение data в таблице
    sc.gridEvDataListener = function () {
        var offCallMeFn = $scope.$on("ngGridEventData", sc.gridEvDataListener);
        offCallMeFn();
        if (_val == "first" && sc.tableData.length > 0) _val = sc.tableData[0][_field];
		console.log(_val);
        sc.tableData.forEach(function (item, i, arr) {
            if (item[_field] == _val) {
                sc.gridOptions.selectRow(i, true);
                if (sc.gridOptions.ngGrid) sc.gridOptions.ngGrid.$viewport.scrollTop(sc.gridOptions.ngGrid.rowMap[i] * sc.gridOptions.ngGrid.config.rowHeight);
            } else {
                sc.gridOptions.selectRow(i, false);
            }
        });
        $(".ngViewport").focus();
    };
    sc.search = function () {
	requests.serverCall.post($http, "SparePartsCtrl/GetHistorySpareParts", sc.formData.SpareParts, function (data, status, headers, config) {
        console.info(data);
      //  sc.SparePartsList = data;
		sc.tableData3 = data;
	
		 });	
	}
	sc.openHistory = function (val) {
	sc.ActId = val;
	requests.serverCall.post($http, "SparePartsCtrl/GetHistorySpareList", val, function (data, status, headers, config) {
             console.info(data);
			sc.tableData = data.InvoiceList;
			sc.tableData2 = data.WriteOffList;
			sc.pageNumber = 'pageTwo';
        }, function () {
        });
		console.log(val);
	}
	sc.openHistoryReport = function () {
	var popupWin = window.open(LogusConfig.serverUrl + 'CostCtrl/GetHistoryResExcel?ActId='+sc.ActId, '_blank');	
	}
	
	
	
    //кнопка Отмена
    sc.cancelClick = function () {
      sc.pageNumber = 'pageOne';
    };
   

}
]);


 