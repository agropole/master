var app = angular.module("RepairsReportsCtrl", ["services", 'ngCookies', "modalWindows", "LogusConfig"]);
app.controller("RepairsReportsCtrl", ["$scope", "$http", "requests", "modals", "modalType",'$cookieStore', "LogusConfig", function ($scope, $http, requests, modals, modalType,$cookieStore, LogusConfig) {
    var sc = $scope;
    sc.formData = [];
    sc.infoData = {}; 
    sc.activeTab = '';
    sc.monthList = [{Id: 1, Name: "Январь"}, {Id: 2, Name: "Февраль"}, {Id: 3, Name: "Март"}, {
        Id: 4,
        Name: "Апрель"
    }, {Id: 5, Name: "Май"}, {Id: 6, Name: "Июнь"}, {Id: 7, Name: "Июль"}, {Id: 8, Name: "Август"}, {
        Id: 9,
        Name: "Сентябрь"
    }, {Id: 10, Name: "Октябрь"}, {Id: 11, Name: "Ноябрь"}, {Id: 12, Name: "Декабрь"}];
    sc.currentYear = new Date().getFullYear();
    sc.yearsList = [];
    $cookieStore.put('dateDailyCalendar', {date: null});
	$cookieStore.put('dateEndDailyCalendar', {date: null});
    sc.formData.yearId = sc.currentYear;
    sc.formData.monthId = new Date().getMonth() + 1;
    sc.formData.timeIsBig = false;
	sc.formData.reserve = false ;
	sc.formData.storage =  true ;
	$("#btn").show();
	var now = new Date();
	sc.formData.docStartDate = now;
	sc.formData.docEndDate = now;
	sc.formData.techStartDate = new Date(sc.formData.yearId, 0, 1, 0, 0, 0, 0);
      sc.formData.sheet = false;
	//наряд на сдельную работу
	if (now.getDate() <= 15) {
		sc.formData.pieceStartDate =  new Date(sc.formData.yearId, now.getMonth()-1, 15, 0, 0, 0, 0);
    	sc.formData.pieceEndDate = new Date(sc.formData.yearId, now.getMonth(), 0, 0, 0, 0, 0);
	} else {
     	sc.formData.pieceStartDate = new Date(sc.formData.yearId, now.getMonth(), 1, 0, 0, 0, 0);
		sc.formData.pieceEndDate = new Date(sc.formData.yearId, now.getMonth(), 15, 0, 0, 0, 0);
	}
	//
	
    for (var i = 0; i < 20; i++) {
        sc.yearsList.push({Id: (sc.currentYear - i), Name: '' + (sc.currentYear - i) + ''});
    }
    sc.formData.machineStartDate = sc.formData.driverStartDate = sc.formData.refuelStartDate = moment().startOf('month').utc().toString("MM.DD.YYYY");
	sc.formData.exportStartDate = moment().startOf('month').utc().toString("MM.DD.YYYY");
	

	sc.changeStorage = function () {
		if (sc.formData.reserve) {sc.formData.reserve = false;}
	}
	sc.changeReserve = function () {
		if (sc.formData.storage) {sc.formData.storage = false;}
	}
	
    requests.serverCall.post($http, "CostCtrl/GetRepairsReportsInfo", true, function (data, status, headers, config) {
        sc.infoData.Storages = data.Storages;

        sc.infoData.Storages.unshift({"Name":"Все","Id":0});
		sc.formData.StorageId = 0 ;
		sc.formData.intStorageId = 0;
   });
   
   
    getResp = function () {
			 	if (sc.formData.timeStartDate) {
     requests.serverCall.post($http, "CostCtrl/GetRespRepairsEmployees", { 
            start:  sc.formData.timeStartDate, 
			end: sc.formData.timeEndDate
        }, 
		function (data, status, headers, config) 
		{
			sc.infoData.responsibleList = data;
			sc.formData.timeEmpId = 0;
			}, function () {
        });
	}
	}
	   getPieceResp = function () {
		 console.log(sc.formData.pieceStartDate);
		if (sc.formData.pieceStartDate) {
        requests.serverCall.post($http, "CostCtrl/GetRespRepairsEmployees",{ 
            start:  sc.formData.pieceStartDate, 
			end: sc.formData.pieceEndDate
        }, 
		function (data, status, headers, config) 
		{
			sc.infoData.responsiblePieceList = data;
			}, function () {
        });
				}
	}
	getResp();
	getPieceResp();
	getRespTech = function (start, end, type ) {
     requests.serverCall.post($http, "CostCtrl/GetRespRepairsTech",{
            start:  start, 
			end: end
        }, 
		function (data, status, headers, config) 
		{
			console.log(data);
			if (type == 1) { 
			sc.infoData.rsTech = data.Tech;
			sc.infoData.rsEq = data.Eq;
			if (sc.infoData.rsTech.length == 1) {
				sc.formData.rsTechId = 0 ;
			} else {
				sc.formData.rsTechId = -1;	
			}
			if (sc.infoData.rsEq.length == 1) {
				sc.formData.rsEqId = 0 ;
			} else {
				sc.formData.rsEqId = -1;	
			}
			sc.techEqChange(type);
			} else {
			sc.infoData.repTech = data.Tech;
			sc.infoData.repEq = data.Eq;
			sc.formData.repTechId = -1;
	        sc.formData.repEqId = -1;
			}
			
		}, function () {
        });
	}
	//  getRespTech();
	sc.formData.rsTechId = 0;
	sc.formData.rsEqId = 0;
		
    sc.getSpStartDate = function (data) {
	   getRespTech(sc.formData.rsStartDate, sc.formData.rsEndDate,1);
    };
	sc.getSpEndDate = function (data) {
		getRespTech(sc.formData.rsStartDate, sc.formData.rsEndDate,1);
    };
            //свод затрат
     sc.getRepStartDate = function (data) {
	   getRespTech(sc.formData.techStartDate, sc.formData.techEndDate,2);
    };
	sc.getRepEndDate = function (data) {
		getRespTech(sc.formData.techStartDate, sc.formData.techEndDate,2);
    };
   
   sc.isSheet = function () {
	   if (sc.formData.sheet) {
		   sc.formData.timeIsBig = false;
		 $("#bigReport").addClass("disabledbutton");   
	   } else {
		  $("#bigReport").removeClass("disabledbutton");     
	   }
	   
   }
   
   
   sc.techEqChange = function (type) {
	   if (type == 1) {
   if (sc.formData.rsEqId == 0 && sc.formData.rsTechId == 0) {
	   $("#rs").addClass("disabledbutton");
   } else {
	   $("#rs").removeClass("disabledbutton");
   }   
	   } else {
		   //свод затрат
		   if (type == 2) {
   if (sc.formData.repEqId == 0 && sc.formData.repTechId == 0) {
	   $("#rep").addClass("disabledbutton");
   } else {
	   $("#rep").removeClass("disabledbutton");
   }     
	//нажатие техника
        if (sc.formData.repTechId != 0 && sc.formData.repTechId != -1) {
			sc.formData.repEqId = 0;
		}
	   }
	   }  
   }
   //свод - оборудование
   sc.techEqChange2 =  function () {
	      if (sc.formData.repEqId == 0 && sc.formData.repTechId == 0) {
	   $("#rep").addClass("disabledbutton");
   } else {
	   $("#rep").removeClass("disabledbutton");
   }  
	   	 if (sc.formData.repEqId != 0 && sc.formData.repEqId != -1) {
			sc.formData.repTechId = 0;
			console.log('111');
		}
	   
   }
   
   
   
   
   
   
    sc.getStartDate = function (data) {
	   getResp();
    };
	sc.getEndDate = function (data) {
		getResp();
    };
	
	sc.getPieceStartDate = function (data) {
	   getPieceResp();
    };
	sc.getPieceEndDate = function (data) {
		getPieceResp();
    };
	
	
	requests.serverCall.get($http, "TechCard/GetTechReportInfo", function (data, status, headers, config) {
        data.TypeTraktor.splice(0,0, {Id: 0, Name: 'Все'});
        data.TypeEqui.splice(0,0, {Id: 0, Name: 'Все'});
        sc.infoData.planEqTypeList = data.TypeEqui;
		sc.infoData.planTechTypeList = data.TypeTraktor;
		sc.formData.planTechTypeId =  0 ;
		sc.formData.planEqTypeId =  0 ;
    }, function() {});
   
   
    sc.setTab = function (tab) {
        sc.activeTab = tab;
    };

    sc.submitForm = function () {
        switch (sc.activeTab) {
			case "balance":
                var popupWin = window.open(LogusConfig.serverUrl + 'CostCtrl/GetSparePartsBalanceExcel?storage_id=' + sc.formData.StorageId +
				                                                                                       '&storage=' + sc.formData.storage +
				                                                                                       '&reserve=' + sc.formData.reserve, '_blank');																							 
                break;
			case "intake":
				var dateStart = angular.copy(sc.formData.intStartDate),
                    dateEnd = angular.copy(sc.formData.intEndDate);
                var popupWin = window.open(LogusConfig.serverUrl + 'CostCtrl/GetSparePartsIntakeExcel?storage_id=' + sc.formData.intStorageId + 
																									'&start=' + moment(dateStart).format("MM.DD.YYYY") + 
																									'&end=' + moment(dateEnd).format("MM.DD.YYYY"), '_blank');																							 
                break;
			case "time":
		     var number = 1;
		    if (sc.formData.sheet) {
			  number = 3;
		     }
				var dateStart = angular.copy(sc.formData.timeStartDate),
                    dateEnd = angular.copy(sc.formData.timeEndDate);
                var popupWin = window.open(LogusConfig.serverUrl + 'CostCtrl/GetRepairsSpendingsExcel?emp_id=' + sc.formData.timeEmpId + 
																									'&start=' + moment(dateStart).format("MM.DD.YYYY") + 
																									'&end=' + moment(dateEnd).format("MM.DD.YYYY") +
																									'&is_big=' + sc.formData.timeIsBig +  
                  																					'&isSheet=' + number ,'_blank');																							 
                break;
			case "rs":
				var dateStart = angular.copy(sc.formData.rsStartDate),
                    dateEnd = angular.copy(sc.formData.rsEndDate);
                var popupWin = window.open(LogusConfig.serverUrl + 'CostCtrl/GetRepairsTechSpendingsExcel?tech_id=' + sc.formData.rsTechId +
																									'&equi_id=' + sc.formData.rsEqId +			
																									'&start=' + moment(dateStart).format("MM.DD.YYYY") + 
																									'&end=' + moment(dateEnd).format("MM.DD.YYYY"), '_blank');																							 
                break;
			case "docs":
				var dateStart = angular.copy(sc.formData.docsStartDate),
                    dateEnd = angular.copy(sc.formData.docsEndDate);
                var popupWin = window.open(LogusConfig.serverUrl + 'CostCtrl/GetDocumentRegistryExcel?start=' + moment(dateStart).format("MM.DD.YYYY") + 
																									'&end=' + moment(dateEnd).format("MM.DD.YYYY"), '_blank');																							 
                break;
			case "plan-chart":
                var popupWin = window.open(LogusConfig.serverUrl + 'CostCtrl/GetPlanChartTechResExcel?techIdType=' + sc.formData.planTechTypeId
																							   + '&eqIdType=' + sc.formData.planEqTypeId
				                                                                               + '&year=' + sc.formData.planYearId, '_blank');																							 
                break;
			case "xml":
				var dateStart = angular.copy(sc.formData.xmlStartDate),
                    dateEnd = angular.copy(sc.formData.xmlEndDate);
                var popupWin = window.open(LogusConfig.serverUrl + 'CostCtrl/Get1CSparePartsReport?start=' + moment(dateStart).format("MM.DD.YYYY") + 	
																			'&end=' + moment(dateEnd).format("MM.DD.YYYY"), '_blank');		

                   //excel
				  var    popupWin1 = window.open(LogusConfig.serverUrl + 'CostCtrl/Get1CSparePartsExcel?start=' + moment(dateStart).format("MM.DD.YYYY") + 	
																			'&end=' + moment(dateEnd).format("MM.DD.YYYY"), '_blank');	
				   //
																			
                break;
			case "repairTech":
				var dateStart = angular.copy(sc.formData.techStartDate),
                    dateEnd = angular.copy(sc.formData.techEndDate);
                var popupWin = window.open(LogusConfig.serverUrl + 'CostCtrl/GetRepairEquipReport?start=' + moment(dateStart).format("MM.DD.YYYY") + 																				
				                                                            '&end=' + moment(dateEnd).format("MM.DD.YYYY") +
																			'&techId='+sc.formData.repTechId +
																			'&eqId='+sc.formData.repEqId,
																			'_blank');																							 
                break;	
			case "movespareparts":
				var dateStart = angular.copy(sc.formData.moveStartDate),
                    dateEnd = angular.copy(sc.formData.moveEndDate);
                var popupWin = window.open(LogusConfig.serverUrl + 'CostCtrl/GetMoveSparePartsReport?start=' + moment(dateStart).format("MM.DD.YYYY") + 																				
				                                                            '&end=' + moment(dateEnd).format("MM.DD.YYYY"), '_blank');																							 
                break;	
		    case "table":
			   var dateStart = angular.copy(sc.formData.tableStartDate);
               var dateEnd = angular.copy(sc.formData.tableEndDate);
			   
			   if (sc.formData.tableStartDate.getMonth() == sc.formData.tableEndDate.getMonth()) {
			   
	           var popupWin = window.open(LogusConfig.serverUrl + 'CostCtrl/GetDailyEmployeesWortTimeExpExl?startDate=' + moment(dateStart).format("MM.DD.YYYY") 
	                                                                                  + '&position=' + null
                                                                                      + '&endDate=' + moment(dateEnd).format("MM.DD.YYYY"), '_blank');																							 
                break;
			   } else {
				   modals.showWindow(modalType.WIN_MESSAGE, {
                              title: "Предупреждение",
                              message: "Неверно задан период выгрузки",
                              callback: function () {
                              } 
                          });
						  break;
			   }
			   case "piecework":
			   var dateStart = angular.copy(sc.formData.pieceStartDate);
               var dateEnd = angular.copy(sc.formData.pieceEndDate);
			   
			   if (sc.formData.pieceStartDate.getMonth() == sc.formData.pieceEndDate.getMonth()) {
			   
	           var popupWin = window.open(LogusConfig.serverUrl + 'ReportsCtrl/GetPieceWorkResExcel?startDate=' + moment(dateStart).format("MM.DD.YYYY") 
	                                                                                  + '&emp_id=' + sc.formData.empId
                                                                                     + '&endDate=' + moment(dateEnd).format("MM.DD.YYYY"), '_blank');																							 
                break;
			   } else {
				   modals.showWindow(modalType.WIN_MESSAGE, {
                              title: "Предупреждение",
                              message: "Неверно задан период выгрузки",
                              callback: function () {
                              } 
                          });
						  break;
			   }
				
				
				
		}
    };

 sc.cleanParam = function() {
       sc.formData.employeerId = [];
	   sc.formData.FieldId=[];
	   sc.formData.PlantId=sc.infoData.AllPlants;
    };
 

    sc.traktorChange = function(item) {
        var typeFuelSel = item.Name_fuel;
        sc.formData.TypeFuel = typeFuelSel.Id;
    };
}]);