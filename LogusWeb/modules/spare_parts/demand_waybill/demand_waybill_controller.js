var app = angular.module("DemandWayBill", ["ngGrid", "services", 'ngCookies', "modalWindows","LogusConfig"]);
app.controller("DemandWayBillCtrl", ["$scope", "$http", "requests", "$location", '$cookieStore', '$rootScope', "modals", "modalType",'$stateParams', 'LogusConfig',  function ($scope, $http, requests, $location, $cookieStore, $rootScope, modals, modalType,$stateParams,LogusConfig) {
    var sc = $scope;
    sc.selectedRow = [];
	 var rowsForDel = [];
	 var rowsForUpd = [];
	 sc.formData = {};
	 sc.multi = false;
	  if ($cookieStore.get('dateDemandCalendar')) {
        if ($cookieStore.get('dateDemandCalendar').date) sc.dateWriteOffActs = $cookieStore.get('dateDemandCalendar').date;
    }
	  if ($cookieStore.get('filterTextDemand')) {
		   $("#datepick").addClass("crystalbutton");
         sc.filterText = $cookieStore.get('filterTextDemand');
    }
	 $cookieStore.put('dateDailyCalendar', {date: null});
	 $cookieStore.put('dateEndDailyCalendar', {date: null});
	 var nextNumber;
    sc.gridOptions = {
        enableColumnResize: true,
        enableRowSelection: true,
        showFooter: false,
        rowHeight: 49,
        headerRowHeight: 49,
        useExternalSorting: false,
        data: 'TableData',
		enablePaging: true,
        selectedItems: sc.selectedRow,
		i18n: 'ru',
        afterSelectionChange: function (row) {
       	for (var i = 0; i < row.length; i++ ) 
   { 
     sc.selectedRow.push(row[i].entity); 
     console.log(sc.selectedRow); 
        } 
	  if (row.entity) {
		  console.log(row.selected);
                if (row.selected) {
                    if (sc.multi) sc.selectedRow.push(row.entity);
                    else {
                        sc.selectedRow = [];
                        sc.selectedRow.push(row.entity);
                    }
                } else {
                    sc.selectedRow.splice(sc.TableData.indexOf(row.entity), 1);
                }
            }
	   
        },
        beforeSelectionChange: function (row, event) {
        	   if (!sc.multi) {
                angular.forEach(sc.TableData, function (data, index) {
                    if (data.selected == true) sc.selectedRow.splice(index, 1);
                    sc.gridOptions.selectRow(index, false);

                });
            }
		return true;
        }
    };

    sc.gridOptions.columnDefs = [
          {field: "Date1", displayName: "Дата", width: "30%", cellTemplate: '<a tooltip="{{row.getProperty(col.field)}}" tooltip-popup-delay="1000" tooltip-class="my-custom-tooltip" tooltip-placement="top" tooltip-append-to-body="true">' +
            '<div class="ngCellText" ng-class="col.colIndex()">' +
            '<input data="{{row.entity[col.field]}}" ng-change="changeCountPrice(row.entity)" ng-model="row.entity[col.field]" ng-readonly="row.entity.Id" ng-class="{bordernone: row.entity[\'Id\']}">' +
            '</div>' +
            '</a>', headerCellTemplate: "<div   ng-class=\"'colt' + col.index\" class=\"ngHeaderText\">{{col.displayName}}</div>"},
			
		 {field: "Act_number", displayName: "Номер акта", width: "25%",
			 cellTemplate: '<a tooltip="{{row.entity[col.field][\'Name\']}}" tooltip-popup-delay="1000" tooltip-class="my-custom-tooltip" tooltip-placement="top" tooltip-append-to-body="true">' +
             '<div class="ngCellText" ng-class="col.colIndex()">' +
             '<input value="{{row.entity[col.field]}}" ng-model="row.entity[col.field]" placeholder="Номер" ng-class="{bordernone: row.entity[\'Id\']}" ng-readonly="row.entity[\'Id\']"  ng-change="changeCountPrice(row.entity)">' +
             '</div>' +'</a>',  headerCellTemplate: "<div   ng-class=\"'colt' + col.index\" class=\"ngHeaderText\">{{col.displayName}}</div>"},
			{field: "Tech", displayName: "TC/Оборудование", width: "25%",  cellTemplate: '<a tooltip="{{row.entity[col.field][\'Name\']}}" tooltip-popup-delay="1000" tooltip-class="my-custom-tooltip" tooltip-placement="top" tooltip-append-to-body="true">' +
             '<div class="ngCellText" ng-class="col.colIndex()">' +
             '<input value="{{row.entity[col.field]}}" ng-model="row.entity[col.field]" placeholder="TC/Оборудование" ng-class="{bordernone: row.entity[\'Id\']}" ng-readonly="row.entity[\'Id\']"  ng-change="changeCountPrice(row.entity)">' +
             '</div>' +'</a>',  headerCellTemplate: "<div   ng-class=\"'colt' + col.index\" class=\"ngHeaderText\">{{col.displayName}}</div>"},
			 
			 {field: "CarNum", displayName: "Гос. №", width: "15%",  cellTemplate: '<a tooltip="{{row.entity[col.field][\'Name\']}}" tooltip-popup-delay="1000" tooltip-class="my-custom-tooltip" tooltip-placement="top" tooltip-append-to-body="true">' +
             '<div class="ngCellText" ng-class="col.colIndex()">' +
             '<input value="{{row.entity[col.field]}}" ng-model="row.entity[col.field]" placeholder="Гос. №" ng-class="{bordernone: row.entity[\'Id\']}" ng-readonly="row.entity[\'Id\']"  ng-change="changeCountPrice(row.entity)">' +
             '</div>' +'</a>',  headerCellTemplate: "<div   ng-class=\"'colt' + col.index\" class=\"ngHeaderText\">{{col.displayName}}</div>"},
			 {field: "Comment", displayName: " ", width: "5%",
             cellTemplate: 'templates/comment_shift_template.html', cellClass: 'cellToolTip'},
			 {field: "Id", width: "0%"},
			 {field: "Date", width: "0%"},
			 /////
			 	 
    ];
    function loadService() {
	   requests.serverCall.post($http, "SparePartsCtrl/GetDemandInvoiceByDate", {Date: new Date(sc.dateWriteOffActs), Number : sc.filterText}, function (data, status, headers, config) {
		  sc.TableData = data;
		});
    };
  loadService();
  
   sc.dateChangeSpareParts = function () {
	   $cookieStore.put('dateDemandCalendar', {date: sc.dateWriteOffActs}); 
	   loadService();
		console.log(sc.dateWriteOffActs);
   }
	//редактирование акт списания
    sc.editBtnClick = function () {
        if (sc.selectedRow[0]) {
		$cookieStore.put('typeoper', 'edit');	
		$cookieStore.put('act_id', sc.selectedRow[0].Id);
		$cookieStore.put('Act_number', sc.selectedRow[0].Act_number);
		$cookieStore.put('DemandDate', sc.selectedRow[0].Date);
        $location.path('/demand_waybill_item');
        } else needSelection('Просмотр документа', "Для просмотра необходимо выбрать документ");
    };

	//поиск по номеру акта
	sc.search = function() {
	$cookieStore.put('filterTextDemand', sc.filterText);
		if (sc.filterText.length==0) {
		 $("#datepick").removeClass("crystalbutton");	
		} else { 
	 $("#datepick").addClass("crystalbutton");
		}
		
      if ($cookieStore.get('timerDem')) {
	   clearInterval($cookieStore.get('timerDem'));
   }
    $cookieStore.put('timerDem', setTimeout(loadService, 1000));	
	 }
	
	 $('#datepick').click(function(){
		  console.log('d');
	  sc.filterText =  "";
      $("#datepick").removeClass("crystalbutton");
     });
	  $('button').click(function(){
		   console.log('d1');
		   $("#datepick").removeClass("crystalbutton"); 
		   sc.filterText ="";
	  });
		sc.printClick = function()  {
		if (sc.selectedRow[0].Id) {
            var popupWin = window.open(LogusConfig.serverUrl + 'SparePartsCtrl/GetPrintWriteoffInfo?req=' + sc.selectedRow[0].Id + '&act_number='
                                                                                        + sc.selectedRow[0].Act_number 
																					    +'&date='+ sc.selectedRow[0].Date,
																						'_blank');
        } else {
            modals.showWindow(modalType.WIN_MESSAGE, {
                title: "Печать",
                message: "Для печати необходимо сохранить данные",
                callback: function () {

                }
            });
        }
	}
	
	sc.printHtml = function()  {
		       requests.serverCall.post($http, "SparePartsCtrl/GetPrintDemandInfo", {ActId: sc.selectedRow[0].Id,RepairNumber: sc.selectedRow[0].Act_number, dateStart: sc.selectedRow[0].Date},
			   function (data, status, headers, config) {
                    console.info(data);
					sc.printData = data;
                    setTimeout(function () {
                        var printContents = document.getElementById('demand').innerHTML;
                        var popupWin = window.open('', '_blank');
                        if (typeof popupWin != 'undefined') {
                            popupWin.document.write('<html><head></head><body >' + printContents + '</html>');
                            popupWin.print();
                            popupWin.close();
                        }
                    }, 1000);
                });
	}
	
	
	
	
	function getMaxOfArray(numArray) {
   return Math.max.apply(null, numArray);
   }
	    // модальное окно с требованием выделить строку
    function needSelection(title, message) {
        modals.showWindow(modalType.WIN_MESSAGE, {
            title: title,
            message: message,
            callback: function () {
            }
        });
    }
	function sec1() { 
 $("div.ngFooterSelectedItems").remove();
}
setInterval(sec1, 200) 

}]);



