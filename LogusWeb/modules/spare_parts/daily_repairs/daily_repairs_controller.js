var app = angular.module("RepairsDailyCtrl", ["ngGrid", "services", 'ngCookies', "modalWindows","LogusConfig"]);
app.controller("RepairsDailyCtrl", ["$scope", "$http", "requests", "$location", '$cookieStore', '$rootScope', "modals", "modalType",'$stateParams', 'LogusConfig',  function ($scope, $http, requests, $location, $cookieStore, $rootScope, modals, modalType,$stateParams,LogusConfig) {
    var sc = $scope;
	 var curUser = $cookieStore.get('currentUser');
    $rootScope.userName = curUser.username;
	console.log(curUser.roleid);
	if (curUser.roleid!=10) {
		 $("#datepick2").addClass("disabledbutton");
		 $("#message").addClass("disabledbutton");
	}
    sc.selectedRow = [];
	 var rowsForDel = [];
	 var rowsForUpd = [];
	 var count = 0;
	 sc.multi = false;
	sc.formData = {};
	 if ($cookieStore.get('dateDailyCalendar')) {
        if ($cookieStore.get('dateDailyCalendar').date) sc.formData.dailyStartDate = $cookieStore.get('dateDailyCalendar').date;
    }
	 if ($cookieStore.get('dateEndDailyCalendar')) {
        if ($cookieStore.get('dateEndDailyCalendar').date) sc.formData.dailyEndDate = $cookieStore.get('dateEndDailyCalendar').date;
    }
    sc.formData.OpenRepair = true;
	 var nextNumber;
    sc.gridOptions = {
        enableColumnResize: true,
        enableRowSelection: true,
        showFooter: false,
        rowHeight: 49,
        headerRowHeight: 49,
        useExternalSorting: false,
        data: 'TableData',
		enablePaging: true,
        selectedItems: sc.selectedRow,
		i18n: 'ru',
        afterSelectionChange: function (row) {
       	for (var i = 0; i < row.length; i++ ) 
   { 
     sc.selectedRow.push(row[i].entity); 
     console.log(sc.selectedRow); 
        } 
	  if (row.entity) {
		  console.log(row.selected);
                if (row.selected) {
                    if (sc.multi) sc.selectedRow.push(row.entity);
                    else {
                        sc.selectedRow = [];
                        sc.selectedRow.push(row.entity);
                    }
                } else {
                    sc.selectedRow.splice(sc.TableData.indexOf(row.entity), 1);
                }
            }
	   
        },
        beforeSelectionChange: function (row, event) {
        	   if (!sc.multi) {
                angular.forEach(sc.TableData, function (data, index) {
                    if (data.selected == true) sc.selectedRow.splice(index, 1);
                    sc.gridOptions.selectRow(index, false);

                });
            }
		return true;
        }
    };

    sc.gridOptions.columnDefs = [
			{field: "Id", width: "0%"},
	        {
                field: "Number",
                displayName: "№",
                width: "10%",
                cellTemplate: '<div class="ngCellText {{getClassCopyCell(row.entity.Status,row)}}" ng-class="col.colIndex()" style = "border-top: 1px solid #06aa9f; border-bottom: 1px solid #06aa9f; border-radius: 0;">' +
				'<input value="{{row.entity[col.field]}}" ng-model="row.entity[col.field]" placeholder="№" class="bordernone" readonly>' +
				'</div>'
            },
			
          {field: "Date", displayName: "Дата", width: "15%", cellTemplate: '<a tooltip="{{row.getProperty(col.field)}}" tooltip-popup-delay="1000" tooltip-class="my-custom-tooltip" tooltip-placement="top" tooltip-append-to-body="true">' +
            '<div class="ngCellText {{getClassCopyCell(row.entity.Status,row)}}" ng-class="col.colIndex()" style = "border-top: 1px solid #06aa9f; border-bottom: 1px solid #06aa9f; border-radius: 0;">' +
            '<input data="{{row.entity[col.field]}}" ng-change="changeCountPrice(row.entity)" ng-model="row.entity[col.field]" ng-readonly="row.entity.Id" ng-class="{bordernone: row.entity[\'Id\']}">' +
            '</div>' +
            '</a>', headerCellTemplate: "<div   ng-class=\"'colt' + col.index\" class=\"ngHeaderText\">{{col.displayName}}</div>"},
			 {field: "CarsName",
                displayName: "TC",
                width: "20%",
                cellTemplate: '<div class="ngCellText {{getClassCopyCell(row.entity.Status,row)}}" ng-class="col.colIndex()" style = "border-top: 1px solid #06aa9f; border-bottom: 1px solid #06aa9f; border-radius: 0;">' +
				'<input value="{{row.entity[col.field]}}" ng-model="row.entity[col.field]" placeholder="TC" class="bordernone" readonly>' +
				'</div>'
            },
			 {field: "CarsNum",
                displayName: "Гос. №",
                width: "15%",
                cellTemplate: '<div class="ngCellText {{getClassCopyCell(row.entity.Status,row)}}" ng-class="col.colIndex()" style = "border-top: 1px solid #06aa9f; border-bottom: 1px solid #06aa9f; border-radius: 0;">' +
				'<input value="{{row.entity[col.field]}}" ng-model="row.entity[col.field]" placeholder="Гос. №" class="bordernone" readonly>' +
				'</div>'
            },
			{field: "Agr",
                displayName: "Агрегат",
                width: "30%",
                cellTemplate: '<div class="ngCellText {{getClassCopyCell(row.entity.Status,row)}}" ng-class="col.colIndex()" style = "border-top: 1px solid #06aa9f; border-bottom: 1px solid #06aa9f; border-radius: 0;">' +
				'<input value="{{row.entity[col.field]}}" ng-model="row.entity[col.field]" placeholder="Агрегат" class="bordernone" readonly>' +
				'</div>'
            },
			
			{field: "Status", displayName: "Статус", width: "10%", cellTemplate: '<div class="ngCellText {{getClassCopyCell(row.entity.Status,row)}}" ng-class="col.colIndex()" style = "border-top: 1px solid #06aa9f; border-bottom: 1px solid #06aa9f; border-radius: 0;">' +
				'<input value="{{row.entity[col.field]}}" ng-model="row.entity[col.field]" placeholder="Статус" class="bordernone" readonly>' +
				'</div>'}		
    ];
	
	sc.getClassCopyCell = function (stat,row) {
        if (stat === "Закрыт" && row.selected==false ) {
            return 'SPclose';
        }
		if (row.selected==true) {  return '';}
    };
	        sc.printBtnClick = function() {
			if (sc.selectedRow[0].Id) {
			 var popupWin = window.open(LogusConfig.serverUrl + 'CostCtrl/GetDailyResExcel?ActId=' + sc.selectedRow[0].Id, '_blank');
			}}

		
			 requests.serverCall.get($http, "SparePartsCtrl/GetDailyBanDate"
			, function (data, status, headers, config) {
				sc.ban_date = data;
			console.log(data);
		});	
		
		
		
    function loadService() {
		requests.serverCall.post($http, "SparePartsCtrl/GetDailyRepairsList", {
			DateStart: sc.formData.dailyStartDate,
			DateEnd : sc.formData.dailyEndDate,
			Name: sc.filterText,
			CheckSearch : sc.filterText2,
			}, function (data, status, headers, config) {
			sc.TableData = data;
			console.log(data);
		});
    };
	
	loadService();
	sc.dateChangeSpareParts = function () {
		 $cookieStore.put('dateDailyCalendar', {date: sc.formData.dailyStartDate});
		 $cookieStore.put('dateEndDailyCalendar', {date: sc.formData.dailyEndDate});
		loadService();
	}
		 sc.search = function() {
		 sc.filterText2=""; 
		if (sc.filterText.length!=0) {
	 $("#datepick").addClass("crystalbutton");
		}
		
      if ($cookieStore.get('timerId2')) {
	   clearInterval($cookieStore.get('timerId2'));
   }
    $cookieStore.put('timerId2', setTimeout(loadService, 1000));	
	};
	
 $('#datepick').click(function(){
	sc.filterText =  "";
	sc.filterText2 ="";
    $("#datepick").removeClass("crystalbutton");
     });
	  $('button').click(function(){
		   $("#datepick").removeClass("crystalbutton"); 
		   sc.filterText ="";
		   sc.filterText2 ="";
	  });
	  sc.search2 = function() {
		 sc.filterText=""; 
		if (sc.filterText2.length!=0) {
	 $("#datepick").addClass("crystalbutton");
		}
		
      if ($cookieStore.get('timerId3')) {
	   clearInterval($cookieStore.get('timerId3'));
   }
    $cookieStore.put('timerId3', setTimeout(loadService, 1000));	
	 }
	 //Дата запрета
	 sc.banEditDaily = function() {
		 count++;
		 if (count > 2) {
		 requests.serverCall.post($http, "SparePartsCtrl/SetDailyBanDate", sc.ban_date 
			, function (data, status, headers, config) {
			console.log(data);
		});
		 }
	 }
	 
	 
	 
	 
   //закрыть акт 
   sc.closeBtnClick = function() {
	   if (sc.selectedRow[0]) {
		  console.log(sc.selectedRow[0].Id);
		  $cookieStore.put('typeoper', 'close');	
		  $cookieStore.put('act_id', sc.selectedRow[0].Id);
          $location.path('/daily_repairs_item/');
        } else needSelection('Закрытие документа', "Для закрытия необходимо выбрать документ");
   }
   
   
    //создать акт 
    sc.createIncomingAct = function() {
		$cookieStore.put('typeoper', 'create');
		$cookieStore.put('act_id', null);
		console.log($cookieStore.get('typeoper'));
		$location.path('/daily_repairs_item/');
    };
	
	//редактирование акт 
    sc.editBtnClick = function () {
        if (sc.selectedRow[0]) {
		$cookieStore.put('typeoper', 'edit');	
		$cookieStore.put('act_id', sc.selectedRow[0].Id);
          $location.path('/daily_repairs_item/');
        } else needSelection('Редактирование документа', "Для редактирования необходимо выбрать документ");
    };
	
	function getMaxOfArray(numArray) {
	   return Math.max.apply(null, numArray);
	}
	
	// модальное окно с требованием выделить строку
    function needSelection(title, message) {
        modals.showWindow(modalType.WIN_MESSAGE, {
            title: title,
            message: message,
            callback: function () {
            }
        });
    }
	
	//удаление строки 
	  sc.deleteAct = function() {
		    console.log(sc.selectedRow);
		    if (sc.selectedRow.length!=0) {
            modals.showWindow(modalType.WIN_DIALOG, {
                    title: "Удаление", message: "Удалить выбранный документ?", callback: function () {	
						console.log(sc.selectedRow[0].Id);
                        requests.serverCall.post($http, "SparePartsCtrl/DeleteAct", {Id : sc.selectedRow[0].Id, Number1 : curUser.roleid }, function (data, status, headers, config) {
							rowsForDel = [];
							loadService();
                            console.info(data);
                        });
						;}
                    }
                
            );
        } else needSelection('Удаление документа', "Для удаления необходимо выбрать документ");
	}
	
	function sec1() { 
		$("div.ngFooterSelectedItems").remove(); 
	}
	
	setInterval(sec1, 200) 

}]);



