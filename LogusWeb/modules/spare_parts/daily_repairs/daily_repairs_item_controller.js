
var app = angular.module("DailyRepairsItem", ["ngGrid", "services", 'ngCookies', "modalWindows", "LogusConfig","ui.bootstrap",'Directives',"ui.utils"]);
app.controller("RepairsDailyItemCtrl", ["$scope", "$http", "requests", "$location", '$cookieStore', '$rootScope', "modals", "modalType", '$stateParams', 'preloader', "LogusConfig", function ($scope, $http, requests, $location, $cookieStore, $rootScope, modals, modalType, $stateParams, preloader, LogusConfig) {
    var sc = $scope;
    var curUser = $cookieStore.get('currentUser');
    $rootScope.userName = curUser.username;
    $rootScope.exit = 'выход';
    sc.mySelections = [];
	sc.mySelections1 =[];
	sc.mySelections2 =[];
    sc.tableData = [];
    sc.tableData2 = [];
    sc.formData = {};
    sc.infoData = {};sc.infoData2 = {};
	sc.formData.Id; var checkDatepick = 0;
    var rowsForUpd = [];
	 sc.multi = false;
	var met=1;
    //тип операции с item (передается по клику по кнопке в documents_list)
   // sc.typeOperation = $stateParams.type;
  var m1=0; count=0;
 
	//таблица2
 sc.gridOptions2 = {
    data: 'tableData2',
    enableColumnResize: true,
    enableRowSelection: true,
    showFooter: false,
    selectedItems: sc.mySelections2,
    rowHeight: 49,
    headerRowHeight: 49,
    
    columnDefs: [
    
		    {field: "Number1", displayName: "№ п/п", width: "10%",
            cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()">' +
            '<input value="{{row.entity[col.field]}}" ng-model="row.entity[col.field]" placeholder="" class="bordernone" readonly>' +
            '</div>',headerCellTemplate: "<div   ng-class=\"'colt' + col.index\" class=\"ngHeaderText\">{{col.displayName}}</div>"
            },
            {
                field: "Employees",
                displayName: "Сотрудник",
                width: "20%",
                cellTemplate:'<a tooltip="{{row.entity[col.field][\'Name\']}}" tooltip-popup-delay="1000" tooltip-class="my-custom-tooltip" tooltip-placement="top" tooltip-append-to-body="true">' +
                '<div class="ngCellText" ng-class="col.colIndex()">' +
                '<input value="{{row.entity[col.field][\'Name\']}}" placeholder="Сотрудник" style="border: none;"  data-ng-click="updateSelectedCell(col.field, row.entity[col.field][\'Id\'], \'Сотрудник\',row.entity,$event)" readonly>' +
                '</div>' +
                '</a>'
            },
			{
                field: "Tasks",
                displayName: "Вид работ",
                width: "20%",
                cellTemplate: '<a tooltip="{{row.entity[col.field][\'Name\']}}" tooltip-popup-delay="1000" tooltip-class="my-custom-tooltip" tooltip-placement="top" tooltip-append-to-body="true">' +
				'<div class="ngCellText" ng-class="col.colIndex()">' +
				'<input value="{{row.entity[col.field][\'Name\']}}" placeholder="Наименование" style="border: none;"  data-ng-click="updateSelectedCell(col.field, row.entity[col.field][\'Id\'], \'Наименование\',row.entity,$event)" readonly>' +
				'</div>' +
				'</a>', headerCellTemplate: "<div   ng-class=\"'colt' + col.index\" class=\"ngHeaderText\">{{col.displayName}}</div>"
            },
			{field: "RateShift", displayName: "Тариф", width: "10%",
            cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()">' +
            '<input value="{{row.entity[col.field]}}" ng-model="row.entity[col.field]" placeholder="Тариф" class="bordernone" readonly  ng-change="changeHours(row.entity)">' +
            '</div>',headerCellTemplate: "<div   ng-class=\"'colt' + col.index\" class=\"ngHeaderText\">{{col.displayName}}</div>"
            },
			{field: "RatePiecework", displayName: "Расценка", width: "10%",
            cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()">' +
            '<input value="{{row.entity[col.field]}}" ng-model="row.entity[col.field]" placeholder="Расценка" class="bordernone" readonly  ng-change="changeHours(row.entity)">' +
            '</div>',headerCellTemplate: "<div   ng-class=\"'colt' + col.index\" class=\"ngHeaderText\">{{col.displayName}}</div>"
            },
			{field: "Hours", displayName: "Отработано часов", width: "15%",
            cellTemplate: '<a tooltip="{{row.entity[col.field][\'Name\']}}" tooltip-popup-delay="1000" tooltip-class="my-custom-tooltip" tooltip-placement="top" tooltip-append-to-body="true">' +
            '<div class="ngCellText" ng-class="col.colIndex()">' +
            '<input value="{{row.entity[col.field]}}" ng-model="row.entity[col.field]" placeholder="" ng-class="{bordernone: row.entity[\'Id\']}" ng-change="changeHours(row.entity)">' +
            '</div>' + '</a>',  headerCellTemplate: "<div   ng-class=\"'colt' + col.index\" class=\"ngHeaderText\">{{col.displayName}}</div>"
            },
			{field: "SumCost", displayName: "Итого начислено", width: "15%",
            cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()">' +
            '<input value="{{row.entity[col.field]}}" ng-model="row.entity[col.field]" placeholder="Суммарная стоимость" class="bordernone" readonly  ng-change="changeHours(row.entity)">' +
            '</div>',headerCellTemplate: "<div   ng-class=\"'colt' + col.index\" class=\"ngHeaderText\">{{col.displayName}}</div>"
            },
			
    ],
    afterSelectionChange: function (row) {	
	met =1;
	for (var i=0; i<row.length;i++ ) 
   { 
     sc.mySelections2.push(row[i].entity); 
     console.log(sc.mySelections2); 
        } 
		if (sc.mySelections1==row.entity) { console.log('есть');met=2;}
			sc.mySelections1 = row.entity;
	  if (row.entity) {
		  console.log(row.selected);
                if (row.selected) {
                    if (sc.multi) sc.mySelections2.push(row.entity);
                    else {
                        sc.mySelections2 = [];
                        sc.mySelections2.push(row.entity);
                    }
                } else {
                    sc.mySelections2.splice(sc.tableData.indexOf(row.entity), 1);
                }
            }
	
    },   
    beforeSelectionChange: function (row, event) {
		   if (!sc.multi) {
                angular.forEach(sc.tableData, function (data, index) {
                    if (data.selected == true) sc.mySelections2.splice(index, 1);
                    sc.gridOptions.selectRow(index, false);

                });
            }
                return true;
            }
    };
	console.log($cookieStore.get('typeoper'));
		if ($cookieStore.get('typeoper')=='open' ||  $cookieStore.get('checkWriteOff')==3 || !$cookieStore.get('dateDailyCalendar')) {
			var d = new Date();
			sc.formData.Date = d;
			 $cookieStore.put('dateDailyCalendar', {date: d});
		}
		//количество ОХР
		
		sc.getInfoGeneralWorks = function() {
		requests.serverCall.get($http, "SparePartsCtrl/GetCountGenaralWorks", function (data, status, headers, config) {
        console.log(data);
		sc.formData.CountGeneralWorks = data.CountGeneralWorks;
	    sc.infoData.Agr = data.Agr;
		sc.infoData.Employees = data.Employees;
		sc.infoData.TaskTypes = data.TaskTypes;
		sc.infoData.TraktorsList = data.TraktorsList;
	});
		}
		sc.getInfoGeneralWorks();
	console.log($cookieStore.get('typeoper'));
	//получение массива номеров актов
	
	if ($cookieStore.get('typeoper')=='create') {
	var d = new Date();
	sc.formData.Date = d;	
	     }
	
	
	requests.serverCall.post($http, "SparePartsCtrl/GetRepairsActs", { "number_act": sc.formData.RepairsActs,"date_start":sc.formData.Date, 
	"typeoper":$cookieStore.get('typeoper'),	
	}, function (data, status, headers, config) {
        console.log(data);
        sc.infoData.RepairsActs = data;	//забить массив актов
	//проверка, редактирование или создание
	     if ($cookieStore.get('typeoper') != 'open') {
//		sc.formData.Date = $cookieStore.get('dateDailyCalendar').date ;
		 }
	sc.acceptButton = "Ок";
	if ($cookieStore.get('typeoper') == 'edit' || $cookieStore.get('typeoper') == 'close')
	{
		// запрос на данные акта с id $cookieStore.get('act_id') и их подгрузка 
		sc.formData.Id = $cookieStore.get('act_id');
		console.log(sc.formData.Id);
		if (sc.formData.Id != null)
		{
			console.log(sc.formData.Id);
			requests.serverCall.post($http, "SparePartsCtrl/GetDailyRepairsItem", { "Id": sc.formData.Id }, function (data, status, headers, config) {
				console.info(data);
				sc.formData = data.Main;
				sc.tableData2 = data.Spendings;
			//			sc.numChange();
				if (sc.formData.GeneralWorks) {
	        $("#repairsNumber").addClass("disabledbutton");
			 $("#generalWork").addClass("disabledbutton");
			 $("#agr").removeClass("disabledbutton");
			 	//количество ОХР
	sc.getInfoGeneralWorks();
			 
			 
			 
		}	else {
			 $("#generalWork").addClass("disabledbutton");
		}
				});
			
			if ($cookieStore.get('typeoper') == 'close')
			{
				sc.acceptButton = "Закрыть";
			}
		}
	
	}
	//нажатие на ОХР
	sc.checkGeneralWorks = function() {
		if (sc.formData.GeneralWorks) {
	$("#repairsNumber").addClass("disabledbutton");
	$("#agr").removeClass("disabledbutton");

	sc.formData.Agr = null;
		sc.formData.TraktorId = null;
	sc.formData.RepairsActs = -1;
	sc.formData.DailyNum = 'ОХР/'+parseInt(sc.formData.CountGeneralWorks);
         sc.getInfoGeneralWorks();
		} else {
		$("#repairsNumber").removeClass("disabledbutton");	
		$("#agr").addClass("disabledbutton");
		sc.formData.Agr = null;
		sc.formData.TraktorId = null;
		sc.formData.DailyNum = null;
		sc.formData.RepairsActs = null;
		} 	
	}

	sc.dateChangeSpareParts = function() {
requests.serverCall.post($http, "SparePartsCtrl/GetRepairsActs", { "number_act": sc.formData.RepairsActs,"date_start":sc.formData.Date, 
	"typeoper":$cookieStore.get('typeoper'),
	
	}, function (data, status, headers, config) {
        console.log(data);
        sc.infoData.RepairsActs = data;	//забить массив актов
	});
	
	} 
	//создание через наряд
	if ($cookieStore.get('typeoper') == 'open')
	{
		$("#generalWork").addClass("disabledbutton"); 
		sc.formData.timeStart = '08:00';
		sc.formData.timeEnd = '17:00';
		if ($cookieStore.get('DayAct')!=null) {
			sc.numChange();	
			sc.formData.RepairsActs = $cookieStore.get('DayAct');
			var d = new Date();
			sc.formData.Date = d;
		}
	}
	if ($cookieStore.get('typeoper') == 'create')
	{
		sc.formData.GeneralWorks = false;
		//копирование ЕН
	if ($cookieStore.get('checkCopyWriteOff') == 1)
	{
	$("#generalWork").addClass("disabledbutton");
	$("#repairsNumber").addClass("disabledbutton");
    $("#datepick").addClass("crystalbutton"); 
	checkDatepick = 1;
	var Id = JSON.parse($.session.get('formDataWriteOff')).ActId;
	var DayId = JSON.parse($.session.get('tableData4WriteOff')).DailyAct;
	console.log(DayId);
 	//	console.log(JSON.parse($.session.get('tableData4WriteOff')));
	//	sc.tableData2.push(JSON.parse($.session.get('tableData4WriteOff')));
				requests.serverCall.post($http, "SparePartsCtrl/GetDailyRepairsItem", { "Id": DayId }, function (data, status, headers, config) {
				console.info(data);
				sc.tableData2 = data.Spendings;
				});
	
	
		  for (var i = 0; i < sc.tableData2.length; i++) {
			   sc.tableData2[i].Number1 = i+1;
		   }
             sc.infoData.RepairsActs = [];
			sc.infoData.RepairsActs.push({Id:Id,
			                              Name : JSON.parse($.session.get('formDataWriteOff')).RepairNumber});
		  sc.formData.RepairsActs = JSON.parse($.session.get('formDataWriteOff')).ActId; 
	}	
		
		
	sc.formData.timeStart = '08:00';
    sc.formData.timeEnd = '17:00';	
	}
	$('button.date-picker-btn').click(function(){
		checkDatepick = 0;
		 $("#datepick").removeClass("crystalbutton");	
		});
	
	
	
    var _field, _val;
    // выделение строчки по определенному столбцу
    function selectRowByField(field, val) {
        _field = field;
        _val = val;
        sc.$on('ngGridEventData', sc.gridEvDataListener);
    }
	sc.changeHours = function(row){
		row.Hours = row.Hours.toString().replace(",", "."); 
		row.SumCost = row.RatePiecework * row.Hours + row.RateShift;
	}
	
	sc.removeTask2 = function() {
		sc.tableData2.splice(sc.tableData2.indexOf(sc.mySelections2[0]), 1);
		   for (var i = 0; i < sc.tableData2.length; i++) {
			   sc.tableData2[i].Number1 = i+1;
		   }
		
	}
	
    // мультивыделение
    sc.keyDownGrid = function (e) {
        if (curUser.roleid == '2' || curUser.roleid == '10') {
            if ((e.keyCode === 16 || e.keyCode === 17 || e.keyCode === 18 || e.keyCode === 91) && !sc.multi) {
             //   console.info("__________________ keyDown  " + e.keyCode);
                sc.multi = true;
             //   console.info(sc.multi);
            }
        }
    };
	    sc.keyUpGrid = function (e) {
        // 16 - Shift, 17 - Ctrl, 18 - Alt, 91 - Win key (Start)
        if (curUser.roleid == '2' || curUser.roleid == '10') {
            if (e.keyCode === 16 || e.keyCode === 17 || e.keyCode === 18 || e.keyCode === 91) {
             //   console.info("_________________ keyUp " + e.keyCode);
                sc.multi = false;
             //   console.info(sc.multi);
            }
        }
    };
    // листнер на событие изменение data в таблице
    sc.gridEvDataListener = function () {
        var offCallMeFn = $scope.$on("ngGridEventData", sc.gridEvDataListener);
        offCallMeFn();
        if (_val == "first" && sc.tableData.length > 0) _val = sc.tableData[0][_field];
		console.log(_val);
        sc.tableData.forEach(function (item, i, arr) {
            if (item[_field] == _val) {
                sc.gridOptions.selectRow(i, true);
                if (sc.gridOptions.ngGrid) sc.gridOptions.ngGrid.$viewport.scrollTop(sc.gridOptions.ngGrid.rowMap[i] * sc.gridOptions.ngGrid.config.rowHeight);
            } else {
                sc.gridOptions.selectRow(i, false);
            }
        });
        $(".ngViewport").focus();
    };
	 });
	
	// получение масива в выпадающие списки
	sc.numChange = function() {
		if ($cookieStore.get('typeoper')=='open') {
			var d = new Date();
			sc.formData.Date = d;
			$cookieStore.put('dateDailyCalendar', {date: d});
		}
		console.log(sc.formData.RepairsActs);
		if (sc.formData.RepairsActs != null) {
		requests.serverCall.post($http, "SparePartsCtrl/GetDictionaryDailyInfo", { "rep_id": sc.formData.RepairsActs,"dail_id": $cookieStore.get('act_id'), 
		"date_start":sc.formData.Date, "typeoper":$cookieStore.get('typeoper') }, function (data, status, headers, config) {
			console.info(data);
			sc.infoData = data;
			if (sc.infoData.NewDailyActs != null) {
			sc.formData.DailyNum = sc.infoData.NewDailyActs;
            }
             if (!sc.formData.GeneralWorks) {
			if (data.Agr.length!=0) {sc.formData.Agr = data.Agr[0].Id;}
			if (data.TraktorsList.length!=0) {sc.formData.TraktorId = data.TraktorsList[0].Id;}
			 }
		});
		}
	}

    // при редатировании ячейки с выпадающим списком
    sc.editDropCell = function (row, column, selectedItem) {
        sc.tableData.forEach(function (item, d) {
            if (item == row) item[column] = selectedItem;
        })
    };

    // при редатировании ячейки с input
    sc.editInputCell = function (row, column, value) {
        sc.tableData.forEach(function (item, d) {
            if (item == row) item[column] = value;
        })
    };
    // редактирование ячек с модальными окнами
          sc.updateSelectedCell = function (colField, selectedVal, displayName, row) {
		    met=1;
            sc.colField = colField;
            sc.selectedRow = [];
            sc.selectedRow.push(row);
			if (sc.mySelections1==row) {met = 2;}
			  console.log(row);
			sc.mySelections1 = row;
			console.log(sc.mySelections1);
            var modalValues;
            if (sc.infoData) {
				if (met==2) {
                switch (sc.colField) {
                    case "SpareParts":
                        modalValues = sc.infoData.SparePartsList;
                        break;
                    case "VendorCode":
                        modalValues = sc.infoData.VendorCodeList;
                        break;
					case "Storage":
                        modalValues = sc.infoData.StorageList;
                        break;
					case "Tasks":
                        modalValues = sc.infoData.TaskTypes;
                        break;
					case "Employees":
				 modalValues = sc.infoData.Employees;
                        break;
				}
             
				
                modals.showWindow(modalType.WIN_SELECT, {
                    nameField: displayName,
                    values: modalValues,
                    selectedValue: selectedVal
                })
				
					//выбор значения ячейки с модальным окном
                    .result.then(function (result) 
					{
                        row[sc.colField] = result;
                       switch (colField) {
						case "SpareParts":
							console.info(row);
							row.VendorCode = {Id: result.Id, Name :result.Description};
							requests.serverCall.post($http, "SparePartsCtrl/GetSparePartCost", { "Id": result.Id }, 
							function (data, status, headers, config) {
								console.log(data.Price);
								row.Cost = data.Price;
							});		
							// список складов, где есть данная зп
							break;
	                    case "VendorCode":
							row.SpareParts = {Id: result.Id, Name :result.Description};
							break;
						case "Tasks":
							//запрос на подгрузку расценки ремонтной работы
							requests.serverCall.post($http, "SparePartsCtrl/GetTaskRate", { "TptasId": result.Id, "TraktId": sc.formData.TraktorId, "EquiId": sc.formData.Agr }, 
							function (data, status, headers, config) {
								console.log(data);
								row.RateShift = data.RateShift;
								row.RatePiecework = data.RatePiecework;
								sc.changeHours(row);
							});		
							break;						
					}	
						
                    }, function () {});
				}
            } else {}
       
    };
	function unique(arr) {
  var obj = {};

  for (var i = 0; i < arr.length; i++) {
    var str = arr[i];
    obj[str] = true; // запомнить строку в виде свойства объекта
  }

  return Object.keys(obj); // или собрать ключи перебором для IE8-
}
	function getMaxOfArray(numArray) {
   return Math.max.apply(null, numArray);
   }

	    //создать таблица2
    sc.createAct2 = function () {
		  var c = [];
	for (i = 0; i < sc.tableData2.length; i++) {
		c.push(sc.tableData2[i].Number1);
      }
	  con=getMaxOfArray(c);
	  		  console.log(con);
		if (con==-Infinity)	 {con=0;} 
        con = parseInt(con)+1;
        var newAct = {
            Date1 : new Date(),
            Number1 : con
        };
        sc.tableData2.push(newAct);
		setTimeout(function() {
			var grid = sc.gridOptions2.ngGrid;
			grid.$viewport.scrollTop(sc.tableData2.length * grid.config.rowHeight + 100);
		}, 100);
    }; 

    //поиск элемента в массиве
	    find = function (array, value) {
	        for (var i = 0; i < array.length; i++) {
	            if (array[i] == value) return i;
	        }
	        return -1;}
	
    function needSelection(title, message) {
		modals.showWindow(modalType.WIN_MESSAGE, {
			title: title,
			message: message,
			callback: function () {
			}
		});
	}
	
	sc.printTask = function () {
		if (sc.formData.Id) {
			 var popupWin = window.open(LogusConfig.serverUrl + 'CostCtrl/GetDailyResExcel?ActId=' + sc.formData.Id, '_blank');
			}
	}


	sc.submitForm = function () {
		if (sc.formData.DailyNum == "" || sc.formData.DailyNum == null )
		{	needSelection("Ошибка", "Заполните номер ежедневного наряда") } else
		if (checkDatepick == 1) {needSelection("Ошибка", "Выберите дату ежедневного наряда")}
		else
		{
			console.info('form data:');
			var newDaily = {};
			
			var formDataSend = angular.copy(sc.formData);
			
			var timeStart = formDataSend.timeStart;
			if(timeStart) if(timeStart.length == 4) formDataSend.timeStart = timeStart.substring(0,2)+':'+timeStart.substring(2,4);

			var timeEnd = formDataSend.timeEnd;
			if(timeEnd) if(timeEnd.length == 4) formDataSend.timeEnd = timeEnd.substring(0,2)+':'+timeEnd.substring(2,4);
			
			newDaily.Main = formDataSend;
			newDaily.Spendings = sc.tableData2;
			console.log(newDaily);
			newDaily.Main.TypeOper = $cookieStore.get('typeoper');
			newDaily.Main.Date = sc.formData.Date;
			
			console.log(newDaily.Main.Date);
			requests.serverCall.post($http, "SparePartsCtrl/CreateUpdateDailyRepairsAct", newDaily, function (data, status, headers, config) {
				console.log(data);
				var newDateOpen	= data;		
				//если закрытие
				var checkClose = 0;
				if ($cookieStore.get('typeoper') == 'close')
				{
				     for (var i = 0, li = sc.tableData2.length; i < li; i++) {
				//	console.log('asdf');	
			 if (sc.tableData2[i].Employees == null || sc.tableData2[i].Employees.Id == null || sc.tableData2[i].Tasks == null || sc.tableData2[i].Tasks.Id == null
              || sc.tableData2[i].SumCost == 0 || sc.tableData2[i].SumCost == null || sc.tableData2[i].Hours == 0 || sc.tableData2[i].Hours == null) {
				checkClose = 1; // needSelection("Ошибка", "Заполните данные по ежедневному наряду");
				  
			  }
		}
                     if (checkClose!=1) {
					requests.serverCall.post($http, "SparePartsCtrl/CloseDailyRepairsAct", sc.formData.Id, function (data, status, headers, config) {
						console.log('act closed successfully');
						$cookieStore.put('DayAct', null);
					    $location.path('/daily_repairs');
					});
					 }
				}
				
			  if (checkClose != 1 && $cookieStore.get('typeoper') != 'close') { 
				if ($cookieStore.get('checkWriteOff')==1 || $cookieStore.get('checkWriteOff')==3 || $cookieStore.get('checkCopyWriteOff') == 1) {
					if ($cookieStore.get('checkWriteOff') != 3) {
					$cookieStore.put('checkWriteOff', 2);
					}
				$cookieStore.put('newDateOpen', newDateOpen); 
				$cookieStore.put('newDateOpen', sc.formData.Date);
		    	$location.path('/writeoff_spare_parts/');
				} else {
				if ($cookieStore.get('DayAct') != null)
				{
					$cookieStore.put('DayAct', null);
					$location.path('/repairs_spare_parts');
				}
				else
				{
					$cookieStore.put('DayAct', null);
					$location.path('/daily_repairs');
				}
				}
				}
			});
		}
    };


    var getNew;

    //кнопка Отмена
    sc.cancelClick = function () {
		if ($cookieStore.get('checkWriteOff')==1 || $cookieStore.get('checkWriteOff')==3 || ($cookieStore.get('checkCopyWriteOff') == 1)) {
		$cookieStore.put('newDateOpen', null); 	
		$cookieStore.put('checkCopyWriteOff', null); 
			console.log($cookieStore.get('typeoper'));
			if ($cookieStore.get('checkWriteOff')!=3) {
			$cookieStore.put('checkWriteOff', 2);
			}
			$location.path('/writeoff_spare_parts/');
		}
		else {
        $location.path('/daily_repairs');
		}
    };

    sc.sorting = function () {
        var names = [];
        var fullData1 = [];
		filteredData1 =[];
		   for (var i = 0, li = sc.formData.MaterialsList.length; i < li; i++) {
                fullData1.push(sc.formData.MaterialsList[i]);
            }
            for (var i = 0, li = fullData1.length; i < li; i++) {
                names.push(fullData1[i].Material.Name);
            }
            names.sort();
            if (count % 2 == 0) { names.reverse(); }
            for (var i = 0, li = fullData1.length; i < li; i++) {
                for (var j = 0, l1 = fullData1.length; j < l1; j++) {
            if (names[i] == fullData1[j].Material.Name) {filteredData1.push( fullData1[j]);fullData1.splice(j,1); break; }
                }
            }
            sc.tableData = filteredData1;
            count++;
    }
	
}
]);