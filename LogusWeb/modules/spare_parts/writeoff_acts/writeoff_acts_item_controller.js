
var app = angular.module("WriteOffActsItem", ["ngGrid", "services", 'ngCookies', "modalWindows", "LogusConfig","ui.bootstrap",'Directives',"ui.utils"]);
app.controller("WriteOffActsItemCtrl", ["$scope", "$http", "requests", "$location", '$cookieStore', '$rootScope', "modals", "modalType", '$stateParams', 'preloader', "LogusConfig", function ($scope, $http, requests, $location, $cookieStore, $rootScope, modals, modalType, $stateParams, preloader, LogusConfig) {
    var sc = $scope;
    var curUser = $cookieStore.get('currentUser');
    $rootScope.userName = curUser.username;
    $rootScope.exit = 'выход';
    sc.mySelections = [];
	sc.mySelections1 =[];
    sc.tableData = [];
    sc.formData = {};
    sc.infoData = {};

	var count = 1;
	var met=1;
    sc.formData.ActId = $cookieStore.get('act_id');
	console.log(sc.formData.ActId);
    //тип операции с item (передается по клику по кнопке в documents_list)
    sc.typeOperation = $cookieStore.get('typeoper');
		console.log(sc.typeOperation);
		
	if (sc.typeOperation=='create') {	sc.formData.Torg12 = true; }
	 var metka=0;  var m1=0;var m2=0;var m3=0; countWin=0;count=0;
    var name; 
    // свойства таблицы
    sc.gridOptions = {
    data: 'tableData',
    enableColumnResize: true,
    enableRowSelection: true,
    showFooter: false,
    selectedItems: sc.mySelections,
    rowHeight: 49,
    headerRowHeight: 49,
    
   columnDefs: [
    
			{field: "SpareParts", displayName: "Наименование запчасти", width: "20%", cellTemplate: '<a tooltip="{{row.entity[col.field][\'Name\']}}" tooltip-popup-delay="1000" tooltip-class="my-custom-tooltip" tooltip-placement="top" tooltip-append-to-body="true">' +
            '<div class="ngCellText" ng-class="col.colIndex()">' +
            '<input value="{{row.entity[col.field][\'Name\']}}" placeholder="Наименование" style="border: none;"  data-ng-click="updateSelectedCell(col.field, row.entity[col.field][\'Id\'], \'Запчасть\',row.entity,$event)" readonly>' +
            '</div>' +
            '</a>', headerCellTemplate: '<div class="ngHeaderSortColumn {{col.headerClass}}" ng-style="{\'cursor\': col.cursor}" ng-class="{ \'ngSorted\': !noSortVisible }">' +
            '<div ng-click="sorting(); col.sort()"  ng-class="\'colt\' + col.index" class="ngHeaderText" >{{col.displayName}}</div>' +
			'<div class="ngSortButtonDown" ng-show="col.showSortButtonDown()">' +'</div><div class="ngSortButtonUp" ng-show="col.showSortButtonUp()">'},
			
			{field: "VendorCode", displayName: "Артикул", width: "15%", cellTemplate: '<a tooltip="{{row.entity[col.field][\'Name\']}}" tooltip-popup-delay="1000" tooltip-class="my-custom-tooltip" tooltip-placement="top" tooltip-append-to-body="true">' +
            '<div class="ngCellText" ng-class="col.colIndex()">' +
            '<input value="{{row.entity[col.field][\'Name\']}}" placeholder="Артикул" style="border: none;"  data-ng-click="updateSelectedCell(col.field, row.entity[col.field][\'Id\'], \'Артикул\',row.entity,$event)" readonly>' +
            '</div>' +
            '</a>', headerCellTemplate: "<div   ng-class=\"'colt' + col.index\" class=\"ngHeaderText\">{{col.displayName}}</div>"},
			
			{field: "Manufacturer", displayName: "Производитель", width: "15%",
            cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()">' +
            '<input value="{{row.entity[col.field]}}" ng-model="row.entity[col.field]" placeholder="Производитель" class="bordernone" readonly>' +
            '</div>',headerCellTemplate: "<div   ng-class=\"'colt' + col.index\" class=\"ngHeaderText\">{{col.displayName}}</div>"
            },
			{field: "Storage1", displayName: "Склад списания", width: "15%",
            cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()">' +
            '<input value="{{row.entity[col.field]}}" ng-model="row.entity[col.field]" placeholder="Склад списания" class="bordernone" readonly>' +
            '</div>',headerCellTemplate: "<div   ng-class=\"'colt' + col.index\" class=\"ngHeaderText\">{{col.displayName}}</div>"
            },
		    {field: "Count", displayName: "Количество", width: "10%",
            cellTemplate: '<a tooltip="{{row.entity[col.field][\'Name\']}}" tooltip-popup-delay="1000" tooltip-class="my-custom-tooltip" tooltip-placement="top" tooltip-append-to-body="true">' +
            '<div class="ngCellText" ng-class="col.colIndex()">' +
            '<input value="{{row.entity[col.field]}}" ng-model="row.entity[col.field]" placeholder="0" ng-class="{bordernone: row.entity[\'Id\']}"  ng-change="changeCountPrice(row.entity)" readonly>' +
            '</div>' + '</a>',  headerCellTemplate: "<div   ng-class=\"'colt' + col.index\" class=\"ngHeaderText\">{{col.displayName}}</div>"
            },
            {field: "Price", displayName: "Цена", width: "10%",
			 cellTemplate: '<a tooltip="{{row.entity[col.field][\'Name\']}}" tooltip-popup-delay="1000" tooltip-class="my-custom-tooltip" tooltip-placement="top" tooltip-append-to-body="true">' +
             '<div class="ngCellText" ng-class="col.colIndex()">' +
             '<input value="{{row.entity[col.field]}}" ng-model="row.entity[col.field]" placeholder="Цена" ng-class="{bordernone: row.entity[\'Id\']}"   ng-change="changeCountPrice(row.entity)" readonly>' +
             '</div>' +'</a>',  headerCellTemplate: "<div   ng-class=\"'colt' + col.index\" class=\"ngHeaderText\">{{col.displayName}}</div>"},
				
             {field: "Cost1", displayName: "Сумма", width: "15%",
             cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()">' +
             '<input value="{{row.entity[col.field]}}" placeholder="0" style="border: none;" readonly>' +
             '</div>',  headerCellTemplate: "<div   ng-class=\"'colt' + col.index\" class=\"ngHeaderText\">{{col.displayName}}</div>"},
			 /*
			  {field: "Cost2", displayName: "НДС", width: "10%",
            cellTemplate: '<a tooltip="{{row.entity[col.field][\'Name\']}}" tooltip-popup-delay="1000" tooltip-class="my-custom-tooltip" tooltip-placement="top" tooltip-append-to-body="true">' +
            '<div class="ngCellText" ng-class="col.colIndex()">' +
            '<input value="{{row.entity[col.field]}}" ng-model="row.entity[col.field]" placeholder="0" ng-class="{bordernone: row.entity[\'Id\']}" ng-change="changeCountPrice(row.entity)">' +
            '</div>' + '</a>',  headerCellTemplate: "<div   ng-class=\"'colt' + col.index\" class=\"ngHeaderText\">{{col.displayName}}</div>"
            },
			 
			   {field: "Cost3", displayName: "Всего с НДС", width: "15%",
             cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()">' +
             '<input value="{{row.entity[col.field]}}" placeholder="0" style="border: none;" readonly>' +
             '</div>',  headerCellTemplate: "<div   ng-class=\"'colt' + col.index\" class=\"ngHeaderText\">{{col.displayName}}</div>"},
			 */
			 {field: "Id", width: "0%"},
    ],
    afterSelectionChange: function (row) {	
	met = 1;
	for (var i=0; i<row.length;i++ ) 
   { 
     sc.mySelections.push(row[i].entity); 
     console.log(sc.mySelections); 
        } 
		if (sc.mySelections1==row.entity) { console.log('есть');met=2;}
			sc.mySelections1 = row.entity;
	  if (row.entity) {
		  console.log(row.selected);
                if (row.selected) {
                    if (sc.multi) sc.mySelections.push(row.entity);
                    else {
                        sc.mySelections = [];
                        sc.mySelections.push(row.entity);
                    }
                } else {
                    sc.mySelections.splice(sc.tableData.indexOf(row.entity), 1);
                }
            }
	
    },   
    beforeSelectionChange: function (row, event) {
		   if (!sc.multi) {
                angular.forEach(sc.tableData, function (data, index) {
                    if (data.selected == true) sc.mySelections.splice(index, 1);
                    sc.gridOptions.selectRow(index, false);
                });
            }
                return true;
            }
    };    
	
    var _field, _val;
    // выделение строчки по определенному столбцу
    function selectRowByField(field, val) {
        _field = field;
        _val = val;
        sc.$on('ngGridEventData', sc.gridEvDataListener);
    }

    // листнер на событие изменение data в таблице
    sc.gridEvDataListener = function () {
        var offCallMeFn = $scope.$on("ngGridEventData", sc.gridEvDataListener);
        offCallMeFn();
        if (_val == "first" && sc.tableData.length > 0) _val = sc.tableData[0][_field];
		console.log(_val);
        sc.tableData.forEach(function (item, i, arr) {
            if (item[_field] == _val) {
                sc.gridOptions.selectRow(i, true);
                if (sc.gridOptions.ngGrid) sc.gridOptions.ngGrid.$viewport.scrollTop(sc.gridOptions.ngGrid.rowMap[i] * sc.gridOptions.ngGrid.config.rowHeight);
            } else {
                sc.gridOptions.selectRow(i, false);
            }
        });
        $(".ngViewport").focus();
    };
    // получение масива в выпадающие списки
    requests.serverCall.post($http, "SparePartsCtrl/GetDictionarySpareParts", true, function (data, status, headers, config) {
        console.info(data);
        sc.infoData = data;
        console.info(sc.formData);
		
		
        if (sc.typeOperation != 'create') {
            requests.serverCall.post($http, "SparePartsCtrl/GetWriteOffDetails", sc.formData.ActId, function (data, status, headers, config) {
				var id = sc.formData.ActId;
                console.info(data);
                sc.formData = data;
                sc.tableData = data.SparePartsList;
				console.log(sc.formData.ActId);
				sc.formData.ActId = id;
            });
        }
    });
			
    // кнопка Печать
  	sc.printBtn = function() 
	{
		if (sc.formData.ActId) {
            var popupWin = window.open(LogusConfig.serverUrl + 'SparePartsCtrl/GetPrintWriteoffInfo?req=' + sc.formData.ActId +'&act_number='
			                                                                                              + sc.formData.Act_number
                                                                                                          +'&date='+ null,
																										  '_blank');
        } else {
            modals.showWindow(modalType.WIN_MESSAGE, {
                title: "Печать",
                message: "Для печати необходимо сохранить данные",
                callback: function () {

                }
            });
        }
	}



    //кнопка Назад
    sc.cancelClick = function () {
        $location.path('/write_off_acts');
    };

		//сортировка
    sc.sorting = function () {
        var names = [];
        var fullData1 = [];
		filteredData1 =[];
		   for (var i = 0, li = sc.formData.SparePartsList.length; i < li; i++) {
                fullData1.push(sc.formData.SparePartsList[i]);
            }
            for (var i = 0, li = fullData1.length; i < li; i++) {
                names.push(fullData1[i].SpareParts.Name);
            }
            names.sort();
            if (count % 2 == 0) { names.reverse(); }
            for (var i = 0, li = fullData1.length; i < li; i++) {
                for (var j = 0, l1 = fullData1.length; j < l1; j++) {
            if (names[i] == fullData1[j].SpareParts.Name) {filteredData1.push( fullData1[j]);fullData1.splice(j,1); break; }
                }
            }
            sc.tableData = filteredData1;
            count++;
    }
}
]);


 