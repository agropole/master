var app = angular.module("BalanceSpareParts", ["ngGrid", "services", 'ngCookies', "modalWindows"]);
app.controller("BalanceSparePartsCtrl", ["$scope", "$http", "requests",'$cookieStore','$rootScope', "$location", function ($scope, $http, requests, $cookieStore,$rootScope, $location) {
    var sc = $scope;
    sc.infoData = {};
    sc.formData = {};
	 var filteredData = [];
     var count = 1;
    $("#btn").show();
	 $cookieStore.put('dateDailyCalendar', {date: null});
	 $cookieStore.put('dateEndDailyCalendar', {date: null});
	 $cookieStore.put('filterTextDemand', null);
	if ($cookieStore.get('checkHistoryLoc')=='invoice') {
	$cookieStore.put('checkHistoryLoc', null);
	$rootScope.selectedTab = 'Приходные накладные';
     $location.path('/spare_parts/');	
	}
	if ($cookieStore.get('checkHistoryLoc')=='repairs') {
	$cookieStore.put('checkHistoryLoc', null);
	$rootScope.selectedTab = 'Наряды на ремонт';
    $location.path('/writeoff_spare_parts/');
	}
	if ($cookieStore.get('checkHistoryLoc')=='writeOff') {
	$cookieStore.put('checkHistoryLoc', null);
	$rootScope.selectedTab = 'Акты списания';
     $location.path('/write_off_acts/');	
	}
    sc.selectedRow = [];
    sc.gridOptions = {
        enableColumnResize: false,
        enableRowSelection: false,
        showFooter: false,
        rowHeight: 49,
        headerRowHeight: 49,
        useExternalSorting: true,
        data: 'TableData',
        selectedItems: sc.selectedRow,
        afterSelectionChange: function (row) {
            console.info(sc.selectedRow);
            if (row.entity) {
                if (row.selected) {
                    sc.selectedRow = [row];
                } else {
                    if (sc.selectedRow && sc.selectedRow > 0) {
                        sc.selectedRow.splice(sc.tableDataFirst.indexOf(row.entity), 1);
                    }
                }
            }
        },
        beforeSelectionChange: function (row, event) {
            angular.forEach(sc.TCData, function (data, index) {
                if (data.selected == true) sc.selectedRow.splice(index, 1);
                sc.gridOptions.selectRow(index, false);
            });
            return true;
        }
    };
    sc.gridOptions.columnDefs = [
        {
         field: "SpareParts1", displayName: "Наименование", width: "17%", cellClass: 'cellToolTip',
         cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()">' +
        '<input value="{{row.entity[col.field]}}" ng-model="row.entity[col.field]" placeholder="Тип" class="bordernone" readonly>' +
        '</div>', headerCellTemplate: '<div class="ngHeaderSortColumn {{col.headerClass}}" ng-style="{\'cursor\': col.cursor}" ng-class="{ \'ngSorted\': !noSortVisible }">' +
        '<div ng-click="sorting(); col.sort()"  ng-class="\'colt\' + col.index" class="ngHeaderText" >{{col.displayName}}</div>' +
        '<div class="ngSortButtonDown" ng-show="col.showSortButtonDown()">' +
        '</div><div class="ngSortButtonUp" ng-show="col.showSortButtonUp()">'
        },
		{  field: "VendorCode1", displayName: "Артикул", width: "8%",
          cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()">' +
            '<input value="{{row.entity[col.field]}}" ng-model="row.entity[col.field]" placeholder="Артикул" class="bordernone" readonly>' +
            '</div>',headerCellTemplate: "<div   ng-class=\"'colt' + col.index\" class=\"ngHeaderText\">{{col.displayName}}</div>"
        },
        {
            field: "Contractor", displayName: "Производитель", width: "13%",
            cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()">' +
            '<input value="{{row.entity[col.field]}}" ng-model="row.entity[col.field]" placeholder="Тип" class="bordernone" readonly>' +
            '</div>',headerCellTemplate: "<div   ng-class=\"'colt' + col.index\" class=\"ngHeaderText\">{{col.displayName}}</div>"
        },
		   {
            field: "Storage1", displayName: "Склад оприходования", width: "18%",
          cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()">' +
            '<input value="{{row.entity[col.field]}}" ng-model="row.entity[col.field]" placeholder="Склад" class="bordernone" readonly>' +
            '</div>',headerCellTemplate: "<div   ng-class=\"'colt' + col.index\" class=\"ngHeaderText\">{{col.displayName}}</div>"
        },
        {
            field: "Count", displayName: "Остаток", width: "8%",
              cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()">' +
            '<input value="{{row.entity[col.field]}}" ng-model="row.entity[col.field]" placeholder="Баланс" class="bordernone" readonly>' +
            '</div>',headerCellTemplate: "<div   ng-class=\"'colt' + col.index\" class=\"ngHeaderText\">{{col.displayName}}</div>"
        },
		 {
            field: "Storage2", displayName: "На складе", width: "9%",
              cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()">' +
            '<input value="{{row.entity[col.field]}}" ng-model="row.entity[col.field]" placeholder="0" class="bordernone" readonly>' +
            '</div>',headerCellTemplate: "<div   ng-class=\"'colt' + col.index\" class=\"ngHeaderText\">{{col.displayName}}</div>"
        },
		 {
            field: "Reserve", displayName: "В резерве", width: "10%",
              cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()">' +
            '<input value="{{row.entity[col.field]}}" ng-model="row.entity[col.field]" placeholder="0" class="bordernone" readonly>' +
            '</div>',headerCellTemplate: "<div   ng-class=\"'colt' + col.index\" class=\"ngHeaderText\">{{col.displayName}}</div>"
        },
        {
            field: "Price", displayName: "Цена, р/ед", width: "10%",
             cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()">' +
            '<input value="{{row.entity[col.field]}}" ng-model="row.entity[col.field]" placeholder="Цена" class="bordernone" readonly>' +
            '</div>',headerCellTemplate: "<div   ng-class=\"'colt' + col.index\" class=\"ngHeaderText\">{{col.displayName}}</div>"
        },
		 {
            field: "Cost3", displayName: "Сумма", width: "9%",
             cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()">' +
            '<input value="{{row.entity[col.field]}}" ng-model="row.entity[col.field]" placeholder="Цена" class="bordernone" readonly>' +
            '</div>',headerCellTemplate: "<div   ng-class=\"'colt' + col.index\" class=\"ngHeaderText\">{{col.displayName}}</div>"
        },
      
        {field: "Id", width: "0%"}
    ];
    
    var fullData; var fullData1;
    var loadTypeList = function() {
		//вывод таблицы
        requests.serverCall.post($http, "SparePartsCtrl/GetBalanceSpareList", 0, function (data, status, headers, config) {
            sc.TableData = fullData = fullData1 = data;
        }, function () {
	}); 

    };
   loadTypeList();
    sc.openIncomingList = function() {
        var url = '/coming_spare_parts';
        $location.path(url);
    };

	
    sc.openWriteOffList = function() {	
        var url = '/writeoff_spare_parts';
        $location.path(url);	
    };

    // перемещение материалов
	  sc.openMovementList = function() {
       var url = '/movement_spare_parts';
        $location.path(url);
    };
	//резерв
		  sc.openReserveList = function() {
       var url = '/reserve_spare_parts';
        $location.path(url);
    };
	
    sc.refreshTable = function() {
		console.log(sc.infoData.MatId);
    	loadTypeList(sc.infoData.MatId);
    };
    sc.search = function() {
        if (!sc.filterText) {
            sc.TableData = fullData;    
        } else {
             filteredData = [];
            for (var i=0, li=fullData.length; i<li; i++) {
                var d = fullData[i];
                var text = '';
                for (var p in d) {
                    text += d[p];
                }
                if (text.toLowerCase().indexOf(sc.filterText.toLowerCase()) != -1) {
                    filteredData.push(fullData[i]);
                }
            }
            sc.TableData = filteredData;
        }
    }
	//сортировка
    sc.sorting = function () {
        var names = [];
        var filteredData1 = [];
        if (!sc.filterText) {
            fullData1 = [];
		
            for (var i = 0, li = fullData.length; i < li; i++) {
                fullData1.push(fullData[i]);
            };
			
        }
        if (sc.filterText) {
          fullData1 = [];
            for (var i = 0, li = filteredData.length; i < li; i++) {
                fullData1.push(filteredData[i]);
            };
        } 
            for (var i = 0, li = fullData1.length; i < li; i++) {
                names.push(fullData1[i].Name);
            }
            names.sort();
            if (count % 2 == 0) { names.reverse(); }
            for (var i = 0, li = fullData1.length; i < li; i++) {
                for (var j = 0, l1 = fullData1.length; j < l1; j++) {
                    if (names[i] == fullData1[j].Name) {filteredData1.push(fullData1[j]);fullData1.splice(j,1); break; }
                }
            }
            sc.TableData = filteredData1;
            count++;
    }
	
	
	
	

}]);