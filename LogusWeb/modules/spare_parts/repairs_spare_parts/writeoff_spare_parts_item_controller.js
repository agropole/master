
var app = angular.module("WriteOffSparePartsItem", ["ngGrid", "services", 'ngCookies', "modalWindows", "LogusConfig","ui.bootstrap",'Directives',"ui.utils"]);
app.controller("WriteOffSparePartsItemCtrl", ["$scope", "$http", "requests", "$location", '$cookieStore', '$rootScope', "modals", "modalType", '$stateParams', 'preloader', "LogusConfig", function ($scope, $http, requests, $location, $cookieStore, $rootScope, modals, modalType, $stateParams, preloader, LogusConfig) {
    var sc = $scope;
    var curUser = $cookieStore.get('currentUser');
    $rootScope.userName = curUser.username;
	console.log(curUser.roleid);
    $rootScope.exit = 'выход';
    sc.mySelections = [];
	sc.mySelections1 = [];
	sc.mySelections2 = [];
	sc.mySelections3 = [];
	sc.mySelections4 = [];
	sc.mySelections5 = [];
    sc.tableData = [];
    sc.tableData2 = [];
    sc.tableData3 = [];
	sc.tableData4 = [];
	sc.tableData5 = [];
    sc.formData = {};
    sc.infoData = {};
    var	checkCopy = 0;
	var countEquip = 0;
    var rowsForUpd = [];
	sc.detailTab = true;
	sc.multi = false;
	var met=1;
    var m1=0; count=0;
    sc.formData.ActId = $cookieStore.get('repair_id');
	console.log(sc.formData.ActId);
    //тип операции с item (передается по клику по кнопке в documents_list)
    sc.typeOperation = $cookieStore.get('tOperation');
		console.log(sc.typeOperation);
	if (sc.typeOperation=='edit' || sc.typeOperation=='create') {
		sc.NameButtom = 'Сохранить';
		sc.detailTab = false;
		sc.formData.WriteOff = false;
	} else {
	sc.NameButtom='Закрыть';	
	sc.formData.WriteOff = true;
	console.log(sc.formData.WriteOff);
	}
	

	
    // свойства таблицы1
    sc.gridOptions = {
    data: 'tableData',
    enableColumnResize: true,
    enableRowSelection: true,
    showFooter: false,
    selectedItems: sc.mySelections,
    rowHeight: 49,
    headerRowHeight: 49,
    multiSelect : false ,
    columnDefs: [
			{field: "Number", displayName: "№ п/п", width: "10%",
            cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()">' +
            '<input value="{{row.entity[col.field]}}" ng-model="row.entity[col.field]" placeholder="№ п/п" class="bordernone" readonly>' +
            '</div>',headerCellTemplate: "<div   ng-class=\"'colt' + col.index\" class=\"ngHeaderText\">{{col.displayName}}</div>"
            },
			
		    {field: "Malfunction", displayName: "Предполагаемая неисправность", width: "90%",
            cellTemplate: '<a tooltip="{{row.entity[col.field][\'Name\']}}" tooltip-popup-delay="1000" tooltip-class="my-custom-tooltip" tooltip-placement="top" tooltip-append-to-body="true">' +
            '<div class="ngCellText" ng-class="col.colIndex()">' +
            '<input value="{{row.entity[col.field]}}" ng-model="row.entity[col.field]" placeholder="" ng-class="{bordernone: row.entity[\'Id\']}"  ng-change="changeCountPrice(row.entity)">' +
            '</div>' + '</a>',  headerCellTemplate: "<div   ng-class=\"'colt' + col.index\" class=\"ngHeaderText\">{{col.displayName}}</div>"
            },
				
			 {field: "Id", width: "0%"},
    ],
    afterSelectionChange: function (row) {	
	met =1;
	for (var i = 0; i<row.length;i++ ) 
   { 
     sc.mySelections.push(row[i].entity); 
     console.log(sc.mySelections); 
        } 
		if (sc.mySelections1==row.entity) { console.log('есть');met=2;}
			sc.mySelections1 = row.entity;
	  if (row.entity) {
		  console.log(row.selected);
                if (row.selected) {
                    if (sc.multi) sc.mySelections.push(row.entity);
                    else {
                        sc.mySelections = [];
                        sc.mySelections.push(row.entity);
                    }
                } else {
                    sc.mySelections.splice(sc.tableData.indexOf(row.entity), 1);
                }
            }
	
    },   
    beforeSelectionChange: function (row, event) {
		   if (!sc.multi) {
                angular.forEach(sc.tableData, function (data, index) {
                    if (data.selected == true) sc.mySelections.splice(index, 1);
                    sc.gridOptions.selectRow(index, false);

                });
            }
		
                return true;
            }
    };    
	//таблица2
 sc.gridOptions2 = {
    data: 'tableData2',
    enableColumnResize: true,
    enableRowSelection: true,
    showFooter: false,
    selectedItems: sc.mySelections2,
    rowHeight: 49,
    headerRowHeight: 49,
	 multiSelect : false ,
    
    columnDefs: [
    
		    {field: "Number", displayName: "№ п/п", width: "10%",
            cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()">' +
            '<input value="{{row.entity[col.field]}}" ng-model="row.entity[col.field]" placeholder="№ п/п" class="bordernone" readonly>' +
            '</div>',headerCellTemplate: "<div   ng-class=\"'colt' + col.index\" class=\"ngHeaderText\">{{col.displayName}}</div>"
            },
			
		    {field: "Malfunction", displayName: "Неисправность", width: "50%",
            cellTemplate: '<a tooltip="{{row.entity[col.field][\'Name\']}}" tooltip-popup-delay="1000" tooltip-class="my-custom-tooltip" tooltip-placement="top" tooltip-append-to-body="true">' +
            '<div class="ngCellText" ng-class="col.colIndex()">' +
            '<input value="{{row.entity[col.field]}}" ng-model="row.entity[col.field]" placeholder="" ng-class="{bordernone: row.entity[\'Id\']}"  ng-change="changeCountPrice(row.entity)">' +
            '</div>' + '</a>',  headerCellTemplate: "<div   ng-class=\"'colt' + col.index\" class=\"ngHeaderText\">{{col.displayName}}</div>"
            },
			 {field: "TypeTask", displayName: "Наименование работ", width: "40%", cellTemplate: '<a tooltip="{{row.entity[col.field][\'Name\']}}" tooltip-popup-delay="1000" tooltip-class="my-custom-tooltip" tooltip-placement="top" tooltip-append-to-body="true">' +
            '<div class="ngCellText" ng-class="col.colIndex()">' +
            '<input value="{{row.entity[col.field][\'Name\']}}" placeholder="Наименование" style="border: none;"  data-ng-click="updateSelectedCell(col.field, row.entity[col.field][\'Id\'], \'Наименование\',row.entity,$event)" readonly>' +
            '</div>' +
            '</a>', headerCellTemplate: "<div   ng-class=\"'colt' + col.index\" class=\"ngHeaderText\">{{col.displayName}}</div>"},
			 {field: "Id", width: "0%"},
    ],
    afterSelectionChange: function (row) {	
	met = 1;
	for (var i=0; i<row.length;i++ ) 
   { 
     sc.mySelections2.push(row[i].entity); 
     console.log(sc.mySelections2); 
        } 
		if (sc.mySelections1==row.entity) { console.log('есть');met=2;}
			sc.mySelections1 = row.entity;
	  if (row.entity) {
		  console.log(row.selected);
                if (row.selected) {
                    if (sc.multi) sc.mySelections2.push(row.entity);
                    else {
                        sc.mySelections2 = [];
                        sc.mySelections2.push(row.entity);
                    }
                } else {
                    sc.mySelections2.splice(sc.tableData2.indexOf(row.entity), 1);
                }
            }
	
    },   
    beforeSelectionChange: function (row, event) {
		   if (!sc.multi) {
                angular.forEach(sc.tableData2, function (data, index) {
                    if (data.selected == true) sc.mySelections2.splice(index, 1);
                    sc.gridOptions2.selectRow(index, false);

                });
            }
                return true;
            }
    };
	
	sc.columnDefs = [];
	sc.updateColumnDefs = function(width1,width2) {
	sc.columnDefs = [
    
		    {field: "Number", displayName: "№ п/п", width: "6%",
            cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()">' +
            '<input value="{{row.entity[col.field]}}" ng-model="row.entity[col.field]" placeholder="№ п/п" class="bordernone" readonly>' +
            '</div>',headerCellTemplate: "<div   ng-class=\"'colt' + col.index\" class=\"ngHeaderText\">{{col.displayName}}</div>"},
			
			{field: "Date",   ////{{getClassDateCell(row)}}  ngDateText
            displayName: "Дата",
            width: "8%",
            cellTemplate:'<div ><a tooltip="{{row.getProperty(col.field)}}" tooltip-popup-delay="1000" tooltip-class="my-custom-tooltip" tooltip-placement="top" tooltip-append-to-body="true">' +
            '<datepicktable id="{{row.entity.Number}}" class="{{getClassDateCell(row)}} logus-headers col-md-2 col-xs-4 col-sm-2 col-lg-2" style="padding-top: 6px;margin-left: -22px;margin-top: 2px;font-size: 15px;" data="row.entity[col.field]" mask="\'99.99.9999\'" regex-name="\'date\'"  date-change="dateSpareChange(row.entity)">' +
            '</datepicktable>' +
            '</a></div>'},
			
		    {field: "SpareParts", displayName: "Наименование", width: "19%", cellTemplate: '<a tooltip="{{row.entity[col.field][\'Name\']}}" tooltip-popup-delay="1000" tooltip-class="my-custom-tooltip" tooltip-placement="top" tooltip-append-to-body="true">' +
            '<div class="ngCellText" ng-class="col.colIndex()">' +
            '<input value="{{row.entity[col.field][\'Name\']}}" placeholder="Наименование" style="border: none;"  data-ng-click="updateSelectedCell(col.field, row.entity[col.field][\'Id\'], \'Наименование\',row.entity,$event)" readonly>' +
            '</div>' +
            '</a>', headerCellTemplate: "<div   ng-class=\"'colt' + col.index\" class=\"ngHeaderText\">{{col.displayName}}</div>"},
			
			  {field: "VendorCode", displayName: "Артикул", width: "15%", cellTemplate: '<a tooltip="{{row.entity[col.field][\'Name\']}}" tooltip-popup-delay="1000" tooltip-class="my-custom-tooltip" tooltip-placement="top" tooltip-append-to-body="true">' +
            '<div class="ngCellText" ng-class="col.colIndex()">' +
            '<input value="{{row.entity[col.field][\'Name\']}}" placeholder="Артикул" style="border: none;"  data-ng-click="updateSelectedCell(col.field, row.entity[col.field][\'Id\'], \'Наименование\',row.entity,$event)" readonly>' +
            '</div>' +
            '</a>', headerCellTemplate: "<div   ng-class=\"'colt' + col.index\" class=\"ngHeaderText\">{{col.displayName}}</div>"},
			
			  {field: "Storage", displayName: "Склад", width: "10%", cellTemplate: '<a tooltip="{{row.entity[col.field][\'Name\']}}" tooltip-popup-delay="1000" tooltip-class="my-custom-tooltip" tooltip-placement="top" tooltip-append-to-body="true">' +
            '<div class="ngCellText" ng-class="col.colIndex()">' +
            '<input value="{{row.entity[col.field][\'Name\']}}" placeholder="Склад" style="border: none;"  data-ng-click="updateSelectedCell(col.field, row.entity[col.field][\'Id\'], \'Наименование\',row.entity,$event)" readonly>' +
            '</div>' +
            '</a>', headerCellTemplate: "<div   ng-class=\"'colt' + col.index\" class=\"ngHeaderText\">{{col.displayName}}</div>"},

			 {field: "Available", displayName: "Доступно", width: width1,
              cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()">' +
             '<input value="{{row.entity[col.field]}}" placeholder="0" style="border: none;" readonly>' +
             '</div>',  headerCellTemplate: "<div   ng-class=\"'colt' + col.index\" class=\"ngHeaderText\">{{col.displayName}}</div>"},
			
			{field: "Reserve", displayName: "В резерве", width: width2,
            cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()">' +
             '<input value="{{row.entity[col.field]}}" placeholder="0" style="border: none;" readonly>' +
             '</div>',  headerCellTemplate: "<div   ng-class=\"'colt' + col.index\" class=\"ngHeaderText\">{{col.displayName}}</div>"},
			
			 {field: "Count", displayName: "Количество", width: "12%",
            cellTemplate: '<a tooltip="{{row.entity[col.field][\'Name\']}}" tooltip-popup-delay="1000" tooltip-class="my-custom-tooltip" tooltip-placement="top" tooltip-append-to-body="true">' +
            '<div class="ngCellText" ng-class="col.colIndex()">' +
            '<input value="{{row.entity[col.field]}}" ng-model="row.entity[col.field]" placeholder="0" ng-class="{bordernone: row.entity[\'Id\']}" ng-change="changeCountPrice(row.entity)">' +
            '</div>' + '</a>',  headerCellTemplate: "<div   ng-class=\"'colt' + col.index\" class=\"ngHeaderText\">{{col.displayName}}</div>"},
			 {field: "Id", width: "0%"},
		     {field: "Balance", width: "0%"},
			 {field: "Reserve2", width: "0%"},
			 {field: "Cost", width: "0%"},
    ];
	}
	sc.updateColumnDefs("15%","15%");
	console.log(sc.columnDefs);
	
	//таблица 3 - запчасти
	 sc.gridOptions3 = {
    data: 'tableData3',
    enableColumnResize: true,
    enableRowSelection: true,
    showFooter: false,
    selectedItems: sc.mySelections3,
    rowHeight: 49,
    headerRowHeight: 49,
     multiSelect : false ,
    columnDefs: 'columnDefs',
    afterSelectionChange: function (row) {	
	met = 1;
	for (var i = 0; i<row.length;i++ ) 
   { 
     sc.mySelections3.push(row[i].entity); 
     console.log(sc.mySelections3); 
        } 
		if (sc.mySelections1==row.entity) { console.log('есть');met = 2;}
			sc.mySelections1 = row.entity;
	  if (row.entity) {
		  console.log(row.selected);
                if (row.selected) {
                    if (sc.multi) sc.mySelections3.push(row.entity);
                    else {
                        sc.mySelections3 = [];
                        sc.mySelections3.push(row.entity);
                    }
                } else {
                    sc.mySelections3.splice(sc.tableData3.indexOf(row.entity), 1);
                }
            }
	
    },   
    beforeSelectionChange: function (row, event) {
		   if (!sc.multi) {
                angular.forEach(sc.tableData3, function (data, index) {
                    if (data.selected == true) sc.mySelections3.splice(index, 1);
                    sc.gridOptions3.selectRow(index, false);

                });
            }
		
                return true;
            }
    };
	// свойства таблицы4 - ежедневные наряды
    sc.gridOptions4 = {
    data: 'tableData4',
    enableColumnResize: true,
    enableRowSelection: true,
    showFooter: false,
    selectedItems: sc.mySelections4,
    rowHeight: 49,
    headerRowHeight: 49,
    multiSelect : false ,
    columnDefs: [
    
			{field: "Number", displayName: "№", width: "9%",
            cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()">' +
            '<input value="{{row.entity[col.field]}}" ng-model="row.entity[col.field]" placeholder="№ п/п" class="bordernone" readonly>' +
            '</div>',headerCellTemplate: "<div   ng-class=\"'colt' + col.index\" class=\"ngHeaderText\">{{col.displayName}}</div>"},
			
			{field: "Date1",displayName: "Дата",width: "10%",  
            cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()">' +
             '<input value="{{row.entity[col.field]}}" placeholder="0" style="border: none;" readonly>' +
             '</div>', headerCellTemplate: "<div   ng-class=\"'colt' + col.index\" class=\"ngHeaderText\">{{col.displayName}}</div>"},
			 
		    {field: "Hours", displayName: "Отработано часов", width: "19%",
              cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()">' +
             '<input value="{{row.entity[col.field]}}" placeholder="0" style="border: none;" readonly>' +
             '</div>',  headerCellTemplate: "<div   ng-class=\"'colt' + col.index\" class=\"ngHeaderText\">{{col.displayName}}</div>"},
			 
			{field: "SumCost", displayName: "Итого начислено", width: "30%",
            cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()">' +
            '<input value="{{row.entity[col.field]}}" ng-model="row.entity[col.field]" placeholder="Итого начислено" class="bordernone" readonly  ng-change="changeHours(row.entity)">' +
            '</div>',headerCellTemplate: "<div   ng-class=\"'colt' + col.index\" class=\"ngHeaderText\">{{col.displayName}}</div>"},

		    {field: "Status", displayName: "Статус", width: "20%",
            cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()">' +
            '<input value="{{row.entity[col.field]}}" ng-model="row.entity[col.field]" placeholder="Статус" class="bordernone" readonly  ng-change="changeHours(row.entity)">' +
            '</div>',headerCellTemplate: "<div   ng-class=\"'colt' + col.index\" class=\"ngHeaderText\">{{col.displayName}}</div>"},
			
			{field: "", displayName: " ", width:"12%",
            cellTemplate: '<div class="{{getClassTempCell(row.entity.TemplateStatus,row)}} ngCellText {{getClassCopyCell(row.entity.Status,row)}}"><button type="button"  class="logus-btn btn btn-border ng-click-active" style="width: 100%;margin-top: 8px" ng-click="copyDailyAct(row.entity)">Копировать</button></div>',
                  },
				  
		   {field: "RateShift", displayName: "Тариф", width: "0%",
            cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()">' +
            '<input value="{{row.entity[col.field]}}" ng-model="row.entity[col.field]" placeholder="Тариф" class="bordernone" readonly  ng-change="changeHours(row.entity)">' +
            '</div>',headerCellTemplate: "<div   ng-class=\"'colt' + col.index\" class=\"ngHeaderText\">{{col.displayName}}</div>"
            },
			{field: "RatePiecework", displayName: "Расценка", width: "0%",
            cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()">' +
            '<input value="{{row.entity[col.field]}}" ng-model="row.entity[col.field]" placeholder="Расценка" class="bordernone" readonly  ng-change="changeHours(row.entity)">' +
            '</div>',headerCellTemplate: "<div   ng-class=\"'colt' + col.index\" class=\"ngHeaderText\">{{col.displayName}}</div>"
            },
			 {field: "Id", width: "0%"},
			 {field: "DailyAct", width: "0%"},
    ],
    afterSelectionChange: function (row) {	
	met =1;
	for (var i = 0; i < row.length; i++ ) 
   { 
     sc.mySelections4.push(row[i].entity); 
     console.log(sc.mySelections4); 
        } 
		if (sc.mySelections1==row.entity) { console.log('есть');met = 2;}
			sc.mySelections1 = row.entity;
	  if (row.entity) {
		  console.log(row.selected);
                if (row.selected) {
                    if (sc.multi) sc.mySelections4.push(row.entity.Id);
                    else {
                        sc.mySelections4 = [];
                        sc.mySelections4.push(row.entity);
                    }
                } else {
                    sc.mySelections4.splice(sc.tableData.indexOf(row.entity), 1);
                }
            }
    },   
    beforeSelectionChange: function (row, event) {
		//переход в ЕН	
		console.log(row.entity);
		console.log(checkCopy);
		if (checkCopy!=1) {
	$cookieStore.put('typeoper', 'edit');	
	$cookieStore.put('DayAct', row.entity.DailyAct);	
	$cookieStore.put('act_id', row.entity.DailyAct);
	//записываем все данные в куки
	$.session.set("formDataWriteOff",JSON.stringify(sc.formData));
	$.session.set("tableDataWriteOff",JSON.stringify(sc.tableData));
	$.session.set("tableData2WriteOff",JSON.stringify(sc.tableData2));
	$.session.set("tableData3WriteOff",JSON.stringify(sc.tableData3));
	$.session.set("tableData5WriteOff",JSON.stringify(sc.tableData5));
	$cookieStore.put('checkWriteOff', 3);
	$location.path('/daily_repairs_item/');
		}
		   if (!sc.multi) {
                angular.forEach(sc.tableData4, function (data, index) {
                    if (data.selected == true) sc.mySelections4.splice(index, 1);
                    sc.gridOptions4.selectRow(index, false);

                });
            }
                return true;
            }
    }; 
	
	sc.dateSpareChange = function(row) {	
	 var start = new Date(sc.formData.dateStart);
		console.log(row);
		console.log(row.Date);
		if (start && row.Date) {
		row.Date.setHours(15,0,0,0);
		start.setHours(15,0,0,0);
		console.log(row.Date);
		console.log(start);
		var id = "#"+row.Number;
		console.log(id);
		if (row.Date.getTime() < start.getTime()) {
			console.log('плохая дата');
			$(id).addClass("ngDateText");
		} else {
			console.log('хорошая дата');
			$(id).removeClass("ngDateText");
		}
	}
	}
	
	//копировать строку из ЕН
	sc.copyDailyAct = function(row) {
       checkCopy = 1;
	   console.log(row);
	  $.session.set("tableData4WriteOff",JSON.stringify(row));
	  	$.session.set("formDataWriteOff",JSON.stringify(sc.formData));
	$.session.set("tableDataWriteOff",JSON.stringify(sc.tableData));
	$.session.set("tableData2WriteOff",JSON.stringify(sc.tableData2));
	$.session.set("tableData3WriteOff",JSON.stringify(sc.tableData3));
	$.session.set("tableData5WriteOff",JSON.stringify(sc.tableData5));
	   $cookieStore.put('checkCopyWriteOff', 1);
	   $cookieStore.put('typeoper', 'create');
	   $cookieStore.put('DayAct', null);	
	   $cookieStore.put('act_id', null);
	   $location.path('/daily_repairs_item/');
	}
		
	//таблица5 - услуги
    sc.gridOptions5 = {
    data: 'tableData5',
    enableColumnResize: true,
    enableRowSelection: true,
    showFooter: false,
    selectedItems: sc.mySelections5,
    rowHeight: 49,
    headerRowHeight: 49,
    multiSelect : false ,
    columnDefs: [
           {field: "Number", displayName: "№ п/п", width: "10%",
           cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()">' +
             '<input value="{{row.entity[col.field]}}" placeholder="0" style="border: none;" readonly>' +
             '</div>',  headerCellTemplate: "<div   ng-class=\"'colt' + col.index\" class=\"ngHeaderText\">{{col.displayName}}</div>"},
			
			{field: "NameService", displayName: "Наименование услуги", width: "35%",
           cellTemplate: '<a tooltip="{{row.entity[col.field][\'Name\']}}" tooltip-popup-delay="1000" tooltip-class="my-custom-tooltip" tooltip-placement="top" tooltip-append-to-body="true">' +
            '<div class="ngCellText" ng-class="col.colIndex()">' +
            '<input value="{{row.entity[col.field]}}" ng-model="row.entity[col.field]" placeholder="" ng-class="{bordernone: row.entity[\'Id\']}"  ng-change="changeServiceTotal(row.entity)">' +
            '</div>' + '</a>',  headerCellTemplate: "<div   ng-class=\"'colt' + col.index\" class=\"ngHeaderText\">{{col.displayName}}</div>"
			},
			
		    {field: "Count", displayName: "Количество", width: "10%",
            cellTemplate: '<a tooltip="{{row.entity[col.field][\'Name\']}}" tooltip-popup-delay="1000" tooltip-class="my-custom-tooltip" tooltip-placement="top" tooltip-append-to-body="true">' +
            '<div class="ngCellText" ng-class="col.colIndex()">' +
            '<input value="{{row.entity[col.field]}}" ng-model="row.entity[col.field]" placeholder="0" ng-class="{bordernone: row.entity[\'Id\']}" ng-change="changeServiceTotal(row.entity)">' +
            '</div>' + '</a>',  headerCellTemplate: "<div   ng-class=\"'colt' + col.index\" class=\"ngHeaderText\">{{col.displayName}}</div>"}, 
			
		    {field: "Price", displayName: "Цена", width: "10%",
            cellTemplate: '<a tooltip="{{row.entity[col.field][\'Name\']}}" tooltip-popup-delay="1000" tooltip-class="my-custom-tooltip" tooltip-placement="top" tooltip-append-to-body="true">' +
            '<div class="ngCellText" ng-class="col.colIndex()">' +
            '<input value="{{row.entity[col.field]}}" ng-model="row.entity[col.field]" placeholder="0" ng-class="{bordernone: row.entity[\'Id\']}" ng-change="changeServiceTotal(row.entity)">' +
            '</div>' + '</a>',  headerCellTemplate: "<div   ng-class=\"'colt' + col.index\" class=\"ngHeaderText\">{{col.displayName}}</div>"},
			 
		    {field: "Cost1", displayName: "Сумма без НДС", width: "15%",
             cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()">' +
             '<input value="{{row.entity[col.field]}}" placeholder="0" style="border: none;" readonly>' +
             '</div>',  headerCellTemplate: "<div   ng-class=\"'colt' + col.index\" class=\"ngHeaderText\">{{col.displayName}}</div>"},
			 
			{field: "Cost2", displayName: "НДС", width: "8%",
            cellTemplate: '<a tooltip="{{row.entity[col.field][\'Name\']}}" tooltip-popup-delay="1000" tooltip-class="my-custom-tooltip" tooltip-placement="top" tooltip-append-to-body="true">' +
            '<div class="ngCellText" ng-class="col.colIndex()">' +
            '<input value="{{row.entity[col.field]}}" ng-model="row.entity[col.field]" placeholder="0" ng-class="{bordernone: row.entity[\'Id\']}" ng-change="changeServiceTotal(row.entity)">' +
            '</div>' + '</a>',  headerCellTemplate: "<div   ng-class=\"'colt' + col.index\" class=\"ngHeaderText\">{{col.displayName}}</div>"
            },
			{field: "Cost3", displayName: "Всего с НДС", width: "22%",
             cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()">' +
             '<input value="{{row.entity[col.field]}}" placeholder="0" style="border: none;" readonly>' +
             '</div>',  headerCellTemplate: "<div   ng-class=\"'colt' + col.index\" class=\"ngHeaderText\">{{col.displayName}}</div>"},
			 {field: "Id", width: "0%"},
    ],
    afterSelectionChange: function (row) {	
	met =1;
	for (var i=0; i<row.length;i++ ) 
   { 
     sc.mySelections5.push(row[i].entity); 
     console.log(sc.mySelections5); 
        } 
		if (sc.mySelections1==row.entity) { console.log('есть');met=2;}
			sc.mySelections1 = row.entity;
	  if (row.entity) {
		  console.log(row.selected);
                if (row.selected) {
                    if (sc.multi) sc.mySelections5.push(row.entity);
                    else {
                        sc.mySelections5 = [];
                        sc.mySelections5.push(row.entity);
                    }
                } else {
                    sc.mySelections5.splice(sc.tableData5.indexOf(row.entity), 1);
                }
            }
	
    },   
    beforeSelectionChange: function (row, event) {
		   if (!sc.multi) {
                angular.forEach(sc.tableData5, function (data, index) {
                    if (data.selected == true) sc.mySelections4.splice(index, 1);
                    sc.gridOptions5.selectRow(index, false);
                });
            }
                return true;
            }
    }; 
	
    var _field, _val;
    // выделение строчки по определенному столбцу
    function selectRowByField(field, val) {
        _field = field;
        _val = val;
        sc.$on('ngGridEventData', sc.gridEvDataListener);
    }
	   if (sc.typeOperation == 'close') {
		   sc.updateColumnDefs("10%","10%");
           sc.columnDefs.push({width:"10%",displayName:"Списать",field: "WriteOff",
		   cellTemplate: '<input name="CloseChack" type="checkbox" style="margin-left: 10px;margin-top: 18px;" value="{{row.entity[col.field]}}" ng-model="row.entity[col.field]" placeholder="">',
		   headerCellTemplate: "<div   ng-class=\"'colt' + col.index\" class=\"ngHeaderText\">{{col.displayName}}</div>"});
	   }
	   
	   

   if (sc.typeOperation == 'create') {
    sc.formData.IsDaily = false;
	sc.formData.IsRequire = false;
	sc.detailTab = false;
	sc.formData.Plan1 = false;
	sc.formData.Plan2 = false;
	sc.formData.Plan3 = false;
	sc.formData.Plan4 = false;
	sc.checkDaily = 'No';  
       }
	   
	sc.changeCountPrice = function(row) {
		
		/*
		 row.Count = row.Count.toString().replace(",", "."); 
		 var count =  (row.Count == null || row.Count=="") ? 0 : row.Count;
		 var res = Math.round((parseFloat(row.Reserve2) + parseFloat(count)) * 1000) / 1000;
		 console.log(row.Reserve2);
		 console.log(count);
		 row.Reserve = getDecimal(res) == 0 ? Math.round(res) : res;           //резерв
		 var ava = Math.round((parseFloat(row.Balance) - parseFloat(row.Reserve)) * 1000) / 1000;  //доступно
		 row.Available = getDecimal(ava) == 0 ? Math.round(ava) : ava;     
		 */
		  
		 	for (i = 0; i < sc.tableData3.length; i++) {
			 sc.tableData3[i].Count = sc.tableData3[i].Count.toString().replace(",", "."); 	
   		    var tCount = (sc.tableData3[i].Count == null || sc.tableData3[i].Count=="") ? 0 : parseFloat(sc.tableData3[i].Count);	
			//пересчет резерва для др. строк
			for (j = 0; j < sc.tableData3.length; j++) {
			if (i != j && sc.tableData3[i].SpareParts.Id == sc.tableData3[j].SpareParts.Id && sc.tableData3[i].Storage.Id == sc.tableData3[j].Storage.Id)
			{
			
   	    	tCount = tCount + ((sc.tableData3[j].Count == null || sc.tableData3[j].Count=="") ? 0 : parseFloat(sc.tableData3[j].Count));	
			}				
			}
			 //резерв	
			 var count =  (tCount == null || tCount=="") ? 0 : tCount;	
             var res = Math.round((parseFloat(sc.tableData3[i].Reserve2) + parseFloat(count)) * 1000) / 1000;
		    	sc.tableData3[i].Reserve = res;
			//доступно
			 var ava = Math.round((parseFloat(sc.tableData3[i].Balance) - parseFloat(res)) * 1000) / 1000;  //доступно
		     sc.tableData3[i].Available = getDecimal(ava) == 0 ? Math.round(ava) : ava;  
             }
	 
        };
        
		
		
		
		sc.checkRequire = function() {
			if (sc.formData.IsRequire) {
				if (sc.typeOperation=='create') {
					sc.formData.IsDaily = false;
					sc.checkDailyAct();
			sc.formData.RepairNumber = 'М/'+sc.infoData.NewRequireNumber;
				}
					$(".requireDisable").addClass("disabledbutton");
					$("#withoutDay").addClass("disabledbutton");
						sc.active = {
	                	sp1: false,
		                sp2: false,
		                sp4: false,
		                sp5: false,
                        sp3: true,};
		   sc.formData.dateOpen = sc.formData.dateStart; 
		   sc.formData.dateEnd = sc.formData.dateStart; 	 
		   sc.formData.Plan1 = false; sc.formData.Plan2 = false;
	       sc.formData.Plan3 = false; sc.formData.Plan4 = false;
		   $cookieStore.put('tOperation', 'close');
		    sc.NameButtom = 'Закрыть';
			sc.detailTab = true;
		   /////==========
		   sc.updateColumnDefs("10%","10%");
           sc.columnDefs.push({width:"10%",displayName:"Списать",field: "WriteOff",
		   cellTemplate: '<input name="CloseChack" type="checkbox" style="margin-left: 10px;margin-top: 18px;" value="{{row.entity[col.field]}}" ng-model="row.entity[col.field]" placeholder="">',
		   headerCellTemplate: "<div   ng-class=\"'colt' + col.index\" class=\"ngHeaderText\">{{col.displayName}}</div>"}); 

           console.log(sc.columnDefs);
			}
			else {
				if (sc.typeOperation=='create') {
			sc.formData.RepairNumber = sc.infoData.NewActNumber;
				sc.formData.dateOpen = null;
				sc.formData.dateEnd = null ; 
				$(".requireDisable").removeClass("disabledbutton");
				$("#withoutDay").removeClass("disabledbutton");
				$cookieStore.put('tOperation', 'create');
				sc.NameButtom = 'Сохранить';
				sc.detailTab = false;
	            if (sc.columnDefs.length == 13) {
            	sc.updateColumnDefs("15%","15%");
				}

	}
		console.log(sc.columnDefs);
			}
			
		}
		
		sc.dateChangeSpareParts = function() {
			if (sc.formData.dateStart) {
		  requests.serverCall.post($http, "SparePartsCtrl/GetDictionaryRepairs", {repairStartDate : sc.formData.dateStart}, function (data, status, headers, config) {
          console.info(data);
		   if (sc.typeOperation == 'create') {
			   if (!sc.formData.IsRequire) {
	      sc.formData.RepairNumber = data.NewActNumber;
			   } else {
	    	sc.formData.RepairNumber = 'М/'+data.NewRequireNumber;	
			   }
			   	sc.infoData.NewActNumber = data.NewActNumber;
                sc.infoData.NewRequireNumber = data.NewRequireNumber;
	            $("#createAct").addClass("disabledbutton");
	            $("#changeStatusRepair").hide();
			 } });
			}
	
	
		if (sc.formData.IsRequire) {
			sc.formData.dateOpen = sc.formData.dateStart ; 
			sc.formData.dateEnd = sc.formData.dateStart ; 
		}	
		}
		
		

    // мультивыделение
    sc.keyDownGrid = function (e) {
        if (curUser.roleid == '2' || curUser.roleid == '10') {
            if ((e.keyCode === 16 || e.keyCode === 17 || e.keyCode === 18 || e.keyCode === 91) && !sc.multi) {
                sc.multi = true;
            }
        }
    };
	    sc.keyUpGrid = function (e) {
        if (curUser.roleid == '2' || curUser.roleid == '10') {
            if (e.keyCode === 16 || e.keyCode === 17 || e.keyCode === 18 || e.keyCode === 91) {
                sc.multi = false;
            }
        }
    };
    // листнер на событие изменение data в таблице
    sc.gridEvDataListener = function () {
        var offCallMeFn = $scope.$on("ngGridEventData", sc.gridEvDataListener);
        offCallMeFn();
        if (_val == "first" && sc.tableData.length > 0) _val = sc.tableData[0][_field];
		console.log(_val);
        sc.tableData.forEach(function (item, i, arr) {
            if (item[_field] == _val) {
                sc.gridOptions.selectRow(i, true);
                if (sc.gridOptions.ngGrid) sc.gridOptions.ngGrid.$viewport.scrollTop(sc.gridOptions.ngGrid.rowMap[i] * sc.gridOptions.ngGrid.config.rowHeight);
            } else {
                sc.gridOptions.selectRow(i, false);
            }
        });
        $(".ngViewport").focus();
    };
	    // получение масива в выпадающие списки
		   if (sc.typeOperation == 'create' && !sc.formData.dateStart) {
			sc.formData.dateStart = new Date(); 
		   }
		console.log(sc.formData.dateStart);
    requests.serverCall.post($http, "SparePartsCtrl/GetDictionaryRepairs", {repairStartDate : sc.formData.dateStart}, function (data, status, headers, config) {
        console.info(data);
        sc.infoData = data;
		   if (sc.typeOperation == 'create') {
	sc.formData.RepairNumber = sc.infoData.NewActNumber;
	$("#createAct").addClass("disabledbutton");
	$("#changeStatusRepair").hide();
       }

        console.info(sc.formData);
		 	if (sc.typeOperation=='edit' || sc.typeOperation=='close') {
		 requests.serverCall.post($http, "SparePartsCtrl/GetRepairsDetails", {ActId: sc.formData.ActId}, function (data, status, headers, config) {
                console.info(data);
                sc.formData = data.GeneralInfo;
                sc.tableData = data.TableInfo1;
				sc.tableData2 = data.TableInfo2;
				sc.tableData3 = data.TableInfo3;
				sc.tableData4 = data.TableInfo4;
				sc.tableData5 = data.TableInfo5;
				console.log(sc.formData.ActId);
				sc.checkRequire();
				 $("#require").addClass("disabledbutton");
				sc.HaveDailyAct = data.GeneralInfo.CheckDailyAct;
				if (sc.formData.IsRequire) {
				$("#createAct").addClass("disabledbutton");
				}
				
				if (sc.formData.IsDaily) {sc.checkDaily = 'Yes';
				  $("#createAct").addClass("disabledbutton");
				} else
				 {sc.checkDaily = 'No';}
			 if (sc.formData.AdminDates) {sc.checkDaily = 'Yes'; }
									//таблица списания
				if (sc.formData.Status==3)
				{
					/*
					 sc.columnDefs[4].width="0%";
					 sc.columnDefs[5].width="0%";
					 sc.columnDefs[1].width="30%";
					 sc.columnDefs[2].width="20%";
					 sc.columnDefs[3].width="20%";
					 sc.columnDefs[6].width="20%";
					 sc.columnDefs[6].cellTemplate='<div class="ngCellText" ng-class="col.colIndex()">' +
             '<input value="{{row.entity[col.field]}}" placeholder="0" style="border: none;" readonly>' +'</div>';
			 */
			         sc.columnDefs[5].width="0%";
					 sc.columnDefs[6].width="0%";
					 sc.columnDefs[2].width="30%";
					 sc.columnDefs[3].width="20%";
					 sc.columnDefs[4].width="20%";
					 sc.columnDefs[7].width="20%";
					 sc.columnDefs[7].cellTemplate='<div class="ngCellText" ng-class="col.colIndex()">' +
             '<input value="{{row.entity[col.field]}}" placeholder="0" style="border: none;" readonly>' +'</div>';
					console.log(sc.columnDefs);
				}
			
				if (sc.formData.Status!=3 || curUser.roleid!=10) {
				$("#changeStatusRepair").hide();	
				}
				if (curUser.roleid != 10 || sc.formData.Status==3) {
		    	$("#changeAdminDates").hide();
		      }
				//переходы
				if ($cookieStore.get('checkWriteOff')==2 || $cookieStore.get('checkWriteOff')==3 || $cookieStore.get('checkCopyWriteOff') == 1 ) {
					$cookieStore.put('checkCopyWriteOff', null);
					$cookieStore.put('DayAct', null);
						sc.active = {
	                	sp1: false,
		                sp2: false,
		                sp4: true,
		                sp5: false,
                        sp3: false,
         };
				sc.formData = JSON.parse($.session.get('formDataWriteOff'));
				sc.tableData = JSON.parse($.session.get('tableDataWriteOff'));
				sc.tableData2 = JSON.parse($.session.get('tableData2WriteOff'));
				sc.tableData3 = JSON.parse($.session.get('tableData3WriteOff'));
				sc.tableData5 = JSON.parse($.session.get('tableData5WriteOff'));
				
				if ($cookieStore.get('newDateOpen')!=null && $cookieStore.get('checkWriteOff')!=3) {
				console.log($cookieStore.get('newDateOpen'));
				
				if (sc.formData.dateOpen==null) {
						sc.formData.dateOpen = $cookieStore.get('newDateOpen');
				} else 
				{
				if (sc.formData.dateOpen.valueOf() > $cookieStore.get('newDateOpen').valueOf()) {
				sc.formData.dateOpen = $cookieStore.get('newDateOpen');
				}			
				}
				}
                $cookieStore.put('checkWriteOff', 0);				
				}
		 if (sc.typeOperation == 'edit') {
		if (data.GeneralInfo.CheckDailyAct!=0) {$("#createAct").addClass("disabledbutton");
		$("#withoutDay").addClass("disabledbutton");
		}  
	 }
            });
		
	}
});

    //при изменении даты в ячейке
    sc.dateChange = function (row, date) {
        sc.tableData.forEach(function (item, d) {
            if (item == row) item["Date"] = moment(date).format("MM.DD.YYYY");
        })
    };

    // при редатировании ячейки с выпадающим списком
    sc.editDropCell = function (row, column, selectedItem) {
        sc.tableData.forEach(function (item, d) {
            if (item == row) item[column] = selectedItem;
        })
    };

    // при редатировании ячейки с input
    sc.editInputCell = function (row, column, value) {
        sc.tableData.forEach(function (item, d) {
            if (item == row) item[column] = value;
        })
    };
	//выбор техники
   sc.traktorChange = function(item) {
	   if (item.Id!=-1) {
	 $("#equipmnets").addClass("disabledbutton");		 
        var typeFuelSel = item.Name_fuel;
		console.log(typeFuelSel);
		sc.formData.TypeFuel = typeFuelSel.Id;		
		GetListRepairsSpareParts();
	   } else {
		   $("#equipmnets").removeClass("disabledbutton");
	   }
		
    };
   sc.moduleChange = function() {
GetListRepairsSpareParts();
   }
     sc.spareChange = function() {
	GetListRepairsSpareParts();	 
	 }
   
   
		function GetListRepairsSpareParts() {
			console.log(sc.formData.AgroreqId);
			if (sc.formData.AgroreqId == -1 || sc.formData.AgroreqId==null) {
							countEquip++;
		console.log(countEquip);
		if (countEquip > 1) {
		$("#createAct").addClass("disabledbutton");	
		}
			  requests.serverCall.post($http, "SparePartsCtrl/GetListRepairsSpareParts",{
                        TraktorId:  sc.formData.TypeSpareId,
						ModId : sc.formData.ModuleId
                 }, function (data, status, headers, config) {
                       console.info(data);
                    sc.infoData.SparePartsList = data;
					sc.infoData.VendorCodeList = [];
					for (var i=0;i<sc.infoData.SparePartsList.length;i++)
					{
						sc.infoData.VendorCodeList.push({Id:sc.infoData.SparePartsList[i].Id,Name:sc.infoData.SparePartsList[i].Description, 
						Description:sc.infoData.SparePartsList[i].Name });
					}
					
                    }, function () {
                      }) 
			} else {
				sc.agroreqChange();
			}	  
		}
		//выбор оборудования
	sc.agroreqChange = function() {
		console.log(sc.formData.AgroreqId);
		if (sc.formData.AgroreqId!=-1) {
		countEquip++;
		console.log(countEquip);
		if (countEquip > 1) {
		$("#createAct").addClass("disabledbutton");	
		}
			 $("#traktors").addClass("disabledbutton");	
		 requests.serverCall.post($http, "SparePartsCtrl/GetListEquipSpareParts",{
                        EquipId:  sc.formData.AgroreqId,
                 }, function (data, status, headers, config) {
                       console.info(data);
                    sc.infoData.SparePartsList = data;
					sc.infoData.VendorCodeList = [];
					for (var i=0;i<sc.infoData.SparePartsList.length;i++)
					{
						sc.infoData.VendorCodeList.push({Id:sc.infoData.SparePartsList[i].Id,Name:sc.infoData.SparePartsList[i].Description, 
						Description:sc.infoData.SparePartsList[i].Name });
					}
					
                    }, function () {
                      })
		} else {
		$("#traktors").removeClass("disabledbutton");		
		}
					  
	}
    // редактирование ячек с модальными окнами
    sc.updateSelectedCell = function (colField, selectedVal, displayName, row) {
		if (sc.formData.Status!=3) {
		met=1;
            sc.colField = colField;
            sc.selectedRow = [];
            sc.selectedRow.push(row);
			if (sc.mySelections1==row) {met=2;}
			  console.log(row);
			sc.mySelections1 = row;
			console.log(sc.mySelections1);
            var modalValues;
            if (sc.infoData) {
				if (met==2) {
                switch (sc.colField) {
                    case "SpareParts":
                        modalValues = sc.infoData.SparePartsList;
                        break;
                     case "VendorCode":
                        modalValues = sc.infoData.VendorCodeList;
                        break;
						 case "Storage":
                        modalValues = sc.infoData.StorageList;
                        break;
						 case "TypeTask":
                        modalValues = sc.infoData.TypeTasksList;
                        break;		
                }
                modals.showWindow(modalType.WIN_SELECT, {
                    nameField: displayName,
                    values: modalValues,
                    selectedValue: selectedVal
                })
                    .result.then(function (result) {
                        row[sc.colField] = result;
                       switch (colField) {
						case "SpareParts":
						console.info(row);
						row.VendorCode = {Id: result.Id, Name: result.Description};
						
						//обновление даты
						var now = new Date();
						row.Date = now;
                         //вывод складов
				    requests.serverCall.post($http, "SparePartsCtrl/GetListStorageSpareParts",{
                        Spare_id:  row.SpareParts.Id,
                 }, function (data, status, headers, config) {
                       console.info(data);
                    sc.infoData.StorageList = data;
                    }, function () {
                      });
						 break;
	                    case "VendorCode":
                         row.SpareParts = {Id: result.Id, Name :result.Description};
						 break;
						 case "Storage":
                       //доступно и резерв
					   requests.serverCall.post($http, "SparePartsCtrl/GetPriceCountBalance",{
                       Spare_id:  row.SpareParts.Id,
					   Store_id:  row.Storage.Id,
					   Pk : sc.formData.ActId,
                 }, function (data, status, headers, config) {
                       console.info(data);
					row.Count="";
					row.Balance = data[0].Count; //баланс
					var ava = Math.round((parseFloat(data[0].Count) -  parseFloat(data[0].Reserve)) * 1000) / 1000;
					console.info(ava);
                    row.Available = getDecimal(ava) == 0 ? Math.round(ava) : ava;
					row.Reserve = data[0].Reserve;
					res2 = Math.round((parseFloat(data[0].Reserve) - parseFloat((row.Count == null || row.Count == "") ? 0 : row.Count)) * 1000) / 1000;
				//	row.Reserve2 = getDecimal(res2) == 0 ? Math.round(res2) : res2;  old
				    row.Reserve2 = data[0].Reserve2;  //new
				  sc.changeCountPrice();	
                    }, function () {
                      });
						 break;
						}
                    }, function () {});
				}
            } else {}
	}  
    };
	sc.changeServiceTotal = function (row) {
    row.Count = row.Count.toString().replace(",", "."); 	
	row.Price = row.Price.toString().replace(",", "."); 
	 if (row.Price === null) {
            row.Price = 0;
        }
        if (row.Count === null) {
            row.Count = 0;
        }	
	row.Cost1 = (row.Count * row.Price).toFixed(2);
	row.Cost3 = (parseFloat(row.Cost1) + parseFloat(row.Cost2/100*row.Cost1)).toFixed(2);
	if (isNaN(row.Cost3)) {row.Cost3=0;}
	}
	
	
	function unique(arr) {
  var obj = {};

  for (var i = 0; i < arr.length; i++) {
    var str = arr[i];
    obj[str] = true; // запомнить строку в виде свойства объекта
  }

  return Object.keys(obj); // или собрать ключи перебором для IE8-
}
	function getMaxOfArray(numArray) {
   return Math.max.apply(null, numArray);
   }
    //создать таблица1
    sc.createAct = function () {
	var c = [];
	for (i = 0; i < sc.tableData.length; i++) {
		c.push(sc.tableData[i].Number);
      }
	  con=getMaxOfArray(c);
	  		  console.log(con);
		if (con == -Infinity)	 {con=0;} 
        con = parseInt(con)+1;
        var newAct = {
            Malfunction: "",
            Number: con
        };
        sc.tableData.push(newAct);
		        setTimeout(function() {
            var grid = sc.gridOptions.ngGrid;
            grid.$viewport.scrollTop(sc.tableData.length * grid.config.rowHeight + 100);
        }, 100);
    };
	    //создать таблица2
    sc.createAct2 = function () {
		  var c = [];
	for (i = 0; i < sc.tableData2.length; i++) {
		c.push(sc.tableData2[i].Number);
      }
	  con=getMaxOfArray(c);
	  		  console.log(con);
		if (con==-Infinity)	 {con=0;} 
        con = parseInt(con)+1;
        var newAct = {
            Malfunction: "",
			TypeTask : {Id : null, Name : null},
            Number: con
        };
        sc.tableData2.push(newAct);
		        setTimeout(function() {
            var grid = sc.gridOptions2.ngGrid;
            grid.$viewport.scrollTop(sc.tableData2.length * grid.config.rowHeight + 100);
        }, 100);
    }; 
		    //создать таблица3
    sc.createAct3 = function () {
		  var c = [];
	for (i = 0; i < sc.tableData3.length; i++) {
		c.push(sc.tableData3[i].Number);
      }
	    con = getMaxOfArray(c);
	    console.log(con);
		if (con == -Infinity)	 {con=0;} 
        con = parseInt(con)+1;
        var newAct = {
           // Malfunction: "",
			SpareParts : {Id : null, Name : null},
			VendorCode : {Id : null, Name : null},
		//	Count: 0,
			Reserve : 0,
			Available : 0,
		//	Date : null,
            Number: con,
			WriteOff : false,
        };
        sc.tableData3.push(newAct);
			console.log(sc.tableData3);
		    setTimeout(function() {
            var grid = sc.gridOptions3.ngGrid;
            grid.$viewport.scrollTop(sc.tableData3.length * grid.config.rowHeight + 100);
        }, 100);
    }; 
	//услуги
	 sc.createAct4 = function () {
		  var c = [];
	for (i = 0; i < sc.tableData5.length; i++) {
		c.push(sc.tableData5[i].Number);
      }
	    con = getMaxOfArray(c);
	    console.log(con);
		if (con == -Infinity)	 {con = 0;} 
        con = parseInt(con)+1;
        var newAct = {
            NameService: "",
            Number: con
        };
        sc.tableData5.push(newAct);
		    setTimeout(function() {
            var grid = sc.gridOptions5.ngGrid;
            grid.$viewport.scrollTop(sc.tableData5.length * grid.config.rowHeight + 100);
        }, 100);
    };
	   sc.removeTask = function () {
		sc.tableData.splice(sc.tableData.indexOf(sc.mySelections[0]), 1);
		var count=1;
        for (i = 0; i < sc.tableData.length; i++) {
	    sc.tableData[i].Number=count;
	    count++;
          }
		 
	   }
	  sc.removeTask2 = function () {
	  sc.tableData2.splice(sc.tableData2.indexOf(sc.mySelections2[0]), 1);  
	  var count=1;
      for (i = 0; i < sc.tableData2.length; i++) {
	  sc.tableData2[i].Number=count;
	  count++;
          }
	   }
	   //запчасти
	   sc.removeTask3 = function () {
		sc.tableData3.splice(sc.tableData3.indexOf(sc.mySelections3[0]), 1);  
	 	 var count = 1;
     for (i = 0; i < sc.tableData3.length; i++) {
	sc.tableData3[i].Number=count;
	   count++;
          }	
     sc.changeCountPrice();		  
	   }
	    sc.removeTask4 = function () {
		sc.tableData5.splice(sc.tableData5.indexOf(sc.mySelections5[0]), 1);  
	 	 var count = 1;
     for (i = 0; i < sc.tableData5.length; i++) {
	 sc.tableData5[i].Number = count;
	   count++;
          }			 
	   }
	//открыть ежедневный наряд 
	 sc.openDailyAct = function () {
		console.log(sc.mySelections4[0]);
		if (sc.mySelections4[0]) {
		$cookieStore.put('typeoper', 'edit');	
		$cookieStore.put('act_id', sc.mySelections4[0].Id);
        $location.path('/daily_repairs_item/');
        } else needSelection('Открытие документа', "Для открытия необходимо выбрать документ");
	 }
	
     sc.addRowsFromInvoiceAct = function () {
	 if (sc.formData.InvoiceAct) {
		      requests.serverCall.post($http, "SparePartsCtrl/GetInvoiceDetailsForRepair", 
			  {
			    ActId : sc.formData.InvoiceAct,
				Pk : sc.formData.ActId,
			  },
			  function (data, status, headers, config) {
                console.info(data);
				for (var i = 0; i < data.RepairsList.length; i++) {
				sc.tableData3.push(data.RepairsList[i]);	
				}
				count = 1;
				for (var i = 0; i < sc.tableData3.length; i++) {
                sc.tableData3[i].Number = count;
		       count++;
				}
			sc.changeCountPrice();
           });
	 }
 }
	
	
    //поиск элемента в массиве
	    find = function (array, value) {
	        for (var i = 0; i < array.length; i++) {
	            if (array[i] == value) return i;
	        }
	        return -1;}

	  sc.checkDailyAct = function () {
		  if (sc.formData.IsDaily) {
			  sc.checkDaily = 'Yes';
			  $("#createAct").addClass("disabledbutton");
		  } else {
			  sc.checkDaily = 'No';
			  if (sc.typeOperation!='create' && sc.HaveDailyAct==0) {    
			  $("#createAct").removeClass("disabledbutton");
			  }
		  }
	  }		
			  sc.changeAdminDates = function () {
				 sc.formData.AdminDates = true;
			     sc.checkDaily = 'Yes';
			  }
			
  sc.checkPlan1 = function () {
	  sc.formData.Plan2 = false;
	  sc.formData.Plan3 = false;
	  sc.formData.Plan4 = false;
  }
	sc.checkPlan2 = function () {
	  sc.formData.Plan1 = false;
	  sc.formData.Plan3 = false;
	  sc.formData.Plan4 = false;
  }
  sc.checkPlan3 = function () {
	  sc.formData.Plan1 = false;
	  sc.formData.Plan2 = false;
	  sc.formData.Plan4 = false;
  }
	  sc.checkPlan4 = function () {
	  sc.formData.Plan1 = false;
	  sc.formData.Plan2 = false;
	  sc.formData.Plan3 = false;
	  }
	
    // кнопка Печать
    sc.printBtn = function () {
    }
    //создать ежедневный наряд по наряду
	sc.createDayActs = function () {
	$cookieStore.put('typeoper', 'open');	
	$cookieStore.put('DayAct', sc.formData.ActId);	
	//записываем все данные в куки
	$.session.set("formDataWriteOff",JSON.stringify(sc.formData));
	$.session.set("tableDataWriteOff",JSON.stringify(sc.tableData));
	$.session.set("tableData2WriteOff",JSON.stringify(sc.tableData2));
	$.session.set("tableData3WriteOff",JSON.stringify(sc.tableData3));
	$.session.set("tableData5WriteOff",JSON.stringify(sc.tableData5));
	$cookieStore.put('checkWriteOff', 1);
	$location.path('/daily_repairs_item/');
	}
	
	
   function needSelection(title, message) {
        modals.showWindow(modalType.WIN_MESSAGE, {
            title: title,
            message: message,
            callback: function () {
            }
        });
    }
    //кнопка Сохранить
    sc.submitForm = function () {	
     var check = 0;
	 if (sc.formData.Status==3)
   {
    check = 1;
     needSelection('Предупреждение', "Документ уже был закрыт. Нажмите на кнопку 'Отмена'.");
     return;
     }
	if (sc.formData.RepairNumber==null || sc.formData.RepairNumber=="")
	{
		check = 1;
	needSelection('Предупреждение', "Введите номер наряда");
	}
	
		if ((sc.formData.TraktorId == null || sc.formData.TraktorId == -1 ) && (sc.formData.AgroreqId == null || sc.formData.AgroreqId == -1 ) && check!=1)
	{
		check = 1;
      needSelection('Предупреждение', "Выберите технику или оборудование");
	}
	
		if (sc.formData.EmpId==null && check!=1)
	{
		check = 1;
      needSelection('Предупреждение', "Выберите ответственного");
	}
		if (!sc.formData.IsRequire && sc.formData.Plan1==false && sc.formData.Plan2==false && sc.formData.Plan3==false && sc.formData.Plan4==false && check!=1)
	{
		check = 1;
      needSelection('Предупреждение', "Выберите вид ремонта");
	}
	if (check!=1) {
		 for (i = 0; i < sc.tableData.length; i++) {
			 if (sc.tableData[i].Malfunction==null || sc.tableData[i].Malfunction=="")
			 {
				check = 1; 
				needSelection('Предупреждение', "Заполните или удалите пустые строки");
				break;
			 }
		 }
	}
	  //пустая таблица запчастей
	  if (check!=1 && sc.formData.IsRequire) {
		 if ( sc.tableData3.length == 0) {
				check = 1; 
				needSelection('Предупреждение', "Заполните таблицу З/ч и материалы");
		 }
		 
	}
	  
		if (check!=1) {
		 for (i = 0; i < sc.tableData2.length; i++) {
			 if (sc.tableData2[i].Malfunction==null || sc.tableData2[i].Malfunction=="" || sc.tableData2[i].TypeTask.Id==null)
			 {
				check = 1; 
				needSelection('Предупреждение', "Заполните или удалите пустые строки");
				break;
			 }
		 }
	}
		if (check!=1) {
		 for (i = 0; i < sc.tableData3.length; i++) {
			 if (sc.tableData3[i].Count=="") {sc.tableData3[i].Count=0;}
			 if (sc.tableData3[i].SpareParts.Id==null || sc.tableData3[i].Storage==null || sc.tableData3[i].Storage.Id==null)
			 {
				check = 1; 
				needSelection('Предупреждение', "Заполните или удалите пустые строки");
				break;
			 }
		 }
	}
		if (check != 1) {
		 for (i = 0; i < sc.tableData3.length; i++) {
			 if (sc.formData.Status != 3 && parseFloat(sc.tableData3[i].Available) < 0)
			 {
				check = 1; 
				needSelection('Предупреждение', "На складе недостаточно запчастей");
				break;
			 }
			 //sc.typeOperation=='close'
			  if  (sc.NameButtom == 'Закрыть' && (sc.tableData3[i].Count == null || parseFloat(sc.tableData3[i].Count) == 0))   
			 {
				check = 1; 
				needSelection('Предупреждение', "Заполните поле 'Количество запчастей'");
				break;
			 }
		 }
	}
	    if ((sc.typeOperation == 'create' || sc.typeOperation == 'edit') && !sc.formData.IsRequire) {
	     if (check != 1) {
		sc.CreateUpdateRepairsDetail();
		 }		
		  }
		  //редактируем и списываем
		  	console.log(sc.typeOperation);
		 if ((sc.typeOperation == 'close' || sc.formData.IsRequire)  && check != 1) {
			  var check2 = 0;
			console.log(sc.tableData3);
			 for (var i = 0;i < sc.tableData3.length;i++)
			 {
				 console.log(sc.tableData3[i].WriteOff);
				if (!sc.tableData3[i].WriteOff) {
				check2 = 1;
				$("#mes").text('Удалить из списка запчасти не подлежащие списанию?');
				$('#mes').css('color', '#000000');
				$("#myModal").modal("show");
				break;
				}  
			 }
			  if (check2!=1 && check != 1) {sc.formData.WriteOff = true; sc.CreateUpdateRepairsDetail();}
			  
		  }	  
    };
    var getNew;
	sc.CreateUpdateRepairsDetail = function()   {
			 requests.serverCall.post($http, "SparePartsCtrl/CreateUpdateRepairsDetail",{
                        GeneralInfo:  sc.formData,
						TableInfo1 : sc.tableData,
						TableInfo2 : sc.tableData2,
						TableInfo3 : sc.tableData3,
						TableInfo5 : sc.tableData5,
                 }, function (data, status, headers, config) {
                       console.info(data);
					   if (Number.isInteger(parseInt(data)) ) {
			  modals.showWindow(modalType.WIN_MESSAGE, {
                title: "Сохранение",
                message: "Документ успешно сохранен",
                callback: function () {
                }
            });
			
			//условие перехода
			if (sc.typeOperation == 'close' || sc.formData.IsRequire) {
			 $location.path('/repairs_spare_parts');
			} else {
				//усл-е создать
				if (!sc.formData.IsDaily &&  !sc.formData.IsRequire && sc.typeOperation == 'create') {
					  $("#createAct").removeClass("disabledbutton");
					    $("#require").addClass("disabledbutton");
					  sc.formData.ActId = parseInt(data) ;
					  sc.typeOperation = 'edit';
					  $cookieStore.put('tOperation', 'edit');  
					 $cookieStore.put('repair_id', sc.formData.ActId); 
				}
			}
			
			}  else {
			 modals.showWindow(modalType.WIN_MESSAGE, {
                              title: "Предупреждение",
                              message: data,
                              callback: function () {
                              }  
                          });	
			}
                    }, function () {
                      });
          $.session.remove('formData_incoming');
          $.session.remove('tableDataWriteOff');
          $.session.remove('tableData2WriteOff');
          $.session.remove('tableData3WriteOff');
          $.session.remove('tableData3WriteOff');			
	}
	//перевод в статус Открыт
	sc.changeStatusRepair = function () {
		 requests.serverCall.post($http, "SparePartsCtrl/ChangeStatusRepair",{
                        ActId:  sc.formData.ActId,
                 }, function (data, status, headers, config) {
                       console.info(data);
					   if (data!=null) {
			  modals.showWindow(modalType.WIN_MESSAGE, {
                title: "Сохранение",
                message: "Документ успешно переведен в статус Открыт",
                callback: function () {
                }
            });
			 $location.path('/repairs_spare_parts');
			}}, function () {
                      });
	}
		function getDecimal(input) {
		return input - (input ^ 0);
	}
     //модальное окно
 sc.cancelButton = function () {
		$("#myModal").modal("hide");
    //  $("div.modal-backdrop").hide();	 
	
    };
	
	     sc.yes = function () {	 
		if ($("#mes").text()=='Удалить из списка запчасти не подлежащие списанию?')	 
		{
		// $("#myModal").modal("hide");
		// $("div.modal-backdrop").hide();
		 //второе окно
		 var kol = 0;
		 var cost = 0;
		  for (var i = 0;i < sc.tableData3.length;i++)
			 {
			 if (sc.tableData3[i].WriteOff == false) {
              kol = kol + sc.tableData3[i].Count;
			  cost = cost + sc.tableData3[i].Cost;
			  console.log(sc.tableData3[i].Cost);
			 }
			 }
			// cost = cost.toFixed(2);
			cost = Math.round(cost * 100) / 100;
			 kol = Math.round(kol * 1000) / 1000;
		 $("#mes").text('Из наряда будут удалены запчасти в количестве '+kol+ ' на общую сумму '+cost+' рублей');
		 $('#mes').css('color', '#FF0000');
		 $("#myModal").modal("show");
		} else {
		 $("#myModal").modal("hide");
		 $("div.modal-backdrop").hide();
		  for (var i = 0;i < sc.tableData3.length;i++)
			 {
			 if (sc.tableData3[i].WriteOff==false) {
				 sc.tableData3.splice(sc.tableData3.indexOf(sc.tableData3[i]), 1);  i--;
			 }
			 }
			 sc.formData.WriteOff = true;
            sc.CreateUpdateRepairsDetail();
		}
	      }
	
	 sc.active = {
		sp1: true,
		sp2: false,
		sp4: false,
        sp3: false,
		sp5: false,
         };
   sc.no = function () {
	 //  console.log($("#mes").text());
	$("#myModal").modal("hide");
	$("div.modal-backdrop").hide();	
	console.log(sc.active);
	
	    sc.active = {
		sp1: false,
		sp2: false,
		sp4: false,
		sp5: false,
        sp3: true,
         };
		
	}
	//

    //кнопка Отмена
    sc.cancelClick = function () {
		  $.session.remove('formData_incoming');
          $.session.remove('tableDataWriteOff');
          $.session.remove('tableData2WriteOff');
          $.session.remove('tableData3WriteOff');
          $.session.remove('tableData3WriteOff');	
        $location.path('/repairs_spare_parts');
    };
		//сортировка
    sc.sorting = function () {
        var names = [];
        var fullData1 = [];
		filteredData1 = [];
		   for (var i = 0, li = sc.formData.MaterialsList.length; i < li; i++) {
                fullData1.push(sc.formData.MaterialsList[i]);
            }
            for (var i = 0, li = fullData1.length; i < li; i++) {
                names.push(fullData1[i].Material.Name);
            }
            names.sort();
            if (count % 2 == 0) { names.reverse(); }
            for (var i = 0, li = fullData1.length; i < li; i++) {
                for (var j = 0, l1 = fullData1.length; j < l1; j++) {
            if (names[i] == fullData1[j].Material.Name) {filteredData1.push( fullData1[j]);fullData1.splice(j,1); break; }
                }
            }
            sc.tableData = filteredData1;
            count++;
    }
	
	//Кнопка выбрать все
	sc.select = false;
	sc.ChackClearAll = 'Выбрать все';
    sc.ChackAll = function (source) {   
          if (sc.select != false) {
  			for(var i=0, n=sc.tableData3.length;i<n;i++) {  			
    			sc.tableData3[i].WriteOff = false;
    		}
    		sc.select = false;
    		sc.ChackClearAll = 'Выбрать все';  
    	} else {
    		for(var i=0, n=sc.tableData3.length;i<n;i++) {  			
    			sc.tableData3[i].WriteOff = true;
    		}   
    		sc.ChackClearAll = 'Убрать все';  			
    		sc.select = true;
    	}  	

    };
}
]);