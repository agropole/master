var app = angular.module("RepairsSparePartsCtrl", ["ngGrid", "services", 'ngCookies', "modalWindows","LogusConfig"]);
app.controller("RepairsSparePartsCtrl", ["$scope", "$http", "requests", "$location", '$cookieStore', '$rootScope', "modals", "modalType",'$stateParams', 'LogusConfig',  function ($scope, $http, requests, $location, $cookieStore, $rootScope, modals, modalType,$stateParams,LogusConfig) {
    var sc = $scope;
		sc.formData = {};
	sc.totalServerItems = 0;
    sc.pagingOptions = {
        pageSizes: [10, 15, 20, 50, 100],
        pageSize: 100,
        currentPage: 1,
        totalServerItems: sc.totalServerItems
    };
	 $cookieStore.put('dateDailyCalendar', {date: null});
	 $cookieStore.put('dateEndDailyCalendar', {date: null});
		 var enddate = new Date();
	 var startD = new Date(enddate.getFullYear(), enddate.getMonth(), 1);
//	$cookieStore.put('repairEndDate', {date: undefined});

        if ($cookieStore.get('repairEndDate') && $cookieStore.get('repairEndDate').date)  {
	sc.formData.repairEndDate = $cookieStore.get('repairEndDate').date;
	console.log(sc.formData.repairEndDate);
		}
	else {
		  sc.formData.repairEndDate = enddate;	
		  console.log(sc.formData.repairEndDate);
	}
	
	if ($cookieStore.get('repairStartDate')&& $cookieStore.get('repairStartDate').date) {
	sc.formData.repairStartDate = $cookieStore.get('repairStartDate').date; 
		console.log(sc.formData.repairStartDate); 
	}
          else {
		 sc.formData.repairStartDate = startD;	
		console.log(sc.formData.repairStartDate); 
	 }

	 if ($cookieStore.get('OpenRepair')!=undefined) {
          sc.formData.OpenRepair = $cookieStore.get('OpenRepair');
    } else {
		  sc.formData.OpenRepair = true;
	}
	sc.openRepair = function() {
		if (sc.formData.OpenRepair==true) {
		  $("#datepick").addClass("disabledbutton");
		} else {
			  $("#datepick").removeClass("disabledbutton");
			  $("#datepick").removeClass("crystalbutton");
		}
		$("#openRepair").removeClass("crystalbutton");
		sc.filterText ="";
		sc.filterText2 ="";
		$cookieStore.put('OpenRepair',sc.formData.OpenRepair);
		loadService();
	}
	 sc.openRepair();
	 var filteredData = [];
	 var fullData;
     sc.selectedRow = [];
	 var rowsForDel = [];
	 var rowsForUpd = [];
	 sc.multi = false;
	 var nextNumber;
	 
	   // смотрим за изменениям в пейджинге
    sc.$watch('pagingOptions', function (newVal, oldVal) {
        if (newVal.currentPage != null && newVal !== oldVal && !isNaN(newVal.currentPage) && !isNaN(oldVal.currentPage)) {
            if (newVal.pageSize !== oldVal.pageSize) {
                sc.pagingOptions.currentPage = 1;
            loadService();
            }
            if (newVal.currentPage !== oldVal.currentPage) 
			{ 
			loadService();
			
			;}
        }
    }, true);

    sc.gridOptions = {
        enableColumnResize: true,
        enableRowSelection: true,
        showFooter: true,
        rowHeight: 49,
        headerRowHeight: 49,
        useExternalSorting: false,
        data: 'TableData',
		enablePaging: true,
        rowTemplate:rowTempl,
		
        pagingOptions: sc.pagingOptions,
        selectedItems: sc.selectedRow,
		i18n: 'ru',
        afterSelectionChange: function (row) {
       	for (var i=0; i<row.length;i++ ) 
   { 
     sc.selectedRow.push(row[i].entity); 
     console.log(sc.selectedRow); 
        } 
	  if (row.entity) {
		  console.log(row.selected);
                if (row.selected) {
                    if (sc.multi) sc.selectedRow.push(row.entity);
                    else {
                        sc.selectedRow = [];
                        sc.selectedRow.push(row.entity);
                    }
                } else {
                    sc.selectedRow.splice(sc.TableData.indexOf(row.entity), 1);
                }
            }
	   
        },
        beforeSelectionChange: function (row, event) {
        	   if (!sc.multi) {
                angular.forEach(sc.TableData, function (data, index) {
                    if (data.selected == true) sc.selectedRow.splice(index, 1);
                    sc.gridOptions.selectRow(index, false);

                });
            }
		return true;
        }
  
    };

	var rowTempl = '<div ng-style="{ \'cursor\': row.cursor }" '+ 
'ng-repeat="col in renderedColumns" '+
'ng-class="grid_spare_parts"' + 
'class="ngCell {{col.cellClass}} {{col.colIndex()}}" ng-cell></div>';
    sc.gridOptions.columnDefs = [
	        {field: "Number",
                displayName: "№",
                width: "10%",
             cellTemplate: '<div class="ngCellText {{getClassCopyCell(row.entity.Status,row)}}" ng-class="col.colIndex()" style = "border-top: 1px solid #06aa9f; border-bottom: 1px solid #06aa9f;  border-radius: 0;">' +
            '<input value="{{row.entity[col.field]}}" ng-model="row.entity[col.field]" placeholder="№" class="bordernone" readonly>' +
            '</div>',headerCellTemplate: "<div   ng-class=\"'colt' + col.index\" class=\"ngHeaderText\">{{col.displayName}}</div>"
            },
			
            {field: "DateBegin", displayName: "Дата создания", width: "13%", cellTemplate: '<a tooltip="{{row.getProperty(col.field)}}" tooltip-popup-delay="1000" tooltip-class="my-custom-tooltip" tooltip-placement="top" tooltip-append-to-body="true">' +
            '<div class="ngCellText {{getClassCopyCell(row.entity.Status,row)}}" ng-class="col.colIndex()" style = "border-top: 1px solid #06aa9f; border-bottom: 1px solid #06aa9f;  border-radius: 0;">' +
            '<input data="{{row.entity[col.field]}}" ng-change="changeCountPrice(row.entity)" ng-model="row.entity[col.field]" ng-readonly="row.entity.Id" ng-class="{bordernone: row.entity[\'Id\']}">' +
            '</div>' +
            '</a>', headerCellTemplate: "<div   ng-class=\"'colt' + col.index\" class=\"ngHeaderText\">{{col.displayName}}</div>"},
			
			 {field: "DateEnd", displayName: "Дата закрытия", width: "13%", cellTemplate: '<a tooltip="{{row.getProperty(col.field)}}" tooltip-popup-delay="1000" tooltip-class="my-custom-tooltip" tooltip-placement="top" tooltip-append-to-body="true">' +
            '<div class="ngCellText {{getClassCopyCell(row.entity.Status,row)}}" ng-class="col.colIndex()" style = "border-top: 1px solid #06aa9f; border-bottom: 1px solid #06aa9f;  border-radius: 0;">' +
            '<input data="{{row.entity[col.field]}}" ng-change="changeCountPrice(row.entity)" ng-model="row.entity[col.field]" ng-readonly="row.entity.Id" ng-class="{bordernone: row.entity[\'Id\']}">' +
            '</div>' +
            '</a>', headerCellTemplate: "<div   ng-class=\"'colt' + col.index\" class=\"ngHeaderText\">{{col.displayName}}</div>"},
			
			{field: "CarsName",
            displayName: "TC/Оборудование",
            width: "40%",
            cellTemplate: '<div class="ngCellText {{getClassCopyCell(row.entity.Status,row)}}" ng-class="col.colIndex()" style = "border-top: 1px solid #06aa9f; border-bottom: 1px solid #06aa9f; border-radius: 0;">' +
            '<input value="{{row.entity[col.field]}}" ng-model="row.entity[col.field]" placeholder="TC/Оборудование" class="bordernone" readonly>' +
            '</div>',headerCellTemplate: "<div   ng-class=\"'colt' + col.index\" class=\"ngHeaderText\">{{col.displayName}}</div>"
            },
			
			{field: "CarsNum",
             displayName: "Гос. №",
             width: "10%",
             cellTemplate: '<div class="ngCellText {{getClassCopyCell(row.entity.Status,row)}}" ng-class="col.colIndex()" style = "border-top: 1px solid #06aa9f; border-bottom: 1px solid #06aa9f;  border-radius: 0;">' +
            '<input value="{{row.entity[col.field]}}" ng-model="row.entity[col.field]" placeholder="Гос. №" class="bordernone" readonly>' +
            '</div>',headerCellTemplate: "<div   ng-class=\"'colt' + col.index\" class=\"ngHeaderText\">{{col.displayName}}</div>"
            },
			
	        {field: "Status", 
			
			displayName: "Статус", width: "15%", 
	        cellTemplate: '<div class="ngCellText {{getClassCopyCell(row.entity.Status,row)}}" ng-class="col.colIndex()" style = "border-top: 1px solid #06aa9f; border-bottom: 1px solid #06aa9f;  border-radius: 0;">' +
            '<input value="{{row.entity[col.field]}}" ng-model="row.entity[col.field]" placeholder="Статус" class="bordernone" readonly>' +
            '</div>',headerCellTemplate: "<div   ng-class=\"'colt' + col.index\" class=\"ngHeaderText\">{{col.displayName}}</div> "
			},
			{field: "Id", width: "0%"},	
						
    ];
        sc.getClassCopyCell = function (stat,row) {
        if (stat === "Закрыт" && row.selected==false ) {
            return 'SPclose';
        }
		if (row.selected==true) {  return '';}
    };
    function loadService() {
	   requests.serverCall.post($http, "SparePartsCtrl/GetRepairsActsByDate",
	   {
	   PageSize : sc.pagingOptions.pageSize,
       PageNum : sc.pagingOptions.currentPage,   
	   repairStartDate :  sc.formData.repairStartDate, 
       repairEndDate : sc.formData.repairEndDate,
       Torg12: sc.formData.OpenRepair, },
	    function (data, status, headers, config) {
		   sc.totalServerItems = data.CountItems;
           sc.pagingOptions.totalServerItems = data.CountItems;
           sc.TableData = fullData =  data.Values;
		});
    };
  loadService();
   sc.dateChangeSpareParts = function () {
	  $cookieStore.put('repairStartDate', {date: sc.formData.repairStartDate});
	  $cookieStore.put('repairEndDate', {date: sc.formData.repairEndDate});
	  console.log(sc.formData.repairEndDate);
	  	console.log(sc.formData.repairStartDate); 
	  loadService();
   }

    sc.createIncomingAct = function() {
	$cookieStore.put('tOperation', 'create');
	$cookieStore.put('repair_id', null);
	//console.log($cookieStore.get('typeoper'));
      $location.path('/writeoff_spare_parts/');
    };
	//редактирование наряда
    sc.editBtnClick = function () {
        if (sc.selectedRow[0]) {
		$cookieStore.put('tOperation', 'edit');	
		$cookieStore.put('repair_id', sc.selectedRow[0].Id);
        $location.path('/writeoff_spare_parts/');
        } else needSelection('Редактирование документа', "Для редактирования необходимо выбрать документ");	
    };
	//закрыть наряд
	sc.closeBtnClick = function () {
		 if (sc.selectedRow[0]) {
		$cookieStore.put('tOperation', 'close');	
		$cookieStore.put('repair_id', sc.selectedRow[0].Id);
        $location.path('/writeoff_spare_parts/');
        } else needSelection('Закрытие документа', "Для закрытия необходимо выбрать документ");
	}
	
	//удалить наряд
	sc.deleteAct = function() {
		    if (sc.selectedRow.length!=0) {
            modals.showWindow(modalType.WIN_DIALOG, {
                    title: "Удаление", message: "Удалить выбранный документ?", callback: function () {
					
						console.log(sc.selectedRow[0].Id);
                        requests.serverCall.post($http, "SparePartsCtrl/DeleteRepairs", sc.selectedRow[0].Id, function (data, status, headers, config) {
							rowsForDel = [];
							loadService();
                            console.info(data);
                        });
						;}
                    });
        } else needSelection('Удаление документа', "Для удаления необходимо выбрать документ");
	}
	
	
  //сохранить
    sc.save = function() {
        var rowsForSave = getNewRows();
        if (rowsForSave.complete) {
            saveRows(rowsForSave.rows, loadService);
        } else {
            saveError();
        }
    };
	
	function getMaxOfArray(numArray) {
   return Math.max.apply(null, numArray);
   }
	    // модальное окно с требованием выделить строку
    function needSelection(title, message) {
        modals.showWindow(modalType.WIN_MESSAGE, {
            title: title,
            message: message,
            callback: function () {
            }
        });
    }
	//печать
	 sc.printBtnClick = function() {
        if (sc.selectedRow[0].Id) {
            requests.serverCall.post($http, "SparePartsCtrl/GetPrintRepairsInfo", sc.selectedRow[0].Id, function (data, status, headers, config) {
                console.info(data);
                sc.data = data;
                setTimeout(function () {
                    var printContents = document.getElementById('SpareParts').innerHTML;
                    var popupWin = window.open('', '_blank');
                    if (typeof popupWin != 'undefined') {
                        popupWin.document.write('<html><head></head><body >' + printContents + '</html>');
                        popupWin.print();
                        popupWin.close();
                    }
                }, 1000);
            });
        } else {
            modals.showWindow(modalType.WIN_MESSAGE, {
                title: "Печать",
                message: "Для печати необходимо сохранить данные",
                callback: function () {
                }
            });
        }    
	 }
	 
	 $('#datepick').click(function(){
		 console.log('d');
	sc.filterText =  "";
	sc.filterText2 ="";
    $("#datepick").removeClass("crystalbutton");
    $("#openRepair").removeClass("crystalbutton");
     });
	 
	  $('button').click(function(){
		  $("#openRepair").removeClass("crystalbutton");
		   $("#datepick").removeClass("crystalbutton"); 
		   sc.filterText ="";
		   sc.filterText2 ="";
	  });
	 
	 sc.search = function() {
		 sc.filterText2=""; 
		if (sc.filterText.length==0) {
		$("#openRepair").removeClass("crystalbutton");
		sc.openRepair();
		} else { 
	 $("#datepick").addClass("crystalbutton");
	 $("#openRepair").addClass("crystalbutton");
		}
		
      if ($cookieStore.get('timerId2')) {
	   clearInterval($cookieStore.get('timerId2'));
   }
    $cookieStore.put('timerId2', setTimeout(GetRepairsActsByDate, 1000));	
	
	};
	 sc.search2 = function() {
		 sc.filterText=""; 
		if (sc.filterText2.length==0) {
		$("#openRepair").removeClass("crystalbutton");
		sc.openRepair();
		} else { 
	 $("#datepick").addClass("crystalbutton");
	 $("#openRepair").addClass("crystalbutton");
		}
		
      if ($cookieStore.get('timerId3')) {
	   clearInterval($cookieStore.get('timerId3'));
   }
    $cookieStore.put('timerId3', setTimeout(GetRepairsActsByDate, 1000));	
	 }

	function GetRepairsActsByDate() {
	requests.serverCall.post($http, "SparePartsCtrl/GetRepairsActsByDate", {
		PageSize: sc.pagingOptions.pageSize,
        PageNum: sc.pagingOptions.currentPage,
	    repairStartDate :  sc.formData.repairStartDate, 
        repairEndDate :    sc.formData.repairEndDate,
        Torg12: sc.formData.OpenRepair,
		Name: sc.filterText,
		CheckSearch : sc.filterText2,
		},
	    function (data, status, headers, config) {
		    sc.totalServerItems = data.CountItems;
            sc.pagingOptions.totalServerItems = data.CountItems;
            sc.TableData = fullData =  data.Values;
		});	
	}
	
	function sec1() { 
     $("div.ngFooterSelectedItems").remove();
      }
     setInterval(sec1, 200) 

}]);



