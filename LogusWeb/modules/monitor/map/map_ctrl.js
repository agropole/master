/**
 *  @ngdoc controller
 *  @name Monitor.contorller:MapCtrl
 *  @description
 *  # MapCtrl
 *  Это контроллер, определеяющий поведение карты
 *  @requires $scope
 *  @requires $http
 *  @requires requests
 *  @requires LogusConfig

 *  @property {Object} sc scope
 *  @property {Object} sc.infoData справочники формы
 *  @property {Object} sc scope
 
 *  @property {function} openInfo отображение информации в правой панели после клика на Feature на карте
 *  @property {function} layersclick отобразить/скрыть слой
 *  @property {function} searchclick поиск Feature на карте. Вызывается из правой панели
 *  @property {function} listFilter фильтр списка поиска
 *  @property {function} createWorks загрузка работ по полю для правой панели
 *  @property {function} createTech загрузка работ по техинке для правой панели
 *  @property {function} sc.statusTranslate переводим тип статуса в слова
 */
var app = angular.module("Map", ['ngTouch', 'ui.grid', 'ui.grid.selection', "services", 'ngCookies', "modalWindows"]);
app.controller("MapCtrl", ["$scope", "LogusConfig","$http", "requests","$cookieStore", function ($scope, LogusConfig, $http, requests,$cookieStore) {
    var sc = $scope;
	console.log('1');
    sc.infoData = {};
    sc.formData = [];
	var firstLoad = false;
	var curUser = $cookieStore.get('currentUser');
    var openInfo = function (type, info, check, year) {
        $("#content-container").empty();
        if (type === "Polygon" || type === "MultiPoligon") {
            sc.SayHello();
            sc.carIcon = "";
            sc.carShow = false;
            sc.plantShow = true;
            sc.titleName = "ПОЛЕ";
            sc.objName = info.name;
            sc.area = info.area + " га";
            sc.plants = info.plants;
			sc.ID = info.id;
			sc.year = year;
            console.info(sc.plants);
            createWorks(info);
            createTMC(info, year);
			createPhoto(info, year);
        } else if (type === "Point"){
            //sc.plants = info.plants;
            sc.plantShow = false;
			sc.carShow = true;
            sc.titleName = "ТЕХНИКА";
            sc.objName = info.model;
            sc.regnum = info.reg_num;
			sc.traktor = info.id;
             console.info(sc.traktor);
			if (info.icon_path!=null) {
            sc.carIcon = info.icon_path.replace("icons", "icons1");
            }
            createTech(info);
        }
		PanelInfo(check);
		 
    };
	var PanelInfo = function(check){
		if ($('.panelInfo').hasClass('openPanel')) {
			
			} else if (check === 1){
            $('.infoButton').addClass('infoButtonSelected');
			var div = document.getElementById("infoPN");
			if (div.style.display === "none") {
			div.style.display = "block ";
			}
            setTimeout(function () {
                sc.$apply(function () {
                    $('.panelInfo').addClass('openPanel');
                })
            }, 1);
        }
	};

	console.log(curUser.orgid);
	    requests.serverCall.post($http, "DriverArmCtrl/GetCoordOrg",{OrgId : curUser.orgid}, function (data, status, headers, config) {
            console.info(data);	
            initMonitoringMap(LogusConfig.serverUrl, openInfo, data);
			 }, function () {
        });

    function layersclick() {
        $('#search_menu').hide();
        if ($("#menu_layers").prop("checked")) {
            $('#layers_menu').show();
        } else {
            $('#layers_menu').hide();
        }
    }

    function searchclick() {
        $('#layers_menu').hide();
        if ($("#menu_search").prop("checked")) {
            $('#search_menu').show();
        } else {
            $('#search_menu').hide();
        }
        $("#searchlist").empty();
        var feats = getFeatures();
        for (var i= 0,li=feats.length;i<li;i++) {
            $("#searchlist").prepend(createLi(feats[i]));
        }
    }

    function listFilter() {
        var header = $("#header");
        var list = $("#searchlist");
        // create and add the filter form to the header
        var form = $("<form>").attr({"class":"filterform","action":"#"}),
            input = $("<input>").attr({"class":"filterinput","type":"text"});

        $(form).append(input).appendTo(header);
        $(input).change( function () {
            $(input).change( function () {
                var filter = $(this).val(); // get the value of the input, which we filter on
                console.log(filter);
                if (filter) {
                    $(list).find("a:not(:contains(" + filter + "))").parent().parent().slideUp();
                    $(list).find("a:contains(" + filter + ")").parent().parent().slideDown();
                } else {
                    $(list).find("li").slideDown();
                }
            });
            jQuery.expr[':'].Contains = function(a,i,m){
                return (a.textContent || a.innerText || "").toUpperCase().indexOf(m[3].toUpperCase())>=0;
            };
        }).keyup( function () {
            // fire the above change event after every letter
            $(this).change();
        });
    }
    listFilter();

    var createWorks = function(info) {
        requests.serverCall.post($http, "DriverArmCtrl/GetTop5WaysListbyFieldId",{id : info.id}, function (data, status, headers, config) {
            console.info(data);
            sc.works = data;
        }, function () {
        });
    }

    var createTech = function (info) {
		if (firstLoad==true) {
		$( ".infoButton" ).trigger("click");
		}
			firstLoad = true;
            requests.serverCall.post($http, "DriverArmCtrl/GetInfoWaysTaskByIdTraktor",{traktor_id : info.id}, function (data, status, headers, config) {
            sc.driverName = (data.driver_name) ? data.driver_name : "";
            sc.equipmentName = (data.equipment_name) ? data.equipment_name : "";
            sc.tasks = data.tasks_info;
			console.info(data.tasks_info);
			sc.plants = data.tasks_info;
			console.info(sc.plants);
			if(sc.plants[0].plant_name != null){sc.CultShow = true;}
			else{sc.CultShow = false;}
            
        }, function () {
        });
    };
  
    sc.statusTranslate = function(val) {
         switch(val) {
            case 1:
                return "выдано";
                break;
            case 2:
                return "в работе";
                break;
            case 4:
                return "закрыто";
                break;
            default:
                return "-";
        }
    };
	sc.ProgressClick = function () {
	 $location.path('/progress');	
	}

    var createTMC = function (info, year)  {
        sc.szr={};
        sc.minFert={};
        sc.seed={};
        sc.otherMater={};
        requests.serverCall.post($http, "DriverArmCtrl/GetTMCforMap",{fieldId : info.id, year: year}, function (data, status, headers, config) {
        for(var i = 0; i < data.length; i++){
            switch(data[i].MaterialType){
                case 1: sc.szr[i] = data[i]; break;
                case 2: sc.minFert[i] = data[i]; break;
                case 4: sc.seed[i] = data[i]; break;
                case 5: sc.otherMater[i] = data[i]; break;
             }
        }}, function () {});
    } 
    
	
	 var createPhoto = function (info, year)  {
		    requests.serverCall.post($http, "DriverArmCtrl/GetPhotoByFieldYear", {fieldId : info.id, year: year}, function (data, status, headers, config) {
				sc.PhotoList = data;		
       }, function () {});
		 
		 
	 }
	

    sc.SayHello = function () {
            sc.$broadcast('SpoilerHide', { hide: true });
        }

}]);