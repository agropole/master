var fieldSource, fieldLayer;
var factFieldSource, factFieldLayer;
var trackSource, trackLayer;
var techSource, techLayer;
//
//var parkVectorLayer ;
 var parkSource;
//
	//console.log('1');
var map, view;
var configUrl;
var openInfo;
var interactionSelect;
var raster, satellate;
var satel;
var yaex;
var years;
var lineText;
var typeSelect;
var draw; 
var modify;
var addInteraction;
var source;
var vector; 
var parkingArr = [];
/**
 * Overlay to show the measurement.
 * @type {ol.Overlay}
 */
var measureTooltip;
function initMonitoringMap(url,openInfoFunction, coord) {
	configUrl = url;
	openInfo = openInfoFunction;
	
	console.log(coord);
	view = new ol.View({
	//	center: ol.proj.transform([39.603334,51.761755], 'EPSG:4326', 'EPSG:3857'),
	   center: ol.proj.transform([coord[0],coord[1]], 'EPSG:4326', 'EPSG:3857'),
	    zoom: 12
	});
			
    $("#btn").hide();
	interactionSelect = new ol.interaction.Select({
	
		layerFilter: function(layer) {
			return layer.get('selectenabled');
		},
		style: function (feature, resolution) {
			var styleArray = [];
			var prop = feature.getProperties();
			switch (feature.getGeometry().getType()) {
				case "Point":
				 if (typeSelect === ''){
					var pointStyle = new ol.style.Style({
						image : new ol.style.Icon({
							src : (prop.icon_path.indexOf('/assets') != -1) ? prop.icon_path.substr(1, prop.icon_path.length-1) : prop.icon_path,
							opacity : 1
						})
					});
					var bgStyle = new ol.style.Style({
						image : new ol.style.Circle({
							radius : 26,
							
							stroke : new ol.style.Stroke({
								color : getRGB('#4ce600', 0.5),
								width : 6
							}),
							fill: new ol.style.Fill({
							color : getRGB('#ccffb3', 0.1),
						})
						})
					});
					styleArray.push(pointStyle);
					styleArray.push(bgStyle);
				}
					break;
				case "MultiPolygon":
				
				case "Polygon":
				if (prop.color!="" && prop.color != "0xffffff") {
					var polStyle = new ol.style.Style({
						//заливка полей при нажатии на поле
						fill : new ol.style.Fill({
							color : getRGB(prop.color, 0.7),
						}),
						stroke : new ol.style.Stroke({
							color : getRGB(prop.color, 1),
							width : 3
						})
					});
				} else if (prop.color==""){
					var polStyle = new ol.style.Style({
						//заливка полей при нажатии на поле
						fill : new ol.style.Fill({
							color : getRGB("#C0C0C0", 0.7),
						}),
						stroke : new ol.style.Stroke({
							color : getRGB("#C0C0C0", 1),
							width : 3
						})
					});
					
				}

					styleArray.push(polStyle);
					if (prop.name) {
						var text1 = new ol.style.Style({
							text : createFieldTextStyle(prop.name, resolution, 'top')
						});
						styleArray.push(text1);
					}
					if (prop.plant) {
						var text2 = new ol.style.Style({
							text : createFieldTextStyle(prop.plant, resolution, 'bottom', 1)
						});
						styleArray.push(text2);
					}
					break;
				default:
					break;
			}
			return styleArray;
		}
	 
	});

	
	map = new ol.Map({
		loadTilesWhileInteracting: true,
		//layers: [satellate],
		target: 'map',
		view:view,
		
		interactions: ol.interaction.defaults().extend([
			interactionSelect
		]),
	});
       
	
	raster = new ol.layer.Tile({
		visible:false,
		source: new ol.source.OSM({
			//url:  'file://mnt/sdcard/logus_map/{z}/{x}/{y}.png'
		})
	});
	map.addLayer(raster);
 
	satellate = new ol.layer.Group({
            visible: true,
			preload: Infinity,
            layers: [
			        new ol.layer.Tile({
                    source:new ol.source.TileArcGISRest({
            		url: 'http://server.arcgisonline.com/arcgis/rest/services/World_Imagery/MapServer',
					 crossOrigin: "Anonymous"
					
                })})
            ]
        });
		map.addLayer(satellate);
	
		yaex = [-20037508.342789244, -20037508.342789244, 20037508.342789244, 20037508.342789244];
		proj4.defs('EPSG:3395', '+proj=merc +lon_0=0 +k=1 +x_0=0 +y_0=0 +datum=WGS84 +units=m +no_defs');
		ol.proj.get('EPSG:3395').setExtent(yaex);

		satel = new ol.layer.Tile({
			//opacity: 0.5,
            
        source: new ol.source.XYZ({
        //    url: 'http://agropole.logus-vrn.ru/bin/tiles.fcgi?l=photo&x={x}&y={y}&z={z}&zo=1',
		 url:  'http://agropole.logus-agro.ru/bin/tiles.fcgi?l=photo&x={x}&y={y}&z={z}&zo=1',
		//	crossOrigin: "Anonymous",
            projection: 'EPSG:3395',
            tileGrid: ol.tilegrid.createXYZ({
				
                extent: yaex
            })
        }),
        visible: true
    });
	map.addLayer(satel);

	
	
	trackSource = new ol.source.Vector({
		projection : 'EPSG:3857'
	});

	trackLayer = new ol.layer.Vector({
		selectenabled: false,
		source : trackSource,
		style : createStyle()
	});
	map.addLayer(trackLayer);
	
	map.on('click', featureClick);
	

	var wgs84Sphere = new ol.Sphere(6378137);

  source = new ol.source.Vector();

/**
 * Currently drawn feature.
 * @type {ol.Feature}
 */
var sketch;
var sketch1;


/**
 * The help tooltip element.
 * @type {Element}
 */
var helpTooltipElement;


/**
 * Overlay to show the help messages.
 * @type {ol.Overlay}
 */
var helpTooltip;


/**
 * The measure tooltip element.
 * @type {Element}
 */
var measureTooltipElement;


//
  var element = document.getElementById('popup');

      var popup = new ol.Overlay({
        element: element,
        positioning: 'bottom-center',
        stopEvent: false,
        offset: [0, -50]
      });
      map.addOverlay(popup);
//

/**
 * Message to show when the user is drawing a polygon.
 * @type {string}
 */
var continuePolygonMsg = 'Укажите другую точку для подсчета площади';


/**
 * Message to show when the user is drawing a line.
 * @type {string}
 */
var continueLineMsg = 'Укажите другую точку маршрута';


/**
 * Handle pointer move.
 * @param {ol.MapBrowserEvent} evt The event.
 */
var pointerMoveHandler = function(evt) {
  if (evt.dragging) {
    return;
  }
  /** @type {string} */
  var helpMsg = 'Нажмите чтобы начать рисовать';

  if (sketch) {
    var geom = (sketch.getGeometry());
    if (geom instanceof ol.geom.Polygon) {
      helpMsg = continuePolygonMsg;
    } else if (geom instanceof ol.geom.LineString) {
      helpMsg = continueLineMsg;
    }
  }

  helpTooltipElement.innerHTML = helpMsg;
  helpTooltip.setPosition(evt.coordinate);

  helpTooltipElement.classList.remove('hidden');
};
/**
 * Format length output.
 * @param {ol.geom.LineString} line The line.
 * @return {string} The formatted length.
 */
var formatLength = function(line) {
  var length;
  
    var coordinates = line.getCoordinates();
    length = 0;
    var sourceProj = map.getView().getProjection();
    for (var i = 0, ii = coordinates.length - 1; i < ii; ++i) {
      var c1 = ol.proj.transform(coordinates[i], sourceProj, 'EPSG:4326');
      var c2 = ol.proj.transform(coordinates[i + 1], sourceProj, 'EPSG:4326');
      length += wgs84Sphere.haversineDistance(c1, c2);
    }
  
  
  var output;
  if (length > 1000) {
    output = (Math.round(length / 1000 * 100) / 100) +
        ' ' + 'км  ';
  } else {
    output = (Math.round(length * 100) / 100) +
        ' ' + 'м ';
  }
  return output ;
};


/**
 * Format area output.
 * @param {ol.geom.Polygon} polygon The polygon.
 * @return {string} Formatted area.
 */
var formatArea = function(polygon) {
  var area;
  
    var sourceProj = map.getView().getProjection();
    var geom = /** @type {ol.geom.Polygon} */(polygon.clone().transform(
        sourceProj, 'EPSG:4326'));
    var coordinates = geom.getLinearRing(0).getCoordinates();
    area = Math.abs(wgs84Sphere.geodesicArea(coordinates));
    area = area / 10000 * 10;
    area = Math.round(area) / 10;
  var output;
 
  output = area + " га"
  return output;
};


 addInteraction = function() {
  if (typeSelect == ''){
	   DrawOnOff();
	   clearSource(source);
	   map.removeOverlay(measureTooltip);
	   map.removeOverlay(helpTooltip);
	  
  }
  else{
  DrawOnOff();
  clearSource(source);
  map.on('pointermove', pointerMoveHandler);

	map.getViewport().addEventListener('mouseout', function() {
	helpTooltipElement.classList.add('hidden');
	});
  var type = (typeSelect == 'area' ? 'Polygon' : 'LineString');
  modify = new ol.interaction.Modify({
                //features: featureOverlay.getSource().getFeatures(),
				source: source,
				deleteCondition: function(event) {
                    return ol.events.condition.shiftKeyOnly(event) &&
                        ol.events.condition.singleClick(event);
                }
            });
			map.addInteraction(modify);
			//
	  var style1 = new ol.style.Style ({
      fill: new ol.style.Fill({
        color: 'rgba(255, 255, 255, 0.2)'
      }),
      stroke: new ol.style.Stroke({
        color: 'rgba(255, 255, 255, 0.7)',
        lineDash: [10, 10],
        width: 4
      }),
      image: new ol.style.Circle({
        radius: 5,
        stroke: new ol.style.Stroke({
          color: 'rgba(0, 0, 0, 0.7)'
        }),
       fill: new ol.style.Fill({
         color: 'rgba(255, 255, 255, 0.4)'
        })
      })
    });
			//
			
			
    draw = new ol.interaction.Draw({
    source: source,
    type: /** @type {ol.geom.GeometryType} */ (type),
    style: style1
  });
  map.addInteraction(draw); 

  createMeasureTooltip();
  createHelpTooltip();

  var listener;
  var listener1;
       
	  draw.on('drawstart',
      function(evt) {
        // set sketch
		createMeasureTooltip();
			console.log('drawstart');		
        sketch = evt.feature;
		sketch1 = evt.feature;
		clearSource(source);
      
        var tooltipCoord = evt.coordinate;
		
        listener = sketch.getGeometry().on('change', function(evt) {
          var geom = evt.target;
          var output;
		  //====
		//   console.log('change');	
	     	sketch.setStyle(style1); 
		  //
		 
          if (geom instanceof ol.geom.Polygon) {
            output = formatArea(geom);// +" " + formatLength(new ol.geom.LineString(geom.getLinearRing(0).getCoordinates()));
            tooltipCoord = geom.getInteriorPoint().getCoordinates();
          } else if (geom instanceof ol.geom.LineString) {
            output = formatLength(geom);
            tooltipCoord = geom.getLastCoordinate();
          }
          measureTooltipElement.innerHTML = output;
		  lineText = output;
          measureTooltip.setPosition(tooltipCoord);
        });
		
      }, this);
     
      draw.on('drawend',
      function() {
		 
		    var style2 = new ol.style.Style({
				
		    stroke : new ol.style.Stroke({
								color : "#ffcc33",
								width : 4
							}),
		   });
		  sketch.setStyle(style2); 
		    
		measureTooltipElement.innerHTML = lineText + '  <button id = drawBtn class = "delete-draw">x</button>'
		document.getElementById("drawBtn").addEventListener("click", DeleteDraw);
        measureTooltipElement.className = 'tooltipDraw tooltipDraw-static';
        //measureTooltip.setOffset([0, -7]);
        // unset sketch
    //    sketch = null;
        // unset tooltip so that a new one can be created
        //measureTooltipElement = null;
        //createMeasureTooltip();
        ol.Observable.unByKey(listener);
      }, this);
	  
	modify.on('modifystart',
      function(evt) {
        // set sketch
        sketch = sketch1;
         console.log('modifystart');
        /** @type {ol.Coordinate|undefined} */
        var tooltipCoord = evt.coordinate;
       
		measureTooltipElement.className = 'tooltipDraw tooltipDraw-measure';
        listener1 = sketch.getGeometry().on('change', function(evt) {
          var geom = evt.target;
          var output;
          if (geom instanceof ol.geom.Polygon) {
            output = formatArea(geom);// +" " + formatLength(new ol.geom.LineString(geom.getLinearRing(0).getCoordinates()));
            
            tooltipCoord = geom.getInteriorPoint().getCoordinates();
          } else if (geom instanceof ol.geom.LineString) {
            output = formatLength(geom);
            tooltipCoord = geom.getLastCoordinate();
          }
          measureTooltipElement.innerHTML = output;
		  lineText = output;
          measureTooltip.setPosition(tooltipCoord);
        });
      }, this);

  modify.on('modifyend',
      function() {
		      console.log('modifyend');
        measureTooltipElement.className = 'tooltipDraw tooltipDraw-static';
		measureTooltipElement.innerHTML = lineText + '  <button id = drawBtn class = "delete-draw">x</button>'
		document.getElementById("drawBtn").addEventListener("click", DeleteDraw);
        ol.Observable.unByKey(listener);
      }, this);
  }
}



/**
 * Creates a new help tooltip
 */
 
function createHelpTooltip() {
  if (helpTooltipElement) {
    helpTooltipElement.parentNode.removeChild(helpTooltipElement);
  }
  helpTooltipElement = document.createElement('div');
  helpTooltipElement.className = 'tooltipDraw hidden';
  helpTooltip = new ol.Overlay({
    element: helpTooltipElement,
    offset: [15, 0],
    positioning: 'center-left'
  });
  map.addOverlay(helpTooltip);
}


/**
 * Creates a new measure tooltip
 */
function createMeasureTooltip() {
  if (measureTooltipElement) {
    measureTooltipElement.parentNode.removeChild(measureTooltipElement);
  }
  measureTooltipElement = document.createElement('div');
  measureTooltipElement.className = 'tooltipDraw tooltipDraw-measure';
  measureTooltip = new ol.Overlay({
    element: measureTooltipElement,
    offset: [0, -15],
    positioning: 'bottom-center'
  });
  map.addOverlay(measureTooltip);
}




vector = new ol.layer.Vector({
  source: source,
  style: new ol.style.Style({
    fill: new ol.style.Fill({
      color: 'rgba(255, 255, 255, 0.2)'
    }),
    stroke: new ol.style.Stroke({
      color: '#ffcc33',
      width: 4
    }),
    image: new ol.style.Circle({
      radius: 7,
      fill: new ol.style.Fill({
        color: '#ffcc33'
      })
    })
  })
});

map.addLayer(vector);

	
	function featureClick(e) {		
		if (typeSelect === '')  {
		$(element).popover('destroy');
		var pixel = e.pixel;
		var f = map.forEachFeatureAtPixel(pixel, function(feature, layer) {
			//
			if (feature.get('type') == 'sign') {
	    	 var coordinates = feature.getGeometry().getCoordinates();
            popup.setPosition(coordinates);
            $(element).popover({
            placement: 'top',
            html: true,
            content: feature.get('name')
          });
          $(element).popover('show');
			} 
			
			if (feature.getProperties().icon_path || feature.getProperties().plants) {
				return feature;
			} else {
				return f;
			}
		});
		if (f) {
			updateDate();
			if (f.getGeometry().getType() === "Polygon") {
				var geom = f.getGeometry().getInteriorPoint().getCoordinates();
				//
				for (var i = 0, li = parkingArr.length; i < li; i++) {
				clearSource(parkingArr[i]);
			}
				//
			} else if (f.getGeometry().getType() === "Point") {
				var geom = f.getGeometry().getCoordinates();
				//
				for (var i = 0, li = parkingArr.length; i < li; i++) {
				clearSource(parkingArr[i]);
			}
				//
				
			}
			clearSource(trackSource);
		    if (f.getProperties().track_id) {
				getAndDrawTrack(f.getProperties());
			}
			openInfo(f.getGeometry().getType(), f.getProperties(), 1, years);
		  }  else {if ($('.panelInfo').hasClass('openPanel')) {
		//	  $( ".infoButton" ).trigger("click");
            $('.infoButton').removeClass('infoButtonSelected');
            $('.panelInfo').removeClass('openPanel');
			var div = document.getElementById("infoPN");
			if (div.style.display !== "none") {
			div.style.display = "none";
			}
			
        }}
	  }
	  
	}
	
	addLayer("field", (new Date()).getFullYear());
	addLayer("tech", (new Date()).getFullYear(), true);
}

 function flyTo(location, done) {
        var duration = 1000;
        var zoom = 16;
       	var parts = 2;
        var called = false;
        function callback(complete) {
          --parts;
          if (called) {
            return;
          }
          if (parts === 0 || !complete) {
            called = true;
            done(complete);
          }
        }
        view.animate({
          center: location,
          duration: duration
		  
        }, callback);
        view.animate({
          zoom: zoom - 2,
          duration: duration / 2
        }, {
          zoom: zoom,
          duration: duration / 2
        }, callback);
      }


function animateMap(meta, bounceFlag) {
	
	var start = +new Date();
	    
	if (bounceFlag) {
		flyTo(meta, function() {});
	}
	 else {
	  flyTo(meta, function() {});
	}

}

function createStyle() {
	return function(feature, resolution) {
		var styleArray = [];
		var prop = feature.getProperties();
		switch (feature.getGeometry().getType()) {
			case "MultiPolygon":
			case "Polygon":
				feature.selectable = false;
				//console.log(prop.color);
				if (prop.color == "0xffffff"){
					var fillStyle = new ol.style.Style({
					stroke : new ol.style.Stroke({
						color : getRGB('#02FA20', 1),
						width : 3,
						lineDash : [10]
					})
				});
				} else if (prop.color!="" && prop.color != "0xffffff") {
				var fillStyle = new ol.style.Style({
					//заливка полей
					fill : new ol.style.Fill({
						color : getRGB(prop.color, 0.4),
					}),
				
					stroke : new ol.style.Stroke({
						color : getRGB(prop.color, 1),
						width : 2
					})
				});
				}else if (prop.color == "") {
					var fillStyle = new ol.style.Style({
					//заливка полей
					stroke : new ol.style.Stroke({
						color : getRGB("#C0C0C0", 1),
						width : 2
					})
				});		
										
				}
				styleArray.push(fillStyle);
				if (prop.name) {
					var text1 = new ol.style.Style({
						text : createFieldTextStyle(prop.name, resolution, 'top')
					});
					styleArray.push(text1);
				}
				if (prop.plant) {
					var text2 = new ol.style.Style({
						text : createFieldTextStyle(prop.plant, resolution, 'bottom', 1)
					});
					styleArray.push(text2);
				}
				break;
			case "Point":
				if (prop.icon_path) {
					var iconStyle = new ol.style.Style({
						image : new ol.style.Icon({
							src : (prop.icon_path.indexOf('/assets') != -1) ? prop.icon_path.substr(1, prop.icon_path.length-1) : prop.icon_path,
							opacity : 1
						})
					});
				//	console.info(prop.icon_path);
					styleArray.push(iconStyle);
				} else {
					var bgStyle = new ol.style.Style({
						image : new ol.style.Circle({
							radius : 30,
							fill : new ol.style.Fill({
								color : getRGB('0xc64f52', 0.9)
							}),
							stroke : new ol.style.Stroke({
								color : '#780407',
								width : 3
							})
						}),
						text : createTextStyle(feature, resolution, 'center')
					});
					styleArray.push(bgStyle);
				}
				break;
			case "LineString":
				var fillStyle = new ol.style.Style({
					stroke : new ol.style.Stroke({
						color : getRGB(prop.color, 1),
						width : "2"
					})
				});
				styleArray.push(fillStyle);
				break;
			default:
				break;
		}
		return styleArray;
	};
}
function DrawOnOff() {
  map.removeInteraction(draw);
  map.removeInteraction(modify);
}
    function DeleteDraw() {
      var txt;
      var r = confirm("Удалить все отметки?");
      if (r == true) {
       clearSource(source);
	   map.removeOverlay(measureTooltip);
          sketch = null;
     } 
}

//text polygon style
function createFieldTextStyle(t, resolution, pos) {
	var textStyle = new ol.style.Text({
		textAlign : 'center',
		textBaseline : 'alphabetic',
		font : (pos === 'top') ? 'normal 18px DINPro' : 'normal 14px DINPro',
		text : (resolution > 16) ? '' : t,
		fill : new ol.style.Fill({
			color : "#000"
		}),
		stroke: new ol.style.Stroke({color: '#fff', width: 5}),
		offsetY : (pos === 'top') ? -15 : 0,
		offsetX : 0,
	});
	return textStyle;
}

var getRGB = function(c, alpha) {
	var color = String(c);
	var HexToR = parseInt(color.substring(2, 4), 16);
	var HexToG = parseInt(color.substring(4, 6), 16);
	var HexToB = parseInt(color.substring(6, 8), 16);
	var rgbaColor = "rgba(" + HexToR + ", " + HexToG + ", " + HexToB + "," + alpha + ")";
	return rgbaColor;
};

function getAndDrawTrack(info) {
	var fs = trackSource.getFeatures();
	for (var i = 0,il = fs.length; i < il; i += 1) {
		source.removeFeature(fs[i]);
	}
	var q = configUrl + 'DriverArmCtrl/GetTop100GPSbyTrackId';
	$.ajax({
		type : "POST",
		url : q,
		data : {
			track_id : info.track_id,
			take_num: 100
		},
		success : function(resp) {
			var coords = [];
			for (var i = 0,
					 li = resp.Coord.length; i < li; i++) {
						var r = resp.Coord[i];
						var coord = r.coord.replace('POINT (', '').replace(')', '');
						coords.push([parseFloat(coord.split(' ')[0]), parseFloat(coord.split(' ')[1])]);
			}
			var fLine = {
				type : "Feature",
				geometry : {
					type : "LineString",
					coordinates : coords
				},
				"properties" : {
					color : "0xFF0000"
				}
			};

			var featCol = {
				"type" : "FeatureCollection",
				"features" : [fLine]
			};
			addTrack(featCol);
		},
		error : function(request, error) {
			console.log(arguments);
		}
	});
	console.log('featureClick ' + q);
	console.log(info.track_id);
}

function addTrack(track) {
	if (trackLayer) {
		map.removeLayer(trackLayer);
	}
	var fs = track.features;
	for (var i=0,li=fs.length;i<li;i++) {
		var f = fs[i];
		if (f.geometry.type === "Point"){
			var newPoint = f.geometry.coordinates;
			if (_lastPoint) {
				_trackDistance += distance(_lastPoint, newPoint);
			}
			_lastPoint = newPoint;
			f.geometry.coordinates = ol.proj.transform(f.geometry.coordinates, 'EPSG:4326', 'EPSG:3857');
		} else if (f.geometry.type === "LineString") {
			f.geometry.coordinates = transformArrayCoordinates(f.geometry.coordinates);
		} else {
			f.geometry.coordinates = [transformArrayCoordinates(f.geometry.coordinates[0])];
		}
	}
	trackSource = new ol.source.Vector({
		features: (new ol.format.GeoJSON()).readFeatures(track)
	});
	trackLayer = new ol.layer.Vector({
		source: trackSource,
		style : createStyle(),
	});
	map.addLayer(trackLayer);
}

function transformArrayCoordinates(ar) {
	var newAr = [];
	for (var i=0,li=ar.length;i<li;i++) {
		newAr.push(ol.proj.transform(ar[i], 'EPSG:4326', 'EPSG:3857'));
	}
	return newAr;
}

function addLayer(l, year, listBool) {
	if (l === "field") {
		if (fieldLayer) {
			removeLayer("field");
		}
		typeSelect = '';
		years = year;
		fieldSource = new ol.source.Vector({
			url: configUrl + "DriverArmCtrl/GetFieldsGeoJsonCoordinaties?year=" + year,
			format: new ol.format.GeoJSON()
		});
		console.log(fieldSource);
		fieldLayer = new ol.layer.Vector({
			source: fieldSource,
			style : createStyle()
		});
		
		if (fieldLayer != undefined ) {
		map.addLayer(fieldLayer);
		}
	}else if (l === "line") {
		if (typeSelect === 'line') {
			typeSelect = '';
			addInteraction();
		}
		else {typeSelect = 'line';
		map.removeLayer(vector);
		map.addLayer(vector);
		addInteraction();}
	}else if (l === "area") {
		if (typeSelect === 'area') {
			typeSelect = '';
			addInteraction();
			map.removeLayer(vector);
		}
		else {typeSelect = 'area';
		addInteraction();}
	   } else if (l === "fact") {
		if (factFieldLayer) {
			removeLayer("fact");
		}
		years = year;
		factFieldSource = new ol.source.Vector({
			url: configUrl + "DriverArmCtrl/GetFieldsGeoFactJsonCoordinaties?year=" + year,
			format: new ol.format.GeoJSON()
		});
		console.log(factFieldSource);
		factFieldLayer = new ol.layer.Vector({
			source: factFieldSource,
			style : createStyle()
		});
		map.addLayer(factFieldLayer);
	}else if (l === "schem"){ 
		raster.setVisible(true);
		satellate.setVisible(false);   
		satel.setVisible(false);
	}
	else if (l === "satel"){
		raster.setVisible(true);
		raster.setVisible(false);
		satellate.setVisible(true);      
		satel.setVisible(true);
	}	else if (l === "tech") {
		if (techLayer) {
			removeLayer("tech");
		}
		
		console.log(configUrl + "DriverArmCtrl/GetTraktorsGeoJsonInfo");
		techSource = new ol.source.Vector({
			url: configUrl + "DriverArmCtrl/GetTraktorsGeoJsonInfo",
			crossOrigin: "Anonymous",
			format: new ol.format.GeoJSON()
		});
		if (listBool) {
			techSource.addEventListener('addfeature', function addfeatlistener(e) {
				var fs = techSource.getFeatures();
				for (var i = 0, li = fs.length; i < li; i++) {
					var f = fs[i];
					if (f.getProperties().id === 40) {
						techSource.removeEventListener('addfeature', addfeatlistener);
						console.log(i + " " + f.getProperties().id);
						openInfo(f.getGeometry().getType(), f.getProperties(), 0, years);
						view.setCenter(f.getGeometry().getCoordinates());
						getAndDrawTrack(f.getProperties());
					}
				}
			});
		}
		techLayer = new ol.layer.Vector({
			source: techSource,
			style : createStyle()
		});
		map.addLayer(techLayer);
	}

}
function removeLayer(l) {
	var sel = interactionSelect.getFeatures();
	while (sel.getLength() > 0) {
		sel.removeAt(0);
	};
	if (l === "field") {
		map.removeLayer(fieldLayer);
		fieldLayer = null;
		fieldSource = null;
		if (techLayer) {
			map.removeLayer(techLayer);
			map.addLayer(techLayer);
		}
	} else if (l === "tech") {
		clearSource(trackSource);
		map.removeLayer(techLayer);
		techLayer = null;
		techSource = null;
	}
	else if (l === "fact"){
		map.removeLayer(factFieldLayer);
		factFieldLayer = null;
		factFieldSource = null;
		if (techLayer) {
			map.removeLayer(techLayer);
			map.addLayer(techLayer);
		}
	}
}

function clearSource(source) {
	
	var fs = source.getFeatures();
		if (fs.length != 0) {
		    var style3 = new ol.style.Style({		
		    stroke : null
		   });
		  fs[0].setStyle(style3); 
		}
	for (var i = 0, il = fs.length; i < il; i += 1) {
    	source.removeFeature(fs[i]);
	}
	
}

function getFeatures() {
	var feats = [];
	if (fieldSource) {
		var fs1 = getLayerFeats(fieldSource, 1);
		feats = fs1;
	}
	if (techSource) {
		var fs2 = getLayerFeats(techSource, 2);
		if (feats.length === 0) {
			feats = fs2;
		} else {
			feats = feats.concat(fs2);
		}
	}
	if (factFieldSource){
		var fs3 = getLayerFeats(factFieldSource, 1);
		feats = fs3;
	}
	console.info(feats.length);
	return feats;
}

function getLayerFeats (source, type) {
	var feats = [];
	if (source) {
		var fs = source.getFeatures();
		for (var i = 0, li = fs.length; i < li; i++) {
			var f = fs[i].getProperties();
			if (f.reg_num || f.name) {
			//	console.log(f.icon_path);
				feats.push({
					Id: [f.id, type],
					Name: (type === 2 ? (f.reg_num + " " + (f.model ? f.model : "")) : (f.name + (f.plant ? " (" + f.plant  + ")": "")))
				});
			}
		}
	}
	console.info(feats);
	return feats;
}

function mapsearch(tag) {
	var type = tag[1];
	var id = tag[0];
	var source = type === 1 ? fieldSource : techSource;
	$('#search_menu').hide();
	var fs = source.getFeatures();
	console.log(fs.length + " " + id);
	for (var i = 0, li = fs.length; i < li; i++) {
		var f = fs[i];
		console.log(f.getProperties().id + " " + id);
		if (f.getProperties().id === id) {
			if (f.getGeometry().getType() === "Point") {
				animateMap(f.getGeometry().getCoordinates(), true);
			} else {
				animateMap(f.getGeometry().getInteriorPoint().getCoordinates(), true);
			}
			openInfo(f.getGeometry().getType(), f.getProperties(), 0, years);
		}
	}
}

function refreshTech() {
	removeLayer("tech");
	addLayer("tech");
}

function getGPStrack( id, start_time, end_time, stop, parking) {
	clearSource(trackSource);
			for (var i = 0, li = parkingArr.length; i < li; i++) {
				clearSource(parkingArr[i]);
			}
	var q = configUrl + 'DriverArmCtrl/GetGPSbyDate';
	$.ajax({
		type : "POST",
		url : q,
		data : {
			track_id : id,
			start_time: start_time,
			end_time: end_time,
			stop : stop,
			parking: parking,
		},
		success : function(resp) {
			var coords = [];
			for (var i = 0, li = resp.length; i < li; i++) {
				var r = resp[i];
				var coord = r.coord.replace('POINT (', '').replace(')', '');
				coords.push([parseFloat(coord.split(' ')[0]), parseFloat(coord.split(' ')[1])]);
							
			}
			var fLine = {
				type : "Feature",
				geometry : {
					type : "LineString",
					coordinates : coords
				},
				"properties" : {
					color : "0xFF0000"
				}
			};

			var featCol = {
				"type" : "FeatureCollection",
				"features" : [fLine]
			};
			addTrack(featCol);
			//===============================================
				for (var i = 0, li = resp.length; i < li; i++) {
				var r = resp[i];
				var coord = r.coord.replace('POINT (', '').replace(')', '');			 
	 if (parking!= true && r.parking == true) {continue;}
	 if (stop != true && r.stop == true) {continue;}
	 if (r.parking == true || r.stop == true) {
	 var park = new ol.Feature({
		geometry: new ol.geom.Point(
		ol.proj.fromLonLat([parseFloat(coord.split(' ')[0]), parseFloat(coord.split(' ')[1])]) ,
		),  
				   name: r.massage,
				   type: 'sign'
		});
		
		var typeSign = r.parking == true ? 'images/ic_parking.png' : 'images/ic_stop.png';
		if (r.stop == true) {
			var df = 0;
		}
		
		
		pointStyle = new ol.style.Style({
						image : new ol.style.Icon({
							src : typeSign,
							opacity : 1
						})
					});
	
		park.setStyle(pointStyle);
		 parkSource = new ol.source.Vector({
			features: [park]
		});
	   parkingArr.push(parkSource);
	 var parkVectorLayer = new ol.layer.Vector({
		source: parkSource,
	});
	  map.addLayer(parkVectorLayer);
	 }
		//==================================================				
			}
			
		},
		error : function(request, error) {
			console.log(arguments);
		}
	});
	console.log('featureClick ' + q);
	
}


function updateDate (){
		var currentdate = new Date(); 
		var startTime;
		var endTime;
		if (currentdate.getHours() > 7 && currentdate.getHours() < 19)
		{
			startTime = currentdate.getDate() + "-" 
						+ ((currentdate.getMonth()+1) < 10 ? "0"+(currentdate.getMonth()+1) : (currentdate.getMonth()+1))+ "-" 
						+ currentdate.getFullYear()+ " " 
						+ "07:00";
			endTime =    currentdate.getDate() + "-" 
						+ ((currentdate.getMonth()+1) < 10 ? "0"+(currentdate.getMonth()+1) : (currentdate.getMonth()+1))+ "-" 
						+ currentdate.getFullYear()+ " " 
						+ "19:00";
		}
		else
		{
			startTime = currentdate.getDate() + "-" 
						+ ((currentdate.getMonth()+1) < 10 ? "0"+(currentdate.getMonth()+1) : (currentdate.getMonth()+1))+ "-" 
						+ currentdate.getFullYear()+ " " 
						+ "19:00";
			endTime = (currentdate.getDate()+1) + "-" 
						+ ((currentdate.getMonth()+1) < 10 ? "0"+(currentdate.getMonth()+1) : (currentdate.getMonth()+1))+ "-" 
						+ urrentdate.getFullYear()+ " " 
						+ "07:00";
		}
		document.getElementById('StartDate').value = startTime;
		document.getElementById('EndDate').value = endTime;
		$("#StartDate").datetimepicker('update');
		$("#EndDate").datetimepicker('update');
	}