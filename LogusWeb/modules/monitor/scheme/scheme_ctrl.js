/**
 *  @ngdoc controller
 *  @name Monitor.contorller:SchemeCtrl
 *  @description
 *  # SchemeCtrl
 *  Это контроллер, определеяющий поведение формы схемы. Только отображает картинку. Не делает ничего
 *  @requires $scope
 *  @property {Object} sc scope
 */
var app = angular.module("Scheme", ['ngTouch', 'ui.grid', 'ui.grid.selection', "services", 'ngCookies', "modalWindows"]);
app.controller("SchemeCtrl", ["$scope", function ($scope) {

}]);