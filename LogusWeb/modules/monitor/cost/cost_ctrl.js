/**
 *  @ngdoc controller
 *  @name Monitor.contorller:CostCtrl
 *  @description
 *  # CostCtrl
 *  Это контроллер, определеяющий поведение формы с графиками План-факторного анализа по годам
 *  @requires $scope
 *  @requires $location
 *  @requires $http
 *  @requires requests
 *  @requires $cookieStore
 *  @requires $rootScope
 *  @requires LogusConfig

 *  @property {Object} sc scope
 
 *  @property {function} loadPlanFact запрос на данные план-факторного анализа
 *  @property {function} showPlanFact отображение графиков результатов план-факторного анализа
 *  @property {function} loadReport запрос на данные по производству за год
 *  @property {function} refreshCharts отображение графиков результатов по производству за год
 *  @property {function} initMainPie инициализация круговой диаграммы структуры затрат
 *  @property {function} setMainPieData отображение отображение данных на круговой диаграмме
 *  @property {function} sc.report выгрузка отчета

 */
var app = angular.module("Cost", ['ngTouch', 'ui.grid', 'ui.grid.selection', "services", 'ngCookies', "modalWindows"]);
app.controller("CostCtrl", ["$scope", "$http", "requests", "$location", '$cookieStore', "$rootScope", 'LogusConfig', function ($scope, $http, requests, $location, $cookieStore, $rootScope, LogusConfig) {
    var sc = $scope;
  sc.formData = [];
   var count =0;
	 sc.currentYear = new Date().getFullYear();
    sc.yearsList = [];
    sc.formData.yearId = sc.currentYear;
    for (var i = 0; i < 20; i++) {
        sc.yearsList.push({Id: (sc.currentYear - i), Name: '' + (sc.currentYear - i) + ''});
    };
	//меняем год
   sc.refreshYear = function () {
	   if (count != 0) {
       console.log(sc.formData.yearId);
	    var year = sc.formData.yearId;
		  requests.serverCall.post($http, "CostCtrl/PlanFact", {year}, function (data, status, headers, config) {
            console.info(data);
           showPlanFact(data);
        }, function () {
        });
		//
		  requests.serverCall.post($http, "ReportsCtrl/GetExpensesList",{year}, function (data, status, headers, config) {
            console.log(data);
            initColumn();
            initMainPie();
            reportData = data;
            refreshCharts(true);
        }, function () {
        });
		
	   }
      count++;
	  
	  sc.IsBig = false;
	  
    };
          sc.IsBig = false;
	      sc.bigReport = function () {
			  if (sc.IsBig) {
		 $(".cultCol2").css('height', '41px');   //38.89   37.78
		  $(".headerAll").html('Статьи затрат (факт/план)');

			  } else {
            $(".cultCol2").css('height', '30px'); 
		   $(".headerAll").html('Статьи затрат (факт)');

			  }
			  
	  }

 if ($rootScope.roleid != 12)
	{
	 $("#btn").hide();
	}
    function showPlanFact(data) {
        var d =[
            {
                name: "План по тех. картам",
                points: [
                    {x: 'Всего', y: data.plan.cost},
                    {x: 'Зар.плата', y: data.plan.cost_salary},
                    {x: 'Топливо', y: data.plan.cost_fuel},
                    {x: 'СЗР', y: data.plan.cost_szr},
                    {x: 'Мин.удобрения', y: data.plan.cost_min},
                    {x: 'Семена', y: data.plan.cost_seed},
					{x: 'Услуги', y: data.plan.service_total},
				    {x: 'Пр. материалы', y: data.plan.dop_material_fact},	
                ]
            },
            {
                name: "Свод фактических затрат",
                points: [
                    {x: 'Всего', y: data.fact.cost},
                    {x: 'Зар.плата', y: data.fact.cost_salary},
                    {x: 'Топливо', y: data.fact.cost_fuel},
                    {x: 'СЗР', y: data.fact.cost_szr},
                    {x: 'Мин.удобрения', y: data.fact.cost_min},
                    {x: 'Семена', y: data.fact.cost_seed},
					{x: 'Услуги', y: data.fact.service_total},
					{x: 'Пр. материалы', y: data.fact.dop_material_fact},
                ]
            }
        ];
        initColumnSeria("#plan_fact", d);
    }
    
    var reportData;


    var tableFieldData;
    var szrCost = [1228,1228, 648,14894,687, 839,0, 1393, 330, 1380, 1334,563, 305,740,4237,230,1380,1072,1380,230,0,0,0];
    function refreshCharts(recount) {
        var data = [];
        var index;
        for (var i= 0,li=reportData.length; i<li;i++) {
            data.push(reportData[i]);
            if (data[i].plant_id === 1) {
                index = i;
            }
        }
        sc.tableFieldData = data;
        setDataColumn(data, 'plant_name');
        setMainPieData(data);
    }

    var initMainPie = function () {
        $("#mainpie").ejChart({
            load: "loadTheme",
            seriesRendering:"seriesRender",
            canResize: true,
            commonSeriesOptions: {
                border :{width:2, color:'white'},
                enableAnimation : true,
                labelPosition:'outsideExtended',
                smartLabelEnabled:true,
                startAngle:145,
                showTooltip : true,
                tooltip :
                {
                    visible:true,
                    template: 'Tooltip'
                }
            },
            pointRegionClick : function() {
                console.log('pieclick');
                setTimeout(function() {
                    var pointIndex = sender.data.region.Region.PointIndex;
                    var d = reportData[pointIndex].plant_id;
                    var curUser = $cookieStore.get('currentUser');
                    curUser.currentRole = 112;
                    $cookieStore.put('currentUser', curUser);
                    $rootScope.roleid = 112;
                    $location.path('/culture/' + d);
                }, 10);
            },
            legend : {
                visible: true,
                shape: 'circle',
                position: 'bottom',
                font : {
                    fontFamily : "DINPro"
                },
                format:  "#point.x# : #point.y#%"
            },
            columnCount: 2,
            enable3D: false,
            enableRotation:false,
            depth: 30,
            tilt: -30,
            rotation: -30,
            perspectiveAngle: 90
        });

        function seriesRender(sender)
        {
            console.log('seriesRender');
            if(sender.model.theme=="flatdark" || sender.model.theme=="gradientdark")
                sender.data.series.marker.dataLabel.connectorLine.color="white";
        }


    };

    var setMainPieData = function(data) {
        var d = [];
        for (var i= 0,li=data.length-1;i<li;i++) {
            if (data[i].plant_id != 36) {
                if (!data[i].salary_total) {
                    data[i].salary_total = 0;
                }
                if (!data[i].salary_dop) {
                    data[i].salary_dop = 0;
                }
                if (!data[i].szr_fact) {
                    data[i].szr_fact = 0;
                }
                if (!data[i].razx_top) {
                    data[i].razx_top = 0;
                }
                if (!data[i].minud_fact) {
                    data[i].minud_fact = 0;
                }
				 if (!data[i].service_total) {
                    data[i].service_total = 0;
                }
				 if (!data[i].dop_material_fact) {
                    data[i].dop_material_fact = 0;
                }
                var o = {
                    x: data[i].plant_name,
                    y: Math.round(data[i].salary_total + data[i].szr_fact + data[i].razx_top + data[i].minud_fact  + data[i].posevmat_fact+ 
					data[i].service_total+data[i].dop_material_fact)
                };
                d.push(o);
            }
        }
        var s = {
            series : [{
                type: 'pie',
                points:d
            }]
        };
        console.info(s);
        $("#mainpie").ejChart("option", {
            "drilldown" : s
        });
    };

    //выгрузка в excel
    sc.report = function () {  
	var popupWin = window.open(LogusConfig.serverUrl + 'CostCtrl/PlanFactAnalysis?year=' + sc.formData.yearId + '&isBig=' + sc.IsBig
	
	
	, '_blank');
		
    };
}]);



