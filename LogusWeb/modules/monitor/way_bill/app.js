//отдельная форма без Angular для отображения деталей путевого листа
(function () {
	var taskId = getUrlParameter('task_id');
	var configUrl = getUrlParameter('config');
	loadMap(taskId, configUrl);
	console.log(taskId);
	loadInfo(taskId, configUrl);

	function getUrlParameter(sParam)
	{
		var sPageURL = window.location.search.substring(1);
		var sURLVariables = sPageURL.split('&');
		for (var i = 0; i < sURLVariables.length; i++)
		{
			var sParameterName = sURLVariables[i].split('=');
			if (sParameterName[0] == sParam)
			{
				return sParameterName[1];
			}
		}
	}

	function loadInfo(taskId, config) {
		var id_task = taskId;
		if (id_task && id_task != ' ') {
			var url = config + 'DriverArmCtrl/GetTaskInfoById';
			console.log(url);
			console.log('task_id ' + taskId);
			$.ajax({
				type : "POST",
				url: url,
				data : {
					id : taskId
				},
			}).success(
				function onLoad(e) {
					console.log(JSON.stringify(e));
					options = e;
					//task
					if (e.field_name) $("#field").text(e.field_name);
					if (e.plant_name) $("#plant").text(e.plant_name);
					if (e.available_name) $("#responsible").text(e.available_name);
					if (e.task_name) $("#task").text(e.task_name);

					if (e.consumption_rate) $("#consumption_rate").text(e.consumption_rate);
					if (e.consumption) $("#consumption").text(e.consumption);
					if (e.consumption_fact) $("#consumption_fact").text(e.consumption_fact);

					if (e.volume_fact) $("#volume_fact").text(e.volume_fact);
					if (e.area) $("#area").text(e.area);
					if (e.work_time) $("#time_fact").text(e.work_time);
					if (e.work_day) $("#time_standart").text(e.work_day);
					if (e.moto_hours) $("#time_fact").text(e.moto_hours);

					if (e.price_days) $("#rate").text(e.price_days);
					if (e.salary) $("#salary").text(e.salary);

					if (e.list_num) $("#way-bill-nomer").text("ПУТЕВОЙ ЛИСТ №" + e.list_num);
					if (e.date_begin) $("#date-way-bill").text(e.date_begin.substr(0,10));
					if (e.driver_name) $("#driver").text(e.driver_name);
					if (e.traktor_brand) $("#car").text(e.traktor_brand);
					if (e.equipment_name) $("#trailer").text(e.equipment_name);
					if (e.traktor_number) $("#gos-nomer").text(e.traktor_number);
				}
			).error(
				function onError(e, st, we) {
					console.warn(e);
					console.warn(st);
					console.warn(we);
				}
			);
		}
	}

	function loadMap(id, config) {
		var raster = new ol.layer.Tile({
			source: new ol.source.OSM()
		});
		var url = config + 'WaysListsCtrl/GetCloseInfoForMap?task_id=' + id;
		console.log(url);
		var fieldSource = new ol.source.Vector({
			url: url,
			format: new ol.format.GeoJSON()
		});
		fieldSource.addEventListener('addfeature', function(e) {
			var fs = fieldSource.getFeatures();
			for (var i= 0,li=fs.length;i<li;i++) {
				var f = fs[i];
				if (f.getGeometry().getType() === 'Polygon') {
					var geom = f.getGeometry().getInteriorPoint().getCoordinates();
					console.log(JSON.stringify(geom));
					map.getView().setCenter(geom);
				}
			}
		});

		var fieldsLayer = new ol.layer.Vector({
			source: fieldSource,
			style : createFieldStyle(),
		});

		var map = new ol.Map({
			layers: [raster,fieldsLayer],
			target: 'map',
			view: new ol.View({
				center: ol.proj.transform([39.603334,51.761755], 'EPSG:4326', 'EPSG:3857'),
				zoom: 14
			})
		});

		function getRGB(c, alpha) {
			var color = String(c);
			var HexToR = parseInt(color.substring(2, 4), 16);
			var HexToG = parseInt(color.substring(4, 6), 16);
			var HexToB = parseInt(color.substring(6, 8), 16);
			var rgbaColor = "rgba(" + HexToR + ", " + HexToG + ", " + HexToB + "," + alpha + ")";
			return rgbaColor;
		};

		function createFieldStyle() {
			return function(feature, resolution) {
				var styleArray = [];
				var prop = feature.getProperties();
				feature.selectable = false;
				var fillStyle = new ol.style.Style({
					fill : new ol.style.Fill({
						color : getRGB(prop.color, 0.2),
					}),
					stroke : new ol.style.Stroke({
						color : getRGB(prop.color, 1),
						width : "4"
					}),
				});
				styleArray.push(fillStyle);
				if (prop.name) {
					var text1 = new ol.style.Style({
						text : createFieldTextStyle(prop.name, resolution, 'top')
					});
					styleArray.push(text1);
				}
				if (prop.name) {
					var text2 = new ol.style.Style({
						text : createFieldTextStyle(prop.plant, resolution, 'bottom', 1)
					});
					styleArray.push(text2);
				}
				return styleArray;
			};
		}

		function createFieldTextStyle(t, resolution, pos) {
			var textStyle = new ol.style.Text({
				textAlign : 'center',
				textBaseline : 'alphabetic',
				font : (pos === 'top') ? 'normal 18px DINPro' : 'normal 14px DINPro',
				text : t,
				fill : new ol.style.Fill({
					color : "#000"
				}),
				stroke: new ol.style.Stroke({color: '#fff', width: 5}),
				offsetY : (pos === 'top') ? -15 : 0,
				offsetX : 0,
			});
			return textStyle;
		};
	}
}());







