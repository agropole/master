//отдельная форма без Angular для отображения деталей путевого листа
(function () {
	var plantId = getUrlParameter('ID');
	var configUrl = getUrlParameter('config');
	var year = getUrlParameter('y');
	var fieldId = getUrlParameter('id');
	loadInfoField(year,fieldId);
	
	
	loadMap(configUrl, year, fieldId);
	console.log(plantId);
	loadInfo(plantId, configUrl, year, fieldId);
	
	function changePlant() {
		
		var d = document.getElementById("mySelect").value;
		loadInfo(d,configUrl, year, fieldId);
		
			
	}

	function getUrlParameter(sParam)
	{
		var sPageURL = window.location.search.substring(1);
		var sURLVariables = sPageURL.split('&');
		for (var i = 0; i < sURLVariables.length; i++)
		{
			var sParameterName = sURLVariables[i].split('=');
			if (sParameterName[0] == sParam)
			{
				return sParameterName[1];
			}
		}
	}

	function loadInfoField(year, fieldId){
		var url = configUrl + 'DriverArmCtrl/GetPlantOnField?year='+year+'&fieldId='+fieldId;
		console.log(url);
		$.ajax({
				type : "POST",
				url: url,
				data : {
					//id : taskId
				},
			}).success(
			function onLoad(e) {
					console.log(JSON.stringify(e));
					options = e;
					//task
					//var Array= e;
				$("#TitleField").text('Прогресс по полю '+e[0].FieldName);
				$("#fieldName").text('ПОЛЕ '+e[0].FieldName);				
				var myDiv = document.getElementById("myDiv");

				//Create array of options to be added
				var array = e;

				//Create and append select list
				var selectList = document.createElement("select");
				selectList.setAttribute("id", "mySelect");
				myDiv.appendChild(selectList);

				//Create and append the options
				for (var i = 0; i < array.length; i++) {
					var option = document.createElement("option");
					option.setAttribute("value", array[i].PlantId);
					option.text = array[i].PlantName;
					selectList.appendChild(option);
					}
				document.getElementById("mySelect").addEventListener("change", changePlant);
				}
				
			).error(
				function onError(e, st, we) {
					console.warn(e);
					console.warn(st);
					console.warn(we);
				}
			);
	}
	
	function loadInfo(plantId, config, year, fieldId) {
		var id_plant = plantId;
		if (id_plant && id_plant != ' ') {
			var url = config + 'DriverArmCtrl/GetFieldInfoById?id='+plantId+'&year='+year+'&fieldId='+fieldId;
			console.log(url);
			console.log('id_plant ' + id_plant);
			console.log('year ' + year);
			console.log('fieldId ' + fieldId);
			$.ajax({
				type : "POST",
				url: url,
				data : {
				},
			}).success(
				function onLoad(e) {
					console.log(JSON.stringify(e));
					options = e;
					//task
					var Array= e;
					//Array.length = N;
					var html='<table class="white-text margin-left-10" cellpadding="7" border = "0">'+
					'<tr><td align="left" style="border-bottom:2px solid #f6b43c; ">Список работ</td>'+
					'<td align="center" style="border-bottom:2px solid #f6b43c; ">Дата(план)</td>'+
					'<td align="center" style="border-bottom:2px solid #f6b43c; ">Дата(факт)</td></tr>';
					for(var i=0; i< Array.length; i++)
					{ 	
						if(Array[i].DateT1 < Array[i].Date){
						html+='<tr>';
						html+='<td align="left" style = "color:'+Array[i].colorgroup+';  border-bottom:2px solid #f6b43c;">'+Array[i].TaskName+'</td>'+
						'<td align="left" style="border-bottom:2px solid #f6b43c; ">'+Array[i].datePlan+'</td>'+
						'<td align="center" bgcolor="#fe9c9f" style="border-bottom:2px solid #f6b43c; ">'+Array[i].dateFact+'</td></tr>';						
						}
						else if(Array[i].DateT > Array[i].Date){
						html+='<tr>';
						html+='<td align="left" style = "color:'+Array[i].colorgroup+';  border-bottom:2px solid #f6b43c;">'+Array[i].TaskName+'</td>'+
						'<td align="left" style="border-bottom:2px solid #f6b43c; ">'+Array[i].datePlan+'</td>'+
						'<td align="center" bgcolor="#CBA023" style="border-bottom:2px solid #f6b43c; ">'+Array[i].dateFact+'</td></tr>';						
						}
						else {html+='<tr>';
						html+='<td align="left" style = "color:'+Array[i].colorgroup+';  border-bottom:2px solid #f6b43c;">'+Array[i].TaskName+'</td>'+
						'<td align="left" style="border-bottom:2px solid #f6b43c; ">'+Array[i].datePlan+'</td>'+
						'<td align="center" style="border-bottom:2px solid #f6b43c; ">'+Array[i].dateFact+'</td></tr>';}
						
						
					}
					html+='</table><span class="orange-text margin-left-3"></span>'; 
					document.getElementById('xlop').innerHTML=html; 
				}
				
			).error(
				function onError(e, st, we) {
					console.warn(e);
					console.warn(st);
					console.warn(we);
				}
			);
		}
	}

	function loadMap(config, year,fieldId) {
		var raster = new ol.layer.Tile({
			source: new ol.source.OSM()
		});
		var yaex = [-20037508.342789244, -20037508.342789244, 20037508.342789244, 20037508.342789244];
		proj4.defs('EPSG:3395', '+proj=merc +lon_0=0 +k=1 +x_0=0 +y_0=0 +datum=WGS84 +units=m +no_defs');
		ol.proj.get('EPSG:3395').setExtent(yaex);
		satel = new ol.layer.Group({
            visible: true,
			preload: Infinity,
            layers: [
			        new ol.layer.Tile({
                    source:new ol.source.TileArcGISRest({
            		url: 'http://server.arcgisonline.com/arcgis/rest/services/World_Imagery/MapServer'
                })}),
				new ol.layer.Tile({
				source: new ol.source.XYZ({
				url: 'http://agropole.logus-vrn.ru/bin/tiles.fcgi?l=photo&x={x}&y={y}&z={z}&zo=1',
				projection: 'EPSG:3395',
				crossOrigin: "Anonymous",
				tileGrid: ol.tilegrid.createXYZ({			
                extent: yaex
				})			
        })})
            ]
        });
		
		var url = config + 'DriverArmCtrl/GetFieldCoordinatGeoProgress/?year=' + year+'&fieldId='+fieldId;
		console.log(url);
		var fieldSource = new ol.source.Vector({
			url: url,
			format: new ol.format.GeoJSON()
		});
		fieldSource.addEventListener('addfeature', function(e) {
			var fs = fieldSource.getFeatures();
			for (var i= 0,li=fs.length;i<li;i++) {
				var f = fs[i];
				if (f.getGeometry().getType() === 'Polygon') {
					var geom = f.getGeometry().getInteriorPoint().getCoordinates();
					console.log(JSON.stringify(geom));
					map.getView().setCenter(geom);
				}
			}
		});

		var fieldsLayer = new ol.layer.Vector({
			source: fieldSource,
			style : createFieldStyle(),
		});

		var map = new ol.Map({
			layers: [satel,fieldsLayer],
			target: 'map',
			view: new ol.View({
				center: ol.proj.transform([39.603334,51.761755], 'EPSG:4326', 'EPSG:3857'),
				zoom: 15
			})
		});

		function getRGB(c, alpha) {
			var color = String(c);
			var HexToR = parseInt(color.substring(2, 4), 16);
			var HexToG = parseInt(color.substring(4, 6), 16);
			var HexToB = parseInt(color.substring(6, 8), 16);
			var rgbaColor = "rgba(" + HexToR + ", " + HexToG + ", " + HexToB + "," + alpha + ")";
			return rgbaColor;
		};

		function createFieldStyle() {
			return function(feature, resolution) {
				var styleArray = [];
				var prop = feature.getProperties();
				feature.selectable = false;
				var fillStyle = new ol.style.Style({
					fill : new ol.style.Fill({
						color : getRGB(prop.color, 0.2),
					}),
					stroke : new ol.style.Stroke({
						color : getRGB(prop.color, 1),
						width : "4"
					}),
				});
				styleArray.push(fillStyle);
				if (prop.name) {
					var text1 = new ol.style.Style({
						text : createFieldTextStyle(prop.name, resolution, 'top')
					});
					styleArray.push(text1);
				}
				if (prop.name) {
					var text2 = new ol.style.Style({
						text : createFieldTextStyle(prop.plant, resolution, 'bottom', 1)
					});
					styleArray.push(text2);
				}
				return styleArray;
			};
		}

		function createFieldTextStyle(t, resolution, pos) {
			var textStyle = new ol.style.Text({
				textAlign : 'center',
				textBaseline : 'alphabetic',
				font : (pos === 'top') ? 'normal 18px DINPro' : 'normal 14px DINPro',
				text : t,
				fill : new ol.style.Fill({
					color : "#000"
				}),
				stroke: new ol.style.Stroke({color: '#fff', width: 5}),
				offsetY : (pos === 'top') ? -15 : 0,
				offsetX : 0,
			});
			return textStyle;
		};
	}
}());







