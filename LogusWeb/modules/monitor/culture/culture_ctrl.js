/**
 *  @ngdoc controller
 *  @name Monitor.contorller:CultureCtrl
 *  @description
 *  # CultureCtrl
 *  Это контроллер, определеяющий поведение формы с графиками План-факторного анализа по годам
 *  @requires $scope
 *  @requires $http
 *  @requires requests

 *  @property {Object} sc scope
 
 *  @property {function} loadCosts запрос на план-факторный анализ и затраты по культуре
 *  @property {function} setTableData отображение сводной таблице по культуре
 *  @property {function} recountTable запрос на данные по производству за год
 *  @property {function} showPlanFact отображение графиков план-факторного анализа
 *  @property {function} calcInvest НЕ ИСПОЛЬЗУЕТСЯ. расчет инвестиционнного алгоритма

 */
var app = angular.module("Culture", ['ngTouch', 'ui.grid', 'ui.grid.selection', "services", 'ngCookies', "modalWindows"]);
app.controller("CultureCtrl", ["$scope", "$http", "requests", function ($scope, $http, requests) {
    
    var sc = $scope;
    sc.infoData = {};
     $("#btn").hide();
    $scope.$on('myCustomEvent', function (event, data) {
        loadCosts(data);
    });
  
    var reportData;
    var dopData;
     var loadCosts = function(d) {
         requests.serverCall.post($http, "CostCtrl/PlanFact", d, function (data, status, headers, config) {
            console.info(data);
			
			
			sc.total_cost = data.fact.cost.toFixed(2); //итого факт
	 		sc.ga_cost = (data.fact.cost/data.fact.area).toFixed(2);    //факт на га
	 		sc.t_cost = (data.fact.cost/data.fact.gross_harvest).toFixed(2); //факт на т
			if (sc.ga_cost==Infinity || isNaN(sc.ga_cost)) {sc.ga_cost='нет данных';}
			if (sc.t_cost==Infinity || isNaN(sc.t_cost)) {sc.t_cost='нет данных';}
			
	 		sc.ga_cost_plan = (data.plan.cost/data.plan.area).toFixed(2);
	 		sc.t_cost_plan = (data.plan.cost/data.plan.gross_harvest).toFixed(2);
			if (sc.ga_cost_plan==Infinity || isNaN(sc.ga_cost_plan)) {sc.ga_cost_plan='нет данных';}
			if (sc.t_cost_plan==Infinity || isNaN(sc.t_cost_plan)) {sc.t_cost_plan='нет данных';}
            showPlanFact(data);
            dopData = data;
            initPieDetail();
            setPieDetailData(dopData.fact);
             requests.serverCall.post($http, "ReportsCtrl/GetExpensesList",d, function (data, status, headers, config) {
                 console.info(data);
                 $("#title_plant").text("Оценка затрат по культуре - " + data[0].plant_name);
                 reportData = data;
                 initColumn();
                setDataColumnField(reportData, 'fields_name');
                setTableData();
            }, function () {
            });
       }, function () {
        });
   };

    var area = 0, gross = 0, total = 0;
    var setTableData = function() {
        sc.tableFieldData = [];

        for (var i= 0, li=reportData.length; i<li; i++) {
            var d = reportData[i];
            var s = d.minud_fact + d.posevmat_fact + d.razx_top + d.salary_total + d.szr_fact;
            total += s;
            area += d.area;
            var gh = d.gross_harvest / 1000;
            gross += gh;
			//таблица по полям
            var item = {
                name: d.fields_name,
                total: s ? s.toFixed(2) : 0,
                perarea : d.area ? (s / d.area).toFixed(2) : 0,
                pergross: d.gross_harvest ? (s / gh ).toFixed(2) : 0,
                productivity: d.area ? gh.toFixed(2) : 0,
				harvest : d.area ? (gh/d.area).toFixed(2) : 0
				
            };
            sc.tableFieldData.push(item);
			
        }
        recountTable();
    };

    var recountTable = function() {
        console.log('RECOUNT');
        console.log("check " + (dopData != null));
        if (dopData && dopData.plan) {
            sc.total_cost_plan = dopData.plan.cost.toFixed(2);
            sc.total_cost_delta = (sc.total_cost_plan - sc.total_cost).toFixed(2);
            sc.ga_cost_delta = (sc.ga_cost_plan - sc.ga_cost).toFixed(2);
            sc.t_cost_delta =  (sc.t_cost_plan - sc.t_cost).toFixed(2); 
			if (sc.ga_cost_delta==Infinity || isNaN(sc.ga_cost_delta)) {sc.ga_cost_delta='нет данных';}
			if (sc.t_cost_delta==Infinity || isNaN(sc.t_cost_delta)) {sc.t_cost_delta='нет данных';}
        }
    };

    var showPlanFact = function(data) {
        var d =[
            {
                name: "План по тех. картам",
                points: [
                    {x: 'Всего', y: data.plan.cost},
                    {x: 'Зар.плата', y: data.plan.cost_salary},
                    {x: 'Топливо', y: data.plan.cost_fuel},
                    {x: 'СЗР', y: data.plan.cost_szr},
                    {x: 'Мин.удобрения', y: data.plan.cost_min},
                    {x: 'Семена', y: data.plan.cost_seed},
					{x: 'Услуги', y: data.plan.service_total},
					{x: 'Пр. материалы', y: data.plan.dop_material_fact},	
                ]
            },
            {
                name: "Свод фактических затрат",
                points: [
                    {x: 'Всего', y: data.fact.cost},
                    {x: 'Зар.плата', y: data.fact.cost_salary},
                    {x: 'Топливо', y: data.fact.cost_fuel},
                    {x: 'СЗР', y: data.fact.cost_szr},
                    {x: 'Мин.удобрения', y: data.fact.cost_min},
                    {x: 'Семена', y: data.fact.cost_seed},
					{x: 'Услуги', y: data.fact.service_total},
					{x: 'Пр. материалы', y: data.fact.dop_material_fact},
                ]
            }
        ];
        recountTable();
        console.log(JSON.stringify(d));
        initColumnSeria("#plan_fact", d);

        if (data.plan.area && data.fact.area) {
            var d2 = [
                {
                    name: "План по тех. картам",
                    points: [
                        {x: 'Всего', y: Math.round(data.plan.cost / data.plan.area * 100) / 100},
                        {x: 'Зар.плата', y: Math.round(data.plan.cost_salary / data.plan.area * 100) / 100},
                        {x: 'Топливо', y: Math.round(data.plan.cost_fuel / data.plan.area * 100) / 100},
                        {x: 'СЗР', y: Math.round(data.plan.cost_szr / data.plan.area * 100) / 100},
                        {x: 'Мин.удобрения', y: Math.round(data.plan.cost_min / data.plan.area * 100) / 100},
                        {x: 'Семена', y: Math.round(data.plan.cost_seed / data.plan.area * 100) / 100},
					    {x: 'Услуги', y: Math.round(data.plan.service_total / data.plan.area * 100) / 100},
						{x: 'Пр. материалы', y: Math.round(data.plan.dop_material_fact / data.plan.area * 100) / 100},
                    ]
                },
                {
                    name: "Свод фактических затрат",
                    points: [
                        {x: 'Всего', y: Math.round(data.fact.cost / data.fact.area * 100) / 100},
                        {x: 'Зар.плата', y: Math.round(data.fact.cost_salary / data.fact.area * 100) / 100},
                        {x: 'Топливо', y: Math.round(data.fact.cost_fuel / data.fact.area * 100) / 100},
                        {x: 'СЗР', y: Math.round(data.fact.cost_szr / data.fact.area * 100) / 100},
                        {x: 'Мин.удобрения', y: Math.round(data.fact.cost_min / data.fact.area * 100) / 100},
                        {x: 'Семена', y: Math.round(data.fact.cost_seed / data.fact.area * 100) / 100},
					    {x: 'Услуги', y: Math.round(data.fact.service_total / data.fact.area * 100) / 100},	
				    	{x: 'Пр. материалы', y: Math.round(data.fact.dop_material_fact / data.fact.area * 100) / 100},		
                    ]
                }
            ];
            console.log(JSON.stringify(d2));
            initColumnSeria("#plan_fact_ga", d2, '', true);
        }

        if (data.plan.gross_harvest) {
            var d3 = [
                {
                    name: "План по тех. картам",
                    points: [
                        {x: 'Всего', y: Math.round(data.plan.cost / data.plan.gross_harvest * 100) / 100},
                        {x: 'Зар.плата', y: Math.round(data.plan.cost_salary / data.plan.gross_harvest * 100) / 100},
                        {x: 'Топливо', y: Math.round(data.plan.cost_fuel / data.plan.gross_harvest * 100) / 100},
                        {x: 'СЗР', y: Math.round(data.plan.cost_szr / data.plan.gross_harvest * 100) / 100},
                        {x: 'Мин.удобрения', y: Math.round(data.plan.cost_min / data.plan.gross_harvest * 100) / 100},
                        {x: 'Семена', y: Math.round(data.plan.cost_seed / data.plan.gross_harvest * 100) / 100},
				        {x: 'Услуги', y: Math.round(data.plan.service_total / data.plan.gross_harvest * 100) / 100},	
				    	{x: 'Пр. материалы', y: Math.round(data.plan.dop_material_fact / data.plan.gross_harvest * 100) / 100},		
                    ]
                },
                {
                    name: "Свод фактических затрат",
                    points: [
                        {x: 'Всего', y: data.fact.gross_harvest ? Math.round(data.fact.cost / data.fact.gross_harvest * 100) / 100 : 0},
                        {x: 'Зар.плата', y: data.fact.gross_harvest ? Math.round(data.fact.cost_salary / data.fact.gross_harvest * 100) / 100 : 0},
                        {x: 'Топливо', y: data.fact.gross_harvest ? Math.round(data.fact.cost_fuel / data.fact.gross_harvest * 100) / 100 : 0},
                        {x: 'СЗР', y: data.fact.gross_harvest ? Math.round(data.fact.cost_szr / data.fact.gross_harvest * 100) / 100 : 0},
                        {x: 'Мин.удобрения', y: data.fact.gross_harvest ? Math.round(data.fact.cost_min / data.fact.gross_harvest * 100) / 100 : 0},
                        {x: 'Семена', y: data.fact.gross_harvest ? Math.round(data.fact.cost_seed / data.fact.gross_harvest * 100) / 100 : 0},
					    {x: 'Услуги', y: data.fact.gross_harvest ? Math.round(data.fact.service_total/ data.fact.gross_harvest * 100) / 100 : 0},
					    {x: 'Пр. материалы', y: Math.round(data.fact.dop_material_fact / data.plan.gross_harvest* 100) / 100},			
                    ]
                }
            ];
            console.log(JSON.stringify(d3));
            initColumnSeria("#plan_fact_t", d3, '', true);
        }
    }

    function calcInvest(val) {
        var VS_o = val.sbor; //валовый сбор освоенной технологии, ц
        var VS_i = val.sbor * 1.5; //валовый сбор инновационной технологии, ц
        var Spos = val.Value.Area; //площадь посева освоенной технологии, га

        var U_o = VS_o / Spos; //урожайность освоенной технологии, га/ц
        var U_i = VS_i / Spos; //урожайность инновационной технологии, га/ц

        var PU = Spos * (U_i - U_o); // прибавка урожая

        initBar("#pu", [
            {points: [{x: "Валовый сбор по освоенной технологии, ц", y: VS_o}]},
            {points: [{x: "Валовый сбор по инновационной технологии, ц", y: VS_i}]},
            {points: [{x: "Прибавка урожая, ц", y: PU}]},
        ], "Прибавка урожая");

        var Zot = val.Value.LaborCosts;
        var Ztr = val.Value.Spenttime;
        var Zmat = 9000000; //TODO учет накладных
        var Znakl = 6000000;

        var Zpr = Zot + Zmat + Znakl; //прибавка урожая

        //затраты на единицу продукции по основной технологии, руб/га
        var Zpr_ed_o = Zpr / VS_o;
        var Zot_ed_o = Zot / VS_o;
        var Zmat_ed_o = Zmat / VS_o;

        //затраты на единицу продукции по основной технологии, руб/га
        var Zpr_ed_i = Zpr / VS_i;
        var Zot_ed_i = Zot / VS_i;
        var Zmat_ed_i = Zmat / VS_i;

        var Zreal = 1000000; //затраты на реализацию

        var Zobw = Zpr + Zreal; //общие затраты
        var d = [
            {
                x: 'Затраты на оплату труда, руб',
                y: Zot
            }, {
                x: 'Материальные затраты (семена, удобрения, СЗР, ГСМ, электроэнергия, амортизация, ремонт), руб',
                y: Zmat
            }, {
                x: 'Накладные расходы, руб',
                y: Znakl
            }
            , {
                x: 'Затраты на реализацию',
                y: Zreal
            }
        ];

        $("#total_costs").text(Math.round(Zobw) + " руб");
        initPie('#cost_structure', d, 'left');
        initFunnel('#revenue', d, '');

        //себестоимость
        var S_o = Zobw / VS_o;
        var S_i = Zobw / VS_i;

        //цена
        var C_o = 700;
        var C_i = 720;

        initColumn3('#col1', [{points: [{x: 'освоенная', y: Math.round(U_o * 100) / 100}]}, {
            points: [{
                x: 'инновационная',
                y: Math.round(U_i * 100) / 100
            }]
        }], 'Урожайность, ц/га');
        initColumn3('#col2', [{points: [{x: 'освоенная', y: Math.round(S_o * 100) / 100}]}, {
            points: [{
                x: 'инновационная',
                y: Math.round(S_i * 100) / 100
            }]
        }], 'Себестоимость, руб/ц');
        initColumn3('#col3', [{points: [{x: 'освоенная', y: Math.round(C_o * 100) / 100}]}, {
            points: [{
                x: 'инновационная',
                y: Math.round(C_i * 100) / 100
            }]
        }], 'Цена за 1 ц');

        var Qreal = VS_i * 0.5; //объем реализованной продукции

        //выручка
        var V_o = C_o * Qreal;
        var V_i = C_i * Qreal;
        $("#revenue_value").text(Math.round(V_i * 100) / 100, + " руб");
        //прибыль
        var P_o = V_o - Zobw;
        var P_i = V_i - Zobw;
        $("#profit_value").text(Math.round(P_i * 100) / 100 + " руб");

        //рентабельность
        var R_o = P_o / Zobw * 100;
        var R_i = P_i / Zobw * 100;

        //производительность
        var PR_o = VS_o / Ztr;
        var PR_i = VS_i / Ztr;

        //валовый доход
        var VD_o = V_o - Zmat;
        var VD_i = V_i - Zmat;
        //на ед. площади
        var VD_S_o = VD_o / Spos;
        var VD_S_i = VD_i / Spos;
        //на ед. продукции
        var VD_VS_o = VD_o / VS_o;
        var VD_VS_i = VD_i / VS_i;
        //на ед. затрат труда
        var VD_Ztr_o = VD_o / Ztr;
        var VD_Ztr_i = VD_i / Ztr;

        sc.table1 = [
            {
                "name": "Валовый доход, руб",
                "plan": Math.round(VD_o * 100) / 100,
                "fact": Math.round(VD_i * 100) / 100
            }, {
                "name": "Валовый доход на единицу площади, руб/га",
                "plan": Math.round(VD_S_o * 100) / 100,
                "fact": Math.round(VD_S_i * 100) / 100
            }, {
                "name": "Валовый доход с единицы продукции, руб/ц",
                "plan": Math.round(VD_VS_o * 100) / 100,
                "fact": Math.round(VD_VS_i * 100) / 100
            }, {
                "name": "Валовый доход с единицы труда, руб/чел.-час",
                "plan": Math.round(VD_Ztr_o * 100) / 100,
                "fact": Math.round(VD_Ztr_i * 100) / 100
            }
        ];

        //окупаемость затрат
        var O_o = PR_o / Zobw;
        var O_i = PR_i / Zobw;

        //годовой экономический эффект
        var E = ((C_i - S_i) - (C_o - S_o)) * Qreal;
        var E_u = (C_o - S_o) * (U_i - U_o); //за счет прироста урожая
        var E_s = (S_o - S_i) * U_i; //за счет снижения себестоимости
        var E_c = (C_i - C_o) * U_i; //за счет повышения цены

        sc.table2 = [
            {
                "name": "Годовой экономический эффект",
                "value": Math.round(E * 100) / 100
            }, {
                "name": "за счет прироста урожая",
                "value": Math.round(E_u * 100) / 100
            }, {
                "name": "за счет снижения себестоимости",
                "value": Math.round(E_s * 100) / 100
            }, {
                "name": "за счет повышения цены",
                "value": Math.round(E_c * 100) / 100
            }
        ];

        var VS_pu = PU * Spos; //валовый сбор прибавки урожая
        var Vreal_pu = VS_pu * C_i; //выручка от прибавки урожая

        var A = 100000; //амортизация
        var Nnds = VD_i * 0.18; //налог на добавленную стоимость
        var Pl = 1000000; //иные платежи из прибыли

        //прибыль налогооблагаемая
        var Pnal_i = VD_i - (A + Nnds);
        var Pnal_o = VD_o - (A + Nnds);
        //чистый доход
        var ChD_i = VD_i - (A + Nnds + Pl);
        var ChD_o = VD_o - (A + Nnds + Pl);

        initBar("#income_and_expenses", [
            {
                points: [{x: 'Валовый доход', y: VD_i}]
            },
            {
                points: [{x: 'Затраты (амортизация, НДС, иные платежи)', y: (A + Nnds + Pl)}]
            },
            {
                points: [{x: 'Чистый доход', y: ChD_i}]
            },
        ], "Соотношение доходов-расходов, руб");


        $("#total_costs2").text(Math.round((A + Nnds + Pl)*100) / 100 + " руб");
        initPie('#expenses_structure', [
            {
                x: 'Иные платежи',
                y: Pl
            }, {
                x: 'Амортизация',
                y: A
            }, {
                x: 'НДС',
                y: Nnds
            }
        ], 'bottom', 'pie');


        //рентабельность производства продукции
        var Rpr_i = Math.round(Pnal_i / Zobw * 100);
        var Rpr_o = Math.round(Pnal_o / Zobw * 100);

        $("#rent_costs").text(Rpr_i + "%");

        // $("#profitability_pr").text(Rpr_i + "%");

        //окупаемость затрат
        var Oz_i = VD_i / Zobw;
        var Oz_o = VD_o / Zobw;

        var I = 18000000; //величина исходной инвестиции
        var Zi = 2000000; //дополнительные затраты по инновационной технологии и кредиту
        var Ri = Math.round(ChD_i / (I + Zi + A) * 100); //рентабельность инновационных вложений

        $("#profitability_i").text(Ri + "%");

        var CF_plus = [V_i, V_i * 1.2, V_i * 1.4, V_i * 1.6, V_i * 1.8, V_i * 2]; //положительный денежный поток
        var Zinvest = I + Zi + Pl;
        var CF_minus = [Zinvest, Zinvest * 0.5, Zinvest * 0.1, 0, 0, 0]; //отрицательный денежный поток

        var NCF = []; //поток чистых платежей
        var K = []; //коэффициент дисконтирования
        var Dp = [], Dv = []; //дисконтированные поступления и выплаты
        var Dp_sum = 0, Dv_sum = 0; //сумма дисконтированных поступлений и выплат
        var PVt = []; //дисконтированные выплаты
        var NPV = 0;
        var b = 0.1;
        var p1 = [], p2 = [], p3 = [];
        for (var i = 0, li = CF_plus.length; i < li; i++) {
            NCF.push(CF_plus[i] - CF_minus[i]);
            K.push(1 / Math.pow((1 + b), i));
            Dp.push(CF_plus[i] * K[i]);
            Dp_sum += CF_plus[i] * K[i];
            Dv.push(CF_minus[i] * K[i]);
            Dv_sum += CF_minus[i] * K[i];
            PVt.push(NCF[i] * K[i]);
            NPV += NCF[i] / Math.pow((1 + b), i);
            p1.push({x: i, y: CF_plus[i]});
            p2.push({x: i, y: CF_minus[i] * -1});
            p3.push({x: i, y: NCF[i]});
        }

        //cash_flow
        initStack("#cash_flow", [{points: p1}, {points: p2}]);
        initLine("#payments_flow", [{points: p3}]);
        var PIz = Math.round(Dp_sum / Dv_sum);
        var PIi = Math.round(Dp_sum / I);

        var PP = Math.ceil(I / CF_plus[0]);

        $("#npv").text(Math.round(NPV));
        $("#irr").text('23%');
        $("#pp").text(CF_plus.length + ' лет');
        $("#piz").text(PIz);
        $("#pii").text(PIi);

        /*var _minDelta, _minI;
         for (var j = 0; j < 100; j = j + 0.2) {
         var npv_test = 0;
         for (var i = 0, li = NCF.length; i < li; i++) {
         npv_test += NCF[i] / Math.pow((1 + i), i);
         }
         if (!_minDelta || npv_test < _minDelta) {
         _minDelta = npv_test;
         _minI = i;
         }
         }
         $("#irr").text(_minI);*/
    }
}]);