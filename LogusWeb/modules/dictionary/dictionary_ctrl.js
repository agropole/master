/**
 *  @ngdoc controller
 *  @name app.controller:DictionaryCtrl
 *  @description
 *  # DictionaryCtrl
 *  Это контроллер позволяющий пользователю работать со списком "Dictionary"
 *  @requires $scope
 *  @requires $http
 *  @requires requests
 *  @requires $location
 *   @requires $cookieStore
 *  @requires $rootScope
 *  @requires modals
 *  @requires modalType
 *  @requires preloader
 *  @requires $stateParams
 *  @requires map
 *  @property {function} selectDictionary:функция Которая позволяет выбрать справочник
 *  @property {function} loadNewDic:функция Которая загружыет выбранный справочник в таблицу
 *  @property {function} delRow = function(row):функция Которая удаляет строку из таблицы
 *  @property {function} update:функция Которая позволяет изменять данные в таблице  
 *  @property {function} addRowForUpdate:функция Которая добавляет измененную строку в базу  
 *  @property {function} create:функция Которая создаёт строку в таблице  
 *  @property {function} cancel:функция Которая отменяет изминения внесенные в таблицу  
 *  @property {function} submitForm:функция Которая вызывается по нажатию на кнопку сохранить  
 *  @property {function} saveDicChanges:функция Которая сохраняет изминения внесенные в таблицу и отправляет их в базу
 *  @property {function} updateSelectedCell:функция Которая вызывает модальное окно выбора
 *  @property {function} editCoord:функция Которая вызывается для редактирования полей карты  
 *  @property {function} search:функция Которая выполняет поиск по данным  в таблицах 

**/
var app = angular.module("Dictionary", ["ngGrid", "services", 'ngCookies', "modalWindows", "mapModule","LogusConfig",]);
app.controller("DictionaryCtrl", ["$scope", "$http", "requests", "modals", "modalType", "preloader","$stateParams", "map","LogusConfig",'$cookieStore',  function ($scope, $http, requests, modals, modalType, preloader, $stateParams, map,LogusConfig,$cookieStore, ) {
    var sc = $scope;
    sc.Dictionaries = [];
    sc.selectedDictionary = null;
    sc.DictInfo = null;
    sc.DictColumns = [];
    sc.DictData = [];
    sc.selectedRow = [];
	sc.formData1 = [];
	sc.infoData5 = [];
	sc.title = LogusConfig.serverUrl;  
	var fieldDate;
    var count = 1;
    var filteredData = [];
    var rowsForDel = [];
    var rowsForUpdate = [];
    var rowsForCreate = [];
    var sortField = null;
	var ok = false;
	var index = 0;
    var metka=0;
    var headerDisTemplate1 = "<div   ng-class=\"'colt' + col.index\" class=\"ngHeaderText\">{{col.displayName}}</div>"
    var headerDisTemplate2 = '<div class="ngHeaderSortColumn {{col.headerClass}}" ng-style="{\'cursor\': col.cursor}" ng-class="{ \'ngSorted\': !noSortVisible }">' +
        '<div ng-click="sorting(); col.sort()"  ng-class="\'colt\' + col.index" class="ngHeaderText" >{{col.displayName}}</div>' +
        '<div class="ngSortButtonDown" ng-show="col.showSortButtonDown()">' +
        '</div><div class="ngSortButtonUp" ng-show="col.showSortButtonUp()">' ;
    var headerDisTemplate3 = "<div   ng-class=\"'colt' + col.index\" class=\"ngHeaderText\"></div>";
     
    console.log($cookieStore.get('currentUser').currentRole);
	var curId = $cookieStore.get('currentUser').currentRole;
    requests.serverCall.get($http, "DictionaryCtrl/GetDictionaries", function (data, status, headers, config) {
        console.info(data);
        sc.Dictionaries = data;
		// проверка условия, что пользователь перешел из приходных накладных 
		if ($cookieStore.get('from_incoming') == 1)
		{
			ok = true;
			sc.selectedDictionary = 30;
		}
		else if ($cookieStore.get('maps2') == 1){
			sc.selectedDictionary = 8;
	}
		else 
			sc.selectedDictionary = 1;
		$cookieStore.put('from_incoming', null);
		$cookieStore.put('maps2', null);
}
, function() {

    });
 $cookieStore.put('dateDailyCalendar', {date: null});
	 $cookieStore.put('dateEndDailyCalendar', {date: null});
    sc.selectDictionary = function () {
			$("#create").addClass("disabledbutton");
		    $("#save").addClass("disabledbutton");
        sc.filterText = "";
        if (rowsForUpdate.length > 0 || rowsForDel.length> 0 || rowsForCreate.length > 0) {
            saveDicChanges(loadNewDic);
        } else {
            loadNewDic();
        }
    };

        var fullData; var fullData1;
        var loadNewDic = function() {
        if (sc.selectedDictionary) {
            sc.DictData = [];
			HiddenButton();
            rowsForUpdate = [];
            requests.serverCall.post($http, 'DictionaryCtrl/GetDictionaryById',sc.selectedDictionary, function (data, status, headers, config) {
                console.log(data);	
			  sortField = data.ColumnDefs[1].field;
                sc.DictInfo = data;

                //updateGridOptions(data.ColumnDefs);
                /*data.ColumnDefs.push({
                    field: "Delete", displayName: " ", width: "5%",
                    cellTemplate: 'templates/delete_row_show.html', cellClass: 'cellToolTip'
                });*/
				
				
                sc.DictColumns = data.ColumnDefs;
                sc.DictColumns[1].headerCellTemplate = headerDisTemplate2;
				   for (var i = 2, li = sc.DictColumns.length; i < li; i++) {
                    sc.DictColumns[i].headerCellTemplate = headerDisTemplate1;
                if (i === sc.DictColumns.length - 1) { sc.DictColumns[i].headerCellTemplate = headerDisTemplate3;}
                }
				
				
				console.log(data);
				
                if (sc.DictInfo.RequestDetail) {
                    requests.serverCall.get($http, sc.DictInfo.RequestDetail, function (data, status, headers, config) {
                        sc.infoData = data;
                    }, function() {});
                }
                requests.serverCall.get($http, sc.DictInfo.Request, function (data, status, headers, config) {
                    console.info(data);
                    sc.DictData = fullData = fullData1 = data; //вывод данных без заголовков
					sc.DictData.Type = fieldDate;
                    var newOpt = {
                        enableColumnResize: false,
                        enableRowSelection: true,
                        showFooter: false,
                        rowHeight: 49,
                        headerRowHeight: 49,
                        useExternalSorting: true,
                        selectedItems: sc.selectedRow,
                        columnDefs: 'DictColumns',
                        data: 'DictData'
                    };
                    for (var p in newOpt) {
                     sc.gridOptions[p] = newOpt[p];
                    };
						$("#create").removeClass("disabledbutton");
					    $("#save").removeClass("disabledbutton");
                }, function() {

                });

            }, function() {

            });
        }
    };

    sc.delRow = function(row) {
        modals.showWindow(modalType.WIN_DIALOG, {
            title: "Удаление", message: "Вы уверены, что хотите удалить запись из справочника? (изменения вступят в силу после нажатия кнопки Сохранить)", callback: function () {
				
				if (sc.selectedDictionary==30 && sc.DictData[sc.DictData.length-1]==row)   {$( "#create" ).removeClass("disabledbutton");}
                sc.DictData.splice(sc.DictData.indexOf(row), 1);
                rowsForCreate.splice(rowsForCreate.indexOf(row), 1);
				//rowsForDel
                rowsForDel.push({
                    command: 'del',
                    Document: row
                });
            }
        });
    };

	//выгрузка в excel
	 sc.getDictionaryExcel = function() {
      var popupWin = window.open(LogusConfig.serverUrl + 'CostCtrl/GetDictionaryExcel?idDictionary=' + sc.selectedDictionary, '_blank');
	  																							
	 }
	
    sc.update = function(row) {
        console.log('update');
		
		// справочник  группы техники
		if (sc.selectedDictionary==25) {
		//	console.log(row.entity.uniq_code.length);
		if (row.entity.uniq_code!=null && row.entity.uniq_code.length<4) {row.entity.uniq_code=row.entity.uniq_code.toUpperCase();   console.log(row.entity.uniq_code);}
		}
		//виды запчастей
		if (sc.selectedDictionary==32) {
		//	console.log(row.entity.uniq_code.length);
		if (row.entity.uniq_code!=null && row.entity.uniq_code.length<4) {row.entity.uniq_code=row.entity.uniq_code.toUpperCase();   console.log(row.entity.uniq_code);}
		}
        row.selected = true;
        var ind = sc.DictData.indexOf(row.entity);
        if (row.entity[sc.DictInfo.index_column] !== null && row.entity[sc.DictInfo.index_column] !== undefined) {
            addRowForUpdate(row.entity, ind);
        }
    };

    var addRowForUpdate = function(row, ind) {
        for (var i = 0, li = rowsForUpdate.length; i < li; i++) {
            if (sc.DictData[rowsForUpdate[i]][sc.DictInfo.index_column] === row[sc.DictInfo.index_column]) {
                rowsForUpdate.splice(i, 1);
            }
        }
        rowsForUpdate.push(ind);
        console.info(rowsForUpdate);
    };

    sc.gridOptions = {
        enableColumnResize: false,
        enableRowSelection: false,
        showFooter: false,
        rowHeight: 49,
        headerRowHeight: 49,
        useExternalSorting: true,
        selectedItems: sc.selectedRow,
        columnDefs: 'DictColumns',
        data: 'DictData',
    };

    sc.create = function() {
		//запчасти
		var mel=0;
		if (sc.selectedDictionary==30)
		{
			console.log(rowsForCreate.length);
			//if (!ok) 
			$( "#create" ).addClass("disabledbutton");
		if (rowsForCreate.length>0) {mel=1;
		} 
			
		}
		
		//console.log(sc.selectedDictionary);
        var obj = {};
        for (var i= 0, li=sc.DictColumns.length; i<li; i++) {
            obj[sc.DictColumns[i].field] = null;
        }
	
        sc.DictData.push(obj);
        rowsForCreate.push(sc.DictData.length - 1);
          console.log(rowsForCreate);		
        setTimeout(function() {
            var grid = sc.gridOptions.ngGrid;
            grid.$viewport.scrollTop(sc.DictData.length * grid.config.rowHeight + 100);
        }, 100);
    };

  sc.cancel = function() {
		sc.formData1.typeId = "0";
	    $("#create").addClass("disabledbutton");
		$("#save").addClass("disabledbutton");
        loadNewDic();
		if (ok) 
		{
			$cookieStore.put('from_incoming', 2) // callback to incoming
			window.history.back();
		}
    };

    sc.submitForm = function() {
		  sc.formData1.typeId = "0";
		  $("#create").addClass("disabledbutton");
		    $("#save").addClass("disabledbutton");
        saveDicChanges(function() {
            //$location.path('/main');
        });
		console.log(metka);
		if (ok && metka==0) 
		{
			$cookieStore.put('from_incoming', 2) // callback to incoming
			window.history.back();
		}
    };
  
   var unique = function(arr) {
  var obj = {};
  for (var i = 0; i < arr.length; i++) {
    var str = arr[i];
    obj[str] = true; 
  }

  return Object.keys(obj); 
}
     //проверка обьекта
 var checkArr = function(arr) {
	if (arr != null) {
		return arr.Id;
	} else {
		return null;
	}
 }


    var saveDicChanges = function(cb) {
		   for (var i = 0, li = rowsForUpdate.length; i<li; i++) {
            rowsForDel.push({
                command: 'update',
                Document: sc.DictData[rowsForUpdate[i]]
            });
			if (sc.DictData.indexOf(rowsForDel[rowsForDel.length-1].Document) > index) { 
			index = sc.DictData.indexOf(rowsForDel[rowsForDel.length-1].Document);
		 	console.log(index);
			}
        }
		
        for (var i = 0, li = rowsForCreate.length; i<li; i++) {
            rowsForDel.push({
                command: 'insert',
                Document: sc.DictData[rowsForCreate[i]]
            })
			index = sc.DictData.length;
        }

		console.log(rowsForDel);
	///*
	 metka=0;
	if (rowsForDel!=undefined && rowsForDel!=null) {
		console.log(rowsForDel);
		for (var i=0; i<rowsForDel.length;i++)
		{
			//удалить пустые строки
			var counter = 0;	var counter2 = 0;
			for (var key in rowsForDel[i].Document) {
				  counter++;
			}
			
			for (var key in rowsForDel[i].Document) {
				console.log(rowsForDel[i].Document[key]);
				//console.log($.isEmptyObject({name:null}));
             if (rowsForDel[i].Document[key]=="" || rowsForDel[i].Document[key]==null) {
				  counter2++;
			 }
			}
			if (counter==counter2) {rowsForDel.splice(i,1); if (i!=0) {i--; } else {break;} ;}
			//
				//	console.log(typeof(rowsForDel[i].Document.type_fuel_id)=='object');
		/*
			if ((rowsForDel[i].command=="insert" && rowsForDel[i].Document!=undefined && (rowsForDel[i].Document.name=="" || rowsForDel[i].Document.name==null)) ||
		    	(rowsForDel[i].command=="update" && rowsForDel[i].Document!=undefined && (rowsForDel[i].Document.name=="" || rowsForDel[i].Document.name==null))) {
				metka=1; rowsForDel = [];
				 modals.showWindow(modalType.WIN_MESSAGE, {
                              title: "Ошибка",
                              message: "Не заполнено поле Название",
                              callback: function () {
             
                              }
							  
                          });
			break;	
			}
			*/
			//проверка типа работ 
			if (rowsForDel[i].Document!=undefined && rowsForDel[i].Document.auxiliary_rate!=undefined && rowsForDel[i].Document.auxiliary_rate!=null  ) {
			if ((rowsForDel[i].command=="insert" && rowsForDel[i].Document!=undefined && (rowsForDel[i].Document.auxiliary_rate.Id==1 || rowsForDel[i].Document.auxiliary_rate.Id==2) && (rowsForDel[i].Document.type_fuel_id =="" || rowsForDel[i].Document.type_fuel_id==null)) ||
	(rowsForDel[i].command=="update" && rowsForDel[i].Document!=undefined && (rowsForDel[i].Document.auxiliary_rate.Id==1 || rowsForDel[i].Document.auxiliary_rate.Id==2) && (rowsForDel[i].Document.type_fuel_id =="" || rowsForDel[i].Document.type_fuel_id ==null))) {
		metka=1; rowsForDel = [];
				 modals.showWindow(modalType.WIN_MESSAGE, {
                              title: "Ошибка",
                              message: "Не заполнено поле Этапы выращивания",
                              callback: function () {
             
                              }
							  
                          });	
						  	break;	
	}
	
			if ((typeof(rowsForDel[i].Document.type_fuel_id)=='object' && rowsForDel[i].command=="insert" && rowsForDel[i].Document!=undefined && (rowsForDel[i].Document.auxiliary_rate.Id==1 || rowsForDel[i].Document.auxiliary_rate.Id==2) && rowsForDel[i].Document.type_fuel_id.Id == null) ||
	(typeof(rowsForDel[i].Document.type_fuel_id)=='object' && rowsForDel[i].command=="update" && rowsForDel[i].Document!=undefined && (rowsForDel[i].Document.auxiliary_rate.Id==1 || rowsForDel[i].Document.auxiliary_rate.Id==2) && rowsForDel[i].Document.type_fuel_id.Id == null)) {
		metka=1; rowsForDel = [];
				 modals.showWindow(modalType.WIN_MESSAGE, {
                              title: "Ошибка",
                              message: "Не заполнено поле Этапы выращивания",
                              callback: function () {
             
                              }
							  
                          });	
						  	break;	
	}
			}
			
			//справочник группы техники
		if (sc.selectedDictionary==25) {
			
			if (rowsForDel[i].Document.uniq_code==null || rowsForDel[i].Document.uniq_code.length!=3) {
				metka=1; rowsForDel = [];
				 modals.showWindow(modalType.WIN_MESSAGE, {
                              title: "Ошибка",
                              message: "Неверный формат поля Код",
                              callback: function () {
             
                              }
							  
                          });	
						  	break;
							
			}
		}
		if (sc.selectedDictionary==32) {
			
			if (rowsForDel[i].Document.uniq_code==null || rowsForDel[i].Document.uniq_code.length!=3) {
				metka=1; rowsForDel = [];
				 modals.showWindow(modalType.WIN_MESSAGE, {
                              title: "Ошибка",
                              message: "Неверный формат поля Код",
                              callback: function () {
             
                              }
							  
                          });	
						  	break;
							
			}
		}
		if (sc.selectedDictionary==30) {
			if ((rowsForDel[i].Document.tptrak_tptrak_id == null || rowsForDel[i].Document.tptrak_tptrak_id.Id == null) && rowsForDel[i].command!='del') {
				metka = 1; rowsForDel = [];
				 modals.showWindow(modalType.WIN_MESSAGE, {
                              title: "Ошибка",
                              message: "Заполните поле Вид запчастей",
                              callback: function () {
                              }
							  });	
				break;				
			}
		}
		}
			// запчасти
			var nm = 0;
				if (sc.DictData.length!=0 && ('vendor_code' in sc.DictData[0]) && sc.DictData[0].vendor_code!=undefined && sc.DictData[0].vendor_code!=null ) {
				for (var j=0; j<sc.DictData.length;j++)
		{
			if (nm == 1) {break;}
			for (var l=0; l<sc.DictData.length;l++)
		{
			if (j!=l  && sc.DictData[j].vendor_code==sc.DictData[l].vendor_code && sc.DictData[j].name==sc.DictData[l].name && 
			sc.DictData[j].tptrak_tptrak_id.Id == sc.DictData[l].tptrak_tptrak_id.Id && checkArr(sc.DictData[l].mod_mod_id)==checkArr(sc.DictData[j].mod_mod_id) &&
			sc.DictData[j].manufacturer == sc.DictData[l].manufacturer) {
				console.log('уже существует1');
				console.log(sc.DictData[j]);
				console.log(sc.DictData[l]);
			metka = 1;	rowsForDel = [];
			
				 modals.showWindow(modalType.WIN_MESSAGE, {
                              title: "Ошибка",
                              message: "Запись уже существует",
                              callback: function () {
             
                              }  
                          });
						  nm = 1;
						  break;
						  }	
			//пустой артикул, запчасть
			
             	if (sc.DictData[l].name=="" || sc.DictData[l].name==null)	
				{
				metka=1; rowsForDel = [];
			    modals.showWindow(modalType.WIN_MESSAGE, {
                              title: "Ошибка",
                              message: "Не введено название запчасти",
                              callback: function () {
             
                              }  
                          });
						  nm = 1;
						  break;	
				}
						  
		}
		}	
		console.log(sc.DictData);
		
		
		//проверка артикла
				for (var j=0; j<sc.DictData.length;j++)
		{
			if (nm==1) {break;}
					for (var l=0; l<sc.DictData.length;l++)
		{

			if (j!=l  && sc.DictData[j].vendor_code==sc.DictData[l].vendor_code && sc.DictData[j].name==sc.DictData[l].name && 
			sc.DictData[j].tptrak_tptrak_id.Id == sc.DictData[l].tptrak_tptrak_id.Id && checkArr(sc.DictData[j].mod_mod_id) == checkArr(sc.DictData[l].mod_mod_id) &&
			sc.DictData[j].manufacturer == sc.DictData[l].manufacturer) {
			metka=1;	rowsForDel = [];
			console.log('2');
				 modals.showWindow(modalType.WIN_MESSAGE, {
                              title: "Ошибка",
                              message: "Запись уже существует",
                              callback: function () {
             
                              }
							  
                          });
						  nm = 1;
						  break;
						  }		  
		}
			//совпадает все, кроме 	 техники или(и) модуля 
			for (var k = 0; k < sc.DictData.length;k++) {
				if (rowsForDel.length > 0)  
			{ 
				if (sc.DictData[k].tptrak_tptrak_id != null  && rowsForDel[0].command=="insert" &&
				sc.DictData[k].name==rowsForDel[0].Document.name && sc.DictData[k].vendor_code==rowsForDel[0].Document.vendor_code &&
				sc.DictData[k].tptrak_tptrak_id.Id==rowsForDel[0].Document.tptrak_tptrak_id.Id  && checkArr(sc.DictData[k].mod_mod_id) != null &&
				checkArr(sc.DictData[k].mod_mod_id) != checkArr(rowsForDel[0].Document.mod_mod_id)   && sc.DictData[k].manufacturer==rowsForDel[0].Document.manufacturer) {
				metka=1;
				$("#mes").text('Запись уже существует c другим модулем. Сохранить с новым кодом?');
				$("#myModal").modal("show");
                break;				
				}
				
			}
		}
		}
				}	
		}
		//*/
		if (metka==0) {
        requests.serverCall.post($http, sc.DictInfo.RequestUpdate, rowsForDel, function (data, status, headers, config) {
            if (cb) cb();
            rowsForDel = [];
            rowsForUpdate = [];
            rowsForCreate = [];
          //  $("#create").removeClass("disabledbutton");				
			loadNewDic();	
			console.log(index);
			  setTimeout(function() {
            var grid = sc.gridOptions.ngGrid;
            grid.$viewport.scrollTop(index * grid.config.rowHeight);
        }, 200);
        }, function() {
			 rowsForDel = [];
			console.log('ошибка');
			   $("#create").removeClass("disabledbutton");
			     $("#save").removeClass("disabledbutton");
        });
		}
		

    };
	//модальное окно
 sc.cancelButton = function () {
	 rowsForDel = [];
		$("#myModal").modal("hide");		 
    };
	     sc.yes = function () {
		 $("#myModal").modal("hide");
		 $("div.modal-backdrop").hide();
		   requests.serverCall.post($http, sc.DictInfo.RequestUpdate, rowsForDel, function (data, status, headers, config) {
          //  if (cb) cb();
            rowsForDel = [];
            rowsForUpdate = [];
            rowsForCreate = [];
			loadNewDic();
     setTimeout(function() {
            var grid = sc.gridOptions.ngGrid;
            grid.$viewport.scrollTop(index * grid.config.rowHeight);
        }, 200);			
        }, function() {
			
        });

	}
   sc.no = function () {
	   rowsForDel = [];
	$("#myModal").modal("hide");
	$("div.modal-backdrop").hide();	 
	}
	//
	
	
    sc.updateSelectedCell = function (colField, selectedVal, displayName, row) {
		if (colField == 'emp_emp_id') {
			var info1=[];
			if (row.entity.id_type_way_list!=null && row.entity.id_type_way_list.Id==1) {
				for (var i=0;i<sc.infoData[colField].length;i++)
				{
					if (sc.infoData[colField][i].index==1) {info1.push({Id:sc.infoData[colField][i].Id, Name:sc.infoData[colField][i].Name, index:sc.infoData[colField][i].index}) }
						
					}
					
					 var modalValues=info1;	
				}
				else if (row.entity.id_type_way_list != null && row.entity.id_type_way_list.Id == 2) {
				for (var i=0;i<sc.infoData[colField].length;i++)
				{
					if (sc.infoData[colField][i].index==5) {info1.push({Id:sc.infoData[colField][i].Id, Name:sc.infoData[colField][i].Name, index:sc.infoData[colField][i].index}) }
						
					}
					
				 var modalValues=info1;		
				}
					else if (row.entity.id_type_way_list.Id==null) {
							var modalValues = sc.infoData[colField];	
					}
				
		}
				if (colField != 'emp_emp_id') {
				var modalValues = sc.infoData[colField];	
				}	
		
		//	console.log(sc.infoData[colField]);
        selectedVal = row.entity[colField];
        var typeModal = modalType.WIN_SELECT;
		
        modals.showWindow(typeModal, {
            nameField: displayName,
            values: modalValues,
            selectedValue: selectedVal
        }).result.then(function (result) {
            row.entity[colField] = result;
            sc.update(row);
			//путевой лист
			if (colField == 'id_type_way_list') {
		if (row.entity.id_type_way_list.Id==1 && row.entity.emp_emp_id.index!=1) {
				
				row.entity.emp_emp_id={Id:null, Name:null, index: null};
			}
			if (row.entity.id_type_way_list.Id==2 && row.entity.emp_emp_id.index!=5) {
				
				row.entity.emp_emp_id = {Id:null, Name:null, index: null};
			}
		}
			//сотрудник
			if (colField =='emp_emp_id') {
			if (row.entity.id_type_way_list.Id==null) {	
		if (result.index==5) {row.entity.id_type_way_list = {Id:2, Name:'Водитель'}}	
	    if (result.index==1) {row.entity.id_type_way_list = {Id:1, Name:'Механизатор'}}	
			}
		
			}
			
			//запчасти - гр. техники
			if (sc.selectedDictionary==30 && colField=='tptrak_tptrak_id') {
				//result.Id
				  requests.serverCall.post($http, "SparePartsCtrl/GetUniqCodeForTech",{
                        TraktorId:  result.Id,
						ModId: row.entity.spare_id,
                 }, function (data, status, headers, config) {
                       console.info(data);
					   row.entity.code = data;
                //    sc.infoData.SparePartsList = data;
				//	sc.infoData.VendorCodeList=[];

                    }, function () {
                      })
			}
			
		    console.log(row.entity);
			console.log(colField);
			console.log(result);
        }, function () {
            console.info('Modal dismissed at: ' + new Date());
        });
    };

    sc.editCoord = function(row) {
        modals.showWindow(modalType.WIN_MAP, {
            map: map,
            fieldid: row.fiel_id,
            coord: row.coord
        }).result.then(function (result) {
            console.log(result);
            row.coord = result;
            if (row[sc.DictInfo.index_column]) {
                var ind = sc.DictData.indexOf(row);
                addRowForUpdate(row,ind);
				
            }
			
        }, function() {
		

        });
    };
        /// поиск
    sc.search = function () {
        filteredData = [];
        if (!sc.filterText) {
            sc.DictData = fullData;    
        } else {
           
            for (var i = 0, li = fullData.length; i < li; i++) {
                var d = fullData[i];
                var text = '';
                for (var p in d) {
                    text += d[p];
                }
                if (text.toLowerCase().indexOf(sc.filterText.toLowerCase()) != -1) {
                    filteredData.push(fullData[i]);
                }
            }
            sc.DictData = filteredData;
        }
    }
    //сортировка
    sc.sorting = function () {
        var names = [];
        var filteredData1 = [];
        if (!sc.filterText) {
            fullData1 = [];
            for (var i = 0, li = fullData.length; i < li; i++) {
                fullData1.push(fullData[i]);
            };
        }
        if (sc.filterText) {
          fullData1 = [];
            for (var i = 0, li = filteredData.length; i < li; i++) {
                fullData1.push(filteredData[i]);
            };
        } 
            for (var i = 0, li = fullData1.length; i < li; i++) {
				 var d1 = fullData1[i];
				for(var p in d1){
           if (sortField==p) {  names.push(d1[p]);  }
                        }  
            }
			console.log(names);
            names.sort();
            if (count % 2 == 0) { names.reverse(); }
			names = unique(names);
            for (var i = 0, li = fullData1.length; i < li; i++) {
                for (var j = 0, l1 = fullData1.length; j < l1; j++) {
					
					 var d1 = fullData1[j];
				for(var p in d1){
			 if (sortField==p && names[i] == d1[p]) {console.log(d1[p]);}		
           if (sortField==p && names[i] == d1[p]) { filteredData1.push(fullData1[j]); break; }
                        }  	
                }
            }
			
            sc.DictData = filteredData1;
            count++;
    }
	
	 //выгрузка в txt
	 sc.getFieldCoordTxt = function() {
      var popupWin = window.open(LogusConfig.serverUrl + 'DictionaryCtrl/GetFieldCoordTxt');
	  																							
	 }
	// KML
	  sc.getFieldCoordKml = function() {	  
    var popupWin = window.open(LogusConfig.serverUrl + 'DictionaryCtrl/GetFieldCoordKml', '_blank');																							
	 }
	 
	 HiddenButton = function() {    
    if (sc.selectedDictionary==8) 
	{sc.txt = true;
	 document.getElementById('shres').innerHTML = '';

	} else { 
	sc.txt = false; 
	document.getElementById('shres').innerHTML = '';
	
	} }

      var a1 = document.getElementById("type1");
       a1.onclick = function() {angular.element(fieldInput).trigger('click');}
	   var a2 = document.getElementById("type2");
       a2.onclick = function() {angular.element(ObjectInput).trigger('click');}
	   var a3 = document.getElementById("type3");
       a3.onclick = function() {angular.element(LayerInput).trigger('click');}
	   
	   sc.infoData5.typelist = [{Id: 0 , Name:"Все"},{Id: 1, Name:"Поля"},{Id: 2, Name:"Объекты"},{Id: 3, Name:"Контуры"}];
	   sc.formData1.typeId = 0;
	   sc.filterFields = function() {
        if (sc.selectedDictionary == 8) {
			HiddenButton();
            sc.DictData = [];
            rowsForUpdate = [];
            requests.serverCall.post($http, 'DictionaryCtrl/GetDictionaryField',sc.selectedDictionary, function (data, status, headers, config) {
                console.log(data);	
			    sortField = data.ColumnDefs[1].field;
                sc.DictInfo = data;

                //updateGridOptions(data.ColumnDefs);
                /*data.ColumnDefs.push({
                    field: "Delete", displayName: " ", width: "5%",
                    cellTemplate: 'templates/delete_row_show.html', cellClass: 'cellToolTip'
                });*/
				
				
                sc.DictColumns = data.ColumnDefs;
                sc.DictColumns[1].headerCellTemplate = headerDisTemplate2;
				   for (var i = 2, li = sc.DictColumns.length; i < li; i++) {
                    sc.DictColumns[i].headerCellTemplate = headerDisTemplate1;
                if (i === sc.DictColumns.length - 1) { sc.DictColumns[i].headerCellTemplate = headerDisTemplate3;}
                }
				
				
				console.log(data);
				
                if (sc.DictInfo.RequestDetail) {
                    requests.serverCall.get($http, 'DictionaryCtrl/DetailFieldsByType?type =' + sc.formData1.typeId, function (data, status, headers, config) {
                        sc.infoData = data;
                    }, function() {});
                }
                requests.serverCall.get($http, 'DictionaryCtrl/GetFieldsByType?type='+ sc.formData1.typeId , function (data, status, headers, config) {
                    console.info(data);
                    sc.DictData = fullData = fullData1 = data; //вывод данных без заголовков
					sc.DictData.Type = fieldDate;
                    var newOpt = {
                        enableColumnResize: false,
                        enableRowSelection: true,
                        showFooter: false,
                        rowHeight: 49,
                        headerRowHeight: 49,
                        useExternalSorting: true,
                        selectedItems: sc.selectedRow,
                        columnDefs: 'DictColumns',
                        data: 'DictData'
                    };
                    for (var p in newOpt) {
                     sc.gridOptions[p] = newOpt[p];
                    };
						$("#create").removeClass("disabledbutton");
					    $("#save").removeClass("disabledbutton");
                }, function() {

                });

            }, function() {

            });
        }
    };
  
	sc.ok = function () {
	 $("#myModal1").modal("hide");	
	 $('#shres').text("Ожидание окончания загрузки");
	 sc.filterFields();
    };
	  
	
}]);

   var grabFileContentObject = function() {
		
        var files = document.getElementById("ObjectInput").files;
        if (files && files.length > 0) {
			$("#myModal1").modal("show");
			$("div.modal-backdrop").show();
			$('#shres').text("Ожидание окончания загрузки");
			var formData = new FormData();  
            formData.append("ObjectInput", $('#ObjectInput')[0].files[0]);

            var t = $('#yura').text();
 
            $.ajax({
		    type: "POST",
		    url: t+'DictionaryCtrl/GetCoordFromTXT?type='+2,
		    processData: false,
                    contentType: false,
		    data: formData,
		    success: function (result) {$('#shres').html(result);
			},
		    error: function (xhr, status, p3) {
		        alert(status);
		    }
		});
		$("#ObjectInput").val("");
		}
		
    };
	

	    var grabFileContentField = function()    {
		
      	var files = document.getElementById("fieldInput").files;
        if (files && files.length > 0) {
			$("#myModal1").modal("show");
			$("div.modal-backdrop").show();
			$('#shres').text("Ожидание окончания загрузки");
			var formData = new FormData();  
            formData.append("fieldInput", $('#fieldInput')[0].files[0]);

            var t = $('#yura').text();
 
            $.ajax({
		    type: "POST",
		    url: t+'DictionaryCtrl/GetCoordFromTXT?type='+1,
		    processData: false,
                    contentType: false,
		    data: formData,
		    success: function (result) {$('#shres').html(result);
			},
		    error: function (xhr, status, p3) {
		        alert(status);
		    }
		});
		$("#fieldInput").val("");
		}
    };
	var grabFileContentLayer = function() {
		
     	var files = document.getElementById("LayerInput").files;
        if (files && files.length > 0) {
			$("#myModal1").modal("show");
			$("div.modal-backdrop").show();
			$('#shres').text("Ожидание окончания загрузки");
			var formData = new FormData();  
            formData.append("LayerInput", $('#LayerInput')[0].files[0]);

            var t = $('#yura').text();
 
            $.ajax({
		    type: "POST",
		    url: t+'DictionaryCtrl/GetCoordFromTXT?type='+3,
		    processData: false,
                    contentType: false,
		    data: formData,
		    success: function (result) {$('#shres').html(result);
			},
		    error: function (xhr, status, p3) {
		        alert(status);
		    }
		});
		$("#LayerInput").val("");
		}
    };
