var app = angular.module("Crater", ["ngGrid", "services", 'ngCookies', "modalWindows"]);
app.controller("CraterCtrl", ["$scope", "$http", "requests", "$location",'$stateParams',"modalType","modals", function ($scope, $http, requests, $location, $stateParams, modalType, modals) {
    var sc = $scope;
 sc.YearId = new Date().getFullYear();
	var arr=[]; 
    var _lastYear;
	   sc.formData = {};
	sc.YearId =2016;
	var totalRate=0;
   sc.formData.GrossHarvest = 0;
   sc.formData.GrossHarvest1 = 0;
	console.log($stateParams.id);
	//меняем год
    sc.refreshYear = function() {
	//	console.log(sc.YearId);
        if (_lastYear != sc.YearId) {
            requests.serverCall.get($http, "KPICtrl/KPIInfo?year=" + sc.YearId, function (data, status, headers, config) {
                _lastYear = sc.YearId;
              //  console.info(data);
                sc.infoData = data;
                sc.IdTc = sc.infoData.TCList[0].Id;
				console.log(sc.IdTc);
            }, function () {
            });
        }
    };
    sc.refreshYear();
 
    sc.refreshPlant = function() {
        if (sc.YearId && sc.IdTc) {
            requests.serverCall.post($http, "KPICtrl/KPIWorks", {
                year: sc.YearId,
                id_tc: sc.IdTc
            }, function (data, status, headers, config) {
              //  console.info(data);
                sc.Works = data.Works;
			   sc.Fields = data.Fields;
			 //  console.log(sc.Works);
			   arr=[];     var kol=0; var rate=0;  totalRate=0;
			
     for (var i=0;i<sc.Works.length;i++)
	    {
	if (sc.Works[i].tasks==null) {kol=0;} else {kol=sc.Works[i].tasks.length;} 
	if (sc.Works[i].TotalKPI==null && kol==0) {rate=100;}
	if (sc.Works[i].TotalKPI==null && kol!=0) {rate=0;}
	if (sc.Works[i].TotalKPI!=null) {rate=sc.Works[i].TotalKPI.rate;}
		arr.push( {"title": sc.Works[i].Name+" ("+kol+") Оценка: "+rate+"%",
		"value": rate});	
	totalRate=totalRate+rate;
	    }
    totalRate=totalRate/sc.Works.length;
	var totalPersent=totalRate+"%";
	//////Воронка2 
var chart = AmCharts.makeChart( "chartdiv", {
  "type": "funnel",
  "theme": "light",
  "dataProvider": arr,
  "balloon": {
    "fixedPosition": true
  },
  "valueField": "value",
  "titleField": "title",
  "marginRight": 240,
  "neckWidth": totalPersent,
  "autoResize": false,
  "autoTransform" : false,
  "groupedPulled" : false,
  "pullDistance": 0,
 "groupedPulled" : true,
  "marginLeft": 50,
  "startX": -500,
  "depth3D": 100,
  "angle": 40,
  "outlineAlpha": 1,
  "outlineColor": "#FFFFFF",
  "outlineThickness": 2,
  "showZeroSlices" : true,
  "labelPosition": "right",
  "balloonText": "[[title]]",
  "export": {
    "enabled": true
  },  
} );	
////////////////////////
function func() {
	//ссылки
//$("a").remove();
//цвет уровней
$("g").children("path").attr('fill', '#12e539');
//console.log($("path").attr('fill', '#12e539'));
$("ellipse").attr('fill', '#12e539');
$("text.amcharts-funnel-label").each(function(){
   var text = $(this).html();
text = (text.split(":")[0]);
 $(this).html(text);
});
$("g").each(function(){
if ($(this).attr("aria-label")!=undefined) { 
var r = $(this).attr("aria-label").split('%');
} else {var r = 0;}

if ($(this).attr("aria-label")==undefined) {$(this).children("g").children("path").attr('fill', '#daef1c');}
if ($(this).attr("aria-label")!=undefined && r[2]>50 && r[2]<70) { $(this).children("g").children("path").attr('fill', '#daef1c');} //желтый
if ($(this).attr("aria-label")!=undefined && r[2]>=70 && r[2]<=100) { $(this).children("g").children("path").attr('fill', '#12e539');} //зеленый
if ($(this).attr("aria-label")!=undefined && r[2]<=50) { $(this).children("g").children("path").attr('fill', '#ff0724');} //красный
//console.log(r); 
});


}
setTimeout(func, 100);	

//
// площадь
    sc.Area = 0;
    for (var i=0, li = sc.Fields.length; i<li; i++) {
    sc.Area += sc.Fields[i].Area;
		//console.log(sc.Area);
             }         	
	loadTcById();
            }, function () {
            });
        } else {
            sc.panelInfo = [];
        }
    };
	//end refreshPlant
	//валовый сбор
    var loadTcById = function() {
		console.log(sc.IdTc);
        if (sc.IdTc) {
            requests.serverCall.post($http, 'TechCard/GetTCById', sc.IdTc, function (data, status, headers, config) {
                console.info(data);
                sc.formData = data.TC;
	 sc.formData.GrossHarvest1 = (sc.formData.GrossHarvest*(totalRate/100)).toFixed(2);
	 console.log(sc.formData.GrossHarvest1);
	 	 console.log(sc.formData.GrossHarvest);
		 	 console.log(totalRate);
            }, function () {
            });
        }
    };
	//

	




///
}]);