
var app = angular.module("Service", ["ngGrid", "services", 'ngCookies', "modalWindows"]);
app.controller("ServiceCtrl", ["$scope", "$http", "requests", "$location",'$stateParams',"modalType","modals", function ($scope, $http, requests, $location, $stateParams, modalType, modals) {
    var sc = $scope;
    sc.selectedRow = [];
	 var rowsForDel = [];
	 var rowsForUpd = [];
	 sc.multi = false;
	 var nextNumber;
	    // пейджинг
    sc.totalServerItems = 0;
    sc.pagingOptions = {
        pageSizes: [10, 15, 20, 50, 100],
        pageSize: 10,
        currentPage: 1,
        totalServerItems: sc.totalServerItems
    };
	   // смотрим за изменениям в пейджинге
    sc.$watch('pagingOptions', function (newVal, oldVal) {
        if (newVal.currentPage != null && newVal !== oldVal && !isNaN(newVal.currentPage) && !isNaN(oldVal.currentPage)) {
            if (newVal.pageSize !== oldVal.pageSize) {
                sc.pagingOptions.currentPage = 1;
            loadService();
            }
            if (newVal.currentPage !== oldVal.currentPage) 
			{ 
			loadService();
			
			;}
        }
    }, true);
	
	
    sc.gridOptions = {
        enableColumnResize: true,
        enableRowSelection: true,
        showFooter: true,
        rowHeight: 49,
        headerRowHeight: 49,
        useExternalSorting: false,
        data: 'TableData',
		enablePaging: true,
        pagingOptions: sc.pagingOptions,
        selectedItems: sc.selectedRow,
		i18n: 'ru',
		//
		//
		
        afterSelectionChange: function (row) {
       	for (var i=0; i<row.length;i++ ) 
   { 
     sc.selectedRow.push(row[i].entity); 
     console.log(sc.selectedRow); 
        }
 console.log(row);		
	  if (row.entity) {
		  console.log(row.selected);
                if (row.selected) {
                    if (sc.multi) sc.selectedRow.push(row.entity);
                    else {
                        sc.selectedRow = [];
                        sc.selectedRow.push(row.entity);
                    }
                } else {
                    sc.selectedRow.splice(sc.TableData.indexOf(row.entity), 1);
                }
            }
	   
        },
        beforeSelectionChange: function (row, event) {
        	   if (!sc.multi) {
                angular.forEach(sc.TableData, function (data, index) {
                    if (data.selected == true) sc.mySelections.splice(index, 1);
                    sc.gridOptions.selectRow(index, false);

                });
            }
		return true;
        }
    };

    sc.gridOptions.columnDefs = [
        {
            field: "Date",
            displayName: "Дата",
            width: "10%",
            cellTemplate:'<a tooltip="{{row.getProperty(col.field)}}" tooltip-popup-delay="1000" tooltip-class="my-custom-tooltip" tooltip-placement="top" tooltip-append-to-body="true">' +
            '<datepicktable class="logus-headers col-md-2 col-xs-4 col-sm-2 col-lg-2" style="padding-top: 6px;margin-left: -22px;font-size: 15px;" data="row.entity[col.field]" mask="\'99.99.9999\'" regex-name="\'date\'" date-change="dateChange(row.entity, curDate)" ng-class="{clicknone: row.entity[\'Id1\']}">' +
            '</datepicktable>' +
            '</a>'
         },

		{field: "Act_number", displayName: "№ акта", width: "10%",
            cellTemplate: '<a tooltip="{{row.entity[col.field][\'Name\']}}" tooltip-popup-delay="1000" tooltip-class="my-custom-tooltip" tooltip-placement="top" tooltip-append-to-body="true">' +
            '<div class="ngCellText" ng-class="col.colIndex()">' +
            '<input value="{{row.entity[col.field]}}" ng-model="row.entity[col.field]" placeholder="" ng-class="{bordernone: row.entity[\'Id\']}" ng-readonly="row.entity[\'Id1\']" ng-change="changeCountPrice(row.entity)">' +
            '</div>' + '</a>',  headerCellTemplate: "<div   ng-class=\"'colt' + col.index\" class=\"ngHeaderText\">{{col.displayName}}</div>"
            },
		
		{ field: "NamePlant", displayName: "Культура", width: "10%",
          cellTemplate: '<a tooltip="{{row.entity[col.field][\'Name\']}}" tooltip-popup-delay="1000" tooltip-class="my-custom-tooltip" tooltip-placement="top" tooltip-append-to-body="true">' +
          '<div class="ngCellText" ng-class="col.colIndex()">' +
          '<input value="{{row.entity[col.field][\'Name\']}}" placeholder="Культура" style="border: none;"  data-ng-click="updateSelectedCell(col.field, row.entity[col.field][\'Id1\'],\'Наименование услуг\',row.entity,$event)"  ng-readonly="row.entity[\'Id1\']" readonly>' +
          '</div>', headerCellTemplate: "<div   ng-class=\"'colt' + col.index\" class=\"ngHeaderText\">{{col.displayName}}</div>"
          },
		  { field: "NameService", displayName: "Тип работ", width: "10%",
          cellTemplate: '<a tooltip="{{row.entity[col.field][\'Name\']}}" tooltip-popup-delay="1000" tooltip-class="my-custom-tooltip" tooltip-placement="top" tooltip-append-to-body="true">' +
          '<div class="ngCellText" ng-class="col.colIndex()">' +
          '<input value="{{row.entity[col.field][\'Name\']}}" placeholder="Тип работ" style="border: none;"  data-ng-click="updateSelectedCell(col.field, row.entity[col.field][\'Id\'],\'Наименование услуг\',row.entity,$event)"  readonly>' +
          '</div>', headerCellTemplate: "<div   ng-class=\"'colt' + col.index\" class=\"ngHeaderText\">{{col.displayName}}</div>"
          },
	       {field: "NameWork", displayName: "Наименование работ, услуг", width: "20%",
			 cellTemplate: '<a tooltip="{{row.entity[col.field][\'Name\']}}" tooltip-popup-delay="1000" tooltip-class="my-custom-tooltip" tooltip-placement="top" tooltip-append-to-body="true">' +
             '<div class="ngCellText" ng-class="col.colIndex()">' +
             '<input value="{{row.entity[col.field]}}" ng-model="row.entity[col.field]" placeholder=" " ng-class="{bordernone: row.entity[\'Id\']}" ng-readonly="row.entity[\'Id1\']"  ng-change="changeCountPrice(row.entity)">' +
             '</div>' +'</a>',  headerCellTemplate: "<div   ng-class=\"'colt' + col.index\" class=\"ngHeaderText\">{{col.displayName}}</div>"},
		
           {field: "Count", displayName: "Количество", width: "10%",
		   
            cellTemplate: '<a tooltip="{{row.entity[col.field][\'Name\']}}" tooltip-popup-delay="1000" tooltip-class="my-custom-tooltip" tooltip-placement="top" tooltip-append-to-body="true">' +
            '<div class="ngCellText" ng-class="col.colIndex()">' +
            '<input value="{{row.entity[col.field]}}" ng-model="row.entity[col.field]" placeholder="Количество" ng-class="{bordernone: row.entity[\'Id\']}" ng-readonly="row.entity[\'Id1\']" ng-change="changeCountPrice(row.entity)">' +
            '</div>' + '</a>', headerCellTemplate: "<div   ng-class=\"'colt' + col.index\" class=\"ngHeaderText\">{{col.displayName}}</div>"
            },
 
		   {
            field: "TypeSalary", displayName: "Ед.", width: "10%",
            cellTemplate:'<a tooltip="{{row.entity[col.field][\'Name\']}}" tooltip-popup-delay="1000" tooltip-class="my-custom-tooltip" tooltip-placement="top" tooltip-append-to-body="true">' +
            '<div class="ngCellText" ng-class="col.colIndex()">' +
            '<input value="{{row.entity[col.field][\'Name\']}}" placeholder="Ед." style="border: none;" data-ng-click="updateSelectedCell(col.field, row.entity[col.field][\'Id\'],\'Тип оплаты\',row.entity,$event)" readonly>' +
            '</div>' +
            '</a>', headerCellTemplate: "<div   ng-class=\"'colt' + col.index\" class=\"ngHeaderText\">{{col.displayName}}</div>"

           },
	      {field: "Price", displayName: "Цена", width: "10%",
			 cellTemplate: '<a tooltip="{{row.entity[col.field][\'Name\']}}" tooltip-popup-delay="1000" tooltip-class="my-custom-tooltip" tooltip-placement="top" tooltip-append-to-body="true">' +
             '<div class="ngCellText" ng-class="col.colIndex()">' +
             '<input value="{{row.entity[col.field]}}" ng-model="row.entity[col.field]" placeholder="Цена" ng-class="{bordernone: row.entity[\'Id\']}" ng-readonly="row.entity[\'Id1\']"  ng-change="changeCountPrice(row.entity)">' +
             '</div>' +'</a>',  headerCellTemplate: "<div   ng-class=\"'colt' + col.index\" class=\"ngHeaderText\">{{col.displayName}}</div>"},
			 
	     {field: "Cost", displayName: "Сумма", width: "10%",
             cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()">' +
             '<input value="{{row.entity[col.field]}}" placeholder="0" style="border: none;" readonly>' +
             '</div>',  headerCellTemplate: "<div   ng-class=\"'colt' + col.index\" class=\"ngHeaderText\">{{col.displayName}}</div>"},
        {field: "Id", width: "0%"},
		{field: "Id1", width: "0%"}
    ];
	
     requests.serverCall.post($http, "ServiceCtrl/GetInfoService", { 
        }, function (data, status, headers, config) {
            console.log(data);
            sc.infoData = data;
        }, function () {
        });



    function loadService() {
        requests.serverCall.post($http, "ServiceCtrl/GetService", {
	 PageSize: sc.pagingOptions.pageSize,
     PageNum: sc.pagingOptions.currentPage,
        }, function (data, status, headers, config) {
			rowsForUpd = [];
		 sc.totalServerItems = data.CountItems;
            sc.pagingOptions.totalServerItems = data.CountItems;
            sc.TableData = data.Values;
        }, function () {
        });
    };
  loadService();
  
   sc.dateChange = function (row) {
	    if (!row.Id1 && row.Id) {
		     	    console.log(sc.selectedRow[0]);
				   rowsForUpd.push({
                   Document: sc.selectedRow[0],
                });	
		         }	
	
   }
  
      // мультивыделение
    sc.keyDownGrid = function (e) {
            if ((e.keyCode === 16 || e.keyCode === 17 || e.keyCode === 18 || e.keyCode === 91) && !sc.multi) {
            //   console.info("__________________ keyDown  " + e.keyCode);
                sc.multi = true;
             //   console.info(sc.multi);
            }
        
    };

    sc.keyUpGrid = function (e) {
        // 16 - Shift, 17 - Ctrl, 18 - Alt, 91 - Win key (Start)
            if (e.keyCode === 16 || e.keyCode === 17 || e.keyCode === 18 || e.keyCode === 91) {
           //     console.info("_________________ keyUp " + e.keyCode);
                sc.multi = false;
              //  console.info(sc.multi);
            }
        
    };
  
  
  

    sc.updateSelectedCell = function (colField, selectedVal, displayName, row) {
	
        if (!row.Id1) {
            sc.selectedRow = [];
            sc.selectedRow.push(row);
            var typeModal = modalType.WIN_SELECT;
            var modalValues;
            switch (colField) {
			case "TypeSalary":
                    modalValues = sc.infoData.TypeDocList;
                    break;
			 case "NameService":
             modalValues = sc.infoData.ServiceList;	
                break;
		     case "NamePlant":
             modalValues = sc.infoData.AllPlants;	
                break;
            }
            if (modalValues && modalValues.length > 0) {
                modals.showWindow(typeModal, {
                    nameField: displayName,
                    values: modalValues,
                    selectedValue: selectedVal
                })
                    .result.then(function (result) {
                      //  console.info(result);
                      //  console.info(colField);
					 if (!row.Id1 && row.Id) {
		     	   // console.log(sc.selectRow[0]);
				   rowsForUpd.push({
                   Document: sc.selectedRow[0],
                });	
		         }
						
                        sc.selectedRow[0][colField] = result;
                    }, function () {
                      
                    });
            } else {
              
            }
        }
    };

    sc.add = function() {	
	if (sc.TableData==undefined || sc.TableData.length==0) {sc.TableData=[];
	nextNumber=1; } else {nextNumber=parseInt(sc.TableData[0].Act_number)+1;}
		
        var newRow = {
            Date: new Date(),
            Count: 0,
            Price: 0,
            Cost: 0,
			Act_number :nextNumber
        };
        sc.TableData.splice(0,0,newRow);
    };
  //сохранить
    sc.save = function() {
        var rowsForSave = getNewRows();
        if (rowsForSave.complete) {
            saveRows(rowsForSave.rows, loadService);
        } else {
            saveError();
        }
    };
	
	function getMaxOfArray(numArray) {
   return Math.max.apply(null, numArray);
   }
	
	  sc.update = function() {
	console.log(sc.selectedRow[0]);
	for (i = 0; i < sc.TableData.length; i++) {	  
  if (sc.selectedRow[0].Id==sc.TableData[i].Id) sc.TableData[i].Id1=0;
	}  
	  }
	
	//////////////////скопировать
	  sc.copyRow = function() {
		  var con=getMaxOfArray()
		  var c=[];
		     console.log(con);
	for (i = 0; i < sc.TableData.length; i++) {
		c.push(sc.TableData[i].Act_number);
      }
	  con=getMaxOfArray(c);
        con = parseInt(con)+1;
			var newStr = {
            Date: sc.selectedRow[0].Date,
			TypeSalary : {Id:sc.selectedRow[0].TypeSalary.Id, Name: sc.selectedRow[0].TypeSalary.Name},
			NameService : {Id:sc.selectedRow[0].NameService.Id, Name: sc.selectedRow[0].NameService.Name},
			NamePlant : {Id:sc.selectedRow[0].NamePlant.Id, Name: sc.selectedRow[0].NamePlant.Name},
            Count: sc.selectedRow[0].Count,
		    Price: sc.selectedRow[0].Price,
		    Cost: sc.selectedRow[0].Cost,
            Act_number : con,
            NameWork: sc.selectedRow[0].NameWork,			
        };
        sc.TableData.splice(0, 0, newStr);     
	  }
	

	    // модальное окно с требованием выделить строку
    function needSelection(title, message) {
        modals.showWindow(modalType.WIN_MESSAGE, {
            title: title,
            message: message,
            callback: function () {
            }
        });
    }
	//удаление строки
	   sc.delRow = function() {
		    console.log(sc.selectedRow);
		    if (sc.selectedRow.length!=0) {
            modals.showWindow(modalType.WIN_DIALOG, {
                    title: "Удаление", message: "Удалить выбранный документ?", callback: function () {
						for (var i=0; i<sc.selectedRow.length;i++) {
						if (sc.selectedRow[i].Id==undefined) {
					    sc.TableData.splice(sc.TableData.indexOf(sc.selectedRow[i]), 1);
							}
						else {
				 rowsForDel.push({
                   rows:sc.selectedRow[i]
                });		
						}
						}
							console.log(rowsForDel);
							
                        requests.serverCall.post($http, "ServiceCtrl/DeleteService", rowsForDel, function (data, status, headers, config) {
							rowsForDel = [];
                       // sc.TableData.splice(sc.TableData.indexOf(sc.selectedRow[0]), 1);
					   loadService();
                            console.info(data);
                        });
						;}
                    }
                
            );
        } else needSelection('Удаление документа', "Для удаления необходимо выбрать документ");
	   }
	
    var saveError = function() {
        modals.showWindow(modalType.WIN_ERROR, {
            title: "Ошибка",
            message: "Заполните новые поля или удалите незаполненные строки",
            callback: function () {
            }
        });
    };
 ////

 //СЧИТАЕМ ПРОИЗВЕДЕНИЕ
     sc.changeCountPrice = function(row) {
			 if (!row.Id1 && row.Id) {
		     	    console.log(sc.selectedRow[0]);
				   rowsForUpd.push({
                   Document: sc.selectedRow[0],
                });	
		         }	 
	row.Act_number=row.Act_number.toString().replace(/[^0-9]/gim,'');	 
    row.Count=row.Count.toString().replace(/[^0-9]/gim,'');
	row.Price=row.Price.toString().replace(/[^0-9]/gim,'');
	row.Cost=row.Cost.toString().replace(/[^0-9]/gim,'');
     row.Count=row.Count.toString().replace(",", "."); 
     row.Price=row.Price.toString().replace(",", "."); 
            if (row.Price === null) {
                row.Price = 0;
            }
            if (row.Count === null) {
                row.Count = 0;
            }
       row.Cost = (parseFloat(row.Price) * parseFloat(row.Count)).toFixed(2);
        };
 //
    var saveRows = function (rowsForSave, cb) {
		
		if (rowsForUpd.length!=0) {
		    requests.serverCall.post($http, "ServiceCtrl/UpdateService", rowsForUpd, function (data, status, headers, config) {
			console.log(data);
			if (data!=undefined) {if (cb) cb();}		
                       rowsForUpd = [];		   
		 }, function() {
        });
		}
		
		if (rowsForSave.length!=0) {
        requests.serverCall.post($http, "ServiceCtrl/AddService", rowsForSave, function (data, status, headers, config) {
		if (data!=undefined) {if (cb) cb();}
        }, function() {
        });
		}
		
    };

    var getNewRows = function() {
        if (sc.TableData) {
            var newRows = [];
            var complete = true;
            for (var i = 0, li = sc.TableData.length; i < li; i++) {
                if (!sc.TableData[i].Id) {
                    var r = sc.TableData[i];
                    newRows.push(sc.TableData[i]);
      if (!r.Price || !r.Count || !r.TypeSalary || !r.NameService || !r.Act_number || !r.NameWork || !r.NamePlant) {
                        complete = false;
                    }
                }
            }
            return {
                rows: newRows,
                complete: complete
            };
        }
    };
	function sec1() { 
 $("div.ngFooterSelectedItems").remove();
}
setInterval(sec1, 200) 

	
	
	
}]);