var app = angular.module("SectionTechcard", ["ngGrid", "services", 'ngCookies', "modalWindows"]);
app.controller("SectionTechcardCtrl", ["$scope", "$http", "requests", "$location",'$stateParams',"modalType","modals", function ($scope, $http, requests, $location, $stateParams, modalType, modals) {
   
 var sc = $scope;
	sc.selectedRow2 = [];
	sc.TableData2 = [];
	 var rowsForSave = [];
	  $("#btn").show();
	  var date = new Date();
	  sc.year = date.getFullYear();
	  sc.year2 = date.getFullYear() + 1;
	 sc.formData2 = [];
	 sc.infoData2 = [];
	 sc.infoData2.YearList = [];
	 var nextNumber;
	 
	    function loadHarvestNZP()  {	
		  requests.serverCall.get($http, "AdministrationCtrl/GetSectionTechcard", function (data, status, headers, config) {
			console.log(data);
		    $("#nzp").empty();
            for (var i = 0; i < data.length; i++) {
			var id = data[i]
		    var val =  data[i];
	        $("#nzp").append( "<div id="+id+" style='font-size: 14pt;'>"+ val + 
	        "<button class='nzp' id='"+id+ "' ><img src='images/del.png'"+ 
	        "style='vertical-align: middle;height:30px;width:30px;margin-left:15px;' ></button></div>" );
			 } 
        }, function () {
        });

    };
	
	for (var i = 0; i <  6 ; i++) {
		sc.infoData2.YearList.push({Id : sc.year2, Name : sc.year2.toString()});
		sc.year2 = sc.year2 + 1;
	}
	loadHarvestNZP();
   //добавить
   /*
    sc.createNzp = function() {
		console.log('df');
		sc.addNzp = "true";
		sc.saveNzp = "false";
    };
	*/
	//сохранить
	 sc.clickSaveNzp = function() { 

	 requests.serverCall.get($http, "AdministrationCtrl/AddSectionTechcard", function (data, status, headers, config) {
	          loadHarvestNZP();
        }, function() {
        });
	
	//	 }
	 }
	 //удалить
     $('#nzp').on('click', '.nzp', function() {
        id = $( this ).attr('id') ;
	    console.log(id);
		   if (id) {
	   console.log(id);
	    console.log(this);
		 modals.showWindow(modalType.WIN_DIALOG, {
        title: "Предупреждение", message: "Удалить выбранную запись?", callback: function () {		
       requests.serverCall.post($http, "AdministrationCtrl/DeleteSectionTechcard", id, function (data, status, headers, config) {
	          loadHarvestNZP();
			  console.log('good');
        }, function() {
			console.log('bad');
        }); 
                    }
                });	
		}
     });
	 
		
	
}]);