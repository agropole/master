
var app = angular.module("ControlTech", ["ngGrid", "services", 'ngCookies', "modalWindows"]);
app.controller("TechControlCtrl", ["$scope", "$http", "requests", "$location",'$stateParams',"modalType","modals", function ($scope, $http, requests, $location, $stateParams, modalType, modals) {
    var sc = $scope;
	sc.formData = [];
	 $("#btn").show();
        function loadService() {
			requests.serverCall.get($http, "AdministrationCtrl/GetControlTech", function (data, status, headers, config) {
             sc.formData = data;
			 sc.summer_date = data.summer_date;
			  sc.winter_date = data.winter_date;
			}, function () {
			});
			};
		loadService();
  
  //сохранить
    sc.save = function() {
		if (parseFloat(sc.formData.Stop) >= parseFloat(sc.formData.Parking)) {
		  modals.showWindow(modalType.WIN_MESSAGE, {
            title: "Предупреждение",
            message: "Время остановки должно быть меньше времени стоянки.",
            callback: function () {
            }
        })	
		}
		
		else {
		sc.formData.summer_date = sc.summer_date;
		sc.formData.winter_date = sc.winter_date;
     requests.serverCall.post($http, "AdministrationCtrl/SaveControlTech",  sc.formData, function (data, status, headers, config) {
		  modals.showWindow(modalType.WIN_MESSAGE, {
            title: "Сохранение",
            message: "Данные сохранены.",
            callback: function () {
            }
        });
			}, function () {
			});
		}
	 
    };
	
	sc.changeNumber  = function() {
		sc.formData.Parking = sc.formData.Parking.replace(/[^0-9.]+/g, "");		
		sc.formData.Speed = sc.formData.Speed.replace(/[^0-9.]+/g, "");
		sc.formData.Stop = sc.formData.Stop.replace(/[^0-9.]+/g, "");
		sc.formData.MaxParking = sc.formData.MaxParking.replace(/[^0-9.]+/g, "");
	}
	

	
    var saveError = function() {
        modals.showWindow(modalType.WIN_ERROR, {
            title: "Ошибка",
            message: "Заполните новые поля или удалите незаполненные строки",
            callback: function () {
            }
        });
    };



    var saveRows = function (rowsForSave, cb) {
		
		if (rowsForUpd.length!=0) {
		    requests.serverCall.post($http, "ServiceCtrl/UpdateService", rowsForUpd, function (data, status, headers, config) {
			console.log(data);
			if (data!=undefined) {if (cb) cb();}		
                       rowsForUpd = [];		   
		 }, function() {
        });
		}
		
		if (rowsForSave.length!=0) {
        requests.serverCall.post($http, "ServiceCtrl/AddService", rowsForSave, function (data, status, headers, config) {
		if (data!=undefined) {if (cb) cb();}
        }, function() {
        });
		}
		
    };

	function sec1() { 
    $("div.ngFooterSelectedItems").remove();
  }
     setInterval(sec1, 200) 

	
	
	
}]);