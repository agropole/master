
var app = angular.module("ParamOrganization", ["ngGrid", "services", 'ngCookies', "modalWindows"]);
app.controller("ParamOrganizationCtrl", ["$scope", "$http", "requests", "$location",'$stateParams',"modalType","modals", function ($scope, $http, requests, $location, $stateParams, modalType, modals) {
    var sc = $scope;
	sc.formData = [];
	 $("#btn").show();
        requests.serverCall.get($http, "AdministrationCtrl/GetInfoParamOrganization", function (data, status, headers, config) {
            console.log(data);
            sc.infoData = data;
        }, function () {
        });

        function loadService() {
			requests.serverCall.get($http, "AdministrationCtrl/GetParamOrganization", function (data, status, headers, config) {
             sc.formData = data;
			}, function () {
			});
			};
		loadService();
  
  //сохранить
    sc.save = function() {
     requests.serverCall.post($http, "AdministrationCtrl/SaveParamOrganization",  sc.formData, function (data, status, headers, config) {
		  modals.showWindow(modalType.WIN_MESSAGE, {
            title: "Сохранение",
            message: "Данные сохранены.",
            callback: function () {
            }
        });
			}, function () {
			});

	 
    };
	
	sc.changeNumber  = function() {
		sc.formData.INN = sc.formData.INN.replace(/[^0-9]+/g, "");
		sc.formData.KPP = sc.formData.KPP.replace(/[^0-9]+/g, "");
		sc.formData.OGRN = sc.formData.OGRN.replace(/[^0-9]+/g, "");
	}
	
	
	 sc.changeOrg = function(id) { 
	 for (var i = 0; i < sc.infoData.OrgList.length; i++ ) {
		 if (sc.infoData.OrgList[i].Id == id) {
	 sc.formData.Adress = sc.infoData.OrgList[i].Description;
		 }
	 }
	 }
       



	
    var saveError = function() {
        modals.showWindow(modalType.WIN_ERROR, {
            title: "Ошибка",
            message: "Заполните новые поля или удалите незаполненные строки",
            callback: function () {
            }
        });
    };



    var saveRows = function (rowsForSave, cb) {
		
		if (rowsForUpd.length!=0) {
		    requests.serverCall.post($http, "ServiceCtrl/UpdateService", rowsForUpd, function (data, status, headers, config) {
			console.log(data);
			if (data!=undefined) {if (cb) cb();}		
                       rowsForUpd = [];		   
		 }, function() {
        });
		}
		
		if (rowsForSave.length!=0) {
        requests.serverCall.post($http, "ServiceCtrl/AddService", rowsForSave, function (data, status, headers, config) {
		if (data!=undefined) {if (cb) cb();}
        }, function() {
        });
		}
		
    };

	function sec1() { 
    $("div.ngFooterSelectedItems").remove();
  }
     setInterval(sec1, 200) 

	
	
	
}]);