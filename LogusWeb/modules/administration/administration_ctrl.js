var app = angular.module("Administration", ["ngGrid", "services", 'ngCookies', "modalWindows"]);
app.controller("AdministrationCtrl", ["$scope", "$http", "requests", "$location",'$stateParams',"modalType","modals", function ($scope, $http, requests, $location, $stateParams, modalType, modals) {
    //мой
 var sc = $scope;
	sc.selectedRow = [];
	 var rowsForDel = [];
	 var rowsForUpd = [];
	  $("#btn").show();
	 sc.multi = false; var met = 1;
	 sc.formData = [];
	 sc.infoData = [];
	 var nextNumber;
	    // пейджинг
    sc.totalServerItems = 0;
    sc.pagingOptions = {
        pageSizes: [10, 15, 20, 50, 100],
        pageSize: 10,
        currentPage: 1,
        totalServerItems: sc.totalServerItems
    };
	   // смотрим за изменениям в пейджинге
    sc.$watch('pagingOptions', function (newVal, oldVal) {
        if (newVal.currentPage != null && newVal !== oldVal && !isNaN(newVal.currentPage) && !isNaN(oldVal.currentPage)) {
            if (newVal.pageSize !== oldVal.pageSize) {
                sc.pagingOptions.currentPage = 1;
            loadService();
            }
            if (newVal.currentPage !== oldVal.currentPage) 
			{ 
			loadService();
			
			;}
        }
    }, true);
	
	
	// $location.path('/roles');
	
	
    sc.gridOptions = {
        enableColumnResize: true,
        enableRowSelection: true,
        showFooter: true,
        rowHeight: 49,
        headerRowHeight: 49,
        useExternalSorting: false,
        data: 'TableData',
		enablePaging: true,
        pagingOptions: sc.pagingOptions,
        selectedItems: sc.selectedRow,
		i18n: 'ru',

     afterSelectionChange: function (row) {	
	met =1;
	for (var i=0; i<row.length;i++ ) 
   { 
     sc.selectedRow.push(row[i].entity); 
     console.log(sc.selectedRow); 
        } 
		if (sc.mySelections1 == row.entity) { console.log('есть');met=2;}
			sc.mySelections1 = row.entity;
	  if (row.entity) {
		  console.log(row.selected);
                if (row.selected) {
                    if (sc.multi) sc.selectedRow.push(row.entity);

                    else {
                        sc.selectedRow = [];
                        sc.selectedRow.push(row.entity);
                    }
                } else {
                    sc.selectedRow.splice(sc.TableData.indexOf(row.entity), 1);
                }
            }
	 console.log(sc.selectedRow);
    },   
    beforeSelectionChange: function (row, event) {
		   if (!sc.multi) {
                angular.forEach(sc.TableData, function (data, index) {
                    if (data.selected == true) sc.selectedRow.splice(index, 1);
                    sc.gridOptions.selectRow(index, false);

                });
            }
                return true;
            }
    };

   sc.gridOptions.columnDefs = [
        {field: "NameUser", displayName: "Сотрудник", width: "20%", 
		  cellTemplate: '<a tooltip="{{row.entity[col.field][\'Name\']}}" tooltip-popup-delay="1000" tooltip-class="my-custom-tooltip" tooltip-placement="top" tooltip-append-to-body="true">' +
          '<div class="ngCellText" ng-class="col.colIndex()">' +
          '<input value="{{row.entity[col.field][\'Name\']}}" placeholder="Сотрудник" style="border: none;"  data-ng-click="updateSelectedCell(col.field, row.entity[col.field][\'Id\'],\'Список сотрудников\',row.entity,$event)" readonly >' +
          '</div>', headerCellTemplate: "<div   ng-class=\"'colt' + col.index\" class=\"ngHeaderText\">{{col.displayName}}</div>"
          },
		  
	    {field: "NameRole", displayName: "Роль", width: "20%",
          cellTemplate: '<a tooltip="{{row.entity[col.field][\'Name\']}}" tooltip-popup-delay="1000" tooltip-class="my-custom-tooltip" tooltip-placement="top" tooltip-append-to-body="true">' +
          '<div class="ngCellText" ng-class="col.colIndex()">' +
          '<input value="{{row.entity[col.field][\'Name\']}}" placeholder="Роль" style="border: none;"  data-ng-click="updateSelectedCell(col.field, row.entity[col.field][\'Id\'],\'Роли\',row.entity,$event)"  readonly>' +
          '</div>', headerCellTemplate: "<div   ng-class=\"'colt' + col.index\" class=\"ngHeaderText\">{{col.displayName}}</div>"
          },
		{field: "UserLogin", displayName: "Логин", width: "15%",
           cellTemplate: '<a tooltip="{{row.entity[col.field][\'Name\']}}" tooltip-popup-delay="1000" tooltip-class="my-custom-tooltip" tooltip-placement="top" tooltip-append-to-body="true">' +
             '<div class="ngCellText" ng-class="col.colIndex()">' +
             '<input value="{{row.entity[col.field]}}" ng-model="row.entity[col.field]" placeholder=" " ng-class="{bordernone: row.entity[\'Id\']}"   ng-change="changeCountPrice(row.entity)">' +
           '</div>' +'</a>',  headerCellTemplate: "<div   ng-class=\"'colt' + col.index\" class=\"ngHeaderText\">{{col.displayName}}</div>"},
		{ field: "Password", displayName: "Пароль", width: "15%",
          cellTemplate: '<a tooltip="{{row.entity[col.field][\'Name\']}}" tooltip-popup-delay="1000" tooltip-class="my-custom-tooltip" tooltip-placement="top" tooltip-append-to-body="true">' +
             '<div class="ngCellText" ng-class="col.colIndex()">' +
             '<input value="{{row.entity[col.field]}}" ng-model="row.entity[col.field]" placeholder=" " ng-class="{bordernone: row.entity[\'Id\']}"   ng-change="changeCountPrice(row.entity)">' +
           '</div>' +'</a>',  headerCellTemplate: "<div   ng-class=\"'colt' + col.index\" class=\"ngHeaderText\">{{col.displayName}}</div>"},
	    {field: "Note", displayName: "Примечание", width: "30%",
			 cellTemplate: '<a tooltip="{{row.entity[col.field][\'Name\']}}" tooltip-popup-delay="1000" tooltip-class="my-custom-tooltip" tooltip-placement="top" tooltip-append-to-body="true">' +
             '<div class="ngCellText" ng-class="col.colIndex()">' +
             '<input value="{{row.entity[col.field]}}" ng-model="row.entity[col.field]" placeholder=" " ng-class="{bordernone: row.entity[\'Id\']}"   ng-change="changeCountPrice(row.entity)">' +
             '</div>' +'</a>',  headerCellTemplate: "<div   ng-class=\"'colt' + col.index\" class=\"ngHeaderText\">{{col.displayName}}</div>"},
		{field: "Id", width: "0%"}
    ];

    requests.serverCall.post($http, "AdministrationCtrl/GetInfoUser", { 
        }, function (data, status, headers, config) {
            console.log(data);
            sc.infoData = data;
        }, function () {
        });

    function loadService() {
		var fl = false;
		if (sc.TableData != undefined) {
		for (var i = 0; i < sc.TableData.length; i++) { 
			for (var j=1; j<sc.TableData.length; j++) {
				if ( i!=j )	{
					if (sc.TableData[i].UserLogin==sc.TableData[j].UserLogin) {
						fl = true;
					}
				}
			}
		}
		}
		
			
		
	if (!fl) { 
     requests.serverCall.post($http, "AdministrationCtrl/GetUsers", {
	 PageSize: sc.pagingOptions.pageSize,
     PageNum: sc.pagingOptions.currentPage,
        }, function (data, status, headers, config) {
			rowsForUpd = [];
		 sc.totalServerItems = data.CountItems;
            sc.pagingOptions.totalServerItems = data.CountItems;
            sc.TableData = data.Values;
        }, function () {
        });
		}
    };
  loadService();
  

  		sc.goToRole = function () {
			 $location.path('/roles');
		}
		
  
  
      // мультивыделение
    sc.keyDownGrid = function (e) {
            if ((e.keyCode === 16 || e.keyCode === 17 || e.keyCode === 18 || e.keyCode === 91) && !sc.multi) {
                sc.multi = true;
            }   
    };

    sc.keyUpGrid = function (e) {
            if (e.keyCode === 16 || e.keyCode === 17 || e.keyCode === 18 || e.keyCode === 91) {
                sc.multi = false;
            }    
    };
  
  
  

    sc.updateSelectedCell = function (colField, selectedVal, displayName, row) {
			met=1;				 
            sc.selectedRow.push(row);
			if (sc.mySelections1==row) {met=2;}
			  console.log(row);
			sc.mySelections1 = row;
            sc.selectedRow.push(row);
            var typeModal = modalType.WIN_SELECT;
            var modalValues;
			if (met == 2) {
           switch (colField) {
			case "NameUser":
			 	 var newEmployees = []; var newEmployees1 = [];
					 
					 
					  for (i = 0; i < sc.infoData.UserList.length; i++) {
						  
						  
						  
					newEmployees.push({Name: sc.infoData.UserList[i].Name,
					                     Id : sc.infoData.UserList[i].Id,
										 index : i,
					});
					  }
					  for (var i = 0; i < sc.TableData.length; i++) {
						   for (var j = 0;j < newEmployees.length; j++) {
							 if (sc.TableData[i].NameUser!=null && sc.TableData[i].NameUser.Id == newEmployees[j].Id)
						   {
					    console.log('равно');
						newEmployees[j].index = -1;
						   }
						   }
					  }
					  console.log(newEmployees);
					  for (i = 0; i < newEmployees.length; i++) {
               if (newEmployees[i].index!=-1) {	
				newEmployees1.push({Name: newEmployees[i].Name,
					                     Id : newEmployees[i].Id,
										 index : i,
					});
			   }      
                }
			 modalValues = newEmployees1;
			 
                    break;
			 case "NameRole":
			 if (row.NameUser.Name !='Администратор') {
             modalValues = sc.infoData.RoleList;	
			 }
                break;
			}
            if (modalValues && modalValues.length > 0) {
                modals.showWindow(typeModal, {
                    nameField: displayName,
                    values: modalValues,
                    selectedValue: selectedVal
                })
                    .result.then(function (result) {
				   rowsForUpd.push({
                   Document: sc.selectedRow[0],
                });	
                   sc.selectedRow[0][colField] = result;
                    }, function () {
                      
                    });
            } 
	}
    };

    sc.create = function() {
        var newRow = {};
        sc.TableData.splice(0,0,newRow); 	
    };
  //сохранить
    sc.save = function() {
        var rowsForSave = getNewRows();
        if (rowsForSave.complete) {
            saveRows(rowsForSave.rows, loadService);
        } else {
            saveError();
        }
    };
	
	function getMaxOfArray(numArray) {
   return Math.max.apply(null, numArray);
   }
	
	  sc.update = function() {
	console.log(sc.selectedRow[0]);
	for (i = 0; i < sc.TableData.length; i++) {	  
  if (sc.selectedRow[0].Id==sc.TableData[i].Id) sc.TableData[i].Id1=0;
	}  
	  }
	
	

	    // модальное окно с требованием выделить строку
    function needSelection(title, message) {
        modals.showWindow(modalType.WIN_MESSAGE, {
            title: title,
            message: message,
            callback: function () {
            }
        });
    }


	 sc.delRow = function() {
		    console.log(sc.selectedRow);
		    if (sc.selectedRow.length!=0) {
            modals.showWindow(modalType.WIN_DIALOG, {
                    title: "Удаление", message: "Удалить выбранные записи?", callback: function () {
						for (var i=0; i<sc.selectedRow.length;i++) {
						if (sc.selectedRow[i].Id==undefined) {
					    sc.TableData.splice(sc.TableData.indexOf(sc.selectedRow[i]), 1);
							}
						else {
				 rowsForDel.push({
                   rows:sc.selectedRow[i]
                });		
						}
						}
					console.log(rowsForDel);
                        requests.serverCall.post($http, "AdministrationCtrl/DeleteUser", rowsForDel, function (data, status, headers, config) {
							rowsForDel = [];
					   loadService();
                            console.info(data);
                        });
						;}
                    }
                
            );
        } else needSelection('Удаление записи', "Для удаления необходимо выбрать запись");
	   }
	
	
	
	
    var saveError = function() {
        modals.showWindow(modalType.WIN_ERROR, {
            title: "Ошибка",
            message: "Заполните новые поля или удалите незаполненные строки",
            callback: function () {
            }
        });
    };
	
	sc.cancel = function() {
        loadService();
    };
	
	
    var saveRows = function (rowsForSave, cb) {
		
		if (rowsForUpd.length!=0) {
		    requests.serverCall.post($http, "AdministrationCtrl/UpdateDic", rowsForUpd, function (data, status, headers, config) {
			console.log(data);
			if (data!=undefined) {if (cb) cb();}		
                       rowsForUpd = [];		   
		 }, function() {
        });
		}
		
		if (rowsForSave.length!=0) {
        requests.serverCall.post($http, "AdministrationCtrl/AddDic", rowsForSave, function (data, status, headers, config) {
		if (data!=undefined) {if (cb) cb();}
        }, function() {
        });
		}
		
    };
	
	 sc.changeCountPrice = function(row) {
			if (row.Id) {
		     	console.log(sc.selectedRow[0]);
				rowsForUpd.push({
					Document: sc.selectedRow[0],
                });	
		    }	 
     };
    var getNewRows = function() {
        if (sc.TableData) {
            var newRows = [];
            var complete = true;
            for (var i = 0, li = sc.TableData.length; i < li; i++) {
                if (!sc.TableData[i].Id) {
                    var r = sc.TableData[i];
                    newRows.push(sc.TableData[i]);
     if (!r.UserLogin || !r.Password || !r.NameUser || !r.NameRole || r.NameUser.Id==null || r.NameRole.Id==null) {
                        complete = false;
                    }
                }
            }
            return {
                rows: newRows,
                complete: complete
            };
        }
    };
	function sec1() { 
 $("div.ngFooterSelectedItems").remove();
}
setInterval(sec1, 200) ;

	
	
	
}]);