var app = angular.module("Roles", ["ngGrid", "services", 'ngCookies', "modalWindows"]);
app.controller("RolesCtrl", ["$scope", "$http", "requests", "$location",'$stateParams',"modalType","modals", function ($scope, $http, requests, $location, $stateParams, modalType, modals) {
    //мой
 var sc = $scope;
	sc.selectedRow = [];
	 var rowsForDel = [];
	 var rowsForUpd = [];
	 sc.TableData = [];
	 sc.rowData = [];
	  $("#btn").show();
	 sc.multi = false; var met = 1;
	 sc.formData = [];
	 sc.infoData = [];
	 var nextNumber;
	    // пейджинг
    sc.totalServerItems = 0;
    sc.pagingOptions = {
        pageSizes: [10, 15, 20, 50, 100],
        pageSize: 10,
        currentPage: 1,
        totalServerItems: sc.totalServerItems
    };
	   // смотрим за изменениям в пейджинге
    sc.$watch('pagingOptions', function (newVal, oldVal) {
        if (newVal.currentPage != null && newVal !== oldVal && !isNaN(newVal.currentPage) && !isNaN(oldVal.currentPage)) {
            if (newVal.pageSize !== oldVal.pageSize) {
                sc.pagingOptions.currentPage = 1;
            loadService();
            }
            if (newVal.currentPage !== oldVal.currentPage) 
			{ 
			loadService();
			
			;}
        }
    }, true);
	
	
    sc.gridOptions = {
        enableColumnResize: true,
        enableRowSelection: true,
        showFooter: true,
        rowHeight: 49,
        headerRowHeight: 49,
        useExternalSorting: false,
        data: 'TableData',
		enablePaging: true,
        pagingOptions: sc.pagingOptions,
        selectedItems: sc.selectedRow,
		i18n: 'ru',

     afterSelectionChange: function (row) {	
	 met =1;
	for (var i=0; i<row.length;i++ ) 
   { 
     sc.selectedRow.push(row[i].entity); 
     console.log(sc.selectedRow); 
        } 
		if (sc.mySelections1 == row.entity) { console.log('есть');met=2;}
			sc.mySelections1 = row.entity;
	  if (row.entity) {
		//  console.log(row.selected);
                if (row.selected) {
                    if (sc.multi) sc.selectedRow.push(row.entity);

                    else {
                        sc.selectedRow = [];
                        sc.selectedRow.push(row.entity);
                    }
                } else {
                    sc.selectedRow.splice(sc.TableData.indexOf(row.entity), 1);
                }
            }
	 //console.log(sc.selectedRow);
    },   
    beforeSelectionChange: function (row, event) {
		
		console.log(row.entity );
		
		if (row.entity.Id == 2 || row.entity.Id == 4 || row.entity.Id == 10 || row.entity.Id == 14 ||
		      row.entity.Id == 15 || row.entity.Id == 16 || row.entity.Id == 17) {
				$("#del").addClass("disabledbutton");    
			  }   else {
				$("#del").removeClass("disabledbutton");        
			  }
		
		
		
		
		
		   if (!sc.multi) {
                angular.forEach(sc.TableData, function (data, index) {
                    if (data.selected == true) sc.selectedRow.splice(index, 1);
                    sc.gridOptions.selectRow(index, false);

                });
            }
                return true;
            }
    };

   sc.gridOptions.columnDefs = [
           {field: "Name", displayName: "Название", width: "50%",
			 cellTemplate: '<a tooltip="{{row.entity[col.field][\'Name\']}}" tooltip-popup-delay="1000" tooltip-class="my-custom-tooltip" tooltip-placement="top" tooltip-append-to-body="true">' +
             '<div class="ngCellText" ng-class="col.colIndex()">' +
             '<input value="{{row.entity[col.field]}}"  ng-readonly="{{checkConstRole(row.entity.Id)}}"  ng-model="row.entity[col.field]" placeholder=" " ng-class="{bordernone: row.entity[\'Id\']}"   ng-change="changeCountPrice(row.entity)">' +
             '</div>' +'</a>',  headerCellTemplate: "<div   ng-class=\"'colt' + col.index\" class=\"ngHeaderText\">{{col.displayName}}</div>" },
		  
		   {
            field: "EmpList", displayName: "Подсистемы", width: "50%",
            cellTemplate: '<div class="{{getClassChanges()}} ngCellText" ng-class="col.colIndex()" style="padding: 0px 5px;">' +
            '<input ng-value="getValueForInput(row.entity.EmpList)" placeholder="Подсистемы" style="border: none; height: 98%" data-ng-click="updateSelectedCell(\'EmpList\', row.entity.EmpList.Id,\'Подсистемы\',row.entity,$event)"  readonly>' +
            '</div>'
        },
		
		{field: "Id", width: "0%"}
    ];

	
	   sc.getValueForInput = function(array) {
		//   console.log(array);
        var str = "";
        if (array && array.length > 0) {
            for (var i = 0, li = array.length; i < li; i++) {
                str += array[i].Name + ", "
            }
            str = str.substr(0, str.length - 2);
        }
        return str;
    };
	
	sc.checkConstRole = function(Id) {
			if (Id == 2 || Id == 4 || Id== 10 || Id== 14 ||
		      Id== 15 || Id == 16 || Id == 17) {
				return true; 
			  }   else {
				return false;      
			  }		
		// console.log(Id);	
	}
	
	
	
	/*
    requests.serverCall.post($http, "AdministrationCtrl/GetInfoUser", { 
        }, function (data, status, headers, config) {
            console.log(data);
            sc.infoData = data;
        }, function () {
        });
           */
		
    function loadService() {
     requests.serverCall.post($http, "AdministrationCtrl/GetRoles", {
	 PageSize: sc.pagingOptions.pageSize,
     PageNum: sc.pagingOptions.currentPage,
        }, function (data, status, headers, config) {
			rowsForUpd = [];
		 sc.totalServerItems = data.CountItems;
            sc.pagingOptions.totalServerItems = data.CountItems;
            sc.TableData = data.Values;
			
			for (var i=0; i < sc.TableData.length; i++ ) {
			for (var j=0; j < sc.TableData[i].EmpList.length; j++ ) {
				
			sc.TableData[i].EmpList[j].Name =   $('#' + sc.TableData[i].EmpList[j].IdBox ).parent().text() ;	
			}
				
				
			}
			
			
        }, function () {
        });
    };
     loadService();
  
 
      sc.goToAdmin  = function () {
		$location.path('/administration');  
	  } 
  
         sc.no = function () {
	      $("#myModal").modal("hide");
	      $("div.modal-backdrop").hide();
	      }
		  
      sc.cancelWin = function () {
		$("#myModal").modal("hide");    
	  }
  
        var arr = [];
          //сохранить роль
      sc.yes = function () {
			    $.each( $('#roles li'), function(index, el) {
			   var id = $(el).find('input').attr('id');
			   var value = $(el).find('input').attr('value');
			   var name = $(el).text();
			   var isCheck =  $("#" + id).prop('checked');
			  //  console.log(isCheck);	 
           if (isCheck) {
               var elem = {Id:index, Name: name, Value: value, IdBox: id };
	           arr.push(elem);
		   }
			
             });
				
		console.log(sc.selectedRow[0]);  
        sc.selectedRow[0].EmpList = arr;
		arr = [];
		console.log(sc.selectedRow[0]);  
		  
		  
		  
		  
	  $("#myModal").modal("hide");
	  $("div.modal-backdrop").hide();
	//	  cleanChecked();
	  }
  
        function cleanChecked() {
			 $.each( $('#roles li'), function(index, el) {
			var id = $(el).find('input').attr('id');
			$("#" + id).prop('checked', false);
			 });
		}
  
  
  
         sc.checkTas1 = false; sc.checkTas2 = false;
		 
		 //открытие списка
         sc.clickTas1 = function () {
		 console.log(sc.checkTas1);
		 if (sc.checkTas1) 
		 {
			 sc.checkTas1 = false;  
    	 } else {
			 sc.checkTas1 = true;
		 }	 		 
	 }
  	 //открытие списка2
         sc.clickTas2 = function () {
		 if (sc.checkTas2) 
		 {
			 sc.checkTas2 = false;  
    	 } else {
			 sc.checkTas2 = true;
		 }	 		 
	 }
  	 //открытие списка3
         sc.clickTas3 = function () {
		 if (sc.checkTas3) 
		 {
			 sc.checkTas3 = false;  
    	 } else {
			 sc.checkTas3 = true;
		 }	 		 
	 }
  
            //поставить всем
       $("#tasM").change(function() {
			//console.log( $("#tasM").prop('checked') );		
			 $.each( $("#tasUl li"), function(index, element) {
			 $("#tas" + index).prop('checked', $("#tasM").prop('checked')); 	 	 
             });
			
			});
  
      $("#tasM2").change(function() {
			 $.each( $("#planUl2 li"), function(index, element) {
			 $("#plan" + (index)).prop('checked', $("#tasM2").prop('checked')); 	 	 
             });
			
			});
  
    $("#tasM3").change(function() {
		//	console.log( $("#tasM2").prop('checked') );
			 $.each( $("#garUl3 li"), function(index, element) {
			 $("#gar" + (index)).prop('checked', $("#tasM3").prop('checked')); 	 	 
             });
			
			});
  
      // мультивыделение
    sc.keyDownGrid = function (e) {
            if ((e.keyCode === 16 || e.keyCode === 17 || e.keyCode === 18 || e.keyCode === 91) && !sc.multi) {
                sc.multi = true;
            }   
    };

    sc.keyUpGrid = function (e) {
            if (e.keyCode === 16 || e.keyCode === 17 || e.keyCode === 18 || e.keyCode === 91) {
                sc.multi = false;
            }    
    };
  
  
  

    sc.updateSelectedCell = function (colField, selectedVal, displayName, row) {
			met = 1;				 
            sc.selectedRow.push(row);
			if (sc.mySelections1==row) {met = 2;}
			  console.log(row);
			sc.mySelections1 = row;
            sc.selectedRow.push(row);
            var typeModal = modalType.WIN_SELECT;
            var modalValues;
			if (met == 2) {
            switch (colField) {
			     case "EmpList":
				   cleanChecked();
				   
				   if (sc.selectedRow[0].EmpList != undefined) {
				   for (var i = 0; i < sc.selectedRow[0].EmpList.length; i++) {
					  $("#" + sc.selectedRow[0].EmpList[i].IdBox ).prop('checked', true); 
					      
				   }
				   }
				  if (sc.selectedRow[0].Id == 2 ||  sc.selectedRow[0].Id == 4 || sc.selectedRow[0].Id == 14 ||
				   sc.selectedRow[0].Id == 15 || sc.selectedRow[0].Id == 16  || sc.selectedRow[0].Id == 10 || sc.selectedRow[0].Id == 17) {
				   $("input[type=checkbox]").attr("disabled", true);
				   } else {
					 $("input[type=checkbox]").attr("disabled", false);
				   }
				   
				    $("#myModal").modal("show");
									
                        break;
			}
            if (modalValues && modalValues.length > 0) {
                modals.showWindow(typeModal, {
                    nameField: displayName,
                    values: modalValues,
                    selectedValue: selectedVal
                })
                    .result.then(function (result) {
				   rowsForUpd.push({
                   Document: sc.selectedRow[0],
                });	
                   sc.selectedRow[0][colField] = result;
                    }, function () {
                      
                    });
            } 
	}
    };

    sc.create = function() {
        var newRow = {	
		Id : null,
		Name: null,
		EmpList : [],
		
		};
        sc.TableData.push(newRow); 	
    };
  //сохранить
    sc.save = function() {
        var rowsForSave = getNewRows();
        if (rowsForSave.complete) {
            saveRows(rowsForSave.rows, loadService);
        } else {
            saveError();
        }
    };
	
	function getMaxOfArray(numArray) {
   return Math.max.apply(null, numArray);
   }
	
	  sc.update = function() {
	console.log(sc.selectedRow[0]);
	for (i = 0; i < sc.TableData.length; i++) {	  
  if (sc.selectedRow[0].Id==sc.TableData[i].Id) sc.TableData[i].Id1 = 0;
	}  
	  }
	
	

	    // модальное окно с требованием выделить строку
    function needSelection(title, message) {
        modals.showWindow(modalType.WIN_MESSAGE, {
            title: title,
            message: message,
            callback: function () {
            }
        });
    }


	 sc.delRow = function() {
		    console.log(sc.selectedRow);
		    if (sc.selectedRow.length!=0) {
            modals.showWindow(modalType.WIN_DIALOG, {
                    title: "Удаление", message: "Удалить выбранные записи?", callback: function () {
						for (var i=0; i<sc.selectedRow.length;i++) {
						if (sc.selectedRow[i].Id==undefined) {
					    sc.TableData.splice(sc.TableData.indexOf(sc.selectedRow[i]), 1);
							}
						else {
				 rowsForDel.push({
                   rows:sc.selectedRow[i]
                });		
						}
						}
					console.log(rowsForDel);
                        requests.serverCall.post($http, "AdministrationCtrl/DeleteRole", rowsForDel, function (data, status, headers, config) {
							rowsForDel = [];
					   loadService();
                            console.info(data);
                        });
						;}
                    }
                
            );
        } else needSelection('Удаление записи', "Для удаления необходимо выбрать запись");
	   }
	
	
	
	
    var saveError = function() {
        modals.showWindow(modalType.WIN_ERROR, {
            title: "Ошибка",
            message: "Заполните новые поля или удалите незаполненные строки",
            callback: function () {
            }
        });
    };
	
    var saveRows = function (rowsForSave, cb) {
		
		console.log(sc.TableData);
          requests.serverCall.post($http, "AdministrationCtrl/AddEditDicRole", sc.TableData, function (data, status, headers, config) {
		  if (data != undefined) {if (cb) cb();}
          }, function() {
          });

		
    };
	
	 sc.changeCountPrice = function(row) {
			if (row.Id) {
		     	console.log(sc.selectedRow[0]);
				rowsForUpd.push({
					Document: sc.selectedRow[0],
                });	
		    }	 
     };
    var getNewRows = function() {
        if (sc.TableData) {
            var newRows = [];
            var complete = true;
            for (var i = 0, li = sc.TableData.length; i < li; i++) {
                if (!sc.TableData[i].Id) {
                    var r = sc.TableData[i];
                    newRows.push(sc.TableData[i]);
     if (!r.Name || r.EmpList == undefined || r.EmpList.length == 0  ) {
                        complete = false;
                    }
                }
            }
            return {
                rows: newRows,
                complete: complete
            };
        }
    };
	  function sec1() { 
      $("div.ngFooterSelectedItems").remove();
}
       setInterval(sec1, 200) ;

	
	
	
}]);