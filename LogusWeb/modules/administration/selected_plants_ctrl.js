var app = angular.module("SelectedPlants", ["ngGrid", "services", 'ngCookies', "modalWindows"]);
app.controller("SelectedPlantsCtrl", ["$scope", "$http", "requests", "$location",'$stateParams',"modalType","modals",'LogusConfig', function ($scope, $http, requests, $location, $stateParams, modalType, modals,LogusConfig) {
    //мой
 var sc = $scope;

	 var rowsForSave = [];
	  $("#btn").show();
	  
	 var date = new Date();
	 sc.year = date.getFullYear();
	 sc.year3 = date.getFullYear() + 1;
	 sc.formData = [];
	 sc.infoData = [];
	 sc.formData2 = [];
	 sc.infoData2 = [];
	 sc.infoData2.YearList = [];
	 var nextNumber;
	 
	    function loadService() {	
		  requests.serverCall.get($http, "AdministrationCtrl/GetPlant", function (data, status, headers, config) {
			  console.log(data);
		    $("#culture").empty();
            for (var i = 0; i < data.length; i++) {
			var id = data[i].id;
			var name =  data[i].name;
		    var year =  data[i].year;
	   $("#culture").append( "<div id="+id+" style='font-size: 14pt;'>"+name+" "+year + 
	   "<button class='cls' id='"+id+ "' ><img src='images/del.png'"+ 
	   "style='vertical-align: middle;height:30px;width:30px;margin-left:15px;' ></button></div>" );
			 } 
        }, function () {
        });

    };
		    function loadHarvestNZP() {	
		  requests.serverCall.get($http, "AdministrationCtrl/GetHarvestNZP", function (data, status, headers, config) {
			console.log(data);
		    $("#nzp").empty();
			$("#crop").empty();
            for (var i = 0; i < data.length; i++) {
			var id = data[i].id;
		    var year =  data[i].year;
	        $("#nzp").append( "<div id="+id+" style='font-size: 14pt;'>Урожай "+year + 
	        "<button class='nzp' id='"+id+ "' ><img src='images/del.png'"+ 
	        "style='vertical-align: middle;height:30px;width:30px;margin-left:15px;' ></button></div>" );
			 $("#crop").append( "<div  style='font-size: 14pt;'>Урожай "+year+" </div>" );
			 } 
        }, function () {
        });

    };
	
	for (var i = 0; i <  6 ; i++) {
		sc.infoData2.YearList.push({Id : sc.year3, Name : sc.year3.toString()});
		sc.year3 = sc.year3 + 1;
	}
	loadHarvestNZP();
	
	    requests.serverCall.post($http, "AdministrationCtrl/GetInfoPlants",0 , function (data, status, headers, config) {
           sc.infoData = data ; 
		      loadService();
        }, function () {
        });
	 
	 sc.changeYear = function() {
		 requests.serverCall.post($http, "AdministrationCtrl/GetInfoPlants", sc.formData.yearId , function (data, status, headers, config) {
           sc.infoData.AllPlants = data.AllPlants ; 
        }, function () {
        }); 
	 }
	 
	 //замена культуры 
	 sc.changeCult = function() { 
			var popupWin = window.open(LogusConfig.serverUrl + 'AdministrationCtrl/ChangeCulture', '_blank');
	 }
	 
	 
   //добавить
    sc.create = function() {
		console.log('df');
		sc.add = "true";
		sc.save = "false";
    };
	//сохранить
	 sc.clickSave = function() {
		 
		 if (sc.formData.plantId && sc.formData.yearId) {
		sc.save = "true";
		sc.add = "false";	
	    rowsForSave = [];
	    rowsForSave.push({plantId : sc.formData.plantId, year : sc.formData.yearId});
	
	 requests.serverCall.post($http, "AdministrationCtrl/AddPlant", rowsForSave[0], function (data, status, headers, config) {
	          loadService();
			  sc.formData.plantId = null;
			  sc.formData.yearId = null;
        }, function() {
			sc.formData.plantId = null;
			sc.formData.yearId = null;
        });
	
		 }
	 }
	 //удалить
     $('#culture').on('click', '.cls', function() {
        id = $( this ).attr('id') ;
	    console.log(id);
		   if (id) {
	   console.log(id);
	    console.log(this);
		     modals.showWindow(modalType.WIN_DIALOG, {
                    title: "Предупреждение", message: "Удалить выбранную запись?", callback: function () {		
       requests.serverCall.post($http, "AdministrationCtrl/DeletePlant", id, function (data, status, headers, config) {
	          loadService();
        }, function() {
        }); 
                    }
                });	
		}
     });
	  //========================NZP
   sc.createNzp = function() {
		console.log('df');
		sc.addNzp = "true";
		sc.saveNzp = "false";
    };
	//сохранить
	 sc.clickSaveNzp = function() {
		 
		if (sc.formData2.yearId) {
		sc.saveNzp = "true";
		sc.addNzp = "false";	
	    rowsForSave = [];
	    rowsForSave.push({plantId : 36, year : sc.formData2.yearId});
	
	 requests.serverCall.post($http, "AdministrationCtrl/AddHarvestNZP", rowsForSave[0], function (data, status, headers, config) {
	          loadHarvestNZP();
        }, function() {
        });
	
		 }
	 }
	 //удалить
     $('#nzp').on('click', '.nzp', function() {
        id = $( this ).attr('id') ;
	    console.log(id);
		   if (id) {
	   console.log(id);
	    console.log(this);
		 modals.showWindow(modalType.WIN_DIALOG, {
        title: "Предупреждение", message: "Удалить выбранную запись?", callback: function () {		
       requests.serverCall.post($http, "AdministrationCtrl/DeleteHarvestNZP", id, function (data, status, headers, config) {
	          loadHarvestNZP();
			  console.log('good');
        }, function() {
			console.log('bad');
        }); 
                    }
                });	
		}
     });


}]);