var app = angular.module("RateFuel", ["services", 'ngCookies', "modalWindows", "LogusConfig"]);
app.controller("RateFuelCtrl", ["$scope", "$http", "requests", "modals", "modalType", "LogusConfig", function ($scope, $http, requests, modals, modalType, LogusConfig) {
	var sc = $scope;
    sc.formData = [];
	sc.dataTech = [];
	sc.dataTech.Car = true;
	sc.dataTech.Tech = false;
	function load(Car, Tech) {
		requests.serverCall.post($http, "RateFuel/Get", {Car, Tech}, function (data, status, headers, config) {
	    sc.formData = data;
		});
	};
	 load(sc.dataTech.Car,sc.dataTech.Tech);

		sc.getTech = function()  {
			if (!sc.dataTech.Car && !sc.dataTech.Tech) {
				sc.dataTech.Tech = true;
			}
			load(sc.dataTech.Car,sc.dataTech.Tech);
		}
		sc.getCar = function()  {
			if (!sc.dataTech.Car && !sc.dataTech.Tech) {
				sc.dataTech.Car = true;
			}
			load(sc.dataTech.Car,sc.dataTech.Tech);
		}
	
	sc.changeIcePer = function(item)  {
		item.Icebox = null
	}
		sc.changeIce = function(item)  {
		item.Icebox_per = null;
	}
	
	sc.delRow = function(item)  {
    modals.showWindow(modalType.WIN_DIALOG, {
                    title: "Удаление", message: "Удалить выбранную запись?", callback: function () {
                      item.Summer_track = null;
					  item.Winter_track = null;
					  item.Summer_city = null;
					  item.Winter_city = null;
					  item.Summer_field = null;
					  item.Winter_field = null;
					  item.Coef = null;
					  item.Body_lifting = null;
					  item.Engine_heating = null;
					  item.Heating = null;
					  item.Icebox = null;	
                      item.Icebox_per = null;		
					  item.Sink = null;
                      item.Refill = null;
                      item.Trailer = null;  
                      requests.serverCall.post($http, "RateFuel/DeleteRate", item.Id, function (data, status, headers, config) {
                        });
						;}
                    })
	};

	sc.save = function()  {
		sc.formData[0].Car = sc.dataTech.Car;
		sc.formData[0].Tech = sc.dataTech.Tech;
		requests.serverCall.post($http, "RateFuel/UpdateRate", sc.formData, function (data, status, headers, config) {
		load(sc.dataTech.Car,sc.dataTech.Tech);
		});
	};

	sc.cancel = function() {
	load(sc.dataTech.Car,sc.dataTech.Tech);
	};
}]);