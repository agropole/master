/**
 *  @ngdoc controller
 *  @name app.controller:LeftPanelCtrl
 *  @description
 *  # LeftPanelCtrl
 *  Это контроллер, определеяющий поведение левой панели в формах План-факторный анализ, Культура, Карта
 *  @requires $scope
 *  @requires $location
 *  @requires $cookieStore
 *  @requires LogusConfig

 *  @property {Object} sc scope
 *  @property {Object} sc.cultId id культуры (если есть)
 *  @property {Object} sc.searchData id объекта на карте 
 
 
 *  @property {function} sc.clickRole выбор роли
 *  @property {function} sc.fileInputChanged событие выбора файла
 *  @property {function} prepareFile прикрепление файла
 *  @property {function} sendToServer отправка файла
 */
var app = angular.module("LeftPanel", ["services"]);
app.controller("LeftPanelCtrl", ["$scope", "$cookieStore", "$http", "requests", "$stateParams", function ($scope, $cookieStore, $http, requests, $stateParams) {
    var sc = $scope;
    sc.cultId = $stateParams.culture_id ? $stateParams.culture_id : 1;
    sc.searchData = "";
    sc.infoData = {};
	sc.tableData = {};
	sc.formData = [];
    var years = [];
	var firstLoad = false;
    var curyear = new Date().getFullYear();
    for (var i=curyear-3, li=curyear+3; i<=li; i++){
        years.push({
            Id: i,
            Name: i.toString()
        });
    }
    sc.infoData.YearList = years;
    sc.Year = curyear;
      $("#text").hide(); 
	  sc.formData.title = false;
	  sc.formData.crop = false;
     requests.serverCall.post($http, "Dashboard/GetStructureCrop", sc.Year , function (data, status, headers, config) {
		 console.log(data);
		sc.tableData = data;	
        });
      sc.enterTitle = function() {
	   if (sc.formData.title) {
		   $("#text").show(); 
	   } else {
		   $("#text").hide(); 
	   }
   }

   
   
     //PRINT_MAP
	sc.printMap = function() {
	console.log('PRINT_MAP');
		 var  print = 1 ;
		//рендер таблицы
	html2canvas(document.querySelector("#widget")).then(canvas2 => {
		console.log(canvas2.width);
		console.log(canvas2.height);
	    var dims = {
        a0: [1189, 841],
        a1: [841, 594],
        a2: [594, 420],
        a3: [420, 297],
        a4: [297, 210],
        a5: [210, 148]
      };
	    var loading = 0;
      var loaded = 0;
	//  var format = document.getElementById('format').value;
	var format = "a4";
    //     var resolution = document.getElementById('resolution').value;
	var resolution = 300;
	
	
	
        var dim = dims[format];
        var width = Math.round(dim[0] * resolution / 25.4);
        var height = Math.round(dim[1] * resolution / 25.4);
        var size = (map.getSize());
        var extent = map.getView().calculateExtent(size);

        var source = raster.getSource();

        var tileLoadStart = function() {
          ++loading;
        };

        var timer;
        var keys = [];

        function tileLoadEndFactory(canvas) {
          return function () {
            ++loaded;
            if (timer) {
              clearTimeout(timer);
              timer = null;
            }

            if (loading === loaded && print != 0) {
              timer = window.setTimeout(function () {
                loading = 0;
                loaded = 0;
				print = 0;
			   console.log(canvas.width);
		       console.log(canvas.height);
				//заголовок 
				if (sc.formData.title) {
				var ctx = canvas.getContext("2d");
                ctx.font = "80px Arial";
            //    ctx.fillText("Карта-схема севооборота на "+sc.Year+" г.",60,80);
			   console.log(sc.formData.titleText.length);
		         ctx.fillText(sc.formData.titleText,(canvas.width/2)-(sc.formData.titleText.length*10),80);
				}
				
				//схема
				var data2 = canvas2.toDataURL('image/jpeg');
				//console.log(data2);
				
				//
                var data = canvas.toDataURL('image/jpeg');
				
                var pdf = new jsPDF('landscape', undefined, format);
               pdf.addImage(data, 'JPEG', 0, 0, dim[0], dim[1]);
		      if (sc.formData.crop) {
				pdf.addImage(data2, 'PNG', dim[0]-canvas2.width/3.8, dim[1]-canvas2.height/3.8, canvas2.width/3.8, canvas2.height/3.8);
			  }
				//сброс настроек
	              sc.formData.title = false;
	              sc.formData.crop = false;
	              sc.formData.titleText = null;
				   $("#text").hide(); 
	              $('.printButton').removeClass('printButtonSelected');
                  $('.panelPrint').removeClass('openPanel');
	              //
                pdf.save('map.pdf');
          //      keys.forEach(unByKey);
                keys = [];
                map.setSize(size);
                map.getView().fit(extent, {size: size});
                map.renderSync();
                document.body.style.cursor = 'auto';
              }, 500);
            }
          };
        }

		
        map.once('postcompose', function(event) {
          var canvas = event.context.canvas;
          var tileLoadEnd = tileLoadEndFactory(canvas);
          keys = [
            source.on('tileloadstart', tileLoadStart),
            source.on('tileloadend', tileLoadEnd),
            source.on('tileloaderror', tileLoadEnd)
          ];
          tileLoadEnd();
        });
      
		
        var printSize = [width, height];
        map.setSize(printSize);
        map.getView().fit(extent, {size: printSize});
        loaded = -1;
        map.renderSync();
		
		
		
		
		});
	}
	
	
	
	
	
	
	
	
    sc.refreshYear = function() {
		requests.serverCall.post($http, "Dashboard/GetStructureCrop", sc.Year , function (data, status, headers, config) {
		console.log(data);
		sc.tableData = data;	
        });
        if (sc.cultureShow) {
            $scope.$emit('myCustomEvent', {plant_id: sc.cultId, year: sc.Year});
        } else {
            sc.fieldOn ? addLayer("field", sc.Year) : null;
            sc.techOn ? addLayer("tech", sc.Year, true) : null;
			sc.schemMap ? addLayer("satel", sc.Year) : null;
        }
    };

    var curUser = $cookieStore.get('currentUser');
    if (curUser.currentRole === 112) {
        sc.cultureShow = true;
        sc.mapShow = false;
    } else if (curUser.currentRole === 113) {
        sc.cultureShow = false;
        sc.mapShow = true;

        sc.fieldOn = true;
        sc.techOn = true;
		sc.layersOnOff = function(layer) {
            if (layer === 'field') {
                sc.fieldOn ? addLayer(layer) : removeLayer(layer);
            } else if (layer === 'tech'){
                sc.techOn ? addLayer(layer) : removeLayer(layer);
            }else if (layer === 'line') {
                sc.fieldOn ? addLayer(layer) : removeLayer(layer);
            } else if (layer === 'area'){
               sc.techOn ? addLayer(layer) : removeLayer(layer);
            }
        };
		
		sc.DrawOnOff = function(layer) {
            if (layer === 'line') {
                sc.fieldOn ? addLayer(layer) : removeLayer(layer);
            } else if (layer === 'area'){
                sc.techOn ? addLayer(layer) : removeLayer(layer);
            }
        };
		
	    sc.schemMap = false;
        sc.satelMap = true;
		sc.MapChange = function(layer) {
            if (layer === 'schem') {
				
                sc.schemMap ? addLayer(layer) : removeLayer(layer);
				sc.satelMap = false;
				
            } else if (layer === 'satel'){
				
                sc.satelMap ? addLayer(layer) : removeLayer(layer);
				sc.schemMap = false;
            }
        };
		
		sc.FactField = false;
		sc.FactFieldChange = function(layer){
			if (layer === 'fact'){
				sc.FactField ? addLayer(layer) : removeLayer(layer);
			}
		};

        sc.mapsearch = function() {
            console.log(sc.searchData);
            mapsearch(sc.searchData);
        };

        sc.searchDrop = [];
    }

    requests.serverCall.get($http, "Dashboard/GetPlants",function(data) {
        sc.plantDrop = data;
    }, function() {});

    sc.plantChange = function(data) {
        if (sc.cultureShow) {
            $scope.$emit('myCustomEvent', {plant_id: sc.cultId, year: sc.Year});
        }
    };
    /*-------------------------------------- end отказы - приятние счетов -------------------------------------*/
    /*------------------------------------- пока так левая панель выезжает ------------------------------------*/
    $('.plantButton').click(function (e) {
        if ($('.panelPlant').hasClass('openPanel')) {
            $('.plantButton').removeClass('plantButtonSelected');
            $('.panelPlant').removeClass('openPanel');
        } else {
            $('.plantButton').addClass('plantButtonSelected');
            setTimeout(function () {
                sc.$apply(function () {
                    $('.panelPlant').addClass('openPanel');
                })
            }, 1);
        }
    });
     //слои
    $('.layersButton').click(function (e) {
		console.log('ffffffffffff');
        if ($('.panelLayers').hasClass('openPanel')) {
            $('.layersButton').removeClass('layersButtonSelected');
            $('.panelLayers').removeClass('openPanel');
			 $('.printButton').removeClass('printButtonSelected');
        } else {
			$('.printButton').removeClass('printButtonSelected');
			 $('.panelPrint').removeClass('openPanel');
            if ($('.panelSearch').hasClass('openPanel')) {
                $('.searchButton').removeClass('searchButtonSelected');
                $('.panelSearch').removeClass('openPanel');
            }else if ($('.AreaButton').hasClass('AreaButtonSelected')) {
            $('.AreaButton').removeClass('AreaButtonSelected');
            sc.layersOnOff('area');			
			} else if ($('.LineButton').hasClass('LineButtonSelected')) {
            $('.LineButton').removeClass('LineButtonSelected');
			sc.layersOnOff('line');            
			}
            setTimeout(function () {
                sc.$apply(function () {
                    $('.layersButton').addClass('layersButtonSelected');
                    $('.panelLayers').addClass('openPanel');
                })
            }, 1);
        }
    });
      // ПЕЧАТЬ
	     $('.printButton').click(function (e) {
        if ($('.panelPrint').hasClass('openPanel')) {
            $('.printButton').removeClass('printButtonSelected');
            $('.panelPrint').removeClass('openPanel');
        } else {
			 $('.panelLayers').removeClass('openPanel');
			 $('.layersButton').removeClass('layersButtonSelected');
            if ($('.panelSearch').hasClass('openPanel')) {
                $('.searchButton').removeClass('searchButtonSelected');
                $('.panelSearch').removeClass('openPanel');
            }else if ($('.AreaButton').hasClass('AreaButtonSelected')) {
            $('.AreaButton').removeClass('AreaButtonSelected');
            sc.layersOnOff('area');			
			} else if ($('.LineButton').hasClass('LineButtonSelected')) {
            $('.LineButton').removeClass('LineButtonSelected');
			sc.layersOnOff('line');            
			}
            setTimeout(function () {
                sc.$apply(function () {
                    $('.printButton').addClass('printButtonSelected');
                    $('.panelPrint').addClass('openPanel');
                })
            }, 1);
        }
    });
	  //
	  
	  
    $('.searchButton').click(function (e) {
        if ($('.panelSearch').hasClass('openPanel')) {
            $('.searchButton').removeClass('searchButtonSelected');
            $('.panelSearch').removeClass('openPanel');
            sc.searchData = [];
        } else {
			$('.printButton').removeClass('printButtonSelected');
			 $('.panelPrint').removeClass('openPanel');
            if ($('.panelLayers').hasClass('openPanel')) {
                $('.layersButton').removeClass('layersButtonSelected');
                $('.panelLayers').removeClass('openPanel');
            }else if ($('.AreaButton').hasClass('AreaButtonSelected')) {
            $('.AreaButton').removeClass('AreaButtonSelected');
            sc.layersOnOff('area');			
			} else if ($('.LineButton').hasClass('LineButtonSelected')) {
            $('.LineButton').removeClass('LineButtonSelected');
			sc.layersOnOff('line');            
			}
            createSearchList();
            $('.searchButton').addClass('searchButtonSelected');
            setTimeout(function () {
                sc.$apply(function () {
                    $('.panelSearch').addClass('openPanel');
                })
            }, 1);
        }
    });
     $('.LineButton').click(function (e) {
        if ($('.LineButton').hasClass('LineButtonSelected')) {
            $('.LineButton').removeClass('LineButtonSelected');
			sc.layersOnOff('line');
            
        } else {
			$('.printButton').removeClass('printButtonSelected');
			 $('.panelPrint').removeClass('openPanel');
            if ($('.AreaButton').hasClass('AreaButtonSelected')) {
            $('.AreaButton').removeClass('AreaButtonSelected'); }
			else if ($('.panelLayers').hasClass('openPanel')) {
                $('.layersButton').removeClass('layersButtonSelected');
                $('.panelLayers').removeClass('openPanel');
            }else if ($('.panelSearch').hasClass('openPanel')) {
                $('.searchButton').removeClass('searchButtonSelected');
                $('.panelSearch').removeClass('openPanel');
            }
			sc.layersOnOff('line');
            $('.LineButton').addClass('LineButtonSelected');
      	}
	 });
	$('.AreaButton').click(function (e) {
        if ($('.AreaButton').hasClass('AreaButtonSelected')) {
            $('.AreaButton').removeClass('AreaButtonSelected');
            sc.layersOnOff('area');
			
        } else {
			$('.printButton').removeClass('printButtonSelected');
			 $('.panelPrint').removeClass('openPanel');
            if ($('.LineButton').hasClass('LineButtonSelected')) {
            $('.LineButton').removeClass('LineButtonSelected'); }
			else if ($('.panelLayers').hasClass('openPanel')) {
                $('.layersButton').removeClass('layersButtonSelected');
                $('.panelLayers').removeClass('openPanel');
            }else if ($('.panelSearch').hasClass('openPanel')) {
                $('.searchButton').removeClass('searchButtonSelected');
                $('.panelSearch').removeClass('openPanel');
            }
			sc.DrawOnOff('area');
			$('.AreaButton').addClass('AreaButtonSelected');
      	}
	 });
    var createSearchList = function() {
        sc.searchDrop = getFeatures();
    };

    $('body').click(function (e) {
        if (!$(e.target).closest('.panelSearch').length && !$(e.target).closest('.tools').length) {
            $('.panelSearch').removeClass('openSearchPanel');
        }
    });
	/*--------------------- end пока так левая панель выезжает -----------------------------*/
	
	
}]);