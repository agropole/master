module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    ngdocs: {
            all: ['app.js', 'modules/*.js', 'modules/**/*.js']
        },

    
  });

  
  grunt.loadNpmTasks('grunt-ngdocs');
  grunt.registerTask('default', ['ngdocs']);
};