var raster = new ol.layer.Tile({
    source: new ol.source.OSM({
    })
});
var fieldsLayer;
//sc.$watch
sc.refreshLayer = function(data){
    if(fieldsLayer){
        map.removeLayer(fieldsLayer);
    }
    var fieldSource = new ol.source.Vector({
        url: LogusConfig.serverUrl+'WaysListsCtrl/GetCloseInfoForMapByField?field_id='+sc.formData.FieldId,
        format: new ol.format.GeoJSON()
    });
    fieldSource.addEventListener('addfeature', function(e) {
        var fs = fieldSource.getFeatures();
        var f = fs[fs.length - 1];
        if (f.getGeometry().getType() === 'Polygon'){
            var geom = f.getGeometry().getInteriorPoint().getCoordinates();
            console.log(JSON.stringify(geom));
            map.getView().setCenter(geom);
        }
    });
    fieldsLayer = new ol.layer.Vector({
        source: fieldSource,
        style : createFieldStyle(),
    });
    map.addLayer(fieldsLayer);
}

var map = new ol.Map({
    layers: [raster],
    target: 'map',
    view: new ol.View({
        center: ol.proj.transform([39.603334,51.761755], 'EPSG:4326', 'EPSG:3857'),
        zoom: 15
    })
});

function getRGB(c, alpha) {
    var color = String(c);
    var HexToR = parseInt(color.substring(2, 4), 16);
    var HexToG = parseInt(color.substring(4, 6), 16);
    var HexToB = parseInt(color.substring(6, 8), 16);
    var rgbaColor = "rgba(" + HexToR + ", " + HexToG + ", " + HexToB + "," + alpha + ")";
    return rgbaColor;
};

function createFieldStyle() {
    return function(feature, resolution) {
        var styleArray = [];
        var prop = feature.getProperties();
        feature.selectable = false;
        var fillStyle = new ol.style.Style({
            fill : new ol.style.Fill({
                color : getRGB(prop.color, 0.2),
            }),
            stroke : new ol.style.Stroke({
                color : getRGB(prop.color, 1),
                width : "2"
            }),
        });
        styleArray.push(fillStyle);
        if (prop.name) {
            var text1 = new ol.style.Style({
                text : createFieldTextStyle(prop.name, resolution, 'top')
            });
            styleArray.push(text1);
        }
        if (prop.name) {
            var text2 = new ol.style.Style({
                text : createFieldTextStyle(prop.plant, resolution, 'bottom', 1)
            });
            styleArray.push(text2);
        }
        return styleArray;
    };
}

function createFieldTextStyle(t, resolution, pos) {
    var textStyle = new ol.style.Text({
        textAlign : 'center',
        textBaseline : 'alphabetic',
        font : (pos === 'top') ? 'normal 18px DINPro' : 'normal 14px DINPro',
        text : t,
        fill : new ol.style.Fill({
     //      color : "#000"
		 color: "#cea5a6"
        }),
        stroke: new ol.style.Stroke({color: '#fff', width: 5}),
        offsetY : (pos === 'top') ? -15 : 0,
        offsetX : 0,
    });
    return textStyle;
};

var featureOverlay = new ol.FeatureOverlay({
    style: function(feature, resolution) {
        var styleArray = [];
        var prop = feature.getProperties();
        feature.selectable = false;
        var fillStyle = new ol.style.Style({
            fill : new ol.style.Fill({
                color: 'rgba(255, 255, 255, 0.2)'
            }),
            stroke: new ol.style.Stroke({
                color: '#f00',
                width: 2
            }),
            text : createTextStyle(feature, resolution, 'center')
        });
        styleArray.push(fillStyle);
        return styleArray;
    }
});
featureOverlay.setMap(map);


function createTextStyle(feature, resolution, pos) {
    if (feature.getGeometry().getType() === "Polygon") {
        var geom = feature.getGeometry();
        var area = formatArea((geom));
        var t = area + ' га';;
        var textStyle = new ol.style.Text({
            textAlign : 'center',
            textBaseline : 'alphabetic',
            font : 'bold 24px DINPro-Bold',
            text : t,
            fill : new ol.style.Fill({
         //       color : "#780407"
		  color : "#cea5a6"
            }),
            offsetY : 10,
            offsetX : 0,
        });
        getArea();
        return textStyle;
    } else {
        return null;
    }
};

var wgs84Sphere = new ol.Sphere(6378137);
var formatArea = function(polygon) {
    var area;
    var sourceProj = map.getView().getProjection();
    var geom = (polygon.clone().transform(sourceProj, 'EPSG:4326'));
    var coordinates = geom.getLinearRing(0).getCoordinates();
    area = Math.abs(wgs84Sphere.geodesicArea(coordinates));
    area = area / 10000 * 10;
    area = Math.round(area) / 10;
    return area;
};

var modify = new ol.interaction.Modify({
    features: featureOverlay.getFeatures(),
    // the SHIFT key must be pressed to delete vertices, so
    // that new vertices can be drawn at the same position
    // of existing vertices
    deleteCondition: function(event) {
        return ol.events.condition.shiftKeyOnly(event) &&
            ol.events.condition.singleClick(event);
    }
});
map.addInteraction(modify);

var draw; // global so we can remove it later
function addInteraction() {
    draw = new ol.interaction.Draw({
        features: featureOverlay.getFeatures(),
        type: ("Polygon")
    });
    map.addInteraction(draw);
}
addInteraction();

sc.clearMeasures = function() {
    console.log('click');
    var fs = featureOverlay.getFeatures();
    while (fs.getLength() > 0) {
        fs.removeAt(0);
    }
    sc.formData.Area = 0;
}

var oldArea;
function getArea() {
    var area = 0;
    var fs = featureOverlay.getFeatures();
    fs.forEach(function (f, ind, array) {
        area += formatArea(f.getGeometry());
    });
    area = Math.round(area*10) / 10;
    if (area !== oldArea) {
        console.log('Общая площадь ' + fs.getLength() + " участков = " + area + " га");
        oldArea = area;
        console.info(area);
        sc.formData.Area = area;
        sc.$apply();
    }
}

