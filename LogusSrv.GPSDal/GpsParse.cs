﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Auth;
using Microsoft.WindowsAzure.Storage.Table;
using System.Configuration;
using LogusSrv.DAL.Operations;
using System.Timers;

namespace LogusSrv.GPSDal
{
    public class GpsParse
    {
        private static Timer aTimer;
        private Boolean StartTimer = false;
        private int timeTimer = 600000;
        public Boolean canBegin = true;
        protected readonly GpsFromXmlDb _gpsParse = new GpsFromXmlDb();
        private void OnTimedEvent(Object source, ElapsedEventArgs e)
        {
            if (canBegin)
            {
                canBegin = false;

                var connectionString = ConfigurationManager.ConnectionStrings["StorageConnectionString"].ConnectionString;
                var storageAccount = CloudStorageAccount.Parse(connectionString);

                var gpsTableClient = storageAccount.CreateCloudTableClient();

                CloudTable table = gpsTableClient.GetTableReference("GpsData");

                //дев версия 
                TableQuery<GpsDataEntity> query = new TableQuery<GpsDataEntity>().Where(TableQuery.GenerateFilterCondition("StatusDev", QueryComparisons.Equal, "1"));
                // пром версия 
                //  TableQuery<GpsDataEntity> query = new TableQuery<GpsDataEntity>().Where(TableQuery.GenerateFilterCondition("StatusProm", QueryComparisons.Equal, "1"));
                var resultList = table.ExecuteQuery(query).ToList();
                var count = resultList.Count();
                foreach (GpsDataEntity entity in resultList)
                {
                    try
                    {
                        _gpsParse.ParseXML(entity.RawData);
                        entity.StatusDev = "2";
                        //    entity.StatusProm = "2";
                    }
                    catch (Exception ex)
                    {
                        entity.Error = ex.Message;
                        entity.StatusDev = "3";
                        //    entity.StatusProm = "3";
                    }

                    TableOperation updateOperation = TableOperation.Replace(entity);
                    table.Execute(updateOperation);

                }
                canBegin = true;
            }
          

          //  return "ok";
     
        }

        public string chnageStatusXML()
        {
            var connectionString = ConfigurationManager.ConnectionStrings["StorageConnectionString"].ConnectionString;
            var storageAccount = CloudStorageAccount.Parse(connectionString);

            var gpsTableClient = storageAccount.CreateCloudTableClient();

            CloudTable table = gpsTableClient.GetTableReference("GpsData");

            TableQuery<GpsDataEntity> query = new TableQuery<GpsDataEntity>();
            var resultList = table.ExecuteQuery(query).ToList();
            var count = resultList.Count();
            foreach (GpsDataEntity entity in resultList)
            {
                entity.StatusDev = "1";
                TableOperation updateOperation = TableOperation.Replace(entity);
                table.Execute(updateOperation);
            }

            return "change";

        }
    

        public string StartGPSTimer()
        {
            if (!StartTimer)
            {
                StartTimer = true;
                aTimer = new System.Timers.Timer(timeTimer);
                // Hook up the Elapsed event for the timer. 
                aTimer.Elapsed += OnTimedEvent;
                aTimer.Enabled = true;
            }
            return "start gps timer";
        }

        public string StopGPSTimer()
        {
            aTimer.Enabled = false;
            StartTimer = false;
            return "stop gps timer";
        } 

        public int setIntervalTimer( int newInt){
            timeTimer = newInt;
            StopGPSTimer();
            StartGPSTimer();
            return newInt;
        }

        public class GpsDataEntity : TableEntity
        {
            public string RawData { get; set; }
            public string DT { get; set; }

            public string StatusDev { get; set; }

            public string StatusProm { get; set; }

            public string Error { get; set; }

            public GpsDataEntity() { }
            public GpsDataEntity(string pKey)
            {
                PartitionKey = pKey;
                RowKey = Guid.NewGuid().ToString().Replace("-", "");
            }


        }
    }
}
