CREATE TABLE Dictionary (id int PRIMARY KEY, name VARCHAR(50), table_name VARCHAR(50));
CREATE TABLE Dictionary_column_type (id INT PRIMARY KEY,name VARCHAR(50), cellTemplate VARCHAR);
CREATE TABLE Dictionary_column (id INT PRIMARY KEY, id_dictionary INT, id_dictionary_column_type INT, field VARCHAR(50), displayName VARCHAR(50), width VARCHAR(4));

ALTER TABLE Dictionary_column_type ALTER COLUMN cellTemplate VARCHAR(1000)

ALTER TABLE [dbo].Dictionary_column  WITH CHECK ADD  CONSTRAINT [FK_Dictionary_column_to_Dictionary] FOREIGN KEY(id_dictionary)
REFERENCES [dbo].Dictionary (id);
ALTER TABLE [dbo].Dictionary_column  WITH CHECK ADD  CONSTRAINT [FK_Dictionary_column_to_Dictionary_column_type] FOREIGN KEY(id_dictionary_column_type)
REFERENCES [dbo].Dictionary_column_type (id);

UPDATE [Type_salary_doc] set [short_name] = '�' where [tpsal_id]=5;
UPDATE [Type_salary_doc] set [short_name] = '��' where [tpsal_id]=4;

INSERT INTO Dictionary VALUES (1, '������� ���������','Type_salary_doc');
INSERT INTO Dictionary_column_type VALUES (1, '������� �����', '<div class="ngCellText" ng-class="col.colIndex()"><input value="{{row.entity[col.field]}}" ng-model="row.entity[col.field]" UPDATE Dictionary SET ng-change="update(row.entity)"></div>');
INSERT INTO Dictionary_column_type VALUES (2, 'ID', '');
INSERT INTO Dictionary_column VALUES (1, 1, 2, 'tpsal_id', '', '0%');
INSERT INTO Dictionary_column VALUES (2, 1, 1, 'name', '��������', '50%');
INSERT INTO Dictionary_column VALUES (3, 1, 1, 'short_name', '����������', '50%');

ALTER TABLE Dictionary ADD [request] varchar(150);
UPDATE Dictionary SET [request]='DictonaryCtrl/GetType_salary_doc';
ALTER TABLE Dictionary ADD [request_update] varchar(150);
UPDATE Dictionary SET [request_update]='DictonaryCtrl/Updateype_salary_doc';

update [Dictionary_column] set [width]='45%' where id=3;


CREATE TABLE Contractor (id int IDENTITY(1,1) PRIMARY KEY, name VARCHAR(50), code VARCHAR(50));
INSERT INTO Dictionary VALUES (2, '�����������','Contractor', 'DictionaryCtrl/GetContractor', 'DictionaryCtrl/UpdateContractor');
INSERT INTO Dictionary_column VALUES (4, 2, 2, 'id', '', '0%');
INSERT INTO Dictionary_column VALUES (5, 2, 1, 'name', '��������', '50%');
INSERT INTO Dictionary_column VALUES (6, 2, 1, 'code', '���', '45%');

  CREATE TABLE VAT (id int IDENTITY(1,1) PRIMARY KEY, vat_value numeric(4,2));
INSERT INTO Dictionary VALUES (3, '���', 'VAT', 'DictionaryCtrl/GetVAT', 'DictionaryCtrl/UpdateVAT');
INSERT INTO Dictionary_column VALUES (7, 3, 2, 'id', '', '0%');
INSERT INTO Dictionary_column VALUES (8, 3, 1, 'vat_value', '������, %', '50%');

ALTER TABLE Dictionary ADD [index_column] varchar(50);
UPDATE Dictionary SET [index_column] = 'id';
UPDATE Dictionary SET [index_column] = 'tpsal_id' where id=1;

ALTER TABLE [Acts_szr_income] ADD [id_contractor] int;
ALTER TABLE [Acts_szr_income] ADD [id_vat] int;
ALTER TABLE [dbo].[Acts_szr_income]  WITH CHECK ADD  CONSTRAINT [FK_Acts_szr_income_to_Contractor] FOREIGN KEY(id_contractor)
REFERENCES [dbo].Contractor (id);

ALTER TABLE [dbo].[Acts_szr_income]  WITH CHECK ADD  CONSTRAINT [FK_Acts_szr_income_to_VAT] FOREIGN KEY(id_vat)
REFERENCES [dbo].VAT (id);

  ALTER TABLE [Acts_szr_income] ADD [id_tpsal] int;
ALTER TABLE [dbo].[Acts_szr_income]  WITH CHECK ADD  CONSTRAINT [FK_Acts_szr_income_to_Type_salary_doc] FOREIGN KEY(id_tpsal)
REFERENCES [dbo].Type_salary_doc (tpsal_id);

ALTER TABLE [Materials_to_ftp] ADD consumption_rate numeric(10,2);
ALTER TABLE [Materials_to_ftp] ADD price numeric(10,2);


create table TC_operation_to_Tech (id int not null identity(1,1), it_tc_operation int not null, equi_equi_id int, trakt_trakt_id int);
  ALTER TABLE [dbo].TC_operation_to_Tech  WITH CHECK ADD  CONSTRAINT [FK_TC_operation_to_Tech_to_Equipments] FOREIGN KEY(equi_equi_id)
REFERENCES [dbo].Equipments (equi_id);

ALTER TABLE [dbo].TC_operation_to_Tech  WITH CHECK ADD  CONSTRAINT [FK_TC_operation_to_Tech_to_Traktors] FOREIGN KEY(trakt_trakt_id)
REFERENCES [dbo].Traktors (trakt_id);
ALTER TABLE [dbo].TC_operation_to_Tech  WITH CHECK ADD  CONSTRAINT [FK_TC_operation_to_Tech_to_TC_operation] FOREIGN KEY(it_tc_operation)
REFERENCES [dbo].TC_operation (id);

DROP TABLE Type_task_to_Equipment;
DROP TABLE [Type_task_to_Traktor];
DROP TABLE [TypeTasks_to_Plants];

ALTER TABLE [Type_fuel] DROP COLUMN [cost];
ALTER TABLE [Type_fuel] ADD [balance] real;
ALTER TABLE [Type_fuel] ADD [current_price] real;

ALTER TABLE [Type_fuel] ADD tpsal_tpsal_id int;
  ALTER TABLE [dbo].[Type_fuel]  WITH CHECK ADD  CONSTRAINT [FK_Type_fuel_to_Type_salary_doc] FOREIGN KEY(tpsal_tpsal_id)
REFERENCES [dbo].Type_salary_doc (tpsal_id);

ALTER TABLE [Dictionary] ADD [request_detail] varchar(50);
INSERT INTO [Dictionary] VALUES (4, '���� �������', 'Type_fuel', 'DictionaryCtrl/GetType_fuel', 'DictionaryCtrl/UpdateType_fuel', 'id', 'DictionaryCtrl/DetailType_fuel');


insert into [Dictionary_column_type] values (3, '�����', '<div class="ngCellText" ng-class="col.colIndex()" ng-click="editCell(row.entity, row.getProperty(col.field), col.field)"><input value="{{row.entity[col.field].Name}}" placeholder="�������" style="border: none;" data-ng-click="updateSelectedCell(col.field, row.entity[col.field].Id, ''�����'', row.entity, $event)" readonly></div>');

INSERT INTO [Dictionary_column] VALUES (9, 4, 2, 'id', '', '0%');
  INSERT INTO [Dictionary_column] VALUES (10, 4, 1, 'name', '��������', '50%');
  INSERT INTO [Dictionary_column] VALUES (11, 4, 1, 'balance', '������� ������', '20%');
  INSERT INTO [Dictionary_column] VALUES (12, 4, 1, 'current_price', '������� ����', '20%');
  INSERT INTO [Dictionary_column] VALUES (13, 4, 3, 'tpsal_tpsal_id', '������� ���������', '10%');

update [Dictionary_column_type] set cellTemplate='<div class="ngCellText" ng-class="col.colIndex()" ng-click="editCell(row.entity, row.getProperty(col.field), col.field)"><input value="{{row.entity[col.field].Name}}" placeholder="�������" style="border: none;" data-ng-click="updateSelectedCell(col.field, row.entity[col.field].Id, ''�����'', row, $event)" readonly></div>' where id=3;

CREATE TABLE [dbo].[Acts_fuel_income](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[id_type_fuel] [int] NOT NULL,
	[count] [real] NOT NULL,
	[price] [real] NOT NULL,
	[cost] [real] NOT NULL,
	[comment] [varchar](50) NULL,
	[date] [datetime] NULL,
	[id_contractor] [int] NULL,
	[id_vat] [int] NULL,
	[id_tpsal] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
);
ALTER TABLE [dbo].[Acts_fuel_income]  WITH CHECK ADD  CONSTRAINT [FK_Acts_fuel_income_to_Type_fuel] FOREIGN KEY([id_type_fuel])
REFERENCES [dbo].Type_fuel (id);
ALTER TABLE [dbo].[Acts_fuel_income]  WITH CHECK ADD  CONSTRAINT [FK_Acts_fuel_income_to_Contractor] FOREIGN KEY([id_contractor])
REFERENCES [dbo].[Contractor] ([id]);
ALTER TABLE [dbo].[Acts_fuel_income]  WITH CHECK ADD  CONSTRAINT [FK_Acts_fuel_income_to_Type_salary_doc] FOREIGN KEY([id_tpsal])
REFERENCES [dbo].[Type_salary_doc] ([tpsal_id]);
ALTER TABLE [dbo].[Acts_fuel_income]  WITH CHECK ADD  CONSTRAINT [FK_Acts_fuel_income_to_VAT] FOREIGN KEY([id_vat])
REFERENCES [dbo].[VAT] ([id]);

ALTER TABLE [Tasks] ADD [fuel_cost] numeric(12,2);

  ALTER TABLE [Sheet_tracks] ADD [id_type_fuel] int;
  ALTER TABLE [Sheet_tracks] ADD [fuel_price] numeric (10,2);

   ALTER TABLE [dbo].[Sheet_tracks]  WITH CHECK ADD  CONSTRAINT [FK_Sheet_tracks_to_Type_fuel] FOREIGN KEY(id_type_fuel)
REFERENCES [dbo].Type_fuel (id);
ALTER TABLE [Type_fuel] ALTER COLUMN [name] varchar(50);