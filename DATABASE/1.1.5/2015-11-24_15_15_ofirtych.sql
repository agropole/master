


  CREATE TABLE Act_sale
(id int NOT NULL IDENTITY(1,1)  PRIMARY KEY,
plant_plant_id int not null,
sort_sort_id int,
count REAL,
price REAL,
cost REAL,
date DATETIME NOT NULL);


ALTER TABLE [dbo].Act_sale  WITH CHECK ADD  CONSTRAINT [FK_Act_sale_to_Plants] FOREIGN KEY(plant_plant_id)
REFERENCES [dbo].Plants (plant_id);

ALTER TABLE [dbo].Act_sale  WITH CHECK ADD  CONSTRAINT [FK_Act_sale_to_Type_sorts_plants] FOREIGN KEY(sort_sort_id)
REFERENCES [dbo].[Type_sorts_plants] ([tpsort_id]);