
insert into Roles (role_id, role_name) values (10,'�����-������������ ����');
insert into [Users] ([emp_emp_id], [login], [password], [role_role_id]) values (34, 'superuser', 'kWoknv1+Q1cqEyoISmzj/iVkchq1elhTENvjgRSxDNydu2Art27OPO/HqYTORSacEEPp1cELZo/+ccrmDInOcg==', 10);

UPDATE [Plants] set [icon_path]='/assets/img/icons/sad.png' where [plant_id]=31;
  UPDATE [Plants] set [icon_path]='/assets/img/icons/sad.png' where [plant_id]=32;
  UPDATE [Plants] set [icon_path]='/assets/img/icons/sad.png' where [plant_id]=33;
  UPDATE [Plants] set [icon_path]='/assets/img/icons/mnogoletnie_trav.png' where [plant_id]=34;
  UPDATE [Plants] set [icon_path]='/assets/img/icons/mnogoletnie_trav.png' where [plant_id]=35;
  UPDATE [Plants] set [icon_path]='/assets/img/icons/oputy.png' where [plant_id]=36;

  

CREATE TABLE [dbo].[Acts_szr_income](
	[id] [int] NOT NULL IDENTITY (1,1),
	[mat_id] [int] NOT NULL,
	[type_mat_id] [int] NOT NULL,
	[count] [real] NOT NULL,
	[price] [real] NOT NULL,
	[cost] [real] NOT NULL,
	[comment] [varchar](50) NULL
);

ALTER TABLE [dbo].[Acts_szr_income]  WITH CHECK ADD  CONSTRAINT [FK_Acts_szr_income_ref_Materials] FOREIGN KEY([mat_id])
REFERENCES [dbo].Materials (mat_id);

 ALTER TABLE [dbo].[Acts_szr_income]  WITH CHECK ADD  CONSTRAINT [FK_Acts_szr_income_ref_Type_materials] FOREIGN KEY([type_mat_id])
REFERENCES [dbo].Type_materials ([type_id]);

ALTER TABLE [Acts_szr_income]
  ADD date datetime;

  ALTER TABLE [Acts_szr_income]
ADD PRIMARY KEY ([id]);
//----------------------------------------------------------------------------------------------------------------------------------------------
  ALTER TABLE [Materials] 
  ADD [balance] real;
  ALTER TABLE [Materials] 
  ADD [current_price] real;
UPDATE [Materials] SET [balance] = 0;
  UPDATE [Materials] SET [current_price] = 0;


CREATE TABLE [dbo].[Type_fuel](
	[id] [int] NOT NULL,
	[name] [nchar](10) NOT NULL,
	[cost] [numeric](6, 2) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
);

ALTER TABLE [dbo].[Type_tasks]  WITH CHECK ADD  CONSTRAINT [FK_Type_tasks_to_Type_fuel] FOREIGN KEY([type_fuel_id])
REFERENCES [dbo].[Type_fuel] ([id]);

ALTER TABLE [Type_tasks] ADD [shift_output] numeric(9,3);
update [Type_tasks] SET [shift_output] = 100 where [tptas_id] = 38;

INSERT INTO [TC_param] VALUES (43, '�������� (��������������� ��������)', 6);
INSERT INTO [TC_param] VALUES (44, '����������� ���� ���������', 6);
INSERT INTO [TC_param] VALUES (45, '���� ������ ������', null);
INSERT INTO [TC_param] VALUES (46, '���� ��������� ������', null);
INSERT INTO [TC_param] VALUES (47, '������ ���� �� 1 ��', NULL);
INSERT INTO [TC_param] VALUES (48, '������ ����������', NULL);
ALTER TABLE [TC] ADD [year] int;
ALTER TABLE [TC_operation] ALTER  COLUMN [multiplicity] numeric(5,2);
ALTER TABLE [dbo].[TC_param_value]  WITH CHECK ADD  CONSTRAINT [FK_TC_param_value_to_Material] FOREIGN KEY([mat_mat_id])
REFERENCES [dbo].Materials (mat_id);
UPDATE [Type_tasks] SET [shift_output] = 30 WHERE [tptas_id] = 133;

ALTER TABLE [TC_operation] ADD [factor] numeric(5,2);
ALTER TABLE Tasks ALTER COLUMN [task_num] int;

ALTER TABLE [Type_fuel] ADD code varchar(250);
UPDATE Type_tasks set type_fuel_id = null ;
  ALTER TABLE [dbo].Type_tasks  WITH CHECK ADD  CONSTRAINT [FK_Type_tasks_to_Type_fuel] FOREIGN KEY(type_fuel_id)
REFERENCES [dbo].Type_fuel (id);

ALTER TABLE [Type_fuel] ALTER COLUMN name varchar(250);