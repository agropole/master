INSERT INTO [Dictionary] VALUES (7, 'Культуры', 'Plants', 'DictionaryCtrl/GetPlants', 'DictionaryCtrl/UpdatePlants', 'plant_id', 'DictionaryCtrl/DetailPlants');
INSERT INTO [Dictionary] VALUES (8, 'Поля', 'Fields', 'DictionaryCtrl/GetFields', 'DictionaryCtrl/UpdateFields', 'fiel_id', 'DictionaryCtrl/DetailFields');
INSERT INTO [Dictionary_column_type] VALUES (4, 'Полигон', 'templates/coord_template.html');

  INSERT INTO [Dictionary_column] VALUES(25, 7, 2, 'plant_id', '', '0%');
  INSERT INTO [Dictionary_column] VALUES(26, 7, 1, 'name', 'Название', '25%');
  INSERT INTO [Dictionary_column] VALUES(27, 7, 1, 'short_name', 'Сокр. название', '20%');
  INSERT INTO [Dictionary_column] VALUES(28, 7, 1, 'description', 'Описание', '30%');
  INSERT INTO [Dictionary_column] VALUES(29, 7, 3, 'tpplant_tpplant_id', 'Тип культуры', '25%');

  INSERT INTO [Dictionary_column] VALUES(30, 8, 2, 'fiel_id', '', '0%');
  INSERT INTO [Dictionary_column] VALUES(31, 8, 1, 'name', 'Название', '25%');
  INSERT INTO [Dictionary_column] VALUES(32, 8, 3, 'org_org_id', 'Организация', '25%');
  INSERT INTO [Dictionary_column] VALUES(33, 8, 1, 'type', 'Тип', '10%');
  INSERT INTO [Dictionary_column] VALUES(34, 8, 1, 'code', 'Код интеграции', '25%');
  INSERT INTO [Dictionary_column] VALUES(35, 8, 4, 'coord', 'Площадь/координаты', '15%');
  UPDATE [Dictionary_column] SET width='5%' WHERE id=33;
  UPDATE [Dictionary_column] SET width='20%' WHERE id=35;
  ALTER TABLE fields DROP COLUMN layer_id;
  ALTER TABLE fields DROP COLUMN plant_id;
  ALTER TABLE fields DROP COLUMN zoom;

  ALTER TABLE plants DROP COLUMN color;
  ALTER TABLE plants DROP COLUMN icon_path;


INSERT INTO Type_sorts_plants VALUES (81,20, 'Касабланка');
INSERT INTO Type_sorts_plants VALUES (82,20, 'Марис Бард');
INSERT INTO Type_sorts_plants VALUES (83,20, 'Джели');
INSERT INTO Type_sorts_plants VALUES (84,20, 'Джувел');
INSERT INTO Type_sorts_plants VALUES (85,20, 'Романце');

INSERT INTO Type_sorts_plants VALUES (86,5, 'Универсо');
INSERT INTO Type_sorts_plants VALUES (87,5, 'Пандеро');

INSERT INTO Type_sorts_plants VALUES (88,8, 'Адаптор');
INSERT INTO Type_sorts_plants VALUES (89,8, 'Тайфун');

INSERT INTO Type_sorts_plants VALUES (90,22, 'Пабло');
INSERT INTO Type_sorts_plants VALUES (91,22, 'Боро');

INSERT INTO Type_sorts_plants VALUES (92,18, 'Александрит');

Update Dictionary set request='DictionaryCtrl/GetType_salary_doc' where id=1;
Update Dictionary set request_update='DictionaryCtrl/Updateype_salary_doc' where id=1;