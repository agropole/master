create table [GPS_track_archive] (
	[id] [int] IDENTITY(1,1) NOT NULL,
	[coord] [geography] NOT NULL,
	[date_start] [datetime] NOT NULL,
	[date_end] [datetime] NOT NULL,
	[id_trakt] [int] NOT NULL,
	[id_task] [int] NOT NULL,
	[length] [numeric] NOT NULL
)

alter table [GPS_track_archive] WITH CHECK ADD  CONSTRAINT [FK_GPS_track_archive_to_Traktors] FOREIGN KEY([id_trakt])
references [Traktors] ([trakt_id])

alter table [GPS_track_archive] WITH CHECK ADD  CONSTRAINT [FK_GPS_track_archive_to_Tasks] FOREIGN KEY([id_task])
references [Tasks] ([tas_id])