  INSERT INTO [dbo].[Type_sorts_plants] VALUES (93, 9, 'Земляника (закладка)');
  INSERT INTO [dbo].[Type_sorts_plants] VALUES (94, 17, 'Яблочный сад (закладка)');

  UPDATE Fields SET area=13.6 where fiel_id=46

  INSERT INTO [dbo].[Type_sorts_plants] VALUES (95, 3, 'Фломенко');
  INSERT INTO [dbo].[Type_sorts_plants] VALUES (96, 3, 'Ниагара');

  INSERT INTO Type_tasks VALUES ('новое', 'Сбор ягод', 'Сбор ягод вручную', null, null, null, null, null, null, null);

    INSERT INTO Materials VALUES (277, 'Многолетние травы', 'Семена', 4, 0, null, null, 0, 0);

	  
INSERT INTO Type_tasks VALUES (215, 'Установка опорных кольев с подвязкой', 'Растениеводство', null, null, null, null, null, null, null);
INSERT INTO Type_tasks VALUES (216, 'Гербицидная обработка приствольных полос', 'Растениеводство', null, null, null, null, null, null, null);
INSERT INTO Type_tasks VALUES (217, 'Опрыскивание сада', 'Растениеводство', null, null, null, null, null, null, null);
INSERT INTO Type_tasks VALUES (218, 'Зеленые операции с удалением дикой поросли', 'Растениеводство', null, null, null, null, null, null, null);
INSERT INTO Type_tasks VALUES (219, 'Ремонт насаждений', 'Растениеводство', null, null, null, null, null, null, null);
INSERT INTO Type_tasks VALUES (220, 'Раскладка затравки', 'Растениеводство', null, null, null, null, null, null, null);
INSERT INTO Type_tasks VALUES (221, 'Погрузка, подвоз, разгрузка саженцев', 'Растениеводство', null, null, null, null, null, null, null);
INSERT INTO Type_tasks VALUES (222, 'Подготовка лунок к поливу', 'Растениеводство', null, null, null, null, null, null, null);
INSERT INTO Type_tasks VALUES (223, 'Подвоз воды, полив', 'Растениеводство', null, null, null, null, null, null, null);
INSERT INTO Type_tasks VALUES (224, 'Оправка яблонь', 'Растениеводство', null, null, null, null, null, null, null);
INSERT INTO Type_tasks VALUES (225, 'После посадочная обрезка', 'Растениеводство', null, null, null, null, null, null, null);
INSERT INTO Type_tasks VALUES (226, 'Мульчирование 2-х кратное', 'Растениеводство', null, null, null, null, null, null, null);
INSERT INTO Type_tasks VALUES (227, 'Нарезка гребней укладка пленки, капли', 'Растениеводство', null, null, null, null, null, null, null);
INSERT INTO Type_tasks VALUES (228, 'Подвоз рассады в ящиках', 'Растениеводство', null, null, null, null, null, null, null);
INSERT INTO Type_tasks VALUES (229, 'Посадка рассады', 'Растениеводство', null, null, null, null, null, null, null);
INSERT INTO Type_tasks VALUES (230, 'Сбор тары после высадки рассады', 'Растениеводство', null, null, null, null, null, null, null);
INSERT INTO Type_tasks VALUES (231, 'Оправка растений в лунках', 'Растениеводство', null, null, null, null, null, null, null);
INSERT INTO Type_tasks VALUES (232, 'Удаление сорной растительности в лунках', 'Растениеводство', null, null, null, null, null, null, null);
INSERT INTO Type_tasks VALUES (233, 'Внесение гербицида в междурядиях', 'Растениеводство', null, null, null, null, null, null, null);
INSERT INTO Type_tasks VALUES (234, 'Погрузка тары', 'Растениеводство', null, null, null, null, null, null, null);
INSERT INTO Type_tasks VALUES (235, 'Транспортировка тары', 'Растениеводство', null, null, null, null, null, null, null);
INSERT INTO Type_tasks VALUES (236, 'Транспортировка весов', 'Растениеводство', null, null, null, null, null, null, null);
INSERT INTO Type_tasks VALUES (237, 'Сбор земляники', 'Растениеводство', null, null, null, null, null, null, null);
INSERT INTO Type_tasks VALUES (238, 'Удаление усов', 'Растениеводство', null, null, null, null, null, null, null);
INSERT INTO Type_tasks VALUES (239, 'Ремонт насаждений', 'Растениеводство', null, null, null, null, null, null, null);
INSERT INTO Type_tasks VALUES (240, 'Подвоз соломы', 'Растениеводство', null, null, null, null, null, null, null);
INSERT INTO Type_tasks VALUES (241, 'Укладка соломы в междурядия', 'Растениеводство', null, null, null, null, null, null, null);
INSERT INTO Type_tasks VALUES (242, 'Подвоз грунта для баластов', 'Растениеводство', null, null, null, null, null, null, null);
INSERT INTO Type_tasks VALUES (243, 'Погрузка укрывного материала', 'Растениеводство', null, null, null, null, null, null, null);
INSERT INTO Type_tasks VALUES (244, 'Развоз укрывного материала по квартолам', 'Растениеводство', null, null, null, null, null, null, null);
INSERT INTO Type_tasks VALUES (245, 'Укрытие земляники', 'Растениеводство', null, null, null, null, null, null, null);
INSERT INTO Type_tasks VALUES (246, 'Снятие укрывного материала, вывоз на склад', 'Растениеводство', null, null, null, null, null, null, null);
INSERT INTO Type_tasks VALUES (247, 'Удаление сухих листьев', 'Растениеводство', null, null, null, null, null, null, null);
INSERT INTO Type_tasks VALUES (248, 'Погрузка, подвоз, монтаж орошения', 'Растениеводство', null, null, null, null, null, null, null);
INSERT INTO Type_tasks VALUES (249, 'Монтаж опорной конструкции', 'Растениеводство', null, null, null, null, null, null, null);
INSERT INTO Type_tasks VALUES (250, 'Сбор ягоды малины', 'Растениеводство', null, null, null, null, null, null, null);
INSERT INTO Type_tasks VALUES (251, 'Формировка деревьев', 'Растениеводство', null, null, null, null, null, null, null);

INSERT INTO Materials VALUES (278, 'Купроксат', 'СЗР', 1, 0, null,null, 0,0);
INSERT INTO Materials VALUES (279, 'Хорус', 'СЗР', 1, 0, null,null, 0,0);
INSERT INTO Materials VALUES (280, 'Медный купорос', 'СЗР', 1, 0, null,null, 0,0);
INSERT INTO Materials VALUES (281, 'Строби', 'СЗР', 1, 0, null,null, 0,0);
INSERT INTO Materials VALUES (282, 'Тиовит-джет', 'СЗР', 1, 0, null,null, 0,0);
INSERT INTO Materials VALUES (283, 'Топсин М', 'СЗР', 1, 0, null,null, 0,0);
INSERT INTO Materials VALUES (284, 'Свитч', 'СЗР', 1, 0, null,null, 0,0);
INSERT INTO Materials VALUES (285, 'Фюзилат форте', 'СЗР', 1, 0, null,null, 0,0);
INSERT INTO Materials VALUES (286, 'Пиктор', 'СЗР', 1, 0, null,null, 0,0);
INSERT INTO Materials VALUES (287, 'Гардо Голд', 'СЗР', 1, 0, null,null, 0,0);

INSERT INTO Materials VALUES (288, 'Мочивина', 'Мин. удобрения', 2, 0, null,null, 0,0);
INSERT INTO Materials VALUES (289, 'Новалон 19.19.19', 'Мин. удобрения', 2, 0, null,null, 0,0);
INSERT INTO Materials VALUES (290, 'Нитрат кальция', 'Мин. удобрения', 2, 0, null,null, 0,0);
INSERT INTO Materials VALUES (291, 'Сульфат калия', 'Мин. удобрения', 2, 0, null,null, 0,0);
INSERT INTO Materials VALUES (292, 'Брексил железа', 'Мин. удобрения', 2, 0, null,null, 0,0);
INSERT INTO Materials VALUES (293, 'Новалон А', 'Мин. удобрения', 2, 0, null,null, 0,0);
INSERT INTO Materials VALUES (294, 'Новалон Р', 'Мин. удобрения', 2, 0, null,null, 0,0);
INSERT INTO Materials VALUES (295, 'Новалон К', 'Мин. удобрения', 2, 0, null,null, 0,0);
INSERT INTO Materials VALUES (296, 'Нитрат калия', 'Мин. удобрения', 2, 0, null,null, 0,0);
INSERT INTO Materials VALUES (297, 'Монофосфат калия', 'Мин. удобрения', 2, 0, null,null, 0,0);
INSERT INTO Materials VALUES (298, 'Рексолин АБС', 'Мин. удобрения', 2, 0, null,null, 0,0);
INSERT INTO Materials VALUES (299, 'Брексил марганца', 'Мин. удобрения', 2, 0, null,null, 0,0);
INSERT INTO Materials VALUES (300, 'Брексил цинка', 'Мин. удобрения', 2, 0, null,null, 0,0);
INSERT INTO Materials VALUES (301, 'Шмели', 'Мин. удобрения', 2, 0, null,null, 0,0);

INSERT INTO Equipments VALUES (1, null, 'Погрузчик-очиститель корнеплодов ', '', '', 1, null, '', null, null);