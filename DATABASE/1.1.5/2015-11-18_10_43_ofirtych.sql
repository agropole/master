CREATE TABLE Gross_harvest
(id int NOT NULL IDENTITY(1,1)  PRIMARY KEY,
ftp_ftp_id INT NOT NULL,
count REAL,
price REAL,
cost REAL,
date DATETIME NOT NULL);

ALTER TABLE [dbo].Gross_harvest  WITH CHECK ADD  CONSTRAINT [FK_Gross_harvest_to_Ftp] FOREIGN KEY(ftp_ftp_id)
REFERENCES [dbo].Fields_to_plants (fielplan_id);


CREATE TABLE Product
(id int NOT NULL IDENTITY(1,1)  PRIMARY KEY,
plant_plant_id INT NOT NULL,
sort_sort_id INT,
balance REAL,
current_price REAL);

ALTER TABLE [dbo].Product  WITH CHECK ADD  CONSTRAINT [FK_Product_to_Plants] FOREIGN KEY(plant_plant_id)
REFERENCES [dbo].Plants (plant_id);

ALTER TABLE [dbo].Product  WITH CHECK ADD  CONSTRAINT [FK_Product_to_Type_sorts_plants] FOREIGN KEY(sort_sort_id)
REFERENCES [dbo].Type_sorts_plants (tpsort_id);

update Fields_to_plants set in_code='559cac55-1feb-11e5-80c6-002590ebf8cd' where fielplan_id=57;
update Fields_to_plants set in_code='2f5102e5-2626-11e5-80c8-002590ebf8cd' where in_code='7c548508-c118-11e4-80c1-002590ebf8cd';
update Fields_to_plants set in_code='6ec5ceff-fa10-11e4-80c6-002590ebf8cd' where plant_plant_id=10;
insert into Fields_to_plants values (23, 3, '2015-01-01', '2015-01-01', 0, '511d6b59-43a4-11e4-926e-005056c00008', 1);
insert into Fields_to_plants values (23, 4, '2015-01-01', '2015-01-01', 0, '6ef885b5-6053-11e5-80ca-002590ebf8cd', 1);
update Fields_to_plants set in_code='6ef885b5-6053-11e5-80ca-002590ebf8cd' where fielplan_id=21;
update Fields_to_plants set in_code='6ef885b5-6053-11e5-80ca-002590ebf8cd' where fielplan_id=22;
update Fields_to_plants set in_code='6ef885b5-6053-11e5-80ca-002590ebf8cd' where fielplan_id=23;
update Fields_to_plants set in_code='d3aa0ba9-a038-11e3-9f68-005056c00008' where in_code='a2cea305-ed4e-11e3-952d-005056c00008';
update Fields_to_plants set in_code='5975541c-f2ea-11e4-80c6-002590ebf8cd' where in_code='2f5102e5-2626-11e5-80c8-002590ebf8cd';
update Fields_to_plants set in_code='7c548508-c118-11e4-80c1-002590ebf8cd' where in_code='5975541c-f2ea-11e4-80c6-002590ebf8cd';