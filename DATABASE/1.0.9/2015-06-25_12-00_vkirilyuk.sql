

alter table Sheet_tracks alter column left_fuel numeric(10,2)
GO
alter table Sheet_tracks alter column remain_fuel numeric(10,2)
GO
alter table Sheet_tracks alter column issued_fuel numeric(10,2)
GO
alter table Sheet_tracks alter column mileage numeric(10,2)
GO

alter table Refuelings alter column volume numeric(10,2)
GO


alter table Tasks alter column consumption numeric(10,2)
GO
alter table Tasks alter column consumption_rate numeric(10,2)
GO
alter table Tasks alter column consumption_fact numeric(10,2)
GO
alter table Tasks alter column volume_fact numeric(10,2)
GO

alter table Tasks alter column worktime numeric(10,2)
GO
alter table Tasks alter column area numeric(10,2)
GO
alter table Tasks alter column mileage numeric(10,2)
GO
alter table Tasks alter column workdays numeric(10,2)
GO
alter table Tasks alter column price_days numeric(10,2)
GO
alter table Tasks alter column price_add numeric(10,2)
GO


alter table Sheet_tracks add  motostart numeric(10,2)
GO
alter table Sheet_tracks add  motoend numeric(10,2)
GO

update Sheet_tracks set motostart = moto_start
GO
update Sheet_tracks set motoend = moto_end
GO

alter table Sheet_tracks drop column  moto_start
GO
alter table Sheet_tracks drop column  moto_end
GO

EXEC sp_RENAME 'Sheet_tracks.motostart' , 'moto_start', 'COLUMN'
GO
EXEC sp_RENAME 'Sheet_tracks.motoend' , 'moto_end', 'COLUMN'
GO


