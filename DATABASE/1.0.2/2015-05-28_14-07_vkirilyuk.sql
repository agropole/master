Alter table dbo.Salaries drop constraint fk_Salaries_ref_Ogranization;

EXEC sp_RENAME 'Salaries.doc_type' , 'tpsal_tpsal_id', 'COLUMN';

create table Type_salary_doc(
 tpsal_id int not null,
 name varchar(50) NULL,
 primary key(tpsal_id)
)

alter table dbo.Salaries add constraint fk_salaries_ref_Type_salary_doc
foreign key (tpsal_tpsal_id)
references dbo.Type_salary_doc(tpsal_id)
on update no action
on delete no action
go