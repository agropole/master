
Alter table dbo.Salaries drop constraint fk_salaries_ref_Employess;
Alter table dbo.Salary_details drop constraint fk_Salary_details_ref_Employees



create table Salaries_employees(
 salemp_id int not null,
 first_name varchar(50) NULL,
 second_name varchar(50) NULL,
 middle_name varchar(50) NULL,
 short_name varchar(50) NULL,
 in_code varchar(100),
 primary key(salemp_id)
)



EXEC sp_RENAME 'Salaries.otv_emp_id' , 'salemp_salemp_id', 'COLUMN';
EXEC sp_RENAME 'Salary_details.emp_emp_id' , 'salemp_salemp_id', 'COLUMN';

alter table dbo.Salaries add constraint fk_salaries_ref_Salaries_employess
foreign key (salemp_salemp_id)
references dbo.Salaries_employees(salemp_id)
on update no action
on delete no action
go

alter table dbo.Salary_details add constraint fk_Salary_details_ref_Salaries_employess
foreign key (salemp_salemp_id)
references dbo.Salaries_employees(salemp_id)
on update no action
on delete no action
go



USE [AgroDB]
GO

/****** Object:  Table [dbo].[Xml_archive]    Script Date: 28.05.2015 10:46:25 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Xml_archive](
	[file_id] [uniqueidentifier] NOT NULL,
	[date_load] [datetime] NULL,
	[content] [text] NULL,
	[status] [int] NULL,
	[status_dev] [int] NULL,
	[error_text] [text] NULL,
PRIMARY KEY CLUSTERED 
(
	[file_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
