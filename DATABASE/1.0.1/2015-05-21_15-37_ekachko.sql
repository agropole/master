




Alter table dbo.Prices drop constraint Prices_Traktors

Alter table dbo.Prices drop column trakt_trakt_id

Alter table dbo.Prices drop constraint Prices_Equipments

Alter table dbo.Prices drop column equi_equi_id

delete from Prices where pris_id>1

create table dbo.Model_Traktors(
id_mod_trak int not null,
code varchar(100),
name varchar(100) not null,
primary key(id_mod_trak)
)


create table dbo.Model_Equipments(
id_mod_equi int not null,
code varchar(100),
name varchar(100) not null,
primary key(id_mod_equi)
)

alter table dbo.Traktors add modtr_modtr_id int

alter table dbo.Traktors add constraint fk_traktors_ref_model_traktors
foreign key (modtr_modtr_id)
references dbo.Model_Traktors(id_mod_trak)
on update no action
on delete no action
go

alter table dbo.Equipments add modequi_modequi_id int

alter table dbo.Equipments add constraint fk_equipments_ref_model_equipments
foreign key (modequi_modequi_id)
references dbo.Model_Equipments(id_mod_equi)
on update no action
on delete no action
go


alter table dbo.Prices add modtr_modtr_id int

alter table dbo.Prices add constraint fk_prices_ref_model_traktors
foreign key (modtr_modtr_id)
references dbo.Model_Traktors(id_mod_trak)
on update no action
on delete no action
go

alter table dbo.Prices add modequi_modequi_id int

alter table dbo.Prices add constraint fk_prices_ref_model_equipments
foreign key (modequi_modequi_id)
references dbo.Model_Equipments(id_mod_equi)
on update no action
on delete no action
go