ALTER TABLE Prices ADD mu varchar(50);


ALTER TABLE Prices  ADD ind_min numeric(4,0);
ALTER TABLE Prices ADD ind_max numeric(4,0);
ALTER TABLE Prices ADD ind_ind_id int;
ALTER TABLE Prices ADD consumption_rate numeric(5,2);
ALTER TABLE Prices ADD plant_plant_id int;


 ALTER TABLE dbo.Prices
ADD CONSTRAINT fk_prices_ref_indicators FOREIGN KEY (ind_ind_id)
  REFERENCES dbo.Indicators (ind_id)
  ON UPDATE NO ACTION
  ON DELETE NO ACTION
GO

ALTER TABLE dbo.Prices
ALTER COLUMN ind_ind_id int NOT NULL
GO

ALTER TABLE dbo.Prices
ADD CONSTRAINT fk_prices_ref_plants FOREIGN KEY (plant_plant_id)
  REFERENCES dbo.Plants (plant_id)
  ON UPDATE NO ACTION
  ON DELETE NO ACTION
GO

ALTER TABLE dbo.Prices
ALTER COLUMN  plant_plant_id int NOT NULL
GO




insert into Indicators(code,name,description,type)
values ('001','Глубина вспашки, см','для вспашки','1')

insert into Indicators(code,name,description,type)
values ('002','Глубина обработки, см','','1')

insert into Indicators(code,name,description,type)
values ('003','Норма внесения, кг/га','для удобрений','1')

insert into Indicators(code,name,description,type)
values ('004','Норма внесения раб жидкости, л/га','для опрыскивания','1')

insert into Indicators(code,name,description,type)
values ('005','Не задано','не задано','1')

insert into Indicators(code,name,description,type)
values ('006','Норма посева, кг/га','посев','1')

insert into Indicators(code,name,description,type)
values ('007','высота среза, см ','кошение','1')

insert into Indicators(code,name,description,type)
values ('008','Высота гребня, см ','для образования гребней','1')

insert into Indicators(code,name,description,type)
values ('009',' Норма полива, м3/га','для полива','1')