
alter table Sheet_tracks add date_begin_fact datetime
go

alter table Sheet_tracks add date_end_fact datetime
go

alter table Tasks add num_runs numeric(5, 0)
go

alter table Tasks add mileage_cargo numeric(10, 2)
go

alter table Tasks add mileage_no_cargo numeric(10, 2)
go

alter table Tasks add mileage_on_track numeric(10, 2)
go

alter table Tasks add mileage_on_ground numeric(10, 2)
go

alter table Tasks add num_lifts numeric(5, 0)
go
