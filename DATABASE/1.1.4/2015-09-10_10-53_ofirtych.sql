

/****** Object:  Table [dbo].[TC]    Script Date: 10.09.2015 9:19:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[TC](
	[id] [int] NOT NULL,
	[tpsort_tpsort_id] [int] NULL,
	[fiel_fiel_id] [int] NULL,
	[plant_plant_id] [int] NULL,
	[id_tc] [int] NULL,
 CONSTRAINT [PK_TC] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO

ALTER TABLE [dbo].[TC]  WITH CHECK ADD  CONSTRAINT [FK_TC_to_Fields] FOREIGN KEY([fiel_fiel_id])
REFERENCES [dbo].[Fields] ([fiel_id])
GO

ALTER TABLE [dbo].[TC] CHECK CONSTRAINT [FK_TC_to_Fields]
GO

ALTER TABLE [dbo].[TC]  WITH CHECK ADD  CONSTRAINT [FK_TC_to_Plants] FOREIGN KEY([plant_plant_id])
REFERENCES [dbo].[Plants] ([plant_id])
GO

ALTER TABLE [dbo].[TC] CHECK CONSTRAINT [FK_TC_to_Plants]
GO

ALTER TABLE [dbo].[TC]  WITH CHECK ADD  CONSTRAINT [FK_TC_to_TC] FOREIGN KEY([id_tc])
REFERENCES [dbo].[TC] ([id])
GO

ALTER TABLE [dbo].[TC] CHECK CONSTRAINT [FK_TC_to_TC]
GO

ALTER TABLE [dbo].[TC]  WITH CHECK ADD  CONSTRAINT [FK_TC_to_Type_sorts_plants] FOREIGN KEY([tpsort_tpsort_id])
REFERENCES [dbo].[Type_sorts_plants] ([tpsort_id])
GO

ALTER TABLE [dbo].[TC] CHECK CONSTRAINT [FK_TC_to_Type_sorts_plants]
GO






/****** Object:  Table [dbo].[TC_param]    Script Date: 10.09.2015 9:23:32 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[TC_param](
	[id] [int] NOT NULL,
	[name] [varchar](300) NOT NULL,
	[tpsal_tpsal_id] [int] NULL,
 CONSTRAINT [PK_TC_param] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[TC_param]  WITH CHECK ADD  CONSTRAINT [FK_TC_param_to_Type_salary_doc] FOREIGN KEY([tpsal_tpsal_id])
REFERENCES [dbo].[Type_salary_doc] ([tpsal_id])
GO

ALTER TABLE [dbo].[TC_param] CHECK CONSTRAINT [FK_TC_param_to_Type_salary_doc]
GO







/****** Object:  Table [dbo].[TC_to_Sheet_track]    Script Date: 10.09.2015 9:24:17 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[TC_to_Sheet_track](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[id_tc] [int] NOT NULL,
	[shtr_shtr_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO

ALTER TABLE [dbo].[TC_to_Sheet_track]  WITH CHECK ADD  CONSTRAINT [FK_TC_to_Sheet_track_to_Sheet_tracks] FOREIGN KEY([shtr_shtr_id])
REFERENCES [dbo].[Sheet_tracks] ([shtr_id])
GO

ALTER TABLE [dbo].[TC_to_Sheet_track] CHECK CONSTRAINT [FK_TC_to_Sheet_track_to_Sheet_tracks]
GO

ALTER TABLE [dbo].[TC_to_Sheet_track]  WITH CHECK ADD  CONSTRAINT [FK_TC_to_Sheet_track_to_TC] FOREIGN KEY([id_tc])
REFERENCES [dbo].[TC] ([id])
GO

ALTER TABLE [dbo].[TC_to_Sheet_track] CHECK CONSTRAINT [FK_TC_to_Sheet_track_to_TC]
GO





/****** Object:  Table [dbo].[TC_operation]    Script Date: 10.09.2015 9:25:39 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[TC_operation](
    [id] [int] NOT NULL,
	[id_tc] [int] NOT NULL,
	[tptas_tptas_id] [int] NOT NULL,
	[multiplicity] [int] NOT NULL,
	[date_start] [date] NULL,
	[date_finish] [date] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO

ALTER TABLE [dbo].[TC_operation]  WITH CHECK ADD  CONSTRAINT [FK_TC_operation_to_TC] FOREIGN KEY([id_tc])
REFERENCES [dbo].[TC] ([id])
GO

ALTER TABLE [dbo].[TC_operation] CHECK CONSTRAINT [FK_TC_operation_to_TC]
GO

ALTER TABLE [dbo].[TC_operation]  WITH CHECK ADD  CONSTRAINT [FK_TC_operation_to_Type_tasks] FOREIGN KEY([tptas_tptas_id])
REFERENCES [dbo].[Type_tasks] ([tptas_id])
GO

ALTER TABLE [dbo].[TC_operation] CHECK CONSTRAINT [FK_TC_operation_to_Type_tasks]
GO








/****** Object:  Table [dbo].[TC_param_value]    Script Date: 10.09.2015 9:24:52 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[TC_param_value](
	[id] [int] NOT NULL,
	[id_tc] [int] NOT NULL,
	[id_tc_param] [int] NOT NULL,
	[id_tc_operation] [int] NULL,
	[value] [real] NULL,
	[plan] [int] NOT NULL,
	[mat_mat_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO

ALTER TABLE [dbo].[TC_param_value] ADD  CONSTRAINT [DF_Table_1_plan]  DEFAULT ((0)) FOR [plan]
GO

ALTER TABLE [dbo].[TC_param_value]  WITH CHECK ADD  CONSTRAINT [FK_TC_param_value_to_TC] FOREIGN KEY([id_tc])
REFERENCES [dbo].[TC] ([id])
GO

ALTER TABLE [dbo].[TC_param_value] CHECK CONSTRAINT [FK_TC_param_value_to_TC]
GO

ALTER TABLE [dbo].[TC_param_value]  WITH CHECK ADD  CONSTRAINT [FK_TC_param_value_to_TC_operation] FOREIGN KEY([id_tc_operation])
REFERENCES [dbo].[TC_operation] ([id])
GO

ALTER TABLE [dbo].[TC_param_value] CHECK CONSTRAINT [FK_TC_param_value_to_TC_operation]
GO

ALTER TABLE [dbo].[TC_param_value]  WITH CHECK ADD  CONSTRAINT [FK_TC_param_value_to_TC_param] FOREIGN KEY([id_tc_param])
REFERENCES [dbo].[TC_param] ([id])
GO

ALTER TABLE [dbo].[TC_param_value] CHECK CONSTRAINT [FK_TC_param_value_to_TC_param]
GO









/****** Object:  Table [dbo].[Type_task_to_Equipment]    Script Date: 10.09.2015 9:27:27 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Type_task_to_Equipment](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[tptas_tptas_id] [int] NOT NULL,
	[equi_equi_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO

ALTER TABLE [dbo].[Type_task_to_Equipment]  WITH CHECK ADD  CONSTRAINT [FK_TC_operation_to_equipment_to_Equipments] FOREIGN KEY([equi_equi_id])
REFERENCES [dbo].[Equipments] ([equi_id])
GO

ALTER TABLE [dbo].[Type_task_to_Equipment] CHECK CONSTRAINT [FK_TC_operation_to_equipment_to_Equipments]
GO

ALTER TABLE [dbo].[Type_task_to_Equipment]  WITH CHECK ADD  CONSTRAINT [FK_TC_operation_to_equipment_to_Type_tasks] FOREIGN KEY([tptas_tptas_id])
REFERENCES [dbo].[Type_tasks] ([tptas_id])
GO

ALTER TABLE [dbo].[Type_task_to_Equipment] CHECK CONSTRAINT [FK_TC_operation_to_equipment_to_Type_tasks]
GO











/****** Object:  Table [dbo].[Type_task_to_Traktor]    Script Date: 10.09.2015 9:28:01 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Type_task_to_Traktor](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[tptas_tptas_id] [int] NOT NULL,
	[trakt_trakt_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO

ALTER TABLE [dbo].[Type_task_to_Traktor]  WITH CHECK ADD  CONSTRAINT [FK_TC_operation_to_traktor_to_Traktors] FOREIGN KEY([trakt_trakt_id])
REFERENCES [dbo].[Traktors] ([trakt_id])
GO

ALTER TABLE [dbo].[Type_task_to_Traktor] CHECK CONSTRAINT [FK_TC_operation_to_traktor_to_Traktors]
GO

ALTER TABLE [dbo].[Type_task_to_Traktor]  WITH CHECK ADD  CONSTRAINT [FK_TC_operation_to_Traktor_to_Type_tasks] FOREIGN KEY([tptas_tptas_id])
REFERENCES [dbo].[Type_tasks] ([tptas_id])
GO

ALTER TABLE [dbo].[Type_task_to_Traktor] CHECK CONSTRAINT [FK_TC_operation_to_Traktor_to_Type_tasks]
GO
