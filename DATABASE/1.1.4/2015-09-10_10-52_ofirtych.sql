  INSERT INTO [Type_salary_doc] VALUES (6, '�����', '���.');
  INSERT INTO [Type_salary_doc] VALUES (7, '�����', '�');
  INSERT INTO [Type_salary_doc] VALUES (8, '', '�/��');
  INSERT INTO [Type_salary_doc] VALUES (9, '����', '�');


insert into Type_tasks([code],[name],[description],[acvite],[in_code]) values ( '�����', '����� ����� �� ��������', '���������������', null, null);
insert into Type_tasks([code],[name],[description],[acvite],[in_code]) values ( '�����', '�������� ���.���������', '����������-������������ ������', null, null);

INSERT INTO [Type_materials] ([code],[name],[description]) VALUES (3,'������','�������� ��������');

ALTER TABLE dbo.[Materials] ADD tpsort_tpsort_id INT NULL;

ALTER TABLE [dbo].[Materials]  WITH CHECK ADD  CONSTRAINT [FK_Materials_to_Type_sorts_plants] FOREIGN KEY(tpsort_tpsort_id)
REFERENCES [dbo].[Type_sorts_plants] ([tpsort_id]);

ALTER TABLE [dbo].[Type_tasks] ADD [rate] NUMERIC(8,2) NULL;


  update [Type_tasks] set rate=1400 where [tptas_id] = 133;
  update [Type_tasks] set rate=1400 where [tptas_id] = 45;
  update [Type_tasks] set rate=1000 where [tptas_id] = 22;
  update [Type_tasks] set rate=1400 where [tptas_id] = 211
  update [Type_tasks] set rate=1400 where [tptas_id] = 22;
  update [Type_tasks] set rate=1400 where [tptas_id] = 158;
  update [Type_tasks] set rate=1900 where [tptas_id] = 161;
  update [Type_tasks] set rate=1400 where [tptas_id] = 206;
  update [Type_tasks] set rate=1400 where [tptas_id] = 212
  update [Type_tasks] set rate=1400 where [tptas_id] = 182;
  update [Type_tasks] set rate=1700 where [tptas_id] = 37;
  update [Type_tasks] set rate=2700 where [tptas_id] = 53;
  update [Type_tasks] set rate=3000 where [tptas_id] = 38;
  update [Type_tasks] set rate=3500 where [tptas_id] = 69;
  update [Type_tasks] set rate=1700 where [tptas_id] = 40;

  ALTER TABLE [Type_tasks] ADD auxiliary_rate NUMERIC(8,2) NULL
  ALTER TABLE [Type_tasks] ADD type_fuel_id INT NULL;
  ALTER TABLE [Type_tasks] ADD fuel_consumption NUMERIC(6,2) NULL;


  update [Type_tasks] set type_fuel_id=1,fuel_consumption=10 where [tptas_id] = 133;
  update [Type_tasks] set type_fuel_id=1,fuel_consumption=12 where [tptas_id] = 45;
  update [Type_tasks] set type_fuel_id=1,fuel_consumption=0.6 where [tptas_id] = 22;
  update [Type_tasks] set type_fuel_id=1,fuel_consumption=1 where [tptas_id] = 211
  update [Type_tasks] set type_fuel_id=2,fuel_consumption=0 where [tptas_id] = 138;
  update [Type_tasks] set type_fuel_id=2,fuel_consumption=0 where [tptas_id] = 22;
  update [Type_tasks] set type_fuel_id=1,fuel_consumption=3 where [tptas_id] = 158;
  update [Type_tasks] set type_fuel_id=1,fuel_consumption=3 where [tptas_id] = 161;
  update [Type_tasks] set type_fuel_id=1,fuel_consumption=1.6 where [tptas_id] = 206;
  update [Type_tasks] set type_fuel_id=1,fuel_consumption=0.5 where [tptas_id] = 212
  update [Type_tasks] set type_fuel_id=1,fuel_consumption=4 where [tptas_id] = 182;
  update [Type_tasks] set type_fuel_id=1,fuel_consumption=3 where [tptas_id] = 37;
  update [Type_tasks] set type_fuel_id=1,fuel_consumption=1 where [tptas_id] = 53;
  update [Type_tasks] set type_fuel_id=1,fuel_consumption=1 where [tptas_id] = 38;
  update [Type_tasks] set type_fuel_id=1,fuel_consumption=12 where [tptas_id] = 69;
  update [Type_tasks] set type_fuel_id=1,fuel_consumption=1 where [tptas_id] = 40;



  UPDATE [Materials] SET cost=1228.81 WHERE [mat_id] = 57;
  UPDATE [Materials] SET cost=813.56 WHERE [mat_id] = 100;
  UPDATE [Materials] SET cost=555.08 WHERE [mat_id] = 39;
  UPDATE [Materials] SET cost=87.50 WHERE [mat_id] = 7;
  UPDATE [Materials] SET cost=5139.83 WHERE [mat_id] = 58;
  UPDATE [Materials] SET cost=572.03 WHERE [mat_id] = 99;
  UPDATE [Materials] SET cost=177.97 WHERE [mat_id] = 128;
  UPDATE [Materials] SET cost=177.97 WHERE [mat_id] = 92;
  UPDATE [Materials] SET cost=402.54 WHERE [mat_id] = 80;
  UPDATE [Materials] SET cost=838.98 WHERE [mat_id] = 133;
  UPDATE [Materials] SET cost=614.41 WHERE [mat_id] = 17;
  UPDATE [Materials] SET cost=360.17 WHERE [mat_id] = 26;
  UPDATE [Materials] SET cost=453.39 WHERE [mat_id] = 134;
  UPDATE [Materials] SET cost=1500 WHERE [mat_id] = 124;
  UPDATE [Materials] SET cost=309.32 WHERE [mat_id] = 96;
  UPDATE [Materials] SET cost=190.68 WHERE [mat_id] = 52;
  UPDATE [Materials] SET cost=720.34 WHERE [mat_id] = 62;

CREATE TABLE Material_to_Plant
(
id int NOT NULL IDENTITY(1,1) PRIMARY KEY,
mat_mat_id INT NOT NULL,
plant_plant_id INT NOT NULL,
consumption NUMERIC(8,2) NOT NULL,
);

ALTER TABLE Material_to_Plant
ADD CONSTRAINT FK_Material_to_Plant_to_Materials FOREIGN KEY (mat_mat_id) 
REFERENCES Materials (mat_id);

ALTER TABLE Material_to_Plant
ADD CONSTRAINT FK_Material_to_Plant_to_Plants FOREIGN KEY (plant_plant_id) 
REFERENCES Plants (plant_id);

  INSERT INTO [Materials](mat_id,name,description,mat_type_id,code,tpsort_tpsort_id,cost) VALUES (152, '����������', '���', 1, null, null, 88.50);
  INSERT INTO [Materials](mat_id,name,description,mat_type_id,code,tpsort_tpsort_id,cost) VALUES (153, '������', '���', 1, null, null, 720.34);
  INSERT INTO [Materials](mat_id,name,description,mat_type_id,code,tpsort_tpsort_id,cost) VALUES (154, '��������� �������', '���. ���������', 2, null, null, 12245.76);
  INSERT INTO [Materials](mat_id,name,description,mat_type_id,code,tpsort_tpsort_id,cost) VALUES (155, '��������', '���. ���������', 2, null, null,  15678);
  INSERT INTO [Materials](mat_id,name,description,mat_type_id,code,tpsort_tpsort_id,cost) VALUES (156, '��������', '������', 4, null, 39,  7000);
  INSERT INTO [Materials](mat_id,name,description,mat_type_id,code,tpsort_tpsort_id,cost) VALUES (157, '��������', '������', 4, null, 40,  7000);
  INSERT INTO [Materials](mat_id,name,description,mat_type_id,code,tpsort_tpsort_id,cost) VALUES (158, '�������', '������', 4, null, 37,  7000);
  INSERT INTO [Materials](mat_id,name,description,mat_type_id,code,tpsort_tpsort_id,cost) VALUES (159, '����� ����', '������', 4, null, null,  18000);
  INSERT INTO [Materials](mat_id,name,description,mat_type_id,code,tpsort_tpsort_id,cost) VALUES (160, '�����', '������', 4, null, 36,  7000);



  insert into [Material_to_plant] (mat_mat_id, plant_plant_id, consumption) values (57, 1, 0.15);
  insert into [Material_to_plant] (mat_mat_id, plant_plant_id, consumption) values (100, 1, 0.03);
  insert into [Material_to_plant] (mat_mat_id, plant_plant_id, consumption) values (39, 1, 0.03);
  insert into [Material_to_plant] (mat_mat_id, plant_plant_id, consumption) values (7, 1, 0.19);
  insert into [Material_to_plant] (mat_mat_id, plant_plant_id, consumption) values (152, 1, 0.19);
  insert into [Material_to_plant] (mat_mat_id, plant_plant_id, consumption) values (57, 1, 0.05);
  insert into [Material_to_plant] (mat_mat_id, plant_plant_id, consumption) values (99, 1, 0.50);
  insert into [Material_to_plant] (mat_mat_id, plant_plant_id, consumption) values (128, 1, 0.15);
  insert into [Material_to_plant] (mat_mat_id, plant_plant_id, consumption) values (153, 1, 0.15);
  insert into [Material_to_plant] (mat_mat_id, plant_plant_id, consumption) values (92, 1, 1);
  insert into [Material_to_plant] (mat_mat_id, plant_plant_id, consumption) values (80, 1, 0.30);
  insert into [Material_to_plant] (mat_mat_id, plant_plant_id, consumption) values (133, 1, 0.60);
  insert into [Material_to_plant] (mat_mat_id, plant_plant_id, consumption) values (57, 2, 0.60);
  insert into [Material_to_plant] (mat_mat_id, plant_plant_id, consumption) values (100, 2, 0.10);
  insert into [Material_to_plant] (mat_mat_id, plant_plant_id, consumption) values (39, 2, 0.10);
  insert into [Material_to_plant] (mat_mat_id, plant_plant_id, consumption) values (7, 2, 0.75);
  insert into [Material_to_plant] (mat_mat_id, plant_plant_id, consumption) values (152, 2, 0.75);
  insert into [Material_to_plant] (mat_mat_id, plant_plant_id, consumption) values (17, 2, 0.50);
  insert into [Material_to_plant] (mat_mat_id, plant_plant_id, consumption) values (26, 2, 0.50);
  insert into [Material_to_plant] (mat_mat_id, plant_plant_id, consumption) values (134, 2, 0.15);
  insert into [Material_to_plant] (mat_mat_id, plant_plant_id, consumption) values (124, 2, 0.30);
  insert into [Material_to_plant] (mat_mat_id, plant_plant_id, consumption) values (96, 2, 80);
  insert into [Material_to_plant] (mat_mat_id, plant_plant_id, consumption) values (52, 2, 2);
  insert into [Material_to_plant] (mat_mat_id, plant_plant_id, consumption) values (62, 2, 0.20);
  INSERT INTO Material_to_Plant (mat_mat_id, plant_plant_id, consumption) VALUES (156,1,0.24);
  INSERT INTO Material_to_Plant (mat_mat_id, plant_plant_id, consumption) VALUES (157,1,0.24);
  INSERT INTO Material_to_Plant (mat_mat_id, plant_plant_id, consumption) VALUES (158,1,0.24);
  INSERT INTO Material_to_Plant (mat_mat_id, plant_plant_id, consumption) VALUES (159,1,0.24);
  INSERT INTO Material_to_Plant (mat_mat_id, plant_plant_id, consumption) VALUES (154,1,0.15);
  INSERT INTO Material_to_Plant (mat_mat_id, plant_plant_id, consumption) VALUES (154,1,0.20);
  INSERT INTO Material_to_Plant (mat_mat_id, plant_plant_id, consumption) VALUES (155,2,0.20);
  INSERT INTO Material_to_Plant (mat_mat_id, plant_plant_id, consumption) VALUES (160,2,0.25);

 

  INSERT INTO [Type_equipments] VALUES (16,'���������', '��������� ������������');
  INSERT INTO [Equipments] ([tpequi_tpequi_id], [name], [model],[description],[width],[shot_name]) VALUES (16, '���-80','���-80','���������',3,'���-80');

ALTER TABLE Tasks ADD id_tc INT NULL;

ALTER TABLE [dbo].Tasks  WITH CHECK ADD  CONSTRAINT [FK_Tasks_to_TC] FOREIGN KEY([id_tc])
REFERENCES [dbo].[TC] ([id]);