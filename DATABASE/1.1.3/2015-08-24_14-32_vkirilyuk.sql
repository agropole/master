
EXEC sp_RENAME 'Type_materials.material_id' , 'type_id', 'COLUMN'

GO


CREATE TABLE [dbo].[Materials](
	[mat_id] [int] NOT NULL,
	[name] [varchar](100) NOT NULL,
	[description] [varchar](200) NULL,
	[mat_type_id] [int] NULL,
 CONSTRAINT [PK_materials_id] PRIMARY KEY CLUSTERED 
(
	[mat_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)


ALTER TABLE [dbo].[Materials]  WITH CHECK ADD  CONSTRAINT [FK_Type_materials] FOREIGN KEY([mat_type_id])
REFERENCES [dbo].[Type_materials] ([type_id])
GO

ALTER TABLE [dbo].[Materials] CHECK CONSTRAINT [FK_Type_materials]
GO


CREATE TABLE [dbo].[Type_soil](
	[soil_id] [int] NOT NULL,
	[name] [varchar](100) NOT NULL,
	[description] [varchar](200) NULL,
 CONSTRAINT [PK_type_soil_id] PRIMARY KEY CLUSTERED 
(
	[soil_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)


CREATE TABLE [dbo].[Acts_szr](
	[szr_id] [int] NOT NULL  IDENTITY(1,1),
	[soil_soil_id] [int] NULL,
	[res_emp_id][int] NULL,
	[rec_emp_id][int] NULL,
	[org_org_id][int] NULL,
	[date] [date] NULL,
	[year] [numeric](4,0) NULL,
 CONSTRAINT [PK_Acts_szr_id] PRIMARY KEY CLUSTERED 
(
	[szr_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)


ALTER TABLE [dbo].[Acts_szr]  WITH CHECK ADD  CONSTRAINT [FK_Acts_szr_ref_Type_soil] FOREIGN KEY([soil_soil_id])
REFERENCES [dbo].[Type_soil] ([soil_id])
GO

ALTER TABLE [dbo].[Acts_szr] CHECK CONSTRAINT [FK_Acts_szr_ref_Type_soil]
GO

ALTER TABLE [dbo].[Acts_szr]  WITH CHECK ADD  CONSTRAINT [FK_Acts_szr_ref_res_Employees] FOREIGN KEY([res_emp_id])
REFERENCES [dbo].[Employees] ([emp_id])
GO

ALTER TABLE [dbo].[Acts_szr] CHECK CONSTRAINT [FK_Acts_szr_ref_res_Employees]
GO

ALTER TABLE [dbo].[Acts_szr]  WITH CHECK ADD  CONSTRAINT [FK_Acts_szr_ref_rec_Employees] FOREIGN KEY([rec_emp_id])
REFERENCES [dbo].[Employees] ([emp_id])
GO

ALTER TABLE [dbo].[Acts_szr] CHECK CONSTRAINT [FK_Acts_szr_ref_rec_Employees]
GO

ALTER TABLE [dbo].[Acts_szr]  WITH CHECK ADD  CONSTRAINT [FK_Acts_szr_ref_Organizations] FOREIGN KEY([org_org_id])
REFERENCES [dbo].[Organizations] ([org_id])
GO

ALTER TABLE [dbo].[Acts_szr] CHECK CONSTRAINT [FK_Acts_szr_ref_Organizations]
GO


insert into Roles(role_name) values('���� ���');

CREATE TABLE [dbo].[Materials_to_ftp](
	[id] [int] NOT NULL  IDENTITY(1,1),
	[mat_mat_id] [int] NOT NULL,
	[ftp_ftp_id] [int] NULL,
	[area] [numeric](10,2) NULL,
	[summ] [numeric](10,2) NULL,
	[count] [numeric](10,2) NULL,
 CONSTRAINT [PK_Materials_to_ftp_id] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

ALTER TABLE [dbo].[Materials_to_ftp]  WITH CHECK ADD  CONSTRAINT [FK_Materials_to_ftp_ref_materials_id] FOREIGN KEY([mat_mat_id])
REFERENCES [dbo].[Materials] ([mat_id])
GO

ALTER TABLE [dbo].[Materials_to_ftp] CHECK CONSTRAINT [FK_Materials_to_ftp_ref_materials_id]
GO

ALTER TABLE [dbo].[Materials_to_ftp]  WITH CHECK ADD  CONSTRAINT [FK_Materials_to_ftp_ref_Fields_to_plants_id] FOREIGN KEY([ftp_ftp_id])
REFERENCES [dbo].[Fields_to_plants] ([fielplan_id])
GO

ALTER TABLE [dbo].[Materials_to_ftp] CHECK CONSTRAINT [FK_Materials_to_ftp_ref_Fields_to_plants_id]
GO

alter table Acts_szr add number varchar(50)
GO
alter table Materials_to_ftp add szr_szr_id int NuLL
GO

ALTER TABLE [dbo].[Materials_to_ftp]  WITH CHECK ADD  CONSTRAINT [FK_Materials_to_ftp_ref_Acts_Szr_id] FOREIGN KEY([szr_szr_id])
REFERENCES [dbo].[Acts_szr] ([szr_id])
GO

ALTER TABLE [dbo].[Materials_to_ftp] CHECK CONSTRAINT [FK_Materials_to_ftp_ref_Acts_Szr_id]
GO


update Users set role_role_id = 8 where login = 'prixodchenko' 

alter table Materials_to_ftp add code varchar(50) NuLL
GO