USE [LogusDevDb]
GO

/****** Object:  Table [dbo].[Type_fuel]    Script Date: 03.06.2016 14:39:21 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[Type_fuel](
	[id] [int] NOT NULL,
	[name] [varchar](250) NULL,
	[balance] [real] NULL,
	[current_price] [real] NULL,
	[tpsal_tpsal_id] [int] NULL,
	[code] [varchar](250) NULL,
	[fuel_flag] [bit] NULL,
	[deleted] [bit] NOT NULL DEFAULT ((0)),
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[Type_fuel]  WITH CHECK ADD  CONSTRAINT [FK_Type_fuel_to_Type_salary_doc] FOREIGN KEY([tpsal_tpsal_id])
REFERENCES [dbo].[Type_salary_doc] ([tpsal_id])
GO

ALTER TABLE [dbo].[Type_fuel] CHECK CONSTRAINT [FK_Type_fuel_to_Type_salary_doc]
GO

s
