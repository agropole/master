alter table Acts_szr add outcome bit

alter table Acts_szr_income add id_act int

ALTER TABLE [dbo].Acts_szr_income  WITH CHECK ADD  CONSTRAINT [FK_Acts_szr_income_To_Acts_szr] FOREIGN KEY(id_act)
REFERENCES [dbo].Acts_szr (szr_id)

update Acts_szr set outcome=0

alter table Acts_szr alter column outcome bit not null

create table Type_type_task (id int not null primary key,
							name varchar (50))

insert into Type_type_task values (1, '???????????');
insert into Type_type_task values (2, '????????');
insert into Type_type_task values (3, '??????');

alter table Type_tasks add id_type int

ALTER TABLE [dbo].Type_tasks  WITH CHECK ADD  CONSTRAINT [FK_Type_tasks_To_Type_type_task] FOREIGN KEY(id_type)
REFERENCES [dbo].Type_type_task (id)

alter table Acts_szr add id_contractor int
ALTER TABLE [dbo].Acts_szr  WITH CHECK ADD  CONSTRAINT [FK_Acts_szr_To_Contractor] FOREIGN KEY(id_contractor)
REFERENCES [dbo].Contractor (id)