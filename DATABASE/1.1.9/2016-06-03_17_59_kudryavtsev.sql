alter table Acts_szr_income alter column count numeric(15,3)
alter table Acts_szr_income alter column price numeric(15,3)
alter table Acts_szr_income alter column cost numeric(15,3)

alter table Materials alter column balance numeric(15,3)
alter table Materials alter column current_price numeric(10,2)