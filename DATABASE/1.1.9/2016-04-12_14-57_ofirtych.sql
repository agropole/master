DROP TABLE Tptask_to_aggreagate
DROP TABLE Tptask_to_plants

CREATE TABLE Task_rate (id int not null primary key,
						id_plant int,
						id_trak int,
						id_equi int,
						id_agroreq int,
						shift_output numeric (8,2),
						rate_shift numeric (8,2),
						rate_piecework numeric (8,2),
						fuel_consumption numeric (8,2))


ALTER TABLE [dbo].Task_rate  WITH CHECK ADD  CONSTRAINT [FK_Task_rate_to_Plants] FOREIGN KEY([id_plant])
REFERENCES [dbo].[Plants] ([plant_id])
ALTER TABLE [dbo].Task_rate  WITH CHECK ADD  CONSTRAINT [FK_Task_rate_to_Traktors] FOREIGN KEY([id_trak])
REFERENCES [dbo].[Traktors] ([trakt_id])
ALTER TABLE [dbo].Task_rate  WITH CHECK ADD  CONSTRAINT [FK_Task_rate_to_Equipments] FOREIGN KEY([id_equi])
REFERENCES [dbo].[Equipments] ([equi_id])
ALTER TABLE [dbo].Task_rate  WITH CHECK ADD  CONSTRAINT [FK_Task_rate_to_Agrotech_requirment] FOREIGN KEY([id_agroreq])
REFERENCES [dbo].[Agrotech_requirment] ([id])


ALTER TABLE Task_rate ADD id_tptas int not null
ALTER TABLE [dbo].Task_rate  WITH CHECK ADD  CONSTRAINT [FK_Task_rate_to_Type_tasks] FOREIGN KEY([id_tptas])
REFERENCES [dbo].[Type_tasks] ([tptas_id])