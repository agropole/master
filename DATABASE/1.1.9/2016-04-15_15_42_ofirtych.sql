ALTER TABLE Day_task_detail ADD id_agroreq int;
ALTER TABLE [dbo].[Day_task_detail]  WITH CHECK ADD  CONSTRAINT [Day_task_detail_to_Agrotech_requirment] FOREIGN KEY(id_agroreq)
REFERENCES [dbo].[Agrotech_requirment] ([id])

alter table Sheet_tracks ADD id_agroreq int;
ALTER TABLE [dbo].[Sheet_tracks]  WITH CHECK ADD  CONSTRAINT [Sheet_tracks_to_Agrotech_requirment] FOREIGN KEY(id_agroreq)
REFERENCES [dbo].[Agrotech_requirment] ([id]);

alter table TC_operation ADD id_rate int;
ALTER TABLE [dbo].[TC_operation]  WITH CHECK ADD  CONSTRAINT [TC_operation_to_Task_rate] FOREIGN KEY(id_rate)
REFERENCES [dbo].Task_rate ([id]);

alter table [TC_operation] ADD id_agroreq int;
ALTER TABLE [dbo].[TC_operation]  WITH CHECK ADD  CONSTRAINT [TC_operation_to_Agrotech_requirment] FOREIGN KEY(id_agroreq)
REFERENCES [dbo].Agrotech_requirment ([id]);

