delete [dbo].[Dictionary_column] where id_dictionary=6 and field='balance'
delete [dbo].[Dictionary_column] where id_dictionary=6 and field='description'
delete [dbo].[Dictionary_column] where id_dictionary=6 and field='current_price'
update [dbo].[Dictionary_column] set width='30%' where id_dictionary=6 and field='name' 
update [dbo].[Dictionary_column] set width='30%' where id_dictionary=6 and field='mat_type_id' 
update [dbo].[Dictionary_column] set width='35%' where id_dictionary=6 and field='code' 

CREATE TABLE [dbo].[TMC_Records](
[tmr_id] [int] NOT NULL IDENTITY(1,1),
[mat_mat_id] [int] NOT NULL,
[cont_cont_id] [int] NOT NULL,
[act_date] [datetime] NOT NULL,
[count] [real] NOT NULL,
[price] [real] NOT NULL,
[emp_from_id] [int] NULL,
[emp_to_id] [int] NULL,
CONSTRAINT [PK_TMC_Records] PRIMARY KEY CLUSTERED 
(
[tmr_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

alter table [dbo].[TMC_Records] add deleted bit not null default 0

CREATE TABLE [dbo].[TMC_Acts](
[tma_id] [int] NOT NULL IDENTITY(1,1),
[fielplan_fielplan_id] [int] NULL,
[act_date] [datetime] NULL,
[act_num] [varchar](50) NULL,
[emp_emp_id] [int] NULL,
CONSTRAINT [PK_TMC_Acts] PRIMARY KEY CLUSTERED 
(
[tma_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO

SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[TMC_Current](
[tmc_id] [int] NOT NULL IDENTITY(1,1),
[mat_mat_id] [int] NOT NULL,
[cont_cont_id] [int] NOT NULL,
[act_date] [datetime] NOT NULL,
[count] [real] NOT NULL,
[price] [real] NOT NULL,
[emp_emp_id] [int] NOT NULL,
CONSTRAINT [PK_TMC_Current] PRIMARY KEY CLUSTERED 
(
[tmc_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

--- create trigger
drop trigger dbo.tmc_record_updater

create trigger dbo.tmc_record_updater
ON tmc_records
FOR INSERT
AS 

--- Prihod
declare @v_mat_id INT
declare @v_emp_id INT
declare @v_act_date DATE
declare @v_cnt FLOAT
declare @v_aver FLOAT
declare xcur CURSOR FOR
select mat_mat_id, 
emp_to_id,
max(act_date) act_date, 
sum([count]) cnt, 
sum([count]*price)/sum([count]) aver
from inserted
where emp_to_id is not null
group by mat_mat_id, emp_to_id;

BEGIN
OPEN xcur 
FETCH NEXT FROM xcur 
INTO @v_mat_id, @v_emp_id, @v_act_date,@v_cnt, @v_aver 
WHILE @@FETCH_STATUS = 0 
BEGIN 
IF (@v_cnt != 0) 
UPDATE tmc_current set 
act_date = @v_act_date,
[count] = [count] + @v_cnt,
price = (price*[count]+@v_cnt*@v_aver)/([count] + @v_cnt)
WHERE mat_mat_id = @v_mat_id
AND emp_emp_id = @v_emp_id;

FETCH NEXT FROM xcur 
INTO @v_mat_id, @v_emp_id, @v_act_date,@v_cnt, @v_aver 
END
CLOSE xcur;
deallocate xcur;

--- Rashod

declare ycur CURSOR FOR
select mat_mat_id, 
emp_from_id,
max(act_date) act_date, 
sum([count]) cnt
from inserted
where emp_from_id is not null
group by mat_mat_id, emp_from_id

OPEN ycur 
FETCH NEXT FROM ycur 
INTO @v_mat_id, @v_emp_id, @v_act_date,@v_cnt
WHILE @@FETCH_STATUS = 0 
BEGIN 
UPDATE tmc_current set 
act_date = @v_act_date,
[count] = [count] - @v_cnt
WHERE mat_mat_id = @v_mat_id
AND emp_emp_id = @v_emp_id;
FETCH NEXT FROM ycur 
INTO @v_mat_id, @v_emp_id, @v_act_date,@v_cnt
END
CLOSE ycur;
deallocate ycur;
END;

alter table [dbo].[Employees] add tmc_responsible bit not null default 0
update [dbo].[Employees] set tmc_responsible=1 where emp_id=30
update [dbo].[Employees] set tmc_responsible=1 where emp_id=31
update [dbo].[Employees] set tmc_responsible=1 where emp_id=132
update [dbo].[Employees] set tmc_responsible=1 where emp_id=184

alter table [dbo].[TMC_Acts] add soil_soil_id  int not null 
alter table [dbo].[TMC_Acts] add emp_utv_id  int not null 
alter table [dbo].[TMC_Records] add area  real  null
alter table [dbo].[TMC_Records] add tma_tma_id int  null
alter table [dbo].[TMC_Records] add fielplan_fielplan_id  int null ;
alter table [dbo].[TMC_Acts] add year numeric(4,0) null ;

