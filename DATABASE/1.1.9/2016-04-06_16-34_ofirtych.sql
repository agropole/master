  CREATE TABLE TypeCount (id int not null primary key, name varchar(50));
  ALTER TABLE Type_tasks ADD id_tpcount int;
  ALTER TABLE Type_tasks  WITH CHECK ADD  CONSTRAINT [FK_Type_tasks_to_TypeCount] FOREIGN KEY(id_tpcount)
  REFERENCES [dbo].TypeCount (id);

  INSERT INTO TypeCount VALUES (1, 'Сдельная(га)');
  INSERT INTO TypeCount VALUES (2, 'Штучная(смены)');
  INSERT INTO TypeCount VALUES (3, 'Штучная(объему)');
  update Type_tasks set id_tpcount=1
  
  ALTER TABLE Type_tasks ADD rate_shift numeric(8,2);
  ALTER TABLE Type_tasks ADD rate_piecework numeric(8,2);
  ALTER TABLE Type_tasks ADD rate_support_shift numeric(8,2);
  ALTER TABLE Type_tasks ADD rate_support_piecework numeric(8,2);
    

  CREATE TABLE Tptask_to_plants(id int not null primary key, 
								id_plant int not null, id_tptask int not null, 
								rate_shift numeric(8,2), rate_piecework numeric(8,2), 
								rate_support_shift numeric(8,2), rate_support_piecework numeric(8,2),
								fuel_comsumption numeric(8,2));
  CREATE TABLE Tptask_to_aggreagate (id int not null primary key,
									 id_tptask int not null, id_trak int, id_equi int,
									 rate_shift numeric(8,2), rate_piecework numeric(8,2), 
									 rate_support_shift numeric(8,2),rate_support_piecework numeric(8,2),
									 fuel_comsumption numeric(8,2));