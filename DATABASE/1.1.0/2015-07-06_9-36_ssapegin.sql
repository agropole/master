ALTER TABLE Sheet_tracks ADD 
exported INT default 0;
GO
update Sheet_tracks set exported=0;
GO
CREATE TRIGGER st_trg1 
ON dbo.Sheet_tracks
AFTER UPDATE 
AS
DECLARE @xx1 integer;
DECLARE @id integer;

DECLARE @xx2 integer;
BEGIN
SET NOCOUNT ON;
declare xx CURSOR FOR 
SELECT shtr_id,exported FROM inserted;

OPEN xx;
WHILE (1=1)
BEGIN
FETCH NEXT FROM xx INTO @id, @xx1;
IF (@@FETCH_STATUS != 0) break;
select @xx2 = exported FROM deleted where shtr_id = @id; 
if (@xx1 = @xx2)
BEGIN
if (@xx1 = 1) 
begin
update Sheet_tracks set exported=2 where shtr_id = @id;
end;
END;
END
CLOSE xx;
DEALLOCATE xx;

END;
GO

CREATE TRIGGER st_trg2 
ON dbo.Tasks
AFTER INSERT,UPDATE,DELETE
AS
DECLARE @id integer;
DECLARE @xx1 integer;
BEGIN
SET NOCOUNT ON;
declare xx� CURSOR FOR 
SELECT distinct shtr_shtr_id FROM 
(SELECT shtr_shtr_id FROM inserted
UNION
SELECT shtr_shtr_id FROM deleted) as R

OPEN xx�;
WHILE (1=1)
BEGIN
FETCH NEXT FROM xx� INTO @id;
IF (@@FETCH_STATUS != 0) break;
update Sheet_tracks set exported=2 where shtr_id = @id and exported=1;
END
CLOSE xx�;
DEALLOCATE xx�;

END;