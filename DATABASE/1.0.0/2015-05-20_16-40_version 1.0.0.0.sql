create table DbVersion (
   id int IDENTITY(1, 1) PRIMARY KEY,
   version nvarchar(32) not null
)
go
 
insert into DbVersion (version) values ('1.0.0.0')
go