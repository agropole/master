﻿update Traktors set name = 'HARDI - 36 м.' where trakt_id = 221
go
update Traktors set name = 'ЧТЗ Т-10М' where trakt_id = 211
go



alter table dbo.Traktors alter column code varchar(50)
go
update dbo.Traktors set code = 'e409bdb0-b085-11e2-bf1d-005056c00008', int_id = '5da2dfe6-e496-11e2-ad10-005056c00008' where trakt_id = 221
go
update dbo.Traktors set code = '117e030c-9169-11df-ab93-000e0ce57eec', int_id = '5b4d5280-c440-11e2-911c-005056c00008' where trakt_id = 8
go
update dbo.Traktors set code = 'd0d5eb23-4867-11e4-a130-005056c00008', int_id = 'a656d462-455b-11e4-926e-005056c00008' where trakt_id = 181
go
update dbo.Traktors set code = '5406a14f-8f3a-11df-ab93-000e0ce57eec', int_id = 'd7168a32-c115-11e2-a7d0-005056c00008' where trakt_id = 5
go
update Traktors set code = '0cb3f2f3-9c9e-11e0-b4d4-1cc1de785a4a' where trakt_id = 211
go



alter table dbo.Equipments alter column code varchar(50)
go
update dbo.Equipments set code = 'e001083a-d6e9-11e4-80c4-002590ebf8cd' where equi_id = 11
go
update dbo.Equipments set code = 'aa8fae0a-7abf-11e0-baeb-000e0ce57eec' where equi_id = 70
go

