USE [AgroDB]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[Type_spare_parts](
	[tsp_id] [int] IDENTITY(1,1) NOT NULL,
	[code] [varchar](30) NULL,
	[name] [varchar](100) NOT NULL,
	[description] [varchar](200) NULL,
	[deleted] [bit] NULL,
	[uniq_code] [varchar](50) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
INSERT [dbo].[Dictionary] ([id], [name], [table_name], [request], [request_update], [index_column], [request_detail]) VALUES (32, N'���� ���������', N'Type_spare_parts', N'DictionaryCtrl/GetTypeSpareParts', N'DictionaryCtrl/UpdateTypeSpareParts', N'tsp_id', NULL)
  INSERT [dbo].[Dictionary_column] ([id], [id_dictionary], [id_dictionary_column_type], [field], [displayName], [width]) VALUES (198, 32, 2, N'tsp_id', N'', N'0%')
INSERT [dbo].[Dictionary_column] ([id], [id_dictionary], [id_dictionary_column_type], [field], [displayName], [width]) VALUES (199, 32, 1, N'name', N'��������', N'55%')
INSERT [dbo].[Dictionary_column] ([id], [id_dictionary], [id_dictionary_column_type], [field], [displayName], [width]) VALUES (200, 32, 1, N'uniq_code', N'���', N'40%')
INSERT [dbo].[Dictionary_column] ([id], [id_dictionary], [id_dictionary_column_type], [field], [displayName], [width]) VALUES (201, 32, 5, N'deleted', NULL, N'5%')

  update [Spare_parts] set [tptrak_tptrak_id]=null
  alter table [Repairs_acts] add type_rep_parts int null