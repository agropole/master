
truncate table Roles

GO
SET IDENTITY_INSERT [dbo].[Roles] ON 

INSERT [dbo].[Roles] ([role_id], [role_name]) VALUES (2, N'Сменное задание')
INSERT [dbo].[Roles] ([role_id], [role_name]) VALUES (4, N'Диспетчер')
INSERT [dbo].[Roles] ([role_id], [role_name]) VALUES (10, N'Администратор')
INSERT [dbo].[Roles] ([role_id], [role_name]) VALUES (14, N'Агроном')
INSERT [dbo].[Roles] ([role_id], [role_name]) VALUES (15, N'Кладовщик')
INSERT [dbo].[Roles] ([role_id], [role_name]) VALUES (16, N'Кладовщик ТМЦ')
SET IDENTITY_INSERT [dbo].[Roles] OFF
SET IDENTITY_INSERT [dbo].[Roles_subsystems] ON 

INSERT [dbo].[Roles_subsystems] ([rss_id], [role_role_id], [subsystem_id]) VALUES (1737, 14, N'tas7')
INSERT [dbo].[Roles_subsystems] ([rss_id], [role_role_id], [subsystem_id]) VALUES (1738, 14, N'tas8')
INSERT [dbo].[Roles_subsystems] ([rss_id], [role_role_id], [subsystem_id]) VALUES (1739, 14, N'plan0')
INSERT [dbo].[Roles_subsystems] ([rss_id], [role_role_id], [subsystem_id]) VALUES (1740, 14, N'plan1')
INSERT [dbo].[Roles_subsystems] ([rss_id], [role_role_id], [subsystem_id]) VALUES (1741, 14, N'plan2')
INSERT [dbo].[Roles_subsystems] ([rss_id], [role_role_id], [subsystem_id]) VALUES (1742, 14, N'plan3')
INSERT [dbo].[Roles_subsystems] ([rss_id], [role_role_id], [subsystem_id]) VALUES (1743, 14, N'plan4')
INSERT [dbo].[Roles_subsystems] ([rss_id], [role_role_id], [subsystem_id]) VALUES (1744, 4, N'tas0')
INSERT [dbo].[Roles_subsystems] ([rss_id], [role_role_id], [subsystem_id]) VALUES (1745, 4, N'tas1')
INSERT [dbo].[Roles_subsystems] ([rss_id], [role_role_id], [subsystem_id]) VALUES (1746, 4, N'tas2')
INSERT [dbo].[Roles_subsystems] ([rss_id], [role_role_id], [subsystem_id]) VALUES (1747, 15, N'tasM3')
INSERT [dbo].[Roles_subsystems] ([rss_id], [role_role_id], [subsystem_id]) VALUES (1748, 15, N'gar0')
INSERT [dbo].[Roles_subsystems] ([rss_id], [role_role_id], [subsystem_id]) VALUES (1749, 15, N'gar1')
INSERT [dbo].[Roles_subsystems] ([rss_id], [role_role_id], [subsystem_id]) VALUES (1750, 15, N'gar2')
INSERT [dbo].[Roles_subsystems] ([rss_id], [role_role_id], [subsystem_id]) VALUES (1751, 15, N'gar3')
INSERT [dbo].[Roles_subsystems] ([rss_id], [role_role_id], [subsystem_id]) VALUES (1752, 15, N'gar4')
INSERT [dbo].[Roles_subsystems] ([rss_id], [role_role_id], [subsystem_id]) VALUES (1753, 15, N'gar5')
INSERT [dbo].[Roles_subsystems] ([rss_id], [role_role_id], [subsystem_id]) VALUES (1754, 15, N'gar6')
INSERT [dbo].[Roles_subsystems] ([rss_id], [role_role_id], [subsystem_id]) VALUES (1755, 15, N'gar7')
INSERT [dbo].[Roles_subsystems] ([rss_id], [role_role_id], [subsystem_id]) VALUES (1756, 15, N'gar8')
INSERT [dbo].[Roles_subsystems] ([rss_id], [role_role_id], [subsystem_id]) VALUES (1757, 15, N'gar9')
INSERT [dbo].[Roles_subsystems] ([rss_id], [role_role_id], [subsystem_id]) VALUES (1758, 16, N'tas5')
INSERT [dbo].[Roles_subsystems] ([rss_id], [role_role_id], [subsystem_id]) VALUES (1759, 2, N'tas0')
INSERT [dbo].[Roles_subsystems] ([rss_id], [role_role_id], [subsystem_id]) VALUES (170, 10, N'tasM5')
INSERT [dbo].[Roles_subsystems] ([rss_id], [role_role_id], [subsystem_id]) VALUES (171, 10, N'tasM6')
INSERT [dbo].[Roles_subsystems] ([rss_id], [role_role_id], [subsystem_id]) VALUES (172, 10, N'tasM7')
INSERT [dbo].[Roles_subsystems] ([rss_id], [role_role_id], [subsystem_id]) VALUES (173, 10, N'tasM')
INSERT [dbo].[Roles_subsystems] ([rss_id], [role_role_id], [subsystem_id]) VALUES (174, 10, N'tas0')
INSERT [dbo].[Roles_subsystems] ([rss_id], [role_role_id], [subsystem_id]) VALUES (175, 10, N'tas1')
INSERT [dbo].[Roles_subsystems] ([rss_id], [role_role_id], [subsystem_id]) VALUES (176, 10, N'tas2')
INSERT [dbo].[Roles_subsystems] ([rss_id], [role_role_id], [subsystem_id]) VALUES (177, 10, N'tas3')
INSERT [dbo].[Roles_subsystems] ([rss_id], [role_role_id], [subsystem_id]) VALUES (178, 10, N'tas4')
INSERT [dbo].[Roles_subsystems] ([rss_id], [role_role_id], [subsystem_id]) VALUES (179, 10, N'tas5')
INSERT [dbo].[Roles_subsystems] ([rss_id], [role_role_id], [subsystem_id]) VALUES (180, 10, N'tas6')
INSERT [dbo].[Roles_subsystems] ([rss_id], [role_role_id], [subsystem_id]) VALUES (181, 10, N'tas7')
INSERT [dbo].[Roles_subsystems] ([rss_id], [role_role_id], [subsystem_id]) VALUES (182, 10, N'tas8')
INSERT [dbo].[Roles_subsystems] ([rss_id], [role_role_id], [subsystem_id]) VALUES (183, 10, N'tas9')
INSERT [dbo].[Roles_subsystems] ([rss_id], [role_role_id], [subsystem_id]) VALUES (184, 10, N'tas10')
INSERT [dbo].[Roles_subsystems] ([rss_id], [role_role_id], [subsystem_id]) VALUES (185, 10, N'tas11')
INSERT [dbo].[Roles_subsystems] ([rss_id], [role_role_id], [subsystem_id]) VALUES (186, 10, N'tas12')
INSERT [dbo].[Roles_subsystems] ([rss_id], [role_role_id], [subsystem_id]) VALUES (187, 10, N'tasM2')
INSERT [dbo].[Roles_subsystems] ([rss_id], [role_role_id], [subsystem_id]) VALUES (188, 10, N'plan0')
INSERT [dbo].[Roles_subsystems] ([rss_id], [role_role_id], [subsystem_id]) VALUES (189, 10, N'plan1')
INSERT [dbo].[Roles_subsystems] ([rss_id], [role_role_id], [subsystem_id]) VALUES (190, 10, N'plan2')
INSERT [dbo].[Roles_subsystems] ([rss_id], [role_role_id], [subsystem_id]) VALUES (191, 10, N'plan3')
INSERT [dbo].[Roles_subsystems] ([rss_id], [role_role_id], [subsystem_id]) VALUES (192, 10, N'plan4')
INSERT [dbo].[Roles_subsystems] ([rss_id], [role_role_id], [subsystem_id]) VALUES (193, 10, N'tasM3')
INSERT [dbo].[Roles_subsystems] ([rss_id], [role_role_id], [subsystem_id]) VALUES (194, 10, N'gar0')
INSERT [dbo].[Roles_subsystems] ([rss_id], [role_role_id], [subsystem_id]) VALUES (195, 10, N'gar1')
INSERT [dbo].[Roles_subsystems] ([rss_id], [role_role_id], [subsystem_id]) VALUES (196, 10, N'gar2')
INSERT [dbo].[Roles_subsystems] ([rss_id], [role_role_id], [subsystem_id]) VALUES (197, 10, N'gar3')
INSERT [dbo].[Roles_subsystems] ([rss_id], [role_role_id], [subsystem_id]) VALUES (198, 10, N'gar4')
INSERT [dbo].[Roles_subsystems] ([rss_id], [role_role_id], [subsystem_id]) VALUES (199, 10, N'gar5')
INSERT [dbo].[Roles_subsystems] ([rss_id], [role_role_id], [subsystem_id]) VALUES (200, 10, N'gar6')
INSERT [dbo].[Roles_subsystems] ([rss_id], [role_role_id], [subsystem_id]) VALUES (201, 10, N'gar7')
INSERT [dbo].[Roles_subsystems] ([rss_id], [role_role_id], [subsystem_id]) VALUES (202, 10, N'gar8')
INSERT [dbo].[Roles_subsystems] ([rss_id], [role_role_id], [subsystem_id]) VALUES (203, 10, N'gar9')
INSERT [dbo].[Roles_subsystems] ([rss_id], [role_role_id], [subsystem_id]) VALUES (204, 10, N'tasM4')
SET IDENTITY_INSERT [dbo].[Roles_subsystems] OFF
