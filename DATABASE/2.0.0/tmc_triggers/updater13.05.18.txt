USE [AgroDB]
GO
/****** Object:  Trigger [dbo].[tmc_record_updater]    Script Date: 14.05.2019 10:40:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Batch submitted through debugger: SQLQuery6.sql|7|0|C:\Users\836D~1\AppData\Local\Temp\~vs1012.sql



ALTER trigger [dbo].[tmc_record_updater]
  ON [dbo].[TMC_Records]
  FOR UPDATE
  AS 
  
  --- Prihod
  declare @v_mat_id INT
  declare @v_emp_id INT
  declare @v_act_date DATE
  declare @v_cnt FLOAT
  declare @v_aversum FLOAT
  declare @v_aver FLOAT
  declare @v_chk INT
  declare @v_rss INT
  declare @v_prc FLOAT

    declare xcur CURSOR FOR
    select mat_mat_id, 
         emp_to_id,
         max(act_date) act_date, 
		 CAST ( sum([count]) AS real) cnt ,  
		 sum([count]*price) aversum
   from deleted
   where emp_to_id is not null and emp_to_id <> 0
  group by mat_mat_id, emp_to_id;
  BEGIN
  OPEN xcur  
  FETCH NEXT FROM xcur   
      INTO @v_mat_id, @v_emp_id, @v_act_date,@v_cnt, @v_aversum 
  WHILE @@FETCH_STATUS = 0  
  BEGIN 
      IF (@v_cnt != 0) 
	  BEGIN 
	
	    SET @v_chk = (SELECT COUNT(*) from TMC_Current WHERE mat_mat_id = @v_mat_id AND  emp_emp_id = @v_emp_id);
		IF (@v_chk = 0)
		BEGIN
		  SET @v_prc = isnull((SELECT current_price from Materials where mat_id = @v_mat_id),0);
		  INSERT INTO tmc_current (act_date, mat_mat_id, cont_cont_id, [count], price, emp_emp_id)
		    VALUES (0, @v_mat_id, 0, 0, @v_prc, @v_emp_id);  
        END;

        SET @v_rss = (SELECT [count] FROM tmc_current WHERE mat_mat_id = @v_mat_id AND  emp_emp_id = @v_emp_id);
		IF (@v_rss != @v_cnt)
        UPDATE tmc_current set 
	      act_date = @v_act_date,
		  [count] = [count] - @v_cnt,
		  price = ABS((price*[count] - @v_aversum)/([count] - @v_cnt))
        WHERE [count] <> @v_cnt
		AND  mat_mat_id = @v_mat_id
	    AND  emp_emp_id = @v_emp_id;
        ELSE 
		  DELETE FROM tmc_current WHERE mat_mat_id = @v_mat_id AND  emp_emp_id = @v_emp_id;

      END;
	  FETCH NEXT FROM xcur   
      INTO @v_mat_id, @v_emp_id, @v_act_date,@v_cnt, @v_aversum 
  END
  CLOSE xcur;
  deallocate xcur;

  declare xcur CURSOR FOR
    select mat_mat_id, 
         emp_to_id,
         max(act_date) act_date, 
		  CAST ( sum([count]) AS real) cnt , 
		 sum([count]*price)/sum([count]) aver
   from inserted
   where emp_to_id is not null and emp_to_id <> 0
  group by mat_mat_id, emp_to_id;

  OPEN xcur  
  FETCH NEXT FROM xcur   
      INTO @v_mat_id, @v_emp_id, @v_act_date,@v_cnt, @v_aver 
  WHILE @@FETCH_STATUS = 0  
  BEGIN 
      IF (@v_cnt != 0) 
	  BEGIN 

	    SET @v_chk = (SELECT COUNT(*) from TMC_Current WHERE mat_mat_id = @v_mat_id AND  emp_emp_id = @v_emp_id);
		IF (@v_chk = 0)
		BEGIN
		  SET @v_prc = isnull((SELECT current_price from Materials where mat_id = @v_mat_id),0);
		  INSERT INTO tmc_current (act_date, mat_mat_id, cont_cont_id, [count], price, emp_emp_id)
		    VALUES (0, @v_mat_id, 0, 0, @v_prc, @v_emp_id);  
        END;

        SET @v_rss = (SELECT [count] FROM tmc_current WHERE mat_mat_id = @v_mat_id AND  emp_emp_id = @v_emp_id);
		IF (@v_rss != -@v_cnt)
        UPDATE tmc_current set 
	      act_date = @v_act_date,
		  [count] = [count] + @v_cnt,
		  price = (price*[count]+@v_cnt*@v_aver)/([count] + @v_cnt)
        WHERE mat_mat_id = @v_mat_id
	    AND  emp_emp_id = @v_emp_id;
        ELSE 
		  DELETE FROM tmc_current WHERE mat_mat_id = @v_mat_id AND  emp_emp_id = @v_emp_id;


      END;
	  FETCH NEXT FROM xcur   
      INTO @v_mat_id, @v_emp_id, @v_act_date,@v_cnt, @v_aver 
  END
  CLOSE xcur;
  deallocate xcur;

  --- Rashod
declare ycur CURSOR FOR
    select mat_mat_id, 
         emp_from_id,
         max(act_date) act_date, 
		 sum([count]) cnt
     from deleted
     where emp_from_id is not null and emp_from_id <> 0
     group by mat_mat_id, emp_from_id

  OPEN ycur  
  FETCH NEXT FROM ycur   
      INTO @v_mat_id, @v_emp_id, @v_act_date,@v_cnt
  WHILE @@FETCH_STATUS = 0  
  BEGIN 

      SET @v_chk = (SELECT COUNT(*) from TMC_Current WHERE mat_mat_id = @v_mat_id AND  emp_emp_id = @v_emp_id);
	  IF (@v_chk = 0)
	  BEGIN
	    SET @v_prc = isnull((SELECT current_price from Materials where mat_id = @v_mat_id),0);
		INSERT INTO tmc_current (act_date, mat_mat_id, cont_cont_id, [count], price, emp_emp_id)
		  VALUES (0, @v_mat_id, 0, 0, @v_prc, @v_emp_id);  
      END;

        SET @v_rss = (SELECT [count] FROM tmc_current WHERE mat_mat_id = @v_mat_id AND  emp_emp_id = @v_emp_id);
		IF (@v_rss != -@v_cnt)
      UPDATE tmc_current set 
	    act_date = @v_act_date,
		[count] = [count] + @v_cnt
      WHERE mat_mat_id = @v_mat_id
	  AND  emp_emp_id = @v_emp_id;
      ELSE 
		DELETE FROM tmc_current WHERE mat_mat_id = @v_mat_id AND  emp_emp_id = @v_emp_id;

	  FETCH NEXT FROM ycur   
      INTO @v_mat_id, @v_emp_id, @v_act_date,@v_cnt
  END
  CLOSE ycur;
  deallocate ycur;

  declare ycur CURSOR FOR
    select mat_mat_id, 
         emp_from_id,
         max(act_date) act_date, 
		 sum([count]) cnt
     from inserted
     where emp_from_id is not null and emp_from_id <> 0
     group by mat_mat_id, emp_from_id

  OPEN ycur  
  FETCH NEXT FROM ycur   
      INTO @v_mat_id, @v_emp_id, @v_act_date,@v_cnt
  WHILE @@FETCH_STATUS = 0  
  BEGIN 

      SET @v_chk = (SELECT COUNT(*) from TMC_Current WHERE mat_mat_id = @v_mat_id AND  emp_emp_id = @v_emp_id);
	  IF (@v_chk = 0)
	  BEGIN
	    SET @v_prc = isnull((SELECT current_price from Materials where mat_id = @v_mat_id),0);
		INSERT INTO tmc_current (act_date, mat_mat_id, cont_cont_id, [count], price, emp_emp_id)
		  VALUES (0, @v_mat_id, 0, 0, @v_prc, @v_emp_id);  
      END

      SET @v_rss = (SELECT [count] FROM tmc_current WHERE mat_mat_id = @v_mat_id AND  emp_emp_id = @v_emp_id);
      IF (@v_rss != @v_cnt)
      UPDATE tmc_current set 
	    act_date = @v_act_date,
		[count] = [count] - @v_cnt
      WHERE mat_mat_id = @v_mat_id
	  AND  emp_emp_id = @v_emp_id;
      ELSE 
		DELETE FROM tmc_current WHERE mat_mat_id = @v_mat_id AND  emp_emp_id = @v_emp_id;

	  FETCH NEXT FROM ycur   
      INTO @v_mat_id, @v_emp_id, @v_act_date,@v_cnt
  END
  CLOSE ycur;
  deallocate ycur;
END;