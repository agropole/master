USE [AgroDB]
GO
/****** Object:  Trigger [dbo].[spare_record_updater]    Script Date: 26.06.2017 19:28:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



USE [AgroDB]
GO
/****** Object:  Trigger [dbo].[spare_record_updater]    Script Date: 26.06.2017 19:28:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



alter trigger [dbo].[spare_parts_record_updater]
  ON [dbo].[Spare_parts_Records]
  FOR UPDATE
  AS 
  
  --- Prihod
  declare @v_mat_id INT
  declare @v_emp_id INT
  declare @v_act_date DATE
  declare @v_cnt FLOAT
  declare @v_aversum FLOAT
  declare @v_aver FLOAT
  declare @v_chk INT
  declare @v_rss INT
  declare @v_prc FLOAT
  declare @v_nds FLOAT
  declare @v_avg FLOAT
  
    declare xcur CURSOR FOR
    select store_to_id, 
         spare_spare_id,
         max(date) date, 
		 sum([count]) cnt, 
		 sum([count]*price) aversum,
		 avg(nds) nds
   from deleted
   where store_to_id is not null and store_to_id <> 0 and count <> 0 and price <> 0
  group by store_to_id, spare_spare_id;

  BEGIN
  OPEN xcur  
  FETCH NEXT FROM xcur   
      INTO @v_mat_id, @v_emp_id, @v_act_date,@v_cnt, @v_aversum , @v_nds
  WHILE @@FETCH_STATUS = 0  
  BEGIN 
      IF (@v_cnt != 0) 
	  BEGIN 

	  SET @v_chk = (SELECT COUNT(*) from Spare_parts_Current WHERE spare_spare_id = @v_emp_id AND  store_store_id = @v_mat_id);
		IF (@v_chk = 0)
		BEGIN
		  INSERT INTO Spare_parts_Current (spare_spare_id, date, count, price, store_store_id, nds)
		    VALUES (@v_emp_id , 0, 0, 0, @v_mat_id,0);  
        END;
        SET @v_rss = (SELECT [count] FROM Spare_parts_Current WHERE spare_spare_id = @v_emp_id AND  store_store_id = @v_mat_id);
		set @v_avg = (SELECT avg(nds) FROM Spare_parts_Records WHERE spare_spare_id = @v_emp_id AND  store_to_id = @v_mat_id);
		IF (@v_rss != @v_cnt)
	
		
        UPDATE Spare_parts_Current set 
	       date = @v_act_date,
		  [count] = [count] - @v_cnt,
		  price = ABS((price*[count] - @v_aversum)/([count] - @v_cnt)),
		  nds = @v_avg
        WHERE [count] <> @v_cnt
		AND spare_spare_id = @v_emp_id
	      AND  store_store_id = @v_mat_id;
        ELSE 
		  DELETE FROM Spare_parts_Current WHERE spare_spare_id = @v_emp_id AND  store_store_id = @v_mat_id;

      END;
	  FETCH NEXT FROM xcur   
      INTO @v_mat_id, @v_emp_id, @v_act_date,@v_cnt, @v_aversum , @v_nds
  END
  CLOSE xcur;
  deallocate xcur;

  declare xcur CURSOR FOR
    select store_to_id, 
         spare_spare_id,
         max(date) date, 
		 sum([count]) cnt, 
		 sum([count]*price)/sum([count]) aver,
		  avg(nds) nds
   from inserted
    where store_to_id is not null and store_to_id <> 0 and count <> 0 and price <> 0
  group by store_to_id, spare_spare_id;

  OPEN xcur  
  FETCH NEXT FROM xcur   
      INTO @v_mat_id, @v_emp_id, @v_act_date,@v_cnt, @v_aver , @v_nds
  WHILE @@FETCH_STATUS = 0  
  BEGIN 
      IF (@v_cnt != 0) 
	  BEGIN 

	    SET @v_chk = (SELECT COUNT(*) from Spare_parts_Current WHERE spare_spare_id = @v_emp_id AND  store_store_id = @v_mat_id);
		IF (@v_chk = 0)
		BEGIN
		  INSERT INTO Spare_parts_Current (spare_spare_id, date, count, price, store_store_id, nds)
		    VALUES (@v_emp_id , 0, 0, 0, @v_mat_id, 0); 
        END;
	    set @v_avg = (SELECT avg(nds) FROM Spare_parts_Records WHERE spare_spare_id = @v_emp_id AND  store_to_id = @v_mat_id);
        SET @v_rss = (SELECT [count] FROM Spare_parts_Current WHERE spare_spare_id = @v_emp_id AND  store_store_id = @v_mat_id);
		IF (@v_rss != -@v_cnt)
		
        UPDATE Spare_parts_Current set 
	      date = @v_act_date,
		  [count] = [count] + @v_cnt,
		  price = (price*[count]+@v_cnt*@v_aver)/([count] + @v_cnt),
		  nds = @v_avg
        WHERE spare_spare_id = @v_emp_id
	      AND  store_store_id = @v_mat_id;
        ELSE 
		DELETE FROM Spare_parts_Current WHERE spare_spare_id = @v_emp_id AND  store_store_id = @v_mat_id;


      END;
	  FETCH NEXT FROM xcur   
      INTO @v_mat_id, @v_emp_id, @v_act_date,@v_cnt, @v_aver , @v_nds
  END
  CLOSE xcur;
  deallocate xcur;

  --- Rashod

  declare ycur CURSOR FOR
   select store_from_id, 
         spare_spare_id,
         max(date) date, 
		 sum([count]) cnt
     from deleted
      where store_from_id is not null and store_from_id <> 0 and count <> 0 and price <> 0
  group by store_from_id, spare_spare_id;

  OPEN ycur  
  FETCH NEXT FROM ycur   
      INTO @v_mat_id, @v_emp_id, @v_act_date,@v_cnt
  WHILE @@FETCH_STATUS = 0  
  BEGIN 

      SET @v_chk = (SELECT COUNT(*) from Spare_parts_Current WHERE spare_spare_id = @v_emp_id AND  store_store_id = @v_mat_id);
	  IF (@v_chk = 0)
	  BEGIN
	    INSERT INTO Spare_parts_Current (spare_spare_id, date, count, price, store_store_id,nds)
		    VALUES (@v_emp_id , 0, 0, 0, @v_mat_id,0);   
      END;

          SET @v_rss = (SELECT [count] FROM Spare_parts_Current WHERE spare_spare_id = @v_emp_id AND  store_store_id = @v_mat_id);
		IF (@v_rss != -@v_cnt)
      UPDATE Spare_parts_Current set 
	    date = @v_act_date,
		[count] = [count] + @v_cnt
      WHERE spare_spare_id = @v_emp_id
	      AND  store_store_id = @v_mat_id;
      ELSE 
	 DELETE FROM Spare_parts_Current WHERE spare_spare_id = @v_emp_id AND  store_store_id = @v_mat_id;

	  FETCH NEXT FROM ycur   
      INTO @v_mat_id, @v_emp_id, @v_act_date,@v_cnt
  END
  CLOSE ycur;
  deallocate ycur;

  declare ycur CURSOR FOR
    select store_from_id, 
         spare_spare_id,
         max(date) date, 
		 sum([count]) cnt
     from inserted
    where store_from_id is not null and store_from_id <> 0 and count <> 0 and price <> 0
  group by store_from_id, spare_spare_id;

  OPEN ycur  
  FETCH NEXT FROM ycur   
      INTO @v_mat_id, @v_emp_id, @v_act_date,@v_cnt
  WHILE @@FETCH_STATUS = 0  
  BEGIN 

        SET @v_chk = (SELECT COUNT(*) from Spare_parts_Current WHERE spare_spare_id = @v_emp_id AND  store_store_id = @v_mat_id);
	  IF (@v_chk = 0)
	  BEGIN
	    INSERT INTO Spare_parts_Current (spare_spare_id, date, count, price, store_store_id,nds)
		    VALUES (@v_emp_id , 0, 0, 0, @v_mat_id,0);   
      END;

      SET @v_rss = (SELECT [count] FROM Spare_parts_Current WHERE spare_spare_id = @v_emp_id AND  store_store_id = @v_mat_id);
      IF (@v_rss != @v_cnt)
      UPDATE Spare_parts_Current set 
	     date = @v_act_date,
		[count] = [count] - @v_cnt
      WHERE spare_spare_id = @v_emp_id
	      AND  store_store_id = @v_mat_id;
      ELSE 
		 DELETE FROM Spare_parts_Current WHERE spare_spare_id = @v_emp_id AND  store_store_id = @v_mat_id;

	  FETCH NEXT FROM ycur   
      INTO @v_mat_id, @v_emp_id, @v_act_date,@v_cnt
  END
  CLOSE ycur;
  deallocate ycur;
END;



