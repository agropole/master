INSERT [dbo].[Dictionary_column] ([id], [id_dictionary], [id_dictionary_column_type], [field], [displayName], [width]) VALUES (1, 1, 2, N'tpsal_id', N'', N'0%')
INSERT [dbo].[Dictionary_column] ([id], [id_dictionary], [id_dictionary_column_type], [field], [displayName], [width]) VALUES (2, 1, 1, N'name', N'Название', N'50%')
INSERT [dbo].[Dictionary_column] ([id], [id_dictionary], [id_dictionary_column_type], [field], [displayName], [width]) VALUES (3, 1, 1, N'short_name', N'Сокращение', N'45%')
INSERT [dbo].[Dictionary_column] ([id], [id_dictionary], [id_dictionary_column_type], [field], [displayName], [width]) VALUES (4, 2, 2, N'id', N'', N'0%')
INSERT [dbo].[Dictionary_column] ([id], [id_dictionary], [id_dictionary_column_type], [field], [displayName], [width]) VALUES (5, 2, 1, N'name', N'Название', N'30%')
INSERT [dbo].[Dictionary_column] ([id], [id_dictionary], [id_dictionary_column_type], [field], [displayName], [width]) VALUES (6, 2, 1, N'code', N'Код', N'25%')
INSERT [dbo].[Dictionary_column] ([id], [id_dictionary], [id_dictionary_column_type], [field], [displayName], [width]) VALUES (7, 3, 2, N'id', N'', N'0%')
INSERT [dbo].[Dictionary_column] ([id], [id_dictionary], [id_dictionary_column_type], [field], [displayName], [width]) VALUES (8, 3, 1, N'vat_value', N'Ставка, %', N'95%')
INSERT [dbo].[Dictionary_column] ([id], [id_dictionary], [id_dictionary_column_type], [field], [displayName], [width]) VALUES (9, 4, 2, N'id', N'', N'0%')
INSERT [dbo].[Dictionary_column] ([id], [id_dictionary], [id_dictionary_column_type], [field], [displayName], [width]) VALUES (10, 4, 1, N'name', N'Название', N'30%')
INSERT [dbo].[Dictionary_column] ([id], [id_dictionary], [id_dictionary_column_type], [field], [displayName], [width]) VALUES (11, 4, 1, N'balance', N'Текущий баланс', N'10%')
INSERT [dbo].[Dictionary_column] ([id], [id_dictionary], [id_dictionary_column_type], [field], [displayName], [width]) VALUES (12, 4, 1, N'current_price', N'Текущая цена', N'15%')
INSERT [dbo].[Dictionary_column] ([id], [id_dictionary], [id_dictionary_column_type], [field], [displayName], [width]) VALUES (13, 4, 3, N'tpsal_tpsal_id', N'Единица измерения', N'10%')
INSERT [dbo].[Dictionary_column] ([id], [id_dictionary], [id_dictionary_column_type], [field], [displayName], [width]) VALUES (14, 5, 2, N'type_id', N'', N'0%')
INSERT [dbo].[Dictionary_column] ([id], [id_dictionary], [id_dictionary_column_type], [field], [displayName], [width]) VALUES (15, 5, 1, N'name', N'Название', N'35%')
INSERT [dbo].[Dictionary_column] ([id], [id_dictionary], [id_dictionary_column_type], [field], [displayName], [width]) VALUES (16, 5, 1, N'code', N'Код', N'20%')
INSERT [dbo].[Dictionary_column] ([id], [id_dictionary], [id_dictionary_column_type], [field], [displayName], [width]) VALUES (17, 5, 1, N'description', N'Описание', N'40%')
INSERT [dbo].[Dictionary_column] ([id], [id_dictionary], [id_dictionary_column_type], [field], [displayName], [width]) VALUES (18, 6, 2, N'mat_id', N'', N'0%')
INSERT [dbo].[Dictionary_column] ([id], [id_dictionary], [id_dictionary_column_type], [field], [displayName], [width]) VALUES (19, 6, 1, N'name', N'Название', N'30%')
INSERT [dbo].[Dictionary_column] ([id], [id_dictionary], [id_dictionary_column_type], [field], [displayName], [width]) VALUES (20, 6, 3, N'mat_type_id', N'Тип', N'30%')
INSERT [dbo].[Dictionary_column] ([id], [id_dictionary], [id_dictionary_column_type], [field], [displayName], [width]) VALUES (21, 6, 1, N'code', N'Код', N'35%')
INSERT [dbo].[Dictionary_column] ([id], [id_dictionary], [id_dictionary_column_type], [field], [displayName], [width]) VALUES (25, 7, 2, N'plant_id', N'', N'0%')
INSERT [dbo].[Dictionary_column] ([id], [id_dictionary], [id_dictionary_column_type], [field], [displayName], [width]) VALUES (26, 7, 1, N'name', N'Название', N'15%')
INSERT [dbo].[Dictionary_column] ([id], [id_dictionary], [id_dictionary_column_type], [field], [displayName], [width]) VALUES (27, 7, 1, N'short_name', N'Сокр. название', N'15%')
INSERT [dbo].[Dictionary_column] ([id], [id_dictionary], [id_dictionary_column_type], [field], [displayName], [width]) VALUES (28, 7, 1, N'description', N'Описание', N'20%')
INSERT [dbo].[Dictionary_column] ([id], [id_dictionary], [id_dictionary_column_type], [field], [displayName], [width]) VALUES (29, 7, 3, N'tpplant_tpplant_id', N'Тип культуры', N'15%')
INSERT [dbo].[Dictionary_column] ([id], [id_dictionary], [id_dictionary_column_type], [field], [displayName], [width]) VALUES (30, 8, 2, N'fiel_id', N'', N'0%')
INSERT [dbo].[Dictionary_column] ([id], [id_dictionary], [id_dictionary_column_type], [field], [displayName], [width]) VALUES (31, 8, 1, N'name', N'Название', N'25%')
INSERT [dbo].[Dictionary_column] ([id], [id_dictionary], [id_dictionary_column_type], [field], [displayName], [width]) VALUES (32, 8, 3, N'org_org_id', N'Организация', N'15%')
INSERT [dbo].[Dictionary_column] ([id], [id_dictionary], [id_dictionary_column_type], [field], [displayName], [width]) VALUES (33, 8, 1, N'type', N'Тип', N'5%')
INSERT [dbo].[Dictionary_column] ([id], [id_dictionary], [id_dictionary_column_type], [field], [displayName], [width]) VALUES (34, 8, 1, N'code', N'Код интеграции', N'30%')
INSERT [dbo].[Dictionary_column] ([id], [id_dictionary], [id_dictionary_column_type], [field], [displayName], [width]) VALUES (35, 8, 4, N'coord', N'Площадь,га/координаты', N'20%')
INSERT [dbo].[Dictionary_column] ([id], [id_dictionary], [id_dictionary_column_type], [field], [displayName], [width]) VALUES (36, 9, 2, N'mat_id', N'', N'0%')
INSERT [dbo].[Dictionary_column] ([id], [id_dictionary], [id_dictionary_column_type], [field], [displayName], [width]) VALUES (37, 9, 1, N'name', N'Название', N'25%')
INSERT [dbo].[Dictionary_column] ([id], [id_dictionary], [id_dictionary_column_type], [field], [displayName], [width]) VALUES (38, 9, 1, N'code', N'Код', N'30%')
INSERT [dbo].[Dictionary_column] ([id], [id_dictionary], [id_dictionary_column_type], [field], [displayName], [width]) VALUES (39, 9, 1, N'balance', N'Баланс', N'20%')
INSERT [dbo].[Dictionary_column] ([id], [id_dictionary], [id_dictionary_column_type], [field], [displayName], [width]) VALUES (40, 9, 1, N'current_price', N'Цена', N'20%')
INSERT [dbo].[Dictionary_column] ([id], [id_dictionary], [id_dictionary_column_type], [field], [displayName], [width]) VALUES (41, 10, 2, N'mat_id', N'', N'0%')
INSERT [dbo].[Dictionary_column] ([id], [id_dictionary], [id_dictionary_column_type], [field], [displayName], [width]) VALUES (42, 10, 1, N'name', N'Название', N'25%')
INSERT [dbo].[Dictionary_column] ([id], [id_dictionary], [id_dictionary_column_type], [field], [displayName], [width]) VALUES (43, 10, 1, N'code', N'Код', N'30%')
INSERT [dbo].[Dictionary_column] ([id], [id_dictionary], [id_dictionary_column_type], [field], [displayName], [width]) VALUES (44, 10, 1, N'balance', N'Баланс', N'20%')
INSERT [dbo].[Dictionary_column] ([id], [id_dictionary], [id_dictionary_column_type], [field], [displayName], [width]) VALUES (45, 10, 1, N'current_price', N'Цена', N'20%')
INSERT [dbo].[Dictionary_column] ([id], [id_dictionary], [id_dictionary_column_type], [field], [displayName], [width]) VALUES (46, 11, 2, N'mat_id', N'', N'0%')
INSERT [dbo].[Dictionary_column] ([id], [id_dictionary], [id_dictionary_column_type], [field], [displayName], [width]) VALUES (47, 11, 1, N'name', N'Название', N'25%')
INSERT [dbo].[Dictionary_column] ([id], [id_dictionary], [id_dictionary_column_type], [field], [displayName], [width]) VALUES (48, 11, 1, N'code', N'Код', N'30%')
INSERT [dbo].[Dictionary_column] ([id], [id_dictionary], [id_dictionary_column_type], [field], [displayName], [width]) VALUES (49, 11, 1, N'balance', N'Баланс', N'20%')
INSERT [dbo].[Dictionary_column] ([id], [id_dictionary], [id_dictionary_column_type], [field], [displayName], [width]) VALUES (50, 11, 1, N'current_price', N'Цена', N'20%')
INSERT [dbo].[Dictionary_column] ([id], [id_dictionary], [id_dictionary_column_type], [field], [displayName], [width]) VALUES (51, 12, 2, N'gar_id', N'', N'0%')
INSERT [dbo].[Dictionary_column] ([id], [id_dictionary], [id_dictionary_column_type], [field], [displayName], [width]) VALUES (52, 12, 1, N'name', N'Название', N'30%')
INSERT [dbo].[Dictionary_column] ([id], [id_dictionary], [id_dictionary_column_type], [field], [displayName], [width]) VALUES (53, 12, 1, N'description', N'Описание', N'35%')
INSERT [dbo].[Dictionary_column] ([id], [id_dictionary], [id_dictionary_column_type], [field], [displayName], [width]) VALUES (54, 12, 3, N'org_org_id', N'Организация', N'30%')
INSERT [dbo].[Dictionary_column] ([id], [id_dictionary], [id_dictionary_column_type], [field], [displayName], [width]) VALUES (59, 14, 2, N'trakt_id', N'', N'0%')
INSERT [dbo].[Dictionary_column] ([id], [id_dictionary], [id_dictionary_column_type], [field], [displayName], [width]) VALUES (60, 14, 1, N'name', N'Название', N'10%')
INSERT [dbo].[Dictionary_column] ([id], [id_dictionary], [id_dictionary_column_type], [field], [displayName], [width]) VALUES (61, 14, 1, N'model', N'Модель', N'10%')
INSERT [dbo].[Dictionary_column] ([id], [id_dictionary], [id_dictionary_column_type], [field], [displayName], [width]) VALUES (62, 14, 1, N'reg_num', N'Рег.знак', N'10%')
INSERT [dbo].[Dictionary_column] ([id], [id_dictionary], [id_dictionary_column_type], [field], [displayName], [width]) VALUES (63, 14, 1, N'width', N'Ширина,м', N'10%')
INSERT [dbo].[Dictionary_column] ([id], [id_dictionary], [id_dictionary_column_type], [field], [displayName], [width]) VALUES (64, 14, 1, N'track_id', N'Трекер', N'10%')
INSERT [dbo].[Dictionary_column] ([id], [id_dictionary], [id_dictionary_column_type], [field], [displayName], [width]) VALUES (65, 14, 3, N'gar_gar_id', N'Гараж', N'10%')
INSERT [dbo].[Dictionary_column] ([id], [id_dictionary], [id_dictionary_column_type], [field], [displayName], [width]) VALUES (66, 14, 3, N'tptrak_tptrak_id', N'Гр.техники', N'8%')
INSERT [dbo].[Dictionary_column] ([id], [id_dictionary], [id_dictionary_column_type], [field], [displayName], [width]) VALUES (67, 15, 2, N'tpequi_id', N'', N'0%')
INSERT [dbo].[Dictionary_column] ([id], [id_dictionary], [id_dictionary_column_type], [field], [displayName], [width]) VALUES (68, 15, 1, N'name', N'Название', N'40%')
INSERT [dbo].[Dictionary_column] ([id], [id_dictionary], [id_dictionary_column_type], [field], [displayName], [width]) VALUES (69, 15, 1, N'description', N'Описание', N'55%')
INSERT [dbo].[Dictionary_column] ([id], [id_dictionary], [id_dictionary_column_type], [field], [displayName], [width]) VALUES (70, 16, 2, N'equi_id', N'', N'0%')
INSERT [dbo].[Dictionary_column] ([id], [id_dictionary], [id_dictionary_column_type], [field], [displayName], [width]) VALUES (71, 16, 1, N'name', N'Название', N'20%')
INSERT [dbo].[Dictionary_column] ([id], [id_dictionary], [id_dictionary_column_type], [field], [displayName], [width]) VALUES (72, 16, 1, N'model', N'Модель', N'10%')
INSERT [dbo].[Dictionary_column] ([id], [id_dictionary], [id_dictionary_column_type], [field], [displayName], [width]) VALUES (73, 16, 1, N'shot_name', N'Кр.название', N'7%')
INSERT [dbo].[Dictionary_column] ([id], [id_dictionary], [id_dictionary_column_type], [field], [displayName], [width]) VALUES (74, 16, 1, N'width', N'Ширина', N'5%')
INSERT [dbo].[Dictionary_column] ([id], [id_dictionary], [id_dictionary_column_type], [field], [displayName], [width]) VALUES (75, 16, 3, N'tpequi_tpequi_id', N'Тип оборудования', N'15%')
INSERT [dbo].[Dictionary_column] ([id], [id_dictionary], [id_dictionary_column_type], [field], [displayName], [width]) VALUES (76, 17, 2, N'tptas_id', N'', N'0%')
INSERT [dbo].[Dictionary_column] ([id], [id_dictionary], [id_dictionary_column_type], [field], [displayName], [width]) VALUES (77, 17, 1, N'name', N'Название', N'24%')
INSERT [dbo].[Dictionary_column] ([id], [id_dictionary], [id_dictionary_column_type], [field], [displayName], [width]) VALUES (78, 17, 3, N'auxiliary_rate', N'Группа', N'10%')
INSERT [dbo].[Dictionary_column] ([id], [id_dictionary], [id_dictionary_column_type], [field], [displayName], [width]) VALUES (84, 17, 1, N'code', N'Код', N'20%')
INSERT [dbo].[Dictionary_column] ([id], [id_dictionary], [id_dictionary_column_type], [field], [displayName], [width]) VALUES (124, 17, 1, N'rate_piecework', N'Сдельная расценка', N'8%')
INSERT [dbo].[Dictionary_column] ([id], [id_dictionary], [id_dictionary_column_type], [field], [displayName], [width]) VALUES (134, 17, 1, N'description', N'Описание', N'21%')
INSERT [dbo].[Dictionary_column] ([id], [id_dictionary], [id_dictionary_column_type], [field], [displayName], [width]) VALUES (169, 17, 3, N'type_fuel_id', N'Этап', N'10%')
INSERT [dbo].[Dictionary_column] ([id], [id_dictionary], [id_dictionary_column_type], [field], [displayName], [width]) VALUES (170, 17, 5, N'deleted', N' ', N'5%')
INSERT [dbo].[Dictionary_column] ([id], [id_dictionary], [id_dictionary_column_type], [field], [displayName], [width]) VALUES (79, 18, 2, N'tpsort_id', N'', N'0%')
INSERT [dbo].[Dictionary_column] ([id], [id_dictionary], [id_dictionary_column_type], [field], [displayName], [width]) VALUES (80, 18, 1, N'name', N'Название', N'45%')
INSERT [dbo].[Dictionary_column] ([id], [id_dictionary], [id_dictionary_column_type], [field], [displayName], [width]) VALUES (81, 18, 3, N'plant_plant_id', N'Культура', N'50%')
INSERT [dbo].[Dictionary_column] ([id], [id_dictionary], [id_dictionary_column_type], [field], [displayName], [width]) VALUES (83, 16, 1, N'code', N'Код', N'30%')
INSERT [dbo].[Dictionary_column] ([id], [id_dictionary], [id_dictionary_column_type], [field], [displayName], [width]) VALUES (85, 4, 1, N'code', N'Код', N'30%')
INSERT [dbo].[Dictionary_column] ([id], [id_dictionary], [id_dictionary_column_type], [field], [displayName], [width]) VALUES (86, 7, 1, N'code', N'Код', N'30%')
INSERT [dbo].[Dictionary_column] ([id], [id_dictionary], [id_dictionary_column_type], [field], [displayName], [width]) VALUES (87, 19, 2, N'id', N'', N'0%')
INSERT [dbo].[Dictionary_column] ([id], [id_dictionary], [id_dictionary_column_type], [field], [displayName], [width]) VALUES (88, 19, 1, N'name', N'Название', N'20%')
INSERT [dbo].[Dictionary_column] ([id], [id_dictionary], [id_dictionary_column_type], [field], [displayName], [width]) VALUES (89, 19, 3, N'tpsal_tpsal_id', N'Ед.', N'5%')
INSERT [dbo].[Dictionary_column] ([id], [id_dictionary], [id_dictionary_column_type], [field], [displayName], [width]) VALUES (90, 19, 1, N'standard_value', N'Эталон. значение', N'10%')
INSERT [dbo].[Dictionary_column] ([id], [id_dictionary], [id_dictionary_column_type], [field], [displayName], [width]) VALUES (91, 19, 1, N'deviation_limit', N'Отклонение 1', N'10%')
INSERT [dbo].[Dictionary_column] ([id], [id_dictionary], [id_dictionary_column_type], [field], [displayName], [width]) VALUES (92, 19, 1, N'rate', N'Оценка 1', N'10%')
INSERT [dbo].[Dictionary_column] ([id], [id_dictionary], [id_dictionary_column_type], [field], [displayName], [width]) VALUES (93, 19, 1, N'deviation_limit2', N'Отклонение 2', N'10%')
INSERT [dbo].[Dictionary_column] ([id], [id_dictionary], [id_dictionary_column_type], [field], [displayName], [width]) VALUES (94, 19, 1, N'rate2', N'Оценка 2', N'10%')
INSERT [dbo].[Dictionary_column] ([id], [id_dictionary], [id_dictionary_column_type], [field], [displayName], [width]) VALUES (95, 19, 1, N'deviation_limit3', N'Отклонение 3', N'10%')
INSERT [dbo].[Dictionary_column] ([id], [id_dictionary], [id_dictionary_column_type], [field], [displayName], [width]) VALUES (96, 19, 1, N'rate3', N'Оценка 3', N'10%')
INSERT [dbo].[Dictionary_column] ([id], [id_dictionary], [id_dictionary_column_type], [field], [displayName], [width]) VALUES (97, 20, 2, N'emp_id', N'', N'0%')
INSERT [dbo].[Dictionary_column] ([id], [id_dictionary], [id_dictionary_column_type], [field], [displayName], [width]) VALUES (98, 20, 1, N'second_name', N'Фамилия', N'10%')
INSERT [dbo].[Dictionary_column] ([id], [id_dictionary], [id_dictionary_column_type], [field], [displayName], [width]) VALUES (99, 20, 1, N'first_name', N'Имя', N'10%')
INSERT [dbo].[Dictionary_column] ([id], [id_dictionary], [id_dictionary_column_type], [field], [displayName], [width]) VALUES (100, 20, 1, N'middle_name', N'Отчество', N'10%')
INSERT [dbo].[Dictionary_column] ([id], [id_dictionary], [id_dictionary_column_type], [field], [displayName], [width]) VALUES (101, 20, 1, N'short_name', N'ФИО', N'10%')
INSERT [dbo].[Dictionary_column] ([id], [id_dictionary], [id_dictionary_column_type], [field], [displayName], [width]) VALUES (102, 20, 3, N'pst_pst_id', N'Должность', N'10%')
INSERT [dbo].[Dictionary_column] ([id], [id_dictionary], [id_dictionary_column_type], [field], [displayName], [width]) VALUES (103, 20, 1, N'driver_license', N'№ вод.прав', N'10%')
INSERT [dbo].[Dictionary_column] ([id], [id_dictionary], [id_dictionary_column_type], [field], [displayName], [width]) VALUES (104, 20, 1, N'code', N'Код', N'20%')
GO
INSERT [dbo].[Dictionary_column] ([id], [id_dictionary], [id_dictionary_column_type], [field], [displayName], [width]) VALUES (105, 20, 3, N'org_org_id', N'Организация', N'10%')
INSERT [dbo].[Dictionary_column] ([id], [id_dictionary], [id_dictionary_column_type], [field], [displayName], [width]) VALUES (106, 21, 2, N'id', N'', N'0%')
INSERT [dbo].[Dictionary_column] ([id], [id_dictionary], [id_dictionary_column_type], [field], [displayName], [width]) VALUES (107, 21, 1, N'name', N'Название', N'95%')
INSERT [dbo].[Dictionary_column] ([id], [id_dictionary], [id_dictionary_column_type], [field], [displayName], [width]) VALUES (108, 22, 2, N'agg_id', N'', N'0%')
INSERT [dbo].[Dictionary_column] ([id], [id_dictionary], [id_dictionary_column_type], [field], [displayName], [width]) VALUES (109, 22, 1, N'name', N'Название', N'20%')
INSERT [dbo].[Dictionary_column] ([id], [id_dictionary], [id_dictionary_column_type], [field], [displayName], [width]) VALUES (110, 22, 1, N'agg_code', N'agg_code', N'25%')
INSERT [dbo].[Dictionary_column] ([id], [id_dictionary], [id_dictionary_column_type], [field], [displayName], [width]) VALUES (111, 22, 1, N'trak_code', N'trak_code', N'25%')
INSERT [dbo].[Dictionary_column] ([id], [id_dictionary], [id_dictionary_column_type], [field], [displayName], [width]) VALUES (112, 22, 1, N'equi_code', N'equi_code', N'25%')
INSERT [dbo].[Dictionary_column] ([id], [id_dictionary], [id_dictionary_column_type], [field], [displayName], [width]) VALUES (113, 14, 3, N'id_type_way_list', N'Путевой лист', N'10%')
INSERT [dbo].[Dictionary_column] ([id], [id_dictionary], [id_dictionary_column_type], [field], [displayName], [width]) VALUES (114, 23, 2, N'trakt_id', N' ', N'0%')
INSERT [dbo].[Dictionary_column] ([id], [id_dictionary], [id_dictionary_column_type], [field], [displayName], [width]) VALUES (115, 23, 1, N'name', N'Название', N'9%')
INSERT [dbo].[Dictionary_column] ([id], [id_dictionary], [id_dictionary_column_type], [field], [displayName], [width]) VALUES (116, 23, 1, N'model', N'Модель', N'9%')
INSERT [dbo].[Dictionary_column] ([id], [id_dictionary], [id_dictionary_column_type], [field], [displayName], [width]) VALUES (117, 23, 1, N'reg_num', N'Рег.знак', N'7%')
INSERT [dbo].[Dictionary_column] ([id], [id_dictionary], [id_dictionary_column_type], [field], [displayName], [width]) VALUES (118, 23, 1, N'code', N'code', N'20%')
INSERT [dbo].[Dictionary_column] ([id], [id_dictionary], [id_dictionary_column_type], [field], [displayName], [width]) VALUES (119, 23, 1, N'int_id', N'int_id', N'25%')
INSERT [dbo].[Dictionary_column] ([id], [id_dictionary], [id_dictionary_column_type], [field], [displayName], [width]) VALUES (120, 23, 1, N'im_code', N'im_code', N'25%')
INSERT [dbo].[Dictionary_column] ([id], [id_dictionary], [id_dictionary_column_type], [field], [displayName], [width]) VALUES (126, 16, 1, N'reg_num', N'Номер', N'8%')
INSERT [dbo].[Dictionary_column] ([id], [id_dictionary], [id_dictionary_column_type], [field], [displayName], [width]) VALUES (127, 16, 5, N'delete', N' ', N'5%')
INSERT [dbo].[Dictionary_column] ([id], [id_dictionary], [id_dictionary_column_type], [field], [displayName], [width]) VALUES (128, 24, 2, N'id', N' ', N'0%')
INSERT [dbo].[Dictionary_column] ([id], [id_dictionary], [id_dictionary_column_type], [field], [displayName], [width]) VALUES (129, 24, 1, N'name', N'Название', N'95%')
INSERT [dbo].[Dictionary_column] ([id], [id_dictionary], [id_dictionary_column_type], [field], [displayName], [width]) VALUES (130, 24, 5, N'deleted', N' ', N'5%')

INSERT [dbo].[Dictionary_column] ([id], [id_dictionary], [id_dictionary_column_type], [field], [displayName], [width]) VALUES (132, 14, 3, N'emp_emp_id', N'Сотрудник', N'7%')

INSERT [dbo].[Dictionary_column] ([id], [id_dictionary], [id_dictionary_column_type], [field], [displayName], [width]) VALUES (133, 23, 5, N'deleted', N' ', N'5%')
INSERT [dbo].[Dictionary_column] ([id], [id_dictionary], [id_dictionary_column_type], [field], [displayName], [width]) VALUES (135, 18, 5, N'deleted', N' ', N'5%')
INSERT [dbo].[Dictionary_column] ([id], [id_dictionary], [id_dictionary_column_type], [field], [displayName], [width]) VALUES (136, 19, 5, N'deleted', N' ', N'5%')
INSERT [dbo].[Dictionary_column] ([id], [id_dictionary], [id_dictionary_column_type], [field], [displayName], [width]) VALUES (137, 20, 5, N'deleted', N' ', N'5%')
INSERT [dbo].[Dictionary_column] ([id], [id_dictionary], [id_dictionary_column_type], [field], [displayName], [width]) VALUES (139, 1, 5, N'deleted', N' ', N'5%')
INSERT [dbo].[Dictionary_column] ([id], [id_dictionary], [id_dictionary_column_type], [field], [displayName], [width]) VALUES (140, 2, 1, N'inn', N' ИНН', N'20%')
INSERT [dbo].[Dictionary_column] ([id], [id_dictionary], [id_dictionary_column_type], [field], [displayName], [width]) VALUES (141, 3, 5, N'deleted', N' ', N'5%')
INSERT [dbo].[Dictionary_column] ([id], [id_dictionary], [id_dictionary_column_type], [field], [displayName], [width]) VALUES (142, 4, 5, N'deleted', N' ', N'5%')
INSERT [dbo].[Dictionary_column] ([id], [id_dictionary], [id_dictionary_column_type], [field], [displayName], [width]) VALUES (143, 5, 5, N'deleted', N' ', N'5%')
INSERT [dbo].[Dictionary_column] ([id], [id_dictionary], [id_dictionary_column_type], [field], [displayName], [width]) VALUES (144, 6, 5, N'deleted', N' ', N'5%')
INSERT [dbo].[Dictionary_column] ([id], [id_dictionary], [id_dictionary_column_type], [field], [displayName], [width]) VALUES (145, 7, 5, N'deleted', N' ', N'5%')
INSERT [dbo].[Dictionary_column] ([id], [id_dictionary], [id_dictionary_column_type], [field], [displayName], [width]) VALUES (146, 8, 5, N'deleted', N' ', N'5%')
INSERT [dbo].[Dictionary_column] ([id], [id_dictionary], [id_dictionary_column_type], [field], [displayName], [width]) VALUES (147, 9, 5, N'deleted', N' ', N'5%')
INSERT [dbo].[Dictionary_column] ([id], [id_dictionary], [id_dictionary_column_type], [field], [displayName], [width]) VALUES (148, 10, 5, N'deleted', N' ', N'5%')
INSERT [dbo].[Dictionary_column] ([id], [id_dictionary], [id_dictionary_column_type], [field], [displayName], [width]) VALUES (149, 11, 5, N'deleted', N' ', N'5%')
INSERT [dbo].[Dictionary_column] ([id], [id_dictionary], [id_dictionary_column_type], [field], [displayName], [width]) VALUES (150, 12, 5, N'deleted', N' ', N'5%')
INSERT [dbo].[Dictionary_column] ([id], [id_dictionary], [id_dictionary_column_type], [field], [displayName], [width]) VALUES (152, 14, 3, N'tpf_tpf_id', N'Тип топлива', N'10%')
INSERT [dbo].[Dictionary_column] ([id], [id_dictionary], [id_dictionary_column_type], [field], [displayName], [width]) VALUES (153, 15, 5, N'deleted', N' ', N'5%')
INSERT [dbo].[Dictionary_column] ([id], [id_dictionary], [id_dictionary_column_type], [field], [displayName], [width]) VALUES (154, 21, 5, N'deleted', N' ', N'5%')
INSERT [dbo].[Dictionary_column] ([id], [id_dictionary], [id_dictionary_column_type], [field], [displayName], [width]) VALUES (155, 22, 5, N'deleted', N' ', N'5%')
INSERT [dbo].[Dictionary_column] ([id], [id_dictionary], [id_dictionary_column_type], [field], [displayName], [width]) VALUES (156, 25, 2, N'grtrak_id', N' ', N'0%')
INSERT [dbo].[Dictionary_column] ([id], [id_dictionary], [id_dictionary_column_type], [field], [displayName], [width]) VALUES (157, 25, 1, N'name', N'Название', N'95%')
INSERT [dbo].[Dictionary_column] ([id], [id_dictionary], [id_dictionary_column_type], [field], [displayName], [width]) VALUES (158, 25, 5, N'deleted', N' ', N'5%')
INSERT [dbo].[Dictionary_column] ([id], [id_dictionary], [id_dictionary_column_type], [field], [displayName], [width]) VALUES (159, 26, 2, N'salemp_id', N'', N'0%')
INSERT [dbo].[Dictionary_column] ([id], [id_dictionary], [id_dictionary_column_type], [field], [displayName], [width]) VALUES (160, 26, 1, N'second_name', N'Фамилия', N'20%')
INSERT [dbo].[Dictionary_column] ([id], [id_dictionary], [id_dictionary_column_type], [field], [displayName], [width]) VALUES (161, 26, 1, N'first_name', N'Имя', N'15%')
INSERT [dbo].[Dictionary_column] ([id], [id_dictionary], [id_dictionary_column_type], [field], [displayName], [width]) VALUES (162, 26, 1, N'middle_name', N'Отчество', N'20%')
INSERT [dbo].[Dictionary_column] ([id], [id_dictionary], [id_dictionary_column_type], [field], [displayName], [width]) VALUES (163, 26, 1, N'short_name', N'ФИО', N'20%')
INSERT [dbo].[Dictionary_column] ([id], [id_dictionary], [id_dictionary_column_type], [field], [displayName], [width]) VALUES (164, 26, 1, N'in_code', N'Код', N'20%')
INSERT [dbo].[Dictionary_column] ([id], [id_dictionary], [id_dictionary_column_type], [field], [displayName], [width]) VALUES (165, 26, 5, N'deleted', N'', N'5%')
INSERT [dbo].[Dictionary_column] ([id], [id_dictionary], [id_dictionary_column_type], [field], [displayName], [width]) VALUES (166, 27, 2, N'grtask_id', N' ', N'0%')
INSERT [dbo].[Dictionary_column] ([id], [id_dictionary], [id_dictionary_column_type], [field], [displayName], [width]) VALUES (167, 27, 1, N'name', N'Название', N'95%')
INSERT [dbo].[Dictionary_column] ([id], [id_dictionary], [id_dictionary_column_type], [field], [displayName], [width]) VALUES (168, 27, 5, N'deleted', N' ', N'5%')
INSERT [dbo].[Dictionary_column] ([id], [id_dictionary], [id_dictionary_column_type], [field], [displayName], [width]) VALUES (171, 28, 2, N'stages_id', N' ', N'0%')
INSERT [dbo].[Dictionary_column] ([id], [id_dictionary], [id_dictionary_column_type], [field], [displayName], [width]) VALUES (172, 28, 1, N'name', N'Название', N'95%')
INSERT [dbo].[Dictionary_column] ([id], [id_dictionary], [id_dictionary_column_type], [field], [displayName], [width]) VALUES (173, 28, 5, N'deleted', N' ', N'5%')
INSERT [dbo].[Dictionary_column] ([id], [id_dictionary], [id_dictionary_column_type], [field], [displayName], [width]) VALUES (174, 29, 2, N'store_id', N' ', N'0%')
INSERT [dbo].[Dictionary_column] ([id], [id_dictionary], [id_dictionary_column_type], [field], [displayName], [width]) VALUES (175, 29, 1, N'name', N'Название', N'95%')
INSERT [dbo].[Dictionary_column] ([id], [id_dictionary], [id_dictionary_column_type], [field], [displayName], [width]) VALUES (176, 29, 5, N'deleted', N' ', N'5%')
INSERT [dbo].[Dictionary_column] ([id], [id_dictionary], [id_dictionary_column_type], [field], [displayName], [width]) VALUES (177, 30, 2, N'spare_id', N' ', N'0%')
INSERT [dbo].[Dictionary_column] ([id], [id_dictionary], [id_dictionary_column_type], [field], [displayName], [width]) VALUES (178, 30, 1, N'vendor_code', N'Артикул', N'30%')
INSERT [dbo].[Dictionary_column] ([id], [id_dictionary], [id_dictionary_column_type], [field], [displayName], [width]) VALUES (179, 30, 1, N'name', N'Наименование', N'20%')
INSERT [dbo].[Dictionary_column] ([id], [id_dictionary], [id_dictionary_column_type], [field], [displayName], [width]) VALUES (180, 30, 1, N'manufacturer', N'Производитель', N'20%')
INSERT [dbo].[Dictionary_column] ([id], [id_dictionary], [id_dictionary_column_type], [field], [displayName], [width]) VALUES (181, 30, 1, N'code', N'Код', N'25%')
INSERT [dbo].[Dictionary_column] ([id], [id_dictionary], [id_dictionary_column_type], [field], [displayName], [width]) VALUES (182, 30, 5, N'deleted', N' ', N'5%')
INSERT [dbo].[Dictionary_column] ([id], [id_dictionary], [id_dictionary_column_type], [field], [displayName], [width]) VALUES (183, 2, 1, N'kpp', N'КПП', N'20%')
INSERT [dbo].[Dictionary_column] ([id], [id_dictionary], [id_dictionary_column_type], [field], [displayName], [width]) VALUES (184, 2, 5, N'deleted', N'', N'5%')
INSERT [dbo].[Dictionary_column] ([id], [id_dictionary], [id_dictionary_column_type], [field], [displayName], [width]) VALUES (185, 14, 5, N'deleted', N'', N'5%')
