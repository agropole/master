alter table Tasks add type_price numeric(2,0)
GO
alter table Type_salary_doc add short_name varchar(50)
GO
update Type_salary_doc set short_name ='Пол. день' where tpsal_id = 1
GO 
update Type_salary_doc set short_name ='Га' where tpsal_id = 2 
GO
update Type_salary_doc set short_name ='Тонна' where tpsal_id = 3 
GO

alter table Refuelings alter column emp_emp_id int
GO