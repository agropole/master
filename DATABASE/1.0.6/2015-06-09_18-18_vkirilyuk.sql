create table Type_sorts_plants(
 tpsort_id int not NULL,
 plant_plant_id int NULL,
 name varchar(150) NULL,
 primary key(tpsort_id)
);
GO
create table Sorts_plants_data(
 sortdat_id int not NULL IDENTITY (1,1),
 tpsort_tpsort_id int NULL,
 fielplan_fielplan_id int NULL,
 value_plan numeric(12,2) NULL,
 value_fact  numeric(12,2) NULL,
 primary key(sortdat_id)
);
GO
create table Indicators_sorts_plants(
 indsort_id int not NULL,
 name varchar(150) NULL,
 primary key(indsort_id)
);
GO
create table Ind_sorts_plants_data(
 inddata_id int not NULL IDENTITY (1,1),
 value_plan numeric(12,2) NULL,
 value_fact  numeric(12,2) NULL,
 indsort_indsort_id int NULL,
 sortdat_sortdat_id int NULL,
 primary key(inddata_id)
);
GO

alter table Type_sorts_plants add constraint fk_Type_sorts_plants_ref_Plants
foreign key (plant_plant_id)
references Plants(plant_id)
on update no action
on delete no action
GO

alter table Sorts_plants_data add constraint fk_Values_sorts_plants_ref_Type_sorts_plants
foreign key (tpsort_tpsort_id)
references Type_sorts_plants(tpsort_id)
on update no action
on delete no action
GO

alter table Sorts_plants_data add constraint fk_Values_sorts_plants_ref_Fields_to_plants
foreign key (fielplan_fielplan_id)
references Fields_to_plants(fielplan_id)
on update no action
on delete no action
GO

alter table Ind_sorts_plants_data add constraint fk_Ind_sorts_plants_data_ref_Indicators_sorts_plants
foreign key (indsort_indsort_id)
references Indicators_sorts_plants(indsort_id)
on update no action
on delete no action
GO

alter table Ind_sorts_plants_data add constraint fk_Ind_sorts_plants_data_ref_Sorts_plants_data
foreign key (sortdat_sortdat_id)
references Sorts_plants_data(sortdat_id)
on update no action
on delete no action
GO

