CREATE TABLE [dbo].[Type_Tasks_to_Plants](
	[tptp_id] [int] IDENTITY(1,1) NOT NULL,
	[tptas_tptas_id] [int],
	[plant_plant_id] [int] NOT NULL,
	[code] [varchar](50) NOT NULL,
	[name] [varchar](100)    
);

insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (6,'b5d02ced-b2a5-11e4-80c1-002590ebf8cd','�����');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (6,'7aa604ff-b2aa-11e4-80c1-002590ebf8cd','�������� ����������� ���������');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (6,'4d640ccd-b36d-11e4-80c1-002590ebf8cd','�������� ���');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (6,'6e27e50c-b36d-11e4-80c1-002590ebf8cd','����� ��������� � ����');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (6,'d456541c-b36d-11e4-80c1-002590ebf8cd','�������� ������');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (6,'1a747f10-b36e-11e4-80c1-002590ebf8cd','�������� ��� � ����');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (6,'25f52bfd-b36e-11e4-80c1-002590ebf8cd','�������� ��� � ���������');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (6,'3d89ce9a-b36e-11e4-80c1-002590ebf8cd','�������� ������������� ����� � ������������� �������� ��������');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (6,'576663d7-b36e-11e4-80c1-002590ebf8cd','����������� ����� ����');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (6,'b55f1dec-b36e-11e4-80c1-002590ebf8cd','����������� ��������� ����� ��������');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (6,'c2a7b74b-b36e-11e4-80c1-002590ebf8cd','����������� ��� ����� ��������');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (6,'0220923a-b36f-11e4-80c1-002590ebf8cd','����������-������������ ������ ���');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (6,'5aa874c9-b36f-11e4-80c1-002590ebf8cd','������ ���� � ����');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (6,'68904dcc-b36f-11e4-80c1-002590ebf8cd','������ ���� � ���');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (6,'8bd08d64-b36f-11e4-80c1-002590ebf8cd','������ ���� �� �������� ��� 1 ���������');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (6,'a75ef5e5-b36f-11e4-80c1-002590ebf8cd','������ ���� �� �������� ��� 2 ����������');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (6,'b3678d51-b36f-11e4-80c1-002590ebf8cd','������ ���');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (6,'c94dd2fd-b36f-11e4-80c1-002590ebf8cd','������������ ����� ������');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (6,'d8f5ed1e-b36f-11e4-80c1-002590ebf8cd','������������ ������');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (6,'f107b079-b36f-11e4-80c1-002590ebf8cd','������');

insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (5,'1f31e02f-b2b6-11e4-80c1-002590ebf8cd','�������� ���');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (5,'5b74f199-b36d-11e4-80c1-002590ebf8cd','����� ��������� � ����');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (5,'b05a9a00-b36d-11e4-80c1-002590ebf8cd','�������� ������');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (5,'b7c48bff-b36d-11e4-80c1-002590ebf8cd','����������');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (5,'cd65fb4b-b36d-11e4-80c1-002590ebf8cd','�������� ��� � ����');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (5,'e366420b-b36d-11e4-80c1-002590ebf8cd','�������� ��� � ���������');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (5,'fdfdc187-b36d-11e4-80c1-002590ebf8cd','�����������');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (5,'1a747f11-b36e-11e4-80c1-002590ebf8cd','����������� ���������');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (5,'3332591f-b36e-11e4-80c1-002590ebf8cd','����������� ����� �����');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (5,'3d89ce9b-b36e-11e4-80c1-002590ebf8cd','����������� ��� ����� ��������');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (5,'576663d6-b36e-11e4-80c1-002590ebf8cd','�������-������������ ������ ��� ');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (5,'7da08454-b36e-11e4-80c1-002590ebf8cd','������ ���� � ����');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (5,'9351eedd-b36e-11e4-80c1-002590ebf8cd','������ ���� � ���');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (5,'bb59e01d-b36e-11e4-80c1-002590ebf8cd','����� � ������������ ��������� ������');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (5,'d6698bee-b36e-11e4-80c1-002590ebf8cd','������');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (5,'e7f21bde-b36e-11e4-80c1-002590ebf8cd','�������� �����');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (5,'cda3f841-d857-11e4-80c4-002590ebf8cd','������������ ����� ������');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (5,'26fde859-d85b-11e4-80c4-002590ebf8cd','����� ����');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (5,'f844cbd7-d85b-11e4-80c4-002590ebf8cd','������ ����');



insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (2,'1532536d-b36f-11e4-80c1-002590ebf8cd','�������� ����������� ���������');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (2,'42d034ef-b36f-11e4-80c1-002590ebf8cd','����� ��������� � ����');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (2,'611347f3-b36f-11e4-80c1-002590ebf8cd','�������� ������');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (2,'6e96fc0d-b36f-11e4-80c1-002590ebf8cd','�������� ��� � ����');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (2,'336f5b8c-b370-11e4-80c1-002590ebf8cd','����������� ��������� ����� ��������');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (2,'64271167-b370-11e4-80c1-002590ebf8cd','�������-������������ ������ ��');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (2,'ab43cac4-b370-11e4-80c1-002590ebf8cd','������ ���� � ����');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (2,'ba1ed92a-b370-11e4-80c1-002590ebf8cd','������ ���');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (2,'c63a82d7-b370-11e4-80c1-002590ebf8cd','�����');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (2,'df9be358-b370-11e4-80c1-002590ebf8cd','������');

insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (21,'336f5b8b-b370-11e4-80c1-002590ebf8cd','�����������');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (21,'57c72004-b370-11e4-80c1-002590ebf8cd','���������� �������');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (21,'64271166-b370-11e4-80c1-002590ebf8cd','�������� ����������� ���������');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (21,'6d219706-b370-11e4-80c1-002590ebf8cd','�������� ���');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (21,'910bd28e-b370-11e4-80c1-002590ebf8cd','���������');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (21,'a2801c4f-b370-11e4-80c1-002590ebf8cd','����� ��������� � ����');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (21,'ba1ed929-b370-11e4-80c1-002590ebf8cd','�������� �����������');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (21,'c63a82d6-b370-11e4-80c1-002590ebf8cd','����������');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (21,'d56dc877-b370-11e4-80c1-002590ebf8cd','�������� ��� � ����');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (21,'f0694584-b370-11e4-80c1-002590ebf8cd','�������� ��� � ���������');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (21,'fbfaa4ab-b370-11e4-80c1-002590ebf8cd','������� ����');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (21,'0450a4e7-b371-11e4-80c1-002590ebf8cd','�����������');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (21,'0450a4e8-b371-11e4-80c1-002590ebf8cd','�������');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (21,'1ae79438-b371-11e4-80c1-002590ebf8cd','����������� ��� ����� ��������');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (21,'2f416747-b371-11e4-80c1-002590ebf8cd','�������� �����');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (21,'684f6fd0-b371-11e4-80c1-002590ebf8cd','����������-������������ ������');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (21,'85c35bbd-b371-11e4-80c1-002590ebf8cd','����������-������������ ������ ��');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (21,'8e680b0e-b371-11e4-80c1-002590ebf8cd','����������-������������ ������ ���');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (21,'9b55885c-b371-11e4-80c1-002590ebf8cd','������ ���� � ����');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (21,'a6b010e7-b371-11e4-80c1-002590ebf8cd','�����');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (21,'b8aeada5-b371-11e4-80c1-002590ebf8cd','����������� ����');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (21,'c90cb953-b371-11e4-80c1-002590ebf8cd','������������ ������');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (21,'d61e9783-b371-11e4-80c1-002590ebf8cd','������');

insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (4,'f63a9b0f-b371-11e4-80c1-002590ebf8cd','�������� ����������� ���������');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (4,'fd139fc6-b371-11e4-80c1-002590ebf8cd','�������� ���');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (4,'1c44600d-b372-11e4-80c1-002590ebf8cd','����� ��������� � ����');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (4,'618bcdd0-b372-11e4-80c1-002590ebf8cd','�������� ������');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (4,'70d2258a-b372-11e4-80c1-002590ebf8cd','����������');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (4,'7ea290d5-b372-11e4-80c1-002590ebf8cd','�������� ��� � ����');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (4,'939e8340-b372-11e4-80c1-002590ebf8cd','�������� ������');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (4,'9b014f22-b372-11e4-80c1-002590ebf8cd','�����������');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (4,'a17fc18a-b372-11e4-80c1-002590ebf8cd','�������');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (4,'aa0384ed-b372-11e4-80c1-002590ebf8cd','����������� ����� ����');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (4,'bd5c4253-b372-11e4-80c1-002590ebf8cd','�������');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (4,'087cad85-b373-11e4-80c1-002590ebf8cd','����������� ��������� ����� ���������');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (4,'26ccb85f-b373-11e4-80c1-002590ebf8cd','����������-������������ ������ ���');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (4,'f95767f6-b373-11e4-80c1-002590ebf8cd','������ ���� � ����');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (4,'3e71a675-b374-11e4-80c1-002590ebf8cd','������ ���� �� �������� ��� 2 ����������');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (4,'4c912e30-b374-11e4-80c1-002590ebf8cd','�����');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (4,'56e3206a-b374-11e4-80c1-002590ebf8cd','������������ ����� ������');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (4,'66639af6-b374-11e4-80c1-002590ebf8cd','������������ ������');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (4,'6cc780c3-b374-11e4-80c1-002590ebf8cd','������');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (4,'7a935e6e-b374-11e4-80c1-002590ebf8cd','������ ����������');

insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (3,'a607a5f3-b374-11e4-80c1-002590ebf8cd','���������� �������');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (3,'b545815a-b374-11e4-80c1-002590ebf8cd','�������� ����������� ���������');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (3,'c482bb34-b374-11e4-80c1-002590ebf8cd','�������� ���');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (3,'cd60f902-b374-11e4-80c1-002590ebf8cd','����� ��������� � ����');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (3,'d8d3598a-b374-11e4-80c1-002590ebf8cd','�������� ������');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (3,'fed1b4e4-b374-11e4-80c1-002590ebf8cd','����������');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (3,'0d0f6416-b375-11e4-80c1-002590ebf8cd','�������� ��� � ����');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (3,'15904f83-b375-11e4-80c1-002590ebf8cd','�����������');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (3,'1e0a13a3-b375-11e4-80c1-002590ebf8cd','����������� ���������');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (3,'3248b7cc-b375-11e4-80c1-002590ebf8cd','�������');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (3,'3d17c121-b375-11e4-80c1-002590ebf8cd','����������� ��� ����� ��������');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (3,'52895cb9-b375-11e4-80c1-002590ebf8cd','�������-������������ ������ ���');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (3,'a7b13cac-b375-11e4-80c1-002590ebf8cd','������ ���� � ����');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (3,'ba94ee8c-b375-11e4-80c1-002590ebf8cd','������ ���');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (3,'c3d95e44-b375-11e4-80c1-002590ebf8cd','�����');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (3,'d07367e6-b375-11e4-80c1-002590ebf8cd','������');

insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (20,'e0e8f8fe-b374-11e4-80c1-002590ebf8cd','�����������');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (20,'06131f3e-b375-11e4-80c1-002590ebf8cd','���������� �������');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (20,'2a1d44d5-b375-11e4-80c1-002590ebf8cd','�������� ����������� ���������');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (20,'3248b7cd-b375-11e4-80c1-002590ebf8cd','�������� ���');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (20,'3d17c122-b375-11e4-80c1-002590ebf8cd','����� ��������� � ����');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (20,'52895cb8-b375-11e4-80c1-002590ebf8cd','�������� ������');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (20,'59633679-b375-11e4-80c1-002590ebf8cd','����������');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (20,'72633d89-b375-11e4-80c1-002590ebf8cd','�������� ��� � ����');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (20,'7947aa36-b375-11e4-80c1-002590ebf8cd','�������� ������');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (20,'7febb700-b375-11e4-80c1-002590ebf8cd','�����������');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (20,'86f649d2-b375-11e4-80c1-002590ebf8cd','�������');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (20,'905e7d6e-b375-11e4-80c1-002590ebf8cd','������� �������');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (20,'9cf624b2-b375-11e4-80c1-002590ebf8cd','����������� ����� �����');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (20,'b1f2a199-b375-11e4-80c1-002590ebf8cd','����������-������������ ������ ���');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (20,'c3d95e45-b375-11e4-80c1-002590ebf8cd','������ ���� � ����');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (20,'e1d15510-b375-11e4-80c1-002590ebf8cd','������ ���� � ���');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (20,'1027f149-b376-11e4-80c1-002590ebf8cd','������ ���� �� �������� ��� 1 ���������');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (20,'23747497-b376-11e4-80c1-002590ebf8cd','������ ���� �� �������� ��� 2 ����������');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (20,'2ff0290f-b376-11e4-80c1-002590ebf8cd','������ ���');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (20,'5326636f-b376-11e4-80c1-002590ebf8cd','���������� ��������, �������� � ��������� ������ (������������, �����)');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (20,'61a0082d-b376-11e4-80c1-002590ebf8cd','�������');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (20,'870dfedd-b376-11e4-80c1-002590ebf8cd','������ ������������ ��� ������ �������������������� ������� (� ��������� ����������� ���������)');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (20,'9508ec3e-b376-11e4-80c1-002590ebf8cd','������������ ������');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (20,'9c7cf7da-b376-11e4-80c1-002590ebf8cd','������');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (20,'a4929773-b376-11e4-80c1-002590ebf8cd','������ ����������');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (20,'ae93646c-b376-11e4-80c1-002590ebf8cd','�������� �����');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (20,'bc1aef20-b376-11e4-80c1-002590ebf8cd','������������ ');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (20,'b0fdeaa0-d487-11e4-80c4-002590ebf8cd','�������');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (20,'102365ac-d48b-11e4-80c4-002590ebf8cd','������������ ���������� �����');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (20,'f56964b1-d6a3-11e4-80c4-002590ebf8cd','������������ ����� ������');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (20,'375c1643-d6af-11e4-80c4-002590ebf8cd','�������� ��� ��������� � ����');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (20,'61943139-d6b2-11e4-80c4-002590ebf8cd','������ ����� � ����');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (20,'150fef6f-d6eb-11e4-80c4-002590ebf8cd','�����');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (20,'7229042a-d774-11e4-80c4-002590ebf8cd','����� ������� ���������');

insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (11,'35fe41ea-b37c-11e4-80c1-002590ebf8cd','�����������');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (11,'4fb13aaa-b37c-11e4-80c1-002590ebf8cd','�������� ���');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (11,'b9b22e81-b37c-11e4-80c1-002590ebf8cd','����� ��������� � ����');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (11,'d897d270-b37c-11e4-80c1-002590ebf8cd','�������� ������');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (11,'ff5c950d-b37c-11e4-80c1-002590ebf8cd','�������� ��� � ����');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (11,'09a74bfe-b37d-11e4-80c1-002590ebf8cd','�����������');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (14,'269587b0-b37d-11e4-80c1-002590ebf8cd','����������� ����');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (11,'26ad5fba-b37d-11e4-80c1-002590ebf8cd','����������� ��������� ����� ��������');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (11,'36539cda-b37d-11e4-80c1-002590ebf8cd','����������� ��� ����� ��������');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (11,'5875376d-b37d-11e4-80c1-002590ebf8cd','����������-������������ ������ ���');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (11,'66fcfbc5-b37d-11e4-80c1-002590ebf8cd','������ ���� � ����');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (14,'6c369bda-b37d-11e4-80c1-002590ebf8cd','���������� �������');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (11,'86212bd9-b37d-11e4-80c1-002590ebf8cd','������ ���� � ���');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (14,'8b003153-b37d-11e4-80c1-002590ebf8cd','�������� ���');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (14,'a3ecbc1a-b37d-11e4-80c1-002590ebf8cd','�������');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (11,'aa5c539f-b37d-11e4-80c1-002590ebf8cd','������ ���� �� �������� ��� 1 ���������');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (11,'bee19ef5-b37d-11e4-80c1-002590ebf8cd','������ ���� �� �������� ��� 2 ����������');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (14,'c57c21e8-b37d-11e4-80c1-002590ebf8cd','����� �������');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (11,'ca4bfc66-b37d-11e4-80c1-002590ebf8cd','������ ���');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (14,'d8b4e5db-b37d-11e4-80c1-002590ebf8cd','����� ���������� � ����');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (14,'e5a53366-b37d-11e4-80c1-002590ebf8cd','�������� ������');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (14,'ecebd6c4-b37d-11e4-80c1-002590ebf8cd','���������� ������ (����� ������)');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (14,'f73b52ee-b37d-11e4-80c1-002590ebf8cd','�������� ��� � ����');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (14,'030f7599-b37e-11e4-80c1-002590ebf8cd','�������� ��� � ���������');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (14,'0b48d921-b37e-11e4-80c1-002590ebf8cd','�������� �����������');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (14,'140cea6c-b37e-11e4-80c1-002590ebf8cd','������� (����� ������)');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (14,'210b173c-b37e-11e4-80c1-002590ebf8cd','����������� ��������� ����� ��� �������� ���.���������');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (14,'353ec3d5-b37e-11e4-80c1-002590ebf8cd','����������� ��� ����� ��������');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (14,'4adb80e6-b37e-11e4-80c1-002590ebf8cd','�������-������������ ������  ��');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (14,'5c42290b-b37e-11e4-80c1-002590ebf8cd','�������-������������ ������ ��� ');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (14,'69446dea-b37e-11e4-80c1-002590ebf8cd','������ ���� � ����');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (14,'750e3c9d-b37e-11e4-80c1-002590ebf8cd','������ ���');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (14,'7de5ce8a-b37e-11e4-80c1-002590ebf8cd','����� � ��������� ���.���������');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (14,'87d685f1-b37e-11e4-80c1-002590ebf8cd','������ �����������');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (14,'a0b4f258-b37e-11e4-80c1-002590ebf8cd','������ ����������');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (11,'1d95c15d-b380-11e4-80c1-002590ebf8cd','���������� ��������, �������� � ��������� ������ (������������, �����)');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (11,'3678c199-b380-11e4-80c1-002590ebf8cd','�����');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (11,'4583ad5d-b380-11e4-80c1-002590ebf8cd','������������ ������');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (11,'531cd656-b380-11e4-80c1-002590ebf8cd','������');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (13,'ac566883-b5a8-11e4-80c1-002590ebf8cd','�������� ����������� ���������');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (13,'b3255fdd-b5a8-11e4-80c1-002590ebf8cd','�������� ���');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (13,'c1804463-b5a8-11e4-80c1-002590ebf8cd','����� ��������� � ����');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (13,'cc174864-b5a8-11e4-80c1-002590ebf8cd','�������� ������');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (13,'d4667231-b5a8-11e4-80c1-002590ebf8cd','����������� ���������');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (13,'ddff9d45-b5a8-11e4-80c1-002590ebf8cd','������� ����');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (13,'fa014520-b5a8-11e4-80c1-002590ebf8cd','�������-������������ ������ ���');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (13,'046afba9-b5a9-11e4-80c1-002590ebf8cd','������ ���� � ����');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (13,'101c1961-b5a9-11e4-80c1-002590ebf8cd','������ ���� � ���');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (13,'2991eb88-b5a9-11e4-80c1-002590ebf8cd','���������� ��������,�������� � ��������� ������ (������������,�����)');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (13,'30da7d0e-b5a9-11e4-80c1-002590ebf8cd','�����');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (13,'378812de-b5a9-11e4-80c1-002590ebf8cd','������');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (13,'41081e95-b5a9-11e4-80c1-002590ebf8cd','������������');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (1,'5eee107f-b5a9-11e4-80c1-002590ebf8cd','�������� ����������� ���������');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (1,'6799e6f3-b5a9-11e4-80c1-002590ebf8cd','�������� ���');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (1,'6f2076dd-b5a9-11e4-80c1-002590ebf8cd','������� ');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (1,'804873de-b5a9-11e4-80c1-002590ebf8cd','����� ��������� � ����');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (1,'9f87f9da-b5a9-11e4-80c1-002590ebf8cd','�������� ������');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (1,'b014fb3a-b5a9-11e4-80c1-002590ebf8cd','����������');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (1,'be1bd34d-b5a9-11e4-80c1-002590ebf8cd','�������� ��� � ����');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (1,'d5ef03be-b5a9-11e4-80c1-002590ebf8cd','�������� ��� � ���������');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (1,'e6026aee-b5a9-11e4-80c1-002590ebf8cd','�����������');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (1,'f90d27db-b5aa-11e4-80c1-002590ebf8cd','����������� ��������� ����� ��������');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (1,'5deae5a7-b5ad-11e4-80c1-002590ebf8cd','������������ ��� ����� ��������');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (1,'eecd7c49-b5ad-11e4-80c1-002590ebf8cd','�������-������������ ������ ���');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (1,'f998f4a9-b5ad-11e4-80c1-002590ebf8cd','������ ���');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (1,'059d394a-b5ae-11e4-80c1-002590ebf8cd','�����');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (1,'19e22960-b5ae-11e4-80c1-002590ebf8cd','������������ ����� ������');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (1,'2b4897a2-b5ae-11e4-80c1-002590ebf8cd','������������ ������');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (1,'3887267a-b5ae-11e4-80c1-002590ebf8cd','������ ');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (22,'b4ca3003-b5ce-11e4-80c1-002590ebf8cd','�������� ����������� ���������');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (22,'c1cfbb7d-b5ce-11e4-80c1-002590ebf8cd','�������� ���');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (22,'dff384af-b5ce-11e4-80c1-002590ebf8cd','����� ��������� � ����');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (22,'eda94a71-b5ce-11e4-80c1-002590ebf8cd','�������� ��� � ����');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (22,'f69a8565-b5ce-11e4-80c1-002590ebf8cd','�������� ������');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (22,'0002b8fd-b5cf-11e4-80c1-002590ebf8cd','�����������');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (22,'13d175f9-b5cf-11e4-80c1-002590ebf8cd','����������� ���������');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (22,'e83b9727-b5cf-11e4-80c1-002590ebf8cd','����������-������������ ������ ���');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (22,'f68f2854-b5cf-11e4-80c1-002590ebf8cd','������ ���� ');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (22,'fdad55c0-b5cf-11e4-80c1-002590ebf8cd','������ ���� � ���');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (22,'0fae4cdf-b5d0-11e4-80c1-002590ebf8cd','���������� ��������,�������� � ������� ����');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (22,'16466d23-b5d0-11e4-80c1-002590ebf8cd','�����');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (22,'3ba703c2-b5d0-11e4-80c1-002590ebf8cd','������������ �� ������');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (22,'5e0f6e5d-b5d0-11e4-80c1-002590ebf8cd','������������ ����� ������');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (22,'6b8bfb80-b5d0-11e4-80c1-002590ebf8cd','������������ ������');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (22,'7351e3f1-b5d0-11e4-80c1-002590ebf8cd','������');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (22,'79cc8918-b5d0-11e4-80c1-002590ebf8cd','�������� �����');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (22,'83a5d8a6-b5d0-11e4-80c1-002590ebf8cd','������������');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (22,'c442d148-d86f-11e4-80c4-002590ebf8cd','������������ �����������');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (22,'883030a8-d871-11e4-80c4-002590ebf8cd','������������ �����');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (8,'ea107cf8-d876-11e4-80c4-002590ebf8cd','������������ �����������');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (8,'1b905ff5-d878-11e4-80c4-002590ebf8cd','������������');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (8,'f2267666-d878-11e4-80c4-002590ebf8cd','������������ ����� ������');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (8,'79508592-d879-11e4-80c4-002590ebf8cd','����� ��������� � ����');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (14,'a33f2adb-d93e-11e4-80c4-002590ebf8cd','������������ ����������� ������');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (14,'4477917a-d93f-11e4-80c4-002590ebf8cd','������������ �����������');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (14,'dd523c27-d945-11e4-80c4-002590ebf8cd','��������������� �����');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (14,'7ab78808-d947-11e4-80c4-002590ebf8cd','����������� ��������� ����� � ��������� ���.���������');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (14,'f0d07191-d949-11e4-80c4-002590ebf8cd','����� ��� �������� ���.���������');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (14,'bfe6eb79-d94a-11e4-80c4-002590ebf8cd','�������� �����������');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (15,'932e24b0-d9f3-11e4-80c4-002590ebf8cd','������� ');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (15,'c28ac6d5-d9f3-11e4-80c4-002590ebf8cd','������������ �����������');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (15,'1d2e4794-d9f4-11e4-80c4-002590ebf8cd','�������������  �����������');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (15,'d44d9f76-d9f4-11e4-80c4-002590ebf8cd','������������');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (15,'6f5b5a63-d9f5-11e4-80c4-002590ebf8cd','������� �����');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (15,'25a3cda5-d9f7-11e4-80c4-002590ebf8cd','������ ��� � ����');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (15,'c12c8c02-d9f7-11e4-80c4-002590ebf8cd','�������� ����������');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (15,'7531fe0f-d9f9-11e4-80c4-002590ebf8cd','������������ ��������');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (15,'d71c521b-d9f9-11e4-80c4-002590ebf8cd','��������� ����������');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (15,'686245f1-d9fa-11e4-80c4-002590ebf8cd','�������� ���');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (15,'c1570080-d9fa-11e4-80c4-002590ebf8cd','������� ����������');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (15,'3e8bf974-d9fb-11e4-80c4-002590ebf8cd','����� ��������� � ����');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (15,'1de1dab7-dc24-11e4-80c4-002590ebf8cd','������� ������');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (15,'bea3e998-dc24-11e4-80c4-002590ebf8cd','������ ���� � ����');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (9,'79b04f97-dc29-11e4-80c4-002590ebf8cd','������� ����');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (9,'3292f45a-dc2a-11e4-80c4-002590ebf8cd','������ ��� � ����');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (9,'ac8bca1a-dc2a-11e4-80c4-002590ebf8cd','�������� ����������');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (9,'1df67006-dc2b-11e4-80c4-002590ebf8cd','�������� ���������� �����');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (9,'43aeb803-dc2d-11e4-80c4-002590ebf8cd','������������ ���������� �����');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (9,'1e1e6d72-dc2e-11e4-80c4-002590ebf8cd','������ ���� � ����');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (9,'d1f91242-dc2e-11e4-80c4-002590ebf8cd','�������� ���');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (9,'4eead841-dc2f-11e4-80c4-002590ebf8cd','������� ���������');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (9,'1afdd9bd-dc31-11e4-80c4-002590ebf8cd','����������� ��������� ����������');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (9,'a8f58af0-dc31-11e4-80c4-002590ebf8cd','����� ���������');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (9,'f4404d6c-dc31-11e4-80c4-002590ebf8cd','�������-������������ ������');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (17,'9fd45369-dc37-11e4-80c4-002590ebf8cd','������������ ����������');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (17,'3140bdda-dc38-11e4-80c4-002590ebf8cd','����������� ��������� ������������ �����');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (17,'87af0453-dc38-11e4-80c4-002590ebf8cd','������������');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (17,'e8848f27-dc38-11e4-80c4-002590ebf8cd','������� ���������� ��');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (17,'9e567e35-dc39-11e4-80c4-002590ebf8cd','���������� ����������');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (17,'4224fb3f-dc3a-11e4-80c4-002590ebf8cd','����������� ����������');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (17,'ac44e2b2-dc3a-11e4-80c4-002590ebf8cd','��������� ����������');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (17,'93ad513f-dc48-11e4-80c4-002590ebf8cd','������� ����������');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (17,'749ea639-dc49-11e4-80c4-002590ebf8cd','����������� ����������');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (17,'c2f96d2a-dc4a-11e4-80c4-002590ebf8cd','����� ���������');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (17,'a9c47674-dc4d-11e4-80c4-002590ebf8cd','������������ �����');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (17,'bf07172e-dc58-11e4-80c4-002590ebf8cd','������� ���� � ������������� ��-2000');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (9,'6d321df1-dc59-11e4-80c4-002590ebf8cd','������� ���� � ������������� ��-2000');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (15,'5f69248c-dc63-11e4-80c4-002590ebf8cd','�������� ���� � ������������� ��-2000');
insert into Type_tasks_to_Plants (plant_plant_id, code, name) values (17,'2517cd00-dc64-11e4-80c4-002590ebf8cd','������ ���� � ����');




update Type_Tasks_to_Plants set tptas_tptas_id = 133 where code in ('35fe41ea-b37c-11e4-80c1-002590ebf8cd','336f5b8b-b370-11e4-80c1-002590ebf8cd','e0e8f8fe-b374-11e4-80c1-002590ebf8cd');

update Type_Tasks_to_Plants set tptas_tptas_id = 143 where code in ('269587b0-b37d-11e4-80c1-002590ebf8cd');

update Type_Tasks_to_Plants set tptas_tptas_id = 169 where code in ('749ea639-dc49-11e4-80c4-002590ebf8cd');

update Type_Tasks_to_Plants set tptas_tptas_id = 131 where code in ('6c369bda-b37d-11e4-80c1-002590ebf8cd','06131f3e-b375-11e4-80c1-002590ebf8cd','a607a5f3-b374-11e4-80c1-002590ebf8cd','57c72004-b370-11e4-80c1-002590ebf8cd');

update Type_Tasks_to_Plants set tptas_tptas_id = 170 where code in ('e8848f27-dc38-11e4-80c4-002590ebf8cd');

update Type_Tasks_to_Plants set tptas_tptas_id = 37 where code in ('7aa604ff-b2aa-11e4-80c1-002590ebf8cd','5eee107f-b5a9-11e4-80c1-002590ebf8cd','b4ca3003-b5ce-11e4-80c1-002590ebf8cd','ac566883-b5a8-11e4-80c1-002590ebf8cd','64271166-b370-11e4-80c1-002590ebf8cd','f63a9b0f-b371-11e4-80c1-002590ebf8cd','b545815a-b374-11e4-80c1-002590ebf8cd','2a1d44d5-b375-11e4-80c1-002590ebf8cd','1532536d-b36f-11e4-80c1-002590ebf8cd');

update Type_Tasks_to_Plants set tptas_tptas_id = 38 where code in ('b3255fdd-b5a8-11e4-80c1-002590ebf8cd','686245f1-d9fa-11e4-80c4-002590ebf8cd','c1cfbb7d-b5ce-11e4-80c1-002590ebf8cd','6799e6f3-b5a9-11e4-80c1-002590ebf8cd','4d640ccd-b36d-11e4-80c1-002590ebf8cd','1f31e02f-b2b6-11e4-80c1-002590ebf8cd','8b003153-b37d-11e4-80c1-002590ebf8cd','4fb13aaa-b37c-11e4-80c1-002590ebf8cd','d1f91242-dc2e-11e4-80c4-002590ebf8cd','3248b7cd-b375-11e4-80c1-002590ebf8cd','c482bb34-b374-11e4-80c1-002590ebf8cd','fd139fc6-b371-11e4-80c1-002590ebf8cd','6d219706-b370-11e4-80c1-002590ebf8cd');




update Type_Tasks_to_Plants set tptas_tptas_id = 39 where code in ('a3ecbc1a-b37d-11e4-80c1-002590ebf8cd','6f2076dd-b5a9-11e4-80c1-002590ebf8cd','932e24b0-d9f3-11e4-80c4-002590ebf8cd','b0fdeaa0-d487-11e4-80c4-002590ebf8cd');

update Type_Tasks_to_Plants set tptas_tptas_id = 109 where code in ('c57c21e8-b37d-11e4-80c1-002590ebf8cd');

update Type_Tasks_to_Plants set tptas_tptas_id = 40 where code in ('a8f58af0-dc31-11e4-80c4-002590ebf8cd','c2f96d2a-dc4a-11e4-80c4-002590ebf8cd','3e8bf974-d9fb-11e4-80c4-002590ebf8cd','b9b22e81-b37c-11e4-80c1-002590ebf8cd','5b74f199-b36d-11e4-80c1-002590ebf8cd','6e27e50c-b36d-11e4-80c1-002590ebf8cd','79508592-d879-11e4-80c4-002590ebf8cd','c1804463-b5a8-11e4-80c1-002590ebf8cd','804873de-b5a9-11e4-80c1-002590ebf8cd','dff384af-b5ce-11e4-80c1-002590ebf8cd','d8b4e5db-b37d-11e4-80c1-002590ebf8cd','42d034ef-b36f-11e4-80c1-002590ebf8cd','cd60f902-b374-11e4-80c1-002590ebf8cd','3d17c122-b375-11e4-80c1-002590ebf8cd','a2801c4f-b370-11e4-80c1-002590ebf8cd','1c44600d-b372-11e4-80c1-002590ebf8cd');

update Type_Tasks_to_Plants set tptas_tptas_id = 204 where code in ('ea107cf8-d876-11e4-80c4-002590ebf8cd','c28ac6d5-d9f3-11e4-80c4-002590ebf8cd','883030a8-d871-11e4-80c4-002590ebf8cd');

update Type_Tasks_to_Plants set tptas_tptas_id = 147 where code in ('a33f2adb-d93e-11e4-80c4-002590ebf8cd','102365ac-d48b-11e4-80c4-002590ebf8cd','f56964b1-d6a3-11e4-80c4-002590ebf8cd');

update Type_Tasks_to_Plants set tptas_tptas_id = 171 where code in ('a9c47674-dc4d-11e4-80c4-002590ebf8cd');


update Type_Tasks_to_Plants set tptas_tptas_id = 172 where code in ('1afdd9bd-dc31-11e4-80c4-002590ebf8cd');
update Type_Tasks_to_Plants set tptas_tptas_id = 173 where code in ('3140bdda-dc38-11e4-80c4-002590ebf8cd');


update Type_Tasks_to_Plants set tptas_tptas_id = 41 where code in ('0b48d921-b37e-11e4-80c1-002590ebf8cd','ba1ed929-b370-11e4-80c1-002590ebf8cd');
update Type_Tasks_to_Plants set tptas_tptas_id = 194 where code in ('dd523c27-d945-11e4-80c4-002590ebf8cd');

update Type_Tasks_to_Plants set tptas_tptas_id = 11 where code in ('cc174864-b5a8-11e4-80c1-002590ebf8cd','9f87f9da-b5a9-11e4-80c1-002590ebf8cd','e5a53366-b37d-11e4-80c1-002590ebf8cd','d897d270-b37c-11e4-80c1-002590ebf8cd','d456541c-b36d-11e4-80c1-002590ebf8cd','b05a9a00-b36d-11e4-80c1-002590ebf8cd','618bcdd0-b372-11e4-80c1-002590ebf8cd','611347f3-b36f-11e4-80c1-002590ebf8cd','52895cb8-b375-11e4-80c1-002590ebf8cd','d8d3598a-b374-11e4-80c1-002590ebf8cd');

update Type_Tasks_to_Plants set tptas_tptas_id = 42 where code in ('b7c48bff-b36d-11e4-80c1-002590ebf8cd','b014fb3a-b5a9-11e4-80c1-002590ebf8cd','fed1b4e4-b374-11e4-80c1-002590ebf8cd','59633679-b375-11e4-80c1-002590ebf8cd','70d2258a-b372-11e4-80c1-002590ebf8cd','c63a82d6-b370-11e4-80c1-002590ebf8cd');



update Type_Tasks_to_Plants set tptas_tptas_id = 174 where code in ('9e567e35-dc39-11e4-80c4-002590ebf8cd');

update Type_Tasks_to_Plants set tptas_tptas_id = 148 where code in ('ecebd6c4-b37d-11e4-80c1-002590ebf8cd');

update Type_Tasks_to_Plants set tptas_tptas_id = 75 where code in ('375c1643-d6af-11e4-80c4-002590ebf8cd');





update Type_Tasks_to_Plants set tptas_tptas_id = 75 where code in ('f73b52ee-b37d-11e4-80c1-002590ebf8cd','ff5c950d-b37c-11e4-80c1-002590ebf8cd','cd65fb4b-b36d-11e4-80c1-002590ebf8cd','1a747f10-b36e-11e4-80c1-002590ebf8cd','be1bd34d-b5a9-11e4-80c1-002590ebf8cd','eda94a71-b5ce-11e4-80c1-002590ebf8cd','6e96fc0d-b36f-11e4-80c1-002590ebf8cd','72633d89-b375-11e4-80c1-002590ebf8cd','0d0f6416-b375-11e4-80c1-002590ebf8cd','d56dc877-b370-11e4-80c1-002590ebf8cd','7ea290d5-b372-11e4-80c1-002590ebf8cd');

update Type_Tasks_to_Plants set tptas_tptas_id = 76 where code in ('f0694584-b370-11e4-80c1-002590ebf8cd','d5ef03be-b5a9-11e4-80c1-002590ebf8cd','25f52bfd-b36e-11e4-80c1-002590ebf8cd','e366420b-b36d-11e4-80c1-002590ebf8cd','030f7599-b37e-11e4-80c1-002590ebf8cd');

update Type_Tasks_to_Plants set tptas_tptas_id = 176 where code in ('6d321df1-dc59-11e4-80c4-002590ebf8cd','bf07172e-dc58-11e4-80c4-002590ebf8cd','bf07172e-dc58-11e4-80c4-002590ebf8cd','5f69248c-dc63-11e4-80c4-002590ebf8cd');

update Type_Tasks_to_Plants set tptas_tptas_id = 149 where code in ('ac44e2b2-dc3a-11e4-80c4-002590ebf8cd','d71c521b-d9f9-11e4-80c4-002590ebf8cd');
update Type_Tasks_to_Plants set tptas_tptas_id = 135 where code in ('3d89ce9a-b36e-11e4-80c1-002590ebf8cd');

update Type_Tasks_to_Plants set tptas_tptas_id = 92 where code in ('939e8340-b372-11e4-80c1-002590ebf8cd','7947aa36-b375-11e4-80c1-002590ebf8cd','f69a8565-b5ce-11e4-80c1-002590ebf8cd');

update Type_Tasks_to_Plants set tptas_tptas_id = 198 where code in ('26fde859-d85b-11e4-80c4-002590ebf8cd');
update Type_Tasks_to_Plants set tptas_tptas_id = 205 where code in ('7229042a-d774-11e4-80c4-002590ebf8cd');


update Type_Tasks_to_Plants set tptas_tptas_id = 178 where code in ('4eead841-dc2f-11e4-80c4-002590ebf8cd');
update Type_Tasks_to_Plants set tptas_tptas_id = 150 where code in ('1de1dab7-dc24-11e4-80c4-002590ebf8cd');

update Type_Tasks_to_Plants set tptas_tptas_id = 151 where code in ('93ad513f-dc48-11e4-80c4-002590ebf8cd','c1570080-d9fa-11e4-80c4-002590ebf8cd');
update Type_Tasks_to_Plants set tptas_tptas_id = 44 where code in ('fbfaa4ab-b370-11e4-80c1-002590ebf8cd');
update Type_Tasks_to_Plants set tptas_tptas_id = 45 where code in ('0450a4e7-b371-11e4-80c1-002590ebf8cd','9b014f22-b372-11e4-80c1-002590ebf8cd','7febb700-b375-11e4-80c1-002590ebf8cd','15904f83-b375-11e4-80c1-002590ebf8cd','0002b8fd-b5cf-11e4-80c1-002590ebf8cd','e6026aee-b5a9-11e4-80c1-002590ebf8cd','09a74bfe-b37d-11e4-80c1-002590ebf8cd','fdfdc187-b36d-11e4-80c1-002590ebf8cd');

update Type_Tasks_to_Plants set tptas_tptas_id = 177 where code in ('4224fb3f-dc3a-11e4-80c4-002590ebf8cd');

update Type_Tasks_to_Plants set tptas_tptas_id = 46 where code in ('86f649d2-b375-11e4-80c1-002590ebf8cd','a17fc18a-b372-11e4-80c1-002590ebf8cd','0450a4e8-b371-11e4-80c1-002590ebf8cd');

update Type_Tasks_to_Plants set tptas_tptas_id = 152 where code in ('140cea6c-b37e-11e4-80c1-002590ebf8cd');

update Type_Tasks_to_Plants set tptas_tptas_id = 179 where code in ('87af0453-dc38-11e4-80c4-002590ebf8cd');

update Type_Tasks_to_Plants set tptas_tptas_id = 47 where code in ('1e0a13a3-b375-11e4-80c1-002590ebf8cd','1a747f11-b36e-11e4-80c1-002590ebf8cd','13d175f9-b5cf-11e4-80c1-002590ebf8cd','d4667231-b5a8-11e4-80c1-002590ebf8cd','210b173c-b37e-11e4-80c1-002590ebf8cd');

update Type_Tasks_to_Plants set tptas_tptas_id = 153 where code in ('7ab78808-d947-11e4-80c4-002590ebf8cd');

update Type_Tasks_to_Plants set tptas_tptas_id = 48 where code in ('905e7d6e-b375-11e4-80c1-002590ebf8cd');



update Type_Tasks_to_Plants set tptas_tptas_id = 49 where code in ('79b04f97-dc29-11e4-80c4-002590ebf8cd');
update Type_Tasks_to_Plants set tptas_tptas_id = 154 where code in ('6f5b5a63-d9f5-11e4-80c4-002590ebf8cd');

update Type_Tasks_to_Plants set tptas_tptas_id = 155 where code in ('ddff9d45-b5a8-11e4-80c1-002590ebf8cd');


update Type_Tasks_to_Plants set tptas_tptas_id = 50 where code in ('aa0384ed-b372-11e4-80c1-002590ebf8cd','9cf624b2-b375-11e4-80c1-002590ebf8cd','3332591f-b36e-11e4-80c1-002590ebf8cd','576663d7-b36e-11e4-80c1-002590ebf8cd');

update Type_Tasks_to_Plants set tptas_tptas_id = 52 where code in ('bd5c4253-b372-11e4-80c1-002590ebf8cd','3248b7cc-b375-11e4-80c1-002590ebf8cd');

update Type_Tasks_to_Plants set tptas_tptas_id = 112 where code in ('336f5b8c-b370-11e4-80c1-002590ebf8cd','087cad85-b373-11e4-80c1-002590ebf8cd','b55f1dec-b36e-11e4-80c1-002590ebf8cd','26ad5fba-b37d-11e4-80c1-002590ebf8cd','f90d27db-b5aa-11e4-80c1-002590ebf8cd');

update Type_Tasks_to_Plants set tptas_tptas_id = 78 where code in ('1ae79438-b371-11e4-80c1-002590ebf8cd','3d17c121-b375-11e4-80c1-002590ebf8cd','36539cda-b37d-11e4-80c1-002590ebf8cd','353ec3d5-b37e-11e4-80c1-002590ebf8cd','c2a7b74b-b36e-11e4-80c1-002590ebf8cd','3d89ce9b-b36e-11e4-80c1-002590ebf8cd','5deae5a7-b5ad-11e4-80c1-002590ebf8cd');

update Type_Tasks_to_Plants set tptas_tptas_id = 156 where code in ('bfe6eb79-d94a-11e4-80c4-002590ebf8cd');

update Type_Tasks_to_Plants set tptas_tptas_id = 5 where code in ('2f416747-b371-11e4-80c1-002590ebf8cd');

update Type_Tasks_to_Plants set tptas_tptas_id = 107 where code in ('f4404d6c-dc31-11e4-80c4-002590ebf8cd');
update Type_Tasks_to_Plants set tptas_tptas_id = 23 where code in ('4adb80e6-b37e-11e4-80c1-002590ebf8cd','64271167-b370-11e4-80c1-002590ebf8cd');

update Type_Tasks_to_Plants set tptas_tptas_id = 24 where code in ('b1f2a199-b375-11e4-80c1-002590ebf8cd','26ccb85f-b373-11e4-80c1-002590ebf8cd','8e680b0e-b371-11e4-80c1-002590ebf8cd','85c35bbd-b371-11e4-80c1-002590ebf8cd','684f6fd0-b371-11e4-80c1-002590ebf8cd','52895cb9-b375-11e4-80c1-002590ebf8cd','5c42290b-b37e-11e4-80c1-002590ebf8cd','576663d6-b36e-11e4-80c1-002590ebf8cd','fa014520-b5a8-11e4-80c1-002590ebf8cd','eecd7c49-b5ad-11e4-80c1-002590ebf8cd','e83b9727-b5cf-11e4-80c1-002590ebf8cd','0220923a-b36f-11e4-80c1-002590ebf8cd','5875376d-b37d-11e4-80c1-002590ebf8cd');

update Type_Tasks_to_Plants set tptas_tptas_id = 157 where code in ('f844cbd7-d85b-11e4-80c4-002590ebf8cd');

update Type_Tasks_to_Plants set tptas_tptas_id = 53 where code in ('9b55885c-b371-11e4-80c1-002590ebf8cd','f95767f6-b373-11e4-80c1-002590ebf8cd','a7b13cac-b375-11e4-80c1-002590ebf8cd','c3d95e45-b375-11e4-80c1-002590ebf8cd','ab43cac4-b370-11e4-80c1-002590ebf8cd','f68f2854-b5cf-11e4-80c1-002590ebf8cd','046afba9-b5a9-11e4-80c1-002590ebf8cd','66fcfbc5-b37d-11e4-80c1-002590ebf8cd','69446dea-b37e-11e4-80c1-002590ebf8cd','5aa874c9-b36f-11e4-80c1-002590ebf8cd','7da08454-b36e-11e4-80c1-002590ebf8cd','1e1e6d72-dc2e-11e4-80c4-002590ebf8cd','bea3e998-dc24-11e4-80c4-002590ebf8cd','2517cd00-dc64-11e4-80c4-002590ebf8cd');
update Type_Tasks_to_Plants set tptas_tptas_id = 54 where code in ('e1d15510-b375-11e4-80c1-002590ebf8cd','9351eedd-b36e-11e4-80c1-002590ebf8cd','68904dcc-b36f-11e4-80c1-002590ebf8cd','86212bd9-b37d-11e4-80c1-002590ebf8cd','101c1961-b5a9-11e4-80c1-002590ebf8cd','fdad55c0-b5cf-11e4-80c1-002590ebf8cd');

update Type_Tasks_to_Plants set tptas_tptas_id = 55 where code in ('1027f149-b376-11e4-80c1-002590ebf8cd','aa5c539f-b37d-11e4-80c1-002590ebf8cd','8bd08d64-b36f-11e4-80c1-002590ebf8cd');
update Type_Tasks_to_Plants set tptas_tptas_id = 56 where code in ('3e71a675-b374-11e4-80c1-002590ebf8cd','23747497-b376-11e4-80c1-002590ebf8cd','a75ef5e5-b36f-11e4-80c1-002590ebf8cd','bee19ef5-b37d-11e4-80c1-002590ebf8cd');

update Type_Tasks_to_Plants set tptas_tptas_id = 57 where code in ('ba1ed92a-b370-11e4-80c1-002590ebf8cd','2ff0290f-b376-11e4-80c1-002590ebf8cd','ba94ee8c-b375-11e4-80c1-002590ebf8cd','ca4bfc66-b37d-11e4-80c1-002590ebf8cd','750e3c9d-b37e-11e4-80c1-002590ebf8cd','b3678d51-b36f-11e4-80c1-002590ebf8cd','f998f4a9-b5ad-11e4-80c1-002590ebf8cd');
update Type_Tasks_to_Plants set tptas_tptas_id = 159 where code in ('25a3cda5-d9f7-11e4-80c4-002590ebf8cd','3292f45a-dc2a-11e4-80c4-002590ebf8cd');

update Type_Tasks_to_Plants set tptas_tptas_id = 100 where code in ('5326636f-b376-11e4-80c1-002590ebf8cd','1d95c15d-b380-11e4-80c1-002590ebf8cd','0fae4cdf-b5d0-11e4-80c1-002590ebf8cd','2991eb88-b5a9-11e4-80c1-002590ebf8cd');
update Type_Tasks_to_Plants set tptas_tptas_id = 161 where code in ('c63a82d7-b370-11e4-80c1-002590ebf8cd','4c912e30-b374-11e4-80c1-002590ebf8cd','a6b010e7-b371-11e4-80c1-002590ebf8cd','61a0082d-b376-11e4-80c1-002590ebf8cd','c3d95e44-b375-11e4-80c1-002590ebf8cd','30da7d0e-b5a9-11e4-80c1-002590ebf8cd','3678c199-b380-11e4-80c1-002590ebf8cd','16466d23-b5d0-11e4-80c1-002590ebf8cd','059d394a-b5ae-11e4-80c1-002590ebf8cd','b5d02ced-b2a5-11e4-80c1-002590ebf8cd','f0d07191-d949-11e4-80c4-002590ebf8cd');

update Type_Tasks_to_Plants set tptas_tptas_id = 158 where code in ('61943139-d6b2-11e4-80c4-002590ebf8cd');
update Type_Tasks_to_Plants set tptas_tptas_id = 6 where code in ('b8aeada5-b371-11e4-80c1-002590ebf8cd');
update Type_Tasks_to_Plants set tptas_tptas_id = 160 where code in ('150fef6f-d6eb-11e4-80c4-002590ebf8cd');

update Type_Tasks_to_Plants set tptas_tptas_id = 59 where code in ('7de5ce8a-b37e-11e4-80c1-002590ebf8cd');
update Type_Tasks_to_Plants set tptas_tptas_id = 163 where code in ('bb59e01d-b36e-11e4-80c1-002590ebf8cd');




update Type_Tasks_to_Plants set tptas_tptas_id = 60 where code in ('4477917a-d93f-11e4-80c4-002590ebf8cd','c442d148-d86f-11e4-80c4-002590ebf8cd');

update Type_Tasks_to_Plants set tptas_tptas_id = 162 where code in ('7531fe0f-d9f9-11e4-80c4-002590ebf8cd');

update Type_Tasks_to_Plants set tptas_tptas_id = 61 where code in ('3ba703c2-b5d0-11e4-80c1-002590ebf8cd');

update Type_Tasks_to_Plants set tptas_tptas_id = 62 where code in ('56e3206a-b374-11e4-80c1-002590ebf8cd','f2267666-d878-11e4-80c4-002590ebf8cd','5e0f6e5d-b5d0-11e4-80c1-002590ebf8cd','19e22960-b5ae-11e4-80c1-002590ebf8cd','c94dd2fd-b36f-11e4-80c1-002590ebf8cd','cda3f841-d857-11e4-80c4-002590ebf8cd');


update Type_Tasks_to_Plants set tptas_tptas_id = 165 where code in ('1d2e4794-d9f4-11e4-80c4-002590ebf8cd');

update Type_Tasks_to_Plants set tptas_tptas_id = 166 where code in ('c12c8c02-d9f7-11e4-80c4-002590ebf8cd','ac8bca1a-dc2a-11e4-80c4-002590ebf8cd');

update Type_Tasks_to_Plants set tptas_tptas_id = 180 where code in ('1df67006-dc2b-11e4-80c4-002590ebf8cd');

update Type_Tasks_to_Plants set tptas_tptas_id = 203 where code in ('66639af6-b374-11e4-80c1-002590ebf8cd','c90cb953-b371-11e4-80c1-002590ebf8cd','9508ec3e-b376-11e4-80c1-002590ebf8cd','4583ad5d-b380-11e4-80c1-002590ebf8cd','2b4897a2-b5ae-11e4-80c1-002590ebf8cd','6b8bfb80-b5d0-11e4-80c1-002590ebf8cd','d8f5ed1e-b36f-11e4-80c1-002590ebf8cd');

update Type_Tasks_to_Plants set tptas_tptas_id = 69 where code in ('d61e9783-b371-11e4-80c1-002590ebf8cd','6cc780c3-b374-11e4-80c1-002590ebf8cd','9c7cf7da-b376-11e4-80c1-002590ebf8cd','d07367e6-b375-11e4-80c1-002590ebf8cd','df9be358-b370-11e4-80c1-002590ebf8cd','f107b079-b36f-11e4-80c1-002590ebf8cd','d6698bee-b36e-11e4-80c1-002590ebf8cd','7351e3f1-b5d0-11e4-80c1-002590ebf8cd','3887267a-b5ae-11e4-80c1-002590ebf8cd','531cd656-b380-11e4-80c1-002590ebf8cd','378812de-b5a9-11e4-80c1-002590ebf8cd');
update Type_Tasks_to_Plants set tptas_tptas_id = 207 where code in ('87d685f1-b37e-11e4-80c1-002590ebf8cd');

update Type_Tasks_to_Plants set tptas_tptas_id = 129 where code in ('a4929773-b376-11e4-80c1-002590ebf8cd','7a935e6e-b374-11e4-80c1-002590ebf8cd','a0b4f258-b37e-11e4-80c1-002590ebf8cd');

update Type_Tasks_to_Plants set tptas_tptas_id = 70 where code in ('ae93646c-b376-11e4-80c1-002590ebf8cd','e7f21bde-b36e-11e4-80c1-002590ebf8cd','79cc8918-b5d0-11e4-80c1-002590ebf8cd');

update Type_Tasks_to_Plants set tptas_tptas_id = 72 where code in ('bc1aef20-b376-11e4-80c1-002590ebf8cd','83a5d8a6-b5d0-11e4-80c1-002590ebf8cd','41081e95-b5a9-11e4-80c1-002590ebf8cd','1b905ff5-d878-11e4-80c4-002590ebf8cd','d44d9f76-d9f4-11e4-80c4-002590ebf8cd');

update Type_Tasks_to_Plants set tptas_tptas_id = 29 where code in ('870dfedd-b376-11e4-80c1-002590ebf8cd');

update Type_Tasks_to_Plants set tptas_tptas_id = 168 where code in ('9fd45369-dc37-11e4-80c4-002590ebf8cd');


update Type_Tasks_to_Plants set tptas_tptas_id = 181 where code in ('43aeb803-dc2d-11e4-80c4-002590ebf8cd');
