CREATE TABLE Shtr_num (id int not null primary key,
						organization int not null,
						year int,
						month int,
						num varchar,
						type int)

ALTER TABLE Shtr_num ALTER COLUMN num int

insert into Shtr_num values (1, 1, 2016, 4, 1, null);