﻿create table dbo.[Service] (id int not null primary key, name varchar(150))

insert into dbo.[Service] values (1, 'Комбайны');
insert into dbo.[Service] values (2, 'Погрузчики');
insert into dbo.[Service] values (3, 'Автотранспорт');
insert into dbo.[Service] values (4, 'Дисикация');

create table dbo.TC_to_service (id int not null identity(1,1), id_tc int not null, id_service int not null, cost real not null);

ALTER TABLE [dbo].TC_to_service  WITH CHECK ADD  CONSTRAINT [FK_TC_to_service_ref_Service] FOREIGN KEY(id_service)
REFERENCES [dbo].Service (id)

drop table TC_to_service

CREATE TABLE [dbo].[TC_to_service](
	[id] [int] IDENTITY(1,1) NOT NULL primary key,
	[id_tc] [int] NOT NULL,
	[id_service] [int] NOT NULL,
	[cost] [real] NOT NULL
)


ALTER TABLE [dbo].[TC_to_service]  WITH CHECK ADD  CONSTRAINT [FK_TC_to_service_ref_Service] FOREIGN KEY([id_service])
REFERENCES [dbo].[Service] ([id])

ALTER TABLE [dbo].[TC_to_service]  WITH CHECK ADD  CONSTRAINT [FK_TC_to_service_ref_TC] FOREIGN KEY([id_tc])
REFERENCES [dbo].TC ([id])
GO

ALTER TABLE [dbo].[TC_to_service] CHECK CONSTRAINT [FK_TC_to_service_ref_Service]
GO

