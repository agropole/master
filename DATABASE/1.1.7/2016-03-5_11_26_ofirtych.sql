﻿CREATE TABLE KPI
(id int primary key,
name varchar(255),
tpsal_tpsal_id int,
standard_value real not null,
deviation_limit real not null,
rate int not null,
deviation_limit2 real,
rate2 int,
deviation_limit3 real,
rate3 int
)

ALTER TABLE [dbo].KPI  WITH CHECK ADD  CONSTRAINT [FK_KPI_to_Type_salary_doc] FOREIGN KEY(tpsal_tpsal_id)
REFERENCES [dbo].Type_salary_doc (tpsal_id);

CREATE TABLE KPI_to_plants
(id int primary key not null,
id_kpi int not null,
tptas_tptas_id int not null,
plant_plant_id int not null,
standard_value real,
)

ALTER TABLE [dbo].KPI_to_plants  WITH CHECK ADD  CONSTRAINT [FK_KPI_to_plants_to_Type_tasks] FOREIGN KEY(tptas_tptas_id)
REFERENCES [dbo].Type_tasks (tptas_id);
ALTER TABLE [dbo].KPI_to_plants  WITH CHECK ADD  CONSTRAINT [FK_KPI_to_plants_to_Plants] FOREIGN KEY(plant_plant_id)
REFERENCES [dbo].Plants (plant_id);

CREATE TABLE KPI_value
(id int primary key identity(1,1) not null,
id_kpi_to_plants int not null,
id_tc int,
tas_tas_id int
)

ALTER TABLE [dbo].KPI_value  WITH CHECK ADD  CONSTRAINT [FK_KPI_value_to_KPI_to_plants] FOREIGN KEY(id_kpi_to_plants)
REFERENCES [dbo].KPI_to_plants (id);
ALTER TABLE [dbo].KPI_value  WITH CHECK ADD  CONSTRAINT [FK_KPI_value_to_TC] FOREIGN KEY(id_tc)
REFERENCES [dbo].TC (id);
ALTER TABLE [dbo].KPI_value  WITH CHECK ADD  CONSTRAINT [FK_KPI_value_to_Tasks] FOREIGN KEY(tas_tas_id)
REFERENCES [dbo].Tasks (tas_id);


select * from Dictionary
insert into Dictionary values (19, 'Показатели качества', 'KPI', 'DictionaryCtrl/GetKPI', 'DictionaryCtrl/UpdateKPI', 'id', 'DictionaryCtrl/DetailKPI');

select * from Dictionary_column

insert into Dictionary_column values (87, 19, 2,'id', '', '0%');
insert into Dictionary_column values (88, 19, 1,'name',  'Название', '10%');
insert into Dictionary_column values (89, 19, 2,'tpsal_tpsal_id',  'Ед.', '5%');
insert into Dictionary_column values (90, 19, 2,'standard_value',  'Эталон. значение', '10%');
insert into Dictionary_column values (91, 19, 2,'deviation_limit',  'Отклонение 1', '15%');
insert into Dictionary_column values (92, 19, 2,'rate',  'Оценка 1', '10%');
insert into Dictionary_column values (93, 19, 2,'deviation_limit2',  'Отклонение 2', '15%');
insert into Dictionary_column values (94, 19, 2,'rate2',  'Оценка 2', '10%');
insert into Dictionary_column values (95, 19, 2,'deviation_limit3',  'Отклонение 3', '15%');
insert into Dictionary_column values (96, 19, 2,'rate3',  'Оценка 3', '10%');

insert into [Type_salary_doc] values (10, 'Сантиметр', 'см');
  insert into [Type_salary_doc] values (11, 'Километров в час', 'км/ч');
  insert into [Type_salary_doc] values (12, 'Ед. на гектар', 'ед/га');
  insert into [Type_salary_doc] values (13, 'Градус', '°');

  insert into [Type_salary_doc] values (14, 'Килограм на гибрид', 'кг/гибрид');
  insert into [Type_salary_doc] values (15, 'Процент', '%');
  insert into [Type_salary_doc] values (16, 'Семян в коробке', 'сем/кор');
  insert into [Type_salary_doc] values (17, 'Ед. на тонну', 'ед/т');
  insert into [Type_salary_doc] values (18, 'Тонн на автомобиль', 'т/авт.');
  insert into [Type_salary_doc] values (19, 'Ед. за рейс', 'кг/рейс');

  INSERT INTO KPI VALUES(	1	,'Глубина',	10	,	10	,
  	10	,	100	,	20	,	90	,	40	,	50	)
INSERT INTO KPI VALUES(	2	,'Скорость',	11	,	10	,	10	,	100	,	15	,	70	,	20	,	50	)
INSERT INTO KPI VALUES(	3	,'Перекрытие',	10	,	5	,	5	,	100	,	10	,	50	,	100	,	0	)
INSERT INTO KPI VALUES(	4	,'Замечания по нарушению качества(без расчета параметра качества)',	null,	0	,	100	,	1	,	null, null, null, null)
INSERT INTO KPI VALUES(	5	,'Равномерность внесения',	12	,	200	,	5	,	100	,	10	,	80	,	20	,	50	)
INSERT INTO KPI VALUES(	6	,'Свал/развал (высота гребней)',	10	,	5	,	10	,	100	,	20	,	90	,	40	,	50	)
INSERT INTO KPI VALUES(	8	,'Плотность посадки',	12	,	1500000	,	5	,	100	,	10	,	70	,	15	,	50	)
INSERT INTO KPI VALUES(	10	,'Норма вылива',	12	,	90	,	10	,	100	,	20	,	90	,	30	,	70	)
INSERT INTO KPI VALUES(	11	,'Глубина полива',	10	,	20	,	10	,	100	,	20	,	90	,	30	,	70	)
INSERT INTO KPI VALUES(	12	,'Разномерность внесения (толщина слоя)'	,	10	,	10	,	5	,	100	,	10	,	80	,	20	,	50	)
INSERT INTO KPI VALUES(	13	,'Степень разделки почвы. Размер комьев',	10	,	2	,	10	,	100	,	20	,	90	,	30	,	70	)
INSERT INTO KPI VALUES(	14	,'Степень разделки почвы. Количество комьев',	12	,	10	,	10	,	100	,	20	,	90	,	30	,	70	)
INSERT INTO KPI VALUES(	15	,'Равномерное распределение (отклонение от середины)',	10	,	10	,	10	,	100	,	20	,	90	,	30	,	70	)
INSERT INTO KPI VALUES(	16	,'Плотность посадки',	12	,	70000	,	5	,	100	,	10	,	70	,	15	,	50	)
INSERT INTO KPI VALUES(	17	,'Направление (угол к предыдущей обработке)',	13	,	30	,	10	,	100	,	20	,	90	,	30	,	50	)
INSERT INTO KPI VALUES(	18	,'Весовая единица для отправки на анализ',	14	,	1	,	10	,	100	,	20	,	90	,	30	,	70	)
INSERT INTO KPI VALUES(	19	,'Всхожесть',	15	,	95	,	5	,	100	,	10	,	0, null, null)
INSERT INTO KPI VALUES(	20	,'Одна посевная единица',	16	,	200000	,	5	,	100	,	10	,	0, null, null)
INSERT INTO KPI VALUES(	21	,'Дозировка семян и удобрений',	17	,	1	,	5	,	100	,	10	,	90	,	20	,	70	)
INSERT INTO KPI VALUES(	23	,'Равномерность распределения семян (отклонение от заданной нормы)',	12	,	130000	,	3	,	100	,	5	,	70	,	10	,	50	)
INSERT INTO KPI VALUES(	24	,'Длина срезанной шейки корнеплода',	10	,	1	,	50	,	100	, null, null, null, null)
INSERT INTO KPI VALUES(	25	,'Длина обломанных хвостиков корнеплода',	10	,	2	,	50	,	100	,	null, null, null, null)
INSERT INTO KPI VALUES(	26	,'% потерь корнеплодов',	15	,	2	,	5	,	100	,	10	,	80	,	20	,	50	)
INSERT INTO KPI VALUES(	27	,'Норма грузоподъемности транспорта',	18	,	100	,	5	,	100	,	10	,	80	,	20	,	50	)
INSERT INTO KPI VALUES(	28	,'% потерь в перевозке',	19	,	10	,	5	,	100	,	10	,	80	,	20	,	50	)
INSERT INTO KPI VALUES(	29	,'Равномерность',	10	,	5	,	10	,	100	,	20	,	0	,	30	,	70	)
INSERT INTO KPI VALUES(	30	,'Прямолинейность движения агрегата (отклонение от середины)',	10	,	5	,	10	,	100	,	20	,	70	,	30	,	50	)
INSERT INTO KPI VALUES(	31	,'% естественный потерь при уборке',	15	,	5	,	5	,	100	,	10	,	80	,	20	,	50	)
INSERT INTO KPI VALUES(	33	,'Норма препарата на тонну семян',	17	,	0.7	,	10	,	100	,	15	,	70	,	50	,	50	)
INSERT INTO KPI VALUES(	34	,'Объемная доля семян с отклонением',	15	,	5	,	10	,	100	,	15	,	70	,	50	,	20	)
INSERT INTO KPI VALUES(	35	,'Интенсивность окраски (отклонение по цвету)',	12	,	200	,	5	,	100	,	10	,	80	,	20	,	50	)
INSERT INTO KPI VALUES(22,'Прямолинейность посева',10,0,0,100,1,50,null,null)
INSERT INTO KPI VALUES(32,'Соответствие концентрации препарата заявленной',15,100,0,100,null,null, null, null)