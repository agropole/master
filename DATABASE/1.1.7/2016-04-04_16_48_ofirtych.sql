 ALTER TABLE [Day_task_detail] ADD trak_trak_id int;
  ALTER TABLE [Day_task_detail] ADD equi_equi_id int;

  ALTER TABLE [dbo].[Day_task_detail]  WITH CHECK ADD  CONSTRAINT [Day_task_detail_to_Traktors] FOREIGN KEY(trak_trak_id)
REFERENCES [dbo].Traktors (trakt_id)
	ALTER TABLE [dbo].[Day_task_detail]  WITH CHECK ADD  CONSTRAINT [Day_task_detail_to_Equipments] FOREIGN KEY(equi_equi_id)
REFERENCES [dbo].Equipments (equi_id)