CREATE TABLE Shtr_num (id int not null primary key,
						organization int not null,
						year int,
						month int,
						num varchar,
						type int)

ALTER TABLE Shtr_num ALTER COLUMN num int

insert into Shtr_num values (1, 1, 2016, 4, 1, null);

insert into Equipments values (2, '', 'Борона сцепка БЗТК-21', 'БЗТК-21', '', 3, null, 'Борона БЗТК-21', null, null, 0);