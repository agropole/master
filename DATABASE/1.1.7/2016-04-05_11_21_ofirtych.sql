insert into Users values (133, 'akonoplya', 'NSJpKivlE2bJk76Br4+ClXoyaQwic/iAdDJ13rT2zm9ehNCSX8pFo41FkBlor+/EPAhZ6yG43ZPgMgetLWfDUQ==', 10);

update Users set login='alimenko', password='LtpuQDgSBpeYVlINWW4XZyTNHMimrL0Iu8nr9SslFRC2YSFM1Yjb0Vr6eDRAKSm6gLwfhPofOQV7KHFwhbNKOA==', role_role_id=10 where user_id=28

CREATE TABLE Type_way_list (id int not null primary key, name varchar (15));

insert into Type_way_list values (1, 'Механизатор');
insert into Type_way_list values (2, 'Водитель');

ALTER TABLE [Traktors] ADD id_type_way_list int
ALTER TABLE [dbo].[Traktors]  WITH CHECK ADD  CONSTRAINT [FK_Traktors_to_Type_way_list] FOREIGN KEY(id_type_way_list)
REFERENCES [dbo].Type_way_list (id)

UPDATE
    tt
SET
    id_type_way_list = tot.group_type
FROM
    Traktors tt
join (select group_type, trakt_id from Group_traktors gr
	join (select * from Traktors t
			join Trakt_to_group_trakt tgr on t.trakt_id = tgr.trakt_trakt_id) grrr
	on grrr.grtrak_grtrak_id = gr.grtrak_id) tot on tt.trakt_id=tot.trakt_id

insert into Dictionary_column values (113, 14, 3, 'id_type_way_list', 'Путевой лист', '7%');
update Dictionary_column set width='10%' where id=60
update Dictionary_column set width='8%' where id=62
