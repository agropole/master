


alter table Salary_details add shift Int
go
alter table Salary_details add date datetime
go
alter table Salary_details add field_day numeric(2,1)
go

alter table Salary_details alter column worked numeric(4,1)
go
alter table Salary_details alter  column price numeric(6,2)
go
alter table Salary_details alter column sum numeric(9,2)
go