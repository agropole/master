
alter table Salary_details add value numeric(10,2);
alter table Salary_details add comment varchar(150);
alter table Salary_details add tpsal_tpsal_id int;

alter table dbo.Salary_details add constraint fk_salaries_details_ref_Type_salary_doc
foreign key (tpsal_tpsal_id)
references dbo.Type_salary_doc(tpsal_id)
on update no action
on delete no action
go

